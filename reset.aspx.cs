﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;

public partial class reset : System.Web.UI.Page
{
    string siteid = "";
    string username = "";
    int i_c = 0;
    int id = 0;
    string check_type;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                siteid = Request.QueryString["siteid"].ToString();
                username = Request.QueryString["user"].ToString();
                i_c = Convert.ToInt32((Request.QueryString["cou"].ToString()));
                id = Convert.ToInt32(Request.QueryString["i"].ToString());

                check_type = (Request.QueryString["check_type"].ToString());
                mydataaccess1 da = new mydataaccess1();
                int res = da.updaterequesteduserstatus_circleadmin(siteid, 4, id, username, i_c, check_type);

                if (res == 0)
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Reset Successfully!!!');</script>");

                }
                else
                {


                    if (res == 3)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Already Modified by another Circle Admin!!!');</script>");
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Already Reset Done by another Circle Admin!!!');</script>");
                    }

                }
            }
            catch
            { }
        }
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}
