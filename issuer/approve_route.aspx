﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="approve_route.aspx.cs" Inherits="basicinfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head id="Head1" runat="server">
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
--%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
--%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .style1
        {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
        .style2
        {
            height: 38px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class='pg'>
        <uc1:Header ID="Header1" runat="server" />
        <div class='pg-main' style="margin-top: 29px">
            <table>
                <tr>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false" 
                            PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                    </td>
                </tr>
            </table>
            <center class="style1">
                 Routes Approval
            </center>
            <hr />
            <div style="text-align: center" id="partial" runat="server">
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                <br />
                  <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Submit" Font-Names="Calibri"
                    Font-Size="13px" />
                <asp:Label ID="error" runat="server"  Font-Size="13px"></asp:Label>
               <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Issueing PTW based on feedback from the site by permit receiver and after mitigating all the risk."
                    TargetControlID="Button1">
                </cc1:ConfirmButtonExtender>
            </div>
        </div>
        <div class='foot'>
            <table width="100%">
                <tr>
                    <td style="text-align: left;">
                        <p>
                            &copy; </p>
                    </td>
                    <td style="text-align: right;">
                        <asp:Image ID="Image1" runat="server" Height="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
