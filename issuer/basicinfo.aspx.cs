﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using dataaccesslayer2;


public partial class basicinfo : System.Web.UI.Page
{
    mydataaccess2 da;

    DataTable dt;
    string ptw;
    int cat;
    protected void Page_Load(object sender, EventArgs e)
    { 

        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {

            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        cat = Convert.ToInt32(Session["cat"].ToString());
        ptw = Session["ptw"].ToString();
        if (!IsPostBack)
        {
            if (cat == 1)
            {
                Image1.ImageUrl = "../image002.png";
            }
            else if (cat == 2)
            {
                Image1.ImageUrl = "../image001.png";
            }
            else if (cat == 3)
            {
                Image1.ImageUrl = "../image003.png";
            }
            else if (cat == 4)
            {
                Image1.ImageUrl = "../image004.png";
            }

            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.ptw_pending_basic_detail_issuer(ptw);
            txtsite.Text = dt.Rows[0][0].ToString();
            TextBox1.Text = dt.Rows[0][9].ToString();
            txtloc.Text = dt.Rows[0][1].ToString();
            txtptw.Text = dt.Rows[0][2].ToString();
            txtissuedate.Text = dt.Rows[0][3].ToString();
            txtpurpose.Text = dt.Rows[0][4].ToString();
            txtfrom.Text = dt.Rows[0][5].ToString();
            
            Session["from_date"] = txtfrom.Text;
            string[] hr = txtfrom.Text.Split(' ');
            string[] m = hr[1].Split(':');
            string[] basic = hr[0].ToString().Split('/');

            string dt_basic = basic[1].ToString() + "/" + basic[0].ToString() + "/" + basic[2].ToString() + " " + m[0].ToString() + ":" + m[1].ToString() + ":00";
            Session["basic_from_date"] = dt_basic;
            txtto.Text = dt.Rows[0][6].ToString();
            txtmwfm.Text = dt.Rows[0][11].ToString();
            txtnumber.Text = dt.Rows[0][12].ToString();
            txttype.Text = dt.Rows[0][14].ToString();

            if (dt.Rows[0][8].ToString() != "")
            {
                txtpurpose0.Text = dt.Rows[0][8].ToString();
            }
        }

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        da = new mydataaccess2();
        da.ptw_update_remark_basic_detail(txtptw.Text, txtremark.Text);

      //  Response.Redirect("purpose.aspx");
     //   Response.Redirect("view_issuer_by_receiver.aspx");

 if (txttype.Text != "Planned  project")
        {
            //  Response.Redirect("purpose.aspx");
            Response.Redirect("view_issuer_by_receiver.aspx");
        }
        else
        {
            Response.Redirect("checklist.aspx");
        }
    }
    protected void txtpurpose_TextChanged(object sender, EventArgs e)
    {

    }
}
