﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="checklist.aspx.cs" Inherits="purpose" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    --%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
    --%>
    <link rel="icon" href="HTML/images/2.png" />
    <title>VIL HSW</title>
    <style type="text/css">
        .style1 {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
    </style>
    <script type="text/javascript">
        function popup(image) {
            //document.getElementById('imgsrc').src = image;
            //document.getElementById('imgsrc').src = "E:/Vodafone_New/PTW-Testing/images/camera.png";
            document.getElementById('divsub').style.opacity = 0.5;
            //$('#divmain').show();
            //$('#divsub').hide();
            //document.getElementById('imgsrc').clientHeight = screen.height;

            /*var span = document.getElementsByClassName("close")[0];
            span.onclick = function () {
            //document.getElementById('divsub').style.opacity = 0;
            $('#divmain').hide();
                
            //$('#divsub').show();
            }*/

            var lightbox =
			'<div id="lightbox" Style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -162px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt">' +
				'<p style="flaot:right;text-align:center"><input type="button" value="Close" onclick="popupclose();" style="width:200;text-align:center"/></p>' +
				'<div id="content">' + //insert clicked link's href into img src
					'<img src="' + image + '" style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -140px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt" />' +
				'</div>' +
			'</div>';

            //insert lightbox HTML into page
            $('body').append(lightbox);
        }

        function popupclose() {
            //$('#divmain').hide();
            $('#lightbox').remove();
            document.getElementById('divsub').style.opacity = 1;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div id="divsub">
        <div class='pg'>
            <uc1:Header ID="Header1" runat="server" />
            <div class='pg-main' style="margin-top: 29px">
                <table>
                    <tr>
                        <td style="text-align: right;">
                            <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false" 
                                PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <center class="style1">
                    Project Checklist</center>
                <hr />
                <asp:Label ID="Label1" runat="server" Text="Your PTW Id Is: " Font-Names="Calibri"
                    Font-Size="13px" Font-Bold="True"></asp:Label>
                <asp:TextBox ID="txtptw" runat="server" Enabled="False" Width="100%" TextMode="MultiLine"></asp:TextBox>
                <br />
                <br />
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                <br />
               
              
                <asp:Button ID="btnsave" runat="server" Text="Next" Font-Names="Calibri" Font-Size="15px"
                    OnClick="btnsave_Click" />
                <br />
                <br />
                <asp:Label ID="Label2" runat="server" Font-Names="Calibri" Font-Size="13px" ></asp:Label>
            </div>
            <div class='foot'>
                <table width="100%">
                    <tr>
                        <td style="text-align: left;">
                            <%--<p>
                            &copy; </p>--%>
                        </td>
                        <td style="text-align: right;">
                            <asp:Image ID="Image1" runat="server" Height="50px" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        </div>
        <%--<div id="divmain" style="width: 100%; display: none;">
            <div style="width: 100%;">
                <span class="close" style="font-size: 40px; float: right; color: red; padding-right: 10px;">
                    <b>
                        <asp:ImageButton runat="server" src="close-button.png" Style="width: 30px; height: 30px; margin-top: 10px"></asp:ImageButton>
                    </b>
                </span>
            </div>
            <div style="width: 100%;">
                <asp:Image ID="imgsrc" runat="server" src="#" Style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -152px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt" />
            </div>
        </div>--%>
    </form>
</body>
</html>
