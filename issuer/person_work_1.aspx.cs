﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using dataaccesslayer2;
using System.Net;
using System.Net.Mail;
using System.IO;

public partial class person_work_1 : System.Web.UI.Page
{
    DataTable dt;

    mydataaccess2 da;

    int flag = 0;

    int cat;

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {
            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        cat = Convert.ToInt32(Session["cat"].ToString());
        txtptw.Text = Session["ptw"].ToString();
        if (!IsPostBack)
        {
            ddlreason.Enabled = false;
            if (cat == 1)
            {
                Image1.ImageUrl = "../image002.png";
                //ViewState["emailid"] = "Bibek.Dey@vodafone.com";
                //  ViewState["emailid"] = "Sanjay.Khare@industowers.com";
                ViewState["emailid"] = "darshini@skyproductivity.com";
            }
            else if (cat == 2)
            {
                Image1.ImageUrl = "../image001.png";
                ViewState["emailid"] = "ranjan_bibek@rediffmail.com";
            }
            else if (cat == 3)
            {
                Image1.ImageUrl = "../image003.png";
            }
            else if (cat == 4)
            {
                Image1.ImageUrl = "../image004.png";
            }

            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.ptw_select_person_work(Session["ptw"].ToString(), Convert.ToInt32(Session["ext"].ToString()));
            GridView1.DataSource = dt;
            GridView1.DataBind();

            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.ptw_select_person_work_closed(Session["ptw"].ToString(), Convert.ToInt32(Session["ext"].ToString()));
            GridView3.DataSource = dt;
            GridView3.DataBind();

            pan.Visible = false;
        }
        else
        {
            pan.Visible = true;
            mod1.Show();
        }
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.ptw_select_person_work(Session["ptw"].ToString(), Convert.ToInt32(Session["ext"].ToString()));

        da = new mydataaccess2();
        string emailid = da.ptw_select_issuer_email(Session["receiver"].ToString());

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            da = new mydataaccess2();
            string sta = dt.Rows[i][2].ToString();
            if (sta == "Pending")
            {
                flag++;
            }
        }
        if (GridView3.Rows.Count == 0 && flag == 0)
        {
            da = new mydataaccess2();
            int count = da.ptw_select_count_status(txtptw.Text);
            string sta = "";
            if (count > 0)
            {
                //if (Convert.ToDateTime(Session["basic_from_date"].ToString()) < System.DateTime.Now)
                //{
                //    da = new mydataaccess2();
                //    dt = new DataTable();
                //    dt = da.ptw_select_date_diff(txtptw.Text, System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute);
                //    DateTime tdate = Convert.ToDateTime(dt.Rows[0][0].ToString());
                //    string year = (tdate.Year.ToString());
                //    string month = tdate.Month.ToString();
                //    string dat = tdate.Day.ToString();
                //    string hour = tdate.Hour.ToString();
                //    string min = tdate.Minute.ToString();

                //    da = new mydataaccess2();
                //    da.ptw_update_to_date_issuer(txtptw.Text, dat + "/" + month + "/" + year + " " + hour + ":" + min);

                //    da = new mydataaccess2();
                //    da.ptw_update_status_basic_detail(txtptw.Text, 2, Session["username"].ToString(), Convert.ToInt32(Session["ext"].ToString()), System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute);
                //}
                //else
                //{
                string[] date = Session["basic_from_date"].ToString().Split(' ');
                string[] date1 = date[0].ToString().Split('/');
                string[] date2 = date[1].Split(':');
                da = new mydataaccess2();
                da.ptw_update_status_basic_detail(txtptw.Text, 2, Session["username"].ToString(), Convert.ToInt32(Session["ext"].ToString()), date1[1].ToString() + "/" + date1[0].ToString() + "/" + date1[2].ToString() + " " + date2[0].ToString() + ":" + date2[1].ToString());
                //}
                da = new mydataaccess2();
                dt = new DataTable();
                dt = da.ptw_issuer_data_for_mail(Session["ptw"].ToString());
                sta = "Approved";

                MyService_JSONP MJ = new MyService_JSONP();
                string Pust_WFM = MJ.Push_WFM_ID(txtptw.Text);

                try
                {
                    string subject = "Requested PTW Status";
                    string body = "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + SendMail.ishtml(dt.Rows[0][0].ToString()) + " <br />Circle :" + SendMail.ishtml(dt.Rows[0][1].ToString()) + "<br />SiteId :" + SendMail.ishtml(dt.Rows[0][2].ToString()) + " <br />SiteName : " + SendMail.ishtml(dt.Rows[0][3].ToString()) + "<br />Issuer Name : " + SendMail.ishtml(dt.Rows[0][4].ToString()) + " <br />Status : " + SendMail.ishtml(sta) + " <br />Issued Time Stamp :" + SendMail.ishtml(dt.Rows[0][6].ToString()) + " <br />Remarks:" + SendMail.ishtml(dt.Rows[0][7].ToString()) + "<br /><br />Click Here : http://ptwtesting.skyproductivity.com/ <br /> <br /> Regards, <br />CIAT Team ";
                    SendMail sm = new SendMail();
                    sm.mailsend(emailid, subject, body);
                    // Vodafone SMTP Credentials
                    //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", emailid, "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + dt.Rows[0][0].ToString() + " <br />Circle :" + dt.Rows[0][1].ToString() + "<br />SiteId :" + dt.Rows[0][2].ToString() + " <br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Issuer Name : " + dt.Rows[0][4].ToString() + " <br />Status : " + sta + " <br />Issued Time Stamp :" + dt.Rows[0][6].ToString() + " <br />Remarks:" + dt.Rows[0][7].ToString() + "<br /><br />Click Here : http://ptwtesting.skyproductivity.com/ <br /> <br /> Regards, <br />CIAT Team ");

                    //message.IsBodyHtml = true;


                    ////SmtpClient emailClient = new SmtpClient("10.94.64.107", 25);

                    ////emailClient.Credentials =
                    ////new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                    ////emailClient.EnableSsl =
                    ////false;
                    //SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

                    //emailClient.Credentials =
                    //new NetworkCredential("support@skyproductivity.com", "sky12345");

                    //emailClient.EnableSsl =
                    //true;

                    //emailClient.Send(message);



                    // SKY SMTP Credentials
                    /*     MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", emailid, "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + dt.Rows[0][0].ToString() + " <br />Circle :" + dt.Rows[0][1].ToString() + "<br />SiteId :" + dt.Rows[0][2].ToString() + " <br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Issuer Name : " + dt.Rows[0][4].ToString() + " <br />Status : " + sta + " <br />Issued Time Stamp :" + dt.Rows[0][6].ToString() + " <br />Remarks:" + dt.Rows[0][7].ToString() + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ");

                         message.IsBodyHtml = true;


                         SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

                         emailClient.Credentials =
                         new NetworkCredential("support@skyproductivity.com", "sky12345");

                         emailClient.EnableSsl =
                         true;
                         emailClient.Send(message);*/


                    Response.Redirect("view_pending_ptw_detail.aspx", false);
                }
                catch (Exception ex)
                {

                }

            }
            else
            {
                if (Convert.ToDateTime(Session["basic_from_date"].ToString()) < System.DateTime.Now)
                {
                    da = new mydataaccess2();
                    dt = new DataTable();
                    dt = da.ptw_select_date_diff(txtptw.Text, System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute);
                    DateTime tdate = Convert.ToDateTime(dt.Rows[0][0].ToString());
                    string year = (tdate.Year.ToString());
                    string month = tdate.Month.ToString();
                    string dat = tdate.Day.ToString();
                    string hour = tdate.Hour.ToString();
                    string min = tdate.Minute.ToString();

                    da = new mydataaccess2();
                    da.ptw_update_to_date_issuer(txtptw.Text, dat + "/" + month + "/" + year + " " + hour + ":" + min);


                    da = new mydataaccess2();
                    da.ptw_update_status_basic_detail(txtptw.Text, 4, Session["username"].ToString(), Convert.ToInt32(Session["ext"].ToString()), System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute);
                }
                else
                {
                    string[] date = Session["basic_from_date"].ToString().Split(' ');
                    string[] date1 = date[0].ToString().Split('/');
                    string[] date2 = date[1].Split(':');
                    da = new mydataaccess2();
                    da.ptw_update_status_basic_detail(txtptw.Text, 4, Session["username"].ToString(), Convert.ToInt32(Session["ext"].ToString()), date1[1].ToString() + "/" + date1[0].ToString() + "/" + date1[2].ToString() + " " + date2[0].ToString() + ":" + date2[1].ToString());
                }

                da = new mydataaccess2();
                dt = new DataTable();
                dt = da.ptw_issuer_data_for_mail(Session["ptw"].ToString());
                sta = "Rejected";

                MyService_JSONP MJ = new MyService_JSONP();
                string Pust_WFM = MJ.Push_WFM_ID(txtptw.Text);

                try
                {
                    string subject = "Requested PTW Status";
                    string body = "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + SendMail.ishtml(dt.Rows[0][0].ToString()) + " <br />Circle :" + SendMail.ishtml(dt.Rows[0][1].ToString()) + "<br />SiteId :" + SendMail.ishtml(dt.Rows[0][2].ToString()) + " <br />SiteName : " + SendMail.ishtml(dt.Rows[0][3].ToString()) + "<br />Issuer Name : " + SendMail.ishtml(dt.Rows[0][4].ToString()) + " <br />Status : " + SendMail.ishtml(sta) + " <br />Issued Time Stamp :" + SendMail.ishtml(dt.Rows[0][6].ToString()) + " <br />Remarks:" + SendMail.ishtml(dt.Rows[0][7].ToString()) + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ";
                    SendMail sm = new SendMail();
                    sm.mailsend(emailid, subject, body);

                    //// Vodafone SMTP Credentials
                    //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", emailid, "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + dt.Rows[0][0].ToString() + " <br />Circle :" + dt.Rows[0][1].ToString() + "<br />SiteId :" + dt.Rows[0][2].ToString() + " <br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Issuer Name : " + dt.Rows[0][4].ToString() + " <br />Status : " + sta + " <br />Issued Time Stamp :" + dt.Rows[0][6].ToString() + " <br />Remarks:" + dt.Rows[0][7].ToString() + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ");

                    //message.IsBodyHtml = true;


                    //SmtpClient emailClient = new SmtpClient("10.94.64.107", 25);

                    //emailClient.Credentials =
                    //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                    //emailClient.EnableSsl =
                    //false;
                    //emailClient.Send(message);



                    // SKY SMTP Credentials
                    /*   MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", emailid, "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + dt.Rows[0][0].ToString() + " <br />Circle :" + dt.Rows[0][1].ToString() + "<br />SiteId :" + dt.Rows[0][2].ToString() + " <br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Issuer Name : " + dt.Rows[0][4].ToString() + " <br />Status : " + sta + " <br />Issued Time Stamp :" + dt.Rows[0][6].ToString() + " <br />Remarks:" + dt.Rows[0][7].ToString() + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ");

                       message.IsBodyHtml = true;


                       SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

                       emailClient.Credentials =
                       new NetworkCredential("support@skyproductivity.com", "sky12345");

                       emailClient.EnableSsl =
                       true;
                       emailClient.Send(message);*/


                    Response.Redirect("view_pending_ptw_detail.aspx", false);
                }
                catch (Exception ex)
                {

                }
            }
        }
        else
        {
            error.Text = "You Have Not Approved/Rejected All the Persons!!!";
        }

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            e.Row.Cells[1].Visible = false;
        }
        catch
        { }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string strUserAgent = Request.UserAgent.ToString().ToLower();

        GridViewRow gr = GridView1.SelectedRow;
        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.ptw_select_person_log_issuer(Convert.ToInt32(gr.Cells[1].Text));
        dt.Columns.Remove("step");
        dt.Columns.Remove("step1");
        dt.Columns.Remove("ptwid");
        dt.Columns.Remove("cat_id");
        dt.Columns.Remove("inspection_date");
        dt.Columns.Remove("report_date");
        dt.Columns.Remove("status");
        dt.Columns.Remove("remark_issuer");
        if (strUserAgent != null)
        {

            if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iphone") ||
            strUserAgent.Contains("blackberry") || strUserAgent.Contains("mobile") ||
            strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera mini") ||
            strUserAgent.Contains("palm"))
            {
                Session["person"] = Convert.ToInt32(gr.Cells[1].Text);
                Session["per_name"] = gr.Cells[2].Text;
                Response.Redirect("mobile_person.aspx?per=" + SpacialCharRemove.XSS_Remove(gr.Cells[1].Text));
            }

            else
            {

                Session["per_name"] = gr.Cells[2].Text;
                Session["person"] = Convert.ToInt32(gr.Cells[1].Text);
                if (Session.Count > 0)
                {
                    if (Session["imei"] == null)
                    {

                        GridView2.DataSource = dt;
                        GridView2.DataBind();
                        pan.Visible = true;
                        mod1.Show();
                        lblperson.Text = SpacialCharRemove.XSS_Remove(gr.Cells[2].Text);
                    }
                    else
                    {
                        Response.Redirect("mobile_person.aspx?per=" + SpacialCharRemove.XSS_Remove(gr.Cells[1].Text), false);
                    }
                }
            }
        }
    }

    protected void btnsave1_Click1(object sender, EventArgs e)
    {
        if (RadioButtonList1.Text == "")
        {

        }
        else
        {
            int person_id = Convert.ToInt32(Session["person"].ToString());
            string status = "";

            if (RadioButtonList1.Text == "Approve")
            {
                if (TextBox1.Text != "")
                {
                    status = "Approved";
                    da = new mydataaccess2();
                    da.ptw_update_status_person_work(TextBox1.Text, status, person_id, "");
                    Response.Redirect("person_work_1.aspx");
                }
                else
                {
                    mod1.Show();
                    Label2.Text = "*";
                }

            }
            if (RadioButtonList1.Text == "Reject")
            {
                if (ddlreason.SelectedIndex != 0)
                {
                    if (ddlreason.SelectedValue == "Others" && TextBox1.Text != "")
                    {
                        status = "Rejected";
                        da = new mydataaccess2();
                        da.ptw_update_status_person_work(TextBox1.Text, status, person_id, ddlreason.SelectedValue);
                        Response.Redirect("person_work_1.aspx");
                    }
                    else
                    {
                        mod1.Show();
                        Label2.Text = "*";
                    }
                    if (ddlreason.SelectedValue != "Others")
                    {
                        status = "Rejected";
                        da = new mydataaccess2();
                        da.ptw_update_status_person_work(TextBox1.Text, status, person_id, ddlreason.SelectedValue);
                        Response.Redirect("person_work_1.aspx");
                    }
                }
                else
                {
                    mod1.Show();
                    Label4.Text = "*";
                }

            }

        }
    }
    protected void btnsave0_Click(object sender, EventArgs e)
    {
        mod1.Hide();
    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //e.Row.Cells[3].Visible = false;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[1].Text == "No")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
                e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                e.Row.Cells[2].BackColor = System.Drawing.Color.Red;


                e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
                e.Row.Cells[1].ForeColor = System.Drawing.Color.White;
                e.Row.Cells[2].ForeColor = System.Drawing.Color.White;

            }
            Image CurrentImage = (Image)e.Row.FindControl("Image1");
            if (!File.Exists(Server.MapPath(CurrentImage.ImageUrl)))
            {
                // if image not exists, use default image
                CurrentImage.ImageUrl = "~/images/noimage.png";
                CurrentImage.Width = 40;
                // CurrentImage.ImageUrl = "";
            }
        }
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            e.Row.Cells[1].Visible = false;
        }
        catch
        { }
    }

    protected void GridView3_SelectedIndexChanged2(object sender, EventArgs e)
    {
        string strUserAgent = Request.UserAgent.ToString().ToLower();

        GridViewRow gr = GridView3.SelectedRow;
        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.ptw_select_person_log_issuer(Convert.ToInt32(gr.Cells[2].Text));
        dt.Columns.Remove("step");
        dt.Columns.Remove("step1");
        dt.Columns.Remove("ptwid");
        dt.Columns.Remove("cat_id");
        dt.Columns.Remove("inspection_date");
        dt.Columns.Remove("report_date");
        dt.Columns.Remove("status");
        dt.Columns.Remove("remark_issuer");
        if (strUserAgent != null)
        {

            if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iphone") ||
            strUserAgent.Contains("blackberry") || strUserAgent.Contains("mobile") ||
            strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera mini") ||
            strUserAgent.Contains("palm"))
            {
                Session["person"] = Convert.ToInt32(gr.Cells[2].Text);
                Session["per_name"] = SpacialCharRemove.XSS_Remove(gr.Cells[2].Text);
                Response.Redirect("mobile_person_closed.aspx?per=" + SpacialCharRemove.XSS_Remove(gr.Cells[2].Text));
            }

            else
            {
                Session["per_name"] = SpacialCharRemove.XSS_Remove(gr.Cells[2].Text);
                Session["person"] = Convert.ToInt32(SpacialCharRemove.XSS_Remove(gr.Cells[2].Text));
                if (Session.Count > 0)
                {
                    if (Session["imei"] == null)
                    {

                        GridView2.DataSource = dt;
                        GridView2.DataBind();


                        pan.Visible = true;
                        //  mod1.Show();
                        lblperson.Text = SpacialCharRemove.XSS_Remove(gr.Cells[2].Text);
                        Response.Redirect("mobile_person_closed.aspx?per=" + SpacialCharRemove.XSS_Remove(gr.Cells[2].Text), false);
                    }
                    else
                    {
                        Response.Redirect("mobile_person_closed.aspx?per=" + SpacialCharRemove.XSS_Remove(gr.Cells[2].Text), false);
                    }
                }
            }
        }

    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        //  int person_id = Convert.ToInt32(Session["person"].ToString());
        da = new mydataaccess2();
        da.ptw_update_status_person_work(SpacialCharRemove.XSS_Remove(TextBox1.Text), "Closed By Issuer", Convert.ToInt32(lb.ToolTip), "");

        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.ptw_select_person_work_closed(Session["ptw"].ToString(), Convert.ToInt32(Session["ext"].ToString()));
        GridView3.DataSource = dt;
        GridView3.DataBind();
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedIndex == 0)
        {
            ddlreason.Enabled = false;
            mod1.Show();
        }
        else
        {
            ddlreason.Enabled = true;
            mod1.Show();
        }
    }
}
