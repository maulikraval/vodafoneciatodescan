﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using business;
using dataaccesslayer2;
using System.Net;
using System.Net.Mail;

public partial class issuer_view_pending_ptw_detail : System.Web.UI.Page
{
    DataTable dt;
    vodabal ba;
    mydataaccess2 da;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {
            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        if (!IsPostBack)
        {
            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.ptw_issuer_view_basic_detail(Session["username"].ToString());
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        string ptw = lb.ToolTip;
        Session["ptw"] = ptw;
        string[] pt = ptw.Split('/');
        string receiver = pt[4].ToString();
        Session["receiver"] = receiver;

        da = new mydataaccess2();
        int ext = da.ptw_select_ext_id(ptw);
        Session["ext"] = ext;
        da = new mydataaccess2();
        int cat = da.ptw_select_cat(ptw);
        Session["cat"] = cat;
        if (cat != 5)
        {

            Response.Redirect("basicinfo.aspx");
        }
        else
        {
            Session["ptwid"] = pt[5].ToString();
            Response.Redirect("basicinfo_nt.aspx");
        }
        // Response.Redirect("basicinfo.aspx?ptw=" + ptw );
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            string status = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "status"));
            if (status.Equals("Pending for closer"))
            {

                LinkButton lb1 = (LinkButton)e.Row.Cells[1].FindControl("LinkButton2");
                LinkButton lb2 = (LinkButton)e.Row.Cells[0].FindControl("LinkButton1");
                lb1.ForeColor = System.Drawing.Color.Blue;
                lb2.ForeColor = System.Drawing.Color.Red;
                lb1.Enabled = true;
                lb2.Enabled = false;
                //lb2.Enabled = true;
            }
            else
            {

                LinkButton lb1 = (LinkButton)e.Row.Cells[1].FindControl("LinkButton2");
                //LinkButton lb2 = (LinkButton)e.Row.Cells[2].FindControl("LinkButton3");
                LinkButton lb2 = (LinkButton)e.Row.Cells[0].FindControl("LinkButton1");
                lb1.ForeColor = System.Drawing.Color.Red;
                lb2.ForeColor = System.Drawing.Color.Blue;
                lb1.Enabled = false;
                lb2.Enabled = true;

            }

        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        string ptw = lb.ToolTip;
        da = new mydataaccess2();
        da.ptw_update_status_basic_issuer(ptw);

        string sta;
        sta = "Closed By Issuer";
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();

            dt = da.ptw_issuer_data_for_mail_receiver(ptw);
            if (dt.Rows.Count > 0)
            {

                string subject = "Requested PTW Status";
                string body = "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + SendMail.ishtml(dt.Rows[0][0].ToString()) + " <br />Circle :" + SendMail.ishtml(dt.Rows[0][1].ToString()) + "<br />SiteId :" + SendMail.ishtml(dt.Rows[0][2].ToString()) + " <br />SiteName : " + SendMail.ishtml(dt.Rows[0][3].ToString()) + "<br />Issuer Name : " + SendMail.ishtml(Session["username"].ToString()) + " <br />Status : " + SendMail.ishtml(sta) + " <br />Closed Time Stamp :" + SendMail.ishtml(dt.Rows[0][6].ToString()) + " <br />Remarks:" + SendMail.ishtml(dt.Rows[0][7].ToString()) + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ";
                string tomail = dt.Rows[0][8].ToString();
                SendMail sm = new SendMail();
                sm.mailsend(tomail, subject, body);

                //// Vodafone SMTP Credentials
                //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", dt.Rows[0][8].ToString(), "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + dt.Rows[0][0].ToString() + " <br />Circle :" + dt.Rows[0][1].ToString() + "<br />SiteId :" + dt.Rows[0][2].ToString() + " <br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Issuer Name : " + Session["username"].ToString() + " <br />Status : " + sta + " <br />Closed Time Stamp :" + dt.Rows[0][6].ToString() + " <br />Remarks:" + dt.Rows[0][7].ToString() + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ");

                //message.IsBodyHtml = true;


                //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

                //emailClient.Credentials =
                //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                //emailClient.EnableSsl =
                //false;
                //emailClient.Send(message);


            }
        }
        // SKY SMTP Credentials

               //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", dt.Rows[0][0].ToString(), "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : " + dt.Rows[0][0].ToString() + " <br />Circle :" + dt.Rows[0][1].ToString() + "<br />SiteId :" + dt.Rows[0][2].ToString() + " <br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Issuer Name : " + dt.Rows[0][4].ToString() + " <br />Status : " + sta + " <br />Issued Time Stamp :" + dt.Rows[0][6].ToString() + " <br />Remarks:" + dt.Rows[0][7].ToString() + "<br /><br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT Team ");
        ////MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", dt.Rows[0][0].ToString(), "Requested PTW Status", "Dear User,<br /><br />Your Requested Details PTW Status As Below :<br /><br /> Sr. No : ");

               //message.IsBodyHtml = true;


               //SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

               //emailClient.Credentials =
        //new NetworkCredential("support@skyproductivity.com", "sky12345");

               //emailClient.EnableSsl =
        //true;
        //emailClient.Send(message);


           //    Response.Redirect("view_pending_ptw_detail.aspx", false);
        //}
        catch (Exception ex)
        {

        }



        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.ptw_issuer_view_basic_detail(Session["username"].ToString());
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
}
