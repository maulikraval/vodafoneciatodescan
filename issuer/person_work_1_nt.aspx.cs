﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using dataaccesslayer2;
using System.Net;
using System.Net.Mail;
using System.IO;

public partial class person_work_1 : System.Web.UI.Page
{
    DataTable dt;

    mydataaccess2 da;

    int flag = 0;
    string ptw = "";
    int cat;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {
            int i = 9;
            ptw = Session["ptw"].ToString();

            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        cat = Convert.ToInt32(Session["cat"].ToString());
        txtptw.Text = Session["ptw"].ToString();
        if (!IsPostBack)
        {

            if (cat == 1)
            {
                Image1.ImageUrl = "../image002.png";
                //ViewState["emailid"] = "Bibek.Dey@vodafone.com";
                //  ViewState["emailid"] = "Sanjay.Khare@industowers.com";
                ViewState["emailid"] = "darshini@skyproductivity.com";
            }
            else if (cat == 2)
            {
                Image1.ImageUrl = "../image001.png";
                ViewState["emailid"] = "ranjan_bibek@rediffmail.com";
            }
            else if (cat == 3)
            {
                Image1.ImageUrl = "../image003.png";
            }
            else if (cat == 4)
            {
                Image1.ImageUrl = "../image004.png";
            }

            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.ptw_select_person_work(Session["ptw"].ToString(), Convert.ToInt32(Session["ext"].ToString()));
            GridView1.DataSource = dt;
            GridView1.DataBind();
            int pid = Convert.ToInt32(dt.Rows[0][0].ToString());


            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.ptw_select_person_log_issuer(pid);
            dt.Columns.Remove("step");
            dt.Columns.Remove("step1");
            dt.Columns.Remove("ptwid");
            dt.Columns.Remove("cat_id");
            dt.Columns.Remove("inspection_date");
            dt.Columns.Remove("report_date");
            dt.Columns.Remove("status");
            dt.Columns.Remove("remark_issuer");
            GridView3.DataSource = dt;
            GridView3.DataBind();
        }
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        try
        {
            // mail_receiver(Session["ptw"].ToString());
            Response.Redirect("approve_route.aspx");

            //Response.Redirect("view_pending_ptw_detail.aspx", false);
        }
        catch (Exception ex)
        {

        }
    }



    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            e.Row.Cells[1].Visible = false;
        }
        catch
        { }
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //e.Row.Cells[3].Visible = false;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            /* if (e.Row.Cells[1].Text == "No")
             {
                 e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
                 e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                 e.Row.Cells[2].BackColor = System.Drawing.Color.Red;


                 e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
                 e.Row.Cells[1].ForeColor = System.Drawing.Color.White;
                 e.Row.Cells[2].ForeColor = System.Drawing.Color.White;

             }*/
            Image CurrentImage = (Image)e.Row.FindControl("Image1");
            if (!File.Exists(Server.MapPath(CurrentImage.ImageUrl)))
            {
                // if image not exists, use default image
                CurrentImage.ImageUrl = "~/images/noimage.png";
                CurrentImage.Width = 40;
                // CurrentImage.ImageUrl = "";
            }
        }
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[1].Text == "No")
                {
                    e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
                    e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
                    e.Row.Cells[2].BackColor = System.Drawing.Color.Red;


                    e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
                    e.Row.Cells[1].ForeColor = System.Drawing.Color.White;
                    e.Row.Cells[2].ForeColor = System.Drawing.Color.White;

                }
            }
        }
        catch
        { }
    }
    private void mail_receiver(string ptw)
    {
        mydataaccess2 da1 = new mydataaccess2();
        DataTable dtFinal = new DataTable();
        dtFinal = da1.ptw_select_routes(ptw);
        string MailBody = "";
        string username = dtFinal.Rows[0]["username"].ToString();
        string email = dtFinal.Rows[0]["emailid"].ToString();
        string person = dtFinal.Rows[0]["person"].ToString();
        string purpose = dtFinal.Rows[0]["basic_pur"].ToString();
        if (dtFinal.Rows.Count > 0)
        {
            MailBody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
              "<tr>" +
                  "<td style=\"background-color:red;color: #FFFFFF\" >" +
                    "<b>Route</b>" +
                  "</td>" +
                  "<td style=\"background-color:red;color: #FFFFFF\">" +
                   "<b>Route ID</b>" +
                  "</td>" +
                  "<td style=\"background-color:red;color: #FFFFFF\">" +
                     "<b>Start Time</b>" +
                  "</td>" +
                  "<td style=\"background-color:red;color: #FFFFFF\">" +
                   "<b>End Time</b>" +
                  "</td>" +
                   "<td style=\"background-color:red;color: #FFFFFF\">" +
                    "<b>Start Location</b>" +
                  "</td>" +
                   "</td>" +
                   "<td style=\"background-color:red;color: #FFFFFF\">" +
                    "<b>End Location</b>" +
                  "</td>" +
                  "<td style=\"background-color:red;color: #FFFFFF\">" +
                    "<b>Purpose</b>" +
                  "</td>" +
                  "<td style=\"background-color:red;color: #FFFFFF\">" +
                    "<b>Status</b>" +
                  "</td>" +

              "</tr>";
            //  "</table>";
            for (int loopCount = 0; loopCount < dtFinal.Rows.Count; loopCount++)
            {

                // MailBody += "<tr><td>" + dtFinal.Rows[loopCount][0] + "(" + dtFinal.Rows[loopCount][1] + ") </td></tr>";
                MailBody += "<tr>" +
                    "<td style=\"border: thin solid #FF0000\" >" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][0].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][6].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][1].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][2].ToString()) +
                    "</td>" +
                      "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][3].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][4].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][7].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][8].ToString()) +
                    "</td>" +
                "</tr>";

            }
            MailBody += "</table>";
        }
        mydataaccess2 da_1 = new mydataaccess2();
        DataTable dt_riskat = new DataTable();
        dt_riskat = da_1.select_at_risk_data_for_report_night(ptw, person);

        string riskbody = "";
        if (dt_riskat.Rows.Count > 0)
        {
            riskbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                  "<tr>" +
                      "<td style=\"width:35%;background-color:red;color: #FFFFFF\" >" +
                        "<b>Person</b>" +
                      "</td>" +
                      "<td style=\"width:35%;background-color:red;color: #FFFFFF\">" +
                       "<b>Company</b>" +
                      "</td>" +

                        "</tr>";
            riskbody += "<tr>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\" >" +
                    SendMail.ishtml(dt_riskat.Rows[0][0].ToString()) +
                  "</td>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\">" +
                     SendMail.ishtml(dt_riskat.Rows[0][6].ToString()) +
                  "</td>" +

                  "</tr>";
            riskbody += "</table>";
        }

        da_1 = new mydataaccess2();
        DataTable dt_log = new DataTable();
        dt_log = da_1.ptw_select_que_log_data_report(ptw, 5, person);

        string quelogbody = "";
        if (dt_log.Rows.Count > 0)
        {
            quelogbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\">" +
                    "<b>Consideration</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\">" +
                      "<b>Answer(Yes/No/NA)</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int q = 0; q < dt_log.Rows.Count; q++)
            {
                quelogbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (q + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            quelogbody += "</table>";
        }

        mydataaccess2 da_2 = new mydataaccess2();
        DataTable dt_log2 = new DataTable();
        dt_log2 = da_2.ptw_select_risk_data_for_report(ptw, 1, "");
        string logbody = "";
        if (dt_log2.Rows.Count > 0)
        {
            logbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\">" +
                    "<b>Risks involved</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\">" +
                      "<b>Risk Level</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:red;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int k = 0; k < dt_log2.Rows.Count; k++)
            {
                logbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (k + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            logbody += "</table>";
        }

        string subject = "PTW Status";
        string body = "Dear " + SendMail.ishtml(username) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "Purpose of visit" + SendMail.ishtml(purpose) + "Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";
        SendMail sm = new SendMail();
        sm.mailsend(email, subject, body);

        //      //string approvebody = "";
        //      //approvebody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
        //      //       "<tr>" +
        //      //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #99FF66;text-align:center;font-size:larger\" >" +
        //      //                         "<a href=\"https://ciat.vodafone.in/approve.aspx?ptwid=" + ptwid + "&flag=1&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Approve All</b></a>" +
        //      //                       "</td>" +
        //      //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #FF0000;color: #FFFFFF;text-align:center;font-size:larger\">" +
        //      //                        "<a href=\"https://ciat.vodafone.in/approve.aspx?ptwid=" + ptwid + "&flag=2&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Reject All</b></a>" +
        //      //                       "</td>" +

        //      //"</table>";

        //      //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", user_dt.Rows[j][1].ToString(), "Reset/Modify Request", "Dear Sir,<br /><br />Siteid is:" + log_[0].siteid + "<br />Sitename :" + dt1.Rows[0][1].ToString() + " <br />SiteAddress : " + dt1.Rows[0][2].ToString() + "Technician Name:" + log_[0].username_ + "<br /><br />To Modify : <br /><br/><a href=\"https://ciat.vodafone.in/modify.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Modify</a> <br /> <br />To Reset : <br /><br /><a href=\"https://ciat.vodafone.in/reset.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Reset</a> <br /> <br /> Regards, <br />CIAT Team ");
        //      // Vodafone SMTP Credentials
        //      MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", email, "PTW Status", "Dear " + username + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "Purpose of visit" + purpose + "Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
        //      message.IsBodyHtml = true;

        //       SmtpClient emailClient = new SmtpClient("10.94.64.107", 25);

        //       emailClient.Credentials =
        //       new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

        //       emailClient.EnableSsl =
        //       false;
        //    /*  SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
        //      emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
        //      emailClient.EnableSsl = true;
        //      emailClient.Send(message);
        //*/
    }


}
