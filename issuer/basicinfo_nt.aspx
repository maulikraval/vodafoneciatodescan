﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="basicinfo_nt.aspx.cs" Inherits="basicinfo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head runat="server">
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
--%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
--%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .style1
        {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
        .style2
        {
            height: 38px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class='pg'>
        <uc1:Header ID="Header1" runat="server" />
        <div class='pg-main' style="margin-top: 29px">
            <table>
                <tr>
                    <td style="text-align: right;">
                        <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false" 
                            PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                    </td>
                </tr>
            </table>
            <center class="style1">
                Basic Details
            </center>
            <hr />
            <table width="100%">
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Circle: " Font-Names="Calibri" Font-Size="13px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtloc" runat="server" Font-Names="Calibri" Font-Size="13px" Width="160px"
                            Enabled="False"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                </tr>
               
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Department and Timeframe Details:" Font-Names="Calibri"
                            Font-Size="13px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:TextBox ID="txtpurpose" runat="server" Font-Names="Calibri" Font-Size="13px"
                            ReadOnly="True" Width="292px" Enabled="False" OnTextChanged="txtpurpose_TextChanged"></asp:TextBox>
                        <br />
                        <br />
                        <asp:TextBox ID="txtpurpose0" runat="server" Font-Names="Calibri" Font-Size="13px"
                            ReadOnly="True" Width="296px" Enabled="False" TextMode="MultiLine"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text=" Vehicle Registration No.:" Font-Names="Calibri"
                            Font-Size="13px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtvehicle" runat="server" Width="160px" Font-Names="Calibri" Font-Size="13px"
                            Enabled="False"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="MWFM ID :" Font-Names="Calibri" Font-Size="13px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtmwfm" runat="server" Width="160px" Font-Names="Calibri" Font-Size="13px"
                            Enabled="False"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label11" runat="server" Text="Work Order Number :" Font-Names="Calibri" Font-Size="13px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtnumber" runat="server" Width="160px" Font-Names="Calibri" Font-Size="13px"
                            Enabled="False"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                </tr>
                  <tr>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="Type of project / activity :" Font-Names="Calibri" Font-Size="13px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txttype" runat="server" Width="160px" Font-Names="Calibri" Font-Size="13px"
                            Enabled="False"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                </tr>
                 <tr>
                    <td>
                         <asp:GridView ID="GridView2" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No data found ..!"
                                ForeColor="Black" Width="100%" AutoGenerateColumns="False" >
                                <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                <Columns>
                                    <asp:BoundField DataField="route" HeaderText="Route" />
                                    <asp:BoundField DataField="date_from" HeaderText="Start Time"/>
                                    <asp:BoundField DataField="date_to" HeaderText="End Time"/>
                                    <asp:BoundField DataField="start_location" HeaderText="Start Location"/>
                                    <asp:BoundField DataField="end_location" HeaderText="End Location"/>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Remark: " Font-Names="Calibri" Font-Size="13px"
                            Font-Bold="True"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtremark" runat="server" Width="160px" Font-Names="Calibri" Font-Size="13px"
                            Enabled="true" MaxLength="250"></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars"
                            InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," TargetControlID="txtremark">
                        </cc1:FilteredTextBoxExtender>
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnsave" runat="server" Text="Next" Font-Names="Calibri" Font-Size="13px"
                            Width="100px" OnClick="btnsave_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <div class='foot'>
            <table width="100%">
                <tr>
                    <td style="text-align: left;">
                        <p>
                            &copy; </p>
                    </td>
                    <td style="text-align: right;">
                        <asp:Image ID="Image1" runat="server" Height="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
