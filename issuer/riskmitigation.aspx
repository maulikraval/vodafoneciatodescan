﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="riskmitigation.aspx.cs" Inherits="riskmitigation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
--%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
--%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .style1
        {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class='pg'>
        <uc1:Header ID="Header1" runat="server" />
        <div class='pg-main' style="margin-top: 29px">
<table>
                    <tr>
                        <td style="text-align: right;">
                            <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false" 
                                PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                                

                        </td>
                    </tr>
                </table>
            <center class="style1">
                Risk Mitigation</center>
            <hr />
            <%-- <asp:Label ID="Label1" runat="server" Text="Your PTW Id Is: " Font-Names="Calibri"
                Font-Size="13px"></asp:Label>
            <asp:TextBox ID="txtptw" runat="server" Enabled="False" Width="250px"></asp:TextBox>--%>
            <br />
           
            <div style="text-align: left">
                <asp:GridView ID="DetailsView1" runat="server" AutoGenerateColumns="False" Height="50px"
                    Width="100%" Font-Names="Calibri" Font-Size="13px">
                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                    <FooterStyle BackColor="#CCCCCC" Height="50px"/>
                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Risk Description">
                            <ItemTemplate>
                                <asp:Label ID="lblrisk" runat="server" Text='<%# Eval("Risk_Details") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                            
                                <asp:Button ID="Button1" runat="server" CommandName="Save & Next" Text="Button" /></FooterTemplate>
                            <ItemStyle Width="170px" Wrap="true" />
                            <FooterStyle Width="170px" Wrap="true" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Proposed Action">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Width="100px" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rdfv1" runat="server" ErrorMessage="*" ControlToValidate="TextBox1">
                                </asp:RequiredFieldValidator>
                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," 
                        TargetControlID="TextBox1">
                    </cc1:FilteredTextBoxExtender>
                            </ItemTemplate>
                            <ItemStyle Width="100px" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action By">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" Width="100px" MaxLength="200"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rdfv2" runat="server" ErrorMessage="*" ControlToValidate="TextBox2">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," 
                        TargetControlID="TextBox2">
                    </cc1:FilteredTextBoxExtender>
                            </ItemTemplate>
                            <ItemStyle Width="100px" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PPE Required">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Width="100px" MaxLength="2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rdfv3" runat="server" ErrorMessage="*" ControlToValidate="TextBox3">
                                </asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," 
                        TargetControlID="TextBox3">
                    </cc1:FilteredTextBoxExtender>
                            </ItemTemplate>
                            <ItemStyle Width="100px" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Due Date">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Enabled="False"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Completed Date">
                            <ItemTemplate>
                                <asp:DropDownList ID="DropDownList4" runat="server" Width="42px">
                                    <asp:ListItem>DD</asp:ListItem>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                    <asp:ListItem>13</asp:ListItem>
                                    <asp:ListItem>14</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>16</asp:ListItem>
                                    <asp:ListItem>17</asp:ListItem>
                                    <asp:ListItem>18</asp:ListItem>
                                    <asp:ListItem>19</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>21</asp:ListItem>
                                    <asp:ListItem>22</asp:ListItem>
                                    <asp:ListItem>23</asp:ListItem>
                                    <asp:ListItem>24</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>26</asp:ListItem>
                                    <asp:ListItem>27</asp:ListItem>
                                    <asp:ListItem>28</asp:ListItem>
                                    <asp:ListItem>29</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>31</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rdfv5" runat="server" ErrorMessage="*" ControlToValidate="DropDownList4"
                                    InitialValue="DD">
                                </asp:RequiredFieldValidator>
                                <asp:DropDownList ID="DropDownList5" runat="server" Width="45px">
                                    <asp:ListItem>MM</asp:ListItem>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>11</asp:ListItem>
                                    <asp:ListItem>12</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="DropDownList5" InitialValue="MM">
                                </asp:RequiredFieldValidator>
                                <asp:DropDownList ID="DropDownList6" runat="server" Width="60px">
                                    <asp:ListItem>YYYY</asp:ListItem>
                                    <asp:ListItem>2019</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                    ControlToValidate="DropDownList6" InitialValue="YYYY">
                                </asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle Wrap="False" />
                </asp:GridView>
                <br />
                 <p >
               <asp:Label ID="Label1" runat="server" style="font-family: Calibri; font-size: 20px; text-align: justify"></asp:Label>.
                <br />
            </p>
                <asp:Button ID="btnsave" runat="server" Text="Save & Next" Font-Names="Calibri" Font-Size="15px"
                    OnClick="btnsave_Click" />
            </div>
            <br />
            
        </div>
        <div class='foot'>
            <table width="100%">
                <tr>
                    <td style="text-align: left;">
                       <%-- <p>
                            &copy; </p>--%>
                    </td>
                    <td style="text-align: right;">
                        <asp:Image ID="Image1" runat="server" Height="50px" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
