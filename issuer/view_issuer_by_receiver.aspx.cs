﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using business;
using dataaccesslayer2;
using System.Net.Mail;
using System.Net;

using System.IO;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.fonts;


public partial class issuer_view_pending_ptw_detail : System.Web.UI.Page
{
    DataTable dt;
    vodabal ba;
    mydataaccess2 da;
    int cat;
    string ptw;
    //int ptwid = 0;
    iTextSharp.text.Document doc;
    string ptwid;
    string ptwid1;

    DataTable dt_site;
    iTextSharp.text.Font verdana;
    Phrase p2;
    Phrase p1_mahesh;
    Chunk titleChunk;
    PdfTemplate template;
    BaseFont bf = null;
    iTextSharp.text.Image footer;
    string pdfFilePath;
    string file;
    string imag_file1;
    string imag_file2;
    string imag_file3;
    string vlogo;
    iTextSharp.text.Rectangle rec;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        if (!IsPostBack)
        {
            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.select_issuer(Convert.ToInt32(Session["ext"].ToString()));
            GridView1.DataSource = dt;
            GridView1.DataBind();

        }


        /*
        imag_file1 = "C:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_CIAT_07082012\\Vodafone_CIAT_07082012\\image002.png";
        imag_file2 = "C:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_CIAT_07082012\\Vodafone_CIAT_07082012\\image001.png";
        vlogo = "C:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_CIAT_07082012\\Vodafone_CIAT_07082012\\vlogo.png";
        */
    }


    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                //    Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        Response.Redirect("purpose.aspx");
    }
}
