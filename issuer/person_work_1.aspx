﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="person_work_1.aspx.cs" Inherits="person_work_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">



</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    --%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
    --%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .style1 {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
    </style>
    <script type="text/javascript">
        function GetListItem() {
            //alert('hi');
            var radioButtonlist = document.getElementsByName("<%=SpacialCharRemove.XSS_Remove(RadioButtonList1.ClientID)%>");

            for (var x = 0; x < radioButtonlist.length; x++) {
                if (radioButtonlist[x].checked) {
                    alert("Selected item Value " + radioButtonlist[x].value);
                }
            }
        }

        function popup(image) {

            document.getElementById('divsub').style.opacity = 0.5;
            var lightbox =
                '<div id="lightbox" Style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -162px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt">' +
                '<p style="flaot:right;text-align:center"><input type="button" value="Close" onclick="popupclose();" style="width:200;text-align:center"/></p>' +
                '<div id="content">' + //insert clicked link's href into img src
                '<img src="' + image + '" style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -140px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt" />' +
                '</div>' +
                '</div>';

            //insert lightbox HTML into page
            $('body').append(lightbox);
        }

        function popupclose() {
            //$('#divmain').hide();
            $('#lightbox').remove();
            document.getElementById('divsub').style.opacity = 1;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <div id="divsub">
                <div class='pg'>
                    <uc1:Header ID="Header1" runat="server" />
                    <div class='pg-main' style="margin-top: 29px">
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false"
                                        PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <center>
                            <b style="font-family: Calibri; font-size: 20px">View Associated Persons : </b>
                        </center>
                        <hr />
                        <asp:Label ID="Label1" runat="server" Text="PTW ID: " Font-Names="Calibri" Font-Size="13px"></asp:Label>
                        <asp:TextBox ID="txtptw" runat="server" Enabled="False" Width="100%" TextMode="MultiLine"></asp:TextBox>
                        &nbsp;<br />
                        <p style="font-family: Calibri; color: Red; font-size: 13px;">
                            Pending person(s) details :
                        </p>
                        <asp:GridView ID="GridView1" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                            BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No Pending person found ..!"
                            ForeColor="Black" Width="100%" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
                            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                            <Columns>
                                <asp:CommandField ShowSelectButton="True" />
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                            <RowStyle Font-Names="Calibri" Font-Size="13px" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                        <br />
                        <p style="font-family: Calibri; color: Red; font-size: 13px;">
                            Closed By Receiver :
                        </p>
                        <asp:GridView ID="GridView3" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                            BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No Closed Person found ..!"
                            ForeColor="Black" Width="100%" OnRowDataBound="GridView3_RowDataBound" OnSelectedIndexChanged="GridView3_SelectedIndexChanged2">
                            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                            <Columns>
                                <asp:TemplateField HeaderText="Close">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click" Font-Names="Calibri"
                                            Font-Size="13px" Text="Close" ToolTip='<%# Eval("id") %>'></asp:LinkButton>
                                        <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" TargetControlID="LinkButton3"
                                            ConfirmText="Are you sure ?">
                                        </cc1:ConfirmButtonExtender>
                                    </ItemTemplate>
                                    <HeaderStyle Width="25%" />
                                    <ItemStyle Font-Underline="False" ForeColor="#FF3300" HorizontalAlign="left" Font-Size="13px" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                            <RowStyle Font-Names="Calibri" Font-Size="13px" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                        <br />
                        <br />
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Submit" Font-Names="Calibri"
                            Font-Size="13px" />
                        <asp:Label ID="error" runat="server" Font-Size="13px"></asp:Label>
                        <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Issueing PTW based on feedback from the site by permit receiver and after mitigating all the risk."
                            TargetControlID="Button1">
                        </cc1:ConfirmButtonExtender>
                        <br />
                        <br />
                        <asp:UpdatePanel ID="modup" runat="server">
                            <ContentTemplate>
                                <cc1:RoundedCornersExtender ID="round1" runat="server" TargetControlID="pan" Corners="All"
                                    Radius="20" Color="#FFA500" BorderColor="#FFA500">
                                </cc1:RoundedCornersExtender>
                                <asp:Panel ID="pan" runat="server" CssClass="modalPopup" Height="430px" ScrollBars="Auto"
                                    Width="800px">
                                    <table width="100%" id="riskpersonname">
                                        <tr>
                                            <td>
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: center; font-family: Calibri; font-size: 16px; font-weight: bold;">
                                                <asp:Label ID="lblperson" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:GridView ID="GridView2" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                        BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No data found ..!"
                                        ForeColor="Black" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GridView2_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="Question" HeaderText="Question" />
                                            <asp:BoundField DataField="Answer" HeaderText="Answer" />
                                            <asp:BoundField DataField="Remark" HeaderText="Remark" />
                                            <asp:BoundField DataField="person" HeaderText="Person" />
                                            <asp:TemplateField HeaderText="Photo" HeaderStyle-Width="40px">
                                                <ItemTemplate>
                                                    <asp:ImageButton Style="margin: 5px" ID="Image1" runat="server" Height="30px"
                                                        ImageUrl='<%# Eval("Filename","~/pics/{0}") %>' Width="30px" AlternateText=" " OnClientClick="popup(this.src);" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                        <FooterStyle BackColor="#CCCCCC" />
                                        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                        <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                    <br />
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" Font-Names="Calibri" Font-Size="16px"
                                        Font-Underline="True" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                        <asp:ListItem>Approve</asp:ListItem>
                                        <asp:ListItem>Reject</asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="RadioButtonList1"
                                        ErrorMessage="*"></asp:RequiredFieldValidator><br />
                                    <br />
                                    <asp:DropDownList ID="ddlreason" runat="server" Font-Names="Calibri" Font-Size="13px">
                                        <asp:ListItem>---Select Reject Reason---</asp:ListItem>
                                        <asp:ListItem>Poor visibility</asp:ListItem>
                                        <asp:ListItem>Extreme Weather</asp:ListItem>
                                        <asp:ListItem>Absence of proper PPE</asp:ListItem>
                                        <asp:ListItem>Unavailability of proper earthing</asp:ListItem>
                                        <asp:ListItem>No supervisor at site</asp:ListItem>
                                        <asp:ListItem>Site without edge protection</asp:ListItem>
                                        <asp:ListItem>Exceeded time line</asp:ListItem>
                                        <asp:ListItem>Untrained resources</asp:ListItem>
                                        <asp:ListItem>Unfit resources</asp:ListItem>
                                        <asp:ListItem>Absence of proper anchorage point</asp:ListItem>
                                        <asp:ListItem>No permission from Govt. authority</asp:ListItem>
                                        <asp:ListItem>HT line present vicinity of work area</asp:ListItem>
                                        <asp:ListItem>Lone working</asp:ListItem>
                                        <asp:ListItem>Safety devices not available</asp:ListItem>
                                        <asp:ListItem>Work vicinity active antenna</asp:ListItem>
                                        <asp:ListItem>Emergency response not present</asp:ListItem>
                                        <asp:ListItem>1@risk wearing an implanted medical device (pacemakers and defibrillators)</asp:ListItem>
                                        <asp:ListItem>PTW information inappropriately filled up</asp:ListItem>
                                        <asp:ListItem>Others</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Label4" runat="server"></asp:Label>
                                    <br />
                                    <asp:Label ID="Label3" runat="server" Text="Remark: " Font-Names="Calibri" Font-Size="13px"
                                        Font-Bold="True"></asp:Label>
                                    <br />
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    <asp:Label ID="Label2" runat="server"></asp:Label>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="InvalidChars"
                                        InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," TargetControlID="TextBox1">
                                    </cc1:FilteredTextBoxExtender>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                ErrorMessage="Pls Enter Remark">*</asp:RequiredFieldValidator><br />--%>
                                    <br />
                                    <asp:Button ID="btnsave1" runat="server" Font-Names="Calibri" Font-Size="13px" Text="Save"
                                        Width="100px" CausesValidation="true" OnClick="btnsave1_Click1" />
                                    <asp:Button ID="btnsave0" runat="server" CausesValidation="false" Font-Names="Calibri"
                                        Font-Size="13px" Text="Cancel" Width="100px" OnClick="btnsave0_Click" />
                                    <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are You Sure?"
                                        TargetControlID="btnsave1">
                                    </cc1:ConfirmButtonExtender>
                                    <br />
                                    <br />
                                </asp:Panel>
                                <asp:LinkButton ID="butt1" runat="server"></asp:LinkButton>
                                <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="butt1" PopupControlID="pan">
                                </cc1:ModalPopupExtender>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class='foot'>
                        <table width="100%">
                            <tr>
                                <td style="text-align: left;">
                                    <%--  <p>
                                &copy; </p>--%>
                                </td>
                                <td style="text-align: right;">
                                    <asp:Image ID="Image1" runat="server" Height="50px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
