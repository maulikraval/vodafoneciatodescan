﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="person_work_1_nt.aspx.cs"
    Inherits="person_work_1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
 
  
  
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    --%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
    --%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .style1 {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
    </style>
    <script type="text/javascript">
        function popup(image) {
            document.getElementById('divsub').style.opacity = 0.5;
            var lightbox =
            '<div id="lightbox" Style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -162px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt">' +
                '<p style="flaot:right;text-align:center"><input type="button" value="Close" onclick="popupclose();" style="width:200;text-align:center"/></p>' +
                '<div id="content">' + //insert clicked link's href into img src
                    '<img src="' + image + '" style="position: fixed; width: 200px; height: 300px; top: 50%; left: 50%; margin-left: -110px; margin-top: -140px; background-color: #ffffff; border: 2px solid #336699; padding: 0px; z-index: 102; font-family: Verdana; font-size: 10pt" />' +
                '</div>' +
            '</div>';

            //insert lightbox HTML into page
            $('body').append(lightbox);
        }

        function popupclose() {
            //$('#divmain').hide();
            $('#lightbox').remove();
            document.getElementById('divsub').style.opacity = 1;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <div id="divsub">
                <div class='pg'>
                    <uc1:Header ID="Header1" runat="server" />
                    <div class='pg-main' style="margin-top: 29px">
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false" 
                                        PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                        <center>
                            <b style="font-family: Calibri; font-size: 20px">View Associated Persons : </b>
                        </center>
                        <hr />
                        <asp:Label ID="Label1" runat="server" Text="PTW ID: " Font-Names="Calibri" Font-Size="13px"></asp:Label>
                        <asp:TextBox ID="txtptw" runat="server" Enabled="False" Width="100%" TextMode="MultiLine"></asp:TextBox>
                        &nbsp;<br />
                        <p style="font-family: Calibri; color: Red; font-size: 13px;">
                            Driver details :
                        </p>
                        <asp:GridView ID="GridView1" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                            BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No Pending Driver found ..!"
                            ForeColor="Black" Width="100%" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="Associated_Person" HeaderText="Associated Driver" />
                                <asp:BoundField DataField="company_name" HeaderText="Company" />
                                <asp:BoundField DataField="circle" HeaderText="Circle" />
                            </Columns>
                            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                            <RowStyle Font-Names="Calibri" Font-Size="13px" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                        <br />
                        <p style="font-family: Calibri; color: Red; font-size: 13px;">
                            PTW form - Driver :
                        </p>
                        <asp:GridView ID="GridView3" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                            BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No Pending Driver found ..!"
                            ForeColor="Black" Width="100%" AutoGenerateColumns="False" OnRowDataBound="GridView2_RowDataBound">
                            <Columns>
                                <asp:BoundField DataField="Question" HeaderText="Question" />
                                <asp:BoundField DataField="Answer" HeaderText="Answer" />
                                <asp:BoundField DataField="Remark" HeaderText="Remark" />
                                <asp:BoundField DataField="person" HeaderText="Person" />
                                <asp:TemplateField HeaderText="Photo" HeaderStyle-Width="40px">
                                    <ItemTemplate>
                                        <asp:ImageButton Style="margin: 5px" ID="Image1" runat="server" Height="30px"
                                            ImageUrl='<%# Eval("Filename","~/pics/{0}") %>' Width="30px" AlternateText=" " OnClientClick="popup(this.src); return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                            <RowStyle Font-Names="Calibri" Font-Size="13px" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                        <br />
                        <br />
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click1" Text="Next" Font-Names="Calibri"
                            Font-Size="13px" />
                        <asp:Label ID="error" runat="server"  Font-Size="13px"></asp:Label>
                        <%--  <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Issueing PTW based on feedback from the site by permit receiver and after mitigating all the risk."
                    TargetControlID="Button1">
                </cc1:ConfirmButtonExtender>--%>
                        <br />
                        <br />
                    </div>
                    <div class='foot'>
                        <table width="100%">
                            <tr>
                                <td style="text-align: left;">
                                    <%--  <p> 
                                &copy; </p>--%>
                                </td>
                                <td style="text-align: right;">
                                    <asp:Image ID="Image1" runat="server" Height="50px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
