﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using business;
using dataaccesslayer2;

public partial class View : System.Web.UI.Page
{
    DataTable dt;
    vodabal ba;
    mydataaccess2 da;

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        if (!IsPostBack)
        {
            try
            {
                if (Session["role"].ToString() != "9")
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx",false);
            }
            
        }
    }
    protected void lnkcreate_Click(object sender, EventArgs e)
    {
        Response.Redirect("view_pending_ptw_detail.aspx");
    }
    protected void lnkview_Click(object sender, EventArgs e)
    {
        Response.Redirect("view_approved_ptw_detail.aspx");
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

             if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
}
