﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ftologout.aspx.cs" Inherits="ftologout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Logout - VIL HSW</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            <tr>
                <td>
                    <img alt="" border="0" height="1" src="../HTML/images/px1.gif" />
                </td>
                <td>
                <table width="100%">
                    <tr>
                        <td width="6%">
                            &nbsp;</td>
                        <td width="6%">
                          <%--  <img alt="" src="../images/1.jpg" height="57px" style="height: 52px; width: 56px"
                                width="57px" />--%>
                        </td>
                        <td class="style3">
                           <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                        </td>
                        <td width="25%" style="text-align: center;">
                        </td>
                    </tr>
                    <tr>
                        <td width="6%">
                            &nbsp;</td>
                        <td width="6%">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                        <td width="25%" style="text-align: center;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="6%">
                            &nbsp;</td>
                        <td width="6%">
                            &nbsp;</td>
                        <td class="style3" style="font-family: Calibri; font-size: 13px">
                            Your Session Has Been Expired.
                            </td>
                        <td width="25%" style="text-align: center;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="6%">
                            &nbsp;</td>
                        <td width="6%">
                            &nbsp;</td>
                        <td class="style3" style="font-family: Calibri; font-size: 13px">
                            Please Login From the Tool in Your Mobile.</td>
                        <td width="25%" style="text-align: center;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="6%">
                            &nbsp;</td>
                        <td width="6%">
                            &nbsp;</td>
                        <td class="style3">
                            &nbsp;</td>
                        <td width="25%" style="text-align: center;">
                            &nbsp;</td>
                    </tr>
                </table>
                </td>
                
            </tr>
        </table>
        
    </div>
    </form>
</body>
</html>
