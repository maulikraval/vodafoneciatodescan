﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using business;
using dataaccesslayer2;
using mybusiness;
using dataaccesslayer;
using System.Text.RegularExpressions;


public partial class _Default : System.Web.UI.Page
{

    mydataaccess2 da;
    vodabal vb;
    mydataaccess2 da1;
    vodabal vb1;
    DataTable dt;
    myvodav2 ba;
    string sp_imei_d = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {
            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                // Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                // Response.End();
                // Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {
            Response.Redirect("ftologout.aspx", false);
        }

        try
        {

            if (!IsPostBack)
            {

                if (Session.Count != 0)
                {
                    da = new mydataaccess2();
                    vb = new vodabal();
                    dt = new DataTable();
                    // split for decodation username.
                    string[] sp_user = Session["user"].ToString().Split('?');
                    //string sp_user_d = decode(sp_user[1].ToString());
                    string sp_user_d = Session["user"].ToString();

                    vb.User = sp_user_d;
                    txtuser.Text = Session["username"].ToString();
                    //lblusername.Text = vb.User;
                }
                else
                {

                    // Response.Redirect("~/FTO/ftologout.aspx");
                }
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("ftologout.aspx", false);
            // Response.Redirect("~/FTO/ftologout.aspx");

        }

    }

    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            Response.Redirect("ftologout.aspx", false);
        }
        catch (Exception ee)
        {
        }

    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void btncreate_Click(object sender, EventArgs e)
    {

        try
        {
            mydataaccess1 da_ = new mydataaccess1();
            ba = new myvodav2();
            ba.User = txtuser.Text;
            ba.Password = HiddenField1.Value;
            ba.Val = HiddenField2.Value;
            ba.Dump = txtpass.Text;

            Match password = Regex.Match(txtpass.Text, @"^(?=.*[a-zA-Z])(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$", RegexOptions.IgnorePatternWhitespace);

            if (password.Success)
            {



                int a = da_.password_change_ptw(ba);

                if (a == 5)
                {
                    Label1.Text = "Old Password is not correct!!!!!";
                }
                else if (a == 1 || a == 3)
                {
                    Label1.Text = "You Have Already Submitted this password before!!!!!";
                }
                else if (a == 2)
                {
                    // Label1.Text = "Your Password Has Been Successfully Changed";
                    //mod1.Show();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Password has been changed successfully');window.location ='../login/Default.aspx';", true);
                }
                else
                {
                    Label1.Text = "";
                }
            }
            else
            {
                Label1.Text = "Password must be between 8 to 19 characters. Password must contain at least one numeric and one Special character and One Capital letter.";
            }
        }
        catch
        {
            Response.Redirect("~/FTO/ftologout.aspx", false);

        }
    }
    protected void btnchangpass0_Click(object sender, EventArgs e)
    {
        try
        {

            if (Session["role"].ToString() == "9")
            {
                Response.Redirect("~/issuer/process_changepass.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void txtoldpass_TextChanged(object sender, EventArgs e)
    {

    }
}
