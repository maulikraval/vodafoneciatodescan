﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="change_password.aspx.cs"
    Inherits="_Default" %>

<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="styles.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />

    <script src="../js/jquery-1.9.0.js" type="text/javascript"></script>

    <script src="../js/crypto.js" type="text/javascript"></script>

    <script src="../js/base64.js" type="text/javascript"></script>

    <script type="text/javascript">
        //function click(){
        $('input[id$=txtpass]').val('');
        $('input[id$=txtpass0]').val('');
        $('input[id$=txtoldpass]').val('');
        function click1() {
            // alert('hi');
            var rpass = $('input[id$=txtpass]').val();
            var hash = CryptoJS.SHA256(rpass);
            var pass = hash.toString(CryptoJS.enc.Base64);
            $('input[id$=HiddenField1]').val(pass);
            //     alert($('input[id$=HiddenField1]').val());
            // alert(rpass);  
            //alert(pass);
            var rpass1 = $('input[id$=txtoldpass]').val();
            var hash1 = CryptoJS.SHA256(rpass1);
            var pass1 = hash1.toString(CryptoJS.enc.Base64);
            $('input[id$=HiddenField2]').val(pass1);
    
            // $('input[id$=txtrepass]').val(pass1);


        }
     

    </script>

    <script type="text/javascript">
        function LinkBtnMouseOver(e) {
            e.className = 'LinkBtnMouseOver';
            e.ForeColor = "Aqua"
        }
        function LinkBtnMouseOut(e) {
            e.className = 'LinkBtnMouseOut';
            e.ForeColor = "Red"
        }
    </script>

    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .LinkBtnMouseOver
        {
            color: Blue;
        }
        .LinkBtnMouseOut
        {
            color: Black;
        }
        .chromestyle
        {
            width: 100%;
            font-weight: bold;
        }
        .chromestyle ul
        {
            border: 1px solid black;
            width: 100%;
            background: url(        '../chrometheme/chromebg4.gif' ) repeat-x center; /*THEME CHANGE HERE*/
            padding: 4px 0;
            margin: 0;
            text-align: left; /*set value to "left", "center", or "right"*/
        }
        .chromestyle ul li
        {
            display: inline;
        }
        .dropmenudiv
        {
            position: absolute;
            top: 0;
            border: 1px solid #E8BEBE; /*THEME CHANGE HERE*/
            border-bottom-width: 0;
            font: normal 12px Verdana;
            line-height: 18px;
            z-index: 100;
            background-color: white;
            width: 200px;
            visibility: hidden;
        }
        p
        {
            color: #000000;
            margin-top: 5px;
            padding-bottom: 10px;
            margin-bottom: 0px;
            margin-left: 10px;
            margin-right: 10px;
            font-size: 11px;
            font-family: Tahoma,Verdana,Arial;
        }
        .style1
        {
            width: 5%;
        }
        .style2
        {
            width: 35%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
   <%-- <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>--%>
    <div class='pg'>
        <uc1:Header ID="Header1" runat="server" />
        <div class="pg-main" style="margin-top: 29px">
            <br />
            <br />
            <br />
            <table width="100%">
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="text-align: center;" colspan="2">
                        <asp:Label ID="Label2" runat="server" CssClass="lblstly" Font-Bold="False" Font-Underline="False"
                            Text="Change Password"></asp:Label>
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="text-align: right;" colspan="2">
                        <hr />
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                    </td>
                    <td style="width: 35%; text-align: right;">
                        <asp:Label ID="lblusername" runat="server" Text="User Name :" CssClass="lblall"></asp:Label>
                    </td>
                    <td style="width: 55%; text-align: left;">
                        <asp:TextBox ID="txtuser" runat="server" BackColor="#FFFFCC" Enabled="False" Width="150px"
                            CssClass="genall"></asp:TextBox>
                    </td>
                    <td style="width: 5%;">
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="width: 35%; text-align: right;">
                        <asp:Label ID="lbloldpass" runat="server" Text="Old Password :" CssClass="lblall"></asp:Label>
                    </td>
                    <td style="width: 55%; text-align: left;">
                        <asp:TextBox ID="txtoldpass" runat="server" Width="150px" OnTextChanged="txtoldpass_TextChanged"
                            TextMode="Password" CssClass="genall"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtoldpass"
                            ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="width: 35%; text-align: right;">
                        <asp:Label ID="lblpass" runat="server" Text="Password :" CssClass="lblall"></asp:Label>
                    </td>
                    <td style="width: 55%; text-align: left;">
                        <asp:TextBox ID="txtpass" runat="server" TextMode="Password" Width="150px" CssClass="genall"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpass"
                            ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" InvalidChars="'#&$%^*()-+=!|\/?;:{}[]`~,"
                            FilterMode="InvalidChars" TargetControlID="txtpass">
                        </cc1:FilteredTextBoxExtender>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtpass"
                            ValidationGroup="a" Display="None" ErrorMessage=" Password must be between 8 to 19 characters. Password must contain at least one numeric and one Special character and One Capital letter. "
                            SetFocusOnError="True" ValidationExpression="^(?=.*[a-zA-Z])(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$"></asp:RegularExpressionValidator>
                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator2">
                        </cc1:ValidatorCalloutExtender>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        &nbsp;
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="width: 35%; text-align: right;">
                        <asp:Label ID="lblpass2" runat="server" Text="Re-Type Password" CssClass="lblall"></asp:Label>
                    </td>
                    <td style="width: 55%; text-align: left;">
                        <asp:TextBox ID="txtpass0" runat="server" TextMode="Password" Width="150px" CssClass="genall"></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtpass"
                            ControlToValidate="txtpass0" ErrorMessage="Password Not Matched"></asp:CompareValidator>
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="width: 35%; text-align: right;">
                        &nbsp;
                    </td>
                    <td style="width: 55%; text-align: left;">
                        &nbsp;
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="width: 35%; text-align: right;">
                        &nbsp;
                        <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
                    </td>
                    <td style="width: 55%; text-align: left;">
                        <asp:Button ID="btnchangpass" runat="server" OnClick="btncreate_Click" Text="Change"
                            OnClientClick="click1()" />
                        &nbsp;<asp:Button ID="btnchangpass0" runat="server" OnClick="btnchangpass0_Click"
                            Text="Cancel" CausesValidation="False" />
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        &nbsp;
                    </td>
                    <td style="text-align: center; font-family: calibri; font-size: 13px; color: #FF0000;"
                        class="style2" colspan="2">
                        &nbsp; &nbsp;
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </td>
                    <td class="style1">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                    <td style="text-align: right; width: 35%;">
                        &nbsp;
                    </td>
                    <td style="text-align: left; width: 55%;">
                    </td>
                    <td style="width: 5%;">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <%--<table width="100%">
   
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="text-align: center;" colspan="2">
                     <center>
                <b style="font-family: Calibri; font-size: 20px">Change Passowrd </b>
            </center>
                </td>
                <td style="width: 5%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    </td>
                <td style="text-align: right;" colspan="2">
                   
                    </td>
                <td style="width: 5%; ">
                    </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="text-align: right;" colspan="2">
                   
                    &nbsp;</td>
                <td style="width: 5%; ">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="Label1" runat="server" Text="User Name :" CssClass="lblall" 
                        Font-Names="Calibri" Font-Size="13px"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtuser" runat="server" BackColor="#FFFFCC" Enabled="False" 
                        Width="150px" CssClass="genall" ></asp:TextBox>
                </td>
                <td style="width: 5%;">
                <br /><br />
                </td>
            </tr>
          
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lbloldpass" runat="server" Text="Old Password :" 
                        CssClass="lblall" Font-Names="Calibri" Font-Size="13px"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtoldpass" runat="server" Width="150px" 
                        OnTextChanged="txtoldpass_TextChanged" TextMode="Password" 
                        CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtoldpass" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                         
                </td>
                <td style="width: 5%;">
                    <br /><br />
                </td>
            </tr>
            
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblpass" runat="server" Text="New Password :" CssClass="lblall" 
                        Font-Names="Calibri" Font-Size="13px"></asp:Label>
                        </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtpass" runat="server" TextMode="Password" Width="150px" 
                        CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpass"
                        ErrorMessage="*" ></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtpass"
                       ValidationGroup="a" Display="None" ErrorMessage=" Password must be between 8 to 19 characters. Password must contain at least one numeric and one Special character and One Capital letter. "
                        SetFocusOnError="True" ValidationExpression="^(?=.*[a-zA-Z])(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RegularExpressionValidator2">
                    </cc1:ValidatorCalloutExtender>
                        
       

                        </td>
                <td style="width: 5%;">
                    <br /><br />
                    </td>
            </tr>
           
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblpass2" runat="server" Text="Re-Type password :" 
                        Font-Names="Calibri" Font-Size="13px"></asp:Label>
                        </td>
                <td style="width: 55%; text-align: left;">
                    
                            <asp:TextBox ID="txtpass0" runat="server"
                                TextMode="Password" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                <td style="width: 5%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    
                </td>
                <td style="width: 35%; text-align: right;">
                    &nbsp;</td>
                <td style="width: 55%; text-align: left;">
                    
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ControlToCompare="txtpass" ControlToValidate="txtpass0" 
                                ErrorMessage="Password Not Matched"></asp:CompareValidator>
                        
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    &nbsp;
                </td>
                <td style="width: 55%; text-align: left;">
                    &nbsp;
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="text-align: center;" colspan="2">
                    <asp:Button ID="btnchangpass1" runat="server" OnClick="btnchangpass0_Click" 
                        Text="Home" CausesValidation="False" Font-Names="Calibri" 
                        Font-Size="13px" PostBackUrl="~/issuer/process_changepass.aspx" />
                    &nbsp;<asp:Button ID="btnchangpass" runat="server" OnClick="btncreate_Click" 
                        Text="Change" OnClientClick="click1()" Font-Names="Calibri" 
                        Font-Size="13px" />
                        
                    &nbsp;<asp:Button ID="btnchangpass0" runat="server" OnClick="btnchangpass0_Click" 
                        Text="Cancel" CausesValidation="False" Font-Names="Calibri" 
                        Font-Size="13px" PostBackUrl="~/issuer/process_changepass.aspx" />
                        <br />
                                         
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;
                </td>
                <td style="text-align: center; font-family: calibri; font-size: 13px; color: #FF0000;" 
                    class="style2" colspan="2">
                     <asp:HiddenField ID="HiddenField1" runat="server" />
                          <asp:HiddenField ID="HiddenField2" runat="server" />
                    
                </td>
                <td class="style1">
                    &nbsp;
                    </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="text-align: center; " colspan="2">
                    &nbsp;
                                  
                <asp:Label ID="Label3" runat="server" Font-Size="13px" Font-Names="Calibri" Visible="true"
                    ForeColor="#F60100"></asp:Label>
         
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="text-align: justify; " colspan="2">
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtpass"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" InvalidChars="'#&$%^*()-+=!|\/?;:{}[]`~,"
                        FilterMode="InvalidChars" TargetControlID="txtpass">
                    </cc1:FilteredTextBoxExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtpass"
                        Display="None" ErrorMessage=" It must be between 8 and 10 characters.First two letters must be character and third must be underscore(_),fourth character should be numeric and remaining characters. "
                        SetFocusOnError="True" ValidationExpression="([a-z0-9_-]+(\.[a-z0-9_-]+)*_[a-zA-Z0-9_]{4,6})$"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator2">
                    </cc1:ValidatorCalloutExtender>
         
                </td>
                <td style="width: 5%;">
                    &nbsp;</td>
            </tr>
        </table>--%>
            <br />
            <br />
        </div>
    </div>
    </form>
</body>
</html>
