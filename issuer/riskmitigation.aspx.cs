﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class riskmitigation : System.Web.UI.Page
{
    int count = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {
            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        if (!IsPostBack)
        {

            if ((Convert.ToInt32(Session["cat"].ToString()) == 1))
            {
                Label1.Text = "It is ensured that all risks identified are mitigated  and reduced as low as possible by appropriate action before executing the Work at Height.";
                Image1.ImageUrl = "../image002.png";
            }
            if ((Convert.ToInt32(Session["cat"].ToString()) == 2))
            {
                Label1.Text = "It is ensured that all risks identified are mitigated  and reduced as low as possible by appropriate action before executing the Work at Electrical.";
                Image1.ImageUrl = "../image001.png";
            }
            if ((Convert.ToInt32(Session["cat"].ToString()) == 3))
            {
                Label1.Text = " It is ensured that all risks identified are mitigated  and reduced as low as possible by appropriate action before executing the Work at Underground activity.";
                Image1.ImageUrl = "../image003.png";
            }
            if ((Convert.ToInt32(Session["cat"].ToString()) == 4))
            {
                Label1.Text = "It is ensured that all risks identified are mitigated  and reduced as low as possible by appropriate action before executing the Work at RF energy.";
                Image1.ImageUrl = "../image004.png";
            }
            dataaccesslayer2.mydataaccess2 da = new dataaccesslayer2.mydataaccess2();
            string ptw = Session["ptw"].ToString();


            da = new dataaccesslayer2.mydataaccess2();
            DataTable dt1 = new DataTable();
            dt1 = da.ptw_select_risk_mitigation_new(Convert.ToInt32(Session["ext"].ToString()));
            DetailsView1.DataSource = dt1;
            DetailsView1.DataBind();


            da = new dataaccesslayer2.mydataaccess2();
            DataTable dt = new DataTable();
            dt = da.ptw_select_risk_back_data(Convert.ToInt32(Session["ext"].ToString()));
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Label lb = (Label)DetailsView1.Rows[i].Cells[0].FindControl("lblrisk");
                    lb.Text = SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString());
                    TextBox txt1 = (TextBox)DetailsView1.Rows[i].Cells[1].FindControl("TextBox1");
                    txt1.Text = dt.Rows[i][1].ToString();
                    TextBox txt2 = (TextBox)DetailsView1.Rows[i].Cells[2].FindControl("TextBox2");
                    txt2.Text = dt.Rows[i][2].ToString();
                    TextBox txt3 = (TextBox)DetailsView1.Rows[i].Cells[3].FindControl("TextBox3");
                    txt3.Text = dt.Rows[i][3].ToString();
                    TextBox txt4 = (TextBox)DetailsView1.Rows[i].Cells[3].FindControl("TextBox4");
                    txt4.Text = Session["from_date"].ToString();

                    string from = dt.Rows[i][5].ToString();
                    string[] hr_ = from.Split(' ');
                    string[] basic_ = hr_[0].ToString().Split('/');

                    DropDownList drp4 = (DropDownList)DetailsView1.Rows[i].Cells[5].FindControl("DropDownList4");
                    DropDownList drp5 = (DropDownList)DetailsView1.Rows[i].Cells[5].FindControl("DropDownList5");
                    DropDownList drp6 = (DropDownList)DetailsView1.Rows[i].Cells[5].FindControl("DropDownList6");


                    drp4.Text = SpacialCharRemove.XSS_Remove(basic_[0].ToString());
                    drp5.Text = SpacialCharRemove.XSS_Remove(basic_[1].ToString());
                    drp6.Text = SpacialCharRemove.XSS_Remove(basic_[2].ToString());

                }
            }
            else
            {
                da = new dataaccesslayer2.mydataaccess2();
                DataTable dt2 = new DataTable();
                dt2 = da.ptw_select_risk_mitigation_new(Convert.ToInt32(Session["ext"].ToString()));
                DetailsView1.DataSource = dt2;
                DetailsView1.DataBind();

                for (int i = 0; i < DetailsView1.Rows.Count; i++)
                {
                    TextBox txt3 = (TextBox)DetailsView1.Rows[i].Cells[3].FindControl("TextBox4");
                    txt3.Text = Session["from_date"].ToString();
                }
                //  count = dt.Rows.Count;

            }


        }
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            dataaccesslayer2.mydataaccess2 da;

            da = new dataaccesslayer2.mydataaccess2();
            DataTable dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new dataaccesslayer2.mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {

        for (int i = 0; i < DetailsView1.Rows.Count; i++)
        {
            Label lb = (Label)DetailsView1.Rows[i].Cells[0].FindControl("lblrisk");
            TextBox txt1 = (TextBox)DetailsView1.Rows[i].Cells[1].FindControl("TextBox1");
            TextBox txt2 = (TextBox)DetailsView1.Rows[i].Cells[2].FindControl("TextBox2");
            TextBox txt3 = (TextBox)DetailsView1.Rows[i].Cells[3].FindControl("TextBox3");
            TextBox txt4 = (TextBox)DetailsView1.Rows[i].Cells[3].FindControl("TextBox4");

            DropDownList drp4 = (DropDownList)DetailsView1.Rows[i].Cells[5].FindControl("DropDownList4");
            DropDownList drp5 = (DropDownList)DetailsView1.Rows[i].Cells[5].FindControl("DropDownList5");
            DropDownList drp6 = (DropDownList)DetailsView1.Rows[i].Cells[5].FindControl("DropDownList6");


            string due_date = txt4.Text; ;
            string com_date = drp4.SelectedValue + "/" + drp5.SelectedValue + "/" + drp6.SelectedValue;
            dataaccesslayer2.mydataaccess2 da = new dataaccesslayer2.mydataaccess2();
            da.ptw_insert_into_mitigation(lb.Text, txt1.Text, txt2.Text, txt3.Text, due_date, com_date, Session["ext"].ToString());




        }
        Response.Redirect("person_work_1.aspx");
        //Session["risk_value"] = RadioButtonList1.SelectedValue;

    }
    protected void DetailsView1_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
    {

    }
    protected void DetailsView1_PageIndexChanging1(object sender, DetailsViewPageEventArgs e)
    {
        //DetailsView1.PageIndex = e.NewPageIndex;
        // DetailsView1.DataBind();
    }
}
