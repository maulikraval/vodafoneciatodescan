﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="view_issuer_by_receiver.aspx.cs"
    Inherits="issuer_view_pending_ptw_detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/issuer/header.ascx" TagPrefix="uc1" TagName="Header" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">


</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
--%>
    <link href="../styles.css" rel="stylesheet" type="text/css" media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
--%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>
    <style type="text/css">
        .style1
        {
            font-size: 20px;
            font-weight: bold;
            font-family: Calibri;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class='pg'>
            <uc1:Header ID="Header1" runat="server" />
            <div class='pg-main' style="margin-top: 29px">
                <table>
                    <tr>
                        <td style="text-align: right;">
                            <asp:LinkButton ID="lnkhome" runat="server" CausesValidation="false" 
                                PostBackUrl="~/issuer/view.aspx">Home</asp:LinkButton>
                        </td>
                    </tr>
                </table>
                <center>
                    <b style="font-family: Calibri; font-size: 20px">View Issuer Details </b>
                </center>
                <hr />
                <asp:GridView ID="GridView1" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No Pending Ptw found ..!"
                    ForeColor="Black" Width="100%">
                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
            </div>
            <br />
                <asp:Button ID="btnsave" runat="server" Text="Next" Font-Names="Calibri" Font-Size="13px"
                            Width="100px" onclick="btnsave_Click"/>
            <div class='foot'>
               <%-- <p>
                    &copy; </p>--%>
            </div>
        </div>
        <%--  <asp:GridView ID="GridView1" runat="server" Font-Names="Calibri" 
            Font-Size="15px" Width="100%">
            <Columns>
                <asp:TemplateField HeaderText="Select">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Select" 
                            ToolTip='<%# Eval("Sr_No") %>' onclick="LinkButton1_Click" 
                            Font-Bold="True" Font-Size="15px" Font-Underline="True" ForeColor="#000099"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>--%>
    </div>
    </form>
</body>
</html>
