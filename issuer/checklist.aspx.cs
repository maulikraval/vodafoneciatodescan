﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using business;
using dataaccesslayer2;
using System.Net.Mail;
using System.Net;
using System.IO;
using MVdataaccesslayer;
using System.Drawing;

public partial class purpose : System.Web.UI.Page
{
    DataTable dt;
    vodabal ba;
    mydataaccess2 da;
    int cat;
    string cat1;

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        try
        {
            if (Session["role"].ToString() != "9")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        txtptw.Text = Session["ptw"].ToString();
        cat = Convert.ToInt32(Session["cat"].ToString());
        if (!IsPostBack)
        {
            if (cat == 1)
            {
                Image1.ImageUrl = "../image002.png";
            }
            else if (cat == 2)
            {
                Image1.ImageUrl = "../image001.png";
            }
            else if (cat == 3)
            {
                Image1.ImageUrl = "../image003.png";
            }
            else if (cat == 4)
            {
                Image1.ImageUrl = "../image004.png";
            }
        }
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CreateDynamicTable();

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess2();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess2();

            da.update_user_master_status(r);

            if (Session["imei"] == null)
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/login/Default.aspx", false);
            }
            else
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

                Response.Redirect("~/issuer/ftologout.aspx", false);
            }
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    private void CreateDynamicTable()
    {
        txtptw.Text = SpacialCharRemove.XSS_Remove(Session["ptw"].ToString());
        cat = Convert.ToInt32(Session["cat"].ToString());
        if (cat == 1)
        { cat = 1; }
        if (cat == 2)
        { cat = 2; }
        if (cat == 3)
        { cat = 3; }
        PlaceHolder1.Controls.Clear();
        Table tbl = new Table();

        PlaceHolder1.Controls.Add(tbl);
        da = new mydataaccess2();
        ba = new vodabal();
        dt = new DataTable();
        dt = da.ptw_select_checklist_issuer(txtptw.Text, cat);

        int count = dt.Rows.Count;
        if (count == 0)
        {
            Label2.Text = "There is no filled Risk assesment for this PTW..";
        }
        else
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TableRow tr = new TableRow();
                TableCell tc = new TableCell();
                Label lb = new Label();
                //lb.CssClass = "lb";
                lb.Text = (i + 1) + ") " + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString());
                lb.Font.Name = "calibri";
                lb.ID = "lb" + i;
                tc.Controls.Add(lb);
                tr.Cells.Add(tc);

                TableRow tr1 = new TableRow();
                TableCell tc1 = new TableCell();
                RadioButtonList rd1 = new RadioButtonList();
                if (cat == 5)
                {
                    rd1.Items.Add("Yes");
                    rd1.Items.Add("No");
                    rd1.Items.Add("NA");
                    //   rd1.Items.Add("NA");
                }
                else
                {
                    rd1.Items.Add("Yes");
                    rd1.Items.Add("No");
                    rd1.Items.Add("NA");
                    //  rd1.Items.Add("NA");
                }
                // rd1.Items.Add("NA");
                rd1.Font.Name = "calibri";
                rd1.ID = "rd" + i;
                rd1.Text = SpacialCharRemove.XSS_Remove(dt.Rows[i][1].ToString());

                rd1.RepeatDirection = RepeatDirection.Horizontal;
                //tc1.BorderWidth = 1;
                rd1.Style["width"] = "200px";
                rd1.Height = Unit.Pixel(0);
                rd1.Enabled = false;


                TableRow tr2 = new TableRow();
                TableCell tc2 = new TableCell();
                TextBox txt1 = new TextBox();
                txt1.ID = "txt" + i;
                txt1.Font.Name = "calibri";
                txt1.Text = SpacialCharRemove.XSS_Remove(dt.Rows[i][2].ToString());
                txt1.Enabled = false;
                //txt1.ID = "details";
                tc2.Controls.Add(txt1);
                tr2.Cells.Add(tc2);



                TableRow tr3 = new TableRow();
                TableCell tc3 = new TableCell();
                Label lb11 = new Label();

                lb11.Text = "<br />";


                tc3.Controls.Add(lb11);
                tr3.Cells.Add(tc3);



                tc1.Controls.Add(rd1);
                tr1.Cells.Add(tc1);

                tbl.Rows.Add(tr);
                tbl.Rows.Add(tr1);
                tbl.Rows.Add(tr2);

                tbl.Rows.Add(tr3);
                PlaceHolder1.Controls.Add(tbl);

                Session["tbDevices"] = tbl;
            }
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        Response.Redirect("view_issuer_by_receiver.aspx");
    }
}
