﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sitecheck.aspx.cs" Inherits="sitecheck" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%">
            <tr>
                <td width="5%">
                </td>
                <td width="45%" colspan="2" style="width: 90%; text-align: center">
                    <asp:Label ID="Label2" runat="server" CssClass="lblstly" Text="Upload Site Master"></asp:Label>
                </td>
                <td width="5%">
                </td>
            </tr>
            <tr>
                <td width="5%" style="height: 26px">
                </td>
                <td width="45%" colspan="2" style="width: 90%; text-align: center; height: 26px;">
                    <hr />
                </td>
                <td width="5%" style="height: 26px">
                </td>
            </tr>
            <tr>
                <td width="5%">
                </td>
                <td width="45%" style="text-align: right">
                    <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Select File To Upload:"
                        CssClass="lblall"></asp:Label>
                </td>
                <td width="45%" style="text-align: left">
                    <asp:FileUpload ID="FileUpload1" runat="server" Font-Size="Medium" CssClass="genall" />
                </td>
                <td width="5%">
                </td>
            </tr>
            <tr>
                <td width="5%">
                </td>
                <td width="45%">
                </td>
                <td width="45%">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please select valid .xls file"></asp:CustomValidator>
                </td>
                <td width="5%">
                </td>
            </tr>
            <tr>
                <td width="5%">
                </td>
                <td width="45%" colspan="2" style="width: 90%; text-align: center">
                    <asp:Button ID="Button1" runat="server" Font-Size="Medium" Height="30px" OnClick="Button1_Click"
                        Text="Upload" Width="100px" />
                </td>
                <td width="5%">
                </td>
            </tr>
            <tr>
                <td width="5%">
                </td>
                <td width="45%">
                </td>
                <td width="45%">
                    <asp:Label ID="lblresult" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Large"
                        Font-Underline="False" ForeColor="Red" Visible="False"></asp:Label>
                </td>
                <td width="5%">
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: center;" colspan="2">
                    <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                        Width="100%">
                        <RowStyle BackColor="White" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
