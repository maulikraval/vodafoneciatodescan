﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using sqlconnection;
using mybusiness;

public partial class sessionerrorpage : System.Web.UI.Page
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

        string strUserAgent = Request.UserAgent.ToString().ToLower();
        if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iphone") ||
strUserAgent.Contains("blackberry") || strUserAgent.Contains("mobile") ||
strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera mini") ||
strUserAgent.Contains("palm"))
        {
            Response.Redirect("~/issuer/ftologout.aspx");
        }
        else
        {
            
        }

        if (!IsPostBack)
        {
        //    if (Request.QueryString.Count != 0)
            try
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
               
                string cook = Request.Cookies["username"].Value;

                da = new mydataaccess1();
                string user = da.select_user_cookie(cook);

                string r = user;
                da = new mydataaccess1();
                da.update_user_master_status(r);

                Response.Cookies["username"].Expires = DateTime.Now;
            }
            catch
            { 
            
            }

        }

        
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/login/Default.aspx");
    }
}
