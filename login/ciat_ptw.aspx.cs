﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using sqlconnection;
using System.Data.OleDb;
using dataaccesslayer;
using business;
using System.Net.Mail;
using System.Net;
using dataaccesslayer2;
using System.Security.Cryptography;
using System.Text;
using System.Web.SessionState;

public partial class login_ciat_ptw : System.Web.UI.Page
{
    vodabal ba;
    mydataaccess1 da;
    mydataaccess2 da1;

    DataTable dt;
    String username = null;
    String password = null;
    String imei = null;

    string user = "";
    string cook = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
           

            user = Session["user"].ToString();

            hidusername.Value = user;
            hidimei.Value = Session["imei"].ToString();
            hiduniqcode.Value = Session["uniqcode"].ToString();
            hidmobile_type.Value = Session["phone_type"].ToString();

            if (user == Session["username"].ToString())
            {
                da = new mydataaccess1();
                int a = da.ptw_select_role1(user);


                da = new mydataaccess1();
                int b = da.ptw_select_role(user);

                if (a == 0)
                {
                    Session["role"] = b;
                }
                else
                {
                    Session["role"] = a;
                }

            }
            else
            {
                da = new mydataaccess1();
                int a = da.ptw_select_role(user);
                Session["role"] = a;
            }
            //if (user.Length >= 20)
            //{
            //    int count = 0;
            //    int count1 = 0;
            //    int count2 = 0;
            //    da = new mydataaccess1();
            //    dt = new DataTable();
            //    string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            //    user = sp_user_d;
            //    Session["user"] = sp_user_d;
            //}
        }
        catch
        {
            //Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        //else
        //{
        //    da = new mydataaccess1();
        //    dt = new DataTable();
        //    string sp_user_d = da.select_user_cookie(Session["user"].ToString());
        //    Session["user"] = username;
        //}
    }
    //public string GetRandomString(int seed)
    //{
    //    //use the following string to control your set of alphabetic characters to choose from
    //    //for example, you could include uppercase too
    //    const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ123456789";

    //    // Random is not truly random, 
    //    // so we try to encourage better randomness by always changing the seed value
    //    Random rnd = new Random((seed + DateTime.Now.Millisecond));

    //    // basic 5 digit random number
    //    string result = rnd.Next(10000, 99999999).ToString();

    //    // single random character in ascii range a-z
    //    string alphaChar = alphabet.Substring(rnd.Next(0, alphabet.Length - 1), 1);

    //    // random position to put the alpha character
    //    int replacementIndex = rnd.Next(0, (result.Length - 1));
    //    result = result.Remove(replacementIndex, 1).Insert(replacementIndex, alphaChar);

    //    return result;
    //}

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + Session["flag"].ToString()+ "');</script>");
            if (Session["role"].ToString() == "2" || Session["role"].ToString() == "10")
            {

                da = new mydataaccess1();
                ba = new vodabal();
                ba.User = user;
                ba.Cookie = cook;
                //  da.update_user_master_status_true(ba);

                //Session["user"] = cook;
                Session["user"] = Session["username"].ToString();
                HttpCookie myhttpcookie = new HttpCookie("username", cook);


                Response.Cookies["username"].Value = cook;
                Response.Cookies["username"].Expires = DateTime.Now.AddHours(3);

                Session["role"] = "2";

                da = new mydataaccess1();
                string user1 = user;
                int s = 1;
                da.update_user_count(user1, s);
                Response.Redirect("~/AllPage/pm_Home.aspx", false);
                //Response.Redirect("~/PM/Home.aspx", false);
            }
            if (Session["role"].ToString() == "3")
            {
                Session["project"] = "ciat";

                //if (Session["flag"].ToString() == "4")
                //{                   
                //    Session["project"] = "both";
                //}       
               // Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                Response.Redirect("~/AllPage/circleadmin_Home.aspx", false);
               // Response.Redirect("~/circle_admin/Home.aspx", false);
            }
            if (Session["role"].ToString() == "4")
            {
                if (Session["flag"].ToString() == "4")
                {

                    Session["project"] = "ciat";
                }
                Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                //Response.Redirect("~/admin/Default3.aspx", false);
            }

            if (Session["role"].ToString() == "1")
            {
                try
                {
                    if ("android" == Session["phone_type"].ToString())
                    {


                        //Session["flag"] = "1";
                        //Session["role"] = dt.Rows[0][2].ToString();
                        //Session["user"] = sp_user_d;
                        //Session["username"] = sp_user_d;
                        Response.Redirect("~/FTO_Android/viewsite.aspx", false);

                    }



                    else if ("BB" == Session["phone_type"].ToString())
                    {


                        //Session["flag"] = "1";
                        //Session["role"] = dt.Rows[0][2].ToString();
                        //Session["user"] = sp_user_d;
                        //Session["username"] = sp_user_d;
                        Response.Redirect("~/FTO_BlackBerry/viewsite.aspx", false);

                    }

                    else
                    {



                        //Session["flag"] = "1";
                        //Session["role"] = dt.Rows[0][2].ToString();
                        //Session["user"] = sp_user_d;
                        //Session["username"] = sp_user_d;
                        Response.Redirect("~/FTO/viewsite.aspx", false);

                    }
                }
                catch (Exception ex)
                { }
            }
        }
        catch
        {
            // Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            if (Session["role_ptw"].ToString() == "3"  )
            {
//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + Session["role"].ToString()+ "');</script>");
                Session["role"] = "3";

//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + Session["role_ptw"].ToString()+ "');</script>");
                Session["project"] = "ptw";

                //if (Session["flag"].ToString() == "4")
                //{
                //    Session["project"] = "both";
                //}
                 Response.Redirect("~/AllPage/circleadmin_Home.aspx", false);
               // Response.Redirect("~/circle_admin/Home.aspx", false);
            }

           
           
            if (Session["role_ptw"].ToString() == "4")
            {
                Session["role"] = "4";
                if (Session["flag"].ToString() == "3")
                {
                    Session["project"] = "ptw";
                }
                Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                //Response.Redirect("~/admin/Default3.aspx", false);
            }
            if (Session["role_ptw"].ToString() == "9")
            {
                Session["role"] = "9";
                Response.Redirect("~/issuer/process_changepass.aspx", false);
            }
            if (Session["role_ptw"].ToString() == "8")
            {
                Session["role"] = "8";
                Response.Redirect("~/receiver/process_changepass.aspx", false);
            }
            if (Session["role_ptw"].ToString() == "10")
            {
                Session["role"] = "10";
                da = new mydataaccess1();
                dt = new DataTable();
                string sp_user_d = da.select_user_cookie(Session["user"].ToString());
                user = sp_user_d;
                Session["user"] = sp_user_d;
                Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
            }
        }
        catch
        {
            //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
}
