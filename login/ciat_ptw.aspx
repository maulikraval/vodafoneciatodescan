﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ciat_ptw.aspx.cs" Inherits="login_ciat_ptw" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
--%>
    <link rel='stylesheet' href="../styles.css" type='text/css' media='screen' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
    <%--    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;" />
--%>
    <link rel="icon" href="#">
    <title>VIL HSW</title>

    <script src="../android_mobile/js/jquery-1.9.1.min.js" type="text/javascript"></script>
  <script src="cordova-2.4.0.js" type="text/javascript"></script>

    <%--<script src="../android_mobile/js/cordova-1.7.0.js" type="text/javascript"></script>--%>
  
    <script type="text/javascript">
        var username = [];
        var imeino = [];
        var uniqcode;
        var mobile_type;

        $(document).ready(function () {


            //alert('Hello');
            $("#ImageButton1").click(function () {
                try {
                    username = $("#hidusername").val();
                    imeino = $("#hidimei").val();
                    uniqcode = $("#hiduniqcode").val();
                    mobile_type = $("#hidmobile_type").val();

                    //alert(username);
                    //alert(imeino);
                    //alert(uniqcode);
                    //alert(mobile_type);
                    if (mobile_type == "Android") {

                        //alert("local URL");
                        //navigator.app.loadUrl("file:///android_asset/www/index.html?qury1="+qury1+"&qury2="+qury3+"&siteid="+siteid12+"&stepid="+step+"&inspection_counter="+inspectcount+"&check_type="+check_type+"&ques_id="+ques_id);
                        navigator.app.loadUrl("file:///android_asset/www/viewsite.html?username=" + username + "&imeino=" + imeino + "&u_c=" + uniqcode);
                        //window.location.href="file:///android_asset/www/viewsite.html?username=" + username + "&imeino=" + imeino + "&u_c=" + uniqcode;
                        //alert("data");
                        return false;

                    }
                    else {
                        //alert("else");
                        return true;
                    }
                } catch (e) { alert(e); }
            });

        });


        //alert(username);
        //     alert(imeino);


    </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class='pg'>
        <div class='head'>
            <table width="100%">
                <tr>
                    <td style="width: 5%; text-align: left">
                       <%-- <img alt="" src="../HTML/images/2.png" style="height: 40px; width: 42px; margin-right: 0px;" />--%>
                    </td>
                      <td style="width: 3%; text-align: left">
                        
                    </td>
                    <td style="width: 72%; text-align: left; margin-top: 0px; color: #FFFFFF;background-color:#FFA500;">
                        &nbsp;&nbsp;&nbsp;Process Selection
                    </td>
                    <td style="width: 20%; text-align: right; margin-top: 0px; color: #000000;background-color:#FFA500">
                        <a id="A1" href="Default.aspx" runat="server"></a>
                    </td>
                </tr>
            </table>
        </div>
        <div class='pg-main' style="margin-top: 29px">
            <center>
                <asp:Label ID="lblselect" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="18px"
                    Text="Select Process" Font-Overline="False" Font-Underline="True"></asp:Label>
            </center>
            <hr />
            <br />
            <center>
                <asp:Panel ID="pnlcreate" runat="server" CssClass="modalPopup">
                    <table width="100%">
                        <tr>
                            <td style="width: 50%; text-align: right; padding-right: 2%">
                                <asp:ImageButton ID="ImageButton1" runat="server" Height="75px" ImageUrl="~/login/ciat.png"
                                    Width="70px" OnClick="ImageButton1_Click" />
                            </td>
                            <td style="width: 50%; text-align: left; padding-left: 2%; padding-top: 1%">
                                &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" Height="80px" ImageUrl="~/login/image002.png"
                                    Width="72px" OnClick="ImageButton2_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </center>
            <input type="hidden" id="hidusername" runat="server" />
            <input type="hidden" id="hidimei" runat="server" />
            <input type="hidden" id="hiduniqcode" runat="server" />
            <input type="hidden" id="hidmobile_type" runat="server" />
        </div>
        <div class='foot' style="margin-top: 50px">
           <%-- <p>
                &copy; </p>--%>
        </div>
    </div>
    </form>
</body>
</html>
