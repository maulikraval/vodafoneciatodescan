﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using sqlconnection;
using System.Data.OleDb;
using dataaccesslayer;
using mybusiness;
using System.Net.Mail;
using System.Net;
using System.Web.SessionState;

public partial class login_Default3 : System.Web.UI.Page
{

    myvodav2 ba;
    mydataaccess1 da;
    String username = null;
    String password = null;
    String imei = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {

                //Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-30); //Delete the cookie
                //Session.Remove("ASP.NET_SessionId");
                //Session.Abandon(); // Session Expire but cookie do exist
                //if (Request.Cookies["ASP.NET_SessionId"] != null)
                //{
                //    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                //    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                //}

                //if (Request.Cookies["AuthToken"] != null)
                //{
                //    Response.Cookies["AuthToken"].Value = string.Empty;
                //    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                //}

                //if (Request.Cookies["TriedTohack"] != null)
                //{
                //    Response.Cookies["TriedTohack"].Value = string.Empty;
                //    Response.Cookies["TriedTohack"].Expires = DateTime.Now.AddMonths(-20);
                //}
                //Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-30); //Delete the cookie
                //Session.Clear();
                //Session.Abandon();
                //Session.RemoveAll();
            }
            if (Session["username"] != null && Session["AuthToken"] != null && Request.Cookies["AuthToken"] != null)
            {
                if (!Session["AuthToken"].ToString().Equals(
                           Request.Cookies["AuthToken"].Value))
                {
                    // redirect to the login page in real application
                    //lblerror.Text = "You are not logged in.";
                    // lblerror.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    // lblerror.Text = "Congratulations !, you are logged in.";
                    // lblerror.ForeColor = System.Drawing.Color.Green;
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
			//Response.Cookies["ASP.NET_SessionId"]?.Secure = true;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
			//Response.Cookies["AuthToken"]?.Secure = true;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }


                    // btnLogout.Visible = true;

                    if (Session["dummy"] != null)
                    {
                        try
                        {


                            String str;
                            // split for decodation username.
                            string[] sp_user = Session["user"].ToString().Split('?');
                            //string sp_user_d = decode(sp_user[1].ToString());
                            string sp_user_d = Session["user"].ToString();
                            da = new mydataaccess1();
                            DataTable dt = new DataTable();
                            dt = da.select_user_password(sp_user_d);
                            string[] sp_imei = Session["imei"].ToString().Split('?');
                            //string sp_imei_d = decode(sp_imei[1].ToString());
                            string sp_imei_d = Session["imei"].ToString();


                            if (dt.Rows.Count > 0)
                            {
                                if (sp_imei_d == dt.Rows[0][7].ToString() & (dt.Rows[0][2].ToString() == "1" || dt.Rows[0][3] == "8"))
                                {
                                    Session.Timeout = 120;

                                    // split for decodation password.
                                    string[] sp_pass = Session["password"].ToString().Split('?');
                                    //string sp_pass_d = decode(sp_pass[1].ToString());
                                    string sp_pass_d = Session["password"].ToString();
                                    // split for decodation imei.

                                    da = new mydataaccess1();
                                    ba = new myvodav2();
                                    int flag = da.check_imei_count(sp_imei_d);

                                    if (flag != 0)
                                    {
                                        //if (dt.Rows[0][5].ToString() == "1" && (!sp_imei_d.Contains('.'))) -- BB is enabled for CIAT
                                        if (dt.Rows[0][5].ToString() == "1")
                                        {
                                            try
                                            {
                                                if ("android" == Session["phone_type"].ToString())
                                                {

                                                    if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "1")
                                                    {
                                                        Session["flag"] = "1";
                                                        Session["role"] = dt.Rows[0][2].ToString();
                                                        Session["user"] = sp_user_d;
                                                        Session["username"] = sp_user_d;
                                                        Response.Redirect("~/FTO_Android/viewsite.aspx", false);
                                                    }
                                                }



                                                else if ("BB" == Session["phone_type"].ToString())
                                                {
                                                    if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "1")
                                                    {


                                                        Session["flag"] = "1";
                                                        Session["role"] = dt.Rows[0][2].ToString();
                                                        Session["user"] = sp_user_d;
                                                        Session["username"] = sp_user_d;
                                                        Response.Redirect("~/FTO_BlackBerry/viewsite.aspx", false);
                                                    }
                                                }

                                                else
                                                {
                                                    if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "1")
                                                    {


                                                        Session["flag"] = "1";
                                                        Session["role"] = dt.Rows[0][2].ToString();
                                                        Session["user"] = sp_user_d;
                                                        Session["username"] = sp_user_d;
                                                        Response.Redirect("~/FTO/viewsite.aspx", false);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            { }


                                        }
                                        if (dt.Rows[0][5].ToString() == "2")
                                        {
                                            if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "8")
                                            {
                                                Session["flag"] = "2";
                                                Session["username"] = dt.Rows[0][0].ToString();
                                                Session["role"] = dt.Rows[0][3].ToString();
                                                Response.Redirect("~/receiver/process_changepass.aspx", false);
                                            }

                                            if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "9")
                                            {
                                                Session["flag"] = "2";
                                                Session["user"] = dt.Rows[0][0].ToString();
                                                Session["username"] = dt.Rows[0][0].ToString();
                                                Session["role"] = dt.Rows[0][3].ToString();
                                                Response.Redirect("~/issuer/process_changepass.aspx", false);
                                            }
                                        }
                                        if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][5].ToString() == "3")
                                        {
                                            // if (!sp_imei_d.Contains('.'))
                                            {
                                                Session["flag"] = "3";
                                                Session["username"] = dt.Rows[0][0].ToString();
                                                Session["user"] = dt.Rows[0][0].ToString();
                                                Session["role"] = dt.Rows[0][2].ToString();
                                                Session["role_ptw"] = dt.Rows[0][3].ToString();
                                                Response.Redirect("ciat_ptw.aspx", false);
                                            }
                                            /*   else
                                               {

                                                   if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "8")
                                                   {
                                                       Session["flag"] = "2";
                                                       Session["username"] = dt.Rows[0][0].ToString();
                                                       Session["role"] = dt.Rows[0][3].ToString();
                                                       Response.Redirect("~/receiver/process_changepass.aspx", false);
                                                   }

                                                   if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "9")
                                                   {
                                                       Session["flag"] = "2";
                                                       Session["user"] = dt.Rows[0][0].ToString();
                                                       Session["username"] = dt.Rows[0][0].ToString();
                                                       Session["role"] = dt.Rows[0][3].ToString();
                                                       Response.Redirect("~/issuer/process_changepass.aspx", false);
                                                   }
                                               }
                                             */
                                        }
                                        else
                                        {
                                            // Label3.Text = "Check Username or Password again";
                                        }

                                    }
                                    else
                                    {
                                        // imei does not exist.
                                    }
                                }

                                else
                                {
                                    Session.Timeout = 120;

                                    // split for decodation password.
                                    string[] sp_pass = Session["password"].ToString().Split('?');
                                    //string sp_pass_d = decode(sp_pass[1].ToString());
                                    string sp_pass_d = Session["password"].ToString();
                                    // split for decodation imei.

                                    da = new mydataaccess1();
                                    ba = new myvodav2();
                                    int flag = da.check_imei_count(sp_imei_d);

                                    if (flag != 0)
                                    {
                                        //if (dt.Rows[0][5].ToString() == "1" && (!sp_imei_d.Contains('.'))) -- BB is enabled for CIAT
                                        if (dt.Rows[0][5].ToString() == "1")
                                        {
                                            try
                                            {
                                                if ("android" == Session["phone_type"].ToString())
                                                {

                                                    if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "1")
                                                    {
                                                        Session["flag"] = "1";
                                                        Session["role"] = dt.Rows[0][2].ToString();
                                                        Session["user"] = sp_user_d;
                                                        Session["username"] = sp_user_d;
                                                        Response.Redirect("~/FTO_Android/viewsite.aspx", false);
                                                    }
                                                }



                                                else if ("BB" == Session["phone_type"].ToString())
                                                {
                                                    if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "1")
                                                    {


                                                        Session["flag"] = "1";
                                                        Session["role"] = dt.Rows[0][2].ToString();
                                                        Session["user"] = sp_user_d;
                                                        Session["username"] = sp_user_d;
                                                        Response.Redirect("~/FTO_BlackBerry/viewsite.aspx", false);
                                                    }
                                                }

                                                else
                                                {
                                                    if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "1")
                                                    {


                                                        Session["flag"] = "1";
                                                        Session["role"] = dt.Rows[0][2].ToString();
                                                        Session["user"] = sp_user_d;
                                                        Session["username"] = sp_user_d;
                                                        Response.Redirect("~/FTO/viewsite.aspx", false);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            { }


                                        }
                                        if (dt.Rows[0][5].ToString() == "2")
                                        {
                                            if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "8")
                                            {
                                                Session["flag"] = "2";
                                                Session["username"] = dt.Rows[0][0].ToString();
                                                Session["role"] = dt.Rows[0][3].ToString();
                                                Response.Redirect("~/receiver/process_changepass.aspx", false);
                                            }

                                            if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "9")
                                            {
                                                Session["flag"] = "2";
                                                Session["user"] = dt.Rows[0][0].ToString();
                                                Session["username"] = dt.Rows[0][0].ToString();
                                                Session["role"] = dt.Rows[0][3].ToString();
                                                Response.Redirect("~/issuer/process_changepass.aspx", false);
                                            }
                                        }
                                        if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][5].ToString() == "3")
                                        {
                                            // if (!sp_imei_d.Contains('.'))
                                            {
                                                Session["flag"] = "3";
                                                Session["username"] = dt.Rows[0][0].ToString();
                                                Session["user"] = dt.Rows[0][0].ToString();
                                                Session["role"] = dt.Rows[0][2].ToString();
                                                Session["role_ptw"] = dt.Rows[0][3].ToString();
                                                Response.Redirect("ciat_ptw.aspx", false);
                                            }
                                            /*   else
                                               {

                                                   if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "8")
                                                   {
                                                       Session["flag"] = "2";
                                                       Session["username"] = dt.Rows[0][0].ToString();
                                                       Session["role"] = dt.Rows[0][3].ToString();
                                                       Response.Redirect("~/receiver/process_changepass.aspx", false);
                                                   }

                                                   if (sp_user_d == dt.Rows[0][0].ToString() && sp_pass_d == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "9")
                                                   {
                                                       Session["flag"] = "2";
                                                       Session["user"] = dt.Rows[0][0].ToString();
                                                       Session["username"] = dt.Rows[0][0].ToString();
                                                       Session["role"] = dt.Rows[0][3].ToString();
                                                       Response.Redirect("~/issuer/process_changepass.aspx", false);
                                                   }
                                               }
                                             */
                                        }
                                        else
                                        {
                                            // Label3.Text = "Check Username or Password again";
                                        }

                                    }
                                    else
                                    {
                                        // imei does not exist.
                                    }
                                }
                            }
                        }

                        catch (Exception ee)
                        {

                        }
                    }
                }
            }
            else
            {
                lblerror.Text = "You are not logged in.";
                lblerror.ForeColor = System.Drawing.Color.White;
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnkNewUser_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Final/new_user.aspx");
    }

    protected void btnLogin_Click1(object sender, EventArgs e)
   {
        try
        {
            //Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-30); //Delete the cookie
            //Session.Remove("ASP.NET_SessionId");
            //Session.Abandon(); // Session Expire but cookie do exist

            lblerror.Text = "";
            string cook = "";
       //    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('hi');</script>");

            HttpCookie myhttpcook;

            ba = new myvodav2();
            da = new mydataaccess1();
            DataTable dt = new DataTable();
            dt = da.select_user_password(txtUser.Text);

      //    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('hi');</script>");
            if (dt.Rows.Count > 0)
            {

 
                /*
                if (dt.Rows[0][2].ToString() == "1" || dt.Rows[0][3].ToString() == "8")
                {
                    string[] sp_imei = Session["imei"].ToString().Split('?');
                    //string sp_imei_d = decode(sp_imei[1].ToString());
                    string sp_imei_d = Session["imei"].ToString();
                    if (sp_imei_d == dt.Rows[0][7].ToString())
                    {
                        if (dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][6].ToString()) <= 5)
                        {
                            Session.Timeout = 10;
//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('hello');</script>");
                            if (dt.Rows[0][4].ToString() == "False")
                            {
                                da = new mydataaccess1();
                                ba = new myvodav2();
                                {
                                    HttpSessionState ss = HttpContext.Current.Session;
                                    cook = ss.SessionID.ToString() + DateTime.Now.Ticks.ToString();

                                    // Only CIAT Users
                                    if (dt.Rows[0][5].ToString() == "1")
                                    {
                                        Session["project"] = "ciat";
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "2")
                                        {

                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;

                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][2].ToString();
                                            Session["flag"] = "1";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("../PM/Home.aspx", false);
                                        }
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "3")
                                        {


                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][2].ToString();
                                            Session["flag"] = "1";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/circle_admin/Home.aspx", false);
                                        }
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "4")
                                        {
//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('admin');</script>");
                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][2].ToString();
                                            Session["flag"] = "1";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/admin/Default3.aspx", false);
                                        }
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "5")
                                        {


                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][2].ToString();
                                            Session["flag"] = "1";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);

                                            Response.Redirect("../PM/Default.aspx", false);
                                        }
                                        else
                                        {
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 0;
                                            da.update_user_count(user, s);
                                            lblerror.Text = "Invalid Username & Password.";
                                        }
                                    }
                                    // Only PTW Users
                                    if (dt.Rows[0][5].ToString() == "2")
                                    {
                                        Session["project"] = "ptw";
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "3")
                                        {
                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][3].ToString();
                                            Session["flag"] = "3";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/circle_admin/Home.aspx", false);
                                        }
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "4")
                                        {

                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][3].ToString();
                                            Session["flag"] = "4";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/admin/Default3.aspx", false);
                                        }
                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "9")
                                        {

                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = txtUser.Text;
                                            Session["username"] = txtUser.Text;

                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][3].ToString();
                                            Session["flag"] = "9";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/issuer/process_changepass.aspx", false);
                                        }


                                        //Maunish- New role for PDF download -Service Partner

                                        if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "10")
                                        {

                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = txtUser.Text;
                                            Session["username"] = txtUser.Text;

                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][3].ToString();
                                            Session["flag"] = "10";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
                                        }
                                        //Maunish -- change END


                                        else
                                        {
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 0;
                                            da.update_user_count(user, s);
                                            lblerror.Text = "Invalid Username & Password.";
                                        }
                                    }

                                    // CIAT & PTW Users
                                    if ((txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString()) && (dt.Rows[0][5].ToString() == "3") && (dt.Rows[0][3].ToString() != "8" || dt.Rows[0][2].ToString() != "1"))
                                    {
                                        if (dt.Rows[0][3].ToString() == "4" || dt.Rows[0][2].ToString() == "4")
                                        {
                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][3].ToString();
                                            Session["flag"] = "4";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/admin/Default3.aspx", false);
                                        }
                                        else if (dt.Rows[0][3].ToString() == "3" && dt.Rows[0][2].ToString() == "3")
                                        {
                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = cook;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = "3";
                                            Session["role_ptw"] = "3";
                                            Session["flag"] = "4";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("ciat_ptw.aspx", false);
                                        }
 

                                             //Maunish- New role for PDF download -Service Partner

                                        else if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "10")
                                        {

                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = txtUser.Text;
                                            Session["username"] = txtUser.Text;

                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            Session["role"] = dt.Rows[0][3].ToString();
                                            Session["flag"] = "10";
                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);
                                            Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
                                        }
                                        //Maunish -- change END

                                        else
                                        {
                                            da = new mydataaccess1();
                                            ba = new myvodav2();
                                            ba.User = txtUser.Text;
                                            ba.Cookie = cook;
                                            da.update_user_master_status_true(ba);

                                            Session["user"] = cook;
                                            Session["username"] = txtUser.Text;
                                            myhttpcook = new HttpCookie("username", cook);
                                            myhttpcook.HttpOnly = true;
                                            myhttpcook.Expires = DateTime.Now.AddHours(3);

                                            da = new mydataaccess1();
                                            string user = txtUser.Text;
                                            int s = 1;
                                            da.update_user_count(user, s);

                                            Session["flag"] = "4";

						
                                            Session["role"] = dt.Rows[0][2].ToString();
                                            Session["role_ptw"] = dt.Rows[0][3].ToString();
                                            Response.Redirect("ciat_ptw.aspx", false);
                                        }
                                    }
                                }
                                // End
                            }

                            else
                            {
                                lblerror.Text = "Session not expired..";
                            }

                        }

                        else
                        {

                            lblerror.Text = "Your Account Has Been Blocked.Please Contact Admin.";
                        }
                    }
                }
                else*/
                {
                    if (dt.Rows.Count > 0 && dt.Rows[0][10].ToString() == "0")
                    {
                        if (dt.Rows.Count > 0 && Convert.ToInt32(dt.Rows[0][6].ToString()) <= 5)
                        {
                            if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString())
                            {
                                if (dt.Rows[0][9].ToString() == "0")
                                {
                                    //Session.Timeout = 10;
                                    // createa a new GUID and save into the session
                                    string guid = Guid.NewGuid().ToString();
                                    Session["AuthToken"] = guid;
                                    // now create a new cookie with this guid value
                                    Response.Cookies.Add(new HttpCookie("AuthToken", guid));

                                    if (dt.Rows[0][4].ToString() == "False")
                                    {
                                        // da = new mydataaccess1();
                                        //   ba = new myvodav2();
                                        {
                                            HttpSessionState ss = HttpContext.Current.Session;
                                            cook = ss.SessionID.ToString() + DateTime.Now.Ticks.ToString();

                                            // Only CIAT Users
                                            if (dt.Rows[0][5].ToString() == "1")
                                            {
                                                Session["project"] = "ciat";
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "2")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;

                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["flag"] = "1";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("../AllPage/pm_Home.aspx", false);
                                                    //Response.Redirect("../PM/Home.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "11")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["flag"] = "1";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("../AllPage/Ex_inspector_Home.aspx", false);
                                                    //Response.Redirect("../External_Inspector/Home.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "3")
                                                {


                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["flag"] = "1";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    //Response.Redirect("~/circle_admin/Home.aspx", false);
                                                    Response.Redirect("~/AllPage/circleadmin_Home.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "4")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["flag"] = "1";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                                                    // Response.Redirect("~/admin/Default3.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "12")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Secure = true;
                                                    myhttpcook.Path = "/PMT/";
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["flag"] = "1";
                                                    Session["User_Auth_Pmt"] = txtUser.Text;
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/AllPage/pmt_Default3.aspx", false);
                                                    // Response.Redirect("~/PMT/Default3.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][2].ToString() == "5")
                                                {


                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;

                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["flag"] = "1";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);

                                                    Response.Redirect("../AllPage/pm_Home.aspx", false);
                                                    //Response.Redirect("../PM/Default.aspx", false);
                                                }
                                                else
                                                {
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 0;
                                                    da.update_user_count(user, s);
                                                    lblerror.Text = "Invalid Username or Password.";
                                                    lblerror.ForeColor = System.Drawing.Color.White;
                                                }
                                            }
                                            // Only PTW Users
                                            if (dt.Rows[0][5].ToString() == "2")
                                            {
                                                Session["project"] = "ptw";
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "3")
                                                {
                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][3].ToString();
                                                    Session["flag"] = "3";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/AllPage/circleadmin_Home.aspx", false);
                                                    //Response.Redirect("~/circle_admin/Home.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "4")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][3].ToString();
                                                    Session["flag"] = "4";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    // Response.Redirect("~/admin/Default3.aspx", false);
                                                    Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                                                }
                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "9")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = txtUser.Text;
                                                    Session["username"] = txtUser.Text;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][3].ToString();
                                                    Session["flag"] = "9";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/issuer/process_changepass.aspx", false);
                                                }


                                                //Maunish- New role for PDF download -Service Partner

                                                if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "10")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = txtUser.Text;
                                                    Session["username"] = txtUser.Text;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][3].ToString();
                                                    Session["flag"] = "10";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
                                                }
                                                //Maunish -- change END


                                                else
                                                {
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 0;
                                                    da.update_user_count(user, s);
                                                    lblerror.Text = "Invalid Username or Password.";
                                                    lblerror.ForeColor = System.Drawing.Color.White;
                                                }
                                            }

                                            // CIAT & PTW Users
                                            if ((txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString()) && (dt.Rows[0][5].ToString() == "3") && (dt.Rows[0][3].ToString() != "8" || dt.Rows[0][2].ToString() != "1"))
                                            {
                                                if (dt.Rows[0][3].ToString() == "4" || dt.Rows[0][2].ToString() == "4")
                                                {
                                                    //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('hello');</script>");
                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][3].ToString();
                                                    Session["flag"] = "4";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                                                    //Response.Redirect("~/admin/Default3.aspx", false);
                                                }
                                                else if (dt.Rows[0][3].ToString() == "10" || dt.Rows[0][2].ToString() == "2")
                                                {
                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);


                                                    Session["role"] = "2";
                                                    Session["role_ptw"] = "10";
                                                    Session["flag"] = "4";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("ciat_ptw.aspx", false);
                                                }
                                                else if (dt.Rows[0][3].ToString() == "3" && dt.Rows[0][2].ToString() == "3")
                                                {
                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = "3";
                                                    Session["role_ptw"] = "3";
                                                    Session["flag"] = "4";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("ciat_ptw.aspx", false);
                                                }
                                                else if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "13" && dt.Rows[0][2].ToString() == "13")
                                                {
                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = cook;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = "13";
                                                    Session["role_ptw"] = "13";
                                                    Session["flag"] = "4";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/AllPage/support_Home.aspx", false);
                                                }

                                                     //Maunish- New role for PDF download -Service Partner

                                                else if (txtUser.Text == dt.Rows[0][0].ToString() && txtPass.Text == dt.Rows[0][1].ToString() && dt.Rows[0][3].ToString() == "10")
                                                {

                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = txtUser.Text;
                                                    Session["username"] = txtUser.Text;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    Session["role"] = dt.Rows[0][3].ToString();
                                                    Session["flag"] = "10";
                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);
                                                    Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
                                                }
                                                //Maunish -- change END

                                                else
                                                {
                                                    da = new mydataaccess1();
                                                    ba = new myvodav2();
                                                    ba.User = txtUser.Text;
                                                    ba.Cookie = cook;
                                                    da.update_user_master_status_true(ba);

                                                    Session["user"] = cook;
                                                    Session["username"] = txtUser.Text;
                                                    Session["um"] = txtUser.Text;
                                                    myhttpcook = new HttpCookie("username", cook);
                                                    myhttpcook.HttpOnly = true;
                                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                                    da = new mydataaccess1();
                                                    string user = txtUser.Text;
                                                    int s = 1;
                                                    da.update_user_count(user, s);

                                                    Session["flag"] = "4";
                                                    Session["role"] = dt.Rows[0][2].ToString();
                                                    Session["role_ptw"] = dt.Rows[0][3].ToString();
                                                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(Session["role"].ToString()) + "');</script>");
                                                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(Session["role_ptw"].ToString()) + "');</script>");

                                                    Response.Redirect("ciat_ptw.aspx", false);
                                                }
                                            }
                                        }
                                        // End
                                    }

                                    else
                                    {
                                        lblerror.Text = "Session not expired..";
                                        lblerror.ForeColor = System.Drawing.Color.White;
                                    }
                                }
                                else
                                {
                                    HttpSessionState ss1 = HttpContext.Current.Session;
                                    cook = ss1.SessionID.ToString() + DateTime.Now.Ticks.ToString();
                                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your password is expired.Please reset it!!!');", true);
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your password is expired.Please reset it!!!');</script>");

                                    da = new mydataaccess1();
                                    ba = new myvodav2();
                                    ba.User = txtUser.Text;
                                    ba.Cookie = cook;
                                    da.update_user_master_status_true_pwdate(ba);

                                    Session["user"] = cook;
                                    Session["username"] = txtUser.Text;
                                    Session["um"] = txtUser.Text;
                                    myhttpcook = new HttpCookie("username", cook);
                                    myhttpcook.HttpOnly = true;
                                    myhttpcook.Expires = DateTime.Now.AddHours(3);

                                    Response.Redirect("../changepassword/change_password.aspx", false);
                                    /* ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your password is expired.Please reset it!!!');window.location ='../changepassword/change_password.aspx';", true);*/
                                    /*ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "setInterval(function(){alert('Your password is expired.Please reset it!!!');window.location ='../changepassword/change_password.aspx', 3000);", true);*/
                                }
                            }
                            else
                            {
                                da = new mydataaccess1();
                                string user = txtUser.Text;
                                int s = 0;
                                da.update_user_count(user, s);
                                lblerror.Text = "Invalid Username or Password.";
                                // lblerror.Text = "You are entering wrong Password.";
                                lblerror.ForeColor = System.Drawing.Color.White;
                            }
                        }
                        else
                        {

                            lblerror.Text = "Your Account Has Been Blocked.Please Contact Admin.";
                            lblerror.ForeColor = System.Drawing.Color.White;
                        }
                    }
                    else
                    {
                        lblerror.Text = "Your Account Has been Deactivated. Please Contact to Support Team";
                        lblerror.ForeColor = System.Drawing.Color.White;
                    }
                }
            }
            else
            {
                //lblerror.Text = "You are entering wrong Username.";
                lblerror.Text = "Invalid Username or Password.";
                lblerror.ForeColor = System.Drawing.Color.White;
            }
        }
        catch (Exception ee)
        { }

    }
    //public string GetRandomString(int seed)
    //{
    //    //use the following string to control your set of alphabetic characters to choose from
    //    //for example, you could include uppercase too
    //    const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ123456789";

    //    // Random is not truly random, 
    //    // so we try to encourage better randomness by always changing the seed value
    //    Random rnd = new Random((seed + DateTime.Now.Millisecond));

    //    // basic 5 digit random number
    //    string result = rnd.Next(10000, 99999999).ToString();

    //    // single random character in ascii range a-z
    //    string alphaChar = alphabet.Substring(rnd.Next(0, alphabet.Length - 1), 1);

    //    // random position to put the alpha character
    //    int replacementIndex = rnd.Next(0, (result.Length - 1));
    //    result = result.Remove(replacementIndex, 1).Insert(replacementIndex, alphaChar);

    //    return result;
    //}
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }

    protected void lnkForgotPass_Click1(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("~/forgotpassword/forgotpassword.aspx", false);
        }
        catch (Exception ee)
        {

        }
    }
    protected void s_Click(object sender, EventArgs e)
    {

    }
}
