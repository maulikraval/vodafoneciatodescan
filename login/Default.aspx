﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="login_Default3" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>Login Page - CIAT</title>
    <link rel="stylesheet" href="fonts/font.css" />
    <link rel="stylesheet" href="css/main.css" />
    <style type="text/css">
        .txtcolor {
            color: Red;
        }

        .container {
            background: url(images/bg5.jpg) no-repeat 0 0;
        }
    </style>

    <script src="../js/crypto.js" type="text/javascript"></script>

    <script src="../js/base64.js" type="text/javascript"></script>

    <script src="../js/jquery-1.9.0.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            // alert('hi');
            $("#btnLogin").click(function () {

                // alert('hi');

                var rpass = $('#txtPass').val();
                var hash = CryptoJS.SHA256(rpass);
                var pass = hash.toString(CryptoJS.enc.Base64);
                $('#txtPass').val(pass);


            });
        });


    </script>

</head>
<body>
    <form id="Form1" runat="server" autocomplete="off">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="container">
            <div class="content clearfix">
                <p class="title">
                    Welcome to CIAT
                </p>
                <p class="subtitle">
                    To start off, tell us your:
                </p>
                <ul class="formfields clearfix">
                    <li class="clearfix">
                        <label for="user_id">
                            User ID</label>
                        <asp:TextBox ID="txtUser" runat="server" Width="100px"></asp:TextBox>

                        <div class="txtcolor">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="None" runat="server"
                                ControlToValidate="txtUser" ErrorMessage="Username Required."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="InvalidChars"
                                InvalidChars="/*+@#$%^&amp;*~!&lt;&gt;:&quot;?|='()" TargetControlID="txtuser">
                            </cc1:FilteredTextBoxExtender>
                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredFieldValidator1">
                            </cc1:ValidatorCalloutExtender>
                        </div>
                    </li>
                    <li class="clearfix">
                        <label for="pass">
                            Password</label>
                        <asp:TextBox ID="txtPass" runat="server" TextMode="Password" Width="100px"  autocomplete="off"  
    onfocus="this.removeAttribute('readonly');"></asp:TextBox>
                        <div class="txtcolor">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="None" ForeColor="White"
                                runat="server" ControlToValidate="txtPass" ErrorMessage="Password Required."></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars"
                                InvalidChars="./*+#$%^&amp;*~!&lt;&gt;:&quot;?|='()" TargetControlID="txtpass">
                            </cc1:FilteredTextBoxExtender>
                            <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RequiredFieldValidator2">
                            </cc1:ValidatorCalloutExtender>
                        </div>
                    </li>
                </ul>
                <asp:Button class="enter" ID="btnLogin" runat="server" Text="Enter" OnClick="btnLogin_Click1" />
                <div class="clearfix">
                </div>
                <asp:Label ID="lblerror" runat="server" ForeColor="White" Font-Names="Calibri" Font-Size="20px"></asp:Label>
                <div class="clearfix">
                </div>
                <asp:Button ID="lnkForgotPass" runat="server" class="enter forgot" Text="Forgot Password?"
                    OnClick="lnkForgotPass_Click1" CausesValidation="False" />
                <div class="clearfix">
                </div>


              <%--  <div class="footer">
                    <p class="vodafone">
                        Vodafone
                    </p>
                    <p class="power">
                        Power to you
                    </p>
                </div>--%>
            </div>
            <table width="33%" style="font-size: 12px;">
                <tr>
                    <td colspan="2"><b>For Quick Support :</b>
                    </td>
                </tr>
              <%--  <tr>
                    <td style="padding: 3px">Version
                    </td>
                    <td>- : 4.30
                    </td>
                </tr>--%>
                <tr>
                    <td style="padding: 3px">E-Mail Us </td>
                    <td>- : CIAT.Helpdesk@skyproductivity.com
                    </td>
                </tr>
                <tr>
                    <td style="padding: 3px">CIAT (9x6) </td>
                    <td>- : 9909924585
                    </td>
                </tr>
                <tr>
                    <td style="padding: 3px">G-PM Passive (9x6) </td>
                    <td>- : 9978791606
                    </td>
                </tr>
                <tr>
                    <td style="padding: 3px">G-PM Active (9x6) </td>
                    <td>- :  9727716004
                    </td>
                </tr>
                <tr>
                    <td style="padding: 3px">PTW (24x7) </td>
                    <td>- : 8141587709
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
