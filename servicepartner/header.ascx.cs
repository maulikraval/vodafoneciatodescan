﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class receiver_header : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int i = 10;
            if (Convert.ToInt32(Session["role"].ToString()) == i)
            {
                lblusername.Text = SpacialCharRemove.XSS_Remove(Session["username"].ToString());
            }
            else
            {
                Session.Abandon();
                Response.End();
            }

            if (Session["flag"].ToString() == "3")
            {
                lnkchangeproject.Visible = true;
            }
            else
            {
                lnkchangeproject.Visible = false;
            }

        }
        catch(Exception ee)
        {
            throw ee;
            //Response.Redirect("ftologout.aspx", false);
        }
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            Response.Redirect("ftologout.aspx", false);
        }
        catch (Exception ee)
        {
        }
    }
}
