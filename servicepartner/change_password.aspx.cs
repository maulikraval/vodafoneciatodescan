﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using mybusiness;
using System.Data;

public partial class change_password : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Label1.Text = "";
            if (!IsPostBack)
            {
                txtuser.Text = Session["username"].ToString();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void txtoldpass_TextChanged(object sender, EventArgs e)
    {

    }
    protected void btncreate_Click(object sender, EventArgs e)
    {
        Label1.Text = "";
        try
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            ba.User = txtuser.Text;
            ba.Password = HiddenField1.Value;
            ba.Val = HiddenField2.Value;
            ba.Dump = txtpass.Text;
            int a = da.password_change_ptw(ba);

            if (a == 5)
            {
                Label1.Text = "Old Password is not correct!!!!!";
            }
            else if (a == 1 || a == 3)
            {
                Label1.Text = "You Have Already Submitted this password before!!!!!";
            }
            else if (a == 2)
            {
                //    Label1.Text = "Your Password Has Been Successfully Changed";
                mod1.Show();
            }
            else
            {
                Label1.Text = "";
            }
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch (Exception ee)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void btnchangpass0_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            string role = sp_user_d;
            string role1 = Session["role"].ToString();
            if (Session["role"].ToString() == "2")
            {
                Response.Redirect("~/AllPage/pm_Home.aspx", false);
                //Response.Redirect("~/PM/Home.aspx", false);
            }
            if (Session["role"].ToString() == "3")
            {
                Response.Redirect("~/circle_admin/Home.aspx", false);
               // Response.Redirect("~/AllPage/circleadmin_Home.aspx", false);
            }
            if (Session["role"].ToString() == "4")
            {
                Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                //Response.Redirect("~/admin/Default3.aspx", false);
            }
            if (Session["role"].ToString() == "9")
            {
                Response.Redirect("~/issuer/view.aspx", false);
            }
            //Maunish service partner PDF
             if (Session["role"].ToString() == "10")
             {
                 Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
             }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnok_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            string role = sp_user_d;
            string role1 = Session["role"].ToString();
            if (Session["role"].ToString() == "2")
            {
                Response.Redirect("~/AllPage/pm_Home.aspx", false);
                //Response.Redirect("~/PM/Home.aspx", false);
            }
            if (Session["role"].ToString() == "3")
            {
                Response.Redirect("~/AllPage/circleadmin_Home.aspx", false);
                //Response.Redirect("~/circle_admin/Home.aspx", false);
            }
            if (Session["role"].ToString() == "4")
            {
                Response.Redirect("~/AllPage/admin_Default3.aspx", false);
                //Response.Redirect("~/admin/Default3.aspx", false);
            }
            if (Session["role"].ToString() == "9")
            {
                Response.Redirect("~/issuer/view.aspx", false);
            }

            //Maunish service partner PDF
            if (Session["role"].ToString() == "10")
            {
                Response.Redirect("~/servicepartner/PDF_Report.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
}
