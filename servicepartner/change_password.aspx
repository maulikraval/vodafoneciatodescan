﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/MasterPage.master" AutoEventWireup="true"
    CodeFile="change_password.aspx.cs" Inherits="change_password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
        //function click(){
        $('input[id$=txtpass]').val('');
        $('input[id$=txtpass0]').val('');
        $('input[id$=txtoldpass]').val('');
        function click1() {
            // alert('hi');
            var rpass = $('input[id$=txtpass]').val();
            var hash = CryptoJS.SHA256(rpass);
            var pass = hash.toString(CryptoJS.enc.Base64);
            $('input[id$=HiddenField1]').val(pass);
            //     alert($('input[id$=HiddenField1]').val());
            // alert(rpass);  
            //alert(pass);
            var rpass1 = $('input[id$=txtoldpass]').val();
            var hash1 = CryptoJS.SHA256(rpass1);
            var pass1 = hash1.toString(CryptoJS.enc.Base64);
            $('input[id$=HiddenField2]').val(pass1);
    
            // $('input[id$=txtrepass]').val(pass1);


        }
     

    </script>
    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--<style type="text/css">
        .style1
        {
            width: 5%;
            height: 23px;
        }
        .style2
        {
            width: 35%;
            height: 23px;
        }
        .style3
        {
            width: 55%;
            height: 23px;
        }
    </style>--%>
  
    <asp:Panel Width="1100px" ID="pan1" runat="server">
        <table width="100%">
   
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="text-align: center;" colspan="2">
                    <asp:Label ID="Label2" runat="server" CssClass="lblstly" Font-Bold="False" 
                        Font-Underline="False" Text="Change Password"></asp:Label>
                </td>
                <td style="width: 5%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    </td>
                <td style="text-align: right;" colspan="2">
                    <hr />
                    </td>
                <td style="width: 5%; ">
                    </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblusername" runat="server" Text="User Name :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtuser" runat="server" BackColor="#FFFFCC" Enabled="False" 
                        Width="150px" CssClass="genall" ></asp:TextBox>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lbloldpass" runat="server" Text="Old Password :" 
                        CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtoldpass" runat="server" Width="150px" 
                        OnTextChanged="txtoldpass_TextChanged" TextMode="Password" 
                        CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="txtoldpass" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                          <asp:HiddenField ID="HiddenField2" runat="server" />
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblpass" runat="server" Text="Password :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtpass" runat="server" TextMode="Password" Width="150px" 
                        CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpass"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                        ControlToValidate="txtpass" Display="None" 
                        
                        ErrorMessage=" It must be between 8 and 10 characters, First two letters must be character and third must be underscore(_),fourth character should be numeric and remaining characters. " SetFocusOnError="True" 
                        ValidationExpression="([a-z0-9_-]+(\.[a-z0-9_-]+)*_[a-zA-Z0-9_]{4,6})$"></asp:RegularExpressionValidator>
                        <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" 
                        TargetControlID="RegularExpressionValidator2">
                    </cc1:ValidatorCalloutExtender>
                     <asp:HiddenField ID="HiddenField1" runat="server" />
                    &nbsp;</td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblpass2" runat="server" Text="Re-Type Password" 
                        CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    
                            <asp:TextBox ID="txtpass0" runat="server"
                                TextMode="Password" Width="150px" CssClass="genall"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ControlToCompare="txtpass" ControlToValidate="txtpass0" 
                                ErrorMessage="Password Not Matched"></asp:CompareValidator>
                        
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    &nbsp;
                </td>
                <td style="width: 55%; text-align: left;">
                    &nbsp;
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    &nbsp;
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:Button ID="btnchangpass" runat="server" OnClick="btncreate_Click" 
                        Text="Change" OnClientClick="click1()" />
                        
                    &nbsp;<asp:Button ID="btnchangpass0" runat="server" OnClick="btnchangpass0_Click" 
                        Text="Cancel" CausesValidation="False" />
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;
                </td>
                <td style="text-align: center; font-family: calibri; font-size: 13px; color: #FF0000;" 
                    class="style2" colspan="2">
                    &nbsp; &nbsp;
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    &nbsp;
                    </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="text-align: right; width: 35%;">
                    &nbsp;
                </td>
                <td style="text-align: left; width: 55%;">
                    <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                    TargetControlID="panel2" runat="server">
                </cc1:RoundedCornersExtender>
    <asp:Panel ID="panel2" runat="server"  Width="500px" Height="100px"
        ForeColor="Black" BackColor="#E4E4E4">
        <table width="100%" style="text-align: center">
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td style="text-align: right; width: 45%;" width="15%">
                    &nbsp;</td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: center; width: 70%; font-size: large; color: #FF0000; font-family: calibri;" 
                    width="35%">
                    &nbsp; Your Password Has Been Changed Successfully.&nbsp; </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td 
                    style="text-align: center; width: 70%; font-size: large; color: #FF0000; font-family: calibri;" 
                    width="35%">
                    &nbsp;</td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td style="text-align: center; width: 70%; font-size: large; color: #FF0000;" 
                    width="35%">
                    <asp:Button ID="btnok" runat="server" CausesValidation="False" 
                        onclick="btnok_Click" Text="Ok" />
                </td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            
        </table>
    </asp:Panel>
    <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="butt1" PopupControlID="panel2">
    </cc1:ModalPopupExtender>
                    </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
