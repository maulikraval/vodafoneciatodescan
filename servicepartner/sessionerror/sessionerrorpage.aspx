﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sessionerrorpage.aspx.cs" Inherits="sessionerrorpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>CIAT | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    
    <link href="../PM/styleinner.css" rel="stylesheet" type="text/css" />
    

    <script src="../js/cufon-yui.js" type="text/javascript"></script>


    <script src="../js/arial.js" type="text/javascript"></script>
    

    <script src="../js/cuf_run.js" type="text/javascript"></script>
    
    
    <link href="../css/menu.css" rel="stylesheet" type="text/css" />


    <script src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/menu.js" type="text/javascript"></script>
 
       <link rel="stylesheet" type="text/css" href="../HTML/style.css">
       
    <link rel="stylesheet" type="text/css" href="../chrometheme/chromestyle4.css" />

    <script type="text/javascript" src="../chromejs/chrome.js"></script>


    <style type="text/css">
        <!
        -- #roll a
        {
            display: inline-block;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        -- > .style1
        {
            width: 100%;
        }
        .style3
        {
            width: 702px;
        }
        .style4
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="main">
        <div class="header">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                  
                      <tr>
                            <td width="6%">
                              <%--  <asp:Image ID="Image1" runat="server" ImageUrl="~/images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style3">
                               <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                &nbsp;</td>
                        </tr>
                    
                    </table>
                </div>
                <div class="clr">
                </div>
                <div id="menu">
                    <ul class="menu">
                 
                 
                    </ul>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
                <%--<div class="sidebar">
                    <asp:ContentPlaceHolder runat="server" ID="sidebar">
                        <div class="gadget">
                            <h2>
                                <span style="text-align: center;">Courses</span></h2>
                            <div class="clr">
                            </div>
                            <ul class="ex_menu">
                                <li><a href="http://www.dreamtemplate.com">DreamTemplate</a><br />
                                    Over 6,000+ Premium Web Templates</li>
                                <li><a href="http://www.templatesold.com/">TemplateSOLD</a><br />
                                    Premium WordPress &amp; Joomla Themes</li>
                                <li><a href="http://www.imhosted.com">ImHosted.com</a><br />
                                    Affordable Web Hosting Provider</li>
                                <li><a href="http://www.myvectorstore.com">MyVectorStore</a><br />
                                    Royalty Free Stock Icons</li>
                                <li><a href="http://www.evrsoft.com">Evrsoft</a><br />
                                    Website Builder Software &amp; Tools</li>
                                <li><a href="http://www.csshub.com/">CSS Hub</a><br />
                                    Premium CSS Templates</li>
                            </ul>
                        </div>
                    </asp:ContentPlaceHolder>
                </div>--%>
                <div class="mainbar">
                    <div class="article">
                        <table class="style4">
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Font-Names="Calibri" Font-Size="18px" 
                                        ForeColor="#F30100" Text="Your Session Has Been Expired."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="15px" 
                                        ForeColor="Black" 
                                        Text="This error has occured for one of the following reasons :"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Font-Size="13px" ForeColor="Black" 
                                        Text="1) You have used Back/Forward/Refresh button of your Browser."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Font-Size="13px" ForeColor="Black" 
                                        Text="2) You have kept the browser window idle for a long time."></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" Font-Size="13px" ForeColor="Black" 
                                        Text="Click "></asp:Label>
                                    <asp:LinkButton ID="LinkButton1" runat="server" Font-Underline="True" 
                                        ForeColor="#F40000" onclick="LinkButton1_Click">here</asp:LinkButton>
&nbsp;<asp:Label ID="Label7" runat="server" Font-Size="13px" ForeColor="Black" Text="to go to Login Page."></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
          <%--      <p class="lf">
                    &copy; Copyright 2011<a href="#"></a>.</p>
                <p class="rf">
                    Powered By : Teleysia Networks Pvt. Ltd.</p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>