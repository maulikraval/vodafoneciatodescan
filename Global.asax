﻿<%@ Application Language="C#" %>

<script RunAt="server">

    private string GenerateHashKey()
    {
        StringBuilder myStr = new StringBuilder();
        myStr.Append(Request.Browser.Browser);
        myStr.Append(Request.Browser.Platform);
        myStr.Append(Request.Browser.MajorVersion);
        myStr.Append(Request.Browser.MinorVersion);
        myStr.Append(Request.LogonUserIdentity.User.Value);
        System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
        byte[] hashdata = sha.ComputeHash(Encoding.UTF8.GetBytes(myStr.ToString()));
        return Convert.ToBase64String(hashdata);
    }

    void Application_Start(object sender, EventArgs e)
    {
        System.Net.ServicePointManager.DefaultConnectionLimit = 2147483647;

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        //Pass the custom Session ID to the browser.
        if (Response.Cookies["ASP.NET_SessionId"] != null)
        {
            //Response.Cookies["ASP.NET_SessionId"].Value = Request.Cookies["ASP.NET_SessionId"].Value + GenerateHashKey();
            Response.Cookies["ASP.NET_SessionId"].Value = GenerateHashKey();
        }

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started

    }
    void Application_PreSendRequestHeaders()
    {
        // Response.Headers.Remove("Server");
        Response.Headers.Set("Server", "My CIAT server");
        Response.Headers.Remove("X-AspNet-Version");
        Response.Headers.Remove("X-AspNetMvc-Version");
        Response.Headers.Remove("Server");
        Response.Headers.Remove("X-AspNet-Version");
        Response.Headers.Remove("Expires");
        Response.Headers.Remove("Cache-Control");
        Response.Headers.Remove("Connection");
        Response.Headers.Remove("Date");
        Response.Headers.Remove("Content-Type");
        HttpContext.Current.Response.Headers.Remove("Etag");
        HttpContext.Current.Response.Headers.Remove("X-Powered-By");
        HttpContext.Current.Response.Headers.Remove("P3P");
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
    void Application_BeginRequest(object sender, EventArgs e)
    {
        HttpContext.Current.Response.AddHeader("x-frame-options", "DENY");
        HttpContext.Current.Response.AddHeader("Cache-Control", "no-cache, no-store");
        HttpContext.Current.Response.AddHeader("Pragma", "no-cache");

        HttpContext.Current.Response.AddHeader("Server","");
        HttpContext.Current.Response.AddHeader("Etag","");
        HttpContext.Current.Response.AddHeader("X-Powered-By","");
        HttpContext.Current.Response.AddHeader("P3P","");
        //HttpContext.Current.Response.Headers.Remove("Server");
        //HttpContext.Current.Response.Headers.Remove("Etag");
        //HttpContext.Current.Response.Headers.Remove("X-Powered-By");
        //HttpContext.Current.Response.Headers.Remove("P3P");

        var application = sender as HttpApplication;
        if (application != null && application.Context != null)
        {
            //application.Context.Response.Headers.Remove("Server");
            application.Context.Response.AddHeader("Server","");
        }


        //-------SQL Inject ----------Code-----Start//

        // To prevent "ASCII Encoded/Binary String Automated SQL Injection Attack" on the Website
        HttpContext context = HttpContext.Current;
        if (context != null)
        {
            string queryString = "";

            queryString = context.Request.ServerVariables["QUERY_STRING"];
            string URL_streaming = context.Request.RawUrl;
            string URL_streaming_header = context.Request.Headers["SOAPAction"];



            try
            {
                #region
                if (URL_streaming.Contains("MVWebservice.asmx"))
                {
                    if (URL_streaming_header.Contains("upload_photos_ptw") || URL_streaming_header.Contains("upload_photos") ||
                        URL_streaming_header.Contains("upload_photos_offline") || URL_streaming_header.Contains("upload_photos_punchpoint"))
                    {
                        // Create byte array to hold request bytes
                        byte[] inputStream = new byte[HttpContext.Current.Request.ContentLength];

                        // Read entire request inputstream
                        HttpContext.Current.Request.InputStream.Read(inputStream, 0, inputStream.Length);

                        //Set stream back to beginning
                        HttpContext.Current.Request.InputStream.Position = 0;

                        //Get  XML request
                        string requestString = ASCIIEncoding.ASCII.GetString(inputStream);

                        System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                        doc.LoadXml(requestString);
                        string jsonText = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);

                        Newtonsoft.Json.Linq.JObject obj = Newtonsoft.Json.Linq.JObject.Parse(jsonText);

                        string[] url_str = URL_streaming_header.Split('/');
                        int url_str_len = url_str.Length;
                        string URL_fun = url_str[url_str_len - 1];
                        string arr_val = obj["v:Envelope"]["v:Body"][URL_fun]["data"]["#text"].ToString();
                        Newtonsoft.Json.Linq.JArray Jarr = Newtonsoft.Json.Linq.JArray.Parse(arr_val);


                        foreach (var JO in Jarr.Children<Newtonsoft.Json.Linq.JObject>())
                        {
                            List<string> keys = JO.Properties().Select(p => p.Name).ToList();
                            for (int i = 0; i < keys.Count; i++)
                            {
                                if (keys[i] == "buffer")
                                {

                                }
                                else {
                                    string ptw = Jarr[0][keys[i]].ToString();
                                    if (ptw != "") {
                                        ValidateJson VJ12 = new ValidateJson();
                                        Newtonsoft.Json.Linq.JArray arr = new Newtonsoft.Json.Linq.JArray();
                                        arr.Add(ptw);
                                        if (VJ12.DetectSqlInjection_20181012(ptw))
                                        {
                                            throw new ApplicationException();

                                            //HttpContext.Current.Request.Cookies.Clear();
                                            //if (HttpContext.Current.Session != null)
                                            //{
                                            //    Session.RemoveAll();
                                            //    Session.Clear();
                                            //    Session.Abandon();
                                            //}
                                            //Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddSeconds(-30);
                                            //Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                                            ////throw new Exception("Invalid request");
                                            //return;
                                            //string res = "";
                                            ////res = //throw new ApplicationException();
                                            //StringBuilder sb = new StringBuilder();
                                            //sb.Append(res);
                                            //HttpContext.Current.Response.ContentType = "text/html";
                                            //HttpContext.Current.Response.Write(sb.ToString());
                                            // context.Response.Write

                                        }
                                    }
                                }
                            }
                        }
                    }


                }
                #endregion

            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                Response.Redirect("~/Login.aspx");
            }

        }

        //-------SQL Inject ----------Code----END///


    }
</script>
