﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using System.Data;
using System.IO;

public partial class site_report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt1;
    DataSet dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        // HttpContext context = HttpContext.Current;
        da = new mydataaccess1();
        dt = new DataSet();
        dt = da.select_all_site();
        // GridView1.DataSource = dt.Tables[0];
        // GridView1.DataBind();

        try
        {
            HttpContext context = HttpContext.Current;
            System.IO.StreamWriter stringWrite = new System.IO.StreamWriter(Server.MapPath("~/site_reports/Site_Master_" + System.DateTime.Now.Day + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Year + ".csv"));


            //rite column header names
            for (int i = 0; i < dt.Tables[0].Columns.Count; i++)
            {
                if (i > 0)
                {
                    stringWrite.Write(",");
                }
                stringWrite.Write(dt.Tables[0].Columns[i].ColumnName);
            }
            stringWrite.Write(Environment.NewLine);
            //Write data
            foreach (DataRow row in dt.Tables[0].Rows)
            {
                for (int i = 0; i < dt.Tables[0].Columns.Count; i++)
                {

                    if (i > 0)
                    {
                        row[i] = row[i].ToString().Replace("\n", "");
                        row[i] = row[i].ToString().Replace("\t", "");
                        row[i] = row[i].ToString().Replace("\r", "");

                        if (row[i].ToString().Contains(','))
                        {
                            string[] a = row[i].ToString().Split(',');
                            string final = ""; ;
                            for (int m = 0; m < a.Length; m++)
                            {
                                final += a[m].ToString() + ";";
                            }
                            row[i] = final;
                            stringWrite.Write(",");
                        }
                        else
                        {
                            stringWrite.Write(",");
                        }
                    }
                    stringWrite.Write(row[i].ToString());
                }
                // context.Response.Write(Environment.NewLine);
                stringWrite.Write(Environment.NewLine);

            }
            //  context.Response.End();
            stringWrite.Close();
            //   System.IO.File.WriteAllText(Server.MapPath("~/site_reports/Site_Master_" + System.DateTime.Now.Day + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Year + ".csv"), stringWrite.ToString());

            da = new mydataaccess1();
            da.insert_into_doc_master("Site_Master_" + System.DateTime.Now.Day + "_" + System.DateTime.Now.Month + "_" + System.DateTime.Now.Year + ".csv");
        }
        catch (Exception ex)
        {

        }


    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    public override void VerifyRenderingInServerForm(Control control)
    {

        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.

    }

}
