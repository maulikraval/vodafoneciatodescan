﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/MasterPage.master" AutoEventWireup="true"
    CodeFile="forget_change_password.aspx.cs" Inherits="change_password" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script type="text/javascript">
    //function click(){
    $('input[id$=txtpass]').val('');
    $('input[id$=txtpass0]').val('');
    $('input[id$=txtoldpass]').val('');
    function click1() {
        // alert('hi');
        var rpass = $('input[id$=txtpass]').val();
        var hash = CryptoJS.SHA256(rpass);
        var pass = hash.toString(CryptoJS.enc.Base64);
        $('input[id$=HiddenField1]').val(pass);
        //     alert($('input[id$=HiddenField1]').val());
        // alert(rpass);  
        //alert(pass);


        // $('input[id$=txtrepass]').val(pass1);


    }


    </script>
    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--<style type="text/css">
        .style1
        {
            width: 5%;
            height: 23px;
        }
        .style2
        {
            width: 35%;
            height: 23px;
        }
        .style3
        {
            width: 55%;
            height: 23px;
        }
    </style>--%>
  
    <asp:Panel Width="1100px" ID="pan1" runat="server">
        <table width="100%">
   
            <tr>
                <td style="width: 5%;">
                    &nbsp;</td>
                <td style="text-align: center;" colspan="2">
                    <asp:Label ID="Label2" runat="server" CssClass="lblstly" Font-Bold="False" 
                        Font-Underline="False" Text="New Password"></asp:Label>
                </td>
                <td style="width: 5%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    </td>
                <td style="text-align: right;" colspan="2">
                    <hr />
                    </td>
                <td style="width: 5%; ">
                    </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblusername" runat="server" Text="User Name :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtuser" runat="server" BackColor="#FFFFCC" Enabled="False" 
                        Width="150px" CssClass="genall" ></asp:TextBox>
                </td>
                <td style="width: 5%;">
                </td>
            </tr>
            
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblpass" runat="server" Text="New Password :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:TextBox ID="txtpass" runat="server" TextMode="Password" Width="150px" 
                        CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpass"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" InvalidChars="'#&$%^*()-+=!|\/?;:{}[]`~,"
                        FilterMode="InvalidChars" TargetControlID="txtpass">
                    </cc1:FilteredTextBoxExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtpass"
                       ValidationGroup="a" Display="None" ErrorMessage=" Password must be between 8 to 19 characters. Password must contain at least one numeric and one Special character and One Capital letter. "
                        SetFocusOnError="True" ValidationExpression="^(?=.*[a-zA-Z])(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator2">
                    </cc1:ValidatorCalloutExtender>
                     <asp:HiddenField ID="HiddenField1" runat="server" />
                    &nbsp;</td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    <asp:Label ID="lblpass2" runat="server" Text="Re-Type Password" 
                        CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 55%; text-align: left;">
                    
                            <asp:TextBox ID="txtpass0" runat="server"
                                TextMode="Password" Width="150px" CssClass="genall"></asp:TextBox>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                ControlToCompare="txtpass" ControlToValidate="txtpass0" 
                                ErrorMessage="Password Not Matched"></asp:CompareValidator>
                        
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    &nbsp;
                </td>
                <td style="width: 55%; text-align: left;">
                    &nbsp;
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="width: 35%; text-align: right;">
                    &nbsp;
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td style="width: 55%; text-align: left;">
                    <asp:Button ID="btnchangpass" runat="server" OnClick="btncreate_Click" 
                        Text="Change" OnClientClick="click1()" />
                        
                    &nbsp;<asp:Button ID="btnchangpass0" runat="server" OnClick="btnchangpass0_Click" 
                        Text="Cancel" CausesValidation="False" />
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;
                </td>
                <td style="text-align: center; font-family: calibri; font-size: 13px; color: #FF0000;" 
                    class="style2" colspan="2">
                    &nbsp; &nbsp;
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </td>
                <td class="style1">
                    &nbsp;
                    </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="text-align: right; width: 35%;">
                    &nbsp;
                </td>
                <td style="text-align: left; width: 55%;">
                   
    
                    </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
