﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_approve_lat_long.aspx.cs" Inherits="admin_showuser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />
    <table width="100%">
        <tr>
            <td style="width: 5%;">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="Label3" runat="server" CssClass="lblstly" Text="Distance Details"></asp:Label>
                <hr />
                <asp:Panel ID="panel1" runat="server" ScrollBars="Horizontal" Width="950px" Height="450px">
                    <asp:GridView ID="grduser" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                         Width="100%" onrowdatabound="grduser_RowDataBound">
                          <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            
                            <asp:TemplateField HeaderText="Approve">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="False" CommandName="Delete"
                                         Text="Approve" ToolTip='<%#Eval("id")%>' onclick="lnkdelete_Click"></asp:LinkButton>
                                    <cc1:ConfirmButtonExtender ID="con" runat="server" TargetControlID="lnkdelete" ConfirmText="Are You sure ?">
                                    </cc1:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reject">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" ToolTip='<%#Eval("id")%>' 
                                        CausesValidation="False" onclick="LinkButton1_Click" >Reject</asp:LinkButton>
                                 <cc1:ConfirmButtonExtender ID="con1" runat="server" TargetControlID="LinkButton1" ConfirmText="Are You sure ?">
                                    </cc1:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>
            <td style="width: 5%;">
            </td>
        </tr>
        <tr>
            <td style="width: 5%;">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="Label4" runat="server"  CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 5%;">
            </td>
        </tr>
        <tr>
            <td style="width: 5%;">
                &nbsp;
            </td>
            <td colspan="2">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
             
            </td>
            <td style="width: 5%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%;">
                &nbsp;
            </td>
            <td style="width: 45%;">
                &nbsp;
            </td>
            <td style="width: 45%;">
                &nbsp;
            </td>
            <td style="width: 5%;">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
