﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using System.Security.Cryptography;
using mybusiness;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.Data.OleDb;
using System.IO;

public partial class admin_Active_manualassignsite : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    HSSFWorkbook hssfworkbook;
    protected void Page_Load(object sender, EventArgs e)
    {
	 da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
        if (Session["role"].ToString() != "12" && Session["um"].ToString()!=sp_user_d)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.End();
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }
    private bool CompareArray(byte[] a1, byte[] a2)
    {

        if (a1.Length != a2.Length)

            return false;



        for (int i = 0; i < a1.Length; i++)
        {

            if (a1[i] != a2[i])

                return false;

        }



        return true;

    }

    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
            imageHeader.Add("XLS", new byte[] { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 });

            // bool acceptFile = false;
            // bool acceptFile1 = false;
            byte[] header;
            string fileExt;

            fileExt = FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.') + 1).ToUpper();
            string acceptedFileTypes = ".XLS";



            byte[] tmp = imageHeader[fileExt];

            header = new byte[tmp.Length];



            // GET HEADER INFORMATION OF UPLOADED FILE

            FileUpload1.FileContent.Read(header, 0, header.Length);



            if (CompareArray(tmp, header))
            {

                try
                {
                    da = new mydataaccess1();

                    string abc = System.Guid.NewGuid() + "_" + FileUpload1.FileName;

                    abc = SpacialCharRemove.SpacialChar_Remove(abc);


                    if (abc == "")
                    {
                        lblresult.Text = "Select file first.";
                        lblresult.Visible = true;
                        ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);
                    }
                    else
                    {
                        FileUpload1.SaveAs(Server.MapPath("~/SampleData/sitedata/") + abc);
                        string path = Server.MapPath("~/SampleData/sitedata/") + abc;
                        //string excelConnectionString = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                        string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                        string constring = excelConnectionString;
                        int result;
                        string sp_user_d = "";

                        da = new mydataaccess1();
                        dt = new DataTable();
                        try
                        {
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());
                        }
                        catch (Exception ex)
                        {
                            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                        }
                        //string username = Session["user"].ToString();
                        if (sp_user_d == "")
                        {
                            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                        }
                        result = Convert.ToInt32(da.pm_insertinto_Manualassignsite_Active_n(constring, sp_user_d));
                        if (result == 1)
                        {
                            lblresult.Visible = true;
                            lblresult.Text = "Persons Successfully Not Uploaded.";
                        }
                        else if (result == 3)
                        {
                            lblresult.Visible = true;
                            lblresult.Text = "No Data In Excel.";
                        }
                        else
                        {
                            da = new mydataaccess1();
                            dt = new DataTable();
                            //dt = da.pm_select_assignsite_error();
                            dt = da.pm_insert_assignsite_error_n("", sp_user_d, 0, "", "SelectCheck");
                            string re = dt.Rows[0][0].ToString();
                            if (re == "False")
                            {

                                lblresult.Visible = false;
                                lblresult.Text = "";
                                //da = new mydataaccess1();
                                //dt = new DataTable();
                                //dt = da.pm_select_assignsite_error_Active();

                                da = new mydataaccess1();
                                dt = new DataTable();
                                //dt = da.pm_select_assignsite_error();
                                dt = da.pm_insert_assignsite_error_n("", sp_user_d, 0, "", "Select");

                                //ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);
                                if (dt.Rows.Count > 0)
                                {
                                    lblresult.Visible = true;
                                    lblresult.Text = "Please check below downloaded excel , Correct the data and Re-upload it.";
                                    string filename = "Risk_Error.xls";
                                    Response.ContentType = "application/vnd.ms-excel";
                                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                                    Response.Clear();

                                    InitializeWorkbook();
                                    exporttoexcel(dt);
                                    Response.BinaryWrite(WriteToStream().GetBuffer());
                                    //ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);
                                }
                                else
                                {
                                    lblresult.Visible = true;
                                    lblresult.Text = "Persons Successfully Uploaded.";
                                    //ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);
                                }
                            }
                            else
                            {
                                lblresult.Visible = true;
                                lblresult.Text = "Persone Successfully Uploaded.";
                            }
                        }
                            
                    }
                    //ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);
                }
                catch (Exception ex)
                {
                    //throw ex;
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Uploaded excel has invalid format or characters!!!')", true);
                    //ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);
                }
            }
            else
            {
                lblresult.Text = "Upload wrong file format,Please Upload .xls file.";
                lblresult.Visible = true;
                ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);

            }
        }
        catch
        {
            lblresult.Text = "Upload wrong file format,Please Upload .xls file.";
            lblresult.Visible = true;
            ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "javascript:loading_hide();", true);

        }
    }
}