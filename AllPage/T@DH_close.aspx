﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true" CodeFile="T@DH_close.aspx.cs" Inherits="AllPage_T_DH_close" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>HSW | Teleysia Networks Pvt. Ltd.</title>
            <script type="text/javascript">
                function Confirmationbox() {
                    var result = confirm('Are you sure you want to update selected person work(s)?');
                    if (result) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

                function SelectAll(CheckBox) {
                    TotalChkBx = parseInt('<%= this.GridView1.Rows.Count %>');
                    var TargetBaseControl = document.getElementById('<%= SpacialCharRemove.XSS_Remove(this.GridView1.ClientID) %>');
                    var TargetChildControl = "chkSelect";
                    var Inputs = TargetBaseControl.getElementsByTagName("input");
                    for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                        if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0)
                            Inputs[iCount].checked = CheckBox.checked;
                    }
                }

                function SelectDeSelectHeader(CheckBox) {
                    TotalChkBx = parseInt('<%= this.GridView1.Rows.Count %>');
                    var TargetBaseControl = document.getElementById('<%= SpacialCharRemove.XSS_Remove(this.GridView1.ClientID) %>');
                    var TargetChildControl = "chkSelect";
                    var TargetHeaderControl = "chkSelectAll";
                    var Inputs = TargetBaseControl.getElementsByTagName("input");
                    var flag = false;
                    var HeaderCheckBox;
                    for (var iCount = 0; iCount < Inputs.length; ++iCount) {
                        if (Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) >= 0)
                            HeaderCheckBox = Inputs[iCount];
                        if (Inputs[iCount] != CheckBox && Inputs[iCount].type == 'checkbox' && Inputs[iCount].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[iCount].id.indexOf(TargetHeaderControl, 0) == -1) {
                            if (CheckBox.checked) {
                                if (!Inputs[iCount].checked) {
                                    flag = false;
                                    HeaderCheckBox.checked = false;
                                    return;
                                }
                                else
                                    flag = true;
                            }
                            else if (!CheckBox.checked)
                                HeaderCheckBox.checked = false;
                        }
                    }
                    if (flag)
                        HeaderCheckBox.checked = CheckBox.checked
                }

                //function checkAll(objRef) {
                //    var GridView = objRef.parentNode.parentNode.parentNode;
                //    var inputList = GridView1.getElementsByTagName("input");
                //    for (var i = 0; i < inputList.length; i++) {
                //        //Get the Cell To find out ColumnIndex
                //        var row = inputList[i].parentNode.parentNode;
                //        if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                //            if (objRef.checked) {
                //                //If the header checkbox is checked
                //                //check all checkboxes
                //                //and highlight all rows
                //                row.style.backgroundColor = "aqua";
                //                inputList[i].checked = true;
                //            }
                //            else {
                //                //If the header checkbox is checked
                //                //uncheck all checkboxes
                //                //and change rowcolor back to original
                //                if (row.rowIndex % 2 == 0) {
                //                    //Alternating Row Color
                //                    row.style.backgroundColor = "#C2D69B";
                //                }
                //                else {
                //                    row.style.backgroundColor = "white";
                //                }
                //                inputList[i].checked = false;
                //            }
                //        }
                //    }
                //}

            </script>
        </head>
        <body>
            <div align="center">
                <table style="text-align: center">
                    <tr>
                        <td>Enter Blanket Id:</td>
                        <td>
                            <asp:TextBox ID="txtSearchBlkId" runat="server"></asp:TextBox></td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /></td>
                    </tr>
                </table>
            </div>
            <br />
            <div>
                <%--<asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                OnClick="ImageButton1_Click" ToolTip="Export To Excel" Width="25px" />--%>
                <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="100%" Visible="false">
                    <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                        ForeColor="Black" Width="100%" OnRowDataBound="GridView1_RowDataBound">
                        <FooterStyle BackColor="White" />
                        <RowStyle HorizontalAlign="Center" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" Text="Select All" onclick="SelectAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" ToolTip="" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </div>
            <div align="center">
                <table style="text-align: center">
                    <tr>
                        <td>
                            <asp:Label ID="lbl1risk" runat="server" Text="Select 1@Risk Status" Visible="false"></asp:Label></td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <%--<td>
                        <asp:Label ID="lblptw" runat="server" Text="Select PTW Status" Visible="false"></asp:Label></td>
                    </tr>--%>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="drp1AtRiskStatus" runat="server" Width="200px" CssClass="genall" AutoPostBack="False" OnSelectedIndexChanged="drp1AtRiskStatus_SelectedIndexChanged" Visible="false">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="3" Enabled="false">Closed By Receiver</asp:ListItem>
                                    <asp:ListItem Value="4" Enabled="false">Rejected</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--<td>
                            <asp:DropDownList ID="drpPTWStatus" runat="server" Width="200px" CssClass="genall" AutoPostBack="True" OnSelectedIndexChanged="drpPTWStatus_SelectedIndexChanged" Visible="false">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                                <asp:ListItem Value="4" Enabled="false">Rejected</asp:ListItem>
                                <asp:ListItem Value="10" Enabled="false">Closed By Issuer</asp:ListItem>
                                <asp:ListItem Value="3" Enabled="false">Closed By Receiver</asp:ListItem>
                            </asp:DropDownList>
                        </td>--%>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" OnClientClick="javascript:return Confirmationbox();" Visible="false" />
                            </td>
                        </tr>
                </table>
            </div>
        </body>
        </html>
    </div>
</asp:Content>

