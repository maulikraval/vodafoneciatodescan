﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FindDataAccessLayer;
//using mybusiness;
using System.Data;

public partial class AllPage_FindUser : System.Web.UI.Page
{
    FindDataAccess da;
    //myvodav2 ba;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btn_find_activate.Visible = false;
            btn_find_deactivate.Visible = false;
        }
    }

    protected void btn_find_user_Click(object sender, EventArgs e)
    {
        if (txt_User.Text != "")
        {

            da = new FindDataAccess();
            int user = da.check_user(txt_User.Text);
            if (user == 1)
            {
                txt_User.Enabled = false;
                btn_find_activate.Visible = true;
                btn_find_deactivate.Visible = true;
            }
            else
            {
                lblerror.Text = "Invalid User";
            }
        }
        else
        {
            lblerror.Text = "Enter User";
        }
    }



    protected void btn_find_activate_Click(object sender, EventArgs e)
    {
        da = new FindDataAccess();
        da.update_user_status(txt_User.Text, 0);

        lblerror.Text = "Activated";
        btn_find_activate.Visible = false;
        btn_find_deactivate.Visible = false;
        txt_User.Enabled = true;
    }
    protected void btn_find_deactivate_Click(object sender, EventArgs e)
    {
        da = new FindDataAccess();
        da.update_user_status(txt_User.Text, 1);

        lblerror.Text = "De-Activated";
        btn_find_activate.Visible = false;
        btn_find_deactivate.Visible = false;
        txt_User.Enabled = true;
    }
}