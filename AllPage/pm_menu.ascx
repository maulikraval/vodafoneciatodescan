﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="pm_menu.ascx.cs" Inherits="admin_menu" %>
<div id="menu">
    <ul class="menu">
        <li><a href="#" class="parent"><span>Dashboard</span></a>
            <ul>
                <li><a href="pm_reminder.aspx"><span>PM</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Site Details</span></a>
            <ul>
                <li><a href="PM_Site_Download.aspx"><span>Site Dump</span></a> </li>
                <li><a href="PM_Site_New_Download.aspx"><span>Sub Site Dump</span></a> </li>
            </ul>
        </li>
        <li><a href="pm_rptgrid.aspx" class="parent"><span>Report View</span></a> </li>
        <li><a href="#" class="parent"><span>Chart</span></a>
            <ul>
                <li><a href="pm_chart.aspx"><span>Inspection(PieChart)</span></a> </li>
                <li><a href="pm_bar_chart.aspx"><span>Inspection(BarChart)</span></a> </li>
                <li><a href="pm_bar_chart_category.aspx"><span> CheckPoint</span></a> </li>
                <li><a href="pm_compli_chart.aspx"><span>Site Compliance</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A"/"P" Charts</span></a>
            <ul>
                <li><a href="pm_pmt_PM_Chart.aspx"><span>g-pm "P" ChecksheetWise Chart</span></a></li>
                <li><a href="pm_pmt_PM_Chart_Survey.aspx"><span>g-pm "P" SiteWise Chart</span></a></li>
                <li><a href="pm_pmt_PM_Chart_YesNoNA.aspx"><span>g-pm "P" QuestionWise Chart</span></a></li>
                <li><a href="pm_Active_PM_Chart.aspx"><span>g-pm "A" ChecksheetWise Chart</span></a></li>
                <li><a href="pm_Active_PM_Chart_Survey.aspx"><span>g-pm "A" SiteWise Chart</span></a></li>
                <li><a href="pm_Active_PM_Chart_YesNoNA.aspx"><span>g-pm "A" QuestionWise Chart</span></a></li>

                <li><a href="pm_Active_PM_Chart_new.aspx"><span>g-pm "A" ChecksheetWise New</span></a></li>
                <li><a href="pm_Active_PM_Chart_Survey_new.aspx"><span>g-pm "A" SiteWise New</span></a></li>
                <li><a href="pm_Active_PM_Chart_YesNoNA_new.aspx"><span>g-pm "A" QuestionWise New</span></a></li>
                <li><a href="pm_chart_tx.aspx"><span>Txequip Pie Chart</span></a></li>
                <li><a href="pm_bar_chart_tx.aspx"><span>Txequip Bar Chart</span></a></li>
                <li><a href="pm_Active_PM_Chart_YesNoNA_tx.aspx"><span>Txequip QuestionWise Chart</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A"/"P" Reports</span></a>
            <ul>
                <li><a href="pm_pmt_Circle_Report.aspx"><span>g-pm "P" Excel Report</span></a></li>
                <li><a href="pm_pmt_Circle_Report_archive.aspx"><span>g-pm "P" Archive Report</span></a></li>
                <li><a href="pm_pmt_pm_PDF_Report.aspx"><span>g-pm "P" PDF Report</span></a></li>
                <li><a href="pm_pmt_TechnicianAssignReport_.aspx"><span>g-pm "P" Technician Report</span></a></li>
                <li><a href="pm_Active_Circle_Report.aspx"><span>g-pm "A" Excel Report</span></a></li>
                <li><a href="pm_Active_Circle_Report_archive.aspx"><span>g-pm "A" Archive Report</span></a></li>
                <li><a href="pm_Active_pm_PDF_Report.aspx"><span>g-pm "A" PDF Report</span></a></li>
                <li><a href="pm_Active_TechnicianAssignReport_.aspx"><span>g-pm "A" Technician Report</span></a></li>

                <li><a href="pm_Active_Circle_Report_new.aspx"><span>g-pm "A" Excel new</span></a></li>
                <li><a href="pm_Active_Circle_Report_archive_new.aspx"><span>g-pm "A" Archive new</span></a></li>
                <li><a href="pm_Active_pm_PDF_Report_new.aspx"><span>g-pm "A" PDF new</span></a></li>
                <li><a href="pm_Active_TechnicianAssignReport_new.aspx"><span>g-pm "A" Technician new</span></a></li>
<li><a href="pm_Punchpoint_Excel_Report_Active.aspx"><span>g-pm "A" PunchPoint Excel Report</span></a></li>

                <li><a href="pm_Circle_Report_tx.aspx"><span>TxEquip Excel</span></a></li>
                <li><a href="pm_Circle_Report_archive_tx.aspx"><span>TxEquip Archive</span></a></li>
                <li><a href="pm_tx_pdf_Report.aspx"><span>TxEquip PDF</span></a></li>
                <li><a href="pm_tx_TechnicianAssignReport_new.aspx"><span>TxEquip Technician</span></a></li>
        <li><a href="pm_Punch_Point_Excel_Report_TXT.aspx"><span>TxEquip PunchPoint Excel Report</span></a></li>
                <li><a href="pm_Active_pm_PDF_Report_Checksheet_And_Passive.aspx"><span>g-pm "A"/"P" PunchPoint PDF Report</span></a></li>
               
            </ul>
        </li>
        <li><a href="pm_car_pdf_Report.aspx" class="parent"><span>Inspection PDF Report</span></a>
        </li>
        <li><a href="#" class="parent"><span>PTWCharts</span></a>
            <ul>
                <li><a href="pm_admin_ptw_chart_type.aspx"><span>Type Wise</span></a></li>
                <li><a href="pm_admin_chart_status.aspx"><span>Status Wise</span></a></li>
                <li><a href="pm_admin_chart_risk.aspx"><span>Risk Wise</span></a></li>
                <li><a href="pm_admin_chart_reject_reason.aspx"><span>Rejection Wise</span></a></li>
                <li><a href="pm_admin_ptw_chart_type_night.aspx"><span>T@DH Type</span></a></li>
                <li><a href="pm_admin_chart_status_night.aspx"><span>T@DH Status</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>PTW Report</span></a>
            <ul>
                <li><a href="pm_circleadmin_Circle_Report_ptw.aspx"><span>PTW Customize</span></a></li>
                <li><a href="pm_circleadmin_Circle_Report_ptw_night.aspx"><span>T@DH Customize</span></a></li>
                <li><a href="pm_circleadmin_pdf_Report.aspx"><span>PTW PDF Report</span></a></li>
                <li><a href="pm_circleadmin_PDF_Report_night.aspx"><span>T@DH PDF Report</span></a></li>
                <%--<li><a href="pm_circleadmin_risk_Report_ptw.aspx"><span>Risk Dump</span></a></li>--%>
            </ul>
        </li>
    </ul>
</div>
