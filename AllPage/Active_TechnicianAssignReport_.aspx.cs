﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using dataaccesslayer2;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.fonts;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.IO;
using mybusiness;

public partial class PMT_Active_TechnicianAssignReport_ : System.Web.UI.Page
{
    mydataaccess1 da1;
    mydataaccess2 da;
    DataTable dt;
    myvodav2 ba;
    myvodav23 ba1;
    string sp_user_d;
    string siteid = "";
    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //  lblError.Visible = false;
        try
        {
            da1 = new mydataaccess1();
            sp_user_d = da1.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            if (!IsPostBack)
            {
                try
                {
                    trlblzone.Visible = false;
                    trddlzone.Visible = false;

                    trddlsheet.Visible = false;
                    trlblsheet.Visible = false;

                    trlbltechni.Visible = false;
                    trddltechni.Visible = false;

                    trlblstatus.Visible = false;
                    trddlstatus.Visible = false;


                    MultiView1.Visible = false;
                    ImageButton1.Visible = false;

                    int i = 12;
                    if (Convert.ToInt32(Session["role"].ToString()) == i)
                    {

                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "3")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da1= new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da1 = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da1.reminder_20_days_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da1.reminder_20_days_circle_admin_v2(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da1.reminder_20_days_circle_admin_v2ms(sp_user_d);
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da1 = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da1.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da1.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da1.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da1 = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da1.reminder_10_days_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da1.reminder_10_days_circle_admin_v2(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da1.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    lblmarquee.Text = "";
                    //lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";

                    //Marquee End

                    //if (Session.Count != 0)
                    {
                        if (Session["role"].ToString() == "12")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da1 = new mydataaccess1();
                                sp_user_d = da1.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                                if (lblusername.Text == "")
                                {
                                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }
                                da1 = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da1.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                //drpcircle.Items.Insert(1, "All");

                                #region Fill Zone
                                ddlzone.Items.Clear();
                                ddlzone.DataSource = null;
                                ddlzone.DataBind();
                                ddlzone.Items.Insert(0, "Select");
                                #endregion
                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu; 
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        Response.ClearHeaders();

        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable zone = new DataTable();

            if (drpcircle.SelectedIndex > 0)
            {
                ddlsheet.SelectedIndex = 0;
                trlblsheet.Visible = false;
                trddlsheet.Visible = false;

                ddlstatus.SelectedIndex = 0;
                trlblstatus.Visible = false;
                trddlstatus.Visible = false;

                // ddltechnicion.SelectedIndex = 0;
                trlbltechni.Visible = false;
                trddltechni.Visible = false;

                MultiView1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
                /*if (drpcircle.SelectedIndex == 1)
                {
                    ddlzone.Items.Clear();
                    ddlzone.DataSource = null;
                    ddlzone.DataBind();
                    ddlzone.Items.Insert(0, "---");
                    trddlzone.Visible = true;
                    trlblzone.Visible = true;

                    ddlsheet.SelectedIndex = 0;
                    trlblsheet.Visible = true;
                    trddlsheet.Visible = true;
                    //ddltechnicion.SelectedIndex = 0;
                    trlbltechni.Visible = false;
                    trddltechni.Visible = false;
                    ddlstatus.SelectedIndex = 0;
                    trlblstatus.Visible = false;
                    trddlstatus.Visible = false;
                    MultiView1.Visible = false;
                    ImageButton1.Visible = false;
                    GridView1.DataSource = null;
                    GridView1.DataBind();

                    DataTable tech = new DataTable();
                    da1 = new mydataaccess1();
                    string sp_user_d1 = da1.select_user_cookie(Session["user"].ToString());
                    da1 = new mydataaccess1();
                    tech = da1.pm_gettechnician(drpcircle.SelectedItem.Text, sp_user_d1);
                    if (tech.Rows.Count > 0)
                    {
                        ddltechnicion.Items.Clear();
                        ddltechnicion.DataSource = tech;
                        ddltechnicion.DataTextField = "username";
                        ddltechnicion.DataBind();
                        ddltechnicion.Items.Insert(0, "Select");
                        ddltechnicion.Items.Insert(1, "All");
                    }
                    else
                    {
                        ddltechnicion.Items.Clear();
                        ddltechnicion.DataSource = null;
                        ddltechnicion.DataTextField = "username";
                        ddltechnicion.DataBind();
                        ddltechnicion.Items.Insert(0, "Select");
                    }
                }
                else*/
                {
                    da1 = new mydataaccess1();
                    string sp_user_d12 = da1.select_user_cookie(Session["user"].ToString());
                    da1 = new mydataaccess1();
                    myvodav2 vb = new myvodav2();
                    vb.Circle = drpcircle.SelectedItem.Text.ToString();
                    zone = da1.PM_getzonesbycircle_onlyforZM(drpcircle.SelectedItem.Text.ToString(), sp_user_d12);
                    if (zone.Rows.Count > 0)
                    {
                        ddlzone.Items.Clear();
                        ddlzone.DataSource = zone;
                        ddlzone.DataTextField = "zone";
                        ddlzone.DataBind();
                        ddlzone.Items.Insert(0, "Select");
                        //ddlzone.Items.Insert(1, "All");
                        trlblzone.Visible = true;
                        trddlzone.Visible = true;
                        GridView1.DataSource = null;
                        GridView1.DataBind();

                        DataTable tech = new DataTable();
                        da1 = new mydataaccess1();
                        string sp_user_d1 = da1.select_user_cookie(Session["user"].ToString());
                        da1 = new mydataaccess1();
                        tech = da1.pm_gettechnician(drpcircle.SelectedItem.Text, sp_user_d1);
                        if (tech.Rows.Count > 0)
                        {
                            ddltechnicion.Items.Clear();
                            ddltechnicion.DataSource = tech;
                            ddltechnicion.DataTextField = "username";
                            ddltechnicion.DataBind();
                            ddltechnicion.Items.Insert(0, "Select");
                            ddltechnicion.Items.Insert(1, "All");
                        }
                        else
                        {
                            ddltechnicion.Items.Clear();
                            ddltechnicion.DataSource = null;
                            ddltechnicion.DataTextField = "username";
                            ddltechnicion.DataBind();
                            ddltechnicion.Items.Insert(0, "Select");
                        }
                    }
                    else
                    {
                        ddlzone.Items.Clear();
                        ddlzone.DataSource = null;
                        ddlzone.DataBind();
                        ddlzone.Items.Insert(0, "Select");
                        trlblzone.Visible = true;
                        trddlzone.Visible = true;
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                    }



                }
            }
            else
            {
                ddlzone.Items.Clear();
                ddlzone.DataSource = null;
                ddlzone.DataBind();
                ddlzone.Items.Insert(0, "Select");
                trlblzone.Visible = false;
                trddlzone.Visible = false;
                ddlzone.SelectedIndex = 0;

                trddlsheet.Visible = false;
                trlblsheet.Visible = false;
                ddlsheet.SelectedIndex = 0;

                trlbltechni.Visible = false;
                trddltechni.Visible = false;
                ddltechnicion.SelectedIndex = 0;

                trlblstatus.Visible = false;
                trddlstatus.Visible = false;
                ddlstatus.SelectedIndex = 0;

                MultiView1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        string circle = "";
        string zone = "";
        string sheet = "";
        string status = "";
        string technicion = "";

        if (ddlzone.SelectedIndex > 0)
        {
            circle = drpcircle.SelectedItem.Text;
            zone = ddlzone.SelectedItem.Text;
        }

        if (ddlsheet.SelectedIndex == 1)
        {
            sheet = "All";
        }
        else
        {
            sheet = ddlsheet.SelectedItem.Text;
        }
        if (ddlstatus.SelectedIndex != 0)
        {
            status = ddlstatus.SelectedItem.Text;
        }
        if (ddlstatus.SelectedItem.Text == "UnAssign")
        {
            technicion = "All";
        }
        else
        {
            technicion = ddltechnicion.SelectedItem.Text;
        }


        da1 = new mydataaccess1();
        dt = new DataTable();
        dt = da1.pm_TechnicianReport_Active(circle, zone, sheet, status, technicion);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
    }
    private void cic_header(Document doc, int i_counter)
    {
        Font calibri_Display = FontFactory.GetFont("Calibri", 10, Font.NORMAL, BaseColor.BLACK);
        Font calibri_Heading = FontFactory.GetFont("Calibri", 15, Font.BOLD, BaseColor.BLACK);

        PdfPTable head = new PdfPTable(1);
        head.WidthPercentage = 100f;

        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });

        //head.DefaultCell.Border = 0;
        string headersheet = "";
        string address = "";

        {
            headersheet = "Preventive Maintenance Checklist V1";
            address = "Address: ";
        }

        Phrase p1header = new Phrase(headersheet, new Font(calibri_Heading));
        PdfPCell c = new PdfPCell(p1header);
        c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
        c.HorizontalAlignment = Element.ALIGN_CENTER;
        c.VerticalAlignment = Element.ALIGN_MIDDLE;
        c.BorderColor = iTextSharp.text.BaseColor.BLACK;
        c.FixedHeight = 20f;
        head.AddCell(c);


        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });


        PdfPTable table1 = new PdfPTable(2);
        table1.WidthPercentage = 100f;
        //table1.SetWidths(new float[] { 4, 3, 4, 3 });

        da1 = new mydataaccess1();
        dt = new DataTable();
        dt = da1.pm_select_site_data_all_pdf(siteid);


        Phrase p1_b2 = new Phrase("Circle : " + dt.Rows[0][1].ToString(), new Font(calibri_Display));
        PdfPCell cell2 = new PdfPCell(p1_b2);
        cell2.HorizontalAlignment = Element.ALIGN_LEFT;
        cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase p1_b3 = new Phrase("Site ID : " + siteid, new Font(calibri_Display));
        PdfPCell cell3 = new PdfPCell(p1_b3);
        cell3.HorizontalAlignment = Element.ALIGN_LEFT;
        cell3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase p1_b4 = new Phrase(address + dt.Rows[0][0].ToString(), new Font(calibri_Display));
        PdfPCell cell4 = new PdfPCell(p1_b4);
        cell4.HorizontalAlignment = Element.ALIGN_LEFT;
        cell4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
        // cell1.BackgroundColor.Brighter(GDI.Color.);

        Phrase p1_b5 = new Phrase("Site Incharge Name & No. : " + dt.Rows[0][2].ToString(), new Font(calibri_Display));
        PdfPCell cell5 = new PdfPCell(p1_b5);
        cell5.HorizontalAlignment = Element.ALIGN_LEFT;
        cell5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        //ha
        /* Phrase p1_b3 = new Phrase("Location of Control room & Contact No. : " + dt.Rows[0][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
         PdfPCell cell4 = new PdfPCell(p1_b3);
         cell4.HorizontalAlignment = Element.ALIGN_LEFT;
         cell4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

         Phrase p1_b14 = new Phrase("Date of Inspection:- " + dt.Rows[0][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
         PdfPCell cell5 = new PdfPCell(p1_b14);
         cell5.HorizontalAlignment = Element.ALIGN_LEFT;
         cell5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

         Phrase p1_b15 = new Phrase("Lattitude & Longitude :" + dt.Rows[0][5].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
         PdfPCell cell6 = new PdfPCell(p1_b15);
         cell6.HorizontalAlignment = Element.ALIGN_LEFT;
         cell6.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
         */
        //

        table1.AddCell(cell2);
        table1.AddCell(cell3);
        table1.AddCell(cell4);
        table1.AddCell(cell5);

        //ha
        /* table1.AddCell(cell4);
         table1.AddCell(cell5);
         table1.AddCell(cell6);
         */
        //

        //PdfPCell note = new PdfPCell(new Phrase("Notes : Every positive answer gets full marks for the question and every negative answer would get Zero marks. Mark 'NA' if the question is Not Applicable for any particular site. Comment / explanation should be given for where a corrective action is requried. Weightage for different critical safety requirement is 2, 1 and 0.5. In case any activity description is not applicable for the Cellsite, then the points would be automatically deducted from the total score to calculate the achieved percentage. Every observation made on the site should be classified as A, B or C (A being most critical) depending on the criticality / hazard Potential (for more details refer procedure).The bold text questions imply basis of rejection at the time site acceptance from Infra Provider.", new Font(Font.FontFamily.TIMES_ROMAN, 7)));
        //note.Colspan = 2;
        //note.FixedHeight = 60f;
        //note.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#04B4AE"));
        //note.HorizontalAlignment = Element.ALIGN_CENTER;
        //note.VerticalAlignment = Element.ALIGN_MIDDLE;
        //table1.AddCell(note);

        PdfPCell blank = new PdfPCell(new Phrase(""));
        blank.Colspan = 2;
        blank.FixedHeight = 15f;
        table1.AddCell(blank);

        doc.Add(head);
        doc.Add(table1);
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlzone.SelectedIndex != 0)
        {
            if (ddlzone.SelectedIndex == 1)
            {
                ddlsheet.SelectedIndex = 0;
                trlblsheet.Visible = true;
                trddlsheet.Visible = true;
                ddltechnicion.SelectedIndex = 0;
                trlbltechni.Visible = false;
                trddltechni.Visible = false;
                ddlstatus.SelectedIndex = 0;
                trlblstatus.Visible = false;
                trddlstatus.Visible = false;
                MultiView1.Visible = false;
                ImageButton1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
            else
            {
                ddlstatus.SelectedIndex = 0;
                trlblstatus.Visible = false;
                trddlstatus.Visible = false;

                // ddltechnicion.SelectedIndex = 0;
                trlbltechni.Visible = false;
                trddltechni.Visible = false;

                MultiView1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();

                ddlsheet.SelectedIndex = 0;
                trlblsheet.Visible = true;
                trddlsheet.Visible = true;
            }
        }
        else
        {
            ddlsheet.SelectedIndex = 0;
            trlblsheet.Visible = false;
            trddlsheet.Visible = false;
            //ddltechnicion.SelectedIndex = 0;
            trlbltechni.Visible = false;
            trddltechni.Visible = false;
            ddlstatus.SelectedIndex = 0;
            trlblstatus.Visible = false;
            trddlstatus.Visible = false;
            MultiView1.Visible = false;
            ImageButton1.Visible = false;
            GridView1.DataSource = null;
            GridView1.DataBind();

        }
    }
    protected void ddlsheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {


            if (ddlsheet.SelectedIndex > 0)
            {
                if (ddlsheet.SelectedIndex == 1)
                {


                    trlbltechni.Visible = false;
                    trddltechni.Visible = false;

                    MultiView1.Visible = false;
                    GridView1.DataSource = null;
                    GridView1.DataBind();



                    ddlstatus.SelectedIndex = 0;
                    trlblstatus.Visible = true;
                    trddlstatus.Visible = true;

                    // trlbltechni.Visible = true;
                    //trddltechni.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
                else
                {


                    trlbltechni.Visible = false;
                    trddltechni.Visible = false;

                    MultiView1.Visible = false;
                    GridView1.DataSource = null;

                    ddlstatus.SelectedIndex = 0;
                    trlblstatus.Visible = true;
                    trddlstatus.Visible = true;

                    // trlbltechni.Visible = true;
                    //trddltechni.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
            else
            {
                ddlstatus.SelectedIndex = 0;
                trlblstatus.Visible = false;
                trddlstatus.Visible = false;
                //ddltechnicion.SelectedIndex = 0;
                trlbltechni.Visible = false;
                trddltechni.Visible = false;
                MultiView1.Visible = false;
                ImageButton1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();

            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void ddltechnicion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddltechnicion.SelectedIndex != 0)
        {
            if (ddltechnicion.SelectedIndex == 1)
            {
                MultiView1.Visible = true;
                MultiView1.ActiveViewIndex = 0;
                ImageButton1.Visible = true;
            }
            else
            {
                MultiView1.Visible = true;
                MultiView1.ActiveViewIndex = 0;
                ImageButton1.Visible = true;
            }
        }
        else
        {
            //ddltechnicion.SelectedIndex = 0;
            MultiView1.Visible = false;
            ImageButton1.Visible = false;
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
    }
    protected void ddlstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlstatus.SelectedIndex != 0)
        {

            if (ddlstatus.SelectedItem.Text != "Assign")
            {
                ddltechnicion.SelectedIndex = 0;
                trlbltechni.Visible = false;
                trddltechni.Visible = false;

                MultiView1.ActiveViewIndex = 0;
                MultiView1.Visible = true;
                ImageButton1.Visible = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
            else
            {
                ddltechnicion.SelectedIndex = 0;
                trlbltechni.Visible = true;
                trddltechni.Visible = true;

                MultiView1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }

            //ddltechnicion.SelectedIndex = 0;
            //trlbltechni.Visible = true;
            //trddltechni.Visible = true;

        }
        else
        {
            MultiView1.Visible = false;
            ImageButton1.Visible = false;
            ddlstatus.SelectedIndex = 0;
            trlbltechni.Visible = false;
            trddltechni.Visible = false;
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
    }
   
}