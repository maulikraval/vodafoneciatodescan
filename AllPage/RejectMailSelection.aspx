﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AllPage/AllCommon.master" EnableEventValidation="false" CodeFile="RejectMailSelection.aspx.cs" Inherits="AllPage_RejectMailSelection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<div>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Reject Mail Selection</title>
        <script type="text/javascript">
            function IsValidEmail(email) {
                var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return expr.test(email);
            };
            function ValidateEmail() {
                var email = document.getElementById("txtother").value;
                if (!IsValidEmail(email) && email != "") {
                    alert("Please enter valid EmailId!!!");
                }
            }
        </script>
    </head>
    <body>
            <div align="center">
                <table style="text-align: center">
                    <tr>
                        <td>Select EmailId:</td>
                        <td>
                            <asp:DropDownList ID="drpemail" runat="server" Width="235px" Height="23px" AutoPostBack="true" OnSelectedIndexChanged="drpemail_SelectedIndexChanged"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblother" Text="Send to other:" Visible="false"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtother" Width="235px" Height="23px" runat="server" Visible="false"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnsend" runat="server" Text="Send Mail" OnClick="btnsend_Click" OnClientClick="return ValidateEmail();" /></td>
                    </tr>
                </table>
            </div>
    </body>
    </html>
</div>
    </asp:Content>