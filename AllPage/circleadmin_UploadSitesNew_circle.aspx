﻿<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="circleadmin_UploadSitesNew_circle.aspx.cs" Inherits="admin_Default3" Title="Untitled Page" %>

<%-- Add content controls here --%>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <script language="javascript" type="text/javascript">
        function ValidateFileUpload(Source, args) {
            var fuData = document.getElementById('<%= SpacialCharRemove.XSS_Remove(FileUpload1.ClientID) %>');
            var FileUploadPath = fuData.value;

            if (FileUploadPath == '') {
                // There is no file selected 
                args.IsValid = false;
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "xls") {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
    </script>

    <table width="100%">
        <tr>
            <td width="5%"></td>
            <td width="45%" colspan="2" style="width: 90%; text-align: center">
                <asp:Label ID="Label2" runat="server" CssClass="lblstly" Text="Upload Site Master"></asp:Label>
            </td>
            <td width="5%"></td>
        </tr>
        <tr>
            <td width="5%" style="height: 26px"></td>
            <td width="45%" colspan="2" style="width: 90%; text-align: center; height: 26px;">
                <hr />
            </td>
            <td width="5%" style="height: 26px"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td width="45%" style="text-align: right">
                <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Select File To Upload:"
                    CssClass="lblall"></asp:Label>
            </td>
            <td width="45%" style="text-align: left">
                <asp:FileUpload ID="FileUpload1" runat="server" Font-Size="Medium" CssClass="genall" />
            </td>
            <td width="5%"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td width="45%"></td>
            <td width="45%">
                <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateFileUpload"
                    ErrorMessage="Please select valid .xls file"></asp:CustomValidator>
            </td>
            <td width="5%"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td width="45%" colspan="2" style="width: 90%; text-align: center">
                <asp:Button ID="Button1" runat="server" Font-Size="Medium" Height="30px" OnClick="Button1_Click"
                    Text="Upload" Width="100px" />
            </td>
            <td width="5%"></td>
        </tr>
        <tr>
            <td width="5%"></td>
            <td width="45%"></td>
            <td width="45%">
                <asp:Label ID="lblresult" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Large"
                    Font-Underline="False" Visible="False"></asp:Label>
            </td>
            <td width="5%"></td>
        </tr>
        <tr>
            <td width="5%">&nbsp;
            </td>
            <td style="width: 0%; text-align: center;">&nbsp;
            </td>
            <td width="45%" style="width: 22%; text-align: left;">
                <asp:GridView ID="GridView1" runat="server" BackColor="#CCCCCC" BorderColor="#999999"
                    BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                    Width="50%">
                    <RowStyle BackColor="White" />
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                </asp:GridView>
            </td>
            <td width="5%">&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
