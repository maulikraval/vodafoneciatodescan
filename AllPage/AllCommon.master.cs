﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using mybusiness;
using dataaccesslayer;

public partial class AllPage_AllCommon : System.Web.UI.MasterPage
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        if (!IsPostBack)
        {

            try
            {

string strPreviousPage = "";
 if (Request.UrlReferrer != null)
   {
    strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];         
    }       
if(strPreviousPage =="")
    {
       Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
     }
                int i = 4;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {
                   // header.Text = "VIL HSW";
                    menu13Support.Visible = false;
                    menubar4.Visible = true;
                    menu3ciat.Visible = false;
                    menu3ptw.Visible = false;
                    menu2pm.Visible = false;
                    menu12pmt.Visible = false;
                    menu11Ex_inspector.Visible = false;
                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 12)
                {
                   // header.Text = "VIL HSW";
                    menu13Support.Visible = false;
                    menu12pmt.Visible = true;
                    menubar4.Visible = false;
                    menu3ciat.Visible = false;
                    menu3ptw.Visible = false;
                    menu2pm.Visible = false;
                    menu11Ex_inspector.Visible = false;
                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 3)
                {
                    if (Session["flag"] == "1")
                    {
                        //this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
                        // this.MasterPageFile = "~/circle_admin/ciat.master";
                     //   header.Text = "VIL HSW";
                        menu13Support.Visible = false;
                        menubar4.Visible = false;
                        menu3ciat.Visible = true;
                        menu3ptw.Visible = false;
                        menu2pm.Visible = false;
                        menu12pmt.Visible = false;
                        menu11Ex_inspector.Visible = false;
                    }
                    if (Session["flag"] == "2")
                    {
                        // this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
                     //   header.Text = "VIL HSW";
                        menu13Support.Visible = false;
                        menubar4.Visible = false;
                        menu3ciat.Visible = false;
                        menu3ptw.Visible = true;
                        menu2pm.Visible = false;
                        menu12pmt.Visible = false;
                        menu11Ex_inspector.Visible = false;
                        //this.MasterPageFile = "~/circle_admin/ptw.master";
                    }
                    if (Session["flag"] == "3")
                    {
                        if (Session["project"].ToString() == "ciat")
                        {
                         //   header.Text = "VIL HSW";
                            menu13Support.Visible = false;
                            menubar4.Visible = false;
                            menu3ciat.Visible = true;
                            menu3ptw.Visible = false;
                            menu2pm.Visible = false;
                            menu12pmt.Visible = false;
                            menu11Ex_inspector.Visible = false;
                            //this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
                            // this.MasterPageFile = "~/circle_admin/ciat.master";
                        }
                        else
                        {
                          //  header.Text = "VIL HSW";
                            menu13Support.Visible = false;
                            menubar4.Visible = false;
                            menu3ciat.Visible = false;
                            menu3ptw.Visible = true;
                            menu2pm.Visible = false;
                            menu12pmt.Visible = false;
                            menu11Ex_inspector.Visible = false;
                        }
                    }
                    if (Session["flag"] == "4")
                    {
                        LinkButton1.Visible = true;
                        if (Session["project"].ToString() == "ciat")
                        {
                          //  header.Text = "VIL HSW";
                            menu13Support.Visible = false;
                            menubar4.Visible = false;
                            menu3ciat.Visible = true;
                            menu3ptw.Visible = false;
                            menu2pm.Visible = false;
                            menu12pmt.Visible = false;
                            menu11Ex_inspector.Visible = false;

                           
                        }
                        if (Session["project"].ToString() == "ptw")
                        {
                          //  header.Text = "VIL HSW";
                            menu13Support.Visible = false;
                            menubar4.Visible = false;
                            menu3ciat.Visible = false;
                            menu3ptw.Visible = true;
                            menu2pm.Visible = false;
                            menu12pmt.Visible = false;
                            menu11Ex_inspector.Visible = false;
                        }
                    }                 
                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 13)
                {
                    //if (Session["project"].ToString() == "ciat")
                    //{
                        //header.Text = "VIL HSW";
                       // header.Text = "VIL HSW";
                        menu13Support.Visible = true;
                        menubar4.Visible = false;
                        menu3ciat.Visible = false;
                        menu3ptw.Visible = false;
                        menu2pm.Visible = false;
                        menu12pmt.Visible = false;
                        menu11Ex_inspector.Visible = false;
                    //}
                    //else
                    //{
                    //    header.Text = "VIL HSW";
                    //    menu13Support.Visible = true;
                    //    menubar4.Visible = false;
                    //    menu3ciat.Visible = false;
                    //    menu3ptw.Visible = false;
                    //    menu2pm.Visible = false;
                    //    menu12pmt.Visible = false;
                    //    menu11Ex_inspector.Visible = false;
                    //}
                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 2 || Convert.ToInt32(Session["role"].ToString()) == 5 )
                    //|| Convert.ToInt32(Session["role"].ToString()) == 10)
                {
                   // header.Text = "VIL HSW";
                    menu13Support.Visible = false;
                    menubar4.Visible = false;
                    menu3ciat.Visible = false;
                    menu3ptw.Visible = false;
                    menu2pm.Visible = true;
                    menu12pmt.Visible = false;
                    menu11Ex_inspector.Visible = false;
                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 11 )
                {
                   // header.Text = "VIL HSW";
                    menu13Support.Visible = false;
                    menu11Ex_inspector.Visible = true;
                    menubar4.Visible = false;
                    menu3ciat.Visible = false;
                    menu3ptw.Visible = false;
                    menu2pm.Visible = false;
                    menu12pmt.Visible = false;
                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();

                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            try
            {
                if (Convert.ToInt32(Session["role"].ToString()) == 3)
                {
                    if (Session["flag"] == "1")
                    {
                        #region CIAT
                        int count = 0;
                        int count1 = 0;
                        int count2 = 0;
                        da = new mydataaccess1();
                        dt = new DataTable();
                        string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    /*    da = new mydataaccess1();
                        DataTable dtv5 = new DataTable();
                        dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                        da = new mydataaccess1();
                        DataTable dtv2 = new DataTable();
                        dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                        da = new mydataaccess1();
                        DataTable dtv2ms = new DataTable();
                        dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);

                        da = new mydataaccess1();
                        DataTable dtall = new DataTable();
                        dtall = da.reminder_20_days_circle_admin_all(sp_user_d);

                        da = new mydataaccess1();
                        DataTable cvall = new DataTable();
                        cvall = da.reminder_20_days_critical_all_circle_admin(sp_user_d);

                        da = new mydataaccess1();
                        DataTable dtall1 = new DataTable();
                        dtall1 = da.reminder_10_days_circle_admin_all(sp_user_d);

                        count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                        // critical points
                        da = new mydataaccess1();
                        DataTable cv2 = new DataTable();
                        cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                        da = new mydataaccess1();
                        DataTable cv5 = new DataTable();
                        cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                        da = new mydataaccess1();
                        DataTable cv2ms = new DataTable();
                        cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                        count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                        da = new mydataaccess1();
                        DataTable dtv51 = new DataTable();
                        dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                        da = new mydataaccess1();
                        DataTable dtv21 = new DataTable();
                        dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                        da = new mydataaccess1();
                        DataTable dtv2ms1 = new DataTable();
                        dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                        count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                        lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
						*/
                        //Marquee End

                        lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                        #endregion
                    }
                    if (Session["flag"] == "2")
                    {
                        #region ptw
                        int countptw = 0;
                        int count1ptw = 0;
                        int count2ptw = 0;
                        da = new mydataaccess1();
                        dt = new DataTable();
                        string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                      /*  da = new mydataaccess1();
                        countptw = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                        da = new mydataaccess1();
                        count1ptw = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);

                        lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + countptw + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1ptw + "</span>)";
                       */ lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                        #endregion
                    }
                    if (Session["flag"] == "4")
                    {
                        if (Session["project"].ToString() == "ciat")
                        {
                            #region CIAT
                            int count = 0;
                            int count1 = 0;
                            int count2 = 0;
                            da = new mydataaccess1();
                            dt = new DataTable();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                       /*     da = new mydataaccess1();
                            DataTable dtv5 = new DataTable();
                            dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv2 = new DataTable();
                            dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv2ms = new DataTable();
                            dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);

                            da = new mydataaccess1();
                            DataTable dtall = new DataTable();
                            dtall = da.reminder_20_days_circle_admin_all(sp_user_d);

                            da = new mydataaccess1();
                            DataTable cvall = new DataTable();
                            cvall = da.reminder_20_days_critical_all_circle_admin(sp_user_d);

                            da = new mydataaccess1();
                            DataTable dtall1 = new DataTable();
                            dtall1 = da.reminder_10_days_circle_admin_all(sp_user_d);

                            count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                            // critical points
                            da = new mydataaccess1();
                            DataTable cv2 = new DataTable();
                            cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable cv5 = new DataTable();
                            cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable cv2ms = new DataTable();
                            cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                            count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                            da = new mydataaccess1();
                            DataTable dtv51 = new DataTable();
                            dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv21 = new DataTable();
                            dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv2ms1 = new DataTable();
                            dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                            count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                            lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
							*/
                            //Marquee End

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            #endregion
                        }
                        else
                        {
                            #region ptw
                            int countptw = 0;
                            int count1ptw = 0;
                            int count2ptw = 0;
                            da = new mydataaccess1();
                            dt = new DataTable();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                        /*    da = new mydataaccess1();
                            countptw = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                            da = new mydataaccess1();
                            count1ptw = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);

                          lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + countptw + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1ptw + "</span>)";
                           */ lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            #endregion
                        }
                    }
                    if (Session["flag"] == "3")
                    {
                        if (Session["project"].ToString() == "ciat")
                        {
                            #region CIAT
                            int count = 0;
                            int count1 = 0;
                            int count2 = 0;
                            da = new mydataaccess1();
                            dt = new DataTable();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                       /*     da = new mydataaccess1();
                            DataTable dtv5 = new DataTable();
                            dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv2 = new DataTable();
                            dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv2ms = new DataTable();
                            dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);

                            da = new mydataaccess1();
                            DataTable dtall = new DataTable();
                            dtall = da.reminder_20_days_circle_admin_all(sp_user_d);

                            da = new mydataaccess1();
                            DataTable cvall = new DataTable();
                            cvall = da.reminder_20_days_critical_all_circle_admin(sp_user_d);

                            da = new mydataaccess1();
                            DataTable dtall1 = new DataTable();
                            dtall1 = da.reminder_10_days_circle_admin_all(sp_user_d);

                            count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                            // critical points
                            da = new mydataaccess1();
                            DataTable cv2 = new DataTable();
                            cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable cv5 = new DataTable();
                            cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable cv2ms = new DataTable();
                            cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                            count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                            da = new mydataaccess1();
                            DataTable dtv51 = new DataTable();
                            dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv21 = new DataTable();
                            dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                            da = new mydataaccess1();
                            DataTable dtv2ms1 = new DataTable();
                            dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                            count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                            lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
							*/
                            //Marquee End

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            #endregion
                        }
                        else
                        {
                            #region ptw
                            int countptw = 0;
                            int count1ptw = 0;
                            int count2ptw = 0;
                            da = new mydataaccess1();
                            dt = new DataTable();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                         /*   da = new mydataaccess1();
                            countptw = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                            da = new mydataaccess1();
                            count1ptw = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);

                            lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + countptw + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1ptw + "</span>)";
                          */  lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            #endregion
                        }
                    }
                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 12)
                {
                    lblmarquee.Text = "";
                    //lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count_pa + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2_pa + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1_pa + "</span>)";
                    da = new mydataaccess1();
                    dt = new DataTable();
                    string s1 = da.select_user_cookie(Session["user"].ToString());
                    lblusername.Text = SpacialCharRemove.XSS_Remove(s1);
                }
                else
                {
                    #region common
                    int count_pa = 0;
                    int count1_pa = 0;
                    int count2_pa = 0;
                    // Marquee start
                  /*  da = new mydataaccess1();
                    DataTable dtv5_pa = new DataTable();
                    dtv5_pa = da.reminder_20_days();
                    da = new mydataaccess1();
                    DataTable dtv2_pa = new DataTable();
                    dtv2_pa = da.reminder_20_days_v2();
                    da = new mydataaccess1();
                    DataTable dtv2ms_pa = new DataTable();
                    dtv2ms_pa = da.reminder_20_days_v2ms();
                    da = new mydataaccess1();
                    DataTable dtall_pa = new DataTable();
                    dtall_pa = da.reminder_20_days_all();
                    da = new mydataaccess1();
                    DataTable cvall_pa = new DataTable();
                    cvall_pa = da.reminder_20_days_critical_all();
                    da = new mydataaccess1();
                    DataTable dtall1_pa = new DataTable();
                    dtall1_pa = da.reminder_10_days_all();
                    count_pa = dtv5_pa.Rows.Count + dtv2_pa.Rows.Count + dtv2ms_pa.Rows.Count + dtall_pa.Rows.Count;

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2_pa = new DataTable();
                    cv2_pa = da.reminder_20_days_critical_v2();
                    da = new mydataaccess1();
                    DataTable cv5_pa = new DataTable();
                    cv5_pa = da.reminder_20_days_critical();
                    da = new mydataaccess1();
                    DataTable cv2ms_pa = new DataTable();
                    cv2ms_pa = da.reminder_20_days_critical_v2ms();
                    count1_pa = cv2_pa.Rows.Count + cv5_pa.Rows.Count + cv2ms_pa.Rows.Count + cvall_pa.Rows.Count;

                    da = new mydataaccess1();
                    DataTable dtv51_pa = new DataTable();
                    dtv51_pa = da.reminder_10_days();
                    da = new mydataaccess1();
                    DataTable dtv21_pa = new DataTable();
                    dtv21_pa = da.reminder_10_days_v2();
                    da = new mydataaccess1();
                    DataTable dtv2ms1_pa = new DataTable();
                    dtv2ms1_pa = da.reminder_10_days_v2ms();
                    count2_pa = dtv51_pa.Rows.Count + dtv21_pa.Rows.Count + dtv2ms1_pa.Rows.Count + dtall1_pa.Rows.Count;
                    lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count_pa + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2_pa + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1_pa + "</span>)";*/

                    da = new mydataaccess1();
                    dt = new DataTable();
                    string s = da.select_user_cookie(Session["user"].ToString());
                    lblusername.Text = SpacialCharRemove.XSS_Remove(s);
                    #endregion
                }
            }
            catch (Exception ee)
            {
               throw ee;
               // Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            finally
            { }
        }
    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess1();

            da.update_user_master_status(r);

            
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }


            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}
