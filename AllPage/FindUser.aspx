﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FindUser.aspx.cs" Inherits="AllPage_FindUser" MasterPageFile="~/AllPage/AllCommon.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbl_User" runat="server" Text="Username"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_User" runat="server"></asp:TextBox>
                    
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:Button ID="btn_find_user" runat="server" Text="Find" OnClick="btn_find_user_Click" />
                    <asp:Label ID="Label1" runat="server" Visible="false" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_contact" runat="server" Text="Contact"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_contact" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:Button ID="btn_find_contact" runat="server" Text="Find" OnClick="btn_find_contact_Click" />
                    <asp:Label ID="Label2" runat="server" Visible="false" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <br />
        <asp:GridView ID="gv_user" runat="server" EnableModelValidation="True" OnSelectedIndexChanged="gv_user_SelectedIndexChanged"
            Visible="false" 
            
            BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="30%">
        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle Font-Names="Calibri" Font-Size="13px" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

            <Columns>
                <asp:CommandField ShowSelectButton="True" />
            </Columns>
            <EmptyDataTemplate>
                <asp:Label ID="lbl_gv" runat="server" Text="No Data Found...!!" ForeColor="Black"></asp:Label>
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
