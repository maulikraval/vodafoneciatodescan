﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using System.Security.Cryptography;
using mybusiness;
using System.IO;

public partial class AllPage_support_request_1Atrisk : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    string fitness_doc_path = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["role"].ToString() != "13")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {

                try
                {
                    da = new mydataaccess1();
                    string sp = da.select_user_cookie(Session["user"].ToString());
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();
                    dt = da.getcircle_user(Convert.ToString(sp));
                    ddlcircle.DataTextField = "circle";
                    ddlcircle.DataSource = dt;
                    ddlcircle.DataBind();
                    ddlcircle.Items.Insert(0, "--Select--");
                    ddltrainedcompany.Items.Insert(0, "--Select--");
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.select_company_issuer();
                    ddlpcompany.DataTextField = "Company Name";
                    ddlpcompany.DataSource = dt;
                    ddlpcompany.DataBind();
                    ddlpcompany.Items.Insert(0, "--Select--");
                    ddlqualification.Items.Insert(0, "--Select--");
                    Label1.Text = "";
                    /*ddlrole.Enabled = false;
                    ddlrole0.Enabled = false;*/
                }
                catch
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }
        }
    }

    private static string CreateSalt(int size)
    {
        // Generate a cryptographic random number using the cryptographic 
        // service provider
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

        byte[] buff = new byte[size];
        rng.GetBytes(buff);

        // Return a Base64 string representation of the random number
        return Convert.ToBase64String(buff);
    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }

    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }

    private bool CompareArray(byte[] a1, byte[] a2)
    {
        if (a1.Length != a2.Length)
            return false;
        for (int i = 0; i < a1.Length; i++)
        {
            if (a1[i] != a2[i])
                return false;
        }
        return true;
    }

    protected void ddltrainigtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddltrainigtype.SelectedIndex == 0)
        {
            ddltrainedcompany.Enabled = true;
            txtothercompany.Text = "";
            txtothercompany.Visible = false;
            ddltrainedcompany.Items.Clear();
            ddltrainedcompany.Items.Insert(0, "--Select--");
            ddlqualification.Items.Clear();
            ddlqualification.Items.Insert(0, "--Select--");
            ddlqualification.Enabled = false;
            txtqualification.Enabled = true;
        }
        if (ddltrainigtype.SelectedIndex == 1 || ddltrainigtype.SelectedIndex == 7 || ddltrainigtype.SelectedIndex == 8 || ddltrainigtype.SelectedIndex == 9 || ddltrainigtype.SelectedIndex == 10 || ddltrainigtype.SelectedIndex == 11 || ddltrainigtype.SelectedIndex == 12)
        {
            ddltrainedcompany.Enabled = true;
            txtothercompany.Visible = false;
            ddltrainedcompany.Items.Clear();
            ddltrainedcompany.Items.Insert(0, "--Select--");
            ddltrainedcompany.Items.Insert(1, "karam");
            ddltrainedcompany.Items.Insert(2, "forcetech");
            ddltrainedcompany.Items.Insert(3, "pentasafe");
            ddlqualification.Items.Clear();
            ddlqualification.Items.Insert(0, "--Select--");
            ddlqualification.Enabled = false;
            txtqualification.Enabled = true;
        }
        if (ddltrainigtype.SelectedIndex == 5)
        {
            ddltrainedcompany.Enabled = true;
            txtothercompany.Visible = false;
            ddltrainedcompany.Items.Clear();
            ddltrainedcompany.Items.Insert(0, "--Select--");
            ddltrainedcompany.Items.Insert(1, "nokia");
            ddltrainedcompany.Items.Insert(2, "vodafone");
            ddltrainedcompany.Items.Insert(3, "hubert ebner india pvt ltd");
            ddlqualification.Items.Clear();
            ddlqualification.Items.Insert(0, "--Select--");
            ddlqualification.Enabled = false;
            txtqualification.Enabled = true;
        }
        if (ddltrainigtype.SelectedIndex == 6)
        {
            ddltrainedcompany.Enabled = true;
            txtothercompany.Visible = false;
            ddltrainedcompany.Items.Clear();
            ddltrainedcompany.Items.Insert(0, "--Select--");
            ddltrainedcompany.Items.Insert(1, "nokia");
            ddltrainedcompany.Items.Insert(2, "maruti driving school");
            ddlqualification.Items.Clear();
            ddlqualification.Items.Insert(0, "--Select--");
            ddlqualification.Enabled = false;
            txtqualification.Enabled = true;
        }
        if (ddltrainigtype.SelectedIndex == 2)
        {
            ddltrainedcompany.Items.Clear();
            ddltrainedcompany.Items.Insert(0, "--Select--");
            ddltrainedcompany.Enabled = false;
            txtothercompany.Text = "";
            txtothercompany.Visible = true;
            txtqualification.Text = "";
            txtqualification.Enabled = false;
            RequiredFieldValidator9.Visible = false;
            ddlqualification.Enabled = true;
            ddlqualification.Items.Clear();
            ddlqualification.Items.Insert(0, "--Select--");
            ddlqualification.Items.Insert(1, "220V/440V wireman license from Govt. authority");
            ddlqualification.Items.Insert(2, "1 year Vocational course in Electrical discipline/domain form any Govt. authorized/ affiliated institute");
            ddlqualification.Items.Insert(3, "2 years ITI on any Electrical in discipline/domain form any Govt. authorized/ affiliated institute");
            ddlqualification.Items.Insert(4, "Diploma in Electrical engineering");
            ddlqualification.Items.Insert(5, "BE/BTech in Electrical engineering");
            ddlqualification.Items.Insert(6, "BE/BTech in Electrical & Electronics");
            ddlqualification.Items.Insert(7, "Only HT license from Govt. authority  to access HT area and work");
            ddlqualification.Items.Insert(8, "Diploma In Mechanical");
        }
        if (ddltrainigtype.SelectedIndex == 3 || ddltrainigtype.SelectedIndex == 4)
        {
            ddltrainedcompany.Items.Clear();
            ddltrainedcompany.Items.Insert(0, "--Select--");
            ddltrainedcompany.Enabled = false;
            txtothercompany.Text = "";
            txtothercompany.Visible = true;
            ddlqualification.Items.Clear();
            ddlqualification.Items.Insert(0, "--Select--");
            ddlqualification.Enabled = false;
            txtqualification.Enabled = true;
        }
    }

    public void form_submit()
    {
        int res = 0;
        da = new mydataaccess1();
        string sp = da.select_user_cookie(Session["user"].ToString());
        #region maincode
        da = new mydataaccess1();        
        if (ddltrainigtype.SelectedIndex == 2)
        {
            res = da.insert_into_person_risk_master_request(ddlcircle.SelectedItem.Text, txtfunction.Text, txtsfunction.Text, txtdriver.Text, txtbdate.Text, txtcompany.Text, txtcontactno.Text, txtspno.Text, ddlempdetails.SelectedItem.Text, ddlqualification.SelectedItem.Text, Convert.ToInt32(txtexp.Text), txtlicenceno.Text, txtlicvalyear.Text, ddlyesno.Text, ddltrainigtype.SelectedIndex, txttrainingdate.Text, txtothercompany.Text, txttcertino.Text, txttrainingvaldate.Text, ddlpcompany.SelectedItem.Text, ddlmedfit.SelectedItem.Text, "", fitness_doc_path, sp, DateTime.Now.ToString(), ddltrainigtype.SelectedItem.Text);
        }
        if (ddltrainigtype.SelectedIndex == 1 || ddltrainigtype.SelectedIndex == 7 || ddltrainigtype.SelectedIndex == 8 || ddltrainigtype.SelectedIndex == 9 || ddltrainigtype.SelectedIndex == 10 || ddltrainigtype.SelectedIndex == 11 || ddltrainigtype.SelectedIndex == 12 || ddltrainigtype.SelectedIndex == 5 || ddltrainigtype.SelectedIndex == 6)
        {
            if (ddltrainigtype.SelectedIndex == 7 || ddltrainigtype.SelectedIndex == 8 || ddltrainigtype.SelectedIndex == 9 || ddltrainigtype.SelectedIndex == 10 || ddltrainigtype.SelectedIndex == 11 || ddltrainigtype.SelectedIndex == 12)
            {
                res = da.insert_into_person_risk_master_request(ddlcircle.SelectedItem.Text, txtfunction.Text, txtsfunction.Text, txtdriver.Text, txtbdate.Text, txtcompany.Text, txtcontactno.Text, txtspno.Text, ddlempdetails.SelectedItem.Text, txtqualification.Text, Convert.ToInt32(txtexp.Text), txtlicenceno.Text, txtlicvalyear.Text, ddlyesno.Text, 1, txttrainingdate.Text, ddltrainedcompany.SelectedItem.Text, txttcertino.Text, txttrainingvaldate.Text, ddlpcompany.SelectedItem.Text, ddlmedfit.SelectedItem.Text, "", fitness_doc_path, sp, DateTime.Now.ToString(), ddltrainigtype.SelectedItem.Text);
            }
            else
            {
                res = da.insert_into_person_risk_master_request(ddlcircle.SelectedItem.Text, txtfunction.Text, txtsfunction.Text, txtdriver.Text, txtbdate.Text, txtcompany.Text, txtcontactno.Text, txtspno.Text, ddlempdetails.SelectedItem.Text, txtqualification.Text, Convert.ToInt32(txtexp.Text), txtlicenceno.Text, txtlicvalyear.Text, ddlyesno.Text, ddltrainigtype.SelectedIndex, txttrainingdate.Text, ddltrainedcompany.SelectedItem.Text, txttcertino.Text, txttrainingvaldate.Text, ddlpcompany.SelectedItem.Text, ddlmedfit.SelectedItem.Text, "", fitness_doc_path, sp, DateTime.Now.ToString(), ddltrainigtype.SelectedItem.Text);
            }
        }
        if (ddltrainigtype.SelectedIndex == 3 || ddltrainigtype.SelectedIndex == 4)
        {
            res = da.insert_into_person_risk_master_request(ddlcircle.SelectedItem.Text, txtfunction.Text, txtsfunction.Text, txtdriver.Text, txtbdate.Text, txtcompany.Text, txtcontactno.Text, txtspno.Text, ddlempdetails.SelectedItem.Text, txtqualification.Text, Convert.ToInt32(txtexp.Text), txtlicenceno.Text, txtlicvalyear.Text, ddlyesno.Text, ddltrainigtype.SelectedIndex, txttrainingdate.Text, txtothercompany.Text, txttcertino.Text, txttrainingvaldate.Text, ddlpcompany.SelectedItem.Text, ddlmedfit.SelectedItem.Text, "", fitness_doc_path, sp, DateTime.Now.ToString(), ddltrainigtype.SelectedItem.Text);
        }

        if (res == 1 || res == 4)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Record Inserted Successfully!!!');</script>");
        }
        else if (res == 2 || res == 3 || res == 5)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Record Updated Successfully!!!');</script>");
        }
        else
        { 
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Record Is In Wrong Format!!!');</script>");
        }
        #endregion maincode
    }

    protected void btncreate_Click(object sender, EventArgs e)
    {
        try
        {
            #region txt_if
            if (txtfunction.Text != "" && txtsfunction.Text != "" && txtdriver.Text != "" && txtbdate.Text != "" && txtcompany.Text != "" && txtcontactno.Text != "" && txtspno.Text != "" && txtexp.Text != "" && txtlicenceno.Text != "" && txtlicvalyear.Text != "" && txttrainingdate.Text != "" && txttcertino.Text != "" && txttrainingvaldate.Text != "")
            {
                #region ddl_if
                if (ddlcircle.SelectedIndex != 0 && ddlempdetails.SelectedIndex != 0 && ddltrainigtype.SelectedIndex != 0 && ddlyesno.SelectedIndex != 0 && ddlpcompany.SelectedIndex != 0 && ddlmedfit.SelectedIndex != 0)
                {
                    #region fileupload_if
                    HttpPostedFile file = FileUpload1.PostedFile;
                    string Path = "~/request_reports/ptw/";
                    string tempfileName = "";
                    string filename = SpacialCharRemove.SpacialChar_Remove(FileUpload1.FileName);
                    FileInfo fi2 = new FileInfo((Server.MapPath(Path) + filename /*+ img.Src*/));
                    string pathToCheck = fi2.ToString();

                    if (file != null && file.ContentLength > 0)
                    {
                        if (file.FileName.ToString() != "")
                        {
                            #region same_file_name_change
                            if (System.IO.File.Exists(pathToCheck))
                            {
                                int counter;
                                //using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
                                //{
                                //   int[] randomNumber = new int[4];//4 for int32
                                //   rng.GetBytes(randomNumber);
                                //   counter = BitConverter.ToInt32(randomNumber, 0);
                                //}
                                Random rand = new Random();
                                counter = rand.Next(00000, 99999);
                                //int counter = 2;
                                while (System.IO.File.Exists(pathToCheck))
                                {
                                    // if a file with this name already exists,
                                    // prefix the filename with a number.
                                    string last = filename.Substring(filename.LastIndexOf('.') + 1);
                                    string first = filename.Remove((filename.Length - 1) - last.Length);
                                    tempfileName = first + "_" + counter.ToString() + "." + last;
                                    pathToCheck = Path + tempfileName;
                                    counter++;
                                }
                                filename = tempfileName;
                                // Notify the user that the file name was changed.
                                //Label1.Visible = true;
                                Label1.Text = "A file with the same name already exists. Your file was saved as " + filename;
                                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(Label1.Text) + "!!!');</script>");
                            }
                            #endregion same_file_name_change

                            file.SaveAs((Server.MapPath(Path) + filename));//Returns the physical file path that corresponds to the specified virtual path.
                            //img.Src = ImagePath + Fu.FileName;
                        }
                        FileInfo fi = new FileInfo((Server.MapPath(Path) + filename /*+ img.Src*/));
                        //fitness_doc_path = fi.ToString();
                        fitness_doc_path = filename;

                        form_submit();
                    }
                    else
                    {
                        if (ddltrainigtype.SelectedIndex == 1)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Upload File First!!!');</script>");
                        }
                        else
                        {
                            form_submit();
                        }
                    }
                    #endregion fileupload_if
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select The Proper Value!!!');</script>");
                }
                #endregion ddl_if
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Fill All The Fields!!!');</script>");
            }
            #endregion txt_if

            ddltrainigtype.SelectedIndex = 0; ddlcircle.SelectedIndex = 0; txtfunction.Text = ""; txtsfunction.Text = "";
            txtdriver.Text = ""; txtbdate.Text = ""; txtcompany.Text = ""; txtcontactno.Text = ""; txtspno.Text = "";
            ddlempdetails.SelectedIndex = 0; txtexp.Text = ""; txtlicenceno.Text = ""; txtlicvalyear.Text = ""; txttrainingvaldate.Text = "";
            ddlyesno.SelectedIndex = 0; txttrainingdate.Text = ""; txttcertino.Text = ""; ddlpcompany.SelectedIndex = 0;
            ddlmedfit.SelectedIndex = 0; ddltrainedcompany.SelectedIndex = 0; ddlqualification.SelectedIndex = 0;
            ddlqualification.Enabled = false; txtqualification.Text = ""; txtothercompany.Text = "";
            ddlqualification.Items.Clear(); ddltrainedcompany.Items.Clear();
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
}
