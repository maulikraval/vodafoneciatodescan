﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using System.IO;
using NPOI.POIFS.FileSystem;
using System.Data.OleDb;
//using Excel = Microsoft.Office.Interop.Excel;

public partial class admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    HSSFWorkbook hssfworkbook;
    string project = "";

  
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try
            {
                int Role = 4;
                if (Session["role"].ToString() != Role.ToString())
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    string sp_user_d = "";
                    da = new mydataaccess1();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.select_company_receiver();
                    Session["grdCompanyReceiver"] = dt;
                    grdcompanyreceiver.DataSource = dt;
                    grdcompanyreceiver.DataBind();
                }

            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

        }

    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    void export(System.Data.DataTable dt1)
    {

        string filename = "CompanyRecieverData.xls";

        int flag = 0;
        InitializeWorkbook();

        if (dt1 == null)
        {
            //Label1.Text = "All Sites Uploaded Successfully...,Wait For reports";
        }
        else
        {
            exporttoexcel(dt1, "Company Reciever");
            flag++;

        }

        if (flag != 0)
        {
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));

            Response.Clear();
            Response.BinaryWrite(WriteToStream().GetBuffer());
            Response.Flush();
        }
        else
        {
            //  TabContainer1.Visible = true;
        }

    }

    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }

    void exporttoexcel(System.Data.DataTable dt, string name)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet(name);
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(SpacialCharRemove.StartsWithAny(dt.Columns[j].ColumnName));
            //row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(SpacialCharRemove.StartsWithAny(dt.Rows[i][j].ToString()));
                //row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }

    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        System.Data.DataTable dt1 = (System.Data.DataTable)Session["grdCompanyReceiver"];
        export(dt1);
    }
}
