﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using System.Security.Cryptography;
using mybusiness;

public partial class AllPage_admin_adduser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
		 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
            if (Session["role"].ToString() != "4" && Session["um"].ToString()!=sp_user_)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {

                try
                {
                    /*if (Convert.ToInt32(Session["role"].ToString()) == 4)
                                    {
                                    }
                                    else if (Convert.ToInt32(Session["role"].ToString()) == 3)
                                    {
                                    }
                                    else
                                    {
                                        Session.Abandon();
                                        Response.End();
                                    }*/

                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();
                    dt = da.selectcircle(ba);
                    ddlcircle.DataTextField = "circle";
                    ddlcircle.DataSource = dt;
                    ddlcircle.DataBind();
                    ddlcircle.Items.Insert(0, "---Select---");
                    //company
                    mydataaccess1 da1 = new mydataaccess1();
                    DataTable dt1 = new DataTable();
                    dt1 = da1.select_company();
                    ddlcompany.DataTextField = "company_name";
                    ddlcompany.DataSource = dt1;
                    ddlcompany.DataBind();
                    ddlcompany.Items.Insert(0, "---Select Company---");

                    ddlrole.Enabled = false;
                    ddlrole0.Enabled = false;
                    trcirclelb.Visible = false;
                    trzonelb.Visible = false;
                    trzone.Visible = false;


                }
                catch
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }
        }
    }

    private static string CreateSalt(int size)
    {
        // Generate a cryptographic random number using the cryptographic 
        // service provider
        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

        byte[] buff = new byte[size];
        rng.GetBytes(buff);

        // Return a Base64 string representation of the random number
        return Convert.ToBase64String(buff);
    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }

    private bool CompareArray(byte[] a1, byte[] a2)
    {

        if (a1.Length != a2.Length)

            return false;



        for (int i = 0; i < a1.Length; i++)
        {

            if (a1[i] != a2[i])

                return false;

        }



        return true;

    }

    protected void btncreate_Click(object sender, EventArgs e)
    {
        try
        {
            int flag = 0;
            if (Label1.Text == "")
            {
            #region maincode
            if (ddlcircle.SelectedIndex != 0 && ddlcompany.SelectedIndex != 0)
            {
                    if (txtsubcompany.Text != "")
                    {
                        Label1.Visible = false;
                        // da = new mydataaccess1();
                        ba = new myvodav2();
                        dt = new DataTable();
                        ba.User = txtuser.Text.Replace(" ", "");


                        //// Create a new instance of the hash crypto service provider.
                        //HashAlgorithm hashAlg = new SHA256CryptoServiceProvider();
                        //// Convert the data to hash to an array of Bytes.
                        //byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(txtpass.Text);
                        //// Compute the Hash. This returns an array of Bytes.
                        //byte[] bytHash = hashAlg.ComputeHash(bytValue);
                        //// Optionally, represent the hash value as a base64-encoded string, 
                        //// For example, if you need to display the value or transmit it over a network.
                        //string base64 = Convert.ToBase64String(bytHash);

                        ba.Password = HiddenField1.Value;
                        string repass = HiddenField2.Value;
                        ba.Dump = txtpass.Text;
                        if (ddlproject.Text == "CIAT" && ddlrole.SelectedIndex != 0)
                        {
                            ba.Stepid = 1;
                            ba.Role = ddlrole.SelectedValue;
                            ba.Siteidnew = ""; //ptw role
                        }
                        if (ddlproject.Text == "PTW" && ddlrole0.SelectedIndex != 0)
                        {
                            ba.Stepid = 2;
                            ba.Role = "";//ciat role
                            ba.Siteidnew = ddlrole0.SelectedValue;
                        }
                        if (ddlproject.Text == "BOTH" && ddlrole.SelectedIndex != 0 && ddlrole0.SelectedIndex != 0)
                        {
                            ba.Stepid = 3;
                            ba.Role = ddlrole.SelectedValue; //ciat role
                            ba.Siteidnew = ddlrole0.SelectedValue; //ptw role

                        }


                        ba.Circle = ddlcircle.SelectedValue;
                        ba.Emailid = txtemailid.Text;
                        ba.Contactno = txtcontactno.Text;
                        ba.Imei = txtimei.Text;

                        ba.parent_company = ddlcompany.SelectedValue;
                        ba.sub_company = txtsubcompany.Text;


                        Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();

                        imageHeader.Add("JPG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });

                        byte[] header_p;
                        string fileExt_p = FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.') + 1).ToUpper();
                        string[] acceptedFileTypes = new string[1];

                        acceptedFileTypes[0] = ".JPG";

                        


                        string abc = FileUpload1.FileName;
                        abc = SpacialCharRemove.SpacialChar_Remove(abc);
                        if (abc == "" || FileUpload1.HasFile == false)
                        {
                            // Label1.Text = "Select file first.";
                            // Label1.Visible = true;
                            flag = 0;
                        }
                       
                        else
                        {
                            try
                            {
                                byte[] tmp_k = imageHeader[fileExt_p];
                                header_p = new byte[tmp_k.Length];
                                FileUpload1.FileContent.Read(header_p, 0, header_p.Length);
                                if (CompareArray(tmp_k, header_p))
                                {
                                    FileUpload1.SaveAs(Server.MapPath("~/signatures/") + abc);
                                    string path = Server.MapPath("~/signatures/") + abc;
                                    mydataaccess1 da1 = new mydataaccess1();
                                    da1.ptw_insert_into_signature(txtuser.Text, ddlcircle.Text, path);
                                    flag = 0;
                                }
                                else
                                {
                                    // mp1.Show();
                                    Label1.Text = "Select .jpg file only.";
                                    Label1.Visible = true;
                                    flag++;
                                }
                            }
                            catch
                            {
                                // mp1.Show();
                                Label1.Text = "Upload wrong file format,Select .jpg File...";
                                Label1.Visible = true;
                                flag++;
                            }


                        }
                       // this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('flag-"+flag+"');", true);
 if (HiddenField1.Value != HiddenField2.Value)
                        {
                            flag = 3;
                        }
// this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('flag-"+flag+"');", true);
                        if (flag == 0)
                        {
                            mydataaccess1 da6 = new mydataaccess1();
                            int s = da6.insertnewuser_circle1(ba);
                            if (s == 4)
                            {
                                Label1.Visible = true;
                                Label1.Text = "User Already Exist";
                            }
                            else if (s == 3)
                            {
                                Label1.Visible = true;
                                Label1.Text = "User Added Succesfully";
                                //Label1.Text = "";
                                if (lbcircle.Items.Count > 0)
                                {
                                    for (int i = 0; i < lbcircle.Items.Count; i++)
                                    {
                                        mydataaccess1 da2 = new mydataaccess1();
                                        da2.insertcircle_user(txtuser.Text, lbcircle.Items[i].ToString());
                                    }
                                }
                                else
                                {
                                    mydataaccess1 da3 = new mydataaccess1();
                                    da3.insertcircle_user(txtuser.Text, ddlcircle.SelectedValue);

                                }
                                if (lbzone.Items.Count > 0)
                                {
                                    for (int i = 0; i < lbzone.Items.Count; i++)
                                    {
                                        mydataaccess1 da4 = new mydataaccess1();
                                        da4.insertzone_user(txtuser.Text, lbzone.Items[i].ToString());
                                    }
                                }
                                else
                                {
                                    if (ddlzone.SelectedIndex != 0)
                                    {
                                        mydataaccess1 da5 = new mydataaccess1();
                                        da5.insertzone_user(txtuser.Text, ddlzone.SelectedValue);
                                    }

                                }
                                txtuser.Text = "";
                                txtcontactno.Text = "";
                                txtemailid.Text = "";
                                ddlcircle.SelectedIndex = 0;

                                ddlrole.SelectedIndex = 0;
                                lbcircle.Items.Clear();
                                txtpass.Text = "";
                                ddlzone.SelectedIndex = 0;
                                lbzone.Items.Clear();
                                txtimei.Text = "";
                                txtsubcompany.Text = "";
                                ddlcompany.SelectedIndex = 0;

                            }
                            else if (s == 1)
                            {
                                Label1.Visible = true;
                                Label1.Text = "Email-ID Already Exist";
                            }
                        }
                        else if (flag == 3)
                        {
                          this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('" + SpacialCharRemove.XSS_Remove(flag.ToString()) + "');", true);
                            Label1.Visible = true;
                            Label1.Text = "Password does not match...";
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.Text = "User not create because Upload wrong file format,Select .jpg File...";
                        }

                    }
                    else {
                        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('Please select Sub Company');", true);
                    }
                }
                #endregion maincode
            }
        }

        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }

    protected void btnremove_Click(object sender, EventArgs e)
    {
        if (lbcircle.SelectedIndex != -1)
        {
            lberrorlb.Text = "";
            lbcircle.Items.Remove(lbcircle.SelectedValue);
        }
        else
        {
            lberrorlb.Text = "Select Any Circle from ListBox then Remove.";
        }
    }
    protected void ddlcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcircle.SelectedIndex != 0)
        {
            if (ddlrole.SelectedValue == "Zonal Manager")
            {
                lbcircle.Enabled = false;
                lbzone.Enabled = true;
                da = new mydataaccess1();
                ba = new myvodav2();
                dt = new DataTable();
                ba.Circle = ddlcircle.SelectedValue;
                dt = da.getzonesbycirclename(ba);
                ddlzone.DataTextField = "zone";
                ddlzone.DataSource = dt;
                ddlzone.DataBind();
                ddlzone.Items.Insert(0, "---Select---");
            }
            else
            {
                int flag = 0;
                for (int i = 0; i < lbcircle.Items.Count; i++)
                {
                    if (lbcircle.Items[i].ToString() == ddlcircle.SelectedValue)
                    {
                        flag = 1;
                    }
                }
                if (flag == 0)
                {
                    lbcircle.Items.Add(ddlcircle.SelectedValue);
                }
            }
        }
    }
    protected void ddlproject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlproject.SelectedIndex != 0)
        {
            if (ddlproject.Text == "CIAT")
            {
                ddlrole.Enabled = true;
                ddlrole0.Enabled = false;
            }
            if (ddlproject.Text == "PTW")
            {
                ddlrole.Enabled = false;
                ddlrole0.Enabled = true;
                trcirclelb.Visible = true;
            }
            if (ddlproject.Text == "BOTH")
            {
                ddlrole.Enabled = true;
                ddlrole0.Enabled = true;
                trcirclelb.Visible = true;
            }
        }
        else
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('Please select Project');", true);
        }
    }

    protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcompany.SelectedIndex != 0)
        {
            //if (ddlproject.Text == "CIAT")
            //{
            //    ddlrole.Enabled = true;
            //    ddlrole0.Enabled = false;
            //}
            //if (ddlproject.Text == "PTW")
            //{
            //    ddlrole.Enabled = false;
            //    ddlrole0.Enabled = true;
            //    trcirclelb.Visible = true;
            //}
            //if (ddlproject.Text == "BOTH")
            //{
            //    ddlrole.Enabled = true;
            //    ddlrole0.Enabled = true;
            //    trcirclelb.Visible = true;
            //}
        }
        else
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('Please select Company');", true);
        }
    }
    protected void lbcircle_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlrole.SelectedValue == "Zonal Manager")
        {
            trcirclelb.Visible = false;
            trzone.Visible = true;
            trzonelb.Visible = true;
        }
        else
        {
            trcirclelb.Visible = true;
            trzonelb.Visible = false;
            trzone.Visible = false;
        }
        if (ddlrole.SelectedValue == "Support")
        {
            ddlrole0.Enabled = true;
            ddlrole0.SelectedValue = "Support";
            ddlproject.SelectedValue = "BOTH";
        }
        if (ddlrole0.SelectedValue == "Support")
        {
            if (ddlrole.SelectedValue != "Support")
            {
                Label1.Text = "Select Both Role Support";
                Label1.Visible = true;
            }
            else
            {
                Label1.Text = "";
                Label1.Visible = false;
            }
        }
        else
        {
            Label1.Text = "";
            Label1.Visible = false;
        }
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlzone.SelectedIndex != 0)
        {
            int flag = 0;
            for (int i = 0; i < lbzone.Items.Count; i++)
            {
                if (lbzone.Items[i].ToString() == ddlzone.SelectedValue)
                {
                    flag = 1;
                }
            }
            if (flag == 0)
            {
                lbzone.Items.Add(ddlzone.SelectedValue);
            }
        }
    }
    protected void btnzoneremove_Click(object sender, EventArgs e)
    {
        if (lbzone.SelectedIndex != -1)
        {
            lbzoneerror.Text = "";
            lbzone.Items.Remove(lbzone.SelectedValue);
        }
        else
        {
            lbzoneerror.Text = "Select Any Zone from ListBox then Remove.";
        }
    }
    protected void ddlrole0_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlrole.SelectedValue == "Zonal Manager")
        {
            trcirclelb.Visible = false;
            trzone.Visible = true;
            trzonelb.Visible = true;
        }
        else
        {
            trcirclelb.Visible = true;
            trzonelb.Visible = false;
            trzone.Visible = false;
        }
        if (ddlrole0.SelectedValue == "Support")
        {
            ddlrole.Enabled = true;
            ddlrole.SelectedValue = "Support";
            ddlproject.SelectedValue = "BOTH";
        }
        if (ddlrole.SelectedValue == "Support")
        {
            if (ddlrole0.SelectedValue != "Support")
            {
                Label1.Text = "Select Both Role Support";
                Label1.Visible = true;
            }
            else
            {
                Label1.Text = "";
                Label1.Visible = false;
            }
        }
        else
        {
            Label1.Text = "";
            Label1.Visible = false;
        }
    }
}
