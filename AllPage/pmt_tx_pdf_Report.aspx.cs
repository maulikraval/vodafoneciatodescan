﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using dataaccesslayer2;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.fonts;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.IO;
using mybusiness;

public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da1;
    mydataaccess2 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    HSSFWorkbook hssfworkbook;
    //vodabal ba;
    int cat;
    string ptw;
    //int ptwid = 0;
    iTextSharp.text.Document doc;
    string ptwid;
    string ptwid1;

    DataTable dt_site;
    iTextSharp.text.Font verdana;
    Phrase p2;
    Phrase p1_mahesh;
    Chunk titleChunk;
    PdfTemplate template;
    BaseFont bf = null;
    iTextSharp.text.Image footer;
    string pdfFilePath;
    string file;
    string imag_file1;
    string imag_file2;
    string imag_file3;
    string vlogo;
    string sp_user_d;
    iTextSharp.text.Rectangle rec;
    string siteid = "";
    int flag = 0;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //  lblError.Visible = false;
        try
        {

            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            da1 = new mydataaccess1();
            sp_user_d = da1.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);


            if (!IsPostBack)
            {
                try
                {
                    ddlCheckSheet.Visible = false;
                    ddlprovidername.Visible = false;
                    Label13.Visible = false;
                    Label7.Visible = false;
                    ImageButton1.Visible = false;
                    //  ImageButton2.Visible = false;
                    int i = 12;
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString() == sp_user_d)
                    {

                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "4")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da1= new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    /*    da1 = new mydataaccess1();
                        DataTable dtv5 = new DataTable();
                        dtv5 = da1.reminder_20_days_circle_admin(sp_user_d);
                        da1 = new mydataaccess1();
                        DataTable dtv2 = new DataTable();
                        dtv2 = da1.reminder_20_days_circle_admin_v2(sp_user_d);
                        da1 = new mydataaccess1();
                        DataTable dtv2ms = new DataTable();
                        dtv2ms = da1.reminder_20_days_circle_admin_v2ms(sp_user_d);
                        count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                        // critical points
                        da1 = new mydataaccess1();
                        DataTable cv2 = new DataTable();
                        cv2 = da1.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                        da1 = new mydataaccess1();
                        DataTable cv5 = new DataTable();
                        cv5 = da1.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                        da1 = new mydataaccess1();
                        DataTable cv2ms = new DataTable();
                        cv2ms = da1.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                        count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                        da1 = new mydataaccess1();
                        DataTable dtv51 = new DataTable();
                        dtv51 = da1.reminder_10_days_circle_admin(sp_user_d);
                        da1 = new mydataaccess1();
                        DataTable dtv21 = new DataTable();
                        dtv21 = da1.reminder_10_days_circle_admin_v2(sp_user_d);
                        da1 = new mydataaccess1();
                        DataTable dtv2ms1 = new DataTable();
                        dtv2ms1 = da1.reminder_10_days_circle_admin_v2ms(sp_user_d);
                        count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                        lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
                        */
                    //Marquee End

                    //if (Session.Count != 0)
                    {
                        if (Session["role"].ToString() == "12")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da1 = new mydataaccess1();
                                sp_user_d = da1.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                                if (lblusername.Text == "")
                                {
                                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }

                                da1 = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da1.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                //drpcircle.Items.Insert(1, "All");
                            }
                        }

                        if (Session["role"].ToString() == "2")
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu;
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        Response.ClearHeaders();

        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }




    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }


    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (drpcircle.SelectedIndex != 0 && ddlCheckSheet.SelectedIndex != 0 && ddlprovidername.SelectedIndex != 0)
        {
            DateTime Date1 = Convert.ToDateTime(TextBox1.Text);
            DateTime Date2 = Convert.ToDateTime(TextBox2.Text);
            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.select_sites_pdf(drpcircle.SelectedValue, ddlprovidername.SelectedItem.Text, Convert.ToInt32(ddlCheckSheet.SelectedValue), Date1, Date2);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gr = GridView1.SelectedRow;
        siteid = gr.Cells[1].Text;
        string sheet = gr.Cells[6].Text;


        verdana = FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.RED);
        Font verdana_small = FontFactory.GetFont("Verdana", 7, Font.NORMAL, BaseColor.BLACK);

        Font calibri_small = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);
        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);


        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.select_sites_data_pdf(siteid, sheet);


        int i_counter = 0;
        if (sheet.ToLower() == "2w4w" || sheet.ToLower() == "txequip")
        {
            i_counter = Convert.ToInt32(dt.Rows[0][4].ToString()) - 1;
        }
        else
        {
            i_counter = Convert.ToInt32(dt.Rows[0][7].ToString()) - 1;
        }
        if (dt.Rows.Count != 0)
        {
            Random rnd = new Random();
            int Random = rnd.Next(1, 100);
            if (sheet.ToLower() == "2w4w" || sheet.ToLower() == "txequip")
            {
                file = SpacialCharRemove.SpacialChar_Remove(siteid) + "1_" + dt.Rows[0][5].ToString().Substring(0, 10).Replace('/', '_') + "_" + Random + ".pdf";
            }
            else
            {
                file = SpacialCharRemove.SpacialChar_Remove(siteid) + "1_" + dt.Rows[0][8].ToString().Substring(0, 10).Replace('/', '_') + "_" + Random + ".pdf";
            }
            doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 40f, 40f, 30f, 40f);
            //pdfFilePath = "F:\\Vodafone\\ciat_hsw\\hsw_final_08_10_2014\\pdf_files\\" + file;
            pdfFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\pdf_files\\" + file.Trim();
            //  pdfFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\pdf_files\\" + file.Trim();
            //pdfFilePath = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + file.Trim();


            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(pdfFilePath, FileMode.Create));
            //writer.GetVerticalPosition(true);


            doc.Open();
            try
            {


                rec = doc.PageSize;

                v2_v5_v2ms_UBR(doc, sheet.ToUpper(), i_counter);

                //steps
                da = new mydataaccess2();
                dt = new DataTable();
                dt = da.select_categories_by_sheet(sheet);

                PdfPTable data = new PdfPTable(4);
                data.WidthPercentage = 100f;
                data.SetWidths(new float[] { 1, 4, 1, 1 });
                PdfPCell cell = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                // cell.Width = 20;
                data.AddCell(cell);
                PdfPCell cell1_ = new PdfPCell(new Phrase("Description", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                // cell1_.Width = 40;
                data.AddCell(cell1_);


                PdfPCell cell4_ = new PdfPCell(new Phrase("Answer", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                //cell4_.Width = 20;
                data.AddCell(cell4_);

                PdfPCell cell5_ = new PdfPCell(new Phrase("Comments / Explanations", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                //cell5_.Width = 20;
                data.AddCell(cell5_);


                /*   da = new mydataaccess2();
                   dt = new DataTable();
                   dt = da.select_categories_by_sheet(sheet);*/

                for (int j = 0; j < 3; j++)
                {
                    flag = 0;
                    PdfPCell cell0 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell0.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                    // cell.Width = 20;
                    data.AddCell(cell0);
                    PdfPCell cell10_ = new PdfPCell(new Phrase(dt.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell10_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                    // cell1_.Width = 40;
                    data.AddCell(cell10_);


                    PdfPCell cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                    //cell4_.Width = 20;
                    data.AddCell(cell40_);
                    PdfPCell cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                    //cell5_.Width = 20;
                    data.AddCell(cell50_);


                    da = new mydataaccess2();
                    DataTable dt1 = new DataTable();
                    dt1 = da.select_site_que_log_pdf(sheet, siteid, i_counter, j + 1);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");

                    int sec = 0;
                    for (int k = 0; k < dt1.Rows.Count; k++)
                    {
                        if (j == 2)
                        {

                            if (dt1.Rows[k][1].ToString() == "Number of GSM Antenna")
                            {
                                sec++;
                                PdfPCell cell00 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                // cell.Width = 20;
                                data.AddCell(cell00);
                                PdfPCell cell100_ = new PdfPCell(new Phrase("Sector -" + sec, new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                // cell1_.Width = 40;
                                data.AddCell(cell100_);

                                if (dt1.Rows[k][3].ToString() == "-1")
                                {
                                    PdfPCell cell400_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell400_);
                                }
                                else
                                {
                                    PdfPCell cell400_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell400_);
                                }


                                PdfPCell cell500_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                                data.AddCell(cell500_);
                                // flag++;

                                PdfPCell cell000 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell000.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                // cell.Width = 20;
                                data.AddCell(cell000);
                                PdfPCell cell1000_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell1000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                // cell1_.Width = 40;
                                data.AddCell(cell1000_);

                                if (dt1.Rows[k][3].ToString() == "-1")
                                {
                                    PdfPCell cell4000_ = new PdfPCell(new Phrase("NA", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell4000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell4000_);
                                }
                                else
                                {
                                    PdfPCell cell4000_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell4000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell4000_);
                                }


                                PdfPCell cell5000_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell5000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell5000_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell5000_.HorizontalAlignment = Element.ALIGN_CENTER;
                                data.AddCell(cell5000_);
                                // flag++;
                            }
                            else
                            {
                                if (dt1.Rows[k][1].ToString() == "Longitude")
                                {
                                    PdfPCell cell00 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    // cell.Width = 20;
                                    data.AddCell(cell00);
                                }
                                else
                                {
                                    PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    // cell.Width = 20;
                                    data.AddCell(cell00);
                                }
                                PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                // cell1_.Width = 40;
                                data.AddCell(cell100_);

                                if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                                {
                                    PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                    cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell400_);
                                }
                                else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                                {
                                    PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                    cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell400_);
                                }
                                else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                                {
                                    PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                    cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell400_);
                                }
                                else
                                {
                                    PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                    cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                    //cell4_.Width = 20;
                                    data.AddCell(cell400_);
                                }

                                PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                                data.AddCell(cell500_);
                                // flag++;
                            }
                        }
                        else
                        {
                            if (dt1.Rows[k][1].ToString() == "Longitude")
                            {
                                PdfPCell cell00 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                // cell.Width = 20;
                                data.AddCell(cell00);
                            }
                            else
                            {
                                PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                // cell.Width = 20;
                                data.AddCell(cell00);
                            }
                            PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                            cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                            // cell1_.Width = 40;
                            data.AddCell(cell100_);

                            if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }
                            else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }
                            else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }
                            else
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }

                            PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                            cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                            //cell5_.Width = 20;
                            //    cell500_.Rowspan = dt1.Rows.Count;
                            cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                            data.AddCell(cell500_);
                            // flag++;

                        }


                    }
                    if (j == dt.Rows.Count - 1)
                    {

                        if (sheet.ToLower() == "2w4w")
                        { }

                    }

                }
                doc.Add(data);


                doc.NewPage();
                da = new mydataaccess2();
                dt = new DataTable();
                dt = da.select_categories_by_sheet(sheet);


                for (int m = 0; m < 3; m++)
                {
                    da = new mydataaccess2();
                    DataTable dt_photo = new DataTable();
                    if (sheet.ToLower() == "cic v1")
                    {
                        dt_photo = da.select_photos_pdf(siteid, i_counter, m, sheet);
                    }
                    else
                    {
                        dt_photo = da.select_photos_pdf(siteid, i_counter, m + 1, sheet);
                    }

                    if (dt_photo.Rows.Count > 0)//------------------------------------
                    {
                        if (m == 0)
                        {
                            PdfPTable new_t1 = new PdfPTable(1);

                            new_t1.WidthPercentage = 100f;

                            Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                             Font.BOLD));

                            PdfPCell new_cell_ = new PdfPCell(head_ph_);
                            new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                            new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                            new_cell_.FixedHeight = 20f;
                            new_t1.AddCell(new_cell_);
                            Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                            ));

                            doc.Add(new_t1);
                            doc.Add(break2);
                        }
                        if (dt_photo.Rows.Count > 0)
                        {
                            PdfPTable new_t = new PdfPTable(1);
                            new_t.WidthPercentage = 100f;

                            Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                             Font.BOLD));

                            PdfPCell new_cell = new PdfPCell(head_ph);
                            new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            new_cell.FixedHeight = 12f;
                            new_t.AddCell(new_cell);
                            doc.Add(new_t);

                            PdfPTable photo = new PdfPTable(4);
                            PdfPCell photo_cell = new PdfPCell();
                            photo.WidthPercentage = 100;
                            int flag = 0;
                            for (int p = 0; p < dt_photo.Rows.Count; p++)
                            {
                                if (flag == 4)
                                {
                                    flag = 0;
                                }
                                try
                                {
                                    if (dt_photo.Rows.Count % 4 == 0)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                    }
                                    else
                                    {
                                        if (p == dt_photo.Rows.Count - 1)
                                        {
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                            imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);
                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            // photo_cell.FixedHeight = 220;
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                            photo.AddCell(photo_cell);

                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                            flag++;

                                            for (int b = 0; b < 4 - flag; b++)
                                            {
                                                photo_cell = new PdfPCell();
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                photo.AddCell(photo_cell);
                                            }


                                        }
                                        else
                                        {
                                            // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                            imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);
                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            //  photo_cell.FixedHeight = 220;
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                            flag++;
                                        }
                                    }
                                }
                                catch
                                {
                                    photo_cell = new PdfPCell();
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                }
                            }
                            doc.Add(photo);
                            Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                            doc.Add(break23);
                        }

                    }//---------------------------

                }
                ////-----Nodeid wise
                mydataaccess1 da2 = new mydataaccess1();
                DataTable dt_node = new DataTable();
                dt_node = da2.select_nodes_from_nss(siteid);
                string nodeid = "";
                string checkstatus = "";
                //  Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                for (int d = 0; d < dt_node.Rows.Count; d++)
                {

                    PdfPTable head = new PdfPTable(1);
                    head.WidthPercentage = 100f;
                    nodeid = dt_node.Rows[d][0].ToString();
                    checkstatus = dt_node.Rows[d][1].ToString();
                    #region Fine Site Count
                    da1 = new mydataaccess1();
                    DataTable pm_findsitecount = new DataTable();
                    pm_findsitecount = da1.pm_findcount_tx(siteid, "TxEquip", nodeid);
                    string AllCount = "";
                    if (pm_findsitecount.Rows.Count > 0)
                    {
                        AllCount = " Date Of Active PM : " + pm_findsitecount.Rows[0][2].ToString() + " " + pm_findsitecount.Rows[0][1].ToString();
                        AllCount += " Done By : " + pm_findsitecount.Rows[0][3].ToString();
                        AllCount += " Contact Number : " + pm_findsitecount.Rows[0][4].ToString();
                        AllCount += " Next Due Date : " + pm_findsitecount.Rows[0][5].ToString();
                        AllCount += " Assigned To : " + pm_findsitecount.Rows[0][6].ToString();

                    }
                    #endregion
                    if (checkstatus == "1")
                    {

                        PdfPCell blank = new PdfPCell(new Phrase(""));
                        blank.Colspan = 2;
                        blank.FixedHeight = 15f;
                        doc.Add(blank);


                        Phrase p1header = new Phrase(nodeid, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
                        PdfPCell c = new PdfPCell(p1header);
                        c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        c.HorizontalAlignment = Element.ALIGN_CENTER;
                        c.VerticalAlignment = Element.ALIGN_MIDDLE;
                        c.BorderColor = iTextSharp.text.BaseColor.BLACK;
                        c.FixedHeight = 20f;
                        head.AddCell(c);
                        doc.Add(head);
                    }
                    else
                    {
                        doc.NewPage();
                        /*  Phrase p1header = new Phrase(nodeid, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
                         PdfPCell c = new PdfPCell(p1header);
                          // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#DB2120"));
                          c.HorizontalAlignment = Element.ALIGN_CENTER;
                          c.VerticalAlignment = Element.ALIGN_MIDDLE;
                          c.BorderColor = iTextSharp.text.BaseColor.BLACK;
                          c.FixedHeight = 20f;
                          head.AddCell(c);
                          doc.Add(head);*/
                        data = new PdfPTable(4);
                        data.WidthPercentage = 100f;
                        data.SetWidths(new float[] { 1, 4, 1, 1 });
                        Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
                        PdfPCell cell1_count = new PdfPCell(new Phrase(nodeid.ToString(), new Font(Site_Heading)));
                        cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_count);

                        PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                        cell1_Specount.Colspan = 3;
                        cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_Specount);

                        PdfPCell blank = new PdfPCell(new Phrase(""));
                        blank.Colspan = 2;
                        blank.FixedHeight = 15f;


                        doc.Add(blank);

                        da = new mydataaccess2();
                        dt = new DataTable();
                        dt = da.select_categories_by_sheet(sheet);


                        cell = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                        cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        // cell.Width = 20;
                        data.AddCell(cell);
                        cell1_ = new PdfPCell(new Phrase("Description", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                        cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        // cell1_.Width = 40;
                        data.AddCell(cell1_);


                        cell4_ = new PdfPCell(new Phrase("Answer", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                        cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell4_.Width = 20;
                        data.AddCell(cell4_);

                        cell5_ = new PdfPCell(new Phrase("Comments / Explanations", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                        cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell5_.Width = 20;
                        data.AddCell(cell5_);
                        //doc.Add(data);

                        /*   da = new mydataaccess2();
                           dt = new DataTable();
                           dt = da.select_categories_by_sheet(sheet);*/

                        for (int j = 3; j < dt.Rows.Count; j++)
                        {
                            flag = 0;
                            PdfPCell cell0 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                            cell0.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                            // cell.Width = 20;
                            data.AddCell(cell0);
                            PdfPCell cell10_ = new PdfPCell(new Phrase(dt.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                            cell10_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                            // cell1_.Width = 40;
                            data.AddCell(cell10_);


                            PdfPCell cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                            cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                            //cell4_.Width = 20;
                            data.AddCell(cell40_);
                            PdfPCell cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                            cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                            //cell5_.Width = 20;
                            data.AddCell(cell50_);


                            da = new mydataaccess2();
                            DataTable dt1 = new DataTable();
                            dt1 = da.select_site_que_log_pdf_tx(sheet, siteid, i_counter, j + 1, nodeid);


                            int sec = 0;
                            for (int k = 0; k < dt1.Rows.Count; k++)
                            {
                                if (j == 2)
                                {

                                    if (dt1.Rows[k][1].ToString() == "Number of GSM Antenna")
                                    {
                                        sec++;
                                        PdfPCell cell00 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                        // cell.Width = 20;
                                        data.AddCell(cell00);
                                        PdfPCell cell100_ = new PdfPCell(new Phrase("Sector -" + sec, new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell100_);

                                        if (dt1.Rows[k][3].ToString() == "-1")
                                        {
                                            PdfPCell cell400_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                            cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell400_);
                                        }
                                        else
                                        {
                                            PdfPCell cell400_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                            cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell400_);
                                        }


                                        PdfPCell cell500_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#C0C0C0"));
                                        //cell5_.Width = 20;
                                        //    cell500_.Rowspan = dt1.Rows.Count;
                                        cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                                        data.AddCell(cell500_);
                                        // flag++;

                                        PdfPCell cell000 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell000.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                        // cell.Width = 20;
                                        data.AddCell(cell000);
                                        PdfPCell cell1000_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell1000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell1000_);

                                        if (dt1.Rows[k][3].ToString() == "-1")
                                        {
                                            PdfPCell cell4000_ = new PdfPCell(new Phrase("NA", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                            cell4000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell4000_);
                                        }
                                        else
                                        {
                                            PdfPCell cell4000_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                            cell4000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell4000_);
                                        }


                                        PdfPCell cell5000_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell5000_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                        //cell5_.Width = 20;
                                        //    cell500_.Rowspan = dt1.Rows.Count;
                                        cell5000_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        cell5000_.HorizontalAlignment = Element.ALIGN_CENTER;
                                        data.AddCell(cell5000_);
                                        // flag++;
                                    }
                                    else
                                    {
                                        PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                        // cell.Width = 20;
                                        data.AddCell(cell00);
                                        PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell100_);

                                        if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                                        {
                                            PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                            cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell400_);
                                        }
                                        else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                                        {
                                            PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                            cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell400_);
                                        }
                                        else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                                        {
                                            PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                            cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell400_);
                                        }
                                        else
                                        {
                                            PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                            cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                            //cell4_.Width = 20;
                                            data.AddCell(cell400_);
                                        }


                                        PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                        cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                        //cell5_.Width = 20;
                                        //    cell500_.Rowspan = dt1.Rows.Count;
                                        cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                                        data.AddCell(cell500_);
                                        // flag++;
                                    }
                                }
                                else
                                {
                                    PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    // cell.Width = 20;
                                    data.AddCell(cell00);
                                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    // cell1_.Width = 40;
                                    data.AddCell(cell100_);

                                    if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                                    {
                                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                                        //cell4_.Width = 20;
                                        data.AddCell(cell400_);
                                    }
                                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                                    {
                                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                                        //cell4_.Width = 20;
                                        data.AddCell(cell400_);
                                    }
                                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                                    {
                                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                                        //cell4_.Width = 20;
                                        data.AddCell(cell400_);
                                    }
                                    else
                                    {
                                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                        //cell4_.Width = 20;
                                        data.AddCell(cell400_);
                                    }


                                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                                    //cell5_.Width = 20;
                                    //    cell500_.Rowspan = dt1.Rows.Count;
                                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                                    data.AddCell(cell500_);
                                    // flag++;

                                }


                            }



                        }
                        doc.Add(data);


                        for (int m = 3; m < dt.Rows.Count; m++)
                        {
                            da = new mydataaccess2();
                            DataTable dt_photo = new DataTable();

                            dt_photo = da.select_photos_pdf_tx(siteid, i_counter, m + 1, sheet, nodeid);


                            if (dt_photo.Rows.Count > 0)//------------------------------------
                            {
                                if (m == 0)
                                {
                                    PdfPTable new_t1 = new PdfPTable(1);

                                    new_t1.WidthPercentage = 100f;

                                    Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                                     Font.BOLD));

                                    PdfPCell new_cell_ = new PdfPCell(head_ph_);
                                    new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                    new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                    new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                                    new_cell_.FixedHeight = 20f;
                                    new_t1.AddCell(new_cell_);
                                    Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                                    ));

                                    doc.Add(new_t1);
                                    doc.Add(break2);
                                }
                                if (dt_photo.Rows.Count > 0)
                                {
                                    PdfPTable new_t = new PdfPTable(1);
                                    new_t.WidthPercentage = 100f;

                                    Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                                     Font.BOLD));

                                    PdfPCell new_cell = new PdfPCell(head_ph);
                                    new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                    new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                    new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    new_cell.FixedHeight = 12f;
                                    new_t.AddCell(new_cell);
                                    doc.Add(new_t);

                                    PdfPTable photo = new PdfPTable(4);
                                    PdfPCell photo_cell = new PdfPCell();
                                    photo.WidthPercentage = 100;
                                    int flag = 0;
                                    for (int p = 0; p < dt_photo.Rows.Count; p++)
                                    {
                                        if (flag == 4)
                                        {
                                            flag = 0;
                                        }
                                        try
                                        {
                                            if (dt_photo.Rows.Count % 4 == 0)
                                            {
                                                PdfPTable pp = new PdfPTable(1);
                                                Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                                PdfPCell ppc = new PdfPCell(name);
                                                ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                                pp.AddCell(ppc);
                                                //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                                imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                iTextSharp.text.Image image_photo;
                                                image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                                image_photo.SetAbsolutePosition(50, 50);
                                                image_photo.ScaleAbsolute(70f, 70f);
                                                pp.AddCell(image_photo);
                                                photo_cell = new PdfPCell(pp);
                                                photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                                //  photo_cell.FixedHeight = 220;
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                photo.AddCell(photo_cell);
                                            }
                                            else
                                            {
                                                if (p == dt_photo.Rows.Count - 1)
                                                {
                                                    PdfPTable pp = new PdfPTable(1);
                                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                                    PdfPCell ppc = new PdfPCell(name);
                                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                                    pp.AddCell(ppc);
                                                    // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                                    imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    iTextSharp.text.Image image_photo;
                                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                                    image_photo.SetAbsolutePosition(50, 50);
                                                    image_photo.ScaleAbsolute(70f, 70f);
                                                    pp.AddCell(image_photo);
                                                    photo_cell = new PdfPCell(pp);
                                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                                    // photo_cell.FixedHeight = 220;
                                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                                    photo.AddCell(photo_cell);

                                                    photo_cell = new PdfPCell();
                                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                    photo.AddCell(photo_cell);
                                                    flag++;

                                                    for (int b = 0; b < 4 - flag; b++)
                                                    {
                                                        photo_cell = new PdfPCell();
                                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                        photo.AddCell(photo_cell);
                                                    }


                                                }
                                                else
                                                {
                                                    // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    PdfPTable pp = new PdfPTable(1);
                                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                                    PdfPCell ppc = new PdfPCell(name);
                                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                                    pp.AddCell(ppc);
                                                    // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                                    imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                    iTextSharp.text.Image image_photo;
                                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                                    image_photo.SetAbsolutePosition(50, 50);
                                                    image_photo.ScaleAbsolute(70f, 70f);
                                                    pp.AddCell(image_photo);
                                                    photo_cell = new PdfPCell(pp);
                                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                                    //  photo_cell.FixedHeight = 220;
                                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                    photo.AddCell(photo_cell);
                                                    flag++;
                                                }
                                            }
                                        }
                                        catch
                                        {
                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                        }
                                    }
                                    doc.Add(photo);
                                    Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                                    doc.Add(break23);
                                }

                            }//---------------------------

                        }
                    }//----
                }
                //-----------------Node complete

                car(doc, sheet, i_counter);
                //doc.Close();
            }


            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

                doc.Close();

            }

            string strUserAgent = Request.UserAgent.ToString().ToLower();
            if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iphone") ||
    strUserAgent.Contains("blackberry") || strUserAgent.Contains("mobile") ||
    strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera mini") ||
    strUserAgent.Contains("palm"))
            {
                Response.ContentType = "Application/pdf";

                Response.AppendHeader("Content-Disposition", "attachment; filename=" + file);

                Response.TransmitFile(Server.MapPath("~/pdf_files/" + SpacialCharRemove.SpacialChar_Remove(ptwid1) + ".pdf"));

                Response.End();
                //           Response.Redirect("https://ciat.vodafone.in/report_ptw/height_pdf/" + file);
            }
            else
            {

                if (Session["imei"] == null)
                {



                    Response.ContentType = "Application/pdf";

                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + file);
                    // Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_08_10_2014/pdf_files/" + file);
                    //Response.TransmitFile("E:/Vodafone_New/PTW-Testing/pdf_files/" + file);
                    Response.TransmitFile("D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/pdf_files/" + file);
                    Response.End();
                }
                else
                {

                    Response.ContentType = "Application/pdf";

                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + file);
                    //Response.TransmitFile("E:/Vodafone_New/PTW_Testing/pdf_files/" + file);
                    Response.TransmitFile("D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/pdf_files/" + file);
                    // Response.TransmitFile("F:/Vodafone/" + file);

                    Response.End();
                    //Response.Redirect("https://ciat.vodafone.in/report_ptw/height_pdf/" + file);

                    //}


                }
            }
        }
    }
    private void car(Document doc, string sheet, int i_counter)
    {
        DataTable dt1 = new DataTable();
        PdfPTable data = new PdfPTable(6);
        data.WidthPercentage = 100f;
        data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });

        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        ////-----Nodeid wise
        mydataaccess1 da2 = new mydataaccess1();
        DataTable dt_node = new DataTable();
        dt_node = da2.select_nodes_from_nss_car_nodes(siteid);
        string nodeid = "";
        string checkstatus = "";
        for (int d = 0; d < dt_node.Rows.Count; d++)
        {

            PdfPTable head = new PdfPTable(1);
            head.WidthPercentage = 100f;
            nodeid = dt_node.Rows[d][0].ToString();
            doc.NewPage();

            if (d == 0)
            {
                PdfPTable head1 = new PdfPTable(1);
                head1.WidthPercentage = 100f;
                Phrase p1header1 = new Phrase("Corrective Action Report", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD));
                PdfPCell c1 = new PdfPCell(p1header1);
                c1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#DB2120"));
                c1.HorizontalAlignment = Element.ALIGN_CENTER;
                c1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1.FixedHeight = 20f;
                head1.AddCell(c1);
                doc.Add(head1);


                Phrase p1headersite = new Phrase("Common Site Details", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD));
                PdfPCell cheader = new PdfPCell(p1headersite);
                // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#DB2120"));
                cheader.HorizontalAlignment = Element.ALIGN_CENTER;
                cheader.VerticalAlignment = Element.ALIGN_MIDDLE;
                cheader.BorderColor = iTextSharp.text.BaseColor.BLACK;
                cheader.FixedHeight = 20f;
                head.AddCell(cheader);
                PdfPCell blank = new PdfPCell(new Phrase(""));
                blank.Colspan = 2;
                blank.FixedHeight = 15f;


                doc.Add(head);
                doc.Add(blank);

                data = new PdfPTable(6);
                data.WidthPercentage = 100f;
                data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });
                PdfPCell cell = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                // cell.Width = 20;
                data.AddCell(cell);
                PdfPCell cell1_ = new PdfPCell(new Phrase("Question", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                // cell1_.Width = 40;
                data.AddCell(cell1_);


                PdfPCell cell4_ = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                //cell4_.Width = 20;
                data.AddCell(cell4_);

                PdfPCell cell5_ = new PdfPCell(new Phrase("Action Remarks", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                //cell5_.Width = 20;
                data.AddCell(cell5_);

                cell4_ = new PdfPCell(new Phrase("Violation Severity", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                //cell4_.Width = 20;
                data.AddCell(cell4_);

                cell5_ = new PdfPCell(new Phrase("Punch Point(External/Internal)", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                //cell5_.Width = 20;
                data.AddCell(cell5_);

                //	doc.Add(data);

                PdfPCell cell0 = new PdfPCell(new Phrase("3", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell0.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell.Width = 20;
                data.AddCell(cell0);
                PdfPCell cell10_ = new PdfPCell(new Phrase("Site Environment", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell10_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell1_.Width = 40;
                data.AddCell(cell10_);


                PdfPCell cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                PdfPCell cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);

                cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);


                da = new mydataaccess2();
                dt1 = new DataTable();
                dt1 = da.select_site_que_log_pdf_tx_car(sheet, siteid, i_counter, 3);
                for (int k = 0; k < dt1.Rows.Count; k++)
                {
                    PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell.Width = 20;
                    data.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell1_.Width = 40;
                    data.AddCell(cell100_);

                    if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }


                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    //cell5_.Width = 20;
                    //    cell500_.Rowspan = dt1.Rows.Count;
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    data.AddCell(cell500_);
                    // flag++;

                    PdfPCell cell4001 = new PdfPCell(new Phrase(dt1.Rows[k][6].ToString(), new Font(calibri_Data)));
                    cell4001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell4001);


                    PdfPCell cell5001 = new PdfPCell(new Phrase(dt1.Rows[k][7].ToString(), new Font(calibri_Data)));
                    cell5001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell5001);

                }
                doc.Add(data);
                da = new mydataaccess2();
                DataTable dt_photo = new DataTable();

                dt_photo = da.select_photos_pdf_tx_car(siteid, i_counter, 3, sheet, nodeid);


                if (dt_photo.Rows.Count > 0)
                {
                    PdfPTable new_t = new PdfPTable(1);
                    new_t.WidthPercentage = 100f;

                    Phrase head_ph = new Phrase("Photo", new Font(Font.FontFamily.HELVETICA, 7,
                     Font.BOLD));

                    PdfPCell new_cell = new PdfPCell(head_ph);
                    new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    new_cell.FixedHeight = 12f;
                    new_t.AddCell(new_cell);
                    doc.Add(new_t);

                    PdfPTable photo_site = new PdfPTable(4);
                    PdfPCell photo_cell = new PdfPCell();
                    photo_site.WidthPercentage = 100;
                    int flag = 0;
                    for (int p = 0; p < dt_photo.Rows.Count; p++)
                    {
                        if (flag == 4)
                        {
                            flag = 0;
                        }
                        try
                        {
                            if (dt_photo.Rows.Count % 4 == 0)
                            {
                                PdfPTable pp = new PdfPTable(1);
                                Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                PdfPCell ppc = new PdfPCell(name);
                                ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                pp.AddCell(ppc);
                                //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                iTextSharp.text.Image image_photo;
                                image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                image_photo.SetAbsolutePosition(50, 50);
                                image_photo.ScaleAbsolute(70f, 70f);
                                pp.AddCell(image_photo);
                                photo_cell = new PdfPCell(pp);
                                photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                //  photo_cell.FixedHeight = 220;
                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                photo_site.AddCell(photo_cell);
                            }
                            else
                            {
                                if (p == dt_photo.Rows.Count - 1)
                                {
                                    PdfPTable pp = new PdfPTable(1);
                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                    PdfPCell ppc = new PdfPCell(name);
                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pp.AddCell(ppc);
                                    // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                    imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    iTextSharp.text.Image image_photo;
                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                    image_photo.SetAbsolutePosition(50, 50);
                                    image_photo.ScaleAbsolute(70f, 70f);
                                    pp.AddCell(image_photo);
                                    photo_cell = new PdfPCell(pp);
                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    // photo_cell.FixedHeight = 220;
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                    photo_site.AddCell(photo_cell);

                                    photo_cell = new PdfPCell();
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo_site.AddCell(photo_cell);
                                    flag++;

                                    for (int b = 0; b < 4 - flag; b++)
                                    {
                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo_site.AddCell(photo_cell);
                                    }


                                }
                                else
                                {
                                    // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    PdfPTable pp = new PdfPTable(1);
                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                    PdfPCell ppc = new PdfPCell(name);
                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pp.AddCell(ppc);
                                    // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                    imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    iTextSharp.text.Image image_photo;
                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                    image_photo.SetAbsolutePosition(50, 50);
                                    image_photo.ScaleAbsolute(70f, 70f);
                                    pp.AddCell(image_photo);
                                    photo_cell = new PdfPCell(pp);
                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //  photo_cell.FixedHeight = 220;
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo_site.AddCell(photo_cell);
                                    flag++;
                                }
                            }
                        }
                        catch
                        {
                            photo_cell = new PdfPCell();
                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                            photo_site.AddCell(photo_cell);
                        }
                    }

                    doc.Add(photo_site);
                }



            }






            doc.NewPage();

            head = new PdfPTable(1);
            head.WidthPercentage = 100f;
            Phrase p1header = new Phrase(nodeid, new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD));
            PdfPCell c = new PdfPCell(p1header);
            // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#DB2120"));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.BorderColor = iTextSharp.text.BaseColor.BLACK;
            c.FixedHeight = 20f;
            head.AddCell(c);

            PdfPCell blank111 = new PdfPCell(new Phrase(""));
            blank111.Colspan = 2;
            blank111.FixedHeight = 15f;


            doc.Add(head);
            doc.Add(blank111);

            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.select_categories_by_sheet(sheet);

            data = new PdfPTable(6);
            data.WidthPercentage = 100f;
            data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });
            PdfPCell cell1111 = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell1111.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell.Width = 20;
            data.AddCell(cell1111);
            PdfPCell cell11_ = new PdfPCell(new Phrase("Question", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell11_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell1_.Width = 40;
            data.AddCell(cell11_);


            PdfPCell cell41_ = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell41_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell41_);

            PdfPCell cell51_ = new PdfPCell(new Phrase("Action Remarks", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell51_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell51_);

            cell41_ = new PdfPCell(new Phrase("Violation Severity", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell41_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell41_);

            cell51_ = new PdfPCell(new Phrase("Punch Point(External/Internal)", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell51_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell51_);



            /*   da = new mydataaccess2();
               dt = new DataTable();
               dt = da.select_categories_by_sheet(sheet);*/

            for (int j = 3; j < dt.Rows.Count; j++)
            {
                flag = 0;
                PdfPCell cell00 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell.Width = 20;
                data.AddCell(cell00);
                PdfPCell cell100_ = new PdfPCell(new Phrase(dt.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell1_.Width = 40;
                data.AddCell(cell100_);


                PdfPCell cell400_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell400_);
                PdfPCell cell500_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell500_);

                cell400_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell400_);
                cell500_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell500_);

                da1 = new mydataaccess1();
                DataTable dt_c = new DataTable();
                dt_c = da1.pm_GetinspectionCounter_tx(siteid, dt.Rows[j][0].ToString(), nodeid);
                if (dt_c.Rows.Count > 0)
                {
                    if (dt_c.Rows[0][0].ToString() != "")
                    {
                        i_counter = Convert.ToInt32(dt_c.Rows[0][0].ToString()) - 1;
                    }
                }

                //DataTable dt1 = new DataTable();
                if (j == 2)
                {
                    da = new mydataaccess2();
                    dt1 = new DataTable();
                    dt1 = da.select_site_que_log_pdf_tx_car(sheet, siteid, i_counter, j + 1);

                }
                else
                {
                    da = new mydataaccess2();
                    dt1 = new DataTable();
                    dt1 = da.select_site_que_log_pdf_tx_car(sheet, siteid, i_counter, j + 1, nodeid);
                }
                int sec = 0;
                for (int k = 0; k < dt1.Rows.Count; k++)
                {

                    PdfPCell cell900 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell900.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell.Width = 20;
                    data.AddCell(cell900);
                    PdfPCell cell9100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell9100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell1_.Width = 40;
                    data.AddCell(cell9100_);

                    if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell9400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell9400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell9400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell9400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell9400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell9400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell9400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell9400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell4_.Width = 20;
                        data.AddCell(cell9400_);
                    }
                    else
                    {
                        PdfPCell cell9400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell9400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        //cell4_.Width = 20;
                        data.AddCell(cell9400_);
                    }


                    PdfPCell cell9500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell9500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    //cell5_.Width = 20;
                    //    cell500_.Rowspan = dt1.Rows.Count;
                    cell9500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell9500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    data.AddCell(cell9500_);
                    // flag++;

                    PdfPCell cell94001 = new PdfPCell(new Phrase(dt1.Rows[k][6].ToString(), new Font(calibri_Data)));
                    cell94001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell94001);


                    PdfPCell cell95001 = new PdfPCell(new Phrase(dt1.Rows[k][7].ToString(), new Font(calibri_Data)));
                    cell95001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell95001);
                }

            }
            doc.Add(data);


            for (int m = 3; m < dt.Rows.Count; m++)
            {
                da = new mydataaccess2();
                DataTable dt_photo = new DataTable();

                dt_photo = da.select_photos_pdf_tx_car(siteid, i_counter, m + 1, sheet, nodeid);


                if (dt_photo.Rows.Count > 0)//------------------------------------
                {
                    if (m == 0)
                    {
                        PdfPTable new_t1 = new PdfPTable(1);

                        new_t1.WidthPercentage = 100f;

                        Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                         Font.BOLD));

                        PdfPCell new_cell_ = new PdfPCell(head_ph_);
                        new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                        new_cell_.FixedHeight = 20f;
                        new_t1.AddCell(new_cell_);
                        Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                        ));

                        doc.Add(new_t1);
                        doc.Add(break2);
                    }
                    if (dt_photo.Rows.Count > 0)
                    {
                        PdfPTable new_t = new PdfPTable(1);
                        new_t.WidthPercentage = 100f;

                        Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                         Font.BOLD));

                        PdfPCell new_cell = new PdfPCell(head_ph);
                        new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        new_cell.FixedHeight = 12f;
                        new_t.AddCell(new_cell);
                        doc.Add(new_t);

                        PdfPTable photo = new PdfPTable(4);
                        PdfPCell photo_cell = new PdfPCell();
                        photo.WidthPercentage = 100;
                        int flag = 0;
                        for (int p = 0; p < dt_photo.Rows.Count; p++)
                        {
                            if (flag == 4)
                            {
                                flag = 0;
                            }
                            try
                            {
                                if (dt_photo.Rows.Count % 4 == 0)
                                {
                                    PdfPTable pp = new PdfPTable(1);
                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                    PdfPCell ppc = new PdfPCell(name);
                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pp.AddCell(ppc);
                                    //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                    imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    iTextSharp.text.Image image_photo;
                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                    image_photo.SetAbsolutePosition(50, 50);
                                    image_photo.ScaleAbsolute(70f, 70f);
                                    pp.AddCell(image_photo);
                                    photo_cell = new PdfPCell(pp);
                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //  photo_cell.FixedHeight = 220;
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                }
                                else
                                {
                                    if (p == dt_photo.Rows.Count - 1)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        // photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                        photo.AddCell(photo_cell);

                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;

                                        for (int b = 0; b < 4 - flag; b++)
                                        {
                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                        }


                                    }
                                    else
                                    {
                                        // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;
                                    }
                                }
                            }
                            catch
                            {
                                photo_cell = new PdfPCell();
                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                photo.AddCell(photo_cell);
                            }
                        }
                        doc.Add(photo);
                        Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                        doc.Add(break23);
                    }

                }//---------------------------


            }//----
        }
        //-----------------Node complete
    }

    private void v2_v5_v2ms_UBR(Document doc, string sheet, int i_counter)
    {
        PdfPTable head = new PdfPTable(1);
        head.WidthPercentage = 100f;

        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });

        //head.DefaultCell.Border = 0;
        string headersheet = "";
        if (sheet == "UBR V1.1")
        {
            headersheet = "UBR - Site Inspection Checklist V1";
        }
        else if (sheet == "V5")
        {
            headersheet = "BSC / BTS / Repeater - Site Inspection Checklist v5.3";
        }
        else if (sheet == "V2")
        {
            headersheet = "IBS / ODM - Site Inspection Checklist V2.2";
        }
        else if (sheet == "V2MS")
        {
            headersheet = "MSC Site Inspection Checklist V3";
        }
        else if (sheet == "2W4W")
        {
            headersheet = "Vehicle Inspection Checklist 2W4W";
        }
        else if (sheet == "TXEQUIP")
        {
            headersheet = "Transport Equipment Checklist";
        }
        else
        {
            headersheet = "Cellsite Infra Inspection Checklist CIIC V1";
        }



        Phrase p1header = new Phrase(headersheet, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
        PdfPCell c = new PdfPCell(p1header);
        // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#DB2120"));
        c.HorizontalAlignment = Element.ALIGN_CENTER;
        c.VerticalAlignment = Element.ALIGN_MIDDLE;
        c.BorderColor = iTextSharp.text.BaseColor.BLACK;
        c.FixedHeight = 20f;
        head.AddCell(c);

        if (sheet == "2W4W" || sheet == "TXEQUIP")
        {
            string col = "NSS ID,Site Type,Name & Address of the site,Inspector Name & No.,Date of Inspection,ISPTW/Status,PTWID";
            string[] col_final = col.Split(',');
            // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });


            PdfPTable table1 = new PdfPTable(2);
            table1.WidthPercentage = 100f;
            //table1.SetWidths(new float[] { 4, 3, 4, 3 });

            da = new mydataaccess2();
            DataTable dt_new = new DataTable();
            dt_new = da.select_site_data_all_pdf(siteid, i_counter, sheet);


            for (int i = 0; i < 7; i++)
            {

                Phrase p1_b = new Phrase(col_final[i].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
                PdfPCell cell1 = new PdfPCell(p1_b);
                cell1.HorizontalAlignment = Element.ALIGN_LEFT;
                cell1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                // cell1.BackgroundColor.Brighter(GDI.Color.);


                Phrase p1_b1 = new Phrase(dt_new.Rows[0][i].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
                PdfPCell cell2 = new PdfPCell(p1_b1);
                cell2.HorizontalAlignment = Element.ALIGN_LEFT;
                cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));


                table1.AddCell(cell1);
                table1.AddCell(cell2);


            }
            PdfPCell note = new PdfPCell(new Phrase("Notes : Every positive answer gets full marks for the question and every negative answer would get Zero marks. Mark 'NA' if the question is Not Applicable for any particular site. Comment / explanation should be given for where a corrective action is requried. Weightage for different critical safety requirement is 2, 1 and 0.5. In case any activity description is not applicable for the Cellsite, then the points would be automatically deducted from the total score to calculate the achieved percentage. Every observation made on the site should be classified as A, B or C (A being most critical) depending on the criticality / hazard Potential (for more details refer procedure).The bold text questions imply basis of rejection at the time site acceptance from Infra Provider.", new Font(Font.FontFamily.TIMES_ROMAN, 7)));
            note.Colspan = 2;
            note.FixedHeight = 60f;
            note.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#04B4AE"));
            note.HorizontalAlignment = Element.ALIGN_CENTER;
            note.VerticalAlignment = Element.ALIGN_MIDDLE;
            //  table1.AddCell(note);

            PdfPCell blank = new PdfPCell(new Phrase(""));
            blank.Colspan = 2;
            blank.FixedHeight = 15f;
            table1.AddCell(blank);

            doc.Add(head);
            doc.Add(table1);
        }
        else
        {
            string col = "NSS ID,Site Type - GBT/RTT/RTP/None,Name & Address of the site,Site Incharge Name & No.,Inspector Name & No.,Date of Inspection,ISPTW,PTWID";
            string[] col_final = col.Split(',');
            // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });


            PdfPTable table1 = new PdfPTable(2);
            table1.WidthPercentage = 100f;
            //table1.SetWidths(new float[] { 4, 3, 4, 3 });

            da = new mydataaccess2();
            DataTable dt_new = new DataTable();
            dt_new = da.select_site_data_all_pdf(siteid, i_counter, sheet);


            for (int i = 0; i < 8; i++)
            {
                if (i == 6)
                {
                    Phrase p1_b = new Phrase(col_final[i].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
                    PdfPCell cell1 = new PdfPCell(p1_b);
                    cell1.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                    // cell1.BackgroundColor.Brighter(GDI.Color.);


                    Phrase p1_b1 = new Phrase(dt_new.Rows[0][6].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
                    PdfPCell cell2 = new PdfPCell(p1_b1);
                    cell2.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));


                    table1.AddCell(cell1);
                    table1.AddCell(cell2);
                }
                else if (i == 7)
                {
                    Phrase p1_b = new Phrase(col_final[i].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
                    PdfPCell cell1 = new PdfPCell(p1_b);
                    cell1.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                    // cell1.BackgroundColor.Brighter(GDI.Color.);


                    Phrase p1_b1 = new Phrase(dt_new.Rows[0][7].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
                    PdfPCell cell2 = new PdfPCell(p1_b1);
                    cell2.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));


                    table1.AddCell(cell1);
                    table1.AddCell(cell2);
                }
                else
                {
                    Phrase p1_b = new Phrase(col_final[i].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
                    PdfPCell cell1 = new PdfPCell(p1_b);
                    cell1.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                    // cell1.BackgroundColor.Brighter(GDI.Color.);


                    Phrase p1_b1 = new Phrase(dt.Rows[0][i].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
                    PdfPCell cell2 = new PdfPCell(p1_b1);
                    cell2.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));


                    table1.AddCell(cell1);
                    table1.AddCell(cell2);
                }

            }
            PdfPCell note = new PdfPCell(new Phrase("Notes : Every positive answer gets full marks for the question and every negative answer would get Zero marks. Mark 'NA' if the question is Not Applicable for any particular site. Comment / explanation should be given for where a corrective action is requried. Weightage for different critical safety requirement is 2, 1 and 0.5. In case any activity description is not applicable for the Cellsite, then the points would be automatically deducted from the total score to calculate the achieved percentage. Every observation made on the site should be classified as A, B or C (A being most critical) depending on the criticality / hazard Potential (for more details refer procedure).The bold text questions imply basis of rejection at the time site acceptance from Infra Provider.", new Font(Font.FontFamily.TIMES_ROMAN, 7)));
            note.Colspan = 2;
            note.FixedHeight = 60f;
            note.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#04B4AE"));
            note.HorizontalAlignment = Element.ALIGN_CENTER;
            note.VerticalAlignment = Element.ALIGN_MIDDLE;
            table1.AddCell(note);

            PdfPCell blank = new PdfPCell(new Phrase(""));
            blank.Colspan = 2;
            blank.FixedHeight = 15f;
            table1.AddCell(blank);

            doc.Add(head);
            doc.Add(table1);
        }

    }
    private void non_network(Document doc, string sheet, int i_counter)
    {
        PdfPTable head = new PdfPTable(1);
        head.WidthPercentage = 100f;

        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });

        //head.DefaultCell.Border = 0;
        string headersheet = "";
        string address = "";
        if (sheet == "WIC V1")
        {
            headersheet = "Warehouse Inspection Checklist V1";
            address = "Warehouse location and Address: ";
        }
        else if (sheet == "VOIC V1")
        {
            headersheet = "Vodafone Office Inspection Checklist V1";
            address = "Office location and Address: ";
        }
        else if (sheet == "VSIC V1")
        {
            headersheet = "Vodafone Stores Inspection Checklist V1";
            address = "Store location and Address: ";
        }
        if (sheet == "VMSIC V1")
        {
            headersheet = "Vodafone Mini Stores Inspection Checklist V1";
            address = "Mini Store location and Address: ";
        }
        else if (sheet == "PSIC V2")
        {
            headersheet = "Vodafone Project Site Checklist V1";
            address = "Site location and Address: ";
        }
        else if (sheet == "GHIC V1")
        {
            headersheet = "Guesthouse Inspection Checklist V1";
            address = "Guesthouse location and Address: ";
        }
        else if (sheet == "FWIC V1")
        {
            headersheet = "FOUR WHEELER INSPECTION CHECK LIST V1";
            address = "Address: ";
        }
        else if (sheet == "CIIC V1")
        {
            headersheet = "Cellsite Infra Inspection Checklist CIIC V1";
            address = "Address: ";
        }
        else if (sheet == "CIC V1")
        {
            headersheet = "Cellsite Inventory Checklist V1";
            address = "Address: ";
        }
        else if (sheet == "VSLIC V1")
        {
            headersheet = "Vodafone Signage Inspection Checklist (LOLIPOPS & POLE KIOSKS) V1";
            address = "Address: ";
        }
        else if (sheet == "EMPSC V1")
        {
            headersheet = "Event Management PERMENANT STRUCTURE";
            address = "Address: ";
        }
        else if (sheet == "EMSC V1")
        {
            headersheet = "Event Management START-UP";
            address = "Address: ";
        }
        else if (sheet == "EMVSC V1")
        {
            headersheet = "Event Management VENUE SELECTION";
            address = "Address: ";
        }
        else if (sheet == "AOIC V1")
        {
            headersheet = "Activation Office Inspection";
            address = "Address: ";
        }
        else if (sheet == "UBR V1")
        {
            headersheet = "Vodafone UBR Inspection Checklist";
            address = "Address: ";
        }
        else if (sheet == "WIC V1")
        {
            headersheet = "Vodafone Warehouse Inspection Checklist";
            address = "Address: ";
        }
        else if (sheet == "VOIC V1")
        {
            headersheet = "Vodafone Office Inspection Checklist";
            address = "Address: ";
        }
        else if (sheet == "VSIC V1")
        {
            headersheet = "Vodafone Stores Inspection Checklist";
            address = "Address: ";
        }
        else if (sheet == "PSIC V2")
        {
            headersheet = "Vodafone Project Inspection Checklist";
            address = "Address: ";
        }
        else
        {
            headersheet = "Vodafone Office Inspection Checklist V1";
            address = "Address: ";
        }
        Phrase p1header = new Phrase(headersheet, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
        PdfPCell c = new PdfPCell(p1header);
        // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#DB2120"));
        c.HorizontalAlignment = Element.ALIGN_CENTER;
        c.VerticalAlignment = Element.ALIGN_MIDDLE;
        c.BorderColor = iTextSharp.text.BaseColor.BLACK;
        c.FixedHeight = 20f;
        head.AddCell(c);


        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });


        PdfPTable table1 = new PdfPTable(2);
        table1.WidthPercentage = 100f;
        //table1.SetWidths(new float[] { 4, 3, 4, 3 });

        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.select_site_data_all_pdf(siteid, i_counter, sheet);


        Phrase p1_b = new Phrase(address + dt.Rows[0][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
        PdfPCell cell1 = new PdfPCell(p1_b);
        cell1.HorizontalAlignment = Element.ALIGN_LEFT;
        cell1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
        // cell1.BackgroundColor.Brighter(GDI.Color.);


        Phrase p1_b1 = new Phrase("Circle :" + dt.Rows[0][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell2 = new PdfPCell(p1_b1);
        cell2.HorizontalAlignment = Element.ALIGN_LEFT;
        cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b2 = new Phrase("Contact Persons Name & No." + dt.Rows[0][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell3 = new PdfPCell(p1_b2);
        cell3.HorizontalAlignment = Element.ALIGN_LEFT;
        cell3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b3 = new Phrase("Name & No. of Inspector:- " + dt.Rows[0][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell4 = new PdfPCell(p1_b3);
        cell4.HorizontalAlignment = Element.ALIGN_LEFT;
        cell4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b14 = new Phrase("Date of Inspection:- " + dt.Rows[0][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell5 = new PdfPCell(p1_b14);
        cell5.HorizontalAlignment = Element.ALIGN_LEFT;
        cell5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b15 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell6 = new PdfPCell(p1_b15);
        cell6.HorizontalAlignment = Element.ALIGN_LEFT;
        cell6.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));


        table1.AddCell(cell1);
        table1.AddCell(cell2);
        table1.AddCell(cell3);
        table1.AddCell(cell4);
        table1.AddCell(cell5);
        table1.AddCell(cell6);


        //PdfPCell note = new PdfPCell(new Phrase("Notes : Every positive answer gets full marks for the question and every negative answer would get Zero marks. Mark 'NA' if the question is Not Applicable for any particular site. Comment / explanation should be given for where a corrective action is requried. Weightage for different critical safety requirement is 2, 1 and 0.5. In case any activity description is not applicable for the Cellsite, then the points would be automatically deducted from the total score to calculate the achieved percentage. Every observation made on the site should be classified as A, B or C (A being most critical) depending on the criticality / hazard Potential (for more details refer procedure).The bold text questions imply basis of rejection at the time site acceptance from Infra Provider.", new Font(Font.FontFamily.TIMES_ROMAN, 7)));
        //note.Colspan = 2;
        //note.FixedHeight = 60f;
        //note.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#04B4AE"));
        //note.HorizontalAlignment = Element.ALIGN_CENTER;
        //note.VerticalAlignment = Element.ALIGN_MIDDLE;
        //table1.AddCell(note);

        PdfPCell blank = new PdfPCell(new Phrase(""));
        blank.Colspan = 2;
        blank.FixedHeight = 15f;
        table1.AddCell(blank);

        doc.Add(head);
        doc.Add(table1);
    }

    private void cic_header(Document doc, string sheet, int i_counter)
    {
        PdfPTable head = new PdfPTable(1);
        head.WidthPercentage = 100f;

        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });

        //head.DefaultCell.Border = 0;
        string headersheet = "";
        string address = "";

        {
            headersheet = "Cellsite Inventory Checklist V1";
            address = "Address: ";
        }

        Phrase p1header = new Phrase(headersheet, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.WHITE));
        PdfPCell c = new PdfPCell(p1header);
        c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
        c.HorizontalAlignment = Element.ALIGN_CENTER;
        c.VerticalAlignment = Element.ALIGN_MIDDLE;
        c.BorderColor = iTextSharp.text.BaseColor.BLACK;
        c.FixedHeight = 20f;
        head.AddCell(c);


        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });


        PdfPTable table1 = new PdfPTable(2);
        table1.WidthPercentage = 100f;
        //table1.SetWidths(new float[] { 4, 3, 4, 3 });

        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.select_site_data_all_pdf(siteid, i_counter, sheet);

        Phrase p1_b_ = new Phrase("Vodafone Site ID :" + siteid, new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
        PdfPCell cell1_ = new PdfPCell(p1_b_);
        cell1_.HorizontalAlignment = Element.ALIGN_LEFT;
        cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b = new Phrase(address + dt.Rows[0][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
        PdfPCell cell1 = new PdfPCell(p1_b);
        cell1.HorizontalAlignment = Element.ALIGN_LEFT;
        cell1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
        // cell1.BackgroundColor.Brighter(GDI.Color.);


        Phrase p1_b1 = new Phrase("Circle :" + dt.Rows[0][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell2 = new PdfPCell(p1_b1);
        cell2.HorizontalAlignment = Element.ALIGN_LEFT;
        cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b2 = new Phrase("Site Incharge Name & No. :" + dt.Rows[0][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell3 = new PdfPCell(p1_b2);
        cell3.HorizontalAlignment = Element.ALIGN_LEFT;
        cell3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b3 = new Phrase("Location of Control room & Contact No. : " + dt.Rows[0][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell4 = new PdfPCell(p1_b3);
        cell4.HorizontalAlignment = Element.ALIGN_LEFT;
        cell4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b14 = new Phrase("Date of Inspection:- " + dt.Rows[0][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell5 = new PdfPCell(p1_b14);
        cell5.HorizontalAlignment = Element.ALIGN_LEFT;
        cell5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

        Phrase p1_b15 = new Phrase("Lattitude & Longitude :" + dt.Rows[0][5].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
        PdfPCell cell6 = new PdfPCell(p1_b15);
        cell6.HorizontalAlignment = Element.ALIGN_LEFT;
        cell6.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));


        table1.AddCell(cell1);
        table1.AddCell(cell2);
        table1.AddCell(cell3);
        table1.AddCell(cell4);
        table1.AddCell(cell5);
        table1.AddCell(cell6);


        //PdfPCell note = new PdfPCell(new Phrase("Notes : Every positive answer gets full marks for the question and every negative answer would get Zero marks. Mark 'NA' if the question is Not Applicable for any particular site. Comment / explanation should be given for where a corrective action is requried. Weightage for different critical safety requirement is 2, 1 and 0.5. In case any activity description is not applicable for the Cellsite, then the points would be automatically deducted from the total score to calculate the achieved percentage. Every observation made on the site should be classified as A, B or C (A being most critical) depending on the criticality / hazard Potential (for more details refer procedure).The bold text questions imply basis of rejection at the time site acceptance from Infra Provider.", new Font(Font.FontFamily.TIMES_ROMAN, 7)));
        //note.Colspan = 2;
        //note.FixedHeight = 60f;
        //note.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#04B4AE"));
        //note.HorizontalAlignment = Element.ALIGN_CENTER;
        //note.VerticalAlignment = Element.ALIGN_MIDDLE;
        //table1.AddCell(note);

        PdfPCell blank = new PdfPCell(new Phrase(""));
        blank.Colspan = 2;
        blank.FixedHeight = 15f;
        table1.AddCell(blank);

        doc.Add(head);
        doc.Add(table1);
    }


    protected void drpcircle_SelectedIndexChanged1(object sender, EventArgs e)
    {
        ddlCheckSheet.Visible = true;
        ddlprovidername.Visible = true;
        Label13.Visible = true;
        Label7.Visible = true;
        ddlCheckSheet.SelectedIndex = 0;
        ddlprovidername.SelectedIndex = 0;
    }
    protected void ddlCheckSheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}
