﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true" CodeFile="LatLongApproval.aspx.cs" Inherits="AllPage_LatLongApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table>
            <tr>
                <td><asp:Label ID="lblSearchSite" runat="server" Text="Enter SiteId"></asp:Label></td>
                <td><asp:TextBox ID="txtSearchSite" runat="server"></asp:TextBox></td>
                <td><asp:Button ID="btnSearchSite" runat="server" Text="Search" OnClick="btnSearchSite_Click" /></td>
            </tr>
            <tr>
                <td><asp:Label ID="lblSearchUser" runat="server" Text="Enter Username"></asp:Label></td>
                <td><asp:TextBox ID="txtSearchUser" runat="server"></asp:TextBox></td>
                <td><asp:Button ID="btnSearchuser" runat="server" Text="Search" OnClick="btnSearchuser_Click" /></td>
            </tr>
        </table>
    </div>
    <%--<asp:TextBox ID="txtSearchSite" runat="server" CssClass="search" ></asp:TextBox> 
    <style type="text/css">
        .search
        {
            background:url(../images/search1.png) right no-repeat;
            background-size: 17px 17px;
            height:15px;
            font:13px Ms Sans Serif;
            padding-right:21px;
            color:#646262;
            width:130px;
            border:1px solid gray;
        }
    </style>--%>
    <br />
    <asp:GridView ID="gv_user" runat="server" EnableModelValidation="True" OnRowCommand="gv_user_RowCommand" Visible="false"
        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="30%">
        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle Font-Names="Calibri" Font-Size="13px" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

        <Columns>
            <asp:ButtonField Text="Select" CommandName="Select" ItemStyle-Width="30" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lbl_gv" runat="server" Text="No Data Found...!!" ForeColor="White"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
    <br />
    <asp:Panel ID="panel1" runat="server" ScrollBars="Horizontal" Width="1030px" Visible="false">
        <asp:GridView ID="GV_SearchLast5LatLong" runat="server" EnableModelValidation="True" Visible="false"
            BackColor="WhiteSmoke" BorderColor="#999999"
            BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="100%">
            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle Font-Names="Calibri" Font-Size="13px" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

            <EmptyDataTemplate>
                <asp:Label ID="lbl_gv" runat="server" Text="No Data Found...!!" ForeColor="White"></asp:Label>
            </EmptyDataTemplate>
        </asp:GridView>
    </asp:Panel>
    <br />
    <asp:Panel ID="panel2" runat="server" ScrollBars="Horizontal" Width="1030px">
        <asp:GridView ID="GV_LatLongApproval" runat="server" AutoGenerateColumns="False"
            EnableModelValidation="True" DataKeyNames="id"
            BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="100%">
            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle Font-Names="Calibri" Font-Size="13px" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

            <Columns>
                <asp:TemplateField HeaderText="Close By">
                    <ItemTemplate>
                        <asp:DropDownList ID="drp_Closeby" runat="server">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Khushboo" Value="1"></asp:ListItem>
			    <asp:ListItem Text="Bhumit" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Mahendra" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Prahlad" Value="4"></asp:ListItem>
			    <asp:ListItem Text="Dixit" Value="5"></asp:ListItem>
                            <asp:ListItem Text="Meet" Value="6"></asp:ListItem>
                            <asp:ListItem Text="Poyam" Value="7"></asp:ListItem>
			    <asp:ListItem Text="Circle Admin" Value="8"></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Approve">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnbtn_Approve" runat="server" ToolTip="<%# bind('id') %>" OnClick="lnbtn_Approve_Click">Approve</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reject">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnbtn_Reject" runat="server" ToolTip="<%# bind('id') %>" OnClick="lnbtn_Reject_Click">Reject</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Distance By">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Distanceby" runat="server" Text="<%# bind('distance_by') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Distance">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Distance" runat="server" Text="<%# bind('distance') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Project">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Project" runat="server" Text="<%# bind('project') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Username">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Username" runat="server" Text="<%# bind('techname') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mobile Number">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Mobile" runat="server" Text="<%# bind('technician_mobile') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Circle">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Circle" runat="server" Text="<%# bind('Circle') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Main Company">
                    <ItemTemplate>
                        <asp:Label ID="lbl_MCompany" runat="server" Text="<%# bind('Main_Company') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Sub Company">
                    <ItemTemplate>
                        <asp:Label ID="lbl_SCompany" runat="server" Text="<%# bind('Sub_company') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Site Id">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Siteid" runat="server" Text="<%# bind('siteid') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Site Name">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Sitename" runat="server" Text="<%# bind('sitename') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Zone">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Zone" runat="server" Text="<%# bind('zone') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Requested Time By User">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Requestedtime" runat="server" Text="<%# bind('open_time') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Site Lat">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Sitelat" runat="server" Text="<%# bind('old_lat') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Site Long">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Sitelong" runat="server" Text="<%# bind('old_long') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="User Lat">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Userlat" runat="server" Text="<%# bind('new_lat') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="User Long">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Userlong" runat="server" Text="<%# bind('new_long') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
		
		<asp:TemplateField HeaderText="LatLong Get Date">
                    <ItemTemplate>
                        <asp:Label ID="lbl_LatLongDate" runat="server" Text="<%# bind('Date') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last Login Date">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Logindate" runat="server" Text="<%# bind('last_login_date') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="User App. Version">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Version" runat="server" Text="<%# bind('version') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>

