﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;
using System.Data;
using System.Net;
using System.Net.Mail;
using NPOI.HSSF.UserModel;
//using Microsoft.Office.Interop.Excel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.IO;
using MVdataaccesslayer;
using System.Configuration;

public partial class circle_admin_car_report_2 : System.Web.UI.Page
{
    mydataaccess1 da;
    System.Data.DataTable dt;
    HSSFWorkbook hssfworkbook;
    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    int userid;
    int Step1;
    string sp_user_d = "";


    private void Page_PreRender(object sender, System.EventArgs e)
    {
        // Response.Cache.SetExpires(DateTime.Now);
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);
        // Response.Cache.SetNoStore();
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["flag"] == "1")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "2")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "4")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }

            // this.MasterPageFile = "~/circle_admin/NewCircle.master";
        }
        if (Session["flag"] == "3")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //da = new mydataaccess1();
            //sp_user_d = da.select_user_cookie(Session["user"].ToString());


            grdbind();

        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }

    private void bind()
    {

        da = new mydataaccess1();
        System.Data.DataTable dt = new System.Data.DataTable();
        dt = da.CAR_Get_Detail_Data(Convert.ToInt32(Session["c_type"].ToString()), Session["mySiteId"].ToString(), Session["user"].ToString(), Convert.ToInt32(Session["Rating_Id"]));
        if (dt.Rows.Count > 0)
        {
            TextBox1.Text = dt.Rows[0][5].ToString();
            TextBox2.Text = dt.Rows[0][6].ToString();
            TextBox3.Text = dt.Rows[0][0].ToString();
            TextBox4.Text = dt.Rows[0][1].ToString();
            TextBox5.Text = dt.Rows[0][2].ToString();
            TextBox6.Text = dt.Rows[0][8].ToString();
           // TextBox7.Text = dt.Rows[0][15].ToString();
            TextBox8.Text = dt.Rows[0][14].ToString();


            grd.DataSource = dt;
            grd.DataBind();
            grd.UseAccessibleHeader = true;
            grd.HeaderRow.TableSection = TableRowSection.TableHeader;

            int flagA = 0;
            int flagB = 0;
            int flagC = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    if (j == 1)
                    {
                        string cri = "A";
                        if (dt.Rows[i][11].ToString() == "A" && (dt.Rows[i][32].ToString() == "---Select---" || dt.Rows[i][32].ToString() == "" || dt.Rows[i][32].ToString() == "B" || dt.Rows[i][32].ToString() == "C" || dt.Rows[i][32].ToString() == "A" || dt.Rows[i][23].ToString() == "Open"))
                        {
                            if (dt.Rows[i][32].ToString() == "A")
                                flagA++;
                            else if (dt.Rows[i][32].ToString() == "B")
                                flagB++;
                            else if (dt.Rows[i][32].ToString() == "C")
                                flagC++;
                            else
                                flagA++;
                        }
                    }
                    if (j == 2)
                    {
                        string cri = "B";
                        if (dt.Rows[i][11].ToString() == "B" && (dt.Rows[i][32].ToString() == "---Select---" || dt.Rows[i][32].ToString() == "" || dt.Rows[i][32].ToString() == "B" || dt.Rows[i][32].ToString() == "C" || dt.Rows[i][32].ToString() == "A" || dt.Rows[i][23].ToString() == "Open"))
                        {
                            if (dt.Rows[i][32].ToString() == "A")
                                flagA++;
                            else if (dt.Rows[i][32].ToString() == "B")
                                flagB++;
                            else if (dt.Rows[i][32].ToString() == "C")
                                flagC++;
                            else
                                flagB++;
                        }
                    }
                    if (j == 3)
                    {
                        string cri = "C";
                        if (dt.Rows[i][11].ToString() == "C" && (dt.Rows[i][32].ToString() == "---Select---" || dt.Rows[i][32].ToString() == "" || dt.Rows[i][32].ToString() == "A" || dt.Rows[i][32].ToString() == "B" || dt.Rows[i][32].ToString() == "C" || dt.Rows[i][23].ToString() == "Open"))
                        {
                            if (dt.Rows[i][32].ToString() == "A")
                                flagA++;
                            else if (dt.Rows[i][32].ToString() == "B")
                                flagB++;
                            else if (dt.Rows[i][32].ToString() == "C")
                                flagC++;
                            else
                                flagC++;
                        }
                    }
                }
            }
            DropDownList drp;
            DropDownList drp_status;
            DropDownList drp_route;
            DropDownList drp_person;
            // DropDownList drp;
            foreach (GridViewRow grdRow in grd.Rows)
            {

                drp = (DropDownList)(grd.Rows[grdRow.RowIndex].Cells[7].FindControl("ddlcric"));
                drp.Text =SpacialCharRemove.XSS_Remove(dt.Rows[grdRow.RowIndex][32].ToString());

                drp_status = (DropDownList)(grd.Rows[grdRow.RowIndex].Cells[12].FindControl("ddlActionStatus"));
                drp_status.Text = null;
                drp_status.Text = SpacialCharRemove.XSS_Remove(dt.Rows[grdRow.RowIndex][23].ToString());

                drp_route = (DropDownList)(grd.Rows[grdRow.RowIndex].Cells[14].FindControl("ddlroute"));
                drp_route.Text = SpacialCharRemove.XSS_Remove(dt.Rows[grdRow.RowIndex][30].ToString());



            }

            da = new mydataaccess1();
            System.Data.DataTable dt1 = new System.Data.DataTable();
            //dt1 = da.select_cricality_count(Convert.ToInt32(Session["c_type"].ToString()), Session["mySiteId"].ToString(), Session["user"].ToString(), Convert.ToInt32(Session["Rating_Id"]));
            //if (dt1.Rows.Count > 0)
            //{
            //    if (dt1.Rows[0][3].ToString() == "" && dt1.Rows[0][4].ToString() == "" && dt1.Rows[0][5].ToString() == "")
            //    {
            //        lbla.Text = dt1.Rows[0][0].ToString();
            //        lblb.Text = dt1.Rows[0][1].ToString();
            //        lblc.Text = dt1.Rows[0][2].ToString();
            //        lblaa.Text = dt1.Rows[0][0].ToString();
            //        lblbb.Text = dt1.Rows[0][1].ToString();
            //        lblcc.Text = dt1.Rows[0][2].ToString();
            //        Label21.Text = "Open";
            //        Label18.Text = "Close";
            //        Label24.Text = "WIP";
            //        Label27.Text = "Never Identified Before";
            //        Label10.Text = "First Inspection";
            //    }
            //    else
            //    {
            //        lbla.Text = dt1.Rows[0][0].ToString();
            //        lblb.Text = dt1.Rows[0][1].ToString();
            //        lblc.Text = dt1.Rows[0][2].ToString();
            //        lblaa.Text = flagA.ToString();
            //        lblbb.Text = flagB.ToString();
            //        lblcc.Text = flagC.ToString();
            //        Label21.Text = "Open";
            //        Label18.Text = "Close";
            //        Label24.Text = "WIP";
            //        //  int total = (Convert.ToInt32(dt1.Rows[0][0].ToString()) + Convert.ToInt32(dt1.Rows[0][1].ToString()) + Convert.ToInt32(dt1.Rows[0][2].ToString())) - (Convert.ToInt32(dt1.Rows[0][3].ToString()) + Convert.ToInt32(dt1.Rows[0][4].ToString()) + Convert.ToInt32(dt1.Rows[0][5].ToString()));
            //        Label27.Text = "Never Identified Before";
            //        Label10.Text = "First Inspection";
            //    }
            //}
        }
        else
        {
            grd.DataSource = null;
            grd.DataBind();
        }
    }

    private void grdbind()
    {
        da = new mydataaccess1();
        System.Data.DataTable dt = new System.Data.DataTable();
        //dt = da.CAR_Get_Detail_Data(Convert.ToInt32(Session["c_type"].ToString()), Session["mySiteId"].ToString(), Session["user"].ToString(), Convert.ToInt32(Session["Rating_Id"]));
        dt = da.select_nodes_from_siteid_car(Session["mySiteId"].ToString());
        if (dt.Rows.Count > 0)
        {
            Session["TXCarReport_DT"] = dt;
            TextBox1.Text = dt.Rows[0][3].ToString();
            TextBox2.Text = dt.Rows[0][4].ToString();
            TextBox3.Text = dt.Rows[0][0].ToString();
            TextBox4.Text = dt.Rows[0][8].ToString();
            TextBox5.Text = dt.Rows[0][6].ToString();
            TextBox6.Text = dt.Rows[0][5].ToString();
            //TextBox7.Text = dt.Rows[0][15].ToString();
            TextBox8.Text = dt.Rows[0][7].ToString();


            grd.DataSource = dt;
            grd.DataBind();
            grd.UseAccessibleHeader = true;
            grd.HeaderRow.TableSection = TableRowSection.TableHeader;

            int flagA = 0;
            int flagB = 0;
            int flagC = 0;
            
            DropDownList drp;
            DropDownList drp_status;
            DropDownList drp_route;
            DropDownList drp_person;
           

            da = new mydataaccess1();
            System.Data.DataTable dt1 = new System.Data.DataTable();
            
        }
        else
        {
            grd.DataSource = null;
            grd.DataBind();
        }
    }

    private Boolean ValidateDetailGrid()
    {
        bool isValidate = true;
        foreach (GridViewRow gvr in grd.Rows)
        {
            CheckBox ck = gvr.Cells[0].FindControl("CheckBox1") as CheckBox;
            if (ck.Checked)
            {
                //System.Web.UI.WebControls.TextBox txtFollowUpComment = gvr.Cells[11].FindControl("txtFollowupComments") as System.Web.UI.WebControls.TextBox;
                //System.Web.UI.WebControls.TextBox txtactionable = gvr.Cells[15].FindControl("txtactionable") as System.Web.UI.WebControls.TextBox;
                //System.Web.UI.WebControls.TextBox txtLastFollowUpDate = gvr.Cells[10].FindControl("txtLastFollowUpDate") as System.Web.UI.WebControls.TextBox;
                //DropDownList ddlActionStatus = gvr.Cells[12].FindControl("ddlActionStatus") as DropDownList;
                //DropDownList ddlroute = gvr.Cells[14].FindControl("ddlroute") as DropDownList;
                //DropDownList ddlcrit = gvr.Cells[9].FindControl("ddlcric") as DropDownList;
                //System.Web.UI.WebControls.TextBox txtActionDate = gvr.Cells[13].FindControl("txtActionDate") as System.Web.UI.WebControls.TextBox;

                System.Web.UI.WebControls.TextBox txtPerson = gvr.Cells[4].FindControl("txtperson") as System.Web.UI.WebControls.TextBox;
                DropDownList ddlActionStatus = gvr.Cells[5].FindControl("ddlActionStatus") as DropDownList;
                if (txtPerson.Text.Trim() == "")
                {
                    txtPerson.BackColor = System.Drawing.Color.Red;
                    isValidate = false;
                    //  break;
                }
                if (ddlActionStatus.SelectedItem.Text == "" || ddlActionStatus.SelectedItem.Text == "---Select---")
                {
                    ddlActionStatus.BackColor = System.Drawing.Color.Red;
                    isValidate = false;
                    // break;
                }
              
            }
        }

        return isValidate;
    }
    protected void grd_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridViewRow tr = e.Row;
            //  tr.Attributes.Add("onClick", "javascript:selectMe(this);");
            // tr.Attributes.Add("style", "cursor:hand;");
        }
    }


    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lbtn = (LinkButton)sender;
        GridViewRow row = (GridViewRow)lbtn.NamingContainer;
        System.Web.UI.WebControls.TextBox txtFollowUpComment = row.Cells[9].FindControl("txtFollowupComments") as System.Web.UI.WebControls.TextBox;
        string a = txtFollowUpComment.Text;
        //  Label lbl = row.FindControl("Label2") as Label;
    }



    protected void grd_RowDataBound2(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {





            if (e.Row.RowIndex == 0)
                e.Row.Style.Add("height", "40px");

            System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)e.Row.Cells[0].FindControl("CheckBox1");

            DropDownList ddlCountries = (e.Row.FindControl("ddlActionStatus") as DropDownList);

            DataTable ActiveCarReport_DT = new DataTable();
            ActiveCarReport_DT = (DataTable)Session["TXCarReport_DT"];
            int Index = e.Row.RowIndex;
            if (ActiveCarReport_DT.Rows[Index][13].ToString() != "")
            {
                ddlCountries.SelectedValue = SpacialCharRemove.XSS_Remove(ActiveCarReport_DT.Rows[Index][13].ToString());
            }

            //string[] abc = chk.ToolTip.ToString().Split(',');
            //string status = abc[1].ToString();
            //string l_status = abc[2].ToString();

            //if (status == "Open")
            //{
            //    e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
            //}
            //else if (status == "Close")
            //{
            //    e.Row.Cells[0].BackColor = System.Drawing.Color.Green;
            //    chk.Enabled = false;
            //}
            //else if (status == "WIP")
            //{
            //    e.Row.Cells[0].BackColor = System.Drawing.Color.Yellow;

            //}
            //else
            //{
            //    e.Row.Cells[0].BackColor = System.Drawing.Color.SkyBlue;

            //}

            //if (l_status == "Open")
            //{
            //    e.Row.Cells[1].BackColor = System.Drawing.Color.Red;
            //}
            //else if (l_status == "Close")
            //{
            //    e.Row.Cells[1].BackColor = System.Drawing.Color.Green;
            //}
            //else if (l_status == "WIP")
            //{
            //    e.Row.Cells[1].BackColor = System.Drawing.Color.Yellow;
            //    chk.Enabled = false;
            //}
            //else
            //{
            //    e.Row.Cells[1].BackColor = System.Drawing.Color.Khaki;
            //}
            /*
            if (e.Row.Cells[11].Text == "Open")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
            }
            if (e.Row.Cells[11].Text == "Close")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Green;
                e.Row.Cells[0].Enabled = false;
                // e.Row.Cells[1].Style.Add("background-color", "GREEN");
                // e.Row.BackColor = System.Drawing.Color.Green;
            }
            if (e.Row.Cells[11].Text == "WIP")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Yellow;
            }*/
        }

    }
    protected void CheckBox1_CheckedChanged1(object sender, EventArgs e)
    {


        System.Web.UI.WebControls.CheckBox ck = (System.Web.UI.WebControls.CheckBox)sender;
        GridViewRow row = (GridViewRow)ck.NamingContainer;
        grd.UseAccessibleHeader = true;
        grd.HeaderRow.TableSection = TableRowSection.TableHeader;
        if (ck.Checked)
        {
            TextBox txtp = (TextBox)row.Cells[4].FindControl("txtperson");
            txtp.Enabled = true;
            txtp.BackColor = System.Drawing.Color.White;

            DropDownList d1 = (DropDownList)row.Cells[5].FindControl("ddlActionStatus");
            d1.Enabled = true;
            d1.BackColor = System.Drawing.Color.White;

            System.Web.UI.WebControls.FileUpload t5 = (System.Web.UI.WebControls.FileUpload)row.Cells[6].FindControl("FileUpload1");
            t5.Enabled = true;
            t5.BackColor = System.Drawing.Color.White;

          

        }
        else
        {

            TextBox txtp = (TextBox)row.Cells[4].FindControl("txtperson");
            txtp.Enabled = false;
            txtp.BackColor = System.Drawing.Color.White;

            DropDownList d1 = (DropDownList)row.Cells[5].FindControl("ddlActionStatus");
            d1.Enabled = false;
            d1.BackColor = System.Drawing.Color.White;

            System.Web.UI.WebControls.FileUpload t5 = (System.Web.UI.WebControls.FileUpload)row.Cells[6].FindControl("FileUpload1");
            t5.Enabled = false;
            t5.BackColor = System.Drawing.Color.White;
        }


    }
    protected void ddlcric_SelectedIndexChanged1(object sender, EventArgs e)
    {

        // GridViewRow pagerRow = grd.s;
        GridViewRow gr = (GridViewRow)((DataControlFieldCell)((DropDownList)sender).Parent).Parent;
        DropDownList ddl = (DropDownList)gr.FindControl("ddlcric");
        System.Web.UI.WebControls.TextBox txt = (System.Web.UI.WebControls.TextBox)gr.FindControl("txtLastFollowUpDate");
        System.Web.UI.WebControls.TextBox txt1 = (System.Web.UI.WebControls.TextBox)gr.FindControl("TargetDueDate");



        if (ddl.SelectedItem.Text == "A")
        {
            string[] date1 = txt1.Text.Split('/');
            DateTime dat1;
            dat1 = Convert.ToDateTime(date1[1].ToString() + "/" + date1[0].ToString() + "/" + date1[2].ToString());
            DateTime date_ = dat1.AddDays(1);
            txt.Text = date_.Day + "/" + date_.Month + "/" + date_.Year;
        }
        if (ddl.SelectedItem.Text == "B")
        {
            //txt.Text = "1";
            string[] date1 = txt1.Text.Split('/');
            DateTime dat1;
            dat1 = Convert.ToDateTime(date1[1].ToString() + "/" + date1[0].ToString() + "/" + date1[2].ToString());
            DateTime date_ = dat1.AddDays(7);
            txt.Text = date_.Day + "/" + date_.Month + "/" + date_.Year;
        }
        if (ddl.SelectedItem.Text == "C")
        {
            //txt.Text = "1";
            string[] date1 = txt1.Text.Split('/');
            DateTime dat1;
            dat1 = Convert.ToDateTime(date1[1].ToString() + "/" + date1[0].ToString() + "/" + date1[2].ToString());
            DateTime date_ = dat1.AddDays(30);
            txt.Text = date_.Day + "/" + date_.Month + "/" + date_.Year;
        }
        if (ddl.SelectedItem.Text == "NULL")
        {
            txt.Text = txt1.Text;
        }

        // Set the PageIndex property to display that page selected by the user.


    }
    protected void lnkque_Click(object sender, EventArgs e)
    {
        LinkButton ln = (LinkButton)sender;
        da = new mydataaccess1();
        string file = da.selectphoto(ln.Text, TextBox3.Text, Convert.ToInt32(Session["c_type"].ToString()));

        ClientScript.RegisterArrayDeclaration("images", "~/FTO_Android/UploadPhoto/" + file);

        ScriptManager.RegisterStartupScript

                    (this.Page, this.GetType(), "sky1", "showImage();", true);
        //   ln.Attributes.Add("onclick", "showIt(~/FTO_Android/UploadPhoto/" + file);


    }
    protected void grd_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("SetURL"))
        {

            Response.ContentType = "image/jpg";
            string url = ConfigurationManager.AppSettings["ImagePath"].ToString() + SpacialCharRemove.SpacialChar_Remove((e.CommandArgument).ToString());

            Image1.ImageUrl = @"data:image/jpg;base64," + Convert.ToBase64String(File.ReadAllBytes(url));
            //Image1.ImageUrl = Convert.ToString(url);
            md1.Show();

        }
    }
    protected void btnSave_Click1(object sender, EventArgs e)
    {

        try
        {
            string bunch = System.Guid.NewGuid().ToString().Substring(0, 4);
            int update = 0;
            bool isValidate = false;
            if (grd.Rows.Count > 0)
            {
                //isValidate = ValidateDetailGrid();
                //if (isValidate)
                {
                    foreach (GridViewRow gvr in grd.Rows)
                    {
                        System.Web.UI.WebControls.CheckBox ck = gvr.Cells[0].FindControl("CheckBox1") as System.Web.UI.WebControls.CheckBox;
                        if (ck.Checked && ck.Enabled == true)
                        {
                            isValidate = ValidateDetailGrid();
                            if (isValidate)
                            {
                                // update++;
                                da = new mydataaccess1();
                                ba = new myvodav2();
                                MVDataAccess da_1 = new MVDataAccess();
                                DataTable dt_1 = new DataTable();
                                ba.Siteid = Session["mySiteId"].ToString();
                                ba.Nssid = gvr.Cells[2].Text;
                                TextBox ddlperson = gvr.Cells[4].FindControl("txtperson") as TextBox;
                                ba.Risk_Mitigity_ = ddlperson.Text;
                               
                                System.Web.UI.WebControls.CheckBox chk = (System.Web.UI.WebControls.CheckBox)gvr.Cells[0].FindControl("CheckBox1");
                                string[] abc = chk.ToolTip.ToString().Split(',');
                                ba.qid = Convert.ToInt32(abc[0].ToString());

                                DropDownList ddlActionStatus = gvr.Cells[5].FindControl("ddlActionStatus") as DropDownList;
                                ba.Action_Status_ = ddlActionStatus.SelectedItem.Text.ToString();

                                FileUpload filepath_ = gvr.Cells[6].FindControl("FileUpload1") as FileUpload;
                                string filePath = filepath_.PostedFile.FileName;

                                ba.Step = Convert.ToInt32(gvr.Cells[6].Text);
                                string route = "";
                                string actionable = "45";
                                string rmail = "";
                                string zmail = "";
                                //ba.Average_Score_ = Convert.ToInt32(TextBox7.Text);
                                //ba.Site_Rectified_ = true;
                                mydataaccess1 da9 = new mydataaccess1();
                                sp_user_d = da9.select_user_cookie(Session["user"].ToString());

                                ba.Responsible_Person_ = sp_user_d; //Session["user"].ToString();
                                ba.Follow_Up_Comment_ = "Test";
                                ba.Contactno = "";
                                int s = da.CAR_INSERT_GPM_A(ba, Convert.ToInt32(Session["c_type"].ToString()), route, actionable, 0, bunch, rmail, zmail);
                                // ba.Average_Score_ = Convert.ToInt32(TextBox7.Text);
                                //ba.Rating_Id_ = Convert.ToInt32(Session["Rating_Id"]);

                                //int s = da.CAR_INSERT_TX(ba, Convert.ToInt32(Session["c_type"].ToString()), 0, bunch);

                                // photo entry
                              
                                if (filePath != "") {
                                    string siteid = Session["mySiteId"].ToString();
                                    string stepid = gvr.Cells[6].Text;
                                    string inspection_counter = gvr.Cells[7].Text;
                                    string check_type = Session["c_type"].ToString();
                                    string ques_id = abc[0].ToString();
                                    string nssid = gvr.Cells[2].Text;
                                    string FileName_new = "";
                                    //string serverPath = "D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/vodafone_pics/UploadPhoto/";
                                    string serverPath = System.Configuration.ConfigurationManager.AppSettings["ImagePath"].ToString();
                                    FileName_new = SpacialCharRemove.SpacialChar_Remove(siteid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + "_" + SpacialCharRemove.SpacialChar_Remove(filePath);
                                    FileName_new = FileName_new.Replace("\r\n", "");
                                    if (!Directory.Exists(serverPath))
                                    {
                                        Directory.CreateDirectory(serverPath);
                                    }

                                    filepath_.SaveAs(Path.Combine(serverPath, FileName_new));
                                    //dt_1 = da_1.insertphotodetails_punchpoint(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new);

                                    if (check_type == "45" && Convert.ToInt32(gvr.Cells[6].Text) < 4)
                                    {
                                        dt_1 = da_1.insertphotodetails_tx_punchpoint(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, siteid);
                                    }
                                    else if (check_type == "45" && Convert.ToInt32(gvr.Cells[6].Text) > 3)
                                    {
                                        dt_1 = da_1.insertphotodetails_tx_punchpoint(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, nssid);
                                    }
                                    else { }
                                }
                                //da = new mydataaccess1();
                                //dt = new System.Data.DataTable();
                                //dt = da.car_select_email(ba.Responsible_Person_);
                                update++;

                            }
                            else
                            {
                                update = 1;
                                lblError.Text = "Fill  all data to proceed";
                                lblError.Visible = true;
                            }
                        }
                        
                     
                        //if (update == 0)
                        //{
                        //    da = new mydataaccess1();
                        //    System.Data.DataTable dt = new System.Data.DataTable();
                        //    dt = da.CAR_Get_Detail_Data(Convert.ToInt32(Session["c_type"].ToString()), Session["mySiteId"].ToString(), Session["user"].ToString(), Convert.ToInt32(Session["Rating_Id"]));
                        //    grd.DataSource = dt;
                        //    grd.DataBind();
                        //    //Response.Redirect("car_report.aspx", false);
                        //    ///Rebind the Master and detail grid
                        //    // bind();
                        //}
                    }
                    da = new mydataaccess1();
                    dt = new System.Data.DataTable();
                    dt = da.select_car_error_data(bunch);
                    if (dt.Rows.Count > 0)
                    {
                        Response.Clear();
                        string filename = "error.xls";
                        Response.ContentType = "application/vnd.ms-excel";
                        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));


                        InitializeWorkbook();
                        exporttoexcel(dt);
                        Response.BinaryWrite(WriteToStream().GetBuffer());

                    }
                    else
                    {
                        if (update > 0)
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Data Uploadaded Successfully... ');</script>");
                            grdbind();
                        }
                        else {
                            //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Data Uploadaded Successfully... ');</script>");
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }

    }
    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }

    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            HSSFCell mycell = row.CreateCell(j);
            mycell.SetCellValue(dt.Columns[j].ColumnName);

            HSSFCellStyle lockedNumericStyle = hssfworkbook.CreateCellStyle();
            lockedNumericStyle.Alignment = HSSFCellStyle.ALIGN_RIGHT;
            lockedNumericStyle.IsLocked = true;//(true);
            mycell.CellStyle = lockedNumericStyle;

            //row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                //HSSFCellStyle lockedNumericStyle = hssfworkbook.CreateCellStyle();
                //lockedNumericStyle.Alignment = HSSFCellStyle.ALIGN_RIGHT;
                //lockedNumericStyle.IsLocked = true;//(true);
                //row.CreateCell(j).CellStyle = lockedNumericStyle;


                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());


            }
        }
    }
    //protected void btndownload_Click(object sender, EventArgs e)
    //{

    //    da = new mydataaccess1();
    //    System.Data.DataTable dt = new System.Data.DataTable();


    //    dt = da.car_bulk_data_sitewise(Convert.ToInt32(Session["c_type"].ToString()), TextBox3.Text);

    //    if (dt.Rows.Count > 0)
    //    {

    //        GridView1.DataSource = dt;
    //        GridView1.DataBind();
    //        Response.Clear();

    //        Response.AddHeader("content-disposition", "attachment; filename=Sheet1.xls");

    //        Response.Charset = "";

    //        // If you want the option to open the Excel file without saving than

    //        // comment out the line below

    //        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

    //        Response.ContentType = "application/vnd.xls";

    //        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

    //        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);


    //        GridView1.RenderControl(htmlWrite);

    //        Response.Write(stringWrite.ToString());

    //        Response.End();

    //    }

    //}
    //protected void Button2_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        da = new mydataaccess1();
    //        ba = new myvodav2();

    //        string abc = FileUpload1.FileName;
    //        if (abc == "")
    //        {
    //            // lblresult.Text = "Select file first.";
    //            // lblresult.Visible = true;
    //        }
    //        else
    //        {
    //            abc = System.DateTime.Now.Millisecond + System.Guid.NewGuid().ToString().Substring(0, 4) + abc;
    //            FileUpload1.SaveAs(Server.MapPath("~/SampleData/sitedata/") + abc);
    //            string path = Server.MapPath("~/SampleData/sitedata/") + abc;
    //            string excelConnectionString = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\";";

    //            string constring = excelConnectionString;
    //            int result;
    //            string sp_user_d = "";
    //            string bunch = System.Guid.NewGuid().ToString().Substring(0, 4);

    //            da = new mydataaccess1();
    //            dt = new System.Data.DataTable();
    //            sp_user_d = da.select_user_cookie(Session["user"].ToString());

    //            result = Convert.ToInt32(da.import_car(constring, sp_user_d, bunch, Convert.ToInt16(Session["c_type"].ToString())));

    //            da = new mydataaccess1();
    //            dt = new System.Data.DataTable();
    //            dt = da.select_car_error_data(bunch);
    //            if (dt.Rows.Count > 0)
    //            {
    //                string filename = "error.xls";
    //                Response.Clear();
    //                Response.ContentType = "application/vnd.ms-excel";
    //                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));


    //                InitializeWorkbook();
    //                exporttoexcel(dt);
    //                Response.BinaryWrite(WriteToStream().GetBuffer());

    //            }

    //            else
    //            {
    //              //  bind();
    //                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Data Uploadaded Successfully... ');</script>");
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Redirect("~/sessionerror/sessionerrorpage.aspx", false);
    //    }

    //}

    protected void btnsitesearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtsearxh.Text != "")
            { 
                da = new mydataaccess1();
                DataTable dt = new DataTable();
                string siteid = TextBox3.Text;
                dt = da.select_nodes_from_nssid_search(siteid, txtsearxh.Text);
                //Session["TXCarReport_DT1"] = dt;
                if (dt.Rows.Count > 0)
                {
                    grd.DataSource = dt;
                    grd.DataBind();
                }
                else
                {
                    grd.DataSource = null;
                    grd.DataBind();
                }
            }
        }
        catch
        {
        }
    }
}