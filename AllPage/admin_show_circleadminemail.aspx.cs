﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;

public partial class AllPage_admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        GridViewRow gr = grduser.SelectedRow;
        da = new mydataaccess1();
        dt = da.get_circle_admin_email_list("");
        grduser.DataSource = dt;
        grduser.DataBind();

        if (!IsPostBack)
        {
		 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
            if (Session["role"].ToString() != "4" && Session["um"].ToString()!=sp_user_)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {
                try
                {
                    // da = new mydataaccess1();
                    //  ba = new myvodav2();
                    //  dt = new DataTable();
                    //  dt = da.showuser(ba);
                    //  grduser.DataSource = dt;
                    //   grduser.DataBind();

                }
                catch
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }
        }
    }

    private bool CompareArray(byte[] a1, byte[] a2)
    {

        if (a1.Length != a2.Length)

            return false;



        for (int i = 0; i < a1.Length; i++)
        {

            if (a1[i] != a2[i])

                return false;

        }



        return true;

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            ba.Circle = txtcircle.Text;
            ba.Name1 = txtname1.Text;
            ba.Email1 = txtemail1.Text;
            ba.Name2 = txtname2.Text;
            ba.Email2 = txtemail2.Text;
            ba.Name3 = txtname3.Text;
            ba.Email3 = txtemail3.Text;
            ba.Name4 = txtname4.Text;
            ba.Email4 = txtemail4.Text;
            ba.Name5 = txtname5.Text;
            ba.Email5 = txtemail5.Text;

            da.updatecircleadminemail(ba);
            mp1.Hide();
            da = new mydataaccess1();
            dt = da.get_circle_admin_email_list("");
            grduser.DataSource = dt;
            grduser.DataBind();
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
   /* protected void grduser_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        // find student id of edit row
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        // find updated values for update
        TextBox name = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txt_Name");
        TextBox branch = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txt_Branch");
        TextBox city = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txt_City");
 
        SqlCommand cmd = new SqlCommand("update tbl_student set Name='" + name.Text + "',
                                                          Branch='"+branch.Text+"', City='"+city.Text+"' where ID="+id, con);
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
 
        GridView1.EditIndex = -1;
        BindGridView();
    }
    protected void grduser_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grduser.EditIndex = -1;
        da = new mydataaccess1();
        dt = da.get_circle_admin_email_list("");
        grduser.DataSource = dt;
        grduser.DataBind();
    }
    protected void grduser_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grduser.EditIndex = e.NewEditIndex;
        da = new mydataaccess1();
        dt = da.get_circle_admin_email_list("");
        grduser.DataSource = dt;
        grduser.DataBind();
    }*/
    protected void grduser_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = grduser.SelectedRow;
            da = new mydataaccess1();
            dt = da.get_circle_admin_email_list("");
            grduser.DataSource = dt;
            grduser.DataBind();

            if (gr.Cells[1].Text == "&nbsp;")
            {
                txtcircle.Text = "";
            }
            else
            {
                txtcircle.Text = gr.Cells[1].Text;
            }
            if (gr.Cells[2].Text == "&nbsp;")
            {
                txtname1.Text = "";
            }
            else
            {
                txtname1.Text = gr.Cells[2].Text;
            }
            if (gr.Cells[3].Text == "&nbsp;")
            {
                txtemail1.Text = "";
            }
            else
            {
                txtemail1.Text = gr.Cells[3].Text;
            }
            if (gr.Cells[4].Text == "&nbsp;")
            {
                txtname2.Text = "";
            }
            else
            {
                txtname2.Text = gr.Cells[4].Text;
            }
            if (gr.Cells[5].Text == "&nbsp;")
            {
                txtemail2.Text = "";
            }
            else
            {
                txtemail2.Text = gr.Cells[5].Text;
            }
            if (gr.Cells[6].Text == "&nbsp;")
            {
                txtname3.Text = "";
            }
            else
            {
                txtname3.Text = gr.Cells[6].Text;
            }
            if (gr.Cells[7].Text == "&nbsp;")
            {
                txtemail3.Text = "";
            }
            else
            {
                txtemail3.Text = gr.Cells[7].Text;
            }
            if (gr.Cells[8].Text == "&nbsp;")
            {
                txtname4.Text = "";
            }
            else
            {
                txtname4.Text = gr.Cells[8].Text;
            }
            if (gr.Cells[9].Text == "&nbsp;")
            {
                txtemail4.Text = "";
            }
            else
            {
                txtemail4.Text = gr.Cells[9].Text;
            }
            if (gr.Cells[10].Text == "&nbsp;")
            {
                txtname5.Text = "";
            }
            else
            {
                txtname5.Text = gr.Cells[10].Text;
            }
            if (gr.Cells[11].Text == "&nbsp;")
            {
                txtemail5.Text = "";
            }
            else
            {
                txtemail5.Text = gr.Cells[11].Text;
            }
            mp1.Show();
        }
        catch
        {
            //throw ex;
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }
}
