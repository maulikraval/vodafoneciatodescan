﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;
using System.Text;

public partial class PM_PMT_Site_New_Download : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    int userid;
    int Step1;
    private void Page_PreRender(object sender, System.EventArgs e)
    {
        //Response.Cache.SetExpires(DateTime.Now);
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
            string User = lblusername.Text;
            da = new mydataaccess1();
            userid = da.selectuserid(User);
            if (!IsPostBack)
            {
                try
                {
                    int i = 12;
                    if (Convert.ToInt32(Session["role"].ToString()) == i)
                    {

                    }

                    else
                    {

                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }

            try
            {

                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {


                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da = new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    lblmarquee.Text = "";

                    {

                        if (Session["role"].ToString() == "12")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da = new mydataaccess1();
                                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                                if (lblusername.Text == "")
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }
                                da = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                // drpcircle.Items.Insert(1, "All");
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            //ha
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            //
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //Response.Clear();

        //Response.AddHeader("content-disposition", "attachment; filename=Report.csv");

        //Response.Charset = "";

        //Response.ContentType = "application/text";

        //System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);


        //GridView1.RenderControl(htmlWrite);

        //Response.Write(stringWrite.ToString());

        //Response.End();
        //GridView1.Columns[0].Visible = true;
        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition",
         "attachment;filename=SiteMaster.csv");
        Response.Charset = "";
        Response.ContentType = "application/text";


        DataTable datedata1 = new DataTable();
        if (drpcircle.SelectedIndex > 0)
        {

            da = new mydataaccess1();
            ba1 = new myvodav23();

            datedata1 = da.Site_New_Download(drpcircle.SelectedItem.Text);
            GridView1.DataSource = datedata1;
            GridView1.DataBind();
        }
        GridView1.AllowPaging = false;


        StringBuilder sb = new StringBuilder();

        for (int k = 0; k < datedata1.Columns.Count; k++)
        {
            //add separator

            sb.Append(datedata1.Columns[k].ToString() + ',');
        }
        //append new line
        sb.Append("\r\n");
        for (int i = 0; i < datedata1.Rows.Count; i++)
        {
            for (int k = 0; k < datedata1.Columns.Count; k++)
            {
                //add separator
                sb.Append(datedata1.Rows[i][k].ToString() + ',');
            }
            //append new line
            sb.Append("\r\n");
        }
        Response.Output.Write(sb.ToString());
        Response.Flush();
        Response.End();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (drpcircle.SelectedIndex > 0)
        {
            da = new mydataaccess1();
            ba1 = new myvodav23();
            DataTable datedata = new DataTable();
            datedata = da.Site_New_Download(drpcircle.SelectedItem.Text);
            GridView1.DataSource = datedata;
            GridView1.DataBind();
        }
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
}
