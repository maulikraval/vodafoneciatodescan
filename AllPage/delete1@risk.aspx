﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="delete1@risk.aspx.cs" Inherits="AllPage_admin_Circle_Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/admin_menu.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>PTW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/admin_styleinner1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/cufon-yui.js"></script>

    <script type="text/javascript" src="../js/arial.js"></script>

    <script type="text/javascript" src="../js/cuf_run.js"></script>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <link href="../css/menu.css" rel="stylesheet" type="text/css" />

    <script src="../js/menu.js" type="text/javascript"></script>

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function Confirmationbox() {
            var result = confirm('Are you sure you want to delete selected 1@Risk(s)?');
            if (result) {
                return true;
            }
            else {
                return false;
            }
        }

        $(function () {
            $('[id*=GridView1] tr').each(function () {
                var toolTip = $(this).attr("ID");
                $(this).find("td").each(function () {
                    $(this).simpletip({
                        content: toolTip
                    });
                });
                $(this).removeAttr("ID");
            });
        });
    </script>
    <style type="text/css">
        #roll a {
            display: inline-table;
            text-decoration: none;
        }

        #roll ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

            #roll ul li {
                list-style-type: none;
                background: none;
            }

                #roll ul li a, #roll ul li a:visited {
                    /* styles for the default button state */
                    margin: 0 0 5px 0;
                    padding: 0 15px;
                    line-height: 32px; /* this value must be at least twice the border-radius value */
                    color: #FFF;
                    text-shadow: 0px 0px 3px #000;
                    background: #EAEAEA url(/images/misc/pattern1.gif);
                    font-family: 'Arial Black', Impact, sans-serif;
                    font-size: 16px;
                    text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
                    letter-spacing: -.06em; /* should be set to 0 for most cases */
                    -moz-border-radius: 16px;
                    -khtml-border-radius: 16px;
                    -webkit-border-radius: 16px;
                    border-radius: 16px;
                }

                    #roll ul li a:hover {
                        /* styles for the rollover button state */
                        color: #FFF;
                        text-shadow: 0px 0px 3px #000;
                        background: #0099FF url(/images/misc/pattern2.gif);
                    }

        .style1 {
            width: 100%;
        }

        .style1 {
            width: 762px;
        }

        .style2 {
            width: 269px;
        }

        .style3 {
            width: 269px;
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <%--<asp:UpdatePanel ID="updatepanel" runat="server">
        <ContentTemplate>--%>
        <div class="main">
            <div class="header">
                <div class="header_resize">
                    <div class="logo">
                        <table width="100%">
                            <tr>
                                <td width="6%" rowspan="2">
                                    <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                                </td>
                                <td class="style1" rowspan="2">
                                   <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                                </td>
                                <td width="25%" style="text-align: right;">
                                    <asp:Label ID="lblusername" runat="server"></asp:Label>
                                    |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change 
                                        Password</asp:LinkButton>
                                    |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="25%" style="text-align: right;">
                                    <asp:LinkButton ID="lnkchangeproject" runat="server" OnClick="lnkchange_Click" CausesValidation="False"
                                        PostBackUrl="~/login/ciat_ptw.aspx" Visible="False">Change 
                                Project</asp:LinkButton>
                                </td>
                                <td>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr">
                    </div>
                    <uc2:Menu ID="menubar" runat="server" />
                    <div class="clr">
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="content">
                <div class="content_resize">
                    <div class="mainbar">
                        <div class="article1">
                            <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server" Text="" CssClass="lblmarquee"></asp:Label>
                    </marquee>
                        </div>
                    </div>
                    <div class="sidebar">
                        <div class="gadget">
                            <table class="style2">
                                <tr>
                                    <td class="style2">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3">
                                        <asp:DropDownList ID="drpcircle" runat="server" Width="200px" CssClass="genall"
                                            OnSelectedIndexChanged="drpcircle_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                            ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        <asp:Label ID="Label2" runat="server" Font-Bold="False" Text="Training Name" CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3">
                                        <asp:DropDownList ID="drptraining" runat="server" Width="200px" CssClass="genall">
                                            <asp:ListItem Value="0">Select/All</asp:ListItem>
                                            <asp:ListItem Value="1">HSW training on Height FARM training</asp:ListItem>
                                            <asp:ListItem Value="2">HSW training on Electrical Safety</asp:ListItem>
                                            <asp:ListItem Value="3">HSW training on OFC Safety</asp:ListItem>
                                            <asp:ListItem Value="4">HSW training on RF & EMF safety</asp:ListItem>
                                            <asp:ListItem Value="5">Defenssive Driving - 2W</asp:ListItem>
                                            <asp:ListItem Value="6">Defenssive Driving - 4W</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drptraining"
                                            ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">&nbsp;
                                    </td>
                                    &nbsp;</td>
                                </tr>
                           <tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnsearch" runat="server" Text="Search" OnClick="btnsearch_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="mainbar">
                        <div class="article">
                            <table width="100%">
                                <%--<tr>
                                <td width="5%">
                                    <asp:Label ID="Label12" runat="server" Font-Names="Calibri" Font-Size="13px" ForeColor="#FFA500"
                                        Text="Search For : "></asp:Label>
                                    <asp:TextBox ID="txtsearxh" runat="server"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="tw" runat="server" WatermarkText="SiteID" WatermarkCssClass="waterback"
                                        TargetControlID="txtsearxh">
                                    </cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars"
                                        InvalidChars="'#&amp;$%^*()+=!|\@/?;:{}[]`~," TargetControlID="txtsearxh">
                                    </cc1:FilteredTextBoxExtender>
                                    <asp:Button ID="btnsitesearch" runat="server" Text="Search" OnClick="btnsitesearch_Click" />
                                </td>
                                <td width="65%">
                                </td>
                                <td width="5%">
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" style="text-align: center">
                                    <asp:Label ID="rptlable" runat="server" Font-Bold="False" CssClass="lblstly"></asp:Label>
                                </td>
                                <td width="65%" style="text-align: center">
                                    &nbsp;
                                </td>
                            </tr>--%>
                                <%--<tr>
                                <td width="5%" style="text-align: center">
                                    <hr />
                                </td>
                            </tr>--%>
                                <tr>
                                    <td style="vertical-align: top;" width="25%">
                                        <div style="width: 100%;">
                                            <asp:ImageButton Visible="false" ID="ImageButton2" runat="server" ImageUrl="~/images/page_excel.png"
                                                OnClick="ImageButton11_Click" />
                                        </div>
                                        <div>
                                            <table class="style2">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                                            OnClick="ImageButton1_Click" ToolTip="Export To Excel" Width="25px" />
                                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="720px" Height="500px">
                                                            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"
                                                                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                                                ForeColor="Black" Width="100%" OnRowDataBound="GridView1_RowDataBound">
                                                                <FooterStyle BackColor="White" />
                                                                <RowStyle HorizontalAlign="Center" />
                                                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                                                <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <asp:Button ID="ButtonDelete" runat="server" Text="Delete" OnClick="ButtonDelete_Click" OnClientClick="javascript:return Confirmationbox();" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td width="5%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
            </div>
            <div class="fbg">
                <div class="footer">
                    <div id="Copyright 2011">
                        <a href="http://apycom.com/"></a>
                    </div>
                    <%-- <p class="lf">
                    © Copyright 2012 By.om">Teleysia Networks Pvt. Ltd.</a></p>--%>
                    <div class="clr">
                    </div>
                </div>
            </div>
        </div>
        <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
