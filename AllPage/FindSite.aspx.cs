﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FindDataAccessLayer;
//using mybusiness;
using System.Data;

public partial class Support_FindSite : System.Web.UI.Page
{
    FindDataAccess da;
    //myvodav2 ba;
    DataTable dt;
    int Site_Id;
    float Lat;
    float Long;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txt_Username.Text = Request.QueryString["UserName"];
            btn_find_site.Enabled = false;
            btnclose.Visible = false;
            btnviewdis.Visible = false;
            txtdesc.Visible = false;
            tr1.Visible = false;
            lbdis.Visible = false;
            txtdiss.Visible = false;
            divtt.Visible = false;
        }
    }

    public void Fill_GV_Site(DataTable Table)
    {
        gv_site.DataSource = Table;
        gv_site.DataBind();
        gv_site.Visible = true;
        if (Table.Rows.Count > 0)
        {
            btn_find_distance.Visible = true;
        }
        else
        {
            btn_find_distance.Visible = false;
            ViewState["DateGPS"] = "0";
            ViewState["DateNetwork"] = "0";
            ViewState["LatGPS1"] = "0";
            ViewState["LongGPS1"] = "0";
            ViewState["LatIMEINP"] = "0";
            ViewState["LongIMEINP"] = "0";
            ViewState["DistanceGPS"] = "0";
            ViewState["DistanceNetwork"] = "0";
            ViewState["SiteLat"] = "0";
            ViewState["SiteLong"] = "0";
            ViewState["lat"] = "0";
            ViewState["long"] = "0";
        }
    }
    protected void btn_find_site_Click(object sender, EventArgs e)
    {
	panel1.Visible=true;
        da = new FindDataAccess();
        //ba = new myvodav2();
        DataTable Site = new DataTable();
        Site = da.searchsiteid_su(txt_Site.Text);
        Fill_GV_Site(Site);
        for (int i = 0; i < Site.Rows.Count; i++)
        {
            if (Site.Rows[i]["id"].ToString() == "")
            {
            }
            else
            {
                ViewState["Site_Id"] = Site.Rows[i]["id"].ToString();
                break;
            }
        }
        btnstart.Visible = false;

    }
    protected void btn_find_distance_Click(object sender, EventArgs e)
    {
	panel2.Visible=true;
        da = new FindDataAccess();
        DataTable Distance = new DataTable();
        Distance = da.Site_User_Distance_su(txt_Site.Text, txt_Username.Text);
        gv_distance.DataSource = Distance;
        gv_distance.DataBind();
        gv_distance.Visible = true;
        btn_show_site.Visible = true;
        btn_find_distance.Visible = false;
        if (Distance.Rows.Count > 0)
        {
            ViewState["DateGPS"] = Distance.Rows[0]["DateGPS"].ToString();
            ViewState["DateNetwork"] = Distance.Rows[0]["DateNetwork"].ToString();
            ViewState["LatGPS1"] = Distance.Rows[0]["LatIMEIGPS"].ToString();
            ViewState["LongGPS1"] = Distance.Rows[0]["LongIMEIGPS"].ToString();
            ViewState["LatIMEINP"] = Distance.Rows[0]["LatIMEINP"].ToString();
            ViewState["LongIMEINP"] = Distance.Rows[0]["LongIMEINP"].ToString();
            ViewState["DistanceGPS"] = Distance.Rows[0]["DistanceGPS"].ToString();
            ViewState["DistanceNetwork"] = Distance.Rows[0]["DistanceNetwork"].ToString();
            ViewState["SiteLat"] = Distance.Rows[0]["SiteLat"].ToString();
            ViewState["SiteLong"] = Distance.Rows[0]["SiteLong"].ToString();
            if (Convert.ToDouble(Distance.Rows[0]["DistanceGPS"].ToString()) >= 2000 || Convert.ToDouble(Distance.Rows[0]["DistanceNetwork"].ToString()) >= 2000)
            {
                lbdis.Visible = true;
                txtdiss.Visible = true;
            }
            else
            {
                lbdis.Visible = false;
                txtdiss.Visible = false;
            }
        }
        else
        {
            ViewState["DateGPS"] = "0";
            ViewState["DateNetwork"] = "0";
            ViewState["LatGPS1"] = "0";
            ViewState["LongGPS1"] = "0";
            ViewState["LatIMEINP"] = "0";
            ViewState["LongIMEINP"] = "0";
            ViewState["DistanceGPS"] = "0";
            ViewState["DistanceNetwork"] = "0";
            ViewState["SiteLat"] = "0";
            ViewState["SiteLong"] = "0";
            ViewState["lat"] = "0";
            ViewState["long"] = "0";
            lbdis.Visible = true;
            txtdiss.Visible = true;
        }
    }
    protected void btn_show_site_Click(object sender, EventArgs e)
    {

        string description = "";

        try
        {
            if (Convert.ToDateTime(ViewState["DateNetwork"].ToString()) > Convert.ToDateTime(ViewState["DateGPS"].ToString()))
            {
                ViewState["lat"] = ViewState["LatIMEINP"].ToString();
                ViewState["long"] = ViewState["LongIMEINP"].ToString();
                description = "Site master data updated user far from site Network Distance " + ViewState["DistanceNetwork"].ToString() + "m";
                ViewState["description"] = description;

                if (Convert.ToDouble(ViewState["DistanceNetwork"].ToString()) <= 2000)
                {
                    da = new FindDataAccess();
                    da.select_update_lat_long_su(float.Parse(ViewState["LatIMEINP"].ToString()), float.Parse(ViewState["LongIMEINP"].ToString()), Convert.ToInt32(ViewState["Site_Id"].ToString()), 1, txt_Username.Text);
                    btnclose.Visible = true;
                    btnviewdis.Visible = true;
                }
                else
                {
                    if (txtdiss.Text != "")
                    {
                        description = "Site master data updated user far from site Network Distance " + txtdiss.Text + "m";
                        ViewState["description"] = description;
                        da = new FindDataAccess();
                        da.manual_update_lat_long(txt_Username.Text, txt_Site.Text);
                        btnclose.Visible = true;
                        btnviewdis.Visible = true;
                    }
                    else
                    {
                        lblerror.Text = "Please Enter Distance!!!";
                    }
                }
            }
            else
            {
                ViewState["lat"] = ViewState["LatGPS1"].ToString();
                ViewState["long"] = ViewState["LongGPS1"].ToString();
                description = "Site master data updated user far from site GPS Distance " + ViewState["DistanceGPS"] + "m ";
                ViewState["description"] = description;
                if (Convert.ToDouble(ViewState["DistanceGPS"].ToString()) <= 2000)
                {
                    da = new FindDataAccess();
                    da.select_update_lat_long_su(float.Parse(ViewState["LatGPS1"].ToString()), float.Parse(ViewState["LongGPS1"].ToString()), Convert.ToInt32(ViewState["Site_Id"].ToString()), 1, txt_Username.Text);
                    btnclose.Visible = true;
                    btnviewdis.Visible = true;
                }
                else
                {
                    if (txtdiss.Text != "")
                    {
                        description = "Site master data updated user far from site Network Distance " + txtdiss.Text + "m";
                        ViewState["description"] = description;

                        da = new FindDataAccess();
                        da.manual_update_lat_long(txt_Username.Text, txt_Site.Text);
                        btnclose.Visible = true;
                        btnviewdis.Visible = true;
                    }
                    else
                    {
                        lblerror.Text = "Please Enter Distance!!!";
                    }

                    //da.select_update_lat_long_su(float.Parse(ViewState["LatGPS"].ToString()), float.Parse(ViewState["LongGPS"].ToString()), Convert.ToInt32(ViewState["Site_Id"].ToString()));

                }
            }
        }
        catch (Exception ex)
        {
            ViewState["lat"] = ViewState["LatGPS1"].ToString();
            ViewState["long"] = ViewState["LongGPS1"].ToString();
            description = "Site master data updated user far from site GPS Distance " + ViewState["DistanceGPS"] + "m ";
            ViewState["description"] = description;
            if (Convert.ToDouble(ViewState["DistanceGPS"].ToString()) <= 2000)
            {
                da = new FindDataAccess();
                da.select_update_lat_long_su(float.Parse(ViewState["LatGPS1"].ToString()), float.Parse(ViewState["LongGPS1"].ToString(), System.Globalization.CultureInfo.InvariantCulture), Convert.ToInt32(ViewState["Site_Id"].ToString()), 1, txt_Username.Text);
                btnclose.Visible = true;
                btnviewdis.Visible = true;
            }
            else
            {
                if (txtdiss.Text != "")
                {
                    description = "Site master data updated user far from site Network Distance " + txtdiss.Text + "m";
                    ViewState["description"] = description;
                    da = new FindDataAccess();
                    da.manual_update_lat_long(txt_Username.Text, txt_Site.Text);
                    btnclose.Visible = true;
                    btnviewdis.Visible = true;
                }
                else
                {
                    lblerror.Text = "Please Enter Distance!!!";
                }

            }
        }
      

    }

    protected void btnstart_Click(object sender, EventArgs e)
    {
        btn_find_site.Enabled = true;
        ViewState["opentime"] = System.DateTime.Now;
        btnstart.Visible = false;
        divtt.Visible = true;
    }
    protected void btnclose_Click(object sender, EventArgs e)
    {
        Session["closetime"] = System.DateTime.Now;
        string issue = "";

        issue = "Change LAT/LONG Of Site";

        DateTime end = Convert.ToDateTime(Session["closetime"].ToString());
        DateTime start = Convert.ToDateTime(ViewState["opentime"].ToString());

        int totalminutes = (int)(end - start).TotalMinutes;
        // double totalminutes = (Convert.ToDateTime( Session["closetime"].ToString()) - Convert.ToDateTime( Session["opentime"].ToString())).TotalMinutes;
        da = new FindDataAccess();
        da.insert_tt(drpproject.Text, txt_Site.Text, txt_Username.Text, drpcompany.Text, issue, ViewState["description"].ToString(), Convert.ToDateTime(ViewState["opentime"].ToString()), Convert.ToDateTime(Session["closetime"].ToString()), Convert.ToInt32(totalminutes), "Closed", drpsupport.Text, ViewState["SiteLat"].ToString(), ViewState["SiteLong"].ToString(), ViewState["lat"].ToString(), ViewState["long"].ToString());

        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Ticket Closed successfully!!!');window.location ='FindUser.aspx';", true);
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DropDownList1.Text == "Other")
        {
            txtdesc.Visible = true;
        }
        else
        {
            txtdesc.Visible = false;
        }
    }
    protected void btnviewdis_Click(object sender, EventArgs e)
    {
        da = new FindDataAccess();
        DataTable Distance = new DataTable();
        Distance = da.Site_User_Distance_su(txt_Site.Text, txt_Username.Text);
        gv_distance.DataSource = Distance;
        gv_distance.DataBind();
        gv_distance.Visible = true;
        btn_show_site.Visible = true;
    }
}