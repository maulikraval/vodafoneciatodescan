﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AllPage/AllCommon.master" CodeFile="NSS_NodeId_Delete.aspx.cs" Inherits="NSS_NodeId_Delete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>HSW | Teleysia Networks Pvt. Ltd.</title>
            <script type="text/javascript">
                function Confirmationbox() {
                    var result = confirm('Are you sure you want to Delete?');
                    if (result) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            </script>
        </head>
        <body>
            <div align="center">
                <table style="text-align: center">
                    <tr>
                <td>
                    <asp:Label ID="lbl_User" runat="server" Text="NSS ID"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_nssid" runat="server"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_nssid" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                    
                </td>
            </tr>
  		    <tr>
                <td>
                    <asp:Label ID="lbl_contact1" runat="server" Text="NODE ID"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_node" runat="server"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_node" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                  
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
                <td>
                    &nbsp
                </td>

                <td style="text-align:center">
                    <asp:Button ID="btn_search_nssnodeid" runat="server" Text="SEARCH"  OnClick="btn_search_nssnodeid_Click" />
                    <%--<asp:Button ID="btn_find_contact" runat="server" Text="DELETE" OnClientClick="javascript:return Confirmationbox();" OnClick="btn_find_contact_Click" />--%>
                </td>
                <td>
                  </td>
                <td>
                    &nbsp
                </td>
                
            </tr>
                    <%--<tr>
                        <td>
                            
                        </td>

                    </tr>--%>
                </table>
                <asp:Panel ID="pnlgrd1" Width="730px" ScrollBars="Both" runat="server" Height="450px">
                            <asp:GridView ID="grd1" runat="server" OnSelectedIndexChanged="grd1_SelectedIndexChanged" OnRowDeleting="grd1_selectedrowdeleting"
                                BackColor="WhiteSmoke" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px"
                                CellPadding="4" CellSpacing="2" ForeColor="Black" Width="100%">
                                <EmptyDataRowStyle BackColor="#DA1918" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#DA1918" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:CommandField HeaderText="Delete" ShowSelectButton="True" SelectText="Delete" />
                                </Columns>
                            </asp:GridView>
                            <center>
                                <asp:Label ID="Label2" runat="server" CssClass="lblall" ForeColor="Red"></asp:Label></center>
                        </asp:Panel>
            </div>
            
        </body>
        </html>
    </div>
</asp:Content>
