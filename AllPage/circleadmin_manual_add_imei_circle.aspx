﻿<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="circleadmin_manual_add_imei_circle.aspx.cs" Inherits="admin_manual_add_imei" Title="Untitled Page" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td style="width: 5%">
                &nbsp;</td>
            <td style="text-align: center;" colspan="2">
                    <asp:Label ID="Label2" runat="server" Font-Bold="False"
                        Font-Underline="False" Text="Add IMEI" CssClass="lblstly"></asp:Label>
            </td>
            <td style="width: 5%">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 5%">
            </td>
            <td style="text-align: center;" colspan="2">
            <hr />
            </td>
            <td style="width: 5%">
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                <asp:Label ID="lblimei" runat="server" Text="IMEI No. :" CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:TextBox ID="txtimei" runat="server" Width="150px" CssClass="lblall" 
                    MaxLength="20"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtimei"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                    TargetControlID="txtimei" ValidChars="0123456789.">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                <asp:Label ID="lblimeimodel" runat="server" Text="IMEI Model :" 
                    CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:TextBox ID="txtimeimodel" runat="server" Width="150px" CssClass="lblall"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtimeimodel"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                    InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|='()" 
                    TargetControlID="txtimeimodel" FilterMode="InvalidChars">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                <asp:Label ID="lblcircle" runat="server" Text="Circle :" CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 55%; text-align: left;">
                    <asp:DropDownList ID="ddlcircle" runat="server" Width="150px" CssClass="genall">
                    </asp:DropDownList>
                
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlcircle"
                        ErrorMessage="*" SetFocusOnError="True" InitialValue="--Select--"></asp:RequiredFieldValidator>
                
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 26px;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right; height: 26px;">
                &nbsp;
            </td>
            <td style="width: 55%; text-align: left; height: 26px;">
                &nbsp;
            </td>
            <td style="width: 5%; height: 26px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                &nbsp;
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:Button ID="btnadd" runat="server"  Text="Add" onclick="btnadd_Click" 
                    Width="55px" />
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                &nbsp;
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:Label ID="lblmsg" runat="server"></asp:Label>
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 20px;">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right; height: 20px;">
                &nbsp;
            </td>
            <td style="width: 55%; text-align: left; height: 20px;">
                &nbsp;
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td style="width: 5%; height: 20px;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 20px;">
            </td>
            <td style="width: 35%; text-align: right; height: 20px;">
            </td>
            <td style="width: 55%; text-align: left; height: 20px;">
                &nbsp;
            </td>
            <td style="width: 5%; height: 20px;">
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="text-align: center;" colspan="2">
                &nbsp;
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">
                &nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                &nbsp;
            </td>
            <td style="width: 55%; text-align: left;">
                &nbsp;
            </td>
            <td style="width: 5%">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
