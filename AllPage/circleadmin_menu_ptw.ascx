﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="circleadmin_menu_ptw.ascx.cs"
    Inherits="admin_menu" %>
<div id="menu">
    <ul class="menu">
        <li><a href="#" class="parent"><span>Dashboard</span></a>
            <ul>
                <li><a href="circleadmin_20daysreminder_ptw.aspx"><span>PTW</span></a> </li>
                <li><a href="circleadmin_20daysreminder_ptw_night.aspx"><span>T@DH</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>User Details</span></a>
            <ul>
                <%--  <li><a href="circleadmin_adduser_circle_both.aspx"><span>Create User</span></a> </li>--%>
                <li><a href="circleadmin_adduser_circle.aspx"><span>Create User</span></a> </li>
                <li><a href="circleadmin_showuser_circle.aspx"><span>View User</span></a> </li>
                <li><a href="circleadmin_viewCompanyIssuer.aspx"><span>View Issuer Company</span></a>
                </li>
              <%--  <li><a href="circleadmin_viewCompanyReceiver.aspx"><span>1@Risk Company</span></a>
                </li>--%>
                <li><a href="circleadmin_1_at_risk.aspx"><span>View 1@Risk</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Site Details</span></a>
            <ul>
                <li><a href="circleadmin_UploadSitesNew_circle.aspx"><span>Upload Site</span></a></li>
                <li><a href="circleadmin_ViewSiteCircle1_ptw.aspx"><span>View Site</span></a> </li>
                <li><a href="circleadmin_view_site_report.aspx"><span>Site Master</span></a> </li>
                <li><a href="circleadmin_approve_lat_long.aspx"><span>Check Distance</span></a>
                </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Mobile Details</span></a>
            <ul>
                <li><a href="circleadmin_manual_add_imei_circle.aspx"><span>Add Mobile Manually</span></a></li>
                <li><a href="circleadmin_UploadImei_circle.aspx"><span>Upload Mobile</span></a></li>
                <li><a href="circleadmin_ViewImei_circle.aspx"><span>View Register Mobile</span></a>
                </li>
            </ul>
        </li>
        <li><a href="circleadmin_ptw_finduser.aspx" class="parent"><span>Find Sites</span></a></li>
        <li><a href="#" class="parent"><span>View Report</span></a>
            <ul>
                <li><a href="circleadmin_Circle_Report_ptw.aspx"><span>PTW Customize Report</span></a></li>
                <li><a href="circleadmin_Circle_Report_ptw_night.aspx"><span>T@DH Customize Report</span></a></li>
                <li><a href="circleadmin_pdf_Report.aspx"><span>PTW PDF Report</span></a></li>
                <li><a href="circleadmin_PDF_Report_night.aspx"><span>T@DH PDF Report</span></a></li>
                <li><a href="circleadmin_risk_Report_ptw.aspx"><span>PTW Master Report</span></a></li>
            </ul>
        </li>
        <li><a href="circleadmin_all_purpose.aspx" class="parent"><span>Issuer Mapping</span></a></li>
        <li><a href="#" class="parent"><span>View Charts</span></a>
            <ul>
                <li><a href="circleadmin_ptw_type_chart.aspx"><span>Type Wise</span></a></li>
                <li><a href="circleadmin_chart_status.aspx"><span>Status Wise</span></a></li>
                <li><a href="circleadmin_chart_risk.aspx"><span>Risk Wise</span></a></li>
                <li><a href="circleadmin_chart_reject_reason.aspx"><span>Rejection Wise</span></a></li>
                <li><a href="circleadmin_ptw_type_chart_night.aspx"><span>T@DH Type</span></a></li>
                <li><a href="circleadmin_chart_status_night.aspx"><span>T@DH Status</span></a></li>
            </ul>
        </li>
        
    </ul>
</div>
