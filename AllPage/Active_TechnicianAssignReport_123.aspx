﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Active_TechnicianAssignReport_123.aspx.cs" 
Inherits="PMT_Active_TechnicianAssignReport_" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/pmt_menu.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <title>PTW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/admin_styleinner1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/cufon-yui.js"></script>
    <script type="text/javascript" src="../js/arial.js"></script>
    <script type="text/javascript" src="../js/cuf_run.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <link href="../css/menu.css" rel="stylesheet" type="text/css" />
    <script src="../js/menu.js" type="text/javascript"></script>
    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #roll a
        {
            display: inline-table;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 762px;
        }
        .style2
        {
            width: 269px;
        }
        .style3
        {
            width: 269px;
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <div class="main">
        <div class="header_resize">
            <div class="logo">
                <table width="100%">
                    <tr>
                        <td width="6%" rowspan="2">
                            <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                        </td>
                        <td class="style1" rowspan="2">
                           <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                        </td>
                        <td width="25%" style="text-align: right;">
                            <asp:Label ID="lblusername" runat="server"></asp:Label>
                            |
                            <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change 
                                        Password</asp:LinkButton>
                            |
                            <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%" style="text-align: right;">
                            <asp:LinkButton ID="lnkchangeproject" runat="server" OnClick="lnkchange_Click" CausesValidation="False"
                                PostBackUrl="~/login/ciat_ptw.aspx" Visible="False">Change 
                                Project</asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clr">
            </div>
            <uc2:Menu ID="menubar" runat="server" />
            <div class="clr">
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
               <%-- <div class="mainbar">
                    <div class="article1">
                  
                    </div>
                </div>
              --%>
                <div class="mainbar" style="float:none">
                    <div class="article"  style="width:96%">
                        <%--<table width="100%">--%>
                            <table style="border-spacing: 7px !important">
            <tr>
                <td>
                  
                </td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
           <%-- <tr>
                <td>
                    <asp:Label ID="lbl_Username" runat="server" Text="Username"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_Username" runat="server" Text="" Enabled="false"></asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <%--  <asp:Label ID="Label6" runat="server" Font-Names="Calibri" Font-Size="13px" ForeColor="#FFA500"
                                        Text="Search Site ID : "></asp:Label>--%>
                    <asp:Label ID="lbl_Site" runat="server" Text="Search Site Id" ForeColor="Black"></asp:Label> </td>
                <td>
                                    <asp:TextBox ID="txtsearxh" runat="server"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="tw" runat="server" WatermarkText="Site ID" WatermarkCssClass="waterback"
                                        TargetControlID="txtsearxh">
                                    </cc1:TextBoxWatermarkExtender>
                                    <asp:Button ID="btnsitesearch" runat="server" Text="Search" CausesValidation ="false"
                                        onclick="btnsitesearch_Click" />
                  <%--  <asp:Label ID="lbl_Site" runat="server" Text="Site Id"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_Site" runat="server" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_Site" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                </td>
            </tr>

              <tr>
                <td>  <asp:Label ID="lbl_tech" runat="server" Text="Technician Name" ForeColor="Black"></asp:Label> </td>
                <td>
                <asp:TextBox ID="txt_techname" runat="server" Text="" Enabled="false"></asp:TextBox></td>
                
            </tr>
            <%-- <tr>
                <td>  <asp:Label ID="lbl_newtech" runat="server" Text="New Technician Name"></asp:Label> </td>
                <td>
                <asp:DropDownList ID="ddltechnicianname" runat="server" AutoPostBack="false" Width="200px"
                                                            CssClass="genall">
                                                        </asp:DropDownList></td>
                
            </tr>--%>

                                 <tr>
                <td>  <asp:Label ID="lbl_findtech" runat="server" Text="New Technician Name" ForeColor="Black" ></asp:Label> </td>
                <td>
                <%--<asp:DropDownList ID="" runat="server" AutoPostBack="false" Width="200px"
                                                            CssClass="genall">
                                                        </asp:DropDownList></td>--%>
                     <asp:TextBox ID="txt_findtech" runat="server" Text=""></asp:TextBox>
                  <asp:Button ID="btn_findtech" runat="server" Text="Find" CausesValidation ="false"
                                        onclick="btnfindtech_Click" />
            </tr>
            <tr>
                <td></td>
                <td> <asp:GridView ID="gv_user" runat="server" EnableModelValidation="True" OnRowCommand="gv_user_RowCommand" Visible="false"
        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="30%">
        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
        <RowStyle Font-Names="Calibri" Font-Size="13px" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

        <Columns>
            <asp:ButtonField Text="Select" CommandName="Select" ItemStyle-Width="30" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lbl_gv" runat="server" Text="No Data Found...!!" ForeColor="White"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView></td>
            </tr>

          <%--  <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Project"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="drpproject" runat="server">
                        <asp:ListItem>CIAT</asp:ListItem>
                        <asp:ListItem>PTW</asp:ListItem>
  <asp:ListItem>G-PM</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>--%>
          
            <%--<tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Issue Handler"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="drpsupport" runat="server">
                        <asp:ListItem>---Select Your Name---</asp:ListItem>
                        <asp:ListItem>Suresh</asp:ListItem>
                        <asp:ListItem>Vishal</asp:ListItem>
                        <asp:ListItem>Poem</asp:ListItem>
                        <asp:ListItem>Dixit</asp:ListItem>
                        <asp:ListItem>Sumit</asp:ListItem>
                        <asp:ListItem>Chetan</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpsupport" ErrorMessage="*" InitialValue="---Select Your Name---"></asp:RequiredFieldValidator>
                </td>
            </tr>--%>
           <%-- <tr id="tr1" runat="server">
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Issue Category" Visible="false"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" Visible="false" AutoPostBack="false" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem>---Select Category---</asp:ListItem>
                        <asp:ListItem>Change LAT/LONG Of Site</asp:ListItem>
                        <asp:ListItem>Site not found due to user is far from Site</asp:ListItem>
                        <asp:ListItem>Site not found due to user has selected Wrong Zone</asp:ListItem>
                        <asp:ListItem>Site Not Exist in system</asp:ListItem>
                        <asp:ListItem>Already Sent To Circle admin for request to recheck</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>

                    </asp:DropDownList>
                    <br />
                    <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" Width="316px"></asp:TextBox>
                </td>
            </tr>--%>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td> <asp:Button ID="btn_updatetech" runat="server" Text="UPDATE" OnClick="btn_updatetech_Click" Width="155px" Height="40px" Visible="false" /></td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                  <%--  &nbsp;<asp:Button ID="btnclose" runat="server" Text="Close Your Ticket" OnClick="btnclose_Click" Height="40px" />

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_find_site" runat="server" Text="Find" OnClick="btn_find_site_Click" Width="155px" Height="40px" />
                 --%>   <br />
                    <br />

                    <br />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
                        <%--</table>--%>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
                <%--    <p class="lf">
                    © Copyright 2012 By.om&quot;&gt;Teleysia Networks Pvt. Ltd.</a></p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
