﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pm_admin_chart_status_night.aspx.cs" Inherits="PM_rptgrid"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/pm_menu.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HSW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/admin_styleinner1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/cufon-yui.js"></script>

    <script type="text/javascript" src="../js/arial.js"></script>

    <script type="text/javascript" src="../js/cuf_run.js"></script>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <link href="../css/menu.css" rel="stylesheet" type="text/css" />

    <script src="../js/menu.js" type="text/javascript"></script>

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #roll a
        {
            display: inline-table;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 762px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

    <script src="../FusionCharts/FusionCharts.js" type="text/javascript"></script>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="updatepanel" runat="server">
        <ContentTemplate>--%>
    <div class="main">
        <div class="header">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                        <tr>
                            <td width="6%">
                                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style1">
                                <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change Password</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clr">
                </div>
                 <uc2:Menu ID="menubar" runat="server" />

                <div class="clr">
                </div>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
                <div class="mainbar">
                    <div class="article1">
                        <marquee behaviour="slide" direction="left" scrollamount="3">
                        <asp:Label ID="lblmarquee" runat="server" CssClass="lblmarquee"></asp:Label></marquee>
                    </div>
                </div>
                <div class="sidebar">
                    <div class="gadget">
                        <table class="style2">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:DropDownList ID="drpcircle" runat="server" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged"
                                        Width="200px" CssClass="genall" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <br />
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Font-Bold="False" Text="Issuer Company" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlissuercompany" runat="server" Width="200px" CssClass="genall"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlissuercompany_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlissuercompany"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Font-Bold="False" Text="Issuer" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlissuer" runat="server" Width="200px" CssClass="genall">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlissuer"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label10" runat="server" Font-Bold="False" Text="Receiver Company"
                                        CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlreceivercompany" runat="server" Width="200px" CssClass="genall">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlreceivercompany"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="False" Text="From Date" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox1"
                                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label5" runat="server" Font-Bold="False" Text="To Date" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox2"
                                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TextBox2">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Submit" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="mainbar">
                    <div class="article">
                        <table width="100%">
                            <tr>
                                <td style="vertical-align: top;" width="25%">
                                    <div>
                                        <table class="style2">
                                            <tr>
                                                <td>
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="panel2" runat="server" Width="700px"
                                                        Height="400px">
                                                        <div style="text-align: center">
                                                            <asp:Label ID="lbl1" runat="server" Font-Size="11px" ForeColor="Black"></asp:Label></div>
                                                        <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    <br />
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="panel1" runat="server" Width="700px"
                                                        ScrollBars="Auto" Height="400px">
                                                        <div style="text-align: center">
                                                            <asp:Label ID="lbl2" runat="server" Font-Size="11px" ForeColor="Black"></asp:Label></div>
                                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    <br />
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="panel3" runat="server" Width="700px"
                                                        ScrollBars="Auto" Height="400px">
                                                        <div style="text-align: center">
                                                            <asp:Label ID="lbl3" runat="server" Font-Size="11px" ForeColor="Black"></asp:Label></div>
                                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td width="5%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
             <%--   <p class="lf">
                    &copy; Copyright 2011 <a href="#"></a>.</p>
                <p class="rf">
                    Powered By. Teleysia Networks Pvt. Ltd.</p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
    </div>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
