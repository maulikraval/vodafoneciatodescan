﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin_delete_sitenewmaster.aspx.cs"
    Inherits="AllPage_admin_delete_sitenewmaster" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%; text-align: center">
            <tr>
                <td colspan="2" style="width: 100%; text-align: center;">
                    <h1>
                        Site Delete</h1>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; text-align: right;">
                   <h4> Circle</h4>
                </td>
                <td style="width: 50%; text-align: left;">
                    <asp:DropDownList ID="drpcircle" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged"
                        Width="200px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; text-align: right;">
                    <h4>
                        Zone</h4>
                </td>
                <td style="width: 50%; text-align: left;" width="150px">
                    <asp:DropDownList ID="ddlzone" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlzone_SelectedIndexChanged"
                        Width="200px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlzone"
                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <%--<asp:TextBox ID="txtsiteid" runat="server"></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; text-align: right;">
                    <h4>
                        Siteid</h4>
                </td>
                <td style="width: 50%; text-align: left;">
                    <asp:DropDownList ID="ddlsiteid" runat="server" OnSelectedIndexChanged="ddlsiteid_SelectedIndexChanged"
                        AutoPostBack="True" Width="200px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlsiteid"
                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <%--<asp:TextBox ID="txtchecksheet" runat="server"></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td style="width: 50%; text-align: right;">
                    <h4>
                        Check Sheet</h4>
                </td>
                <td style="width: 50%; text-align: left;">
                    <asp:DropDownList ID="ddlchecksheet" runat="server" Width="200px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlchecksheet"
                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <%--<asp:TextBox ID="txtchecksheet" runat="server"></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%; text-align: center;">
                    <asp:Button ID="btndelete" runat="server" Text="Delete" OnClick="btndelete_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 100%; text-align: center;">
                    <asp:Label ID="lblmsg" runat="server" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
