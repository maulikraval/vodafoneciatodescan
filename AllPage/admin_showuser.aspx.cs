﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;

public partial class AllPage_admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            da = new mydataaccess1();
            string sp_user_ = da.select_user_cookie(Session["user"].ToString());
            if (Session["role"].ToString() != "4" && Session["um"].ToString() != sp_user_)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {
                try
                {
                    // da = new mydataaccess1();
                    //  ba = new myvodav2();
                    //  dt = new DataTable();
                    //  dt = da.showuser(ba);
                    //  grduser.DataSource = dt;
                    //   grduser.DataBind();
                    mydataaccess1 da1 = new mydataaccess1();
                    DataTable dt1 = new DataTable();
                    dt1 = da1.select_company();
                    ddlcompany.DataTextField = "company_name";
                    ddlcompany.DataSource = dt1;
                    ddlcompany.DataBind();
                    ddlcompany.Items.Insert(0, "---Select Company---");
                }
                catch
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }
        }
    }
    protected void grduser_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {


            GridViewRow gr = grduser.SelectedRow;


            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            dt = da.selectcircle(ba);
            ddlcircleupdate.DataTextField = "circle";
            ddlcircleupdate.DataSource = dt;
            ddlcircleupdate.DataBind();

            ddlcircleupdate.Items.Insert(0, "---Select---");


            // company
            mydataaccess1 da1 = new mydataaccess1();
            DataTable dt1 = new DataTable();
            dt1 = da1.select_company();
            ddlcompany.DataTextField = "company_name";
            ddlcompany.DataSource = dt1;
            ddlcompany.DataBind();
            ddlcompany.Items.Insert(0, "---Select Company---");

            if (gr.Cells[12].Text == "&nbsp;")
            {
                txtsubcompany.Text = "";
            }
            else
            {
                txtsubcompany.Text = SpacialCharRemove.XSS_Remove(gr.Cells[12].Text);
            }
            if (gr.Cells[11].Text == "&nbsp;")
            {
                ddlcompany.SelectedIndex = 0;
            }
            else
            {
                ddlcompany.Text = SpacialCharRemove.XSS_Remove(gr.Cells[11].Text);
            }


            txtuserupdate.Text = SpacialCharRemove.XSS_Remove(gr.Cells[3].Text);
            // ddlroleupdate.Text = gr.Cells[3].Text;
            ddlproject0.Text = SpacialCharRemove.XSS_Remove(gr.Cells[9].Text);
            ddlroleupdate.SelectedIndex = 0;
            ddlroleupdate0.SelectedIndex = 0;
            if (gr.Cells[9].Text == "CIAT")
            {
                ddlroleupdate.Enabled = true;

                ddlroleupdate.Text = SpacialCharRemove.XSS_Remove(gr.Cells[4].Text);
                ddlroleupdate0.Enabled = false;
            }
            if (gr.Cells[9].Text == "PTW")
            {
                ddlroleupdate.Enabled = false;
                ddlroleupdate0.Enabled = true;
                ddlroleupdate0.Text = SpacialCharRemove.XSS_Remove(gr.Cells[5].Text);
            }
            if (gr.Cells[9].Text == "BOTH")
            {
                ddlroleupdate.Enabled = true;
                ddlroleupdate0.Enabled = true;
                ddlroleupdate.Text = SpacialCharRemove.XSS_Remove(gr.Cells[4].Text);
                ddlroleupdate0.Text = SpacialCharRemove.XSS_Remove(gr.Cells[5].Text);
            }
            ddlcircleupdate.Text = SpacialCharRemove.XSS_Remove(gr.Cells[8].Text);
            {

                if (ddlcircleupdate.SelectedIndex != 0)
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();
                    ba.Circle = ddlcircleupdate.SelectedValue;
                    dt = da.getzonesbycirclename(ba);
                    ddlzone.DataTextField = "zone";
                    ddlzone.DataSource = dt;
                    ddlzone.DataBind();
                    ddlzone.Items.Insert(0, "---Select---");
                }
                else
                {
                    ddlzone.Items.Clear();
                    ddlzone.DataTextField = "zone";
                    ddlzone.DataSource = null;
                    ddlzone.DataBind();
                    ddlzone.Items.Insert(0, "---Select---");
                }
            }
            txtemailid.Text = SpacialCharRemove.XSS_Remove(gr.Cells[6].Text);
            txtcontactno.Text = SpacialCharRemove.XSS_Remove(gr.Cells[7].Text);
            if (gr.Cells[10].Text == "&nbsp;")
            {
                txtimei.Text = "";
            }
            else
            {
                txtimei.Text = SpacialCharRemove.XSS_Remove(gr.Cells[10].Text);
            }
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.selectcircle_user(gr.Cells[3].Text);
            lbcircle.DataTextField = "circle";
            lbcircle.DataSource = dt;
            lbcircle.DataBind();
            mp1.Show();

            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.pm_selectzone_user(gr.Cells[3].Text);
            lbzone.DataTextField = "zone";
            lbzone.DataSource = dt;
            lbzone.DataBind();
            mp1.Show();


            if (ddlroleupdate.SelectedItem.Text == "Zonal Manager")
            {
                trzone.Visible = true;
                trzonelb.Visible = true;
                mp1.Show();
            }
            else
            {
                trzone.Visible = false;
                trzonelb.Visible = false;
                mp1.Show();
            }


        }
        catch
        {
            //throw ex;
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }
    private bool CompareArray(byte[] a1, byte[] a2)
    {

        if (a1.Length != a2.Length)

            return false;



        for (int i = 0; i < a1.Length; i++)
        {

            if (a1[i] != a2[i])

                return false;

        }



        return true;

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();

            imageHeader.Add("JPG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });

            byte[] header_p;
            string fileExt_p = FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.') + 1).ToUpper();
            string[] acceptedFileTypes = new string[1];

            acceptedFileTypes[0] = ".JPG";


            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();

            if (ddlcompany.SelectedValue != "---Select Company---")
            {
                ba.User = txtuserupdate.Text;
                if (ddlproject0.Text == "CIAT" && ddlroleupdate.SelectedIndex != 0)
                {
                    ba.Stepid = 1;
                    ba.Role = ddlroleupdate.SelectedValue;
                    ba.Siteidnew = ""; //ptw role
                }
                if (ddlproject0.Text == "PTW" && ddlroleupdate0.SelectedIndex != 0)
                {
                    ba.Stepid = 2;
                    ba.Role = "";//ciat role
                    ba.Siteidnew = ddlroleupdate0.SelectedValue;
                }
                if (ddlproject0.Text == "BOTH" && ddlroleupdate.SelectedIndex != 0 && ddlroleupdate0.SelectedIndex != 0)
                {
                    ba.Stepid = 3;
                    ba.Role = ddlroleupdate.SelectedValue; //ciat role
                    ba.Siteidnew = ddlroleupdate0.SelectedValue; //ptw role

                }
                // ba.Role = ddlroleupdate.SelectedValue;
                ba.Circle = ddlcircleupdate.SelectedValue;
                ba.Emailid = txtemailid.Text;
                ba.Contactno = txtcontactno.Text;
                ba.Imei = txtimei.Text;
                ba.parent_company = ddlcompany.SelectedValue;
                ba.sub_company = txtsubcompany.Text;
                int res = 0;
                string u = SpacialCharRemove.XSS_Remove(ba.User);
                res = Convert.ToInt32(da.updateuser1(ba));
                if (res == 1)
                {
                    mp1.Show();
                    Label1.Text = "EmailID already Exist";
                }
                else if (res == 4)
                {
                    mp1.Show();
                    Label1.Text = "IMEI does not Exist";
                }
                else
                {
                    mp1.Hide();
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.showuser_from_user1("%" + u + "%");
                    grduser.DataSource = dt;
                    grduser.DataBind();
                    Label4.Text = "'" + u + "' is Updated successfully...";
                    Label4.Visible = true;
                    da = new mydataaccess1();
                    da.delete_circle_user(txtuserupdate.Text);
                    if (ddlroleupdate.SelectedItem.Text == "Zonal Manager")
                    {
                        da = new mydataaccess1();
                        da.PM_delete_zone_user(txtuserupdate.Text);
                    }
                    string abc = FileUpload1.FileName;
                    abc = SpacialCharRemove.SpacialChar_Remove(abc);

                    if (abc == "")
                    {
                        //Label1.Text = "Select file first.";
                        //Label1.Visible = true;
                    }
                    else
                    {
                        try
                        {
                            byte[] tmp_k = imageHeader[fileExt_p];
                            header_p = new byte[tmp_k.Length];
                            FileUpload1.FileContent.Read(header_p, 0, header_p.Length);
                            if (CompareArray(tmp_k, header_p))
                            {

                                FileUpload1.SaveAs(Server.MapPath("~/signatures/") + abc);
                                string path = Server.MapPath("~/signatures/") + abc;
                                da = new mydataaccess1();
                                da.ptw_insert_into_signature(txtuserupdate.Text, ddlcircleupdate.Text, path);
                            }
                            else
                            {
                                mp1.Show();
                                Label1.Text = "Select .jpg file only.";
                                Label1.Visible = true;
                            }
                        }
                        catch
                        {
                            mp1.Show();
                            Label1.Text = "Upload wrong file format...";
                            Label1.Visible = true;
                        }


                    }

                    if (lbcircle.Items.Count > 0)
                    {
                        for (int i = 0; i < lbcircle.Items.Count; i++)
                        {
                            da = new mydataaccess1();
                            da.insertcircle_user(txtuserupdate.Text, lbcircle.Items[i].ToString());
                        }
                    }
                    else
                    {
                        da = new mydataaccess1();
                        da.insertcircle_user(txtuserupdate.Text, ddlcircleupdate.SelectedValue);

                    }
                    lbcircle.Items.Clear();
                    if (ddlroleupdate.SelectedItem.Text == "Zonal Manager")
                    {
                        if (lbzone.Items.Count > 0)
                        {
                            for (int i = 0; i < lbzone.Items.Count; i++)
                            {
                                da = new mydataaccess1();
                                da.insertzone_user(txtuserupdate.Text, lbzone.Items[i].ToString());
                            }
                        }
                        else
                        {
                            if (ddlzone.SelectedIndex != 0)
                            {
                                da = new mydataaccess1();
                                da.insertzone_user(txtuserupdate.Text, ddlzone.SelectedValue);
                            }
                        }
                        lbzone.Items.Clear();
                    }
                }
            }
            else
            {
                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('Please select Parent Company');", true);
                mp1.Show();
            }
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }

    protected void lnkdelete_deleteclick(object sender, EventArgs e)
    {
        try
        {
            LinkButton tb = (LinkButton)sender;
            string s = tb.ToolTip.ToString();

            da = new mydataaccess1();
            ba = new myvodav2();
            ba.User = s.ToString();
            da.deleteuser(ba);


            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            dt = da.showuser(ba);
            grduser.DataSource = dt;
            grduser.DataBind();

            Label4.Text = "'" + SpacialCharRemove.XSS_Remove(s.ToString()) + "' is deleted successfully...";
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void btnremove_Click(object sender, EventArgs e)
    {

        if (lbcircle.SelectedIndex != -1)
        {
            lberrorlb.Text = "";
            lbcircle.Items.Remove(lbcircle.SelectedValue);
        }
        else
        {
            lberrorlb.Text = "Select Any Circle from ListBox then Remove.";
        }
        mp1.Show();
    }
    protected void ddlcircleupdate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcircleupdate.SelectedIndex != 0)
        {
            if (ddlroleupdate.SelectedValue == "Zonal Manager")
            {
                lbcircle.Enabled = false;
                lbzone.Enabled = true;
                da = new mydataaccess1();
                ba = new myvodav2();
                dt = new DataTable();
                ba.Circle = ddlcircleupdate.SelectedValue;
                dt = da.getzonesbycirclename(ba);
                ddlzone.DataTextField = "zone";
                ddlzone.DataSource = dt;
                ddlzone.DataBind();
                ddlzone.Items.Insert(0, "---Select---");
            }
            int flag = 0;
            for (int i = 0; i < lbcircle.Items.Count; i++)
            {
                if (lbcircle.Items[i].ToString() == ddlcircleupdate.SelectedValue)
                {
                    flag = 1;
                }
            }
            if (flag == 0)
            {
                lbcircle.Items.Add(ddlcircleupdate.SelectedValue);
            }
        }
        mp1.Show();
    }
    protected void ddlproject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlproject0.SelectedIndex != 0)
        {
            if (ddlproject0.Text == "CIAT")
            {
                ddlroleupdate.Enabled = true;
                ddlroleupdate0.Enabled = false;
            }
            if (ddlproject0.Text == "PTW")
            {
                ddlroleupdate.Enabled = false;
                ddlroleupdate0.Enabled = true;
            }
            if (ddlproject0.Text == "BOTH")
            {
                ddlroleupdate.Enabled = true;
                ddlroleupdate0.Enabled = true;
            }
        }
        mp1.Show();
    }

    protected void ddlcompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcompany.SelectedIndex != 0)
        {
            //if (ddlproject.Text == "CIAT")
            //{
            //    ddlrole.Enabled = true;
            //    ddlrole0.Enabled = false;
            //}
            //if (ddlproject.Text == "PTW")
            //{
            //    ddlrole.Enabled = false;
            //    ddlrole0.Enabled = true;
            //    trcirclelb.Visible = true;
            //}
            //if (ddlproject.Text == "BOTH")
            //{
            //    ddlrole.Enabled = true;
            //    ddlrole0.Enabled = true;
            //    trcirclelb.Visible = true;
            //}
        }
        else
        {
            //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('Please select Project');", true);
        }
        mp1.Show();
    }
    protected void LinkButton1_Click1(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        da = new mydataaccess1();
        da.unblock_user(lb.ToolTip);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('User Unblocked');", true);
    }
    protected void Button1_Click(object sender, EventArgs e)
    {


    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        if (TextBox1.Text != "")
        {
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.showuser_from_user1("%" + TextBox1.Text + "%");
            grduser.DataSource = dt;
            grduser.DataBind();
        }
        else
        {
            Labelerror.Text = "Please Enter Username!!!";
            // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Enter Username!!!');</script>");
        }
    }
    protected void btnzoneremove_Click(object sender, EventArgs e)
    {
        if (lbzone.SelectedIndex != -1)
        {
            lbzoneerror.Text = "";
            lbzone.Items.Remove(lbzone.SelectedValue);
        }
        else
        {
            lbzoneerror.Text = "Select Any Zone from ListBox then Remove.";
        }
        mp1.Show();
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlzone.SelectedIndex != 0)
        {
            int flag = 0;
            for (int i = 0; i < lbzone.Items.Count; i++)
            {
                if (lbzone.Items[i].ToString() == ddlzone.SelectedValue)
                {
                    flag = 1;
                }
            }
            if (flag == 0)
            {
                lbzone.Items.Add(ddlzone.SelectedValue);
            }
        }
        mp1.Show();
    }
    protected void ddlroleupdate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlroleupdate.SelectedItem.Text == "Zonal Manager")
        {
            trzone.Visible = true;
            trzonelb.Visible = true;
            mp1.Show();
        }
        else
        {
            trzone.Visible = false;
            trzonelb.Visible = false;
            mp1.Show();
        }
    }
}
