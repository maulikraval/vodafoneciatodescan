﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using System.Data;
using FindDataAccessLayer;
using dataaccesslayer2;

public partial class NSS_NodeId_Delete : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //drp1AtRiskStatus.ClearSelection();
            //drpPTWStatus.ClearSelection();
        }

    }

    protected void btn_search_nssnodeid_Click(object sender, EventArgs e)
    {
        string sp_user_d = "";
        da = new mydataaccess1();
        dt = new DataTable();
        sp_user_d = da.select_user_cookie(Session["user"].ToString());
        Label2.Text = "";
        FindDataAccess da1 = new FindDataAccess();
        //int res = da1.update_nss_on_site(txt_nssid.Text, txt_node.Text, sp_user_d);
        dt = da1.Nss_Nodeid_Search(txt_nssid.Text, txt_node.Text);
        if (dt.Rows.Count > 0)
        {
            grd1.DataSource = dt;
            grd1.DataBind();
        }
        else
        {
            Label2.Text = "No data found.!";
        }
    }

    protected void grd1_selectedrowdeleting(object sender, EventArgs e)
    {
        try
        {
            string sp_user_d = "";
            int res = 0;
            GridViewRow gr = grd1.SelectedRow;
            FindDataAccess da1 = new FindDataAccess();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());

            if (String.Equals(txt_nssid.Text, txt_node.Text))
                res = 2;
            else
                res = da1.delete_nodeid(txt_nssid.Text, txt_node.Text, sp_user_d);

            if (res == 1)
            {
                Label2.Text = "Deleted Successfully..";
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Deleted Successfully');</script>");
            }
            else if (res == 2)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Both Id are common.');</script>");
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Error!!!');</script>");
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void grd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int res = 0;
            string sp_user_d = "";
            GridViewRow gr = grd1.SelectedRow;
            FindDataAccess da1 = new FindDataAccess();
            da = new mydataaccess1();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());

            if (String.Equals(txt_nssid.Text, txt_node.Text))
                res = 2;
            else
                res = da1.delete_nodeid(txt_nssid.Text, txt_node.Text, sp_user_d);

            if (res == 1)
            {
                Label2.Text = "Deleted Successfully..";
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Deleted Successfully');</script>");
            }
            else if (res == 2)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Both Id are common.');</script>");
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Error!!!');</script>");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    //protected void btn_find_contact_Click(object sender, EventArgs e)
    //{
    //    string sp_user_d = "";
    //    da = new mydataaccess1();
    //    dt = new DataTable();
    //    sp_user_d = da.select_user_cookie(Session["user"].ToString());

    //    FindDataAccess da1 = new FindDataAccess();
    //    //int res = da1.update_nss_on_site(txt_nssid.Text, txt_node.Text, sp_user_d);
    //    int res = da1.delete_nodeid(txt_nssid.Text, txt_node.Text, sp_user_d);

    //    if (res == 1)
    //    {
    //        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Updated Successfully');</script>");
    //    }
    //    else
    //    {
    //        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Error!!!');</script>");
    //    }
    //}
}