﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Active_deactive_User.aspx.cs" Inherits="AllPage_FindUser" MasterPageFile="~/AllPage/AllCommon.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblerror" runat="server" ></asp:Label>
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbl_User" runat="server" Text="Username"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_User" runat="server"></asp:TextBox>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                    <asp:Button ID="btn_find_user" runat="server" Text="Find" 
                        OnClick="btn_find_user_Click" Width="114px" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td style="text-align:center">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:Button ID="btn_find_activate" runat="server" Text="Activate" 
                        Width="153px" onclick="btn_find_activate_Click" />
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                    <asp:Button ID="btn_find_deactivate" runat="server" Text="De-Activate" 
                       Width="153px" onclick="btn_find_deactivate_Click" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
