﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;
using InfoSoftGlobal;


public partial class PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;

    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        // lblmsg.Text = "";
        //FCLiteral.Text = CreateChart();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        panel1.Visible = false;
        panel2.Visible = false;
        panel3.Visible = false;
        if (!IsPostBack)
        {


            try
            {
                int i = 4;

                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                //  da = new mydataaccess1();
                // count = da.ptw_dashboard_active_ptw();

                // da = new mydataaccess1();
                //// count1 = da.ptw_dashboard_expire_ptw();
                // lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
            }
            catch (Exception ee)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();

                            DataTable circle = new DataTable();
                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            ba = new myvodav2();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                        }
                    }

                    if (Session["role"].ToString() == "4")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");


                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_company();
                            ddlissuercompany.DataTextField = "company_name";
                            ddlissuercompany.DataSource = dt;
                            ddlissuercompany.DataBind();
                            ddlissuercompany.Items.Insert(0, "Select");
                            ddlissuercompany.Items.Insert(1, "All");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_risk_company_master(drpcircle.Text);
                            ddlreceivercompany.DataTextField = "company_name";
                            ddlreceivercompany.DataSource = dt;
                            ddlreceivercompany.DataBind();
                            ddlreceivercompany.Items.Insert(0, "Select");
                            ddlreceivercompany.Items.Insert(1, "All");
                        }
                    }

                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_risk_company_master(drpcircle.Text);
        ddlreceivercompany.DataTextField = "company_name";
        ddlreceivercompany.DataSource = dt;
        ddlreceivercompany.DataBind();
        ddlreceivercompany.Items.Insert(0, "Select");
        ddlreceivercompany.Items.Insert(1, "All");
    }
    private string CreateChart_barchart(string fil1, string fil2)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC;

        strXML = "<graph caption=' PTW Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix=''rotateNames='1'  formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Low Risk' color='8EB229'>";
        strDataProdB = "<dataset seriesName='Medium Risk' color='F6BD0F'>";
        strDataProdC = "<dataset seriesName='High Risk' color='F72125'>";


        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_risk_counts(fil1, fil2);

        if (dt.Rows.Count > 0)
        {

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][4].ToString()) + Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][4].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";

            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales1", countdrp, "300", false, false);

    }
    private string CreateChart_barchart_trend(string fil1, string fil2)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC;

        strXML = "<graph caption=' PTW Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix=''  formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Low Risk' color='8EB229'>";
        strDataProdB = "<dataset seriesName='Medium Risk' color='F6BD0F'>";
        strDataProdC = "<dataset seriesName='High Risk' color='F72125'>";


        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_risk_counts_trend(fil1, fil2);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][4].ToString()) + Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][4].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";

            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales134", countdrp, "300", false, false);

    }





    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    public string CreateChart_circle()
    {
        string strXML = "";
        try
        {
            // lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();

            dt = da.ptw_chart_counts(" where site_master.circle='" + drpcircle.Text + "'", "", "5");

            if (dt.Rows[0][0].ToString() == "")
            {

                strXML = "<graph caption=' Blanket PTW Status, Circle: " + drpcircle.SelectedValue + "' numberPrefix='%' decimalPrecision='0' >";

                // lblmsg.Text = "No Data Found.";
                return strXML;

            }

            strXML = "<graph caption=' Blanket PTW Status, Circle: " + drpcircle.SelectedValue + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";
            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Electrical" + " + (" + dt.Rows[0][3].ToString() + ") +" + "' value='" + dt.Rows[0][1].ToString() + "' />";

            strXML += "<set name='" + "Height" + " + (" + dt.Rows[0][2].ToString() + ") + " + "' value='" + dt.Rows[0][0].ToString() + "' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }
        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);

    }
    public string CreateChart(string fil1, string fil2)
    {
        string strXML = "";
        try
        {
            //lbl1.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();
            string username = lblusername.Text;
            dt = da.ptw_chart_risk_counts(fil1, fil2);


            int sum_count = Convert.ToInt32(dt.Rows[0][3].ToString()) + Convert.ToInt32(dt.Rows[0][4].ToString()) + Convert.ToInt32(dt.Rows[0][5].ToString());

            strXML = "<graph caption=' Risk Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " (" + sum_count + ")" + "'  showPercentageInLabel='1' pieSliceDepth='30'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Low Risk" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' color='8EB229' />";

            strXML += "<set name='" + "Medium Risk" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][4].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' color='F6BD0F' />";

            strXML += "<set name='" + "High Risk" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][5].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' color='F72125' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }
        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {

        string message = "";
        string filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";

        if (drpcircle.SelectedIndex == 1)
        {
            filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";
            message = "Summary of PTW (T@DH) issued by " + SpacialCharRemove.XSS_Remove(ddlissuercompany.SelectedValue) + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + SpacialCharRemove.XSS_Remove(ddlissuer.SelectedValue) + " and Receiver Company -" + SpacialCharRemove.XSS_Remove(ddlreceivercompany.SelectedValue) + ")";

            if (ddlissuercompany.SelectedIndex != 1)
            {
                // message += " and For Company " + ddlissuercompany.SelectedValue;
                filter += "  and  company_master.company_name='" + ddlissuercompany.SelectedValue + "'";
            }
            if (ddlissuer.SelectedIndex != 1)
            {
                // message += " and For Issuer " + ddlissuer.SelectedValue;
                filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
            }
            if (ddlreceivercompany.SelectedIndex != 1)
            {
                // message += " and For Receiver " + ddlreceivercompany.SelectedValue;
                filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
            }
        }
        else
        {
            filter = " where site_master.circle='" + drpcircle.SelectedItem.Text.ToString() + "' and (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";
            message = "Summary of PTW (T@DH) issued by " + SpacialCharRemove.XSS_Remove(ddlissuercompany.SelectedValue) + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + SpacialCharRemove.XSS_Remove(ddlissuer.SelectedValue) + " and Receiver Company -" + SpacialCharRemove.XSS_Remove(ddlreceivercompany.SelectedValue) + ")";

            if (ddlissuercompany.SelectedIndex != 1)
            {
                // message += " and For Company " + ddlissuercompany.SelectedValue;
                filter += "  and  company_master.company_name='" + ddlissuercompany.SelectedValue + "'";
            }
            if (ddlissuer.SelectedIndex != 1)
            {
                //  message += " and For Issuer " + ddlissuer.SelectedValue;
                filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
            }
            if (ddlreceivercompany.SelectedIndex != 1)
            {
                // message += " and For Receiver " + ddlreceivercompany.SelectedValue;
                filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
            }
        }

        panel1.Visible = true;
        panel2.Visible = true;
        panel3.Visible = true;

        lbl1.Text = message;
        lbl2.Text = message;
        lbl3.Text = message;

        FCLiteral.Text = CreateChart(filter, "");
        Literal1.Text = CreateChart_barchart(filter, "All");
        Literal2.Text = CreateChart_barchart_trend(filter, "All");




    }
    protected void ddlissuercompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        da = new mydataaccess1();
        dt = da.select_issuer_from_company_chart(ddlissuercompany.Text, "5", drpcircle.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            ddlissuer.DataSource = dt;
            ddlissuer.DataTextField = "username";
            ddlissuer.DataBind();
            ddlissuer.Items.Insert(0, "Select");
            ddlissuer.Items.Insert(1, "All");
        }
        else
        {
            ddlissuer.Items.Clear();

            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('There is no any issuer for selected company');</script>");
        }
    }
}
