﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using System.Data;
using mybusiness;

public partial class AllPage_admin_delete_sitenewmaster : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;
    string sp_user_d;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            da = new mydataaccess1();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());
            if (sp_user_d == "")
            {
                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

            }
            da = new mydataaccess1();
            ba = new myvodav2();
            DataTable circle = new DataTable();

            ba.User = sp_user_d;
            circle = da.getcirclenamefromusername(ba);

            drpcircle.DataSource = circle;
            drpcircle.DataTextField = "circle";
            drpcircle.DataBind();
            drpcircle.Items.Insert(0, "Select");
        }
    }
    protected void btndelete_Click(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();

        string siteid = ddlsiteid.SelectedItem.Text.ToString();
        string check_sheet = ddlchecksheet.SelectedItem.Text.ToString();

        int status = da.delete_sitenewmaster_site(siteid, check_sheet);

        if (status == 1) { lblmsg.Text = "Successfully Deleted"; }
        else { lblmsg.Text = "Not Existing site or checksheet"; }
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {

        myvodav2 vb = new myvodav2();
        da = new mydataaccess1();
        DataTable zone = new DataTable();
        vb.Circle = drpcircle.SelectedItem.Text.ToString();
        zone = da.getzonesbycirclename(vb);
        if (zone.Rows.Count > 0)
        {
            ddlzone.Items.Clear();
            ddlzone.DataSource = zone;
            ddlzone.DataTextField = "zone";
            ddlzone.DataBind();
            ddlzone.Items.Insert(0, "Select");
            
           
        }
        else
        {
            ddlzone.Items.Clear();
            ddlzone.DataSource = null;
            ddlzone.DataBind();
            ddlzone.Items.Insert(0, "Select");
           
        }
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();

        string circle= drpcircle.SelectedItem.Text.ToString();
        string zone = ddlzone.SelectedItem.Text.ToString();

        dt = da.getsiteidbycirclezone(circle, zone);

        ddlsiteid.Items.Clear();
        ddlsiteid.DataSource = dt;
        ddlsiteid.DataTextField = "siteid";
        ddlsiteid.DataBind();
        ddlsiteid.Items.Insert(0, "Select");
        

    }
    protected void ddlsiteid_SelectedIndexChanged(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();
        string siteid = ddlsiteid.SelectedItem.Text.ToString();

        dt = da.getchecksheetbysiteid(siteid);

        ddlchecksheet.Items.Clear();
        ddlchecksheet.DataSource = dt;
        ddlchecksheet.DataTextField = "Checksheet_Type";
        ddlchecksheet.DataBind();
        ddlchecksheet.Items.Insert(0, "Select");
       
    }
}