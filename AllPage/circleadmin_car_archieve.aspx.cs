﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.IO;

public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    HSSFWorkbook hssfworkbook;
    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    int userid;
    int Step1;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
       // Response.Cache.SetExpires(DateTime.Now);
       // Response.Cache.SetCacheability(HttpCacheability.NoCache);
       // Response.Cache.SetNoStore();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
            string User = lblusername.Text;
            da = new mydataaccess1();
            userid = da.selectuserid(User);

            if (!IsPostBack)
            {
                try
                {
                    ImageButton1.Visible = false;
                    //ImageButton2.Visible = false;
                    int i = 3;
                    if (Convert.ToInt32(Session["role"].ToString()) == i)
                    {

                    }
                    else
                    {

                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                    }
                }
                catch
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "4")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                  /*  int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    da = new mydataaccess1();
                    dt = new DataTable();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);

                    da = new mydataaccess1();
                    DataTable dtall = new DataTable();
                    dtall = da.reminder_20_days_circle_admin_all(sp_user_d);

                    da = new mydataaccess1();
                    DataTable cvall = new DataTable();
                    cvall = da.reminder_20_days_critical_all_circle_admin(sp_user_d);

                    da = new mydataaccess1();
                    DataTable dtall1 = new DataTable();
                    dtall1 = da.reminder_10_days_circle_admin_all(sp_user_d);

                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                    da = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                    lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
*/
                    //Marquee End

                    //            if (Session.Count != 0)
                    {
                        /*
                        if (Session["role"].ToString() == "2")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {

                                da = new mydataaccess1();
                                string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                                da = new mydataaccess1();
                                DataTable circle = new DataTable();
                                lblusername.Text = sp_user_d;

                                circle = da.getcirclename();

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                drpcircle.Items.Insert(1, "All");
                            }
                        }
                         */
                        if (Session["role"].ToString() == "3")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da = new mydataaccess1();
                                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                                if (lblusername.Text == "")
                                {
                                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }
                                da = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                // drpcircle.Items.Insert(1, "All");
                            }
                        }

                        if (Session["role"].ToString() == "2")
                        {
                            //da = new mydataaccess1();
                            //DataTable dt = new DataTable();
                            //dt = da.viewpivotrpt();
                            //GridView1.DataSource = dt;
                            //GridView1.DataBind();



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }

    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        DataTable dt = (DataTable)ViewState["dt"];
        if (dt.Rows.Count > 0)
        {
            string filename = "Bulk_car_archieve.xls";
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
            Response.Clear();

            InitializeWorkbook();
            exporttoexcel(dt);
            Response.BinaryWrite(WriteToStream().GetBuffer());

        }
    }


    protected void btnsearch_Click(object sender, EventArgs e)
    {
        try
        {

            ImageButton1.Visible = true;
            DateTime Date1 = Convert.ToDateTime(TextBox1.Text);
            DateTime Date2 = Convert.ToDateTime(TextBox2.Text);

            da = new mydataaccess1();
            DataTable dt = new DataTable();

            if (Convert.ToInt16(ddlCheckSheet.SelectedValue) > 0)
                dt = da.car_bulk_data_archieve(Convert.ToInt16(ddlCheckSheet.SelectedValue), drpcircle.SelectedValue, ddlprovidername.SelectedItem.Text, Date1, Date2);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            ViewState["dt"] = dt;
            rptlable.Text = "Report between " + SpacialCharRemove.XSS_Remove(TextBox1.Text) + " to " + SpacialCharRemove.XSS_Remove(TextBox2.Text) + "";

        }
        catch (Exception ex)
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }



    }



   
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void drpcircle_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            ImageButton1.Visible = true;
            //ImageButton2.Visible = true;

            ddlCheckSheet.SelectedIndex = 0;

            GridView1.DataSource = null;
            GridView1.DataBind();



        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }



    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlsheet_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }
    protected void ddlCheckSheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}
