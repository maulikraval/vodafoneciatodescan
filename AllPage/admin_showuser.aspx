﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_showuser.aspx.cs" Inherits="AllPage_admin_showuser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        //function click(){

        function click1() {


            var file = document.getElementById("<%=SpacialCharRemove.XSS_Remove(FileUpload1.ClientID)%>");
         if (file.value != '') {

             var label = document.getElementById("<%=SpacialCharRemove.XSS_Remove(Label1.ClientID)%>");

                var path = file.value;

                var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();

                var isValidFile = false;
                var validFilesTypes = ["jpg"];

                for (var i = 0; i < validFilesTypes.length; i++) {

                    if (ext == validFilesTypes[i]) {

                        isValidFile = true;

                        break;

                    }

                }

                if (isValidFile != true) {

                    label.style.color = "red";

                    label.innerHTML = "Invalid File. Please upload a File with" +

                        " extension:\n\n" + validFilesTypes.join(", ");

                }

                return isValidFile;


            }
        }


    </script>



    <table width="100%">
        <tr>
            <td style="width: 100%; text-align: center">
                <asp:Label ID="Label3" runat="server" CssClass="lblstly" Text="User Details"></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <asp:Label ID="Label6" runat="server" Text="Enter Username :"></asp:Label>
                <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                &nbsp;
                <asp:Label ID="Labelerror" runat="server"></asp:Label>
                <asp:Label ID="Label4" runat="server" CssClass="lblall"></asp:Label>
            </td>
        </tr>
        <tr>
            <hr />
            <td style="text-align: center; width: 100%">
                <asp:Panel ID="panel1" runat="server" ScrollBars="Horizontal" Width="950px" Height="450px">
                    <asp:GridView ID="grduser" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                        OnSelectedIndexChanged="grduser_SelectedIndexChanged" Width="100%">
                        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle Font-Names="Calibri" Font-Size="13px" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:CommandField HeaderText="Edit" ShowSelectButton="True" SelectText="Edit" />
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="False" CommandName="Delete"
                                        OnClick="lnkdelete_deleteclick" Text="Delete" ToolTip='<%#Eval("User")%>'></asp:LinkButton>
                                    <cc1:ConfirmButtonExtender ID="con" runat="server" TargetControlID="lnkdelete" ConfirmText="Are You sure ?">
                                    </cc1:ConfirmButtonExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Unblock">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" ToolTip='<%#Eval("User")%>' CausesValidation="False"
                                        OnClick="LinkButton1_Click1">Unblock</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>

        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                    TargetControlID="pan1" runat="server">
                </cc1:RoundedCornersExtender>
                <asp:Panel ID="pan1" BackColor="#E4E4E4" runat="server">
                    <table width="100%">
                        <tr>
                            <td style="width: 5%"></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="Label2" runat="server" CssClass="lblstly" Text="Edit User"></asp:Label>
                            </td>
                            <td style="width: 5%;"></td>
                        </tr>
                        <tr>
                            <td style="width: 5%">&nbsp;
                            </td>
                            <td colspan="2" style="text-align: center;">
                                <hr />
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblusernameupdate" runat="server" Text="User Name :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtuserupdate" Enabled="false" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 45%; text-align: right;">
                                <asp:Label ID="lblcompany" runat="server" CssClass="lblall" Text="Parent Company :"></asp:Label>
                            </td>
                            <td style="width: 45%; text-align: left;">
                                <asp:DropDownList ID="ddlcompany" runat="server" CssClass="genall" Width="154px"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlcompany_SelectedIndexChanged">
                                    <%-- <asp:ListItem>Select Company</asp:ListItem>--%>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlcompany"
                                    ErrorMessage="*" InitialValue="Select Company" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 45%; text-align: right;">
                                <asp:Label ID="lblsubcompany" runat="server" CssClass="lblall" Text="Sub Company :"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtsubcompany" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtsubcompany"
                                    ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="InvalidChars"
                        InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," TargetControlID="txtsubcompany">
                    </cc1:FilteredTextBoxExtender>--%>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 5%">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblproject" runat="server" CssClass="lblall" Text="Project :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:DropDownList ID="ddlproject0" runat="server" AutoPostBack="True" CssClass="genall"
                                    OnSelectedIndexChanged="ddlproject_SelectedIndexChanged" Width="154px">
                                    <asp:ListItem>Select Project</asp:ListItem>
                                    <asp:ListItem>CIAT</asp:ListItem>
                                    <asp:ListItem>PTW</asp:ListItem>
                                    <asp:ListItem>BOTH</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlproject0"
                                    ErrorMessage="*" InitialValue="Select Project" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblroleupdate" runat="server" Text="CIAT Role :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:DropDownList ID="ddlroleupdate" runat="server" Width="154px" CssClass="genall"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlroleupdate_SelectedIndexChanged">
                                    <asp:ListItem>--Select--</asp:ListItem>
                                    <asp:ListItem>PM</asp:ListItem>
                                    <asp:ListItem>Technician</asp:ListItem>
                                    <asp:ListItem>Circle Admin</asp:ListItem>
                                    <asp:ListItem>Super Admin</asp:ListItem>
                                    <asp:ListItem>Zonal Manager</asp:ListItem>


                                </asp:DropDownList>
                                &nbsp;&nbsp;
                                <asp:Label ID="lblroleupdate0" runat="server" CssClass="lblall" Text="PTW Role :"></asp:Label>
                                <asp:DropDownList ID="ddlroleupdate0" runat="server" CssClass="genall" Width="154px">
                                    <asp:ListItem>--Select--</asp:ListItem>
                                    <asp:ListItem>Receiver</asp:ListItem>
                                    <asp:ListItem>Issuer</asp:ListItem>
                                    <asp:ListItem>Circle Admin</asp:ListItem>
                                    <asp:ListItem>Super Admin</asp:ListItem>
                                    <asp:ListItem>Service Partner</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%; height: 26px;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right; height: 26px;">
                                <asp:Label ID="lblcircleupdate" runat="server" CssClass="lblall" Text="Circle :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left; height: 26px;">
                                <asp:DropDownList ID="ddlcircleupdate" runat="server" AutoPostBack="True" CssClass="genall"
                                    OnSelectedIndexChanged="ddlcircleupdate_SelectedIndexChanged" Width="154px">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 5%; height: 26px;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%; height: 26px;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right; height: 26px;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: left; height: 26px;">
                                <asp:ListBox ID="lbcircle" runat="server" Width="120px"></asp:ListBox>
                                <asp:Button ID="btnremove" runat="server" CausesValidation="False" OnClick="btnremove_Click"
                                    Text="Remove" />
                                <asp:Label ID="lberrorlb" runat="server"></asp:Label>
                            </td>
                            <td style="width: 5%; height: 26px;">&nbsp;
                            </td>
                        </tr>
                        <tr id="trzone" runat="server">
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 45%; text-align: right;">
                                <asp:Label ID="Label7" runat="server" Text="Zone :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 45%; text-align: left;">
                                <asp:DropDownList ID="ddlzone" runat="server" CssClass="genall" Width="154px" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlzone_SelectedIndexChanged">
                                    <asp:ListItem>-----------Select-----------</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlzone"
                                    ErrorMessage="*" InitialValue="---Select---" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr id="trzonelb" runat="server">
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 45%; text-align: right;">&nbsp;
                            </td>
                            <td style="width: 45%; text-align: left;">
                                <asp:ListBox ID="lbzone" runat="server" Width="120px"></asp:ListBox>
                                <asp:Button ID="btnzoneremove" runat="server" CausesValidation="False" Text="Remove"
                                    OnClick="btnzoneremove_Click" />
                                <asp:Label ID="lbzoneerror" runat="server"></asp:Label>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblemailid" runat="server" Text="Email - ID :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtemailid" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtemailid"
                                    ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemailid"
                                    ErrorMessage="Enter Correct Email-Id" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblcontactno" runat="server" CssClass="lblall" Text="Contact No :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtcontactno" runat="server" CssClass="genall" MaxLength="10" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcontactno"
                                    ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtcontactno"
                                    ErrorMessage="Enter Only 10 Digit." Display="Dynamic" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtcontactno"
                                    ValidChars="1234567890">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="text-align: right; width: 40%;">
                                <asp:Label ID="lblcontactno0" runat="server" CssClass="lblall" Text="IMEI No:"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtimei" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtimei"
                                    ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtimei"
                                    ValidChars="1234567890.">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%; height: 33px;">&nbsp;
                            </td>
                            <td style="text-align: right; height: 33px; width: 40%;">
                                <asp:Label ID="Label5" runat="server" CssClass="lblall" Font-Bold="False" Text="Select File To Upload(Sign)"></asp:Label>
                                &nbsp;
                            </td>
                            <td style="width: 40%; height: 33px; text-align: left;">
                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="genall" Font-Size="Medium" />
                            </td>
                            <td style="width: 5%; height: 33px;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%; height: 26px;"></td>
                            <td style="text-align: center; height: 26px;" colspan="2">
                                <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" OnClientClick="return click1()" Text="Update" />
                                <asp:Button ID="btncancel" runat="server" CausesValidation="false" Text="Cancel" />
                            </td>
                            <td style="width: 5%; height: 26px;"></td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="text-align: right; width: 40%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: left;">&nbsp;
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:LinkButton ID="lnk" runat="server" Width="1px"></asp:LinkButton>
                <cc1:ModalPopupExtender ID="mp1" runat="server" BackgroundCssClass="modalBackground"
                    TargetControlID="lnk" PopupControlID="pan1">
                </cc1:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
