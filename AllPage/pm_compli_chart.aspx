﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="pm_compli_chart.aspx.cs" Inherits="PM_rptgrid"
    EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CIAT | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/admin_styleinner1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/cufon-yui.js"></script>

    <script type="text/javascript" src="../js/arial.js"></script>

    <script type="text/javascript" src="../js/cuf_run.js"></script>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <link href="../css/menu.css" rel="stylesheet" type="text/css" />

    <script src="../js/menu.js" type="text/javascript"></script>

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #roll a
        {
            display: inline-table;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 762px;
        }
        .style2
        {
            width: 269px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

        

    <script src="../FusionCharts/FusionCharts.js" type="text/javascript"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="updatepanel" runat="server">
        <ContentTemplate>--%>
    <div class="main">
        <div class="header">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                        <tr>
                            <td width="6%">
                                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style1">
                              <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change Password</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" 
                                    CausesValidation="False">Log Out</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                        </tr>
<tr>
                            <td width="6%">
                            </td>
                            <td class="style3">
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:LinkButton ID="lnkchangeproject" runat="server" OnClick="lnkchange_Click" CausesValidation="False"
                                    PostBackUrl="~/login/ciat_ptw.aspx" Visible="False">Change 
                                Project</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clr">
                </div>
                <div id="menu">
                    <ul class="menu">
                       <li><a href="#" class="parent"><span>Dashboard</span></a>
                            <ul>
                                <li><a href="pm_reminder.aspx"><span>PM</span></a> </li>
                            </ul>
                        </li>
                        <li><a href="pm_rptgrid.aspx" class="parent"><span>Report View</span></a></li>
                        <li><a href="#" class="parent"><span>Chart</span></a>
                            <ul>
                                <li><a href="pm_chart.aspx"><span>Inspection(PieChart)</span></a> </li>
                                <li><a href="pm_bar_chart.aspx"><span>Inspection(BarChart)</span></a> </li>
                                <li><a href="pm_bar_chart_category.aspx"><span> CheckPoint</span></a> </li>
                                <li><a href="pm_compli_chart.aspx"><span>Site Compliance</span></a> </li>
                            </ul>
                        </li>
                        <li><a href="#" class="parent"><span>PMT Charts</span></a>
                            <ul>
                                <li><a href="pm_pmt_PM_Chart.aspx"><span>Inspection(PieChart)</span></a></li>
                                <li><a href="pm_pmt_PM_Chart_Survey.aspx"><span>Inspection(BarChart)</span></a></li>
                                <li><a href="pm_pmt_PM_Chart_YesNoNA.aspx"><span>Checksheet point</span></a></li>
                            </ul>
                        </li>
                        <li><a href="#" class="parent"><span>PMTReports</span></a>
                            <ul>
                                <li><a href="pm_pmt_Circle_Report.aspx"><span>CIAT Latest</span></a></li>
                                <li><a href="pm_pmt_Circle_Report_archive.aspx"><span>CIAT Archive</span></a></li>
                                <li><a href="pm_pmt_pm_PDF_Report.aspx"><span>PDF Report</span></a></li>
                                <li><a href="pm_pmt_TechnicianAssignReport_.aspx"><span>Technician Report</span></a></li>
                            </ul>
                        </li>
                        <li><a href="pm_car_pdf_Report.aspx" class="parent"><span>Inspection PDF Report</span></a></li> </ul>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
            <div class="mainbar">
                    <div class="article1">
                        
                            <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server" Text="" CssClass="lblmarquee"></asp:Label>
                    
                    </marquee>
                        
                    </div>
                </div>
                <div class="sidebar">
                    <div class="gadget">
                        <table class="style2">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:DropDownList ID="drpcircle" runat="server" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged"
                                        Width="200px" CssClass="genall" AutoPostBack="True">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:DropDownList ID="drpchecksheet" runat="server" OnSelectedIndexChanged="drpchecksheet_SelectedIndexChanged"
                                        Width="200px" CssClass="genall" AutoPostBack="True">
                                        <asp:ListItem>--Select Checksheet--</asp:ListItem>
                                        <asp:ListItem Value="10">ISIC V2.3</asp:ListItem>
                                        <asp:ListItem Value="11">BSIC V5.3</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style2" style="color: #000000">
                                   <%-- <strong>Date:</strong></td>--%>
                                    <asp:DropDownList ID="drpprovider" runat="server" 
                                        Width="200px" CssClass="genall" AutoPostBack="True" 
                                        onselectedindexchanged="drpprovider_SelectedIndexChanged">
                                        <asp:ListItem>--Select Provider--</asp:ListItem>
                                        <asp:ListItem Value="10">VIL</asp:ListItem>
                                        <asp:ListItem Value="11">Indus</asp:ListItem>
                                        <asp:ListItem Value="12">Other BOO</asp:ListItem>
                                    </asp:DropDownList>
                            </tr>
                      <%--      <tr>
                                <td class="style2" style="color: #000000">
                                    From :<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    <cc1:CalendarExtender ID="ccl1" runat="server" TargetControlID="TextBox1"></cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2" style="color: #000000">
                                    To:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                       <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox2"></cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2" style="color: #000000">
                                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Search" />
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="style2" style="color: #000000">
                                    &nbsp;</tr>
                            <tr>
                                <td class="style2" style="color: #000000">
                                    <asp:DropDownList ID="ddlsite" runat="server" 
                                        Width="200px" CssClass="genall" AutoPostBack="True" onselectedindexchanged="ddlsite_SelectedIndexChanged" 
                                      >
                                        <asp:ListItem>--Select Sites---</asp:ListItem>
                                        <asp:ListItem Value="10">New</asp:ListItem>
                                        <asp:ListItem Value="11">Old</asp:ListItem>
                                      
                                    </asp:DropDownList>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="mainbar">
                    <div class="article">
                        <table width="100%">
                            <tr>
                                <td width="5%" style="text-align: center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;" width="25%">
                                    <div>
                                        <table class="style2">
                                            <tr>
                                                <td>
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="panel2" runat="server" Width="700px"
                                                        Height="400px" ScrollBars="Auto">
                                                        <h2>
                                                            &nbsp;</h2>
                                                        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                                                        <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    &nbsp;
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="panel1" runat="server" Width="700px"
                                                        Height="400px">
                                                        <h2>
                                                            &nbsp;</h2>
                                                        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    &nbsp;
                                                </td>

                                            </tr>
                                              <tr>
                                                <td>
                                                    <asp:Panel Visible="false" BorderColor="Black" BorderWidth="2px" ID="panel3" runat="server" Width="700px"
                                                        Height="400px">
                                                        <h2>
                                                            &nbsp;</h2>
                                                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    &nbsp;
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td width="5%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
              <%--  <p class="lf">
                    © Copyright 2011 <a href="#"></a>.</p>
                <p class="rf">
                    Powered By.<a href="http://teleysia.com">Teleysia Networks Pvt. Ltd.</a></p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
    </div>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
