﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;
using NPOI.HPSF;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel.Contrib;
using NPOI.HSSF.UserModel;
using System.IO;


public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    HSSFWorkbook hssfworkbook;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                string strPreviousPage = "";
                if (Request.UrlReferrer != null)
                {
                    strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
                }
                if (strPreviousPage == "")
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                ImageButton1.Visible = false;
 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
                int i = 4;
                if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString()==sp_user_)
                {

                }
                else
                {

                    if (Session["role"].ToString() == "2")
                    {
                        Response.Redirect("~/PM/Home.aspx");
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        Response.Redirect("~/circle_admin/Home.aspx");
                    }
                    if (Session["role"].ToString() == "4")
                    {
                        Response.Redirect("~/AllPage/admin_Default3.aspx");
                    }
                    if (Session["role"].ToString() == "5")
                    {
                        Response.Redirect("~/PM/SiteInfonew.aspx");
                    }
                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {

            // marque Start
            int count = 0;
            int count1 = 0;
            int count2 = 0;
          //  da = new mydataaccess1();
         //   count = da.ptw_dashboard_active_ptw();

           // da = new mydataaccess1();
           // count1 = da.ptw_dashboard_expire_ptw();
           // lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
            da = new mydataaccess1();
            dt = new DataTable();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

            //Marquee End

            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    /*
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = sp_user_d;

                            circle = da.getcirclename();

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                     */
                    if (Session["role"].ToString() == "4")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();

                            ba.User = lblusername.Text;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                            drpcircle.Items.Insert(2, "North Hub");
                            drpcircle.Items.Insert(3, "East Hub");
                            drpcircle.Items.Insert(4, "South Hub");
                            drpcircle.Items.Insert(5, "West Hub");
                        }
                    }

                    if (Session["role"].ToString() == "2")
                    {
                        //da = new mydataaccess1();
                        //DataTable dt = new DataTable();
                        //dt = da.viewpivotrpt();
                        //GridView1.DataSource = dt;
                        //GridView1.DataBind();



                    }
                }
            }
        }
        catch (Exception ex)
        {
           // throw ex;
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        string filename = "PTWREPORT.xls";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
        Response.Clear();

        InitializeWorkbook();
        DataTable dt1 = (DataTable)Session["dt"];
        exporttoexcel(dt1);
        Response.BinaryWrite(WriteToStream().GetBuffer());     

      
        
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            MultiView1.Visible = true;
            if (ddlcategory.SelectedIndex == 0)
            {
                MultiView1.ActiveViewIndex = -1;
            }
            else
            {

                if (ddlcategory.Text == "Date Wise")
                {
                    MultiView1.Visible = false;
                    //MultiView1.ActiveViewIndex = 0;
                }
                if (ddlcategory.Text == "Status Wise")
                {
                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 0;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_getptwstatus();

                    ddlstatus.DataSource = dt;
                    ddlstatus.DataTextField = "discription";
                    ddlstatus.DataValueField = "status";
                    ddlstatus.DataBind();
                    ddlstatus.Items.Insert(0, "Select Status");

                }
                if (ddlcategory.Text == "Receiver Wise")
                {
                    GridView1.Visible = false;
                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 1;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_getreceiverandissuername(drpcircle.SelectedItem.Text.ToString(), 8);

                    ddlreceiver.DataSource = dt;
                    ddlreceiver.DataTextField = "username";
                    ddlreceiver.DataValueField = "id";
                    ddlreceiver.DataBind();
                    ddlreceiver.Items.Insert(0, "Select Permit Receiver");
                }
                if (ddlcategory.Text == "Issuer Wise")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 2;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_getreceiverandissuername(drpcircle.SelectedItem.Text.ToString(), 9);

                    ddlissuer.DataSource = dt;
                    ddlissuer.DataTextField = "username";
                    ddlissuer.DataBind();
                    ddlissuer.Items.Insert(0, "Select Permit Issuer");
                }

                if (ddlcategory.Text == "PTW Type Wise")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 3;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_selectcategory();

                    ddlptwtype.DataSource = dt;
                    ddlptwtype.DataTextField = "category";
                    ddlptwtype.DataValueField = "ptwcategorymaster_id";
                    ddlptwtype.DataBind();
                    ddlptwtype.Items.Insert(0, "Select PTW Type");
                }

                if (ddlcategory.Text == "Risk Mitigation Dump")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = -1;

                }
                if (ddlcategory.Text == "Company Wise")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 4;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.select_company_ptw();

                    ddlcompany.DataSource = dt;
                    ddlcompany.DataTextField = "company_name";
                    ddlcompany.DataBind();
                    ddlcompany.Items.Insert(0, "Select Company");
                }
            }

        }

        catch (Exception ex)
        {
            throw ex;
            //Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }


    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        ddlcategory.SelectedIndex = 0;
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        ImageButton1.Visible = true;
        if (TextBox1.Text != "" && TextBox2.Text != "")
        {
            DateTime first = Convert.ToDateTime(TextBox1.Text);
            DateTime second = Convert.ToDateTime(TextBox2.Text);
            var monthDiff = Math.Abs((second.Year * 12 + (second.Month - 1)) - (first.Year * 12 + (first.Month - 1)));
          //  Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + monthDiff + "');</script>");
            if (monthDiff <= 3)
            {
                if (drpcircle.SelectedIndex != 0 && ddlcategory.SelectedIndex != 0)
                {
                    if (ddlcategory.SelectedIndex == 1 && TextBox1.Text != "" && TextBox2.Text != "")
                    {
                        da = new mydataaccess1();
                        dt = new DataTable();

                        if (drpcircle.SelectedIndex == 1)
                        {

                            dt = da.select_ptw_Master_report_night("", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 2)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Delhi' or ptw_basic_detail_master.location='HP' or ptw_basic_detail_master.location='Haryana' or ptw_basic_detail_master.location='J&K' or ptw_basic_detail_master.location='Punjab' or ptw_basic_detail_master.location='UPE' or ptw_basic_detail_master.location='UPW'", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 3)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Assam' or ptw_basic_detail_master.location='Bihar' or ptw_basic_detail_master.location='North East' or ptw_basic_detail_master.location='Orissa' or ptw_basic_detail_master.location='ROB' or ptw_basic_detail_master.location='Kolkata'", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 4)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='AP' or ptw_basic_detail_master.location='Chennai' or ptw_basic_detail_master.location='ROTN' or ptw_basic_detail_master.location='Karnataka' or ptw_basic_detail_master.location='Kerala'", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 5)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='RAJASTHAN' or ptw_basic_detail_master.location='Gujarat' or ptw_basic_detail_master.location='MAG' or ptw_basic_detail_master.location='MP' or ptw_basic_detail_master.location='Mumbai'", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='" + drpcircle.Text + "'", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        Session["dt"] = dt;
                        GridView1.DataSource = dt;
                        GridView1.DataBind();

                        GridView1.Visible = true;
                    }

                    if (ddlcategory.SelectedIndex == 2 && ddlstatus.SelectedIndex != 0)
                    {
                        da = new mydataaccess1();
                        dt = new DataTable();

                        if (drpcircle.SelectedIndex == 1)
                        {
                            dt = da.select_ptw_Master_report_night("", "where ptw_extension_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 2)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Delhi' or ptw_basic_detail_master.location='HP' or ptw_basic_detail_master.location='Haryana' or ptw_basic_detail_master.location='J&K' or ptw_basic_detail_master.location='Punjab' or ptw_basic_detail_master.location='UPE' or ptw_basic_detail_master.location='UPW'", "where ptw_extension_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 3)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Assam' or ptw_basic_detail_master.location='Bihar' or ptw_basic_detail_master.location='North East' or ptw_basic_detail_master.location='Orissa' or ptw_basic_detail_master.location='ROB' or ptw_basic_detail_master.location='Kolkata'", "where ptw_extension_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 4)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='AP' or ptw_basic_detail_master.location='Chennai' or ptw_basic_detail_master.location='ROTN' or ptw_basic_detail_master.location='Karnataka' or ptw_basic_detail_master.location='Kerala'", "where ptw_extension_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 5)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='RAJASTHAN' or ptw_basic_detail_master.location='Gujarat' or ptw_basic_detail_master.location='MAG' or ptw_basic_detail_master.location='MP' or ptw_basic_detail_master.location='Mumbai'", "where ptw_extension_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='" + drpcircle.Text + "'", "where ptw_extension_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        Session["dt"] = dt;
                        GridView1.DataSource = dt;
                        GridView1.DataBind();

                        GridView1.Visible = true;
                    }
                    if (ddlcategory.SelectedIndex == 3 && ddlreceiver.SelectedIndex != 0)
                    {
                        da = new mydataaccess1();
                        dt = new DataTable();
                        if (drpcircle.SelectedIndex == 1)
                        {

                            dt = da.select_ptw_Master_report_night("", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 2)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Delhi' or ptw_basic_detail_master.location='HP' or ptw_basic_detail_master.location='Haryana' or ptw_basic_detail_master.location='J&K' or ptw_basic_detail_master.location='Punjab' or ptw_basic_detail_master.location='UPE' or ptw_basic_detail_master.location='UPW'", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 3)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Assam' or ptw_basic_detail_master.location='Bihar' or ptw_basic_detail_master.location='North East' or ptw_basic_detail_master.location='Orissa' or ptw_basic_detail_master.location='ROB' or ptw_basic_detail_master.location='Kolkata'", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 4)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='AP' or ptw_basic_detail_master.location='Chennai' or ptw_basic_detail_master.location='ROTN' or ptw_basic_detail_master.location='Karnataka' or ptw_basic_detail_master.location='Kerala'", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 5)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='RAJASTHAN' or ptw_basic_detail_master.location='Gujarat' or ptw_basic_detail_master.location='MAG' or ptw_basic_detail_master.location='MP' or ptw_basic_detail_master.location='Mumbai'", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='" + drpcircle.Text + "'", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        Session["dt"] = dt;
                        GridView1.DataSource = dt;
                        GridView1.DataBind();

                        GridView1.Visible = true;
                    }
                    if (ddlcategory.SelectedIndex == 4 && ddlissuer.SelectedIndex != 0)
                    {
                        da = new mydataaccess1();
                        dt = new DataTable();
                        if (drpcircle.SelectedIndex == 1)
                        {
                            dt = da.select_ptw_Master_report_night("", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");

                        }
                        else if (drpcircle.SelectedIndex == 2)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Delhi' or ptw_basic_detail_master.location='HP' or ptw_basic_detail_master.location='Haryana' or ptw_basic_detail_master.location='J&K' or ptw_basic_detail_master.location='Punjab' or ptw_basic_detail_master.location='UPE' or ptw_basic_detail_master.location='UPW'", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 3)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Assam' or ptw_basic_detail_master.location='Bihar' or ptw_basic_detail_master.location='North East' or ptw_basic_detail_master.location='Orissa' or ptw_basic_detail_master.location='ROB' or ptw_basic_detail_master.location='Kolkata'", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 4)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='AP' or ptw_basic_detail_master.location='Chennai' or ptw_basic_detail_master.location='ROTN' or ptw_basic_detail_master.location='Karnataka' or ptw_basic_detail_master.location='Kerala'", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 5)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='RAJASTHAN' or ptw_basic_detail_master.location='Gujarat' or ptw_basic_detail_master.location='MAG' or ptw_basic_detail_master.location='MP' or ptw_basic_detail_master.location='Mumbai'", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='" + drpcircle.Text + "'", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        Session["dt"] = dt;
                        GridView1.DataSource = dt;
                        GridView1.DataBind();

                        GridView1.Visible = true;
                    }
                  

                    
                    if (ddlcategory.SelectedIndex == 5 && ddlcompany.SelectedIndex != 0)
                    {
                        da = new mydataaccess1();
                        dt = new DataTable();
                        if (drpcircle.SelectedIndex == 1)
                        {
                            dt = da.select_ptw_Master_report_night("", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");

                        }
                        else if (drpcircle.SelectedIndex == 2)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Delhi' or ptw_basic_detail_master.location='HP' or ptw_basic_detail_master.location='Haryana' or ptw_basic_detail_master.location='J&K' or ptw_basic_detail_master.location='Punjab' or ptw_basic_detail_master.location='UPE' or ptw_basic_detail_master.location='UPW'", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 3)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Assam' or ptw_basic_detail_master.location='Bihar' or ptw_basic_detail_master.location='North East' or ptw_basic_detail_master.location='Orissa' or ptw_basic_detail_master.location='ROB' or ptw_basic_detail_master.location='Kolkata'", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 4)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='AP' or ptw_basic_detail_master.location='Chennai' or ptw_basic_detail_master.location='ROTN' or ptw_basic_detail_master.location='Karnataka' or ptw_basic_detail_master.location='Kerala'", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else if (drpcircle.SelectedIndex == 5)
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='RAJASTHAN' or ptw_basic_detail_master.location='Gujarat' or ptw_basic_detail_master.location='MAG' or ptw_basic_detail_master.location='MP' or ptw_basic_detail_master.location='Mumbai'", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        else
                        {
                            dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='" + drpcircle.Text + "'", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                        }
                        Session["dt"] = dt;
                        GridView1.DataSource = dt;
                        GridView1.DataBind();

                        GridView1.Visible = true;
                    }

                }

                if (drpcircle.SelectedIndex != 0 && ddlcategory.SelectedIndex == 0)
                {

                    da = new mydataaccess1();
                    dt = new DataTable();
                    if (drpcircle.SelectedIndex == 1)
                    {
                        dt = da.select_ptw_Master_report_night("", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else if (drpcircle.SelectedIndex == 2)
                    {
                        dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Delhi' or ptw_basic_detail_master.location='HP' or ptw_basic_detail_master.location='Haryana' or ptw_basic_detail_master.location='J&K' or ptw_basic_detail_master.location='Punjab' or ptw_basic_detail_master.location='UPE' or ptw_basic_detail_master.location='UPW'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else if (drpcircle.SelectedIndex == 3)
                    {
                        dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='Assam' or ptw_basic_detail_master.location='Bihar' or ptw_basic_detail_master.location='North East' or ptw_basic_detail_master.location='Orissa' or ptw_basic_detail_master.location='ROB' or ptw_basic_detail_master.location='Kolkata'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else if (drpcircle.SelectedIndex == 4)
                    {
                        dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='AP' or ptw_basic_detail_master.location='Chennai' or ptw_basic_detail_master.location='ROTN' or ptw_basic_detail_master.location='Karnataka' or ptw_basic_detail_master.location='Kerala'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else if (drpcircle.SelectedIndex == 5)
                    {
                        dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='RAJASTHAN' or ptw_basic_detail_master.location='Gujarat' or ptw_basic_detail_master.location='MAG' or ptw_basic_detail_master.location='MP' or ptw_basic_detail_master.location='Mumbai'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }

                    else
                    {
                        dt = da.select_ptw_Master_report_night("where ptw_basic_detail_master.location='" + drpcircle.Text + "'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    Session["dt"] = dt;
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please enter Dates having only 3 months difference!!!');</script>");
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView1.Visible = true;
            }
        }

        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Dates!!!');</script>");
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.Visible = true;
        }

    }
    protected void btnsitesearch_Click(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();

        dt = da.select_ptw_Master_report_night("", "where site_master.siteid='" + txtsearxh.Text + "'");
        Session["dt"] = dt;
        GridView1.DataSource = dt;
        GridView1.DataBind();

        GridView1.Visible = true;
    }
    protected void ddlptwtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.ptw_selectpurpose(ddlptwtype.SelectedValue.ToString());

            ddlpurposeofwork.DataSource = dt;
            ddlpurposeofwork.DataTextField = "sub_category";
            ddlpurposeofwork.DataValueField = "ptwsubcategorymaster_id";
            ddlpurposeofwork.DataBind();
            ddlpurposeofwork.Items.Insert(0, "Select Purpose of work");
        }
        catch (Exception ee)
        { }
    }
    protected void ddlpurposeofwork_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }

    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(SpacialCharRemove.StartsWithAny(dt.Columns[j].ColumnName));
            //row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(SpacialCharRemove.StartsWithAny(dt.Rows[i][j].ToString()));
                //row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }

}
