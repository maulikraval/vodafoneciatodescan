﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using InfoSoftGlobal;
using mybusiness;

public partial class admin_admin_Active_PM_Chart_Survey : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        //FCLiteral.Text = CreateChart();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        if (!IsPostBack)
        {
            try
            {
                int i = 12;

                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                // Marquee start
                /*     da = new mydataaccess1();
                     DataTable dtv5 = new DataTable();
                     dtv5 = da.reminder_20_days();
                     da = new mydataaccess1();
                     DataTable dtv2 = new DataTable();
                     dtv2 = da.reminder_20_days_v2();
                     da = new mydataaccess1();
                     DataTable dtv2ms = new DataTable();
                     dtv2ms = da.reminder_20_days_v2ms();
                     da = new mydataaccess1();
                     DataTable dtall = new DataTable();
                     dtall = da.reminder_20_days_all();
                     da = new mydataaccess1();
                     DataTable cvall = new DataTable();
                     cvall = da.reminder_20_days_critical_all();
                     da = new mydataaccess1();
                     DataTable dtall1 = new DataTable();
                     dtall1 = da.reminder_10_days_all();
                     count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                     // critical points
                     da = new mydataaccess1();
                     DataTable cv2 = new DataTable();
                     cv2 = da.reminder_20_days_critical_v2();
                     da = new mydataaccess1();
                     DataTable cv5 = new DataTable();
                     cv5 = da.reminder_20_days_critical();
                     da = new mydataaccess1();
                     DataTable cv2ms = new DataTable();
                     cv2ms = da.reminder_20_days_critical_v2ms();
                     count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                     da = new mydataaccess1();
                     DataTable dtv51 = new DataTable();
                     dtv51 = da.reminder_10_days();
                     da = new mydataaccess1();
                     DataTable dtv21 = new DataTable();
                     dtv21 = da.reminder_10_days_v2();
                     da = new mydataaccess1();
                     DataTable dtv2ms1 = new DataTable();
                     dtv2ms1 = da.reminder_10_days_v2ms();
                     count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                     lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
                     */
                // Marquee End
                // lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>10</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>24</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>3</span>)";
            }
            catch (Exception ee)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    if (Session["role"].ToString() == "12")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            ba = new myvodav2();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            //drpcircle.Items.Insert(1, "All");

                            #region Fill Zone
                            DrpZone.Items.Clear();
                            DrpZone.DataSource = null;
                            DrpZone.DataBind();
                            DrpZone.Items.Insert(0, "Select");

                            #endregion
                            // drpcircle.Items.Insert(1, "All");
                        }
                    }
                    //if (Session["role"].ToString() == "4")
                    //{
                    //    if (drpcircle.SelectedIndex != 0)
                    //    {
                    //        da = new mydataaccess1();
                    //        string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    //        da = new mydataaccess1();
                    //        ba = new myvodav2();

                    //        DataTable circle = new DataTable();



                    //        ba.User = sp_user_d;
                    //        circle = da.getcirclenamefromusername(ba);

                    //        drpcircle.DataSource = circle;
                    //        drpcircle.DataTextField = "circle";
                    //        drpcircle.DataBind();
                    //        drpcircle.Items.Insert(0, "Select");
                    //        drpcircle.Items.Insert(1, "All");

                    //        #region Fill Zone
                    //        DrpZone.Items.Clear();
                    //        DrpZone.DataSource = null;
                    //        DrpZone.DataBind();
                    //        DrpZone.Items.Insert(0, "Select");

                    //        #endregion


                    //    }
                    //}
                    //#region temp
                    //if (Session["role"].ToString() == "4")
                    //{
                    //    if (drpcircle.SelectedIndex != 0)
                    //    {
                    //        da = new mydataaccess1();
                    //        string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    //        lblusername.Text = sp_user_d;

                    //        da = new mydataaccess1();
                    //        ba = new myvodav2();

                    //        DataTable circle = new DataTable();



                    //        ba.User = sp_user_d;
                    //        circle = da.getcirclename(sp_user_d);

                    //        drpcircle.DataSource = circle;
                    //        drpcircle.DataTextField = "circle";
                    //        drpcircle.DataBind();
                    //        drpcircle.Items.Insert(0, "Select");
                    //        drpcircle.Items.Insert(1, "All");
                    //        // drpcircle.Items.Insert(1, "All");
                    //        #region Fill Zone
                    //        DrpZone.Items.Clear();
                    //        DrpZone.DataSource = null;
                    //        DrpZone.DataBind();
                    //        DrpZone.Items.Insert(0, "Select");

                    //        #endregion


                    //    }
                    //}
                    //#endregion
                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        //panel2.Visible = true;

        //drpchecksheet.Visible = true;
        //drpchecksheet.SelectedIndex = 0;
        try
        {
            myvodav2 vb = new myvodav2();
            da = new mydataaccess1();
            DataTable zone = new DataTable();

            if (drpcircle.SelectedIndex > 0)
            {
                vb.Circle = drpcircle.SelectedItem.Text.ToString();
                zone = da.getzonesbycirclename(vb);
                if (zone.Rows.Count > 0)
                {
                    DrpZone.Items.Clear();
                    DrpZone.DataSource = zone;
                    DrpZone.DataTextField = "zone";
                    DrpZone.DataBind();
                    DrpZone.Items.Insert(0, "Select");
                    DrpZone.Items.Insert(1, "All");
                }
                else
                {
                    DrpZone.Items.Clear();
                    DrpZone.DataSource = null;
                    DrpZone.DataBind();
                    DrpZone.Items.Insert(0, "Select");
                }
            }
            else
            {
                DrpZone.Items.Clear();
                DrpZone.DataSource = null;
                DrpZone.DataBind();
                DrpZone.Items.Insert(0, "Select");
                //drpchecksheet.Visible = false;
                panel2.Visible = false;
                pnlbarchart.Visible = false;
            }
        }
        catch (Exception ex)
        {

        }
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    public string CreateChart_circle_check()
    {
        da = new mydataaccess1();
        string sp_user_d_new = da.select_user_cookie(Session["user"].ToString());

        da = new mydataaccess1();
        dt = new DataTable();
        string check = "";
        string zone = "";

            if (DrpZone.SelectedIndex > 0)
            {
                zone = DrpZone.SelectedItem.Text;
            }

        if (drpcircle.SelectedIndex != 0)
        {

            dt = da.pm_pie_chart_compl_circle_checksheet_survey_site_Active_New(drpcircle.SelectedValue, zone, sp_user_d_new);
        }
        bool chekdt = false;
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() == "" && dt.Rows[i][1].ToString() == "" && dt.Rows[i][2].ToString() == "" && dt.Rows[i][3].ToString() == ""
                   && dt.Rows[i][4].ToString() == "" && dt.Rows[i][5].ToString() == "")
                {
                    chekdt = true;
                }
                else
                {
                    if (dt.Rows[i][0].ToString() == "0" && dt.Rows[i][1].ToString() == "0" && dt.Rows[i][2].ToString() == "0" && dt.Rows[i][3].ToString() == "0"
                   && dt.Rows[i][4].ToString() == "0" && dt.Rows[i][5].ToString() == "0")
                    {
                        chekdt = true;
                    }
                    else
                    {
                        chekdt = false;
                    }
                }

            }
        }
        if (chekdt == false)
        {
            if (dt.Rows.Count == 0)
            {
                string strXML = "";
                lblmsg.Text = "Active Equipment Status Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , Zone : " + SpacialCharRemove.XSS_Remove(zone) + " .";
                strXML = "<graph caption='No Data Found'   showPercentageInLabel='1' pieSliceDepth='25'  decimalSeparator='.' showNames='1'>";
                return strXML;
            }
            else
            {
                string strXML = "";
                strXML = "<graph  caption='Active Equipment Status Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , Zone: " + SpacialCharRemove.XSS_Remove(zone) + "'showPercentageInLabel='1' pieSliceDepth='25' decimalSeparator='.' showNames='1' >";
                // decimalPrecision='0'  showNames='1'  showValues='1'  pieYScale='100' pieBorderAlpha='100' pieFillAlpha='100' pieSliceDepth='15' pieRadius='100' enableSmartLabels ='0' skipOverlapLabels = '0'>";
                strXML += "<set name='" + "Survey Completed" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' color='#008000' hoverText='Survey Completed' />";
                strXML += "<set name='" + "Survey NonCompleted" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][4].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' color='#FF0000'  hoverText='Survey NonCompleted' />";
                strXML += "<set name='" + "Survey Partial Completed" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][5].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' color='#FFFF00'  hoverText='Survey Partial Completed' />";
                strXML += "</graph>";
                //return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Sales", "700", "350", false, false);
                return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);
            }
        }
        else
        {
            string strXML = "";
            lblmsg.Text = "No Data Found as there are no sites for " + SpacialCharRemove.XSS_Remove(check) + " .";
            strXML = "<graph caption='No Data Found'   showPercentageInLabel='1' pieSliceDepth='25'  decimalSeparator='.' showNames='1'>";
            return strXML;
        }

    }
    public string CreateChartBarChart()
    {
        da = new mydataaccess1();
        string sp_user_d_new = da.select_user_cookie(Session["user"].ToString());

        string strXML = "";
        Labelblmsgbarchart.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();
        string check = "";
        string zone = "";
            if (DrpZone.SelectedIndex > 0)
            {
                zone = DrpZone.SelectedItem.Text;
            }
        if (drpcircle.SelectedIndex != 0)
        {
            dt = da.pm_bar_chart_survey_site_Active_new(drpcircle.SelectedValue, zone, sp_user_d_new);
        }
        bool chekdt = false;
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString() == "" && dt.Rows[i][1].ToString() == "" && dt.Rows[i][2].ToString() == "" && dt.Rows[i][3].ToString() == ""
                   && dt.Rows[i][4].ToString() == "" && dt.Rows[i][5].ToString() == "" && dt.Rows[i][6].ToString() == ""
                   )
                {
                    chekdt = true;
                }
                else
                {
                    if (dt.Rows[i][0].ToString() == "0" && dt.Rows[i][1].ToString() == "0" && dt.Rows[i][2].ToString() == "0" && dt.Rows[i][3].ToString() == "0"
                   && dt.Rows[i][4].ToString() == "0" && dt.Rows[i][5].ToString() == "0" && dt.Rows[i][6].ToString() == "0"
                   )
                    {
                        chekdt = true;
                    }
                    else
                    {
                        chekdt = false;
                    }
                }

            }
        }
        if (chekdt == false)
        {
            if (dt.Rows.Count == 0)
            {
                strXML = "<graph caption='Active Equipment Status Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , Zone : " + SpacialCharRemove.XSS_Remove(zone) + "' numberPrefix='%' rotateNames='1' formatNumberScale='0' decimalSeparator='.' >";
                Labelblmsgbarchart.Text = "No Data Found.";
                return strXML;
            }
            else
            {
                string[,] arrData = new string[dt.Rows.Count, 6];
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    arrData[j, 0] = dt.Rows[j][0].ToString();
                    arrData[j, 1] = dt.Rows[j][1].ToString();
                    arrData[j, 2] = dt.Rows[j][2].ToString();
                    arrData[j, 3] = dt.Rows[j][3].ToString();

                    arrData[j, 4] = dt.Rows[j][4].ToString();
                    arrData[j, 5] = dt.Rows[j][5].ToString();

                }
                string strCategories, strDataCurr, strDataPrev, strDataOthe;
                int i;
                //Initialize <graph> element
                strXML = "<graph caption='Active Equipment Status Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , Zone : " + SpacialCharRemove.XSS_Remove(zone) + "' numberPrefix='%' rotateNames='1' formatNumberScale='0' decimalSeparator='.' >";
                //Initialize <categories> element - necessary to generate a multi-series chart

                //Initiate <dataset> elements
                strCategories = "<categories>";
                strDataCurr = "<dataset seriesName='Survey Completed' color='#008000'>";
                strDataPrev = "<dataset seriesName='Survey NonCompleted' color='#FF0000'>";
                strDataOthe = "<dataset seriesName='Survey Partial Completed' color='#FFFF00'>";
                //Iterate through the data	
                for (i = 0; i < dt.Rows.Count; i++)
                {
                    /* strCategories = "<categories>";
                     strDataCurr = "<dataset seriesName='Completed (" + arrData[i, 4] + ")' color='FFD700'>";
                     strDataPrev = "<dataset seriesName='NonCompleted (" + arrData[i, 5] + ")' color='E00D0D'>";
                     strDataOthe = "<dataset seriesName='Partial Completed (" + arrData[i, 6] + ")' color='008000'>";
                     //Append <category name='...' /> to strCategories*/
                    strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(arrData[i, 0]) + "' />";
                    //Add <set value='...' /> to both the datasets
                    strDataCurr += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 1]) + "' />";
                    strDataPrev += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 2]) + "' />";
                    strDataOthe += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 3]) + "' />";


                }
                //Close <categories> element
                strCategories += "</categories>";
                //Close <dataset> elements
                strDataCurr += "</dataset>";
                strDataPrev += "</dataset>";
                strDataOthe += "</dataset>";
                //Assemble the entire XML now
                strXML += strCategories + strDataCurr + strDataPrev + strDataOthe + "</graph>";
                //Create the chart - MS Column 3D Chart with data contained in strXML
                return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales", "1500", "400", false, false);
            }
        }
        else
        {
            strXML = "<graph caption='Active Equipment Status Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + SpacialCharRemove.XSS_Remove(check) + "' numberPrefix='%' rotateNames='1' formatNumberScale='0' decimalSeparator='.' >";
            Labelblmsgbarchart.Text = "No Data Found.";
            return strXML;
        }
    }
    protected void DrpZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        Labelblmsgbarchart.Text = "";
        if (drpcircle.SelectedIndex == 1)
        {
            panel2.Visible = true;
            FCLiteral.Text = CreateChart_circle_check();
            pnlbarchart.Visible = true;
            FCLiteralbarchart.Text = CreateChartBarChart();
        }
        else
        {
            if (DrpZone.SelectedIndex != 0)
            {
                panel2.Visible = true;
                FCLiteral.Text = CreateChart_circle_check();
                pnlbarchart.Visible = true;
                FCLiteralbarchart.Text = CreateChartBarChart();
            }
            else
            {
                panel2.Visible = false;
                pnlbarchart.Visible = false;
            }
        }

        //if (DrpZone.SelectedIndex > 0)
        //{
        //    drpchecksheet.Visible = true;
        //    drpchecksheet.SelectedIndex = 0;
        //}
        //else
        //{
        //    drpchecksheet.Visible = false;
        //    panel2.Visible = false;
        //    pnlbarchart.Visible = false;
        //}
    }

}