﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="circleadmin_menu_both.ascx.cs" Inherits="admin_menu" %>


<div id="menu">
    <ul class="menu">
        <li><a href="20daysreminder.aspx"><span>Dashboard</span></a></li>
        <li><a href="#" class="parent"><span>User Details</span></a>
             <ul>
                <%-- <li><a href="circleadmin_adduser_circle_both.aspx"><span>CreateUser</span></a> </li>--%>
               <li><a href="circleadmin_adduser_circle.aspx"><span>CreateUser</span></a> </li>
                <li><a href="circleadmin_showuser_circle.aspx"><span>ViewUser</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Site Details</span></a>
            <ul>
                <li><a href="UploadSitesNew_circle.aspx"><span>Upload Site</span></a></li>
                <li><a href="Viewsite_circle.aspx"><span>View Site</span></a> </li>
                <li><a href="view_site_report.aspx"><span>Site History</span></a> </li>
                <li><a href="approve_lat_long.aspx"><span>Applied For Lat/Long Change</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Mobile Details</span></a>
            <ul>
                <li><a href="manual_add_imei_circle.aspx"><span>Add Mobile Manually</span></a></li>
                <li><a href="UploadImei_circle.aspx"><span>Upload Mobile</span></a></li>
                <li><a href="ViewImei_circle.aspx"><span>View Register Mobile</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Find Sites</span></a>
            <ul>
                <li><a href="finduser_ciat.aspx"><span>CIAT</span></a></li>
                <li><a href="ptw_finduser_ptw.aspx"><span>PTW</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>View Report</span></a>
            <ul>
                <li><a href="Circle_Report_ciat.aspx"><span>CIAT Report</span></a></li>
                <li><a href="Circle_Report_ptw_copy.aspx"><span>Customize Report</span></a></li>
                <li><a href="PDF_Report_copy.aspx"><span>PTW PDF Report</span></a></li>
                <li><a href="risk_Report_ptw.aspx"><span>PTW Master Report</span></a></li>
            </ul>
        </li>
        <li><a href="RequestUsers.aspx" class="parent"><span>Requests From Tech.</span></a></li>
        <li><a class="parent"><span>Corrective Action Report</span></a>
            <ul>
                <li><a href="CAR_Report.aspx"><span>CAR Process</span></a></li>
                <li><a href="car_pdf_Report.aspx"><span>Inspection PDF Report</span></a></li>
                <li><a href="car_archieve.aspx"><span>Archieve Report</span></a></li>
            </ul>
        </li>
        <li><a class="parent"><span>Chart</span></a>
            <ul>
                <li><a href="chart.aspx"><span>Inspection(PieChart)</span></a></li>
                <li><a href="bar_chart.aspx"><span>Inspection(BarChart)</span></a></li>
                <%--<li><a href="bar_chart_gold.aspx"><span>GSBB Classification</span></a></li>--%>
                <li><a href="bar_chart_category.aspx"><span>Checksheet point</span></a></li>
                <li><a href="compli_chart.aspx"><span>Site Compliance</span></a></li>
                <li><a href="ptw_type_chart.aspx"><span>Type Wise</span></a></li>
                <li><a href="chart_status.aspx"><span>Status Wise</span></a></li>
                <li><a href="chart_risk.aspx"><span>Risk Wise</span></a></li>
                <li><a href="chart_reject_reason.aspx"><span>Rejection Wise</span></a></li>
            </ul>
        </li>
    </ul>
</div>
