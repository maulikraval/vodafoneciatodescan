﻿<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_ViewImei.aspx.cs" Inherits="AllPage_admin_ViewImei" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../admin_StyleSheet.css" rel="stylesheet" type="text/css" />
    
    <table width="100%">
        <body>
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td width="90%" align="right" style="width: 90%; text-align: center">
       <asp:Label ID="Label2" runat="server" CssClass="lblstly" 
                    Text="View IMEI"></asp:Label></td>
              
            </tr>
           
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td width="90%" align="right" style="width: 90%; text-align: center">
                 <hr />
                 
                        <asp:GridView ID="grd1" runat="server" 
                            OnSelectedIndexChanged="grd1_SelectedIndexChanged"  
                        BackColor="WhiteSmoke" BorderColor="#999999"
                    BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                            OnRowDeleted="grd1_RowDeleted" Width="100%" 
                            OnRowDataBound="grd1_RowDataBound">
                               <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:CommandField SelectText="Edit" ShowSelectButton="True" HeaderText="Edit" />
                                <asp:TemplateField HeaderText="Delete">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                    <asp:LinkButton ID="lb" runat="server" Text="Delete" CausesValidation="False" 
                                            ToolTip='<%# Eval("Imei") %>' onclick="lb_Click1"></asp:LinkButton>
                                        <cc1:ConfirmButtonExtender ID="cb1" runat="server" ConfirmText="Are You Sure?" 
                                            TargetControlID="lb">
                                        </cc1:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                    </td>
                <td width="90%" align="right" style="width: 90%; text-align: center">
                    <asp:Panel ID="panl" runat="server" Width="920px" ScrollBars="Vertical">
                    </asp:Panel>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td width="90%" align="right" style="width: 90%; text-align: center">
                    <asp:Label ID="Label5" runat="server" CssClass="lblall" ></asp:Label>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
    </table>
    <asp:Panel ID="panel2" runat="server" Width="850px" Height="200px"
        ForeColor="Black" BackColor="#E4E4E4">
        <table width="100%" style="text-align: center">
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td width="15%" style="text-align: center; width: 45%;" colspan="4">
                    <asp:Label ID="Label3" runat="server" CssClass="lblstly" Text="Edit IMEI"></asp:Label>
                </td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td style="text-align: right; text-align:center width: 45%;" width="15%" colspan="4">
                    <hr /></td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%" style="height: 26px">
                    <asp:TextBox ID="txtid" runat="server" Height="1px" Visible="False" 
                        Width="16px"></asp:TextBox>
                </td>
                <td class="lblall" style="text-align: right; height: 26px;" width="15%">
                    <asp:Label ID="Label4" runat="server" CssClass="lblall" Text="Imei No:"></asp:Label>
                   </td>
                <td style="text-align: left; height: 20px;" width="30%">
                    <asp:TextBox ID="txt1" runat="server" CssClass="genall" Width="170px" 
                        Enabled="False"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txt1" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                        TargetControlID="txt1" ValidChars="0123456789">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td class="lblall" style="text-align: right; width: 15%; height: 26px;">
                    Mobile Model:
                </td>
                <td style="text-align: left; height: 26px;" width="30%">
                    <asp:TextBox ID="txt2" runat="server" CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txt2" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|='()" 
                        TargetControlID="txt2">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%" style="height: 26px">
                    </td>
            </tr>
            <tr>
                <td width="5%">
                   </td>
                <td style="text-align: right" width="15%">
                                    </td>
                <td style="text-align: right" width="30%">
                    &nbsp;</td>
                <td style="text-align: left; width: 15%;">
                    
                    &nbsp;</td>
                <td style="text-align: left" width="30%">
                    
                </td>
                <td width="5%">
                    
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td style="text-align: right" width="15%">
                    &nbsp;</td>
                <td style="text-align: right" width="30%">
                    &nbsp;</td>
                <td style="text-align: left; width: 15%;">
                    &nbsp;</td>
                <td style="text-align: left" width="30%">
                    &nbsp;</td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%" style="height: 30px">
          
                </td>
                <td style="text-align: right; height: 30px;" width="15%">
                    &nbsp;
                </td>
                <td style="text-align: right; height: 30px;" width="30%">
                    <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" Text="Update" />
                </td>
                <td style="text-align: left; width: 15%; height: 30px;">
                    <asp:Button ID="btncancle0" runat="server" OnClick="btncancle_Click" 
                        Text="Cancel" CausesValidation="False" />
                </td>
                <td style="text-align: left; height: 30px;" width="30%">
                    
                    
                </td>
                <td width="5%" style="height: 30px">
                    
                </td>
            </tr>
        </table>
    </asp:Panel>
     <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                    TargetControlID="panel2" runat="server">
                </cc1:RoundedCornersExtender>
    <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="butt1" PopupControlID="panel2">
    </cc1:ModalPopupExtender>
</asp:Content>
