﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.IO;
using YYClass;
using NPOI.HSSF.Util;

public partial class circle_admin_CAR_Report : System.Web.UI.Page
{
    #region "Global Variables"
    mydataaccess1 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    int userid;
    int Step1;
    string sp_user_d;
    HSSFWorkbook hssfworkbook;
    #endregion

    #region "Page Events"
    private void Page_PreRender(object sender, System.EventArgs e)
    {
        // Response.Cache.SetExpires(DateTime.Now);
        // Response.Cache.SetCacheability(HttpCacheability.NoCache);
        // Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //  lblError.Visible = false;
        try
        {
            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            da = new mydataaccess1();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
            string User = lblusername.Text;
            da = new mydataaccess1();
            userid = da.selectuserid(User);

            if (!IsPostBack)
            {
                try
                {
                    FileUpload1.Visible = false;
                    Button2.Visible = false;
                    Button1.Visible = false;
                    pp.Visible = false;
                    //  ImageButton1.Visible = false;
                    //  ImageButton2.Visible = false;
                    int i = 3;
                    if (Convert.ToInt32(Session["role"].ToString()) == i)
                    {

                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "4")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                   /* int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da = new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
*/
                    //Marquee End

                    //if (Session.Count != 0)
                    {
                        if (Session["role"].ToString() == "3")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da = new mydataaccess1();
                                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                                if (lblusername.Text == "")
                                {
                                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }
                                da = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                //drpcircle.Items.Insert(1, "All");
                            }
                        }

                        if (Session["role"].ToString() == "2")
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    #endregion

    #region "Drop Down Events"
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlCheckSheet.SelectedIndex != 0)
            {
                if (ddlCheckSheet.SelectedIndex == 1)
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";

                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }

                    DataTable circlemap_v2 = new DataTable();
                    circlemap_v2 = da.getcirclemapgrid_v2(ba);
                    grdMaster.DataSource = circlemap_v2;
                    grdMaster.DataBind();
                    rptlable.Text = "Report For V2";


                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";
                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }
                    DataTable circlemap1_v2 = new DataTable();
                    circlemap1_v2 = da.getcirclemapgrid_v2_criticality(ba);


                }
                else
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";
                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }
                    DataTable circlemap = new DataTable();
                    circlemap = da.getcirclemapgrid(ba);
                    grdMaster.DataSource = circlemap;
                    grdMaster.DataBind();
                    rptlable.Text = "Report For V5";

                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";
                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }
                    DataTable circlemap1 = new DataTable();
                    circlemap1 = da.getcirclemapgridwithcriticality(ba);

                }
            }
        }
        catch (Exception ex)
        {
            // Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ddlsheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Button1.Visible = true;
            // ImageButton2.Visible = true;
            da = new mydataaccess1();
            ba1 = new myvodav23();
            if (drpcircle.Text == "North East")
            {
                ba1.Labelvalue = "ne";
            }
            if (drpcircle.Text != "North East")
            {
                ba1.Labelvalue = drpcircle.SelectedValue;
            }

            if (drpcircle.SelectedIndex != 0 && drpcircle.SelectedIndex == 1)
            {
                if (ddlCheckSheet.SelectedIndex != 0)
                {
                    if (ddlCheckSheet.SelectedIndex == 1)
                    {
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap = new DataTable();
                        ba1.Pri = "v2";
                        ba1.Dump = "max";
                        ba1.User = lblusername.Text;
                        circlemap = da.pivort_allsheet_allcircle(ba1);
                        grdMaster.DataSource = circlemap;
                        grdMaster.DataBind();
                        Session["circlsheet"] = circlemap;

                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap1 = new DataTable();
                        ba1.Pri = "v2";
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap1 = da.pivort_allsheet_allcircle_criticality(ba1);
                        //  grdDetail.DataSource = circlemap1;
                        // grdDetail.DataBind();
                        Session["circlsheetcriti"] = circlemap1;
                    }
                    if (ddlCheckSheet.SelectedIndex == 2)
                    {
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap = new DataTable();
                        ba1.Pri = "v5";
                        ba1.Dump = "max";
                        ba1.User = lblusername.Text;
                        circlemap = da.pivort_allsheet_allcircle(ba1);
                        grdMaster.DataSource = circlemap;
                        grdMaster.DataBind();
                        Session["circlsheet"] = circlemap;

                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap1 = new DataTable();
                        ba1.Pri = "v5";
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap1 = da.pivort_allsheet_allcircle_criticality(ba1);
                        //grdDetail.DataSource = circlemap1;
                        //grdDetail.DataBind();
                        Session["circlsheetcriti"] = circlemap1;
                    }
                    if (ddlCheckSheet.SelectedIndex == 3)
                    {
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap = new DataTable();
                        ba1.Pri = "v2ms";
                        ba1.Dump = "max";
                        ba1.User = lblusername.Text;
                        circlemap = da.pivort_allsheet_allcircle(ba1);
                        grdMaster.DataSource = circlemap;
                        grdMaster.DataBind();
                        Session["circlsheet"] = circlemap;


                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap1 = new DataTable();
                        ba1.Pri = "v2ms";
                        ba1.Dump = "max";
                        ba1.User = lblusername.Text;
                        circlemap1 = da.pivort_allsheet_allcircle(ba1);
                        //grdDetail.DataSource = circlemap1;
                        //grdDetail.DataBind();
                        Session["circlsheetcriti"] = circlemap1;
                    }
                }
            }
            else
            {
                if (ddlCheckSheet.SelectedIndex != 0)
                {
                    if (ddlCheckSheet.SelectedIndex == 1)
                    {
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap = new DataTable();
                        ba1.Pri = "v2";
                        ba1.Labelvalue = SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue);
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap = da.pivort_allsheet_by_circle(ba1);
                        grdMaster.DataSource = circlemap;
                        grdMaster.DataBind();
                        Session["circlsheet"] = circlemap;

                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap1 = new DataTable();
                        ba1.Pri = "v2";
                        ba1.Labelvalue = SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue);
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap1 = da.pivort_allsheet_by_circle_criticality(ba1);
                        //grdDetail.DataSource = circlemap1;
                        //grdDetail.DataBind();
                        Session["circlsheetcriti"] = circlemap1;
                    }
                    if (ddlCheckSheet.SelectedIndex == 2)
                    {
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap = new DataTable();
                        ba1.Pri = "v5";
                        ba1.Labelvalue = SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue);
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap = da.pivort_allsheet_by_circle(ba1);
                        grdMaster.DataSource = circlemap;
                        grdMaster.DataBind();
                        Session["circlsheet"] = circlemap;

                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap1 = new DataTable();
                        ba1.Pri = "v5";
                        ba1.Labelvalue = SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue);
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap1 = da.pivort_allsheet_by_circle_criticality(ba1);
                        //grdDetail.DataSource = circlemap1;
                        //grdDetail.DataBind();
                        Session["circlsheetcriti"] = circlemap1;
                    }
                    if (ddlCheckSheet.SelectedIndex == 3)
                    {
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap = new DataTable();
                        ba1.Pri = "v2ms";
                        ba1.Labelvalue = SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue);
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap = da.pivort_allsheet_by_circle(ba1);
                        grdMaster.DataSource = circlemap;
                        grdMaster.DataBind();
                        Session["circlsheet"] = circlemap;

                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        DataTable circlemap1 = new DataTable();
                        ba1.Pri = "v2ms";
                        ba1.Labelvalue = SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue);
                        ba1.Dump = "max";
                        da = new mydataaccess1();
                        sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
                        da = new mydataaccess1();
                        circlemap1 = da.pivort_allsheet_by_circle_criticality(ba1);
                        //grdDetail.DataSource = circlemap1;
                        //grdDetail.DataBind();
                        Session["circlsheetcriti"] = circlemap1;

                    }


                }
            }
            rptlable.Text = "Report for Sheet '" + SpacialCharRemove.XSS_Remove(ddlCheckSheet.SelectedValue) + "'";
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void drpcircle_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            Button1.Visible = true;
            //ImageButton2.Visible = true;
            ddlCheckSheet.SelectedIndex = 0;
            grdMaster.DataSource = null;
            grdMaster.DataBind();
            //grdDetail.DataSource = null;
            //grdDetail.DataBind();

            da = new mydataaccess1();
            ba = new myvodav2();
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void ddlCheckSheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        grdMaster.DataSource = null;
        grdMaster.DataBind();
        pp.Visible = false;
        Button1.Visible = false;

    }
    #endregion

    #region "Image Button Event"
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
        Response.Charset = "";

        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        grdMaster.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());

        Response.End();
        grdMaster.Columns[0].Visible = true;
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        //  grdDetail.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());

        Response.End();
        //   grdDetail.Columns[0].Visible = true;
    }
    #endregion

    #region "Encode / Decode"
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);
        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);
        return result;
    }
    #endregion

    #region "Link Button Events"
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());

            da = new mydataaccess1();
            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnkSiteId_Click(object sender, EventArgs e)
    {

        LinkButton lnkSiteId = (LinkButton)sender;
        string[] arrIds = lnkSiteId.ToolTip.Split(',');

        string mySiteId = arrIds[0].ToString();
        Session["mySiteId"] = mySiteId;
        ViewState["Rating_Id"] = arrIds[1].ToString();
        Session["Rating_Id"] = arrIds[1].ToString();

        // da = new mydataaccess1();
        // DataTable dt = new DataTable();
        Session["c_type"] = Convert.ToInt16(ddlCheckSheet.SelectedValue);
        Response.Redirect("circleadmin_car_report_2.aspx");
        //  dt = da.CAR_Get_Detail_Data(Convert.ToInt16(ddlCheckSheet.SelectedValue), mySiteId, Session["user"].ToString(), Convert.ToInt32(ViewState["Rating_Id"]));
        //if (dt.Rows.Count > 0)
        //{
        //   // grdDetail.DataSource = dt;
        //  //  grdDetail.DataBind();
        //}
        //else
        //{
        //    grdDetail.DataSource = null;
        //    grdDetail.DataBind();
        //}
    }
    #endregion

    #region "Button Event"





    #endregion

    #region "Public/Private Functions"
    private void BindGrid()
    {
        //Button1.Visible = true;
        DateTime Date1 = Convert.ToDateTime(TextBox1.Text);
        DateTime Date2 = Convert.ToDateTime(TextBox2.Text);
        da = new mydataaccess1();
        DataTable dt = new DataTable();

        if (Convert.ToInt16(ddlCheckSheet.SelectedValue) > 0)
            dt = da.CAR_Get_Master_Data(Convert.ToInt16(ddlCheckSheet.SelectedValue), drpcircle.SelectedValue, ddlprovidername.SelectedItem.Text, Date1, Date2);

        da = new mydataaccess1();
        DataTable dt1 = new DataTable();

        if (Convert.ToInt16(ddlCheckSheet.SelectedValue) > 0)
            dt1 = da.car_dashboard(Convert.ToInt16(ddlCheckSheet.SelectedValue), drpcircle.SelectedValue, ddlprovidername.SelectedItem.Text, Date1, Date2);

        if (dt.Rows.Count > 0)
        {
            grdMaster.DataSource = dt;
            grdMaster.DataBind();
            FileUpload1.Visible = true;
            Button2.Visible = true;
            Button1.Visible = true;

        }
        else
        {
            grdMaster.DataSource = null;
            grdMaster.DataBind();
            FileUpload1.Visible = false;
            Button2.Visible = false;
            Button1.Visible = false;


        }
        if (dt1.Rows.Count > 0)
        {
            pp.Visible = true;
            FileUpload1.Visible = true;
            Button2.Visible = true;
            Button1.Visible = true;
            lbla.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[0][2].ToString());
            lblb.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[0][3].ToString());
            lblc.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[0][4].ToString());
            lblaa.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[0][5].ToString());
            lblbb.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[0][6].ToString());
            lblcc.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[0][7].ToString());
            Label28.Text = "Total Site Count is : " + SpacialCharRemove.XSS_Remove(dt1.Rows[0][0].ToString());

        }
        else
        {

            pp.Visible = false;
            FileUpload1.Visible = false;
            Button2.Visible = false;
            Button1.Visible = false;


        }
    }
    #endregion

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            e.Row.Cells[9].Visible = false;
            e.Row.Cells[10].Visible = false;
        }
        catch { }
    }
    protected void grdMaster_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        FileUpload1.Visible = true;
        Button2.Visible = true;
        Button1.Visible = true;

        da = new mydataaccess1();
        DataTable dt = new DataTable();
        DateTime Date1 = Convert.ToDateTime(TextBox1.Text);
        DateTime Date2 = Convert.ToDateTime(TextBox2.Text);
        if (Convert.ToInt16(ddlCheckSheet.SelectedValue) > 0)
            dt = da.car_bulk_data(Convert.ToInt16(ddlCheckSheet.SelectedValue), drpcircle.SelectedValue, ddlprovidername.SelectedItem.Text, Date1, Date2);
        if (dt.Rows.Count > 0)
        {
            string filename = "Bulk_car.xls";
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
            Response.Clear();

            InitializeWorkbook();
            exporttoexcel(dt);
            Response.BinaryWrite(WriteToStream().GetBuffer());

        }

    }
    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }

    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");

        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                if (j == 1)
                {

                    row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
                }
                else
                {
                    row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
                }
            }
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            ba = new myvodav2();

            string abc = FileUpload1.FileName;
            abc = SpacialCharRemove.SpacialChar_Remove(abc);
            if (abc == "")
            {
                // lblresult.Text = "Select file first.";
                // lblresult.Visible = true;
            }
            else
            {
                abc = System.DateTime.Now.Millisecond + System.Guid.NewGuid().ToString().Substring(0, 4) + abc;
                FileUpload1.SaveAs(Server.MapPath("~/SampleData/sitedata/") + abc);
                string path = Server.MapPath("~/SampleData/sitedata/") + abc;
                string excelConnectionString = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\";";

                string constring = excelConnectionString;
                int result;
                string sp_user_d = "";
                string bunch = System.Guid.NewGuid().ToString().Substring(0, 4);

                da = new mydataaccess1();
                dt = new DataTable();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());


                result = Convert.ToInt32(da.import_car(excelConnectionString, sp_user_d, bunch, Convert.ToInt16(ddlCheckSheet.SelectedValue)));
                da = new mydataaccess1();
                dt = new DataTable();
                dt = da.select_car_error_data(bunch);
                if (dt.Rows.Count > 0)
                {
                    string filename = "error_car.xls";
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                    Response.Clear();

                    InitializeWorkbook();
                    exporttoexcel(dt);
                    Response.BinaryWrite(WriteToStream().GetBuffer());

                }
                else
                {

                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Data Uploadaded Successfully... ');</script>");
                    BindGrid();
                }


            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx", false);
        }

    }
    protected void grdMaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.Cells[8].Text == "Open")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Red;
                e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
            }
            else if (e.Row.Cells[8].Text == "Close")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Green;
                e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
            }
            else if (e.Row.Cells[8].Text == "WIP")
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Yellow;
                e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
            }
            else
            {
                e.Row.Cells[0].BackColor = System.Drawing.Color.Khaki;
                e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
            }
        }
    }
    protected void ddlprovidername_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {

        BindGrid();
    }
}
