﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using mybusiness;
using System.Data;

public partial class AllPage_admin_20daysreminder : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    DataRow row;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int Role = 3;
            da = new mydataaccess1();
            string user = da.select_user_cookie(Session["user"].ToString());
            if (Session["role"].ToString() != Role.ToString() && Session["um"].ToString()!=user)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {
                da = new mydataaccess1();
                DataTable dtv5 = new DataTable();
                dtv5 = da.Dashboard_All_Active(user);
                grd20.DataSource = dtv5;
                grd20.DataBind();

                da = new mydataaccess1();
                DataTable dtallp = new DataTable();
                dtallp = da.Dashboard_All_Active_punchpoint(user);
                grdallp.DataSource = dtallp;
                grdallp.DataBind();

                da = new mydataaccess1();
                DataTable dtbts = new DataTable();
                dtbts = da.Dashboard_BTS(user);
                grd10.DataSource = dtbts;
                grd10.DataBind();

                da = new mydataaccess1();
                DataTable dtbtsp = new DataTable();
                dtbtsp = da.Dashboard_BTS_punchpoint(user);
                grdbtsp.DataSource = dtbtsp;
                grdbtsp.DataBind();

                da = new mydataaccess1();
                DataTable dtmw = new DataTable();
                dtmw = da.Dashboard_MW(user);
                grdmw.DataSource = dtmw;
                grdmw.DataBind();

                da = new mydataaccess1();
                DataTable dtmwp = new DataTable();
                dtmwp = da.Dashboard_MW_punchpoint(user);
                grdmwp.DataSource = dtmwp;
                grdmwp.DataBind();

                da = new mydataaccess1();
                DataTable dttmea= new DataTable();
                dttmea = da.Dashboard_TMEA(user);
                grdtmea.DataSource = dttmea;
                grdtmea.DataBind();

                da = new mydataaccess1();
                DataTable dttmeap = new DataTable();
                dttmeap = da.Dashboard_TMEA_punchpoint(user);
                grdtmeap.DataSource = dttmeap;
                grdtmeap.DataBind();


                da = new mydataaccess1();
                DataTable dtbsc = new DataTable();
                dtbsc = da.Dashboard_BSC(user);
                grdbsc.DataSource = dtbsc;
                grdbsc.DataBind();

                da = new mydataaccess1();
                DataTable dtbscp = new DataTable();
                dtbscp = da.Dashboard_BSC_punchpoint(user);
                grdbscp.DataSource = dtbscp;
                grdbscp.DataBind();

                da = new mydataaccess1();
                DataTable dtiv = new DataTable();
                dtiv = da.Dashboard_IV(user);
                grdinfra.DataSource = dtiv;
                grdinfra.DataBind();

                da = new mydataaccess1();
                DataTable dtivp = new DataTable();
                dtivp = da.Dashboard_IV_punchpoint(user);
                grdivp.DataSource = dtivp;
                grdivp.DataBind();
               
            }
        }
    }
    private string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
        string newSortDirection = String.Empty;

        switch (sortDirection)
        {
            case SortDirection.Ascending:
                newSortDirection = "ASC";
                break;

            case SortDirection.Descending:
                newSortDirection = "DESC";
                break;
        }

        return newSortDirection;
    }
    protected void grd20_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grd20.DataSource as DataTable;


        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grd20.DataSource = dataView;
            grd20.DataBind();
        }
    }
    protected void grd10_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grd10.DataSource as DataTable;


        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grd10.DataSource = dataView;
            grd10.DataBind();
        }
    }
  
}
