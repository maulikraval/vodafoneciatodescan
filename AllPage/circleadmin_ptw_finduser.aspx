﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="circleadmin_ptw_finduser.aspx.cs" Inherits="AllPage_admin_finduser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="~/AllPage/circleadmin_menu_ptw.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HSW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="admin_styleinner1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/cufon-yui.js"></script>

    <script type="text/javascript" src="../js/arial.js"></script>

    <script type="text/javascript" src="../js/cuf_run.js"></script>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <link href="../css/menu.css" rel="stylesheet" type="text/css" />

    <script src="../js/menu.js" type="text/javascript"></script>

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        <!
        -- #roll a
        {
            display: inline-block;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        -- > .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 724px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="main">
        <div class="header">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                        <tr>
                            <td width="6%">
                                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style1">
                                <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change Password</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                        </tr>
<tr>
                            <td width="6%">
                            </td>
                            <td class="style1">
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:LinkButton ID="lnkchangeproject" runat="server" OnClick="lnkchange_Click" CausesValidation="False"
                                    PostBackUrl="~/login/ciat_ptw.aspx" Visible="False">Change 
                                Project</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clr">
                </div>
                <uc2:Menu ID="menubar" runat="server" />
                <div class="clr">
                </div>
            </div>
        </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
                <div class="mainbar">
                    <div class="article1">
                        <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server" Text="abc" CssClass="lblmarquee"></asp:Label>
                    
                    </marquee>
                    </div>
                </div>
                <div class="sidebar">
                    <div class="gadget">
                        <asp:ScriptManager ID="SM1" runat="server">
                        </asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <table class="style2">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlcircle" runat="server" OnSelectedIndexChanged="ddlcircle_SelectedIndexChanged"
                                                AutoPostBack="true" Width="200px" CssClass="genall">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" CssClass="lblall" Font-Bold="False" Text="Enter User"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="TextBox1" runat="server" Width="100px"></asp:TextBox>
                                            <asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox ID="lbuser" runat="server" Width="200px" 
                                                Font-Names="Calibri" Font-Size="13px" Height="200px" AutoPostBack="True"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="mainbar">
                    <div class="article">
                        <center>
                            <asp:Label ID="Label3" runat="server" CssClass="lblstly">View Distance Details</asp:Label></center>
                        <hr />
                        <asp:UpdatePanel ID="up2" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="GridView1" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                                                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Site Details Found..!"
                                                ForeColor="Black" Width="100%">
                                                <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                                <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                <FooterStyle BackColor="#CCCCCC" />
                                                <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                                <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Font-Names="Calibri" Font-Size="13px" ForeColor="Black"
                                                Text="SiteId :"></asp:Label>
                                            &nbsp;<asp:TextBox ID="txtsite" runat="server" OnTextChanged="txtsite_TextChanged"
                                                Width="200px"></asp:TextBox>
                                            &nbsp;
                                            <asp:LinkButton ID="lbsearch" runat="server" Font-Underline="True" OnClick="lbsearch_Click">Search</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblerror" runat="server" ></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="panel1" Visible="false" runat="server" Width="730px" Height="207px"
                                                ScrollBars="Horizontal">
                                                <asp:Label ID="lblerror0" runat="server" >Requested Site Details Shown Below :</asp:Label>
                                                <asp:GridView ID="grdinfo" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                                                    BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                                                    Width="100%" EmptyDataText="No Site Details Found..!">
                                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                                    <FooterStyle BackColor="#CCCCCC" />
                                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                                <center>
                                                    <asp:Label ID="Label2" runat="server" CssClass="lblall" ></asp:Label></center>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
            <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
               <%-- <p class="lf">
                    &copy; Copyright 2011 <a href="#"></a>.</p>
                <p class="rf">
                    Powered By. Teleysia Networks Pvt. Ltd.</p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
