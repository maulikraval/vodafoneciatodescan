﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using System.Data;


public partial class AllPage_T_DH_close : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drp1AtRiskStatus.ClearSelection();
            /*drpPTWStatus.ClearSelection();*/

        }
        //ImageButton1.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearchBlkId.Text != "")
        {
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.select_ptw_Master_report_night("", "where ptw_basic_detail_master.ptwid='" + txtSearchBlkId.Text + "'");
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.Visible = true;
            Panel1.Visible = true;
            lbl1risk.Visible = true;
            /*lblptw.Visible = true;*/
            drp1AtRiskStatus.Visible = true;
            /*drpPTWStatus.Visible = true;*/
            btnUpdate.Visible = true;
            if (GridView1.Rows.Count > 0)
            {
                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    CheckBox chkdelete = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkSelect");
                    CheckBox chkall = (CheckBox)GridView1.HeaderRow.Cells[0].FindControl("chkSelectAll");
                    string ptwexid = GridView1.Rows[i].Cells[7].Text;
                    /*string ptw_status = GridView1.Rows[i].Cells[15].Text;*/
                    int id2 = ptwexid.LastIndexOf("/") + 1;
                    int id = Convert.ToInt32(ptwexid.Substring(id2, (ptwexid.Length - id2)));
                    chkdelete.ToolTip = id.ToString();
                    if (dt.Rows[i]["PTW Status"].ToString() == "Awaiting approval for new")
                    {
                        drp1AtRiskStatus.Items.FindByText("Closed By Receiver").Enabled = false;
                        drp1AtRiskStatus.Items.FindByText("Rejected").Enabled = true;
                        GridView1.Rows[i].Cells[0].Enabled = false;
                        chkdelete.Checked = true;
                        chkall.Checked = true;
                        drp1AtRiskStatus.ClearSelection();
                    }
                    else if (dt.Rows[i]["PTW Status"].ToString() == "Approved")
                    {
                        drp1AtRiskStatus.Items.FindByText("Closed By Receiver").Enabled = true;
                        drp1AtRiskStatus.Items.FindByText("Rejected").Enabled = false;
                    }
                    else if (dt.Rows[i]["PTW Status"].ToString() == "Rejected")
                    {
                        GridView1.Rows[i].Cells[0].Enabled = false;
                        chkdelete.Checked = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Issuer Assigned"].ToString()) + "'/SuperAdmin Has Rejected Your '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Route"].ToString()) + "' For This PTW:- '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Blanket PTW ID"].ToString()) + "');</script>");
                    }
                    else if (dt.Rows[i]["PTW Status"].ToString() == "Closed By Receiver")
                    {
                        GridView1.Rows[i].Cells[0].Enabled = false;
                        chkdelete.Checked = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('You Have Closed '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Route"].ToString()) + "' For This PTW:- '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Blanket PTW ID"].ToString()) + "');</script>");
                    }
                }
            }
            drp1AtRiskStatus.ClearSelection();
        }
        else
        {
            GridView1.Visible = false;
            Panel1.Visible = false;
        }
    }

    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.ClearContent();
    //    string FileName = "PTWCustomizeReport_" + DateTime.Now + ".xls";
    //    Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
    //    Response.ContentType = "application/excel";
    //    System.IO.StringWriter sw = new System.IO.StringWriter();
    //    HtmlTextWriter htw = new HtmlTextWriter(sw);
    //    GridView1.RenderControl(htw);
    //    Response.Write(sw.ToString());
    //    Response.End();
    //}

    protected void drp1AtRiskStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drp1AtRiskStatus.SelectedIndex == 1)
        {
            /*drpPTWStatus.Items.FindByText("Closed By Issuer").Enabled = true;
            drpPTWStatus.Items.FindByText("Closed By Receiver").Enabled = true;
            drpPTWStatus.Items.FindByText("Rejected").Enabled = false;*/
        }
        else if (drp1AtRiskStatus.SelectedIndex == 2)
        {

            /*drpPTWStatus.Items.FindByText("Rejected").Enabled = true;
            drpPTWStatus.Items.FindByText("Closed By Issuer").Enabled = false;
            drpPTWStatus.Items.FindByText("Closed By Receiver").Enabled = false;*/
        }
        else
        {
            /*drpPTWStatus.Items.FindByText("Rejected").Enabled = false;
            drpPTWStatus.Items.FindByText("Closed By Issuer").Enabled = false;
            drpPTWStatus.Items.FindByText("Closed By Receiver").Enabled = false;*/
        }
    }

    protected void drpPTWStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (drp1AtRiskStatus.SelectedIndex != 0)
        {
            if (GridView1.Rows.Count > 0)
            {
                for (int j = 0; j < GridView1.Rows.Count; j++)
                {
                    CheckBox chkdelete = (CheckBox)GridView1.Rows[j].Cells[0].FindControl("chkSelect");
                    string ptwblkid = GridView1.Rows[j].Cells[5].Text;
                    string ptwexid = GridView1.Rows[j].Cells[7].Text;

                    if ((chkdelete.Checked) && (chkdelete.Enabled = true))
                    {
                        da = new mydataaccess1();
                        int ptwid2 = ptwblkid.LastIndexOf("/") + 1;
                        int ptwid = Convert.ToInt32(ptwblkid.Substring(ptwid2, (ptwblkid.Length - ptwid2)));
                        int id2 = ptwexid.LastIndexOf("/") + 1;
                        int id = Convert.ToInt32(ptwexid.Substring(id2, (ptwexid.Length - id2)));
                        da.t8dh_report_update(id, ptwid, Convert.ToInt32(drp1AtRiskStatus.SelectedItem.Value), "");
                    }
                    //else
                    //{
                    //    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select CheckBox First!!!');</script>");
                    //    break;
                    //}
                }

                da = new mydataaccess1();
                dt = new DataTable();
                dt = da.select_ptw_Master_report_night("", "where ptw_basic_detail_master.ptwid='" + txtSearchBlkId.Text + "'");
                GridView1.DataSource = dt;
                GridView1.DataBind();
                GridView1.Visible = true;
                Panel1.Visible = true;
                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    CheckBox chkdelete = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkSelect");
                    CheckBox chkall = (CheckBox)GridView1.HeaderRow.Cells[0].FindControl("chkSelectAll");
                    string ptwexid = GridView1.Rows[i].Cells[7].Text;
                    /*string ptw_status = GridView1.Rows[i].Cells[15].Text;*/
                    int id2 = ptwexid.LastIndexOf("/") + 1;
                    int id = Convert.ToInt32(ptwexid.Substring(id2, (ptwexid.Length - id2)));
                    chkdelete.ToolTip = id.ToString();
                    if (dt.Rows[i]["PTW Status"].ToString() == "Awaiting approval for new")
                    {
                        drp1AtRiskStatus.Items.FindByText("Closed By Receiver").Enabled = false;
                        drp1AtRiskStatus.Items.FindByText("Rejected").Enabled = true;
                        GridView1.Rows[i].Cells[0].Enabled = false;
                        chkdelete.Checked = true;
                        chkall.Checked = true;
                        drp1AtRiskStatus.ClearSelection();
                    }
                    else if (dt.Rows[i]["PTW Status"].ToString() == "Approved")
                    {
                        drp1AtRiskStatus.Items.FindByText("Closed By Receiver").Enabled = true;
                        drp1AtRiskStatus.Items.FindByText("Rejected").Enabled = false;
                    }
                    else if (dt.Rows[i]["PTW Status"].ToString() == "Rejected")
                    {
                        GridView1.Rows[i].Cells[0].Enabled = false;
                        chkdelete.Checked = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Issuer Assigned"].ToString()) + "'/SuperAdmin Has Rejected Your '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Route"].ToString()) + "' For This PTW:- '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Blanket PTW ID"].ToString()) + "');</script>");
                    }
                    else if (dt.Rows[i]["PTW Status"].ToString() == "Closed By Receiver")
                    {
                        GridView1.Rows[i].Cells[0].Enabled = false;
                        chkdelete.Checked = false;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('You Have Closed '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Route"].ToString()) + "' For This PTW:- '" + SpacialCharRemove.XSS_Remove(dt.Rows[i]["Blanket PTW ID"].ToString()) + "');</script>");
                    }
                }
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView1.Visible = false;
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('You Have No Any Record To Update Status!!!');</script>");
                drp1AtRiskStatus.ClearSelection();
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select 1@Risk Status!!!');</script>");
        }

        //if (dt.Rows.Count == 0)
        //{
        //da = new mydataaccess1();
        //dt = new DataTable();
        //dt = da.select_ptw_Master_report_night("", "where ptw_basic_detail_master.ptwid='" + txtSearchBlkId.Text + "'");

        /*for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            if (dt.Rows[i]["PTW Status"].ToString() == "Closed By Receiver")
            {
                //CheckBox chkdelete = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkSelect");
                string ptwblkid = dt.Rows[i]["Blanket PTW ID"].ToString();
                string ptwexid = dt.Rows[i]["Individual_PTW_ID"].ToString();

                da = new mydataaccess1();
                int ptwid2 = ptwblkid.LastIndexOf("/") + 1;
                int ptwid = Convert.ToInt32(ptwblkid.Substring(ptwid2, (ptwblkid.Length - ptwid2)));
                int id2 = ptwexid.LastIndexOf("/") + 1;
                int id = Convert.ToInt32(ptwexid.Substring(id2, (ptwexid.Length - id2)));
                da.t8dh_report_update(id, ptwid, 3, "Close");
            }
        }
        drp1AtRiskStatus.SelectedIndex = 0;*/

        //}

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string strScript = "SelectDeSelectHeader(" + ((CheckBox)e.Row.Cells[0].FindControl("chkSelect")).ClientID + ");";
                ((CheckBox)e.Row.Cells[0].FindControl("chkSelect")).Attributes.Add("onclick", strScript);
            }
        }
        catch (Exception Ex)
        {
            //report error
        }
    }
}