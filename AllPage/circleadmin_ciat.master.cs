﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using business;
using dataaccesslayer;
using System.Data;
using mybusiness;

public partial class circle_admin_ciat : System.Web.UI.MasterPage
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
		  da = new mydataaccess1();
                   string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (!IsPostBack)
        {


            try
            {
string strPreviousPage = "";
 if (Request.UrlReferrer != null)
   {
    strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];         
    }       
if(strPreviousPage =="")
    {
       Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
     }

                if (Session["flag"].ToString() == "4")
                {
                    lnkchangeproject.Visible = true;
                }
                else
                {
                    lnkchangeproject.Visible = false;
                }

                // marque Start
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                da = new mydataaccess1();
                dt = new DataTable();
                string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                da = new mydataaccess1();
                DataTable dtv5 = new DataTable();
                dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                da = new mydataaccess1();
                DataTable dtv2 = new DataTable();
                dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                da = new mydataaccess1();
                DataTable dtv2ms = new DataTable();
                dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);

                da = new mydataaccess1();
                DataTable dtall = new DataTable();
                dtall = da.reminder_20_days_circle_admin_all(sp_user_d);

                da = new mydataaccess1();
                DataTable cvall = new DataTable();
                cvall = da.reminder_20_days_critical_all_circle_admin(sp_user_d);

                da = new mydataaccess1();
                DataTable dtall1 = new DataTable();
                dtall1 = da.reminder_10_days_circle_admin_all(sp_user_d);

                count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                // critical points
                da = new mydataaccess1();
                DataTable cv2 = new DataTable();
                cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                da = new mydataaccess1();
                DataTable cv5 = new DataTable();
                cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                da = new mydataaccess1();
                DataTable cv2ms = new DataTable();
                cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                da = new mydataaccess1();
                DataTable dtv51 = new DataTable();
                dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                da = new mydataaccess1();
                DataTable dtv21 = new DataTable();
                dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                da = new mydataaccess1();
                DataTable dtv2ms1 = new DataTable();
                dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";

                //Marquee End

                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                int i = 3;
                if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString()==sp_user_)
                {

                }
                else
                {

                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();

                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            string r = da.select_user_cookie(Session["user"].ToString());

            da = new mydataaccess1();
            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}
