﻿<%@ Page Language="C#" MasterPageFile="~/Support/Support.master" AutoEventWireup="true"
    CodeFile="UploadIssuer.aspx.cs" Inherits="admin_Imei" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript">
        function ValidateFileUpload(Source, args) {
            var fuData = document.getElementById('<%= SpacialCharRemove.XSS_Remove(FileUpload1.ClientID) %>');
            var FileUploadPath = fuData.value;

            if (FileUploadPath == '') {
                // There is no file selected 
                args.IsValid = false;
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "xls") {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
    </script>

    <table width="100%">
        <body>
            <tr>
                <td width="20%">&nbsp;
                </td>
                <td width="30%" align="right" colspan="2" style="width: 60%; text-align: center">
                    <asp:Label ID="Label2" runat="server" Font-Bold="False"
                        Font-Underline="False" Text="Upload Permit Issuer Details" CssClass="lblstly"></asp:Label>
                </td>
                <td width="20%">&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 40%; text-align: center height: 26px">
                    <hr />
                </td>
            </tr>
            <tr>
                <td width="20%" style="height: 29px"></td>
                <td width="30%" align="right" style="height: 29px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="False"
                        Text="Select File To Upload" CssClass="lblall"></asp:Label>
                </td>
                <td width="30%" align="left" style="height: 29px">
                    <asp:FileUpload ID="FileUpload1" runat="server" Font-Size="Medium"
                        CssClass="genall" />
                    <%--   <asp:RegularExpressionValidator ID="MyFileUpLoadValidator" runat="server" ErrorMessage="Upload .xls files only."
                        ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.xls|.XLS)$"
                        ControlToValidate="FileUpload1">
                    </asp:RegularExpressionValidator>
                    --%>
                </td>
                <td width="20%" style="height: 29px"></td>
            </tr>
            <tr>
                <td width="20%">&nbsp;
                </td>
                <td width="30%" align="right" colspan="2" style="width: 60%; text-align: center">
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please select valid .xls file"></asp:CustomValidator>
                </td>
                <td width="20%">&nbsp;
                </td>
            </tr>
            <tr>
                <td width="20%" style="height: 26px">&nbsp;
                </td>
                <td width="30%" align="right" colspan="2"
                    style="width: 60%; text-align: center; height: 26px;">
                    <asp:Button ID="Button1" runat="server" Font-Size="Medium" Height="30px" OnClick="Button1_Click"
                        Text="Upload" Width="100px" />
                </td>
                <td width="20%" style="height: 26px">&nbsp;
                </td>
            </tr>
            <tr>
                <td width="20%" style="height: 23px"></td>
                <td width="30%" align="right" style="height: 23px; width: 60%; text-align: center" colspan="2">
                    <asp:Label ID="lblresult" runat="server" Font-Bold="False"
                        Font-Underline="False"
                        Visible="False"></asp:Label>
                </td>
                <td width="20%" style="height: 23px"></td>
            </tr>
    </table>
</asp:Content>
