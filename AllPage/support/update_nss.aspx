﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Support/Support.master" AutoEventWireup="true" CodeFile="update_nss.cs" Inherits="Support_FindUser1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbl_User" runat="server" Text="Site ID"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_site" runat="server"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_Site" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                    
                </td>
            </tr>
  		<tr>
                <td>
                    <asp:Label ID="lbl_contact1" runat="server" Text="NSS ID"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_nss" runat="server"></asp:TextBox>
  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_nss" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                  
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_contact" runat="server" Text="Update By"></asp:Label>
                </td>
                <td>
                    &nbsp
                </td>
                <td>
                  <asp:DropDownList ID="drpsupport" runat="server">
                        <asp:ListItem>---Select Your Name---</asp:ListItem>
                        <asp:ListItem>Bhumit</asp:ListItem>
                        <asp:ListItem>Mahendra</asp:ListItem>
                        <asp:ListItem>Meet</asp:ListItem>
                        <asp:ListItem>Astha</asp:ListItem>
                        <asp:ListItem>Poem</asp:ListItem>
                        <asp:ListItem>Chetan</asp:ListItem>
			<asp:ListItem>Darshini</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="drpsupport" ErrorMessage="*" InitialValue="---Select Your Name---"></asp:RequiredFieldValidator>
                </td>
                <td>
                    &nbsp
                </td>
                <td style="text-align:center">
                    <asp:Button ID="btn_find_contact" runat="server" Text="UPDATE" OnClick="btn_find_contact_Click" />
                </td>
            </tr>
        </table>
    </div>
  
</asp:Content>

