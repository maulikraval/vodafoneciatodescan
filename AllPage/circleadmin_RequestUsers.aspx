﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="circleadmin_RequestUsers.aspx.cs" Inherits="circle_admin_RequestUsers" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" type="text/css" href="../HTML/style.css">
    <link rel="stylesheet" type="text/css" href="../chrometheme/chromestyle4.css" />

    <script type="text/javascript" src="../chromejs/chrome.js"></script>

    <style type="text/css">
        .style1
        {
            width: 221px;
        }
        .style2
        {
            width: 100%;
        }
    </style>
    <table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
        <tr>
            <table border="0" cellspacing="0" cellpadding="0" width="1000">
                <tr valign="top">
                    <td>
                        <!-- main -->
                        <!-- /main -->
                        <table class="style2">
                            <tr>
                                <td style="text-align: center;">
                                    <asp:Label ID="lblusername" runat="server" Enabled="False" Visible="False"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" Font-Names="Calibri" Font-Size="X-Large" ForeColor="#FFA500"
                                        Text="Request From Technician"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="1030px" Height="250px">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                                            EmptyDataText="No Requests Found ...!">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Modify">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="31px" ImageUrl="~/images/modify-key-icon.png"
                                                            Width="22px" OnClick="ImageButton1_Click" ToolTip='<%# Eval("site") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reset">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="31px" ImageUrl="~/images/reset_button-297x300.jpg"
                                                            Width="22px" OnClick="ImageButton2_Click" ToolTip='<%# Eval("site") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Site ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("siteid") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="20%" />
                                                    <ItemStyle Font-Names="Calibri" Font-Size="Large" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Technician Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label11" runat="server" Text='<%# Bind("technician_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="20%" />
                                                    <ItemStyle Font-Names="Calibri" Font-Size="Large" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No Time Requested">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label111" runat="server" Text='<%# Bind("counter") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="20%" />
                                                    <ItemStyle Font-Names="Calibri" Font-Size="Large" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Request Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label1111" runat="server" Text='<%# Bind("Technician_Request_datetechnician_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="20%" />
                                                    <ItemStyle Font-Names="Calibri" Font-Size="Large" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Check Sheet">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label11111" runat="server" Text='<%# Bind("check_sheet") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="20%" />
                                                    <ItemStyle Font-Names="Calibri" Font-Size="Large" HorizontalAlign="Left" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--  <tr bgcolor="#666666">
                        <td height="16px" style="background-color: #C0C0C0">
                        </td>
                    </tr>--%>
                <%-- <tr>
                        <td>
                            <img src="HTML/images/px1.gif" width="1" height="1" alt="" border="0"><p style="font-size: 10px;"
                                align="center">
                                Copyright 2011 ©2010<a href="http://www.teleysia.com"> Teleysia Networks Pvt Ltd.</a>
                                All rights reserved.</p>
                        </td>
                    </tr>--%>
                <%-- <tr bgcolor="#666666">
                        <td height="16px" style="background-color: #C0C0C0">
                        </td>
                    </tr>--%>
            </table>
            <br>
</asp:Content>
