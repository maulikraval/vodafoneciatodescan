﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using mybusiness;
using dataaccesslayer;
using NPOI.HPSF;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel.Contrib;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Collections.Generic;

public partial class admin_Uploadcompany : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    HSSFWorkbook hssfworkbook;
    protected void Page_Load(object sender, EventArgs e)
    {
	 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (Session["role"].ToString() != "4" && Session["um"].ToString()!=sp_user_)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.End();
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    private bool CompareArray(byte[] a1, byte[] a2)
    {

        if (a1.Length != a2.Length)

            return false;



        for (int i = 0; i < a1.Length; i++)
        {

            if (a1[i] != a2[i])

                return false;

        }



        return true;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
            imageHeader.Add("XLS", new byte[] { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 });

            // bool acceptFile = false;
            // bool acceptFile1 = false;
            byte[] header;
            string fileExt;

            fileExt = FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.') + 1).ToUpper();
            string acceptedFileTypes = ".XLS";



            byte[] tmp = imageHeader[fileExt];

            header = new byte[tmp.Length];



            // GET HEADER INFORMATION OF UPLOADED FILE

            FileUpload1.FileContent.Read(header, 0, header.Length);



            if (CompareArray(tmp, header))
            {
                try
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();

                    string abc = FileUpload1.FileName;
                    abc = SpacialCharRemove.SpacialChar_Remove(abc);
                    if (abc == "")
                    {
                        lblresult.Text = "Select file first.";
                        lblresult.Visible = true;
                    }
                    else
                    {
                        FileUpload1.SaveAs(Server.MapPath("~/SampleData/sitedata/") + abc);
                        string path = Server.MapPath("~/SampleData/sitedata/") + abc;
                      //  string excelConnectionString = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                        string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                        string constring = excelConnectionString;
                        int result;
                        string sp_user_d = "";

                        da = new mydataaccess1();
                        dt = new DataTable();
                        sp_user_d = da.select_user_cookie(Session["user"].ToString());

                        result = Convert.ToInt32(da.insertcompanyname(constring, sp_user_d));

                        da = new mydataaccess1();
                        dt = new DataTable();
                        dt = da.select_error_company();
                        if (dt.Rows.Count > 0)
                        {
                            string filename = "Error.xls";
                            Response.ContentType = "application/vnd.ms-excel";
                            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
                            Response.Clear();

                            InitializeWorkbook();
                            exporttoexcel(dt);
                            Response.BinaryWrite(WriteToStream().GetBuffer());

                        }
                        else
                        {
                            lblresult.Visible = true;
                            lblresult.Text = "Companies Successfully Uploaded.";
                        }

                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx", false);
                }
            }
        }
        catch
        {
            lblresult.Visible = true;
            lblresult.Text = "Upload wrong format,please upload .xls file.";
        }
    }
    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }

    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}



