﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mybusiness;
using dataaccesslayer;
using dataaccesslayer2;
using System.Data;
using System.IO;

public partial class AllPage_pmt_Active_Techician_Assigne : System.Web.UI.Page
{
    mydataaccess1 da1;
    // mydataaccess2 da;
    DataTable dt;
    myvodav2 ba;
    myvodav23 ba1;
    string sp_user_d;
    string siteid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        da1 = new mydataaccess1();
        string sp_user_ = da1.select_user_cookie(Session["user"].ToString());
        try
        {
            da1 = new mydataaccess1();
            sp_user_d = da1.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);


            if (!IsPostBack)
            {
                try
                {
                    trddlsheet.Visible = false;
                    trlblsheet.Visible = false;

                    ImageButton1.Visible = false;

                    int i = 12;
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString() == sp_user_)
                    {

                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "3")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //Marquee End

                    //if (Session.Count != 0)
                    {
                        if (Session["role"].ToString() == "12")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da1 = new mydataaccess1();
                                sp_user_d = da1.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                                if (lblusername.Text == "")
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);
                                }
                                da1 = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da1.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                //drpcircle.Items.Insert(1, "All");

                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable zone = new DataTable();

            if (drpcircle.SelectedIndex > 0)
            {
                ddlsheet.SelectedIndex = 0;
                trlblsheet.Visible = false;
                trddlsheet.Visible = false;

                GridView1.DataSource = null;
                GridView1.DataBind();
                {
                    da1 = new mydataaccess1();
                    myvodav2 vb = new myvodav2();
                    vb.Circle = drpcircle.SelectedItem.Text.ToString();
                    zone = da1.getzonesbycirclename(vb);
                    if (zone.Rows.Count > 0)
                    {
                        GridView1.DataSource = null;
                        GridView1.DataBind();

                        DataTable tech = new DataTable();
                        da1 = new mydataaccess1();
                        string sp_user_d1 = da1.select_user_cookie(Session["user"].ToString());
                        da1 = new mydataaccess1();
                        tech = da1.pm_gettechnician(drpcircle.SelectedItem.Text, sp_user_d1);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlpunchpoint_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlcriticality_SelectedIndexChanged(object sender, EventArgs e)
    {
        trddlsheet.Visible = true;
        trlblsheet.Visible = true;
        trlblfrom.Visible = true;
    }

    protected void ddlsheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlsheet.SelectedIndex > 0)
            {
                if (ddlsheet.SelectedIndex == 1)
                {
                    ImageButton1.Visible = true;

                    GridView1.DataSource = null;
                    GridView1.DataBind();

                    trlblfrom.Visible = true;
                    trtxtfromdate.Visible = true;

                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
                else
                {
                    ImageButton1.Visible = true;
                    trlblfrom.Visible = true;
                    trtxtfromdate.Visible = true;
                    ImageButton1.Visible = true;

                    GridView1.DataSource = null;

                    trtxtfromdate.Visible = true;
                    trtxtfromdate.Visible = true;

                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
            else
            {
                trlblfrom.Visible = false;
                trtxtfromdate.Visible = false;
                trlblto.Visible = false;
                trtxttodate.Visible = false;

                trlblfrom.Visible = false;
                trtxtfromdate.Visible = false;
                trlblto.Visible = false;
                trtxttodate.Visible = false;

                ImageButton1.Visible = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void txttodate_Click(object sender, EventArgs e)
    {
        ImageButton1.Visible = true;
    }
    string circle = "";
    string punchpoint = "";
    int sheet;
    string criticality = "";
    string from = "";
    string to = "";
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (ddlpunchpoint.SelectedIndex > 0)
        {
            circle = drpcircle.SelectedItem.Text;
            punchpoint = ddlpunchpoint.SelectedItem.Text;
        }

        sheet = Convert.ToInt32(ddlsheet.SelectedValue);

        if (ddlcriticality.SelectedIndex != 0)
        {
            criticality = ddlcriticality.SelectedItem.Text;
        }
        else
        {
            criticality = ddlcriticality.SelectedItem.Text;
        }
        from = txtfromdate.Text;
        to = txttodate.Text;

        mydataaccess1 da = new mydataaccess1();
        string username = da.select_user_cookie(Session["user"].ToString());

        mydataaccess1 da12 = new mydataaccess1();
        DataTable dt2 = new DataTable();


        //  string wherecondition = "WHERE SM.CIRCLE = '" + circle + "' AND PPM.Punch_Point= '" + punchpoint + "' AND QMAC.VIOLATION_SEVERITY = '" + criticality + "' AND CM.ID= '" + sheet + "' AND TRMA.SURVEYDATE BETWEEN '" + from + "' AND '" + to + "'";

        //  dt2 = da12.Punchpoint_Excel_Report_Active(circle, punchpoint, criticality, sheet, from, to, username);
        // dt = da1.pm_TechnicianReport_Active_new(circle, punchpoint, sheet, criticality, technicion);
        if (punchpoint != "")
        {
            if (criticality != "" && criticality != "Select Criticality")
            {
                string wherecondition1 = wherecondition(circle, punchpoint, criticality, sheet, from, to);
                dt2 = da12.Punchpoint_Excel_Report_Active(wherecondition1, username);
                if (dt2.Rows.Count > 0)
                {
                    GridView1.DataSource = dt2;
                    GridView1.DataBind();
                }
                else
                {
                    ImageButton1.Visible = false;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select Criticality Value');</script>");
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select Punch Point Value');</script>");
        }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";

        Response.Clear();

        Response.ClearHeaders();

        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        Response.ContentType = "application/vnd.ms-excel";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }

    protected string wherecondition(string circle, string punch_point, string criticality, int sheet, string from, string to)
    {
        //string wherecondition = "WHERE SM.CIRCLE = '" + circle + "' AND PPM.Punch_Point= '" + punchpoint + "' AND QMAC.VIOLATION_SEVERITY = '" + criticality + "' AND CM.ID= '" + sheet + "' AND TRMA.SURVEYDATE BETWEEN '" + from + "' AND '" + to + "'";
        string where_c = "WHERE ";
        if (circle != "ALL")
        {
            where_c += "SM.CIRCLE = '" + circle + "'";
        }
        if (punch_point.ToUpper() != "ALL")
        {
            where_c += "and  PPM.Punch_Point = '" + punch_point + "'";
        }
        if (criticality.ToUpper() != "ALL")
        {
            where_c += "AND QMAC.VIOLATION_SEVERITY = '" + criticality + "'";
        }
        if (sheet != 0 && sheet != 1)
        {
            where_c += " AND CM.ID= '" + sheet + "'";
        }
        if (from.ToUpper() != "ALL")
        {
            where_c += " AND  (TRMA.SURVEYDATE >= CONVERT(DATETIME, '" + from + "', 102)) and (TRMA.SURVEYDATE <= CONVERT(DATETIME, '" + to + "  23:59:59', 102)) ";

        }

        return where_c;



    }
}