﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;

public partial class AllPage_admin_PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    int userid;
    int Step1;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        //Response.Cache.SetExpires(DateTime.Now);
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //Response.Cache.SetNoStore();
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        try
        {
            //if (ddlstatus.SelectedIndex == 2)
            //{
            //    pnn.Visible = false;
            //}
            //if (ddlstatusall.SelectedIndex == 2)
            //{
            //    pnn.Visible = false;
            //}
            //else
            //{
            // pnn.Visible = true;
            //   }
            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }


            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
            string User = lblusername.Text;
            da = new mydataaccess1();
            userid = da.selectuserid(User);
            if (!IsPostBack)
            {
                try
                {
                    Label3.Visible = false;
                    Label4.Visible = false;
                    Label5.Visible = false;
                    TextBox1.Visible = false;
                    TextBox2.Visible = false;
                    btnsearch.Visible = false;
                    ImageButton1.Visible = false;
                    ImageButton2.Visible = false;
                    int i = 4;
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString() == sp_user_d)
                    {

                    }
                    else
                    {

                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }

            try
            {

                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {


                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da = new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    /*  da = new mydataaccess1();
                      DataTable dtv5 = new DataTable();
                      dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                      da = new mydataaccess1();
                      DataTable dtv2 = new DataTable();
                      dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                      da = new mydataaccess1();
                      DataTable dtv2ms = new DataTable();
                      dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);
                      count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                      // critical points
                      da = new mydataaccess1();
                      DataTable cv2 = new DataTable();
                      cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                      da = new mydataaccess1();
                      DataTable cv5 = new DataTable();
                      cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                      da = new mydataaccess1();
                      DataTable cv2ms = new DataTable();
                      cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                      count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                      da = new mydataaccess1();
                      DataTable dtv51 = new DataTable();
                      dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                      da = new mydataaccess1();
                      DataTable dtv21 = new DataTable();
                      dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                      da = new mydataaccess1();
                      DataTable dtv2ms1 = new DataTable();
                      dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                      count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                      lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
                      */
                    //Marquee End

                    //            if (Session.Count != 0)
                    {
                        /*
                        if (Session["role"].ToString() == "2")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {

                                da = new mydataaccess1();
                                string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                                da = new mydataaccess1();
                                DataTable circle = new DataTable();
                                lblusername.Text = sp_user_d;

                                circle = da.getcirclename();

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                drpcircle.Items.Insert(1, "All");
                            }
                        }
                         */
                        if (Session["role"].ToString() == "4")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da = new mydataaccess1();
                                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                                if (lblusername.Text == "")
                                {
                                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }
                                da = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                                drpcircle.Items.Insert(1, "All");
                            }
                        }



                        if (Session["role"].ToString() == "2")
                        {
                            //da = new mydataaccess1();
                            //DataTable dt = new DataTable();
                            //dt = da.viewpivotrpt();
                            //GridView1.DataSource = dt;
                            //GridView1.DataBind();



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu;
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;

        Response.Clear();

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);


        GridView1.RenderControl(htmlWrite);

        string style = @"<style> TABLE { border: 1px solid Black; } TD { border: 1px solid Black; } </style> ";
        Response.Write(style);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Label4.Visible = true;
            Label5.Visible = true;
            TextBox1.Visible = true;
            TextBox2.Visible = true;
            btnsearch.Visible = true;
            ddlstatusall.SelectedIndex = 0;
            if (drpcircle.SelectedIndex != 0)
            {

                if (ddlcategory.Text == "Date Wise")
                {
                    MultiView1.Visible = false;
                    // MultiView1.ActiveViewIndex = 0;
                    pnn.Visible = true;
                }
                if (ddlcategory.Text == "Technician Wise")
                {
                    MultiView1.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView2.DataSource = null;
                    GridView2.DataBind();
                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 0;
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    ba.Circle = drpcircle.SelectedValue;
                    DataTable tech = new DataTable();
                    da = new mydataaccess1();
                    string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                    ba.User = sp_user_d1;
                    da = new mydataaccess1();
                    tech = da.gettechnician(ba);
                    ddltechnicianname.DataSource = tech;
                    ddltechnicianname.DataTextField = "username";
                    ddltechnicianname.DataBind();
                    ddltechnicianname.Items.Insert(0, "Select");
                    pnn.Visible = true;
                    TextBox1.Visible = true;
                    TextBox1.Visible = true;
                    //  ddlstatus.Visible = true;
                }
                if (ddlcategory.Text == "Percentage Wise")
                {
                    MultiView1.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView2.DataSource = null;
                    GridView2.DataBind();
                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 1;
                    pnn.Visible = true;
                    TextBox1.Visible = true;
                    TextBox1.Visible = true;
                }
                if (ddlcategory.Text == "Status Wise")
                {
                    MultiView1.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView2.DataSource = null;
                    GridView2.DataBind();
                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 2;
                    TextBox1.Visible = true;
                    TextBox1.Visible = true;
                    pnn.Visible = true;
                }
                if (ddlcategory.Text == "Inspection Status Wise From April")
                {
                    MultiView1.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView2.DataSource = null;
                    GridView2.DataBind();
                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 4;
                    Label4.Visible = false;
                    Label5.Visible = false;
                    TextBox1.Visible = false;
                    TextBox2.Visible = false;
                    pnn.Visible = true;
                }
                if (ddlcategory.Text == "Zone Wise")
                {
                    TextBox1.Visible = true;
                    TextBox1.Visible = true;
                    MultiView1.Visible = true;
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView2.DataSource = null;
                    GridView2.DataBind();
                    rptlable.Text = "";
                    pnn.Visible = true;

                    myvodav2 vb = new myvodav2();
                    da = new mydataaccess1();
                    DataTable zone = new DataTable();
                    vb.Circle = drpcircle.SelectedItem.Text.ToString();
                    zone = da.getzonesbycirclename(vb);

                    ddlzone.DataSource = zone;
                    ddlzone.DataTextField = "zone";
                    ddlzone.DataBind();

                    ddlzone.Items.Insert(0, "---Select---");
                    MultiView1.ActiveViewIndex = 3;
                }

            }
            else
            {

            }
        }

        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ddlpercentage_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView2.DataSource = null;
            GridView2.DataBind();
            rptlable.Text = "";
            da = new mydataaccess1();
            ba1 = new myvodav23();

            DataTable percentagemap = new DataTable();
            DataTable percentagemap1 = new DataTable();
            if (ddlsheet.SelectedIndex == 1)
            {
                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    ba1.Step = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    ba1.Step = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    ba1.Step = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    ba1.Step = 4;


                }
                ba1.Pri = "v2";
                ba1.Labelvalue = drpcircle.SelectedValue;
                percentagemap = new DataTable();
                ba1.Dump = "max";
                da = new mydataaccess1();
                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                ba1.User = sp_user_d1;
                da = new mydataaccess1();
                percentagemap = da.pivort_allsheet_by_percentage(ba1);

                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    ba1.Step = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    ba1.Step = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    ba1.Step = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    ba1.Step = 4;


                }
                ba1.Pri = "v2";
                ba1.Labelvalue = drpcircle.SelectedValue;
                percentagemap1 = new DataTable();
                ba1.Dump = "max";
                da = new mydataaccess1();
                string sp_user_d2 = da.select_user_cookie(Session["user"].ToString());
                ba1.User = sp_user_d2;
                da = new mydataaccess1();
                percentagemap1 = da.pivort_allsheet_by_percentage_criticality(ba1);

            }
            if (ddlsheet.SelectedIndex == 2)
            {
                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    ba1.Step = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    ba1.Step = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    ba1.Step = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    ba1.Step = 4;


                }
                ba1.Pri = "v5";
                ba1.Labelvalue = drpcircle.SelectedValue;
                percentagemap = new DataTable();
                ba1.Dump = "max";
                da = new mydataaccess1();
                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                ba1.User = sp_user_d1;
                da = new mydataaccess1();
                percentagemap = da.pivort_allsheet_by_percentage(ba1);

                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    ba1.Step = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    ba1.Step = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    ba1.Step = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    ba1.Step = 4;


                }
                ba1.Pri = "v5";
                ba1.Labelvalue = drpcircle.SelectedValue;
                percentagemap1 = new DataTable();
                ba1.Dump = "max";
                da = new mydataaccess1();
                string sp_user_d2 = da.select_user_cookie(Session["user"].ToString());
                ba1.User = sp_user_d2;
                da = new mydataaccess1();
                percentagemap1 = da.pivort_allsheet_by_percentage_criticality(ba1);

            }
            if (ddlsheet.SelectedIndex == 3)
            {
                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    ba1.Step = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    ba1.Step = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    ba1.Step = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    ba1.Step = 4;


                }
                ba1.Pri = "v2ms";

                ba1.Labelvalue = drpcircle.SelectedValue;
                percentagemap = new DataTable();
                ba1.Dump = "max";
                da = new mydataaccess1();
                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                ba1.User = sp_user_d1;
                da = new mydataaccess1();
                percentagemap = da.pivort_allsheet_by_percentage(ba1);

                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    ba1.Step = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    ba1.Step = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    ba1.Step = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    ba1.Step = 4;


                }
                ba1.Pri = "v2ms";
                ba1.Labelvalue = drpcircle.SelectedValue;
                percentagemap1 = new DataTable();
                ba1.Dump = "max";
                da = new mydataaccess1();
                string sp_user_d2 = da.select_user_cookie(Session["user"].ToString());
                ba1.User = sp_user_d2;
                da = new mydataaccess1();
                percentagemap1 = da.pivort_allsheet_by_percentage_criticality(ba1);

            }
            if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
            {
                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    Step1 = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    Step1 = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    Step1 = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    Step1 = 4;


                }

                int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                string Labelvalue = drpcircle.SelectedValue;
                percentagemap = new DataTable();
                string Dump = "max";
                da = new mydataaccess1();
                if (Labelvalue == "All")
                {
                    percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where user_circle_master.userid =" + userid + " and technician_rating_master_allchecksheet.check_type= " + Sheet + "", Sheet, Step1, Labelvalue, Dump);
                }
                else
                {
                    percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where user_circle_master.userid =" + userid + " and technician_rating_master_allchecksheet.check_type= " + Sheet + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                }
                da = new mydataaccess1();
                ba1 = new myvodav23();
                if (ddlpercentage.SelectedIndex == 1)
                {
                    Step1 = 1;


                }
                if (ddlpercentage.SelectedIndex == 2)
                {
                    Step1 = 2;


                }
                if (ddlpercentage.SelectedIndex == 3)
                {
                    Step1 = 3;


                }
                if (ddlpercentage.SelectedIndex == 4)
                {
                    Step1 = 4;


                }
                Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                Labelvalue = drpcircle.SelectedValue;
                percentagemap1 = new DataTable();
                Dump = "max";
                da = new mydataaccess1();
                if (Labelvalue == "All")
                {
                    percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where user_circle_master.userid =" + userid + " and technician_rating_master_allchecksheet.check_type= " + Sheet + " ", Sheet, Step1, Labelvalue, Dump);

                }
                else
                {
                    percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where user_circle_master.userid =" + userid + " and technician_rating_master_allchecksheet.check_type= " + Sheet + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                }
            }

            GridView1.DataSource = percentagemap;
            GridView1.DataBind();
            Session["circlsheet"] = percentagemap;
            GridView2.DataSource = percentagemap1;
            GridView2.DataBind();
            Session["circlsheetcriti"] = percentagemap1;


            rptlable.Text = "Report for Percentage ('" + SpacialCharRemove.XSS_Remove(ddlpercentage.SelectedValue) + "')";

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        //if (TextBox1.Text != "" && TextBox2.Text != "")
        {

            rptlable0.Visible = true;
            Panel2.Visible = true;
            if (ddlcategory.SelectedIndex == 1)
            {
                if (TextBox1.Text != "" && TextBox2.Text != "")
                {
                    DateTime from = Convert.ToDateTime(TextBox1.Text);
                    DateTime to = Convert.ToDateTime(TextBox2.Text);
                    try
                    {
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                        GridView2.DataSource = null;
                        GridView2.DataBind();
                        rptlable.Text = "";
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        ImageButton2.Visible = true;
                        ImageButton1.Visible = true;

                        //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                        {
                            da = new mydataaccess1();
                            ba1 = new myvodav23();
                            DataTable datedata = new DataTable();
                            string check_sheet = "";
                            if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                            {
                                check_sheet = "v2";
                            }
                            else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                            {
                                check_sheet = "v5";
                            }
                            else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                            {
                                check_sheet = "v2ms";
                            }
                            else
                            {
                                check_sheet = ddlsheet.SelectedItem.Text;
                                if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                                {
                                    check_sheet = "UBR V1";
                                }
                            }
                            int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                            string Circle = drpcircle.SelectedValue;
                            string Remarks = ddltechnicianname.SelectedValue;
                            string Dump = "max";
                            da = new mydataaccess1();
                            if (Circle == "All")
                            {
                                if (ddlprovidername.SelectedValue == "1")
                                {
                                    datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "'", Sheet, Circle, Dump);
                                }
                                else
                                {
                                    datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' ", Sheet, Circle, Dump);
                                }
                            }
                            else
                            {
                                if (ddlprovidername.SelectedValue == "1")
                                {
                                    datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                }
                                else
                                {
                                    datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                }
                            }
                            GridView1.DataSource = datedata;
                            GridView1.DataBind();
                            Session["circlsheet"] = datedata;

                            if (Sheet != 19 && Sheet != 44)
                            {
                                DataTable datedata1 = new DataTable();
                                da = new mydataaccess1();
                                if (Circle == "All")
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "'", Sheet, Circle, Dump);
                                    }
                                    else
                                    {
                                        datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "'", Sheet, Circle, Dump);
                                    }
                                }
                                else
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                    }
                                    else
                                    {
                                        datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                    }
                                }
                                GridView2.DataSource = datedata1;
                                GridView2.DataBind();
                                Session["circlsheetcriti"] = datedata1;
                            }
                            else
                            {
                                ImageButton2.Visible = false;
                                rptlable0.Visible = false;
                                Panel2.Visible = false;
                            }
                        }



                        rptlable.Text = "Report Between " + SpacialCharRemove.XSS_Remove(TextBox1.Text) + " to " + SpacialCharRemove.XSS_Remove(TextBox2.Text);
                    }
                    catch (Exception ex)
                    {
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }


                }

                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please enter date values');</script>");
                    //   ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Technician and status)", true);
                }
            }
            if (ddlcategory.SelectedIndex == 2)
            {

                /* if (ddltechnicianname.SelectedIndex != 0 && ddlstatus.SelectedIndex == 0)
                 {
                     if (TextBox1.Text != "" && TextBox2.Text != "")
                     {

                         DateTime from = Convert.ToDateTime(TextBox1.Text);
                         DateTime to = Convert.ToDateTime(TextBox2.Text);
                         try
                         {
                             GridView1.DataSource = null;
                             GridView1.DataBind();
                             GridView2.DataSource = null;
                             GridView2.DataBind();
                             rptlable.Text = "";
                             da = new mydataaccess1();
                             ba1 = new myvodav23();
                             ImageButton2.Visible = true;
                             ImageButton1.Visible = true;

                             //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                             {
                                 da = new mydataaccess1();
                                 ba1 = new myvodav23();
                                 DataTable datedata = new DataTable();
                                 string check_sheet = "";
                                 if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                                 {
                                     check_sheet = "v2";
                                 }
                                 else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                                 {
                                     check_sheet = "v5";
                                 }
                                 else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                                 {
                                     check_sheet = "v2ms";
                                 }
                                 else
                                 {
                                     check_sheet = ddlsheet.SelectedItem.Text;
                                 }
                                 int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                                 string Circle = drpcircle.SelectedValue;
                                 string Remarks = ddltechnicianname.SelectedValue;
                                 string Dump = "max";
                                 da = new mydataaccess1();
                                 if (Circle == "All")
                                 {
                                     if (ddlprovidername.SelectedValue == "1")
                                     {
                                         datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'", Sheet, Circle, Dump);
                                     }
                                     else
                                     {
                                         datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'", Sheet, Circle, Dump);
                                     }
                                 }
                                 else
                                 {
                                     if (ddlprovidername.SelectedValue == "1")
                                     {
                                         datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                     }
                                     else
                                     {
                                         datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                     }
                                 }
                                 GridView1.DataSource = datedata;
                                 GridView1.DataBind();
                                 Session["circlsheet"] = datedata;


                                 DataTable datedata1 = new DataTable();
                                 da = new mydataaccess1();
                                 if (Circle == "All")
                                 {
                                     if (ddlprovidername.SelectedValue == "1")
                                     {
                                         datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'", Sheet, Circle, Dump);
                                     }
                                     else
                                     {
                                         datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'", Sheet, Circle, Dump);
                                     }
                                 }
                                 else
                                 {
                                     if (ddlprovidername.SelectedValue == "1")
                                     {
                                         datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                     }
                                     else
                                     {
                                         datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                     }
                                 }
                                 GridView2.DataSource = datedata1;
                                 GridView2.DataBind();
                                 Session["circlsheetcriti"] = datedata1;
                             }
                             //for (int i = 0; i < datedata.Rows.Count; i++)
                             //{
                             //    if (datedata.Rows[i][104].ToString() == "2")
                             //    {

                             //        datedata.Rows[i][104] = "Completed";
                             //    }
                             //    else if (datedata.Rows[i][104].ToString() == "1")
                             //    {
                             //        datedata.Rows[i][104] = "Not Completed";
                             //    }
                             //    else
                             //    {
                             //        datedata.Rows[i][104] = "Modifying";
                             //    }
                             //}


                             rptlable.Text = "Report For " + ddltechnicianname.Text;
                         }
                         catch (Exception ex)
                         {
                             Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                         }
                     }
                     else
                     {
                         Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Dates');</script>");
                         //   ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Technician and status)", true);
                     }

                 }*/
                if (ddltechnicianname.SelectedIndex != 0)
                {
                    if (TextBox1.Text != "" && TextBox2.Text != "")
                    {
                        try
                        {
                            DateTime from = Convert.ToDateTime(TextBox1.Text);
                            DateTime to = Convert.ToDateTime(TextBox2.Text);
                            ImageButton2.Visible = true;
                            ImageButton1.Visible = true;
                            GridView1.DataSource = null;
                            GridView1.DataBind();
                            GridView2.DataSource = null;
                            GridView2.DataBind();
                            rptlable.Text = "";
                            da = new mydataaccess1();
                            ba1 = new myvodav23();
                            ba1.Labelvalue = drpcircle.SelectedValue;
                            ba1.Remarks = ddltechnicianname.SelectedValue;
                            //if (ddlstatus.SelectedValue == "Completed")
                            {
                                //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                                {
                                    string Siteid = "2";
                                    string Labelvalue = drpcircle.SelectedValue;
                                    string Remarks = ddltechnicianname.SelectedValue;
                                    string check_sheet = "";
                                    if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                                    {
                                        check_sheet = "v2";
                                    }
                                    else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                                    {
                                        check_sheet = "v5";
                                    }
                                    else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                                    {
                                        check_sheet = "v2ms";
                                    }
                                    else
                                    {
                                        check_sheet = ddlsheet.SelectedItem.Text;
                                        if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                                        {
                                            check_sheet = "UBR V1";
                                        }
                                    }
                                    int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                                    DataTable datedata = new DataTable();
                                    string Dump = "max";
                                    da = new mydataaccess1();
                                    if (Labelvalue == "All")
                                    {
                                        if (ddlprovidername.SelectedValue == "1")
                                        {
                                            datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status=" + Siteid + " and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'", Sheet, Labelvalue, Dump);
                                        }
                                        else
                                        {
                                            datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status=" + Siteid + " and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'", Sheet, Labelvalue, Dump);
                                        }
                                    }
                                    else
                                    {
                                        if (ddlprovidername.SelectedValue == "1")
                                        {
                                            datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and user_circle_master.userid =" + userid + " and site_master.status=" + Siteid + " and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                        }
                                        else
                                        {
                                            datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status=" + Siteid + " and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                        }
                                    }
                                    //int i = datedata.Columns.Count;
                                    GridView1.DataSource = datedata;
                                    GridView1.DataBind();
                                    Session["circlsheet"] = datedata;

                                    DataTable datedata1 = new DataTable();
                                    da = new mydataaccess1();
                                    if (Sheet != 19 && Sheet != 44)
                                    {
                                        if (Labelvalue == "All")
                                        {
                                            if (ddlprovidername.SelectedValue == "1")
                                            {
                                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status='" + Siteid + "' and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "' ", Sheet, Labelvalue, Dump);
                                            }
                                            else
                                            {
                                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status='" + Siteid + "' and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "' ", Sheet, Labelvalue, Dump);
                                            }

                                        }
                                        else
                                        {
                                            if (ddlprovidername.SelectedValue == "1")
                                            {
                                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status='" + Siteid + "' and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "' and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                            }
                                            else
                                            {
                                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and site_master.status='" + Siteid + "' and site_master.check_sheet='" + check_sheet + "' and tech_table.userid='" + Remarks + "' and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                            }
                                        }
                                        //int i = datedata.Columns.Count;
                                        GridView2.DataSource = datedata1;
                                        GridView2.DataBind();
                                        Session["circlsheetcriti"] = datedata1;
                                    }
                                    else
                                    {
                                        ImageButton2.Visible = false;
                                        rptlable0.Visible = false;
                                        Panel2.Visible = false;
                                    }
                                    rptlable.Text = "Report For " + SpacialCharRemove.XSS_Remove(ddltechnicianname.Text) + " With Complete Status";
                                }

                            }



                        }
                        catch (Exception ex)
                        {
                            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                        }
                    }

                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Dates');</script>");
                        //   ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Technician and status)", true);
                    }

                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Technician');</script>");

                }
                //
                /*   else if (ddlstatus.SelectedIndex == 2 && ddltechnicianname.SelectedIndex != 0)
                   {
                       // if (ddlstatus.SelectedValue == "Not Completed")
                       {
                           statusflag = 1;
                           da = new mydataaccess1();
                           ba1 = new myvodav23();
                           ba1.Siteid = "1";
                           ba1.Labelvalue = drpcircle.SelectedValue;
                           ba1.Remarks = ddltechnicianname.SelectedValue;
                           DataTable datedata = new DataTable();
                           da = new mydataaccess1();
                           string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                           ba1.User = sp_user_d1;
                           ba1.Provider = ddlprovidername.SelectedItem.Text;
                           da = new mydataaccess1();
                           datedata = da.notcompleted(ba1);
                           if (datedata.Rows.Count > 0)
                           {
                               for (int i = 0; i < datedata.Rows.Count; i++)
                               {
                                   if (datedata.Rows[i][6].ToString() == "2")
                                   {

                                       datedata.Rows[i][6] = "Completed";
                                   }
                                   else if (datedata.Rows[i][6].ToString() != "2")
                                   {
                                       datedata.Rows[i][6] = "Not Completed";
                                   }
                               }

                               GridView1.DataSource = datedata;
                               GridView1.DataBind();
                               Session["circlsheet"] = datedata;
                               rptlable.Text = "Not Completed Site List";
                               GridView2.DataSource = null;
                               GridView2.DataBind();
                           }
                       }

                   }

                   else
                   {
                       Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Technician and status');</script>");

                   }*/
            }

            if (ddlcategory.SelectedIndex == 3)
            {

                try
                {
                    if (ddlpercentage.SelectedIndex != 0)
                    {
                        DateTime from = Convert.ToDateTime(TextBox1.Text);
                        DateTime to = Convert.ToDateTime(TextBox2.Text);
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                        GridView2.DataSource = null;
                        GridView2.DataBind();
                        rptlable.Text = "";

                        DataTable percentagemap = new DataTable();
                        DataTable percentagemap1 = new DataTable();
                        string condition = "";
                        //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                        {
                            da = new mydataaccess1();
                            ba1 = new myvodav23();
                            if (ddlpercentage.SelectedIndex == 1)
                            {
                                condition = " and tech_table.percentage<70";



                            }
                            if (ddlpercentage.SelectedIndex == 2)
                            {
                                condition = " and (tech_table.percentage<80 and tech_table.percentage>=70) ";


                            }
                            if (ddlpercentage.SelectedIndex == 3)
                            {
                                condition = " and (tech_table.percentage<90 and tech_table.percentage>=80) ";


                            }
                            if (ddlpercentage.SelectedIndex == 4)
                            {
                                condition = " and tech_table.percentage>=90";


                            }
                            string check_sheet = "";
                            if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                            {
                                check_sheet = "v2";
                            }
                            else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                            {
                                check_sheet = "v5";
                            }
                            else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                            {
                                check_sheet = "v2ms";
                            }
                            else
                            {
                                check_sheet = ddlsheet.SelectedItem.Text;
                                if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                                {
                                    check_sheet = "UBR V1";
                                }
                            }

                            int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                            string Labelvalue = drpcircle.SelectedValue;
                            percentagemap = new DataTable();
                            string Dump = "max";
                            da = new mydataaccess1();
                            if (Labelvalue == "All")
                            {
                                if (ddlprovidername.SelectedValue == "1")
                                {
                                    percentagemap = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and  site_master.check_sheet='" + check_sheet + "'  " + condition + "", Sheet, Labelvalue, Dump);
                                }
                                else
                                {
                                    percentagemap = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + "  and site_master.check_sheet='" + check_sheet + "'  " + condition + "", Sheet, Labelvalue, Dump);
                                }
                            }
                            else
                            {
                                if (ddlprovidername.SelectedValue == "1")
                                {
                                    percentagemap = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and  site_master.check_sheet='" + check_sheet + "'  " + condition + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                }
                                else
                                {
                                    percentagemap = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + "  and site_master.check_sheet='" + check_sheet + "'  " + condition + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                }
                            }
                            da = new mydataaccess1();
                            percentagemap1 = new DataTable();
                            if (Sheet != 19 && Sheet != 44)
                            {
                                if (Labelvalue == "All")
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        percentagemap1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and  site_master.check_sheet='" + check_sheet + "'  " + condition + "", Sheet, Labelvalue, Dump);
                                    }
                                    else
                                    {
                                        percentagemap1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + "  and site_master.check_sheet='" + check_sheet + "'  " + condition + "", Sheet, Labelvalue, Dump);
                                    }
                                }
                                else
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        percentagemap1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " and  site_master.check_sheet='" + check_sheet + "'  " + condition + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                    }
                                    else
                                    {
                                        percentagemap1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + "  and site_master.check_sheet='" + check_sheet + "'  " + condition + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                    }
                                }
                                GridView2.DataSource = percentagemap1;
                                GridView2.DataBind();
                                Session["circlsheetcriti"] = percentagemap1;
                            }
                            else
                            {
                                ImageButton2.Visible = false;
                                rptlable0.Visible = false;
                                Panel2.Visible = false;
                            }
                            /*   string chk_type = "";
                               if (Sheet == 10 || Sheet == 11 || Sheet == 12)
                               {
                                   chk_type = null;
                               }
                               else
                               {
                                   chk_type = "and technician_rating_master_allchecksheet.check_type= " + Sheet + "";
                               }


                               if (Labelvalue == "All" && ddlpercentage.SelectedIndex == 1)
                               {
                                   if (ddlprovidername.SelectedValue == "1")
                                   {
                                       percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and tech_table.percentage<70 and user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                                   }
                                   else
                                   {
                                       percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  tech_table.percentage<70 and user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                                   }
                               }
                           

                               else
                               {
                                   if (ddlprovidername.SelectedValue == "1")
                                   {
                                       percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                                   }
                                   else
                                   {
                                       percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                                   }
                               }*/

                            /*  da = new mydataaccess1();
                              if (Labelvalue == "All")
                              {
                                  if (ddlprovidername.SelectedValue == "1")
                                  {
                                      percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                                  }
                                  else
                                  {
                                      percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                                  }
                              }
                              else
                              {
                                  if (ddlprovidername.SelectedValue == "1")
                                  {
                                      percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                                  }
                                  else
                                  {
                                      percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                                  }
                              }*/
                        }

                        GridView1.DataSource = percentagemap;
                        GridView1.DataBind();
                        Session["circlsheet"] = percentagemap;


                        rptlable.Text = "Report for Percentage ('" + SpacialCharRemove.XSS_Remove(ddlpercentage.SelectedValue) + "')";
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Percentage Value');</script>");
                        //  ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Percentage Value)", true);
                    }

                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }



            }
            if (ddlcategory.SelectedIndex == 4)
            {

                try
                {
                    if (ddlstatusall.SelectedIndex == 1 && TextBox1.Text != "" && TextBox2.Text != "")
                    {
                        DateTime from = Convert.ToDateTime(TextBox1.Text);
                        DateTime to = Convert.ToDateTime(TextBox2.Text);
                        ImageButton2.Visible = true;
                        ImageButton1.Visible = true;
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                        GridView2.DataSource = null;
                        GridView2.DataBind();
                        rptlable.Text = "";
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        ba1.Labelvalue = drpcircle.SelectedValue;
                        if (ddlstatusall.SelectedValue == "Completed")
                        {

                            //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                            {
                                string Remarks = "2";
                                string check_sheet = "";
                                if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                                {
                                    check_sheet = "v2";
                                }
                                else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                                {
                                    check_sheet = "v5";
                                }
                                else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                                {
                                    check_sheet = "v2ms";
                                }
                                else
                                {
                                    check_sheet = ddlsheet.SelectedItem.Text;
                                    if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                                    {
                                        check_sheet = "UBR V1";
                                    }
                                }
                                string Labelvalue = drpcircle.SelectedValue;
                                // ba1.Remarks = ddltechnicianname.SelectedValue;
                                int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                                DataTable datedata = new DataTable();
                                string Dump = "max";
                                da = new mydataaccess1();
                                if (Labelvalue == "All")
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                                    }
                                    else
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and  surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                                    }
                                }
                                else
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                    }
                                    else
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                    }
                                }
                                //int i = datedata.Columns.Count;
                                GridView1.DataSource = datedata;
                                GridView1.DataBind();
                                Session["circlsheet"] = datedata;

                                DataTable datedata1 = new DataTable();
                                da = new mydataaccess1();
                                if (Sheet != 19 && Sheet != 44)
                                {
                                    if (Labelvalue == "All")
                                    {
                                        if (ddlprovidername.SelectedValue == "1")
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and  surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                                        }
                                        else
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                                        }
                                    }
                                    else
                                    {
                                        if (ddlprovidername.SelectedValue == "1")
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                        }
                                        else
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                                        }
                                    }
                                    //int i = datedata.Columns.Count;
                                    GridView2.DataSource = datedata1;
                                    GridView2.DataBind();
                                    Session["circlsheetcriti"] = datedata1;
                                }
                                else
                                {
                                    ImageButton2.Visible = false;
                                    rptlable0.Visible = false;
                                    Panel2.Visible = false;
                                }

                                rptlable.Text = "Report With Complete Status";
                            }

                        }
                    }
                    else if (ddlstatusall.SelectedValue == "Not Completed")
                    {
                        statusflag = 1;
                        da = new mydataaccess1();
                        ba1 = new myvodav23();
                        ba1.Labelvalue = drpcircle.SelectedValue;
                        ba1.Siteid = "1";
                        DataTable datedata1 = new DataTable();
                        da = new mydataaccess1();
                        string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                        ba1.User = sp_user_d1;

                        if (ddlsheet.SelectedIndex == 1)
                        {
                            ba1.Pri = "v2";
                        }
                        if (ddlsheet.SelectedIndex == 2)
                        {
                            ba1.Pri = "v5";
                        }
                        if (ddlsheet.SelectedIndex == 3)
                        {
                            ba1.Pri = "v2ms";
                        }
                        if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                        {
                            ba1.Pri = ddlsheet.SelectedItem.Text;
                            if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                            {
                                ba1.Pri = "UBR V1";
                            }
                        }
                        da = new mydataaccess1();
                        ba1.Provider = ddlprovidername.SelectedItem.Text;
                        datedata1 = da.notcompleted1(ba1);
                        if (datedata1.Rows.Count > 0)
                        {
                            /*for (int i = 0; i < datedata1.Rows.Count; i++)
                            {
                                if (datedata1.Rows[i][6].ToString() == "2")
                                {
                                    datedata1.Rows[i][6] = "Completed";
                                }
                                if (datedata1.Rows[i][6].ToString() == "1")
                                {
                                    datedata1.Rows[i][6] = "Never inspected";
                                }
                                if (datedata1.Rows[i][6].ToString() == "3")
                                {
                                    datedata1.Rows[i][6] = "Modify by circle admin";
                                }
                                if (datedata1.Rows[i][6].ToString() == "4")
                                {
                                    datedata1.Rows[i][6] = "Reset by circle admin";
                                }
                                if (datedata1.Rows[i][6].ToString() == "6")
                                {
                                    datedata1.Rows[i][6] = "Request to Recheck";
                                }

                            }*/
                            GridView1.DataSource = datedata1;
                            GridView1.DataBind();
                            Session["circlsheet"] = datedata1;

                            rptlable.Text = "Not Completed Site List";

                            GridView2.DataSource = null;
                            GridView2.DataBind();
                        }
                        rptlable.Text = "Not Completed Site List";
                    }

                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Status');</script>");
                        // ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Status )", true);
                    }


                }

                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }



            }
            if (ddlcategory.SelectedIndex == 5)
            {
                if (TextBox1.Text != "" && TextBox2.Text != "")
                {
                    if (ddlzone.SelectedIndex != 0)
                    {
                        DateTime from = Convert.ToDateTime(TextBox1.Text);
                        DateTime to = Convert.ToDateTime(TextBox2.Text);
                        try
                        {
                            GridView1.DataSource = null;
                            GridView1.DataBind();
                            GridView2.DataSource = null;
                            GridView2.DataBind();
                            rptlable.Text = "";
                            da = new mydataaccess1();
                            ba1 = new myvodav23();
                            ImageButton2.Visible = true;
                            ImageButton1.Visible = true;

                            //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                            {
                                da = new mydataaccess1();
                                ba1 = new myvodav23();
                                DataTable datedata = new DataTable();
                                string check_sheet = "";
                                if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                                {
                                    check_sheet = "v2";
                                }
                                else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                                {
                                    check_sheet = "v5";
                                }
                                else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                                {
                                    check_sheet = "v2ms";
                                }
                                else
                                {
                                    check_sheet = ddlsheet.SelectedItem.Text;
                                    if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                                    {
                                        check_sheet = "UBR V1";
                                    }
                                }
                                int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                                string Circle = drpcircle.SelectedValue;
                                string Remarks = ddltechnicianname.SelectedValue;
                                string Dump = "max";
                                da = new mydataaccess1();
                                if (Circle == "All")
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "'", Sheet, Circle, Dump);
                                    }
                                    else
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' ", Sheet, Circle, Dump);
                                    }
                                }
                                else
                                {
                                    if (ddlprovidername.SelectedValue == "1")
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                    }
                                    else
                                    {
                                        datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                    }
                                }
                                GridView1.DataSource = datedata;
                                GridView1.DataBind();
                                Session["circlsheet"] = datedata;


                                DataTable datedata1 = new DataTable();
                                da = new mydataaccess1();
                                if (Sheet != 19 && Sheet != 44)
                                {
                                    if (Circle == "All")
                                    {
                                        if (ddlprovidername.SelectedValue == "1")
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "'", Sheet, Circle, Dump);
                                        }
                                        else
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "'", Sheet, Circle, Dump);
                                        }
                                    }
                                    else
                                    {
                                        if (ddlprovidername.SelectedValue == "1")
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                        }
                                        else
                                        {
                                            datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and site_master.zone='" + ddlzone.SelectedValue + "' and user_circle_master.userid = " + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.check_sheet='" + check_sheet + "' and user_circle_master.circle='" + Circle + "'", Sheet, Circle, Dump);
                                        }
                                    }
                                    GridView2.DataSource = datedata1;
                                    GridView2.DataBind();
                                    Session["circlsheetcriti"] = datedata1;
                                }
                                else
                                {
                                    ImageButton2.Visible = false;
                                    rptlable0.Visible = false;
                                    Panel2.Visible = false;
                                }
                            }



                            rptlable.Text = "Report Between " + SpacialCharRemove.XSS_Remove(TextBox1.Text) + " to " + SpacialCharRemove.XSS_Remove(TextBox2.Text);
                        }
                        catch (Exception ex)
                        {
                            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select Zone!!!');</script>");
                        //   ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Technician and status)", true);
                    }

                }
            }
            if (ddlcategory.SelectedIndex == 6)
            {
                ImageButton2.Visible = false;
                ImageButton1.Visible = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView2.DataSource = null;
                GridView2.DataBind();
                rptlable.Text = "";
                da = new mydataaccess1();
                ba1 = new myvodav23();
                ba1.Labelvalue = drpcircle.SelectedValue;
                if (DropDownList1.SelectedValue == "Completed")
                {


                    string Remarks = "2";
                    string check_sheet = "";

                    check_sheet = ddlsheet.SelectedItem.Text;

                    string Labelvalue = drpcircle.SelectedValue;
                    // ba1.Remarks = ddltechnicianname.SelectedValue;
                    // string check_sheet = "";
                    if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                    {
                        check_sheet = "v2";
                    }
                    else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                    {
                        check_sheet = "v5";
                    }
                    else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                    {
                        check_sheet = "v2ms";
                    }
                    else
                    {
                        check_sheet = ddlsheet.SelectedItem.Text;
                        if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                        {
                            check_sheet = "UBR V1";
                        }
                    }
                    da = new mydataaccess1();
                    int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                    DataTable datedata = new DataTable();
                    datedata = da.select_inspection_report(Labelvalue, check_sheet);

                    //int i = datedata.Columns.Count;
                    GridView1.DataSource = datedata;
                    GridView1.DataBind();
                    Session["circlsheet"] = datedata;
                }
                if (DropDownList1.SelectedValue == "Not Completed")
                {


                    string Remarks = "2";
                    string check_sheet = "";

                    check_sheet = ddlsheet.SelectedItem.Text;

                    string Labelvalue = drpcircle.SelectedValue;
                    // ba1.Remarks = ddltechnicianname.SelectedValue;
                    //  string check_sheet = "";
                    if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                    {
                        check_sheet = "v2";
                    }
                    else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                    {
                        check_sheet = "v5";
                    }
                    else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                    {
                        check_sheet = "v2ms";
                    }
                    else
                    {
                        check_sheet = ddlsheet.SelectedItem.Text;
                        if (ddlsheet.SelectedItem.Text == "UBR V1.1")
                        {
                            check_sheet = "UBR V1";
                        }
                    }
                    da = new mydataaccess1();
                    int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                    DataTable datedata = new DataTable();
                    datedata = da.select_inspection_report_not(Labelvalue, check_sheet);

                    //int i = datedata.Columns.Count;
                    GridView1.DataSource = datedata;
                    GridView1.DataBind();
                    Session["circlsheet"] = datedata;
                }

            }

        }

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (ddlcategory.SelectedIndex == 3)
            {
                if (GridView1.Rows.Count > 0)
                {
                    if (ddlsheet.SelectedIndex == 3)
                    {
                        if (GridView1.DataSource != null)
                        {
                            for (int i = 3; i <= 219; i++)
                            {

                                if (e.Row.RowType == DataControlRowType.DataRow)
                                { }
                                if (e.Row.Cells[i].Text == "-1")
                                {

                                    e.Row.Cells[i].Text = "NA";
                                }

                            }
                        }
                    }
                }


                if (GridView1.Rows.Count > 0)
                {
                    //    int c= GridView1.Rows.Count;

                    if (ddlsheet.SelectedIndex == 2)
                    {
                        //  for (int i = 2; i <= 102; i++)
                        for (int i = 3; i <= 100; i++)
                        {

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1")
                            {

                                e.Row.Cells[i].Text = "NA";
                            }

                        }
                    }
                    if (ddlsheet.SelectedIndex == 1)
                    {
                        //  for (int i = 2; i <= 102; i++)
                        for (int i = 3; i <= 72; i++)
                        {

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1")
                            {

                                e.Row.Cells[i].Text = "NA";
                            }

                        }
                    }

                }
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);


        GridView2.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView2.Columns[0].Visible = true;
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlsheet.SelectedIndex != 0)
            {
                if (ddlsheet.SelectedIndex == 1)
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";

                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;


                    }

                    DataTable circlemap_v2 = new DataTable();
                    circlemap_v2 = da.getcirclemapgrid_v2(ba);
                    GridView1.DataSource = circlemap_v2;
                    GridView1.DataBind();
                    rptlable.Text = "Report For V2";


                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";
                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }
                    DataTable circlemap1_v2 = new DataTable();
                    circlemap1_v2 = da.getcirclemapgrid_v2_criticality(ba);
                    GridView2.DataSource = circlemap1_v2;
                    GridView2.DataBind();
                    rptlable0.Text = "Report For V2 Criticality";

                }
                else
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";
                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }
                    DataTable circlemap = new DataTable();
                    circlemap = da.getcirclemapgrid(ba);
                    GridView1.DataSource = circlemap;
                    GridView1.DataBind();
                    rptlable.Text = "Report For V5";

                    da = new mydataaccess1();
                    ba = new myvodav2();
                    if (drpcircle.Text == "North East")
                    {
                        ba.Circle = "ne";
                    }
                    if (drpcircle.Text != "North East")
                    {
                        ba.Circle = drpcircle.SelectedValue;
                    }
                    DataTable circlemap1 = new DataTable();
                    circlemap1 = da.getcirclemapgridwithcriticality(ba);
                    GridView2.DataSource = circlemap1;
                    GridView2.DataBind();
                    rptlable0.Text = "Report For V5 Criticality";


                }
            }

        }
        catch (Exception ex)
        {
            // Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlsheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlsheet.SelectedValue) == 44)
        {
            ddlcategory.Items[3].Enabled = false;
            ddlstatusall.Items[2].Enabled = false;
            // criticality.Visible = false;
        }
        else
        {
            ddlcategory.Items[3].Enabled = true;
            ddlstatusall.Items[2].Enabled = true;
            //  criticality.Visible = true;
        }
        //try
        //{
        //    ddlcategory.SelectedIndex = 0;
        //    da = new mydataaccess1();
        //    ba1 = new myvodav23();
        //    if (drpcircle.Text == "North East")
        //    {
        //        ba1.Labelvalue = "ne";
        //    }
        //    if (drpcircle.Text != "North East")
        //    {
        //        ba1.Labelvalue = drpcircle.SelectedValue;
        //    }

        //    if (drpcircle.SelectedIndex != 0 && drpcircle.SelectedIndex == 1)
        //    {



        //        if (ddlsheet.SelectedIndex != 0)
        //        {
        //            if (ddlsheet.SelectedIndex == 1)
        //            {
        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap = new DataTable();
        //                ba1.Pri = "v2";
        //                ba1.Dump = "max";
        //                ba1.User = lblusername.Text;
        //                circlemap = da.pivort_allsheet_allcircle(ba1);
        //                GridView1.DataSource = circlemap;
        //                GridView1.DataBind();
        //                Session["circlsheet"] = circlemap;

        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap1 = new DataTable();
        //                ba1.Pri = "v2";
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap1 = da.pivort_allsheet_allcircle_criticality(ba1);
        //                GridView2.DataSource = circlemap1;
        //                GridView2.DataBind();
        //                Session["circlsheetcriti"] = circlemap1;
        //            }
        //            if (ddlsheet.SelectedIndex == 2)
        //            {
        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap = new DataTable();
        //                ba1.Pri = "v5";
        //                ba1.Dump = "max";
        //                ba1.User = lblusername.Text;
        //                circlemap = da.pivort_allsheet_allcircle(ba1);
        //                GridView1.DataSource = circlemap;
        //                GridView1.DataBind();
        //                Session["circlsheet"] = circlemap;

        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap1 = new DataTable();
        //                ba1.Pri = "v5";
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap1 = da.pivort_allsheet_allcircle_criticality(ba1);
        //                GridView2.DataSource = circlemap1;
        //                GridView2.DataBind();
        //                Session["circlsheetcriti"] = circlemap1;
        //            }
        //            if (ddlsheet.SelectedIndex == 3)
        //            {
        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap = new DataTable();
        //                ba1.Pri = "v2ms";
        //                ba1.Dump = "max";
        //                ba1.User = lblusername.Text;
        //                circlemap = da.pivort_allsheet_allcircle(ba1);
        //                GridView1.DataSource = circlemap;
        //                GridView1.DataBind();
        //                Session["circlsheet"] = circlemap;


        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap1 = new DataTable();
        //                ba1.Pri = "v2ms";
        //                ba1.Dump = "max";
        //                ba1.User = lblusername.Text;
        //                circlemap1 = da.pivort_allsheet_allcircle(ba1);
        //                GridView2.DataSource = circlemap1;
        //                GridView2.DataBind();
        //                Session["circlsheetcriti"] = circlemap1;

        //            }


        //        }

        //    }
        //    else
        //    {
        //        if (ddlsheet.SelectedIndex != 0)
        //        {
        //            if (ddlsheet.SelectedIndex == 1)
        //            {
        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap = new DataTable();
        //                ba1.Pri = "v2";
        //                ba1.Labelvalue = drpcircle.SelectedValue;
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap = da.pivort_allsheet_by_circle(ba1);
        //                GridView1.DataSource = circlemap;
        //                GridView1.DataBind();
        //                Session["circlsheet"] = circlemap;


        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap1 = new DataTable();
        //                ba1.Pri = "v2";
        //                ba1.Labelvalue = drpcircle.SelectedValue;
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap1 = da.pivort_allsheet_by_circle_criticality(ba1);
        //                GridView2.DataSource = circlemap1;
        //                GridView2.DataBind();
        //                Session["circlsheetcriti"] = circlemap1;
        //            }
        //            if (ddlsheet.SelectedIndex == 2)
        //            {
        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap = new DataTable();
        //                ba1.Pri = "v5";
        //                ba1.Labelvalue = drpcircle.SelectedValue;
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap = da.pivort_allsheet_by_circle(ba1);
        //                GridView1.DataSource = circlemap;
        //                GridView1.DataBind();
        //                Session["circlsheet"] = circlemap;

        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap1 = new DataTable();
        //                ba1.Pri = "v5";
        //                ba1.Labelvalue = drpcircle.SelectedValue;
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap1 = da.pivort_allsheet_by_circle_criticality(ba1);
        //                GridView2.DataSource = circlemap1;
        //                GridView2.DataBind();
        //                Session["circlsheetcriti"] = circlemap1;
        //            }
        //            if (ddlsheet.SelectedIndex == 3)
        //            {
        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap = new DataTable();
        //                ba1.Pri = "v2ms";
        //                ba1.Labelvalue = drpcircle.SelectedValue;
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                string sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap = da.pivort_allsheet_by_circle(ba1);
        //                GridView1.DataSource = circlemap;
        //                GridView1.DataBind();
        //                Session["circlsheet"] = circlemap;

        //                da = new mydataaccess1();
        //                ba1 = new myvodav23();
        //                DataTable circlemap1 = new DataTable();
        //                ba1.Pri = "v2ms";
        //                ba1.Labelvalue = drpcircle.SelectedValue;
        //                ba1.Dump = "max";
        //                da = new mydataaccess1();
        //                sp_user_d1 = da.select_user_cookie(Session["user"].ToString()); ba1.User = sp_user_d1;
        //                da = new mydataaccess1();
        //                circlemap1 = da.pivort_allsheet_by_circle_criticality(ba1);
        //                GridView2.DataSource = circlemap1;
        //                GridView2.DataBind();
        //                Session["circlsheetcriti"] = circlemap1;

        //            }


        //        }
        //    }
        //    //DataTable circlemap = new DataTable();
        //    //circlemap = da.getcirclemapgrid(ba);
        //    //GridView1.DataSource = circlemap;
        //    //GridView1.DataBind();


        //    //da = new mydataaccess1();
        //    //ba = new myvodav2();
        //    //if (drpcircle.Text == "North East")
        //    //{
        //    //    ba.Circle = "ne";
        //    //}
        //    //if (drpcircle.Text != "North East")
        //    //{
        //    //    ba.Circle = drpcircle.SelectedValue;
        //    //}
        //    //DataTable circlemap1 = new DataTable();
        //    //circlemap1 = da.getcirclemapgridwithcriticality(ba);
        //    //GridView2.DataSource = circlemap1;
        //    //GridView2.DataBind();
        //    rptlable.Text = "Report for Sheet '" + ddlsheet.SelectedValue + "'";

        //}
        //catch (Exception ex)
        //{
        //    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        //}




    }
    protected void drpcircle_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            ImageButton1.Visible = true;
            ImageButton2.Visible = true;
            ;
            ddlsheet.SelectedIndex = 0;
            ddlcategory.SelectedIndex = 0;
            ddlpercentage.SelectedIndex = 0;
            ddlstatusall.SelectedIndex = 0;
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView2.DataSource = null;
            GridView2.DataBind();

            da = new mydataaccess1();
            ba = new myvodav2();



        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (ddlcategory.SelectedIndex == 3)
            {
                if (GridView2.Rows.Count > 0)
                {
                    if (ddlsheet.SelectedIndex == 3)
                    {
                        if (GridView2.DataSource != null)
                        {
                            for (int i = 3; i <= 219; i++)
                            {

                                if (e.Row.RowType == DataControlRowType.DataRow)
                                { }
                                if (e.Row.Cells[i].Text == "-1")
                                {

                                    e.Row.Cells[i].Text = "NA";
                                }

                            }
                        }
                    }
                }


                if (GridView2.Rows.Count > 0)
                {
                    //    int c= GridView2.Rows.Count;

                    if (ddlsheet.SelectedIndex == 2)
                    {
                        //  for (int i = 2; i <= 102; i++)
                        for (int i = 3; i <= 100; i++)
                        {

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1")
                            {

                                e.Row.Cells[i].Text = "NA";
                            }

                        }
                    }
                    if (ddlsheet.SelectedIndex == 1)
                    {
                        //  for (int i = 2; i <= 102; i++)
                        for (int i = 3; i <= 72; i++)
                        {

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1")
                            {

                                e.Row.Cells[i].Text = "NA";
                            }

                        }
                    }

                }
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex == 0)
                e.Row.Style.Add("height", "40px");
        }
        try
        {
            if (statusflag == 0 && ddlcategory.SelectedIndex != 6)
            {
                if (e.Row.Cells.Count > 1)
                {
                    if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                    {
                        for (int i = 7; i <= 85; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                    {
                        for (int i = 3; i <= 109; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                    {
                        for (int i = 3; i <= 225; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "1")
                    {
                        for (int i = 3; i <= 51; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "2")
                    {
                        for (int i = 6; i <= 75; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "3")//painding
                    {
                        for (int i = 6; i <= 219; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "4")
                    {
                        for (int i = 7; i <= 91; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "5")
                    {
                        for (int i = 6; i <= 38; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "6")
                    {
                        for (int i = 6; i <= 51; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "7")
                    {
                        for (int i = 7; i <= 107; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "8")
                    {
                        for (int i = 3; i <= 57; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "9")
                    {
                        for (int i = 6; i <= 29; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "13")
                    {
                        for (int i = 3; i <= 35; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "14")
                    {
                        for (int i = 3; i <= 32; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "15")
                    {
                        for (int i = 3; i <= 28; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "16")
                    {
                        for (int i = 3; i <= 43; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "17")
                    {
                        for (int i = 3; i <= 68; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "18")
                    {
                        for (int i = 3; i <= 66; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                    if (ddlsheet.Text == "19")
                    {
                        for (int i = 3; i <= 103; i++)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1" || e.Row.Cells[i].Text == "" || e.Row.Cells[i].Text == "&nbsp;")
                            {
                                e.Row.Cells[i].Text = "NA";
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void GridView2_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex == 0)
                e.Row.Style.Add("height", "40px");
        }
        try
        {

            if (ddlcategory.SelectedIndex == 3)
            {
                if (GridView2.Rows.Count > 0)
                {
                    if (ddlsheet.SelectedIndex == 3)
                    {
                        if (GridView2.DataSource != null)
                        {
                            for (int i = 3; i <= 219; i++)
                            {

                                if (e.Row.RowType == DataControlRowType.DataRow)
                                { }
                                if (e.Row.Cells[i].Text == "-1")
                                {

                                    e.Row.Cells[i].Text = "NA";
                                }

                            }
                        }
                    }
                }


                if (GridView2.Rows.Count > 0)
                {
                    //    int c= GridView2.Rows.Count;

                    if (ddlsheet.SelectedIndex == 2)
                    {
                        //  for (int i = 2; i <= 102; i++)
                        for (int i = 3; i <= 100; i++)
                        {

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1")
                            {

                                e.Row.Cells[i].Text = "NA";
                            }

                        }
                    }
                    if (ddlsheet.SelectedIndex == 1)
                    {
                        //  for (int i = 2; i <= 102; i++)
                        for (int i = 3; i <= 72; i++)
                        {

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            { }
                            if (e.Row.Cells[i].Text == "-1")
                            {

                                e.Row.Cells[i].Text = "NA";
                            }

                        }
                    }

                }
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnsitesearch_Click(object sender, EventArgs e)
    {

        try
        {
            if (ddlstatusall.SelectedIndex == 2)
            {
                DataTable circle = (DataTable)Session["circlsheet"];

                DataTable criti = (DataTable)Session["circlsheetcriti"];

                for (int i = 0; i < circle.Rows.Count; i++)
                {
                    if (txtsearxh.Text != circle.Rows[i][1].ToString())
                    {
                        circle.Rows.Remove(circle.Rows[i]);
                        i--;
                    }

                }
                statusflag = 1;
                GridView1.DataSource = circle;
                GridView1.DataBind();
                GridView2.DataSource = null;
                GridView2.DataBind();

            }
            else
            {
                DataTable circle = (DataTable)Session["circlsheet"];

                DataTable criti = (DataTable)Session["circlsheetcriti"];

                for (int i = 0; i < circle.Rows.Count; i++)
                {
                    if (txtsearxh.Text != circle.Rows[i][1].ToString())
                    {
                        circle.Rows.Remove(circle.Rows[i]);
                        i--;
                    }

                }

                GridView1.DataSource = circle;
                GridView1.DataBind();


                for (int j = 0; j < criti.Rows.Count; j++)
                {

                    if (txtsearxh.Text != criti.Rows[j][1].ToString())
                    {
                        criti.Rows.Remove(criti.Rows[j]);
                        j--;
                    }


                }
                GridView2.DataSource = criti;
                GridView2.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void btnper_Click(object sender, EventArgs e)
    {

        try
        {
            if (ddlpercentage.SelectedIndex != 0)
            {
                DateTime from = Convert.ToDateTime(TextBox1.Text);
                DateTime to = Convert.ToDateTime(TextBox2.Text);
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView2.DataSource = null;
                GridView2.DataBind();
                rptlable.Text = "";

                DataTable percentagemap = new DataTable();
                DataTable percentagemap1 = new DataTable();

                //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                {
                    da = new mydataaccess1();
                    ba1 = new myvodav23();
                    if (ddlpercentage.SelectedIndex == 1)
                    {
                        Step1 = 1;


                    }
                    if (ddlpercentage.SelectedIndex == 2)
                    {
                        Step1 = 2;


                    }
                    if (ddlpercentage.SelectedIndex == 3)
                    {
                        Step1 = 3;


                    }
                    if (ddlpercentage.SelectedIndex == 4)
                    {
                        Step1 = 4;


                    }

                    int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                    string Labelvalue = drpcircle.SelectedValue;
                    percentagemap = new DataTable();
                    string Dump = "max";
                    da = new mydataaccess1();

                    string chk_type = "";
                    if (Sheet == 10 || Sheet == 11 || Sheet == 12)
                    {
                        chk_type = null;
                    }
                    else
                    {
                        chk_type = "and technician_rating_master_allchecksheet.check_type= " + Sheet + "";
                    }


                    if (Labelvalue == "All")
                    {
                        if (ddlprovidername.SelectedValue == "1")
                        {
                            percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                        }
                        else
                        {
                            percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                        }
                    }
                    else
                    {
                        if (ddlprovidername.SelectedValue == "1")
                        {
                            percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                        }
                        else
                        {
                            percentagemap = da.pivort_allsheet_by_percentage_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                        }
                    }

                    da = new mydataaccess1();
                    if (Labelvalue == "All")
                    {
                        if (ddlprovidername.SelectedValue == "1")
                        {
                            percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                        }
                        else
                        {
                            percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " ", Sheet, Step1, Labelvalue, Dump);
                        }
                    }
                    else
                    {
                        if (ddlprovidername.SelectedValue == "1")
                        {
                            percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                        }
                        else
                        {
                            percentagemap1 = da.pivort_allsheet_by_percentage_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and  user_circle_master.userid =" + userid + " " + chk_type + " and user_circle_master.circle='" + Labelvalue + "'", Sheet, Step1, Labelvalue, Dump);
                        }
                    }
                }

                GridView1.DataSource = percentagemap;
                GridView1.DataBind();
                Session["circlsheet"] = percentagemap;
                GridView2.DataSource = percentagemap1;
                GridView2.DataBind();
                Session["circlsheetcriti"] = percentagemap1;

                rptlable.Text = "Report for Percentage ('" + SpacialCharRemove.XSS_Remove(ddlpercentage.SelectedValue) + "')";
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Percentage Value');</script>");
                //  ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Percentage Value)", true);
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }


    }
    protected void btnstatus_Click(object sender, EventArgs e)
    {

        try
        {
            if (ddlstatusall.SelectedIndex != 0)
            {
                DateTime from = Convert.ToDateTime(TextBox1.Text);
                DateTime to = Convert.ToDateTime(TextBox2.Text);
                ImageButton2.Visible = true;
                ImageButton1.Visible = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView2.DataSource = null;
                GridView2.DataBind();
                rptlable.Text = "";
                da = new mydataaccess1();
                ba1 = new myvodav23();
                ba1.Labelvalue = drpcircle.SelectedValue;
                if (ddlstatusall.SelectedValue == "Completed")
                {

                    //if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                    {
                        string Remarks = "2";
                        string check_sheet = "";
                        if (ddlsheet.SelectedItem.Text == "ISIC V2.3")
                        {
                            check_sheet = "v2";
                        }
                        else if (ddlsheet.SelectedItem.Text == "BSIC V5.3")
                        {
                            check_sheet = "v5";
                        }
                        else if (ddlsheet.SelectedItem.Text == "MTX/VTH")
                        {
                            check_sheet = "v2ms";
                        }
                        else
                        {
                            check_sheet = ddlsheet.SelectedItem.Text;
                        }
                        string Labelvalue = drpcircle.SelectedValue;
                        // ba1.Remarks = ddltechnicianname.SelectedValue;
                        int Sheet = Convert.ToInt32(ddlsheet.SelectedItem.Value);
                        DataTable datedata = new DataTable();
                        string Dump = "max";
                        da = new mydataaccess1();
                        if (Labelvalue == "All")
                        {
                            if (ddlprovidername.SelectedValue == "1")
                            {
                                datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                            }
                            else
                            {
                                datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and  surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                            }
                        }
                        else
                        {
                            if (ddlprovidername.SelectedValue == "1")
                            {
                                datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                            }
                            else
                            {
                                datedata = da.pivort_allsheet_by_technician_allsheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                            }
                        }
                        //int i = datedata.Columns.Count;
                        GridView1.DataSource = datedata;
                        GridView1.DataBind();
                        Session["circlsheet"] = datedata;

                        DataTable datedata1 = new DataTable();
                        da = new mydataaccess1();
                        if (Labelvalue == "All")
                        {
                            if (ddlprovidername.SelectedValue == "1")
                            {
                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and  surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "') and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                            }
                            else
                            {
                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'", Sheet, Labelvalue, Dump);
                            }
                        }
                        else
                        {
                            if (ddlprovidername.SelectedValue == "1")
                            {
                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                            }
                            else
                            {
                                datedata1 = da.pivort_allsheet_by_technician_criticality_allchecksheet("where site_master.provider not in ('Vodafone','VIL') and user_circle_master.userid =" + userid + " and surveydate>='" + from + "' and surveydate<= dateadd(dd,1,'" + to + "')  and site_master.status='" + Remarks + "'and site_master.check_sheet='" + check_sheet + "'and user_circle_master.circle='" + Labelvalue + "'", Sheet, Labelvalue, Dump);
                            }
                        }
                        //int i = datedata.Columns.Count;
                        GridView2.DataSource = datedata1;
                        GridView2.DataBind();
                        Session["circlsheetcriti"] = datedata1;

                        rptlable.Text = "Report With Complete Status";
                    }

                }
                if (ddlstatusall.SelectedValue == "Not Completed")
                {
                    statusflag = 1;
                    da = new mydataaccess1();
                    ba1 = new myvodav23();
                    ba1.Labelvalue = drpcircle.SelectedValue;
                    ba1.Siteid = "1";
                    DataTable datedata1 = new DataTable();
                    da = new mydataaccess1();
                    string sp_user_d1 = da.select_user_cookie(Session["user"].ToString());
                    ba1.User = sp_user_d1;

                    if (ddlsheet.SelectedIndex == 1)
                    {
                        ba1.Pri = "v2";
                    }
                    if (ddlsheet.SelectedIndex == 2)
                    {
                        ba1.Pri = "v5";
                    }
                    if (ddlsheet.SelectedIndex == 3)
                    {
                        ba1.Pri = "v2ms";
                    }
                    if (ddlsheet.SelectedIndex != 1 && ddlsheet.SelectedIndex != 2 && ddlsheet.SelectedIndex != 3)
                    {
                        ba1.Pri = ddlsheet.SelectedItem.Text;
                    }
                    da = new mydataaccess1();
                    ba1.Provider = ddlprovidername.SelectedItem.Text;
                    datedata1 = da.notcompleted1(ba1);
                    if (datedata1.Rows.Count > 0)
                    {
                        for (int i = 0; i < datedata1.Rows.Count; i++)
                        {
                            if (datedata1.Rows[i][6].ToString() == "2")
                            {
                                datedata1.Rows[i][6] = "Completed";
                            }
                            if (datedata1.Rows[i][6].ToString() == "1")
                            {
                                datedata1.Rows[i][6] = "Never inspected";
                            }
                            if (datedata1.Rows[i][6].ToString() == "3")
                            {
                                datedata1.Rows[i][6] = "Modify by circle admin";
                            }
                            if (datedata1.Rows[i][6].ToString() == "4")
                            {
                                datedata1.Rows[i][6] = "Reset by circle admin";
                            }
                            if (datedata1.Rows[i][6].ToString() == "6")
                            {
                                datedata1.Rows[i][6] = "Request to Recheck";
                            }

                        }
                        GridView1.DataSource = datedata1;
                        GridView1.DataBind();
                        Session["circlsheet"] = datedata1;

                        rptlable.Text = "Not Completed Site List";

                        GridView2.DataSource = null;
                        GridView2.DataBind();
                    }
                    rptlable.Text = "Not Completed Site List";
                }

            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Status');</script>");
                // ScriptManager.RegisterStartupScript(this, typeof(string), "SHOW_ALERT", "alert('Please select Status )", true);
            }
        }

        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }


    }
    protected void ddlprovidername_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlstatus_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //if (ddlstatus.SelectedIndex == 2)
        //{
        //    pnn.Visible = false;
        //}

    }
    protected void ddlstatusall_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlstatusall.SelectedIndex == 2)
        {
            pnn.Visible = false;
        }
    }
}
