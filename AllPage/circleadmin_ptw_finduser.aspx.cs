﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using mybusiness;
using System.Data;

public partial class AllPage_admin_finduser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    string sp_user_d = "";
   protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
        
        if (!IsPostBack)
        {
            try
            {
                int i = 3;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {
                }
                else
                {

                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            try
            {

                if (Session["flag"].ToString() == "4")
                {
                    lnkchangeproject.Visible = true;
                }
                else
                {
                    lnkchangeproject.Visible = false;
                }

                // marque Start
               /* int count = 0;
                int count1 = 0;
                int count2 = 0;
                da = new mydataaccess1();
                dt = new DataTable();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());
                

                da = new mydataaccess1();
                count = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                da = new mydataaccess1();
                count1 = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);
                lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
*/
                //Marquee End

                da = new mydataaccess1();
                dt = new DataTable();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());
                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                if (lblusername.Text == "")
                {
                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                }
                da = new mydataaccess1();
                dt = new DataTable();
                dt = da.view_circle_find_sites(sp_user_d);
                ddlcircle.DataTextField = "circle";
                ddlcircle.DataSource = dt;
                ddlcircle.DataBind();

                ddlcircle_SelectedIndexChanged(sender, e);

                panel1.Visible = false;
                
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

        }
    }

    protected void ddlcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        panel1.Visible = false;
        GridView1.DataSource = null;
        GridView1.DataBind();
        if (ddlcircle.SelectedIndex != 0)
        {

            lblerror.Text = "";
            grdinfo.DataSource = null;
            grdinfo.DataBind();
            txtsite.Text = "";

            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.view_fto(ddlcircle.SelectedValue, "2");
            lbuser.DataTextField = "username";
            lbuser.DataSource = dt;
            lbuser.DataBind();
        }
    }
    protected void lbsearch_Click(object sender, EventArgs e)
    {
        if (lbuser.SelectedIndex == -1 || txtsite.Text == "")
        {
            lblerror.Text = "Select Any User From ListBox and write SiteId.";
        }
        else
        {
            try
            {
                lblerror.Text = "";
                da = new mydataaccess1();
                dt = new DataTable();
                dt = da.viewsitesbydis_for_map(txtsite.Text, lbuser.SelectedValue);
                if (dt.Rows.Count == 0)
                {
                    grdinfo.DataSource = null;
                    grdinfo.DataBind();
                    panel1.Visible = true;
                }
                else
                {
                    grdinfo.DataSource = dt;
                    grdinfo.DataBind();
                    panel1.Visible = true;
                }
            }
            catch
            { }

        }
    }
    protected void lbuser_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           /* grdinfo.DataSource = null;
            grdinfo.DataBind();

            ba = new myvodav2();
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.get_imei_from_user(lbuser.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ba = new myvodav2();
                da = new mydataaccess1();
                DataTable latlong = new DataTable();
                ba.Imei = dt.Rows[0][0].ToString();
                latlong = da.getlatlong(ba);

                if (latlong.Rows.Count != 0)
                {
                    Double radlatmob = Convert.ToDouble((Convert.ToDouble(latlong.Rows[0][0].ToString()) * 3.14) / 180);
                    Double radlongmob = Convert.ToDouble((Convert.ToDouble(latlong.Rows[0][1].ToString()) * 3.14) / 180);

                    ba = new myvodav2();
                    da = new mydataaccess1();
                    ba.Circle = ddlcircle.SelectedValue;
                    ba.Lat = radlatmob;
                    ba.Longitude = radlongmob;
                    ba.Accuracy = Convert.ToDouble(latlong.Rows[0][2].ToString());
                    dt = new DataTable();
                    dt = da.viewsitesbydis_fin_sites(ba);
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
*/
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            string s = da.select_user_cookie(Session["user"].ToString());

            string r = s;
            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void txtsite_TextChanged(object sender, EventArgs e)
    {
        if (lbuser.SelectedIndex == -1 || txtsite.Text == "")
        {
            lblerror.Text = "Select Any User From ListBox and write SiteId.";

        }
        else
        {
            lblerror.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.viewsitesbydis_for_map(txtsite.Text, lbuser.SelectedValue);
            if (dt.Rows.Count == 0)
            {
                grdinfo.DataSource = null;
                grdinfo.DataBind();
                panel1.Visible = true;
            }
            else
            {
                grdinfo.DataSource = dt;
                grdinfo.DataBind();
                panel1.Visible = true;
            }
        }
    }
 protected void Button1_Click(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.view_fto_username(ddlcircle.SelectedValue, "2", TextBox1.Text);
        lbuser.DataTextField = "username";
        lbuser.DataSource = dt;
        lbuser.DataBind();
    }
}
