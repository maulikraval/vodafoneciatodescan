﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;
using NPOI.HPSF;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel.Contrib;
using NPOI.HSSF.UserModel;
using System.IO;


public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
 HSSFWorkbook hssfworkbook;
    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;

   
    protected void Page_Load(object sender, EventArgs e)
    {
 

        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                string strPreviousPage = "";
                if (Request.UrlReferrer != null)
                {
                    strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
                }
                if (strPreviousPage == "")
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int i = 3;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }


            try
            {

                if (Session["flag"].ToString() == "4")
                {
                    //lnkchangeproject.Visible = true;
                }
                else
                {
                    //lnkchangeproject.Visible = false;
                }
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {


                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    da = new mydataaccess1();
                    dt = new DataTable();
                    string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                /*    da = new mydataaccess1();
                    count = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                    da = new mydataaccess1();
                    count1 = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);

                    lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
					*/

                    da = new mydataaccess1();
                    dt = new DataTable();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());
                    lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);


                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            da = new mydataaccess1();
                            ba = new myvodav2();
                            ba = new myvodav2();
                            DataTable circle = new DataTable();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }


                }
            }

            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
    }






   public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
         DataTable dt = (DataTable)Session["dtexport"];     

        string filename = "issuers.xls";
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));
        Response.Clear();

        InitializeWorkbook();
        exporttoexcel(dt);
        Response.BinaryWrite(WriteToStream().GetBuffer());

    }

   
  void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }
    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }

    void exporttoexcel(System.Data.DataTable dt)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet("Sheet1");
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }


    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        if (drpcircle.SelectedIndex != 0)
        {

            da = new mydataaccess1();
            dt = new DataTable();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.ptw_select_all_issuer_mapping_data(drpcircle.Text, sp_user_d, ddlcategory.SelectedValue);
Session["dtexport"] = dt; 
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        
    }
  protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
	e.Row.Cells[7].Visible = false;

e.Row.Cells[8].Visible = false;       
    }

}
