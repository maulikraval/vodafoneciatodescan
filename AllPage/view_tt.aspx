﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="~/AllPage/view_tt.aspx.cs" Inherits="AllPage_admin_addcircle" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
       
    </script>
    <table width="100%">

        <tr>

            <td style="text-align: center; height: 20px;">
             
                <asp:Panel ID="pan1" ScrollBars="Both" Height="500px" Width="1030px"
                    runat="server">

                    <asp:GridView ID="grduser" runat="server" AutoGenerateColumns="False" EmptyDataText="No TT Found"
            EnableModelValidation="True" DataKeyNames="id" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="200px">
            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle Font-Names="Calibri" Font-Size="13px" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White"  />

                        <Columns>
                 <asp:TemplateField HeaderText="Close By">
                    <ItemTemplate>
                        <asp:DropDownList ID="drp_Closeby" runat="server">
                            <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Chetan Patel" Value="1"></asp:ListItem>
                            
                            <asp:ListItem Text="Poyam" Value="2"></asp:ListItem>
                          
                            <asp:ListItem Text="Mahendra" Value="3"></asp:ListItem>
                              <asp:ListItem Text="Darshini" Value="4"></asp:ListItem>

<asp:ListItem Text="Parth" Value="5"></asp:ListItem>
<asp:ListItem Text="Dhaval" Value="6"></asp:ListItem>
<asp:ListItem Text="Zeel" Value="7"></asp:ListItem>
<asp:ListItem Text="Pooja" Value="8"></asp:ListItem>  
<asp:ListItem Text="Kavya" Value="9"></asp:ListItem>                            
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="Support Remark">
                    <ItemTemplate>
                        <asp:TextBox ID="txt_remark" runat="server" ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Approve">
                    <ItemTemplate>
                        <asp:Button ID="lnbtn_Approve" runat="server" ToolTip="<%# bind('id') %>" OnClick="lnbtn_Approve_Click" Text="Submit"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
               
                <asp:TemplateField HeaderText="Project">
                    <ItemTemplate>
                        <asp:Label ID="lbl_project" runat="server" Text="<%# bind('Project') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Circle">
                    <ItemTemplate>
                        <asp:Label ID="lbl_circle" runat="server" Text="<%# bind('Circle') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="SiteID">
                    <ItemTemplate>
                        <asp:Label ID="lbl_siteid" runat="server" Text="<%# bind('SiteID') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Technician">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Username" runat="server" Text="<%# bind('Technician') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                             
                              <asp:TemplateField HeaderText="Technician Mobile">
                    <ItemTemplate>
                        <asp:Label ID="lbl_mobile" runat="server" Text="<%# bind('technician_mobile') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CompanyName">
                    <ItemTemplate>
                        <asp:Label ID="lbl_company" runat="server" Text="<%# bind('CompanyName') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
               <asp:TemplateField HeaderText="SubCompany">
                    <ItemTemplate>
                        <asp:Label ID="lbl_subcompany" runat="server" Text="<%# bind('Sub_company') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="IssueCategory">
                    <ItemTemplate>
                        <asp:Label ID="lbl_issue" runat="server" Text="<%# bind('IssueCategory') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                             <asp:TemplateField HeaderText="Requested Time By User">
                    <ItemTemplate>
                        <asp:Label ID="lbl_Requestedtime" runat="server" Text="<%# bind('open_time') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="lbl_desc" runat="server" Text="<%# bind('Description') %>"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
              </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>

        </tr>

    </table>
</asp:Content>
