﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;



public partial class admin_Active_dashboard : System.Web.UI.Page
{
    mydataaccess1 da;
    mydataaccess1 da1;
    myvodav2 ba;
    DataTable dt;
    DataRow row;
    private void Page_PreRender(object sender, System.EventArgs e)
    {  
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
    }
    public void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {		
		        da1 = new mydataaccess1();
                string sp_user_d = da1.select_user_cookie(Session["user"].ToString());
     
            if (Session["role"].ToString() != "12" && Session["um"].ToString()!=sp_user_d)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {
              
                //lblusername.Text = sp_user_d;
                string User = sp_user_d;
                da1 = new mydataaccess1();
                int userid = 0;
                try
                {
                    userid = da1.selectuserid(User);
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }

                DataTable DT = new DataTable();
                #region JobShedulePlanned
                da = new mydataaccess1();
                DT.Clear();
                DT = da.Active_DashBoard("JobShedulePlanned", userid.ToString());
                if (DT.Rows.Count > 0)
                {
                    grdjobsheduleplanned.DataSource = DT;
                    grdjobsheduleplanned.DataBind();
                }
                else
                {
                    grdjobsheduleplanned.DataSource = null;
                    grdjobsheduleplanned.DataBind();
                }
                #endregion
                #region JobSheduleCurrent
                da = new mydataaccess1();
                DT.Clear();

                DT = da.Active_DashBoard("JobSheduleCurrent", userid.ToString());
                if (DT.Rows.Count > 0)
                {
                    grdjobshedulecurrent.DataSource = DT;
                    grdjobshedulecurrent.DataBind();
                }
                else
                {
                    grdjobshedulecurrent.DataSource = null;
                    grdjobshedulecurrent.DataBind();
                }
                #endregion
                #region PendingSchedule
                da = new mydataaccess1();
                DT.Clear();
                DT = da.Active_DashBoard("PendingSchedule", userid.ToString());
                if (DT.Rows.Count > 0)
                {
                    grdPendingSchedule.DataSource = DT;
                    grdPendingSchedule.DataBind();
                }
                else
                {
                    grdPendingSchedule.DataSource = null;
                    grdPendingSchedule.DataBind();
                }
                #endregion
                #region FortnightlyCritical
                da = new mydataaccess1();
                DT.Clear();
                DT = da.Active_DashBoard("FortnightlyCritical", userid.ToString());
                if (DT.Rows.Count > 0)
                {
                    grdFortnightlyCritical.DataSource = DT;
                    grdFortnightlyCritical.DataBind();
                }
                else
                {
                    grdFortnightlyCritical.DataSource = null;
                    grdFortnightlyCritical.DataBind();
                }
                #endregion
                //#region DailyPunchPoint
                //da = new mydataaccess1();
                //DT.Clear();
                //DT = da.pm_DashBoard("DailyPunchPoint");
                //if (DT.Rows.Count > 0)
                //{
                //    grdDailyPunchPoint.DataSource = DT;
                //    grdDailyPunchPoint.DataBind();
                //}
                //else
                //{
                //    grdDailyPunchPoint.DataSource = null;
                //    grdDailyPunchPoint.DataBind();
                //}
                //#endregion
                //#region WeeklyPunchPoint
                //da = new mydataaccess1();
                //DT.Clear();
                //DT = da.pm_DashBoard("WeeklyPunchPoint");
                //if (DT.Rows.Count > 0)
                //{
                //    grdWeeklyPunchPoint.DataSource = DT;
                //    grdWeeklyPunchPoint.DataBind();
                //}
                //else
                //{
                //    grdWeeklyPunchPoint.DataSource = null;
                //    grdWeeklyPunchPoint.DataBind();
                //}
                //#endregion
            }
        }
    }
    public string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
        string newSortDirection = String.Empty;
        switch (sortDirection)
        {
            case SortDirection.Ascending:
                newSortDirection = "ASC";
                break;

            case SortDirection.Descending:
                newSortDirection = "DESC";
                break;
        }
        return newSortDirection;
    }
    public void grdjobsheduleplanned_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grdjobsheduleplanned.DataSource as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grdjobsheduleplanned.DataSource = dataView;
            grdjobsheduleplanned.DataBind();
        }
    }
    public void grdjobshedulecurrent_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grdjobshedulecurrent.DataSource as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grdjobshedulecurrent.DataSource = dataView;
            grdjobshedulecurrent.DataBind();
        }
    }
    public void grdPendingSchedule_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grdPendingSchedule.DataSource as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
            grdPendingSchedule.DataSource = dataView;
            grdPendingSchedule.DataBind();
        }
    }
    public void grdFortnightlyCritical_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grdFortnightlyCritical.DataSource as DataTable;
        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
            grdFortnightlyCritical.DataSource = dataView;
            grdFortnightlyCritical.DataBind();
        }
    }
    //public void grdDailyPunchPoint_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dataTable = grdDailyPunchPoint.DataSource as DataTable;
    //    if (dataTable != null)
    //    {
    //        DataView dataView = new DataView(dataTable);
    //        dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
    //        grdDailyPunchPoint.DataSource = dataView;
    //        grdDailyPunchPoint.DataBind();
    //    }
    //}
    //public void grdWeeklyPunchPoint_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dataTable = grdWeeklyPunchPoint.DataSource as DataTable;
    //    if (dataTable != null)
    //    {
    //        DataView dataView = new DataView(dataTable);
    //        dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);
    //        grdWeeklyPunchPoint.DataSource = dataView;
    //        grdWeeklyPunchPoint.DataBind();
    //    }
    //}
    #region Export Excel for Planned 
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
       
        grdjobsheduleplanned.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
        
    }
    #endregion
    #region Export Excel for Current/Running
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        grdjobshedulecurrent.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
    }
    #endregion
    #region Export Excel for Pending Shedule
    protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        grdPendingSchedule.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
    }

    #endregion
    #region Export Excel for Fortnightly Critical  
    protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        grdFortnightlyCritical.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
    }
    #endregion
    //#region Export Excel for Daily PunchPoint
    //protected void ImageButton5_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Clear();
    //    Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
    //    Response.Charset = "";
    //    Response.ContentType = "application/vnd.xls";
    //    System.IO.StringWriter stringWrite = new System.IO.StringWriter();
    //    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
    //    grdDailyPunchPoint.RenderControl(htmlWrite);
    //    Response.Write(stringWrite.ToString());
    //    Response.End();
    //}
    //#endregion
    //#region Export Excel for Weekly PunchPoint
    //protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.Clear();
    //    Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
    //    Response.Charset = "";
    //    Response.ContentType = "application/vnd.xls";
    //    System.IO.StringWriter stringWrite = new System.IO.StringWriter();
    //    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
    //    grdWeeklyPunchPoint.RenderControl(htmlWrite);
    //    Response.Write(stringWrite.ToString());
    //    Response.End();
    //}
    //#endregion
   
}