﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;


public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["flag"].ToString() == "4")
            {
                lnkchangeproject.Visible = true;
            }
            else
            {
                lnkchangeproject.Visible = false;
            }

            statusflag = 0;
            try
            {
                ImageButton1.Visible = false;

                int i = 2;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {

                    if (Session["role"].ToString() == "2")
                    {
                        Response.Redirect("~/PM/Home.aspx");
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        Response.Redirect("~/AllPage/circleadmin_Home.aspx");
                    }
                    if (Session["role"].ToString() == "4")
                    {
                        Response.Redirect("~/AllPage/admin_Default3.aspx");
                    }
                    if (Session["role"].ToString() == "5")
                    {
                        Response.Redirect("~/PM/SiteInfonew.aspx");
                    }
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {

            // marque Start
            /*  int count = 0;
              int count1 = 0;
              int count2 = 0;
              da = new mydataaccess1();
              count = da.ptw_dashboard_active_ptw();

              da = new mydataaccess1();
              count1 = da.ptw_dashboard_expire_ptw();
              lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
  */
            da = new mydataaccess1();
            dt = new DataTable();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

            //Marquee End

            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    /*
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = sp_user_d;

                            circle = da.getcirclename();

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                     */
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();

                            ba.User = lblusername.Text;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_company();
                            ddlissuercompany.DataTextField = "company_name";
                            ddlissuercompany.DataSource = dt;
                            ddlissuercompany.DataBind();
                            ddlissuercompany.Items.Insert(0, "Select");
                            ddlissuercompany.Items.Insert(1, "All");
                        }
                    }

                    if (Session["role"].ToString() == "2")
                    {
                        //da = new mydataaccess1();
                        //DataTable dt = new DataTable();
                        //dt = da.viewpivotrpt();
                        //GridView1.DataSource = dt;
                        //GridView1.DataBind();



                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu;
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        // Response.ClearHeaders();

        //        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=ptw_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }



    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_risk_company_master(drpcircle.Text);
        ddlreceivercompany.DataTextField = "company_name";
        ddlreceivercompany.DataSource = dt;
        ddlreceivercompany.DataBind();
        ddlreceivercompany.Items.Insert(0, "Select");
        ddlreceivercompany.Items.Insert(1, "All");
        GridView1.DataSource = null;
        GridView1.DataBind();
    }

    protected void btnsitesearch_Click(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();

        dt = da.Ptw_Question_Risk_Pivot("where site_master.siteid='" + txtsearxh.Text + "'", "");

        GridView1.DataSource = dt;
        GridView1.DataBind();

        GridView1.Visible = true;
    }

    protected void ddlpurposeofwork_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        ImageButton1.Visible = true;
        string message = "";
        string filter = " where (bdm.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and bdm.status!=0 and bdm.status!=1 and bdm.status!=4 and bdm.status!=5 and (bdm.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";

        filter = " where sm.circle='" + drpcircle.SelectedItem.Text.ToString() + "' and (bdm.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and bdm.status!=0 and bdm.status!=1 and bdm.status!=4 and bdm.status!=5 and (bdm.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";
        message = "Summary of PTW (" + ddlcategory.SelectedItem.Text + ") issued by " + ddlissuercompany.SelectedValue + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + ddlissuer.SelectedValue + " and Receiver Company -" + ddlreceivercompany.SelectedValue + ")";
        if (ddlcategory.SelectedIndex != 0)
        {
            filter += " and bdm.cat=" + ddlcategory.SelectedValue + "";
        }
        if (ddlissuercompany.SelectedIndex != 1)
        {
            filter += "  and  cm.company_name='" + ddlissuercompany.SelectedValue + "'";
        }
        if (ddlissuer.SelectedIndex != 1)
        {
            filter += "  and  um.username='" + ddlissuer.SelectedValue + "'";
        }
        if (ddlreceivercompany.SelectedIndex != 1)
        {
            filter += " and rcm.company_name='" + ddlreceivercompany.SelectedValue + "'";
        }

        da = new mydataaccess1();
        dt = new DataTable();

        dt = da.Ptw_Question_Risk_Pivot(filter, ddlcategory.SelectedValue);
        GridView1.DataSource = dt;
        GridView1.DataBind();

    }
    protected void ddlissuercompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        da = new mydataaccess1();
        dt = da.select_issuer_from_company_chart(ddlissuercompany.Text, ddlcategory.SelectedValue, drpcircle.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            ddlissuer.DataSource = dt;
            ddlissuer.DataTextField = "username";
            ddlissuer.DataBind();
            ddlissuer.Items.Insert(0, "Select");
            ddlissuer.Items.Insert(1, "All");
        }
        else
        {
            ddlissuer.Items.Clear();

            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('There is no any issuer for selected company');</script>");
        }
    }
}
