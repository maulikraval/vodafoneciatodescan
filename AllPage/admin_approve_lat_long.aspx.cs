﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;
using System.Net.Mail;
using System.Net;

public partial class admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try
            {
                da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
                if (Session["role"].ToString() != "4" && Session["um"].ToString() != sp_user_)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.select_distance_data();
                    grduser.DataSource = dt;
                    grduser.DataBind();
                }

            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }
    }



    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }

    protected void LinkButton1_Click1(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        da = new mydataaccess1();
        da.unblock_user(lb.ToolTip);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('User Unblocked');", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        string[] data = lb.ToolTip.Split('-');

        da = new mydataaccess1();
        da.update_approval_siteid(Convert.ToInt32(data[0].ToString()), 2);

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.get_circle_email_from_siteid(data[1].ToString());

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string subject = "Approval of Lat/Long";
            string body = "Dear User,<br /><br /> Lat Long of SiteID: '" + SendMail.ishtml(data[1].ToString()) + "' are approved...<br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT/PTW Team ";
            SendMail sm = new SendMail();
            string Tomail = dt.Rows[i][0].ToString();
            sm.mailsend(Tomail, subject, body);
            ////MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", dt.Rows[i][0].ToString(), "Approval about Lat/Long-"+lb.ToolTip+", Dear Sir,<br /><br />Lat Long Of SiteID:"+lb.ToolTip+"  are Approved <br /> <br /> Regards, <br />PTW Team ");
            //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", dt.Rows[i][0].ToString(), "Approval of Lat/Long", "Dear User,<br /><br /> Lat Long of SiteID: '" + data[1].ToString() + "' are approved...<br />Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />CIAT/PTW Team ");
            //message.IsBodyHtml = true;

            //#region "VODAFONE SERVER CREDENTIALS"

            ////SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);
            ////emailClient.Credentials = new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");
            ////emailClient.EnableSsl = false;

            //#endregion

            //#region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

            //SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
            //emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
            //emailClient.EnableSsl = true;

            //#endregion

            //emailClient.Send(message);

        }

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_distance_data();
        grduser.DataSource = dt;
        grduser.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        string[] data = lb.ToolTip.Split('-');

        da = new mydataaccess1();
        da.update_approval_siteid(Convert.ToInt32(data[0].ToString()), -2);
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_distance_data();
        grduser.DataSource = dt;
        grduser.DataBind();
    }
    protected void grduser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            e.Row.Cells[2].Visible = false;
        }
        catch
        { }
    }
}
