﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using FindDataAccessLayer;
//using mybusiness;
using System.Data;

public partial class AllPage_FindUser : System.Web.UI.Page
{
    FindDataAccess da;
    //myvodav2 ba;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    public void Fill_GV_User(DataTable Table)
    {
        gv_user.DataSource = Table;
        gv_user.DataBind();
        gv_user.Visible = true;
    }

    protected void btn_find_user_Click(object sender, EventArgs e)
    {
        if (txt_User.Text != "")
        {
            Label1.Text = "";
            Label1.Visible = false;
            da = new FindDataAccess();
            Fill_GV_User(da.Support_Find_User_su(txt_User.Text, ""));
        }
        else
        {
            Label1.Text = "Please Enter UserName!";
            Label1.Visible = true;
        }
    }
    protected void btn_find_contact_Click(object sender, EventArgs e)
    {
        if (txt_contact.Text != "")
        {
            da = new FindDataAccess();
            Fill_GV_User(da.Support_Find_User_su("", txt_contact.Text));
        }
        else
        {
            Label2.Text = "Please Enter UserName!";
            Label2.Visible = true;
        }
    }

    protected void gv_user_SelectedIndexChanged(object sender, EventArgs e)
    {
        string User_Id = gv_user.SelectedRow.Cells[1].Text;
        string User_Name = gv_user.SelectedRow.Cells[2].Text;

        Response.Redirect("~/AllPage/FindSite.aspx?UserId=" + SpacialCharRemove.XSS_Remove(User_Id) + "&UserName=" + SpacialCharRemove.XSS_Remove(User_Name));    
    }
}