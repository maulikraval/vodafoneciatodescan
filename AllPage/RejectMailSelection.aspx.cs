﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer2;
using FindDataAccessLayer;
using System.Data;
using System.Net.Mail;
using System.Net;

public partial class AllPage_RejectMailSelection : System.Web.UI.Page
{
    FindDataAccess DA;
    DataTable dt;
    mydataaccess2 da2;
    int id;
    string project, siteid, sitename, oldtime, oldlat, oldlong, newlat, newlong, username, distance, circle, mobile, maincompany, subcompany, zone, closeby;
    string to_email;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (drpemail.SelectedIndex != 1)
        {
            lblother.Visible = false;
            txtother.Visible = false;
        }
        else
        {
            lblother.Visible = true;
            txtother.Visible = true;
        }
        id = Convert.ToInt32(Request.QueryString["id"]);
        project = Request.QueryString["pro"].ToString();
        siteid = Request.QueryString["siteid"].ToString();
        sitename = Request.QueryString["sname"].ToString();
        oldtime = Request.QueryString["otime"].ToString();
        oldlat = Request.QueryString["olat"].ToString();
        oldlong = Request.QueryString["olong"].ToString();
        newlat = Request.QueryString["nlat"].ToString();
        newlong = Request.QueryString["nlong"].ToString();
        username = Request.QueryString["user"].ToString();
        distance = Request.QueryString["dist"].ToString();
        circle = Request.QueryString["circle"].ToString();
        mobile = Request.QueryString["mo"].ToString();
        maincompany = Request.QueryString["mcom"].ToString();
        subcompany = Request.QueryString["scom"].ToString();
        zone = Request.QueryString["zone"].ToString();
        closeby = Request.QueryString["closeby"].ToString();

        if (!IsPostBack)
        {
            da2 = new mydataaccess2();
            DataSet email = new DataSet();
            email = da2.get_email_id_user_rolewise(circle);
            drpemail.DataSource = email;
            drpemail.DataTextField = "emailid";
            drpemail.DataBind();
            drpemail.Items.Insert(0, "Select");
            drpemail.Items.Insert(1, "Other");
        }
    }

    protected void btnsend_Click(object sender, EventArgs e)
    {
        DA = new FindDataAccess();
        DA.CLOSE_TT_22042016(id, closeby, "Rejected by CIAT Helpdesk");
        da2 = new mydataaccess2();
        if (drpemail.SelectedIndex != 0)
        {
            if (drpemail.SelectedIndex != 1)
            {
                da2.insert_circleadmin_email_in_tt(id, drpemail.SelectedItem.Text);
                Send_Email_To_CircleAdmin("Reject", project, username, siteid, sitename, oldtime, DateTime.Now.ToString(), oldlat, oldlong, newlat, newlong, distance, circle, mobile, maincompany, subcompany, zone);
                //Response.Redirect("http://ptw.skyproductivity.com/AllPage/LatLongApproval.aspx");
            }
            else
            {
                if (txtother.Text != "")
                {
                    da2.insert_circleadmin_email_in_tt(id, txtother.Text);
                    Send_Email_To_CircleAdmin("Reject", project, username, siteid, sitename, oldtime, DateTime.Now.ToString(), oldlat, oldlong, newlat, newlong, distance, circle, mobile, maincompany, subcompany, zone);
                    //Response.Redirect("http://ptw.skyproductivity.com/AllPage/LatLongApproval.aspx");
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Fill The Value!!!');</script>");
                }
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select Email!!!');</script>");
        }
    }

    protected void Send_Email_To_CircleAdmin(string Action_Status, string ProjectName, string Username, string SiteId, string SiteName, string OTime, string CTime, string OLat, string OLong, string NLat, string NLong, string Distance, string Circle, string Mobile, string MCompnay, string SCompany, string Zone)
    {
        try
        {
            ////Label Circle = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Circle");
            //DataSet ds = new DataSet();
            //ds = da2.get_email_id_user_rolewise(Circle);
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //string[] to_email2 = new string[ds.Tables[0].Rows.Count];
            //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //{
            //to_email2[i] = ds.Tables[0].Rows[i]["emailid"].ToString();
            //if (to_email2[i] != "" && to_email2[i] != null)
            //{
            // Vodafone SMTP Credentials
            da2 = new mydataaccess2();
            to_email = da2.get_email_id_user(Username);
            string Body = Email_Body2(Action_Status, ProjectName, Username, SiteId, SiteName, OTime, CTime, OLat, OLong, NLat, NLong, Distance, Circle, Mobile, to_email, MCompnay, SCompany, Zone);
            string subject = "";
            // MailMessage message;
            if (drpemail.SelectedIndex != 0)
            {
                if (drpemail.SelectedIndex == 1)
                {
                    if (Action_Status.ToLower() == "approve")
                    {
                        subject = "Need Approval to Update lat/long for site - " + SendMail.ishtml(SiteId) + " requested by user - " + SendMail.ishtml(Username);
                    }
                    else
                    {
                        subject = "Need Approval to Update lat/long for site - " + SendMail.ishtml(SiteId) + " requested by user - " + SendMail.ishtml(Username);
                    }

                    SendMail sm = new SendMail();
                    string ccmail_ = "latlong.helpdesk@gmail.com";
                    sm.mailsend(to_email, subject, Body, ccmail: ccmail_);

                    //              message = new MailMessage("CITappsupport@vodafoneidea.com", txtother.Text);

                    //              if (Action_Status.ToLower() == "approve")
                    //              {
                    //                  message.Subject = "Need Approval to Update lat/long for site - " + SiteId + " requested by user - " + Username;
                    //              }
                    //              else
                    //              {
                    //                  message.Subject = "Need Approval to Update lat/long for site - " + SiteId + " requested by user - " + Username;
                    //              }
                    //              message.Body = Body;
                    //message.CC.Add("latlong.helpdesk@gmail.com");
                    //              message.IsBodyHtml = true;                   

                    //              #region "VODAFONE SERVER CREDENTIALS"

                    //           SmtpClient emailClient = new SmtpClient("10.94.64.107", 25);

                    //      emailClient.Credentials =
                    //      new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                    //      emailClient.EnableSsl = false;

                    //              #endregion

                    //              #region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

                    //            /*  SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
                    //              emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
                    //              emailClient.EnableSsl = true; */

                    //              #endregion

                    //              emailClient.Send(message);
                    Response.Redirect("http://ptw.skyproductivity.com/AllPage/LatLongApproval.aspx");
                }
                else
                {
                    if (Action_Status.ToLower() == "approve")
                    {
                        subject = "Need Approval to Update lat/long for site - " + SendMail.ishtml(SiteId) + " requested by user - " + SendMail.ishtml(Username);
                    }
                    else
                    {
                        subject = "Need Approval to Update lat/long for site - " + SendMail.ishtml(SiteId) + " requested by user - " + SendMail.ishtml(Username);
                    }
                    SendMail sm = new SendMail();
                    string ccmail_ = "latlong.helpdesk@gmail.com";
                    sm.mailsend(to_email, subject, Body, ccmail: ccmail_);

                    //              message = new MailMessage("CITappsupport@vodafoneidea.com", drpemail.SelectedItem.Text);

                    //              if (Action_Status.ToLower() == "approve")
                    //              {
                    //                  message.Subject = "Need Approval to Update lat/long for site - " + SiteId + " requested by user - " + Username;
                    //              }
                    //              else
                    //              {
                    //                  message.Subject = "Need Approval to Update lat/long for site - " + SiteId + " requested by user - " + Username;
                    //              }
                    //              message.Body = Body;
                    //message.CC.Add("latlong.helpdesk@gmail.com");
                    //              /*if (txtother.Text != "")
                    //              {
                    //                  message.CC.Add(txtother.Text);
                    //              }*/
                    //              message.IsBodyHtml = true;

                    //              #region "VODAFONE SERVER CREDENTIALS"

                    //                 SmtpClient emailClient = new SmtpClient("10.94.64.107", 25);

                    //      emailClient.Credentials =
                    //      new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                    //      emailClient.EnableSsl = false;

                    //              #endregion

                    //              #region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

                    //             /* SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
                    //              emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
                    //              emailClient.EnableSsl = true; */

                    //              #endregion

                    //              emailClient.Send(message); 
                    Response.Redirect("http://ptw.skyproductivity.com/AllPage/LatLongApproval.aspx");
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    public string Email_Body2(string Action_Status, string ProjectName, string Username, string SiteId, string SiteName, string OTime, string CTime, string OLat, string OLong, string NLat, string NLong, string Distance, string Circle, string Mobile, string Email, string MCompnay, string SCompany, String Zone)
    {
        string Body = "";

        Body = "Dear Circle Admin, <br /><br />";

        if (Action_Status.ToLower() == "reject")
        {
            Body += "User " + SendMail.ishtml(Username) + "'s Lat / Long Request has been Rejected by CIAT Helpdesk due to Distance from site is more than 2km.<br /><br />";
            Body += "System is sending this mail to you for approval to change lat/long of b/m site as per requested by User.<br /><br />";
            Body += "Kindly check the correctness and take action approve or Reject accordingly.<br /><br />";
            Body += "For give approval To Update Lat-Long >>>> Click on Approve link.<br /><br />";
            Body += "For Reject Lat-Long Update request  >>>>> Click on Reject link.<br /><br />";
            Body += "Lat/long will be automatically get update as per your action.<br /><br />";

            Body += "<table border='1'>";
            Body += "<tr style='background-color:red;color:white'>";
            Body += "<th>ProjectName</th><th>Site Id</th> <th>Site Name</th><th>Zone</th> <th>Requested time by user</th> <th>Rejected Time By Support Team</th> <th>Current lat of site</th> <th>Current long of site</th> <th>User lat</th> <th>User long</th> <th>Requested distance to update as per user location in mtr</th>";
            Body += "</tr>";
            Body += "<tr>";
            Body += "<td>" + SendMail.ishtml(ProjectName) + "</td><td>" + SendMail.ishtml(SiteId) + "</td> <td>" + SendMail.ishtml(SiteName) + "</td> <td>" + SendMail.ishtml(Zone) + "</td> <td>" + SendMail.ishtml(OTime) + "</td> <td>" + SendMail.ishtml(CTime) + "</td> <td>" + SendMail.ishtml(OLat) + "</td> <td>" + SendMail.ishtml(OLong) + "</td> <td>" + SendMail.ishtml(NLat) + "</td> <td>" + SendMail.ishtml(NLong) + "</td> <td>" + SendMail.ishtml(Distance) + "</td>";
            Body += "</tr>";
            Body += "</table><br />";

        }
        Body += "<table border='1'>";
        Body += "<tr style='background-color:red;color:white'>";
        Body += "<th>Requested by username</th> <th>Circle</th> <th>Mobile number of user</th> <th>EmailId of user</th> <th>Main company</th> <th>Sub Company</th>";
        Body += "</tr>";
        Body += "<tr>";
        Body += "<td>" + SendMail.ishtml(Username) + "</td> <td>" + SendMail.ishtml(Circle) + "</td> <td>" + SendMail.ishtml(Mobile) + "</td> <td>" + SendMail.ishtml(Email) + "</td> <td>" + SendMail.ishtml(MCompnay) + "</td> <td>" + SendMail.ishtml(SCompany) + "</td>";
        Body += "</tr>";
        Body += "</table><br />";

        Body += "<a href='http://ptw.skyproductivity.com/PopupApprove.aspx?id=" + SendMail.ishtml(id.ToString()) + "&siteid=" + SendMail.ishtml(SiteId) + "&username=" + SendMail.ishtml(Username) + "&newlat=" + SendMail.ishtml(NLat) + "&newlong=" + SendMail.ishtml(NLong) + "' style='background:#44c767;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Approve</a>&nbsp;&nbsp;&nbsp;";
        Body += "<a href='http://ptw.skyproductivity.com/PopupReject.aspx?id=" + SendMail.ishtml(id.ToString()) + "&siteid=" + SendMail.ishtml(SiteId) + "&username=" + SendMail.ishtml(Username) + "&newlat=" + SendMail.ishtml(NLat) + "&newlong=" + SendMail.ishtml(NLong) + "' style='background:#FF0000;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Reject</a><br /><br />";
        //Body += "<a href='http://ptw.skyproductivity.com/PopupApprove.aspx?pro=" + ProjectName + "&siteid=" + SiteId + "&sname=" + SiteName + "&zone=" + Zone + "&otime=" + OTime + "&ctime=" + CTime + "&olat=" + OLat + "&olong=" + OLong + "&nlat=" + NLat + "&nlong=" + NLong + "&dis=" + Distance + "&user=" + Username + "&ctime=" + Circle + "&olat=" + Mobile + "&olong=" + Email + "&nlat=" + MCompnay + "&nlong=" + SCompany + "' style='background:#44c767;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Approve</a>&nbsp;&nbsp;&nbsp;";
        //Body += "<a href='http://ptw.skyproductivity.com/PopupReject.aspx?pro=" + ProjectName + "&siteid=" + SiteId + "&sname=" + SiteName + "&zone=" + Zone + "&otime=" + OTime + "&ctime=" + CTime + "&olat=" + OLat + "&olong=" + OLong + "&nlat=" + NLat + "&nlong=" + NLong + "&dis=" + Distance + "&user=" + Username + "&ctime=" + Circle + "&olat=" + Mobile + "&olong=" + Email + "&nlat=" + MCompnay + "&nlong=" + SCompany + "' style='background:#FF0000;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Reject</a><br /><br />";

        Body += "Please Note :- This is auto generated mail by system, do not Reply on this mail, please contact below for more details.<br /><br />";
        Body += "Cellsite Inspection Automation Tool<br />";
        Body += "Helpdesk Number :- 079 40009543, 9978791606, 8141587709, 9727716004<br />";
        Body += "Email :- CIAT.Helpdesk@skyproductivity.com<br />";

        return Body;
    }

    protected void drpemail_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
