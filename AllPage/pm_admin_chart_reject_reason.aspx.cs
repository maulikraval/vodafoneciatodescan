﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;
using InfoSoftGlobal;


public partial class PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;

    protected void Page_Load(object sender, EventArgs e)
    {
       // lblmsg.Text = "";
        //FCLiteral.Text = CreateChart();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        panel1.Visible = false;
        panel2.Visible = false;
        panel3.Visible = false;
        if (!IsPostBack)
        {
            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            try
            {
                int i = 2;

                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else if (Convert.ToInt32(Session["role"].ToString()) == 5)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                da = new mydataaccess1();
                count = da.ptw_dashboard_active_ptw();

                da = new mydataaccess1();
                count1 = da.ptw_dashboard_expire_ptw();
                lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
            }
            catch (Exception ee)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                        }
                    }

                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_company();
                            ddlissuercompany.DataTextField = "company_name";
                            ddlissuercompany.DataSource = dt;
                            ddlissuercompany.DataBind();
                            ddlissuercompany.Items.Insert(0, "Select");
                            ddlissuercompany.Items.Insert(1, "All");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_risk_company_master(drpcircle.Text);
                            ddlreceivercompany.DataTextField = "company_name";
                            ddlreceivercompany.DataSource = dt;
                            ddlreceivercompany.DataBind();
                            ddlreceivercompany.Items.Insert(0, "Select");
                            ddlreceivercompany.Items.Insert(1, "All");

                        }
                    }
                    if (Session["role"].ToString() == "5")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_company();
                            ddlissuercompany.DataTextField = "company_name";
                            ddlissuercompany.DataSource = dt;
                            ddlissuercompany.DataBind();
                            ddlissuercompany.Items.Insert(0, "Select");
                            ddlissuercompany.Items.Insert(1, "All");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_risk_company_master(drpcircle.Text);
                            ddlreceivercompany.DataTextField = "company_name";
                            ddlreceivercompany.DataSource = dt;
                            ddlreceivercompany.DataBind();
                            ddlreceivercompany.Items.Insert(0, "Select");
                            ddlreceivercompany.Items.Insert(1, "All");

                        }
                    }

                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_risk_company_master(drpcircle.Text);
        ddlreceivercompany.DataTextField = "company_name";
        ddlreceivercompany.DataSource = dt;
        ddlreceivercompany.DataBind();
        ddlreceivercompany.Items.Insert(0, "Select");
        ddlreceivercompany.Items.Insert(1, "All");
    }

    private string CreateChart_barchart(string fil1, string fil2)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE, strDataProdF, strDataProdH, strDataProdG, strDataProdI, strDataProdJ, strDataProdK, strDataProdL, strDataProdM, strDataProdN, strDataProdO, strDataProdP, strDataProdQ, strDataProdR, strDataProdS;
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_select_chart_reject_status(fil1, fil2);
        if (dt.Rows.Count > 0)
        {
            int sum_count = Convert.ToInt32(dt.Rows[0][20].ToString()) + Convert.ToInt32(dt.Rows[0][21].ToString()) + Convert.ToInt32(dt.Rows[0][22].ToString()) + Convert.ToInt32(dt.Rows[0][23].ToString()) + Convert.ToInt32(dt.Rows[0][24].ToString()) + Convert.ToInt32(dt.Rows[0][25].ToString()) + Convert.ToInt32(dt.Rows[0][26].ToString()) + Convert.ToInt32(dt.Rows[0][27].ToString()) + Convert.ToInt32(dt.Rows[0][28].ToString()) + Convert.ToInt32(dt.Rows[0][29].ToString()) + Convert.ToInt32(dt.Rows[0][30].ToString()) + Convert.ToInt32(dt.Rows[0][31].ToString()) + Convert.ToInt32(dt.Rows[0][32].ToString()) + Convert.ToInt32(dt.Rows[0][33].ToString()) + Convert.ToInt32(dt.Rows[0][34].ToString()) + Convert.ToInt32(dt.Rows[0][35].ToString()) + Convert.ToInt32(dt.Rows[0][36].ToString()) + Convert.ToInt32(dt.Rows[0][37].ToString()) + +Convert.ToInt32(dt.Rows[0][38].ToString());
            if (sum_count > 0)
            {
                strXML = "<graph caption=' PTW Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='' formatNumberScale='0' decimalPrecision='0'>";

                //Initialize <categories> element - necessary to generate a stacked chart
                strCategories = "<categories>";

                //Initiate <dataset> elements
                strDataProdA = "<dataset seriesName='Poor visibility' color='F6BD0F'>";
                strDataProdB = "<dataset seriesName='Extreme Weather' color='AFD8F8'>";
                strDataProdC = "<dataset seriesName='Absence of proper PPE' color='FF0000'>";
                strDataProdD = "<dataset seriesName='Unavailability of proper earthing' color='AFD7F8'>";
                strDataProdE = "<dataset seriesName='No supervisor at site' color='ABD8F8'>";
                strDataProdF = "<dataset seriesName='Site without edge protection' color='EED8F8'>";
                strDataProdG = "<dataset seriesName='Exceeded time line' color='AF67F8'>";
                strDataProdH = "<dataset seriesName='Untrained resources' color='AF00F8'>";
                strDataProdI = "<dataset seriesName='Unfit resources' color='AFD988'>";
                strDataProdJ = "<dataset seriesName='Absence of proper anchorage point' color='808000'>";
                strDataProdK = "<dataset seriesName='No permission from Govt. authority' color='ADD8E6'>";
                strDataProdL = "<dataset seriesName='HT line present vicinity of work area' color='9E1E9E'>";
                strDataProdM = "<dataset seriesName='Lone working' color='4E004E'>";
                strDataProdN = "<dataset seriesName='Safety devices not available' color='800015'>";
                strDataProdO = "<dataset seriesName='Work vicinity active antenna' color='00FF00'>";
                strDataProdP = "<dataset seriesName='Emergency response not present' color='0000FF'>";
                strDataProdQ = "<dataset seriesName='1@risk wearing an implanted medical device (pacemakers and defibrillators)' color='7F7F7F'>";
                strDataProdR = "<dataset seriesName='PTW information inappropriately filled up' color='D9FF00'>";
                strDataProdS = "<dataset seriesName='Others' color='00E1E1'>";

                //Iterate through the data	


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // int sum_count = Convert.ToInt32(dt.Rows[i][11].ToString()) + Convert.ToInt32(dt.Rows[i][12].ToString()) + Convert.ToInt32(dt.Rows[i][13].ToString()) + Convert.ToInt32(dt.Rows[i][14].ToString()) + Convert.ToInt32(dt.Rows[i][15].ToString()) + Convert.ToInt32(dt.Rows[i][16].ToString()) + Convert.ToInt32(dt.Rows[i][17].ToString()) + Convert.ToInt32(dt.Rows[i][18].ToString()) + Convert.ToInt32(dt.Rows[i][19].ToString());
                    int sum_count1 = Convert.ToInt32(dt.Rows[0][20].ToString()) + Convert.ToInt32(dt.Rows[0][21].ToString()) + Convert.ToInt32(dt.Rows[0][22].ToString()) + Convert.ToInt32(dt.Rows[0][23].ToString()) + Convert.ToInt32(dt.Rows[0][24].ToString()) + Convert.ToInt32(dt.Rows[0][25].ToString()) + Convert.ToInt32(dt.Rows[0][26].ToString()) + Convert.ToInt32(dt.Rows[0][27].ToString()) + Convert.ToInt32(dt.Rows[0][28].ToString()) + Convert.ToInt32(dt.Rows[0][29].ToString()) + Convert.ToInt32(dt.Rows[0][30].ToString()) + Convert.ToInt32(dt.Rows[0][31].ToString()) + Convert.ToInt32(dt.Rows[0][32].ToString()) + Convert.ToInt32(dt.Rows[0][33].ToString()) + Convert.ToInt32(dt.Rows[0][34].ToString()) + Convert.ToInt32(dt.Rows[0][35].ToString()) + Convert.ToInt32(dt.Rows[0][36].ToString()) + Convert.ToInt32(dt.Rows[0][37].ToString()) + +Convert.ToInt32(dt.Rows[0][38].ToString());
                    //Append <category name='...' /> to strCategories
                    strCategories +="<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count1 + ")' />";
                    strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][20].ToString()) + "' />";
                    strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][21].ToString()) + "' />";
                    strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][22].ToString()) + "' />";
                    strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][23].ToString()) + "' />";
                    strDataProdE += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][24].ToString()) + "' />";
                    strDataProdF += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][25].ToString()) + "' />";
                    strDataProdG += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][26].ToString()) + "' />";
                    strDataProdH += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][27].ToString()) + "' />";
                    strDataProdI += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][28].ToString()) + "' />";
                    strDataProdJ += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][29].ToString()) + "' />";
                    strDataProdK += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][30].ToString()) + "' />";
                    strDataProdL += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][31].ToString()) + "' />";
                    strDataProdM += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][32].ToString()) + "' />";
                    strDataProdN += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][33].ToString()) + "' />";
                    strDataProdO += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][34].ToString()) + "' />";
                    strDataProdP += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][35].ToString()) + "' />";
                    strDataProdQ += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][36].ToString()) + "' />";
                    strDataProdR += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][37].ToString()) + "' />";
                    strDataProdS += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][38].ToString()) + "' />";

                    //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                    //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
                }


                strCategories += "</categories>";

                //Close <dataset> elements
                strDataProdA += "</dataset>";
                strDataProdB += "</dataset>";
                strDataProdC += "</dataset>";
                strDataProdD += "</dataset>";
                strDataProdE += "</dataset>";
                strDataProdF += "</dataset>";
                strDataProdG += "</dataset>";
                strDataProdH += "</dataset>";
                strDataProdI += "</dataset>";
                strDataProdJ += "</dataset>";
                strDataProdK += "</dataset>";
                strDataProdL += "</dataset>";
                strDataProdM += "</dataset>";
                strDataProdN += "</dataset>";
                strDataProdO += "</dataset>";
                strDataProdP += "</dataset>";
                strDataProdQ += "</dataset>";
                strDataProdR += "</dataset>";
                strDataProdS += "</dataset>";
                //Close <categories> element


                //Assemble the entire XML now
                strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + strDataProdE + strDataProdF + strDataProdG + strDataProdH + strDataProdI + strDataProdJ + strDataProdK + strDataProdL + strDataProdM + strDataProdN + strDataProdO + strDataProdP + strDataProdQ + strDataProdR + strDataProdS + "</graph>";
            }
            else
            {
                strXML = "No Data Found..";
            }
        }
        else
        { strXML = "No Data Found.."; }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales1", countdrp, "400", false, false);

    }
    private string CreateChart_barchart_trend(string fil1, string fil2)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE, strDataProdF, strDataProdH, strDataProdG, strDataProdI, strDataProdJ, strDataProdK, strDataProdL, strDataProdM, strDataProdN, strDataProdO, strDataProdP, strDataProdQ, strDataProdR, strDataProdS;
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_select_chart_reject_status_trend(fil1, fil2);
        if (dt.Rows.Count > 0)
        {
            int sum_count = Convert.ToInt32(dt.Rows[0][20].ToString()) + Convert.ToInt32(dt.Rows[0][21].ToString()) + Convert.ToInt32(dt.Rows[0][22].ToString()) + Convert.ToInt32(dt.Rows[0][23].ToString()) + Convert.ToInt32(dt.Rows[0][24].ToString()) + Convert.ToInt32(dt.Rows[0][25].ToString()) + Convert.ToInt32(dt.Rows[0][26].ToString()) + Convert.ToInt32(dt.Rows[0][27].ToString()) + Convert.ToInt32(dt.Rows[0][28].ToString()) + Convert.ToInt32(dt.Rows[0][29].ToString()) + Convert.ToInt32(dt.Rows[0][30].ToString()) + Convert.ToInt32(dt.Rows[0][31].ToString()) + Convert.ToInt32(dt.Rows[0][32].ToString()) + Convert.ToInt32(dt.Rows[0][33].ToString()) + Convert.ToInt32(dt.Rows[0][34].ToString()) + Convert.ToInt32(dt.Rows[0][35].ToString()) + Convert.ToInt32(dt.Rows[0][36].ToString()) + Convert.ToInt32(dt.Rows[0][37].ToString()) + +Convert.ToInt32(dt.Rows[0][38].ToString());
            if (sum_count > 0)
            {
                strXML = "<graph caption=' PTW Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='' formatNumberScale='0' decimalPrecision='0'>";

                //Initialize <categories> element - necessary to generate a stacked chart
                strCategories = "<categories>";

                //Initiate <dataset> elements
                strDataProdA = "<dataset seriesName='Poor visibility' color='F6BD0F'>";
                strDataProdB = "<dataset seriesName='Extreme Weather' color='AFD8F8'>";
                strDataProdC = "<dataset seriesName='Absence of proper PPE' color='FF0000'>";
                strDataProdD = "<dataset seriesName='Unavailability of proper earthing' color='AFD7F8'>";
                strDataProdE = "<dataset seriesName='No supervisor at site' color='ABD8F8'>";
                strDataProdF = "<dataset seriesName='Site without edge protection' color='EED8F8'>";
                strDataProdG = "<dataset seriesName='Exceeded time line' color='AF67F8'>";
                strDataProdH = "<dataset seriesName='Untrained resources' color='AF00F8'>";
                strDataProdI = "<dataset seriesName='Unfit resources' color='AFD988'>";
                strDataProdJ = "<dataset seriesName='Absence of proper anchorage point' color='808000'>";
                strDataProdK = "<dataset seriesName='No permission from Govt. authority' color='ADD8E6'>";
                strDataProdL = "<dataset seriesName='HT line present vicinity of work area' color='9E1E9E'>";
                strDataProdM = "<dataset seriesName='Lone working' color='4E004E'>";
                strDataProdN = "<dataset seriesName='Safety devices not available' color='800015'>";
                strDataProdO = "<dataset seriesName='Work vicinity active antenna' color='00FF00'>";
                strDataProdP = "<dataset seriesName='Emergency response not present' color='0000FF'>";
                strDataProdQ = "<dataset seriesName='1@risk wearing an implanted medical device (pacemakers and defibrillators)' color='7F7F7F'>";
                strDataProdR = "<dataset seriesName='PTW information inappropriately filled up' color='D9FF00'>";
                strDataProdS = "<dataset seriesName='Others' color='00E1E1'>";
                //Iterate through the data	


                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int sum_count1 = Convert.ToInt32(dt.Rows[0][20].ToString()) + Convert.ToInt32(dt.Rows[0][21].ToString()) + Convert.ToInt32(dt.Rows[0][22].ToString()) + Convert.ToInt32(dt.Rows[0][23].ToString()) + Convert.ToInt32(dt.Rows[0][24].ToString()) + Convert.ToInt32(dt.Rows[0][25].ToString()) + Convert.ToInt32(dt.Rows[0][26].ToString()) + Convert.ToInt32(dt.Rows[0][27].ToString()) + Convert.ToInt32(dt.Rows[0][28].ToString()) + Convert.ToInt32(dt.Rows[0][29].ToString()) + Convert.ToInt32(dt.Rows[0][30].ToString()) + Convert.ToInt32(dt.Rows[0][31].ToString()) + Convert.ToInt32(dt.Rows[0][32].ToString()) + Convert.ToInt32(dt.Rows[0][33].ToString()) + Convert.ToInt32(dt.Rows[0][34].ToString()) + Convert.ToInt32(dt.Rows[0][35].ToString()) + Convert.ToInt32(dt.Rows[0][36].ToString()) + Convert.ToInt32(dt.Rows[0][37].ToString()) + +Convert.ToInt32(dt.Rows[0][38].ToString());
                    //Append <category name='...' /> to strCategories
                    strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count1 + ")' />";
                    strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][20].ToString()) + "' />";
                    strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][21].ToString()) + "' />";
                    strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][22].ToString()) + "' />";
                    strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][23].ToString()) + "' />";
                    strDataProdE += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][24].ToString()) + "' />";
                    strDataProdF += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][25].ToString()) + "' />";
                    strDataProdG += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][26].ToString()) + "' />";
                    strDataProdH += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][27].ToString()) + "' />";
                    strDataProdI += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][28].ToString()) + "' />";
                    strDataProdJ += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][29].ToString()) + "' />";
                    strDataProdK += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][30].ToString()) + "' />";
                    strDataProdL += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][31].ToString()) + "' />";
                    strDataProdM += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][32].ToString()) + "' />";
                    strDataProdN += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][33].ToString()) + "' />";
                    strDataProdO += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][34].ToString()) + "' />";
                    strDataProdP += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][35].ToString()) + "' />";
                    strDataProdQ += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][36].ToString()) + "' />";
                    strDataProdR += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][37].ToString()) + "' />";
                    strDataProdS += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][38].ToString()) + "' />";

                    //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                    //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
                }


                strCategories += "</categories>";

                //Close <dataset> elements
                strDataProdA += "</dataset>";
                strDataProdB += "</dataset>";
                strDataProdC += "</dataset>";
                strDataProdD += "</dataset>";
                strDataProdE += "</dataset>";
                strDataProdF += "</dataset>";
                strDataProdG += "</dataset>";
                strDataProdH += "</dataset>";
                strDataProdI += "</dataset>";
                strDataProdJ += "</dataset>";
                strDataProdK += "</dataset>";
                strDataProdL += "</dataset>";
                strDataProdM += "</dataset>";
                strDataProdN += "</dataset>";
                strDataProdO += "</dataset>";
                strDataProdP += "</dataset>";
                strDataProdQ += "</dataset>";
                strDataProdR += "</dataset>";
                strDataProdS += "</dataset>";
                //Close <categories> element


                //Assemble the entire XML now
                strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + strDataProdE + strDataProdF + strDataProdG + strDataProdH + strDataProdI + strDataProdJ + strDataProdK + strDataProdL + strDataProdM + strDataProdN + strDataProdO + strDataProdP + strDataProdQ + strDataProdR + strDataProdS + "</graph>";
            }
            else
            {
                strXML = "No Data Found..";
            }
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales156", countdrp, "400", false, false);

    }


    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    public string CreateChart_circle()
    {
        string strXML = "";
        try
        {
           // lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();

            dt = da.ptw_chart_counts(" where site_master.circle='" + drpcircle.Text + "'", "", ddlcategory.SelectedValue);

            if (dt.Rows[0][0].ToString() == "")
            {

                strXML = "<graph caption=' Blanket PTW Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='%' decimalPrecision='0' >";

              //  lblmsg.Text = "No Data Found.";
                return strXML;

            }

            int sum_count = Convert.ToInt32(dt.Rows[0][3].ToString()) + Convert.ToInt32(dt.Rows[0][2].ToString());

            strXML = "<graph caption=' Blanket PTW Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " (" + sum_count + ")" + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";
            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Electrical" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";

            strXML += "<set name='" + "Height" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }

        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);

    }
    public string CreateChart(string fil1, string fil2)
    {
        string strXML = "";

        try
        {
           // lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();
            string username = lblusername.Text;
            dt = da.ptw_select_chart_reject_status(fil1, fil2);


            int sum_count = Convert.ToInt32(dt.Rows[0][19].ToString()) + Convert.ToInt32(dt.Rows[0][20].ToString()) + Convert.ToInt32(dt.Rows[0][21].ToString()) + Convert.ToInt32(dt.Rows[0][22].ToString()) + Convert.ToInt32(dt.Rows[0][23].ToString()) + Convert.ToInt32(dt.Rows[0][24].ToString()) + Convert.ToInt32(dt.Rows[0][25].ToString()) + Convert.ToInt32(dt.Rows[0][26].ToString()) + Convert.ToInt32(dt.Rows[0][27].ToString()) + Convert.ToInt32(dt.Rows[0][28].ToString()) + Convert.ToInt32(dt.Rows[0][29].ToString()) + Convert.ToInt32(dt.Rows[0][30].ToString()) + Convert.ToInt32(dt.Rows[0][31].ToString()) + Convert.ToInt32(dt.Rows[0][32].ToString()) + Convert.ToInt32(dt.Rows[0][33].ToString()) + Convert.ToInt32(dt.Rows[0][34].ToString()) + Convert.ToInt32(dt.Rows[0][35].ToString()) + Convert.ToInt32(dt.Rows[0][36].ToString()) + +Convert.ToInt32(dt.Rows[0][37].ToString());

            strXML = "<graph caption=' PTW Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " (" + sum_count + ")" + "'  showPercentageInLabel='1' pieSliceDepth='30'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Poor visibility" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][19].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";

            strXML += "<set name='" + "Extreme Weather" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][20].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";
            strXML += "<set name='" + "Absence of proper PPE" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][21].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' />";
            strXML += "<set name='" + "Unavailability of proper earthing" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][22].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + "' />";

            strXML += "<set name='" + "No supervisor at site" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][23].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][4].ToString()) + "' />";
            strXML += "<set name='" + "Site without edge protection" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][24].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][5].ToString()) + "' />";
            strXML += "<set name='" + "Exceeded time line" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][25].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][6].ToString()) + "' />";

            strXML += "<set name='" + "Untrained resources" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][26].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][7].ToString()) + "' />";
            strXML += "<set name='" + "Unfit resources" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][27].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][8].ToString()) + "' />";

            strXML += "<set name='" + "Absence of proper anchorage point" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][28].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][9].ToString()) + "' />";

            strXML += "<set name='" + "No permission from Govt. authority" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][29].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][10].ToString()) + "' />";
            strXML += "<set name='" + "HT line present vicinity of work area" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][30].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][11].ToString()) + "' />";
            strXML += "<set name='" + "Lone working" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][31].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][12].ToString()) + "' />";

            strXML += "<set name='" + "Safety devices not available" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][32].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][13].ToString()) + "' />";
            strXML += "<set name='" + "Work vicinity active antenna" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][33].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][14].ToString()) + "' />";
            strXML += "<set name='" + "Emergency response not present" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][34].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][15].ToString()) + "' />";

            strXML += "<set name='" + "1@risk wearing an implanted medical device" + " + ("+ SpacialCharRemove.XSS_Remove(dt.Rows[0][35].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][16].ToString()) + "' />";
            strXML += "<set name='" + "PTW information inappropriately filled up" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][36].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][17].ToString()) + "' />";
            strXML += "<set name='" + "Others" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][37].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][18].ToString()) + "' />";

            strXML += "</graph>";
        }

        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }
        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "700", "350", false, false);
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
       // lblmsg.Text = "";
        string message = "";
        string filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and  ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";

        if (drpcircle.SelectedIndex == 1)
        {
            filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";
            message = "Summary of PTW (" + SpacialCharRemove.XSS_Remove(ddlcategory.SelectedItem.Text) + ") issued by " + SpacialCharRemove.XSS_Remove(ddlissuercompany.SelectedValue) + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + SpacialCharRemove.XSS_Remove(ddlissuer.SelectedValue) + " and Receiver Company -" + SpacialCharRemove.XSS_Remove(ddlreceivercompany.SelectedValue) + ")";
            if (ddlcategory.SelectedIndex != 1)
            {
                filter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
            }
            if (ddlissuercompany.SelectedIndex != 1)
            {
                filter += "  and  company_master.company_name='" + ddlissuercompany.SelectedValue + "'";
            }
            if (ddlissuer.SelectedIndex != 1)
            {
                filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
            }
            if (ddlreceivercompany.SelectedIndex != 1)
            {
                filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
            }
        }
        else
        {
            filter = " where site_master.circle='" + drpcircle.SelectedItem.Text.ToString() + "' and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))";
            message = "Summary of PTW (" + SpacialCharRemove.XSS_Remove(ddlcategory.SelectedItem.Text) + ") issued by " + SpacialCharRemove.XSS_Remove(ddlissuercompany.SelectedValue) + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + SpacialCharRemove.XSS_Remove(ddlissuer.SelectedValue) + " and Receiver Company -" + SpacialCharRemove.XSS_Remove(ddlreceivercompany.SelectedValue) + ")";
            if (ddlcategory.SelectedIndex != 1)
            {
                filter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
            }
            if (ddlissuercompany.SelectedIndex != 1)
            {
                filter += "  and  company_master.company_name='" + ddlissuercompany.SelectedValue + "'";
            }
            if (ddlissuer.SelectedIndex != 1)
            {
                filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
            }
            if (ddlreceivercompany.SelectedIndex != 1)
            {
                filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
            }
        }

        panel1.Visible = true;
        panel2.Visible = true;
        panel3.Visible = true;
        lbl1.Text = message;
        lbl2.Text = message;
        lbl3.Text = message;

        FCLiteral.Text = CreateChart(filter, "");
        Literal1.Text = CreateChart_barchart(filter, "All");
        Literal2.Text = CreateChart_barchart_trend(filter, "All");



    }
    protected void ddlissuercompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        da = new mydataaccess1();
        dt = da.select_issuer_from_company_chart(ddlissuercompany.Text, ddlcategory.SelectedValue, drpcircle.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            ddlissuer.DataSource = dt;
            ddlissuer.DataTextField = "username";
            ddlissuer.DataBind();
            ddlissuer.Items.Insert(0, "Select");
            ddlissuer.Items.Insert(1, "All");
        }
        else
        {
            ddlissuer.Items.Clear();

            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('There is no any issuer for selected company');</script>");
        }
    }
}
