﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using mybusiness;
using System.Data;

public partial class AllPage_admin_20daysreminder : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    DataRow row;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int Role = 4;
			 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
            if (Session["role"].ToString() != Role.ToString() && Session["um"].ToString()!=sp_user_)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {
                try
                {

                    //active PTW
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_dashboard_active_ptw_tab();
                    grd20.DataSource = dt;
                    grd20.DataBind();

                    //Extended Approved PTW 
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_dashboard_extended_ptw_tab();
                    grd10.DataSource = dt;
                    grd10.DataBind();

                    //Renewal Approved PTW
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_dashboard_renewal_ptw_tab();
                    grdcritical.DataSource = dt;
                    grdcritical.DataBind();

                    //Pending PTW
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_dashboard_pending_ptw_tab();
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    //Expired PTW
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_dashboard_expired_ptw_tab();
                    GridView2.DataSource = dt;
                    GridView2.DataBind();

                }
                catch
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

                }
            }
        }

    }
    private string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
        string newSortDirection = String.Empty;

        switch (sortDirection)
        {
            case SortDirection.Ascending:
                newSortDirection = "ASC";
                break;

            case SortDirection.Descending:
                newSortDirection = "DESC";
                break;
        }

        return newSortDirection;
    }
    protected void grd20_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grd20.DataSource as DataTable;


        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grd20.DataSource = dataView;
            grd20.DataBind();
        }
    }
    protected void grd10_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grd10.DataSource as DataTable;


        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grd10.DataSource = dataView;
            grd10.DataBind();
        }
    }
    protected void grdcritical_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grdcritical.DataSource as DataTable;

        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grdcritical.DataSource = dataView;
            grdcritical.DataBind();
        }
    }
}
