﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="circleadmin_Punchpoint_Excel_Report_Active.aspx.cs" Inherits="AllPage_pmt_Active_Techician_Assigne" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/circleadmin_menu_ciat.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PTW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/admin_styleinner1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/cufon-yui.js"></script>
    <script type="text/javascript" src="../js/arial.js"></script>
    <script type="text/javascript" src="../js/cuf_run.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <link href="../css/menu.css" rel="stylesheet" type="text/css" />
    <script src="../js/menu.js" type="text/javascript"></script>
    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #roll a {
            display: inline-table;
            text-decoration: none;
        }

        #roll ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

            #roll ul li {
                list-style-type: none;
                background: none;
            }

                #roll ul li a, #roll ul li a:visited {
                    /* styles for the default button state */
                    margin: 0 0 5px 0;
                    padding: 0 15px;
                    line-height: 32px; /* this value must be at least twice the border-radius value */
                    color: #FFF;
                    text-shadow: 0px 0px 3px #000;
                    background: #EAEAEA url(/images/misc/pattern1.gif);
                    font-family: 'Arial Black', Impact, sans-serif;
                    font-size: 16px;
                    text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
                    letter-spacing: -.06em; /* should be set to 0 for most cases */
                    -moz-border-radius: 16px;
                    -khtml-border-radius: 16px;
                    -webkit-border-radius: 16px;
                    border-radius: 16px;
                }

                    #roll ul li a:hover {
                        /* styles for the rollover button state */
                        color: #FFF;
                        text-shadow: 0px 0px 3px #000;
                        background: #0099FF url(/images/misc/pattern2.gif);
                    }

        .style1 {
            width: 100%;
        }

        .style1 {
            width: 762px;
        }

        .style2 {
            width: 269px;
        }

        .style3 {
            width: 269px;
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div class="main">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                        <tr>
                            <td width="6%" rowspan="2">
                                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style1" rowspan="2">
                               <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                |
                            <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change 
                                        Password</asp:LinkButton>
                                |
                            <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td width="25%" style="text-align: right;">
                                <asp:LinkButton ID="lnkchangeproject" runat="server" OnClick="lnkchange_Click" CausesValidation="False"
                                    PostBackUrl="~/login/ciat_ptw.aspx" Visible="False">Change 
                                Project</asp:LinkButton>
                            </td>
                            <td>&nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clr">
                </div>
                <uc2:Menu ID="menubar" runat="server" />
                <div class="clr">
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="content">
                <div class="content_resize">
                    <div class="mainbar">
                        <div class="article1">
                            <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server" Text="" CssClass="lblmarquee"></asp:Label></marquee>
                        </div>
                    </div>
                    <div class="sidebar">
                        <div class="gadget">
                            <table class="style2">
                                <tr>
                                    <td class="style2">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style3">
                                        <asp:DropDownList ID="drpcircle" runat="server" AutoPostBack="True" Width="200px"
                                            CssClass="genall" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                            ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trlblpunchpoint" runat="server">
                                    <td class="style3">
                                        <asp:Label ID="lblpunchpoint" runat="server" Font-Bold="False" Text="Punch Point" CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trddlpunchpoint" runat="server">
                                    <td class="style3">
                                        <asp:DropDownList ID="ddlpunchpoint" runat="server" AutoPostBack="false" Width="200px" CssClass="genall"
                                            OnSelectedIndexChanged="ddlpunchpoint_SelectedIndexChanged">
                                            <asp:ListItem>Select Punch Point</asp:ListItem>
                                            <asp:ListItem Value="all">All</asp:ListItem>
                                            <asp:ListItem Value="external">External</asp:ListItem>
                                            <asp:ListItem Value="internal">Internal</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlpunchpoint"
                                            ErrorMessage="*" InitialValue="Select Provider" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trlblcriticality" runat="server">
                                    <td class="style3">
                                        <asp:Label ID="lblcriticality" runat="server" Font-Bold="False" Text="Criticality" CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trddlcriticality" runat="server">
                                    <td>
                                        <asp:DropDownList ID="ddlcriticality" runat="server" AutoPostBack="true" Width="200px"
                                            CssClass="genall" OnSelectedIndexChanged="ddlcriticality_SelectedIndexChanged">
                                            <asp:ListItem>Select Criticality</asp:ListItem>
                                            <asp:ListItem Value="all">All</asp:ListItem>
                                            <asp:ListItem Value="major">Major</asp:ListItem>
                                            <asp:ListItem Value="minor">Minor</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlcriticality"
                                            ErrorMessage="*" InitialValue="Select Sheet" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>

                                <tr id="trlblsheet" runat="server">
                                    <td class="style3">
                                        <asp:Label ID="lblchecksheet" runat="server" Font-Bold="False" Text="Check Sheet" CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trddlsheet" runat="server">
                                    <td class="style3">
                                        <asp:DropDownList ID="ddlsheet" runat="server" AutoPostBack="true" Width="200px"
                                            CssClass="genall" OnSelectedIndexChanged="ddlsheet_SelectedIndexChanged">
                                            <asp:ListItem Value="0">Select Sheet</asp:ListItem>
                                            <asp:ListItem Value="1">All</asp:ListItem>
                                            <asp:ListItem Value="46">BTS_New</asp:ListItem>
                                            <asp:ListItem Value="47">MW</asp:ListItem>
                                            <asp:ListItem Value="48">TMEA</asp:ListItem>
                                            <asp:ListItem Value="49">BSC</asp:ListItem>
                                            <asp:ListItem Value="50">InfraVisual</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvddlsheet" runat="server" ControlToValidate="ddlsheet"
                                            ErrorMessage="*" InitialValue="Select Sheet" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr id="trlblfrom" runat="server">
                                    <td class="style3">
                                        <asp:Label ID="lblfrom" runat="server" Font-Bold="False" Text="From : " CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trtxtfromdate" runat="server">
                                    <td>
                                        <asp:TextBox ID="txtfromdate" runat="server" CssClass="genall" Width="100px"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtfromdate">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr id="trlblto" runat="server">
                                    <td>
                                        <asp:Label ID="lblto" runat="server" Font-Bold="False" Text="To : " CssClass="lblall"></asp:Label>
                                    </td>
                                </tr>
                                <tr id="trtxttodate" runat="server">
                                    <td>
                                        <asp:TextBox ID="txttodate" runat="server" CssClass="genall" Width="100px" OnClick="txttodate_Click"></asp:TextBox>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txttodate">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        <table class="style2">
                                            <tr>
                                                <td style="text-align: center">
                                                    <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" Text="Search" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center">
                                                    <asp:Label ID="Label3_" runat="server" Font-Names="Calibri" ></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="mainbar">
                        <div class="article">
                            <table width="100%">
                                <tr>
                                    <td width="5%" style="text-align: center">
                                        <asp:Label ID="rptlable" runat="server" Font-Bold="False" CssClass="lblstly"></asp:Label>
                                    </td>
                                    <td width="65%" style="text-align: center">&nbsp;
                                    </td>
                                </tr>

                                <tr>
                                    <td style="vertical-align: top;" width="25%">
                                        <div>
                                            <table class="style2">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                                            OnClick="ImageButton1_Click" ToolTip="Export To Excel" Width="25px" />
                                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="720px" Height="500px">
                                                            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"
                                                                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                                                ForeColor="Black" Width="100%">
                                                                <FooterStyle BackColor="White" />
                                                                <RowStyle HorizontalAlign="Center" />
                                                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                                                <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                                                            </asp:GridView>
                                                            <%--<asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"
                                                                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                                                ForeColor="Black" Width="100%" AutoGenerateColumns="false">
                                                                <FooterStyle BackColor="White" />
                                                                <RowStyle HorizontalAlign="Center" />
                                                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                                                <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                                                            </asp:GridView>--%>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                    <td width="5%">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
            </div>
            <div class="fbg">
                <div class="footer">
                    <div id="Copyright 2011">
                        <a href="http://apycom.com/"></a>
                    </div>
                    <%--    <p class="lf">
                    © Copyright 2012 By.om&quot;&gt;Teleysia Networks Pvt. Ltd.</a></p>--%>
                    <div class="clr">
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
