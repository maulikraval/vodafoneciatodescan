﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;


public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

 		 da = new mydataaccess1();
                 string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                if (Session["flag"].ToString() == "4")
                {
                    lnkchangeproject.Visible = true;
                }
                else
                {
                    lnkchangeproject.Visible = false;
                }

                ImageButton1.Visible = false;

                int i = 2;
                if (Convert.ToInt32(Session["role"].ToString()) == i  && Session["um"].ToString()==sp_user_)
                {

                }
                else
                {

                    if (Session["role"].ToString() == "2")
                    {
                        Response.Redirect("~/PM/Home.aspx");
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        Response.Redirect("~/AllPage/circleadmin_Home.aspx");
                    }
                    if (Session["role"].ToString() == "4")
                    {
                        Response.Redirect("~/AllPage/admin_Default3.aspx");
                    }
                    if (Session["role"].ToString() == "5")
                    {
                        Response.Redirect("~/PM/SiteInfonew.aspx");
                    }
                }
            }
            catch
            {
             //   Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {

            // marque Start
         /*   int count = 0;
            int count1 = 0;
            int count2 = 0;
            da = new mydataaccess1();
            count = da.ptw_dashboard_active_ptw();

            da = new mydataaccess1();
            count1 = da.ptw_dashboard_expire_ptw();
            lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
*/
            da = new mydataaccess1();
            dt = new DataTable();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

            //Marquee End

            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    /*
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = sp_user_d;

                            circle = da.getcirclename();

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                     */
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();

                            ba.User = lblusername.Text;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                        }
                    }

                    if (Session["role"].ToString() == "2")
                    {
                        //da = new mydataaccess1();
                        //DataTable dt = new DataTable();
                        //dt = da.viewpivotrpt();
                        //GridView1.DataSource = dt;
                        //GridView1.DataBind();



                    }
                }
            }
        }
        catch (Exception ex)
        {
       //     Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu;
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        // Response.ClearHeaders();

        //        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=ptw_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            MultiView1.Visible = true;
            if (ddlcategory.SelectedIndex == 0)
            {
                MultiView1.ActiveViewIndex = -1;
            }
            else
            {

                if (ddlcategory.Text == "Date Wise")
                {
                    MultiView1.Visible = false;
                    //MultiView1.ActiveViewIndex = 0;
                }
                if (ddlcategory.Text == "Status Wise")
                {
                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 0;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_getptwstatus();

                    ddlstatus.DataSource = dt;
                    ddlstatus.DataTextField = "discription";
                    ddlstatus.DataValueField = "status";
                    ddlstatus.DataBind();
                    ddlstatus.Items.Insert(0, "Select Status");

                }
                if (ddlcategory.Text == "Receiver Wise")
                {
                    GridView1.Visible = false;
                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 1;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_getreceiverandissuername(drpcircle.SelectedItem.Text.ToString(), 8);

                    ddlreceiver.DataSource = dt;
                    ddlreceiver.DataTextField = "username";
                    ddlreceiver.DataValueField = "id";
                    ddlreceiver.DataBind();
                    ddlreceiver.Items.Insert(0, "Select Permit Receiver");
                }
                if (ddlcategory.Text == "Issuer Wise")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 2;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_getreceiverandissuername(drpcircle.SelectedItem.Text.ToString(), 9);

                    ddlissuer.DataSource = dt;
                    ddlissuer.DataTextField = "username";
                    ddlissuer.DataBind();
                    ddlissuer.Items.Insert(0, "Select Permit Issuer");
                }

                if (ddlcategory.Text == "PTW Type Wise")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 3;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.ptw_selectcategory();

                    ddlptwtype.DataSource = dt;
                    ddlptwtype.DataTextField = "category";
                    ddlptwtype.DataValueField = "ptwcategorymaster_id";
                    ddlptwtype.DataBind();
                    ddlptwtype.Items.Insert(0, "Select PTW Type");
                }

                if (ddlcategory.Text == "Risk Mitigation Dump")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = -1;

                }
                if (ddlcategory.Text == "Company Wise")
                {

                    GridView1.Visible = false;

                    rptlable.Text = "";
                    MultiView1.ActiveViewIndex = 4;

                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.select_company_ptw();

                    ddlcompany.DataSource = dt;
                    ddlcompany.DataTextField = "company_name";
                    ddlcompany.DataBind();
                    ddlcompany.Items.Insert(0, "Select Company");
                }
            }

        }

        catch (Exception ex)
        {
         //   Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }


    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

          //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = -1;
        ddlcategory.SelectedIndex = 0;
        GridView1.DataSource = null;
        GridView1.DataBind();
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        ImageButton1.Visible = true;
        if (TextBox1.Text != "" && TextBox2.Text != "")
        {
            if (drpcircle.SelectedIndex != 0 && ddlcategory.SelectedIndex != 0)
            {
                if (ddlcategory.SelectedIndex == 1 && TextBox1.Text != "" && TextBox2.Text != "")
                {
                    da = new mydataaccess1();
                     //dt = new DataTable();
                        DataSet dt = new DataSet();

                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else
                    {
                        dt = da.select_ptw_Master_report("", "WHERE    (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }

                if (ddlcategory.SelectedIndex == 2 && ddlstatus.SelectedIndex != 0)
                {
                    da = new mydataaccess1();
                      //dt = new DataTable();
                        DataSet dt = new DataSet();

                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where ptw_basic_detail_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else
                    {
                        dt = da.select_ptw_Master_report("", "where ptw_basic_detail_master.status='" + ddlstatus.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                  

                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }
                if (ddlcategory.SelectedIndex == 3 && ddlreceiver.SelectedIndex != 0)
                {
                    da = new mydataaccess1();
                      //dt = new DataTable();
                        DataSet dt = new DataSet();
                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else
                    {
                        dt = da.select_ptw_Master_report("", "where ptw_basic_detail_master.issuer_id='" + ddlreceiver.SelectedValue.ToString() + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                   

                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }
                if (ddlcategory.SelectedIndex == 4 && ddlissuer.SelectedIndex != 0)
                {
                    da = new mydataaccess1();
                      //dt = new DataTable();
                        DataSet dt = new DataSet();
                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else
                    {
                        dt = da.select_ptw_Master_report("", "where ptw_basic_detail_master.approver='" + ddlissuer.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                   

                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }
                if (ddlcategory.SelectedIndex == 5 && ddlptwtype.SelectedIndex != 0 && ddlpurposeofwork.SelectedIndex!=0)
                {

                    da = new mydataaccess1();
                    //dt = new DataTable();
                        DataSet dt = new DataSet();

                    dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) and ptw_basic_detail_master.cat='" + ddlptwtype.SelectedValue.ToString() + "' and ptw_basic_detail_master.purpose_id='" + ddlpurposeofwork.SelectedValue.ToString() + "'");

                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;


                }
                if (ddlcategory.SelectedIndex == 5 && ddlptwtype.SelectedIndex != 0 && ddlpurposeofwork.SelectedIndex == 0)
                {

                    da = new mydataaccess1();
                     //dt = new DataTable();
                        DataSet dt = new DataSet();
                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) and ptw_basic_detail_master.cat='" + ddlptwtype.SelectedValue.ToString() + "' ");
                    }
                    else
                    {
                        dt = da.select_ptw_Master_report("", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) and ptw_basic_detail_master.cat='" + ddlptwtype.SelectedValue.ToString() + "' ");
                    }
                    

                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;


                }

                if (ddlcategory.SelectedIndex == 6)
                {
                    da = new mydataaccess1();
                      dt = new DataTable();
                        //DataSet dt = new DataSet();

                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.ptw_select_risk_mitigation_dump("where site_master.circle='" + drpcircle.Text + "' and ((issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)))");
                    }
                    else
                    {
                        dt = da.ptw_select_risk_mitigation_dump("where  ((issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)))");
                    }
                   


                       Session["dt"] = dt;
                        GridView1.DataSource = dt;
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }

                if (ddlcategory.SelectedIndex == 7 && ddlcompany.SelectedIndex != 0)
                {
                    da = new mydataaccess1();
                    //dt = new DataTable();
                        DataSet dt = new DataSet();

                    if (drpcircle.SelectedIndex != 1)
                    {
                        dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                    else
                    {
                        dt = da.select_ptw_Master_report("", "where issuer_mail_master.company='" + ddlcompany.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                    }
                  

                       Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                    GridView1.DataBind();

                    GridView1.Visible = true;
                }

            }

            if (drpcircle.SelectedIndex != 0 && ddlcategory.SelectedIndex == 0)
            {

                da = new mydataaccess1();
                 //dt = new DataTable();
                        DataSet dt = new DataSet();

                if (drpcircle.SelectedIndex != 1)
                {
                    dt = da.select_ptw_Master_report("where site_master.circle='" + drpcircle.Text + "'", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                }
                else
                {
                    dt = da.select_ptw_Master_report("", "where (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))");
                }
              

                   Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();

                GridView1.Visible = true;
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Dates!!!');</script>");
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.Visible = true;
        }

    }
    protected void btnsitesearch_Click(object sender, EventArgs e)
    {
        da = new mydataaccess1();
          //dt = new DataTable();
                        DataSet dt = new DataSet();

        dt = da.select_ptw_Master_report("", "where site_master.siteid='" + txtsearxh.Text + "'");

           Session["dt"] = dt.Tables[0];
                        GridView1.DataSource = dt.Tables[0];
        GridView1.DataBind();

        GridView1.Visible = true;
    }
    protected void ddlptwtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.ptw_selectpurpose(ddlptwtype.SelectedValue.ToString());

            ddlpurposeofwork.DataSource = dt;
            ddlpurposeofwork.DataTextField = "sub_category";
            ddlpurposeofwork.DataValueField = "ptwsubcategorymaster_id";
            ddlpurposeofwork.DataBind();
            ddlpurposeofwork.Items.Insert(0, "Select Purpose of work");
        }
        catch (Exception ee)
        { }
    }
    protected void ddlpurposeofwork_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

}
