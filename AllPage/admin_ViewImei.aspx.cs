﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using mybusiness;

public partial class AllPage_admin_ViewImei : System.Web.UI.Page
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["role"].ToString() != "4")
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }

                ba = new myvodav2();
                da = new mydataaccess1();
                dt = new DataTable();

                dt = da.selectimeno(ba);
                grd1.DataSource = dt;
                grd1.DataBind();
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        string sp_user_d = "";
        da = new mydataaccess1();
        sp_user_d = da.select_user_cookie(Session["user"].ToString());

        ba = new myvodav2();
        da = new mydataaccess1();
        dt = new DataTable();
      //  ba.Imeiid = Convert.ToInt32(txtid.Text);
        ba.Im =txt1.Text;
        ba.Mob_model = txt2.Text;
        // split for decodation username.
        
        ba.User = sp_user_d;
        da.updateimeino(ba);

        ba = new myvodav2();
        da = new mydataaccess1();
        dt = new DataTable();

        dt = da.selectimeno(ba);
        grd1.DataSource = dt;
        grd1.DataBind();

        Label5.Text = "Updated successfully.....";


    }
    protected void btncancle_Click(object sender, EventArgs e)
    {

    }
    protected void grd1_SelectedIndexChanged(object sender, EventArgs e)
    {

        GridViewRow gr = grd1.SelectedRow;
        txt1.Text = gr.Cells[2].Text;
        txt2.Text = gr.Cells[3].Text;
       // txtid.Text = gr.Cells[2].Text;

        mod1.Show();

    }


    protected void grd1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {

    }
    protected void lb_Click(object sender, EventArgs e)
    {
       
    }
    protected void grd1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (grd1.Rows.Count > 0)
        //{
        //    e.Row.Cells[2].Visible = false;
        //}
        
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lb_Click1(object sender, EventArgs e)
    {
        da = new mydataaccess1();

        LinkButton lb = (LinkButton)sender;
        //   GridViewRow gr = grd1.SelectedRow;
        string s = lb.ToolTip.ToString();
        da.deleteimeino(s);

        dt = new DataTable();
        da = new mydataaccess1();
        ba = new myvodav2();
        dt = da.selectimeno(ba);
        grd1.DataSource = dt;
        grd1.DataBind();
        Label5.Text = "Deleted successfully...";
    }
}
