﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="admin_menu.ascx.cs" Inherits="admin_menu" %>

<div id="menu">
    <ul class="menu">
        <li><a href="#" class="parent"><span>Dashboard</span></a>
            <ul>
                <li><a href="admin_20daysreminder.aspx"><span>CIAT</span></a> </li>
                <li><a href="admin_20daysreminder_ptw.aspx"><span>PTW</span></a> </li>
                <li><a href="admin_20daysreminder_ptw_night.aspx"><span>T@DH</span></a> </li>
                <li><a href="admin_dashboard_active.aspx"><span>ACTIVE</span></a> </li>
                <li><a href="admin_dashboard_tx.aspx"><span>TXEQUIP</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Users</span></a>
            <ul>
                <li><a href="admin_adduser.aspx"><span>Create User</span></a> </li>
                <li><a href="admin_showuser.aspx"><span>View User</span></a> </li>
                <li><a href="admin_UploadRiskPerson.aspx"><span>Upload 1@ Risk</span></a> </li>
                <li><a href="admin_Uploadcompany.aspx"><span>Add Issuer Company</span></a> </li>
                <li><a href="admin_add_risk_company.aspx"><span>Add 1@Risk Company</span></a> </li>
                <li><a href="admin_viewCompanyIssuer.aspx"><span>View Issuer Company</span></a> </li>
                <li><a href="admin_viewCompanyReceiver.aspx"><span>View 1@Risk Company</span></a> </li>
                <li><a href="admin_1_at_risk.aspx"><span>View 1@ Risk</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Circle</span></a>
            <ul>
                <li><a href="admin_addcircle.aspx"><span>Add &amp; View Circle </span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>SiteDetails</span></a>
            <ul>
                <li><a href="admin_UploadSitesNew.aspx"><span>Upload Site</span></a></li>
                <li><a href="admin_UploadSitesNew_tx.aspx"><span>Upload TxSite</span></a></li>
                <li><a href="admin_Viewsite.aspx"><span>View Site</span></a> </li>
                <li><a href="admin_view_site_report.aspx"><span>Site Master</span></a> </li>
                <li><a href="Site_Download.aspx"><span>Site Dump</span></a> </li>
                <li><a href="Site_New_Download.aspx"><span>Sub Site Dump</span></a> </li>
                <li><a href="admin_sites_delete.aspx"><span>Delete Site</span></a> </li>
                <li><a href="admin_approve_lat_long.aspx"><span>Approve Lat/Long</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>IMEI</span></a>
            <ul>
                <li><a href="admin_manual_add_imei.aspx"><span>Add IMEI Manually</span></a></li>
                <li><a href="admin_UploadImei.aspx"><span>Upload IMEI</span></a></li>
                <li><a href="admin_ViewImei.aspx"><span>View Registred Mobile</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Support</span></a>
            <ul>
                <li><a href="admin_finduser.aspx"><span>Find Site</span></a></li>
                <li><a href="LatLongApproval.aspx"><span>Lat-Long Approval</span></a></li>
                <li><a href="LatLongRequestReport.aspx"><span>TT Excel Report</span></a></li>
                <li><a href="FindUser.aspx"><span>Manual Lat-Long Update</span></a></li>
                <li><a href="admin_show_circleadminemail.aspx"><span>Circle-Admin Email List</span></a></li>
                <li><a href="delete1@risk.aspx"><span>Delete 1 At Risk</span></a></li>
                <li><a href="ptw_close.aspx"><span>PTW Close</span></a></li>
                <li><a href="T@DH_close.aspx"><span>T@DH Close</span></a></li>
                <li><a href="NSS_NodeId_Delete.aspx"><span>NODE DELETE</span></a></li>
                <li><a href="view_tt.aspx"><span>View Call TT</span></a></li>
                <li><a href="tt_excel_report.aspx"><span>Call TT Excel Report</span></a></li>
                <li><a href="Active_deactive_User.aspx"><span>User Active</span></a></li>

                
                <li><a href="../upload_user.aspx"><span>User Upload</span></a></li>
                <li><a href="support/UploadIssuer.aspx"><span>Issuer Upload</span></a></li>
                <li><a href="../site_delete.aspx"><span>Site Delete</span></a></li>
                <li><a href="../sitecheck.aspx"><span>Check Site</span></a></li>
                <li><a href="../update_checksheet.aspx"><span>Checksheet Update</span></a></li>
                <li><a href="admin_delete_sitenewmaster.aspx"><span>SiteNewMaster DELETE</span></a></li>
                <li><a href="../site_report.aspx"><span>Site Report</span></a></li>
                <li><a href="support/update_nss.aspx"><span>NSS Update</span></a></li>
                
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Inspections/PTW reports</span></a>
            <ul>
                <li><a href="admin_Circle_Report.aspx"><span>CIAT Latest Report</span></a></li>
                <li><a href="admin_Circle_Report_archive.aspx"><span>CIAT Archive Report</span></a></li>
                <li><a href="admin_Circle_Report_ptw.aspx"><span>PTW Customize Report</span></a></li>
                <li><a href="admin_Circle_Report_ptw_night.aspx"><span>T@DH Customize Report</span></a></li>
                <li><a href="admin_PDF_Report.aspx"><span>PTW PDF Report</span></a></li>
                <li><a href="admin_car_pdf_Report.aspx"><span>CIAT PDF Report</span></a></li>
                <li><a href="admin_car_archieve.aspx"><span>CIAT CAR Archive</span></a></li>
                <li><a href="admin_risk_Report_ptw.aspx"><span>PTW Master Report</span></a></li>
                <li><a href="admin_PDF_Report_night.aspx"><span>PTW T@DH PDF Report</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A"/"P" Reports</span></a>
            <ul>
                <li><a href="admin_pmt_Circle_Report.aspx"><span>g-pm "P" Excel Report</span></a></li>
                <li><a href="admin_pmt_Circle_Report_archive.aspx"><span>g-pm "P" Archive Report</span></a></li>
                <li><a href="admin_pmt_pm_PDF_Report.aspx"><span>g-pm "P" PDF Report</span></a></li>
                <li><a href="admin_pmt_TechnicianAssignReport_.aspx"><span>g-pm "P" Technician Report</span></a></li>
                <li><a href="admin_fps_PDF_Report.aspx"><span>FAS Revalidation Cerificate</span></a></li>



                <li><a href="admin_Active_Circle_Report_new.aspx"><span>g-pm "A" Excel new</span></a></li>
                <li><a href="admin_Active_Circle_Report_archive_new.aspx"><span>g-pm "A" Archive new</span></a></li>
                <li><a href="admin_Active_pm_PDF_Report_new.aspx"><span>g-pm "A" PDF new</span></a></li>
                <li><a href="admin_Active_TechnicianAssignReport_new.aspx"><span>g-pm "A" Technician new</span></a></li>

                <li><a href="admin_Punchpoint_Excel_Report_Active.aspx"><span>g-pm "A" PunchPoint Excel Report</span></a></li>
                <li><a href="admin_Punch_Point_Excel_Report_TXT.aspx"><span>Tx-Equip PunchPoint Excel Report</span></a></li>
                <li><a href="admin_Active_pm_PDF_Report_Checksheet_And_Passive.aspx"><span>g-pm "A"/"P" PunchPoint PDF Report</span></a></li>


                <li><a href="admin_Circle_Report_tx.aspx"><span>TxEquip Excel</span></a></li>
                <li><a href="admin_Circle_Report_archive_tx.aspx"><span>TxEquip Archive</span></a></li>
                <li><a href="admin_tx_pdf_Report.aspx"><span>TxEquip PDF</span></a></li>
                <li><a href="admin_tx_TechnicianAssignReport_new.aspx"><span>TxEquip Technician</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Inspections/PTW Charts</span></a>
            <ul>
                <li><a href="admin_chart.aspx"><span>CIAT Pie Chart</span></a></li>
                <li><a href="admin_bar_chart.aspx"><span>CIAT Bar Chart</span></a></li>
                <li><a href="admin_bar_chart_category.aspx"><span>CIAT Checksheetpoint</span></a></li>
                <li><a href="admin_compli_chart.aspx"><span>CIAT Site Compliance</span></a></li>
                <li><a href="admin_ptw_chart_type.aspx"><span>PTW Type Wise</span></a></li>
                <li><a href="admin_chart_status.aspx"><span>PTW Status Wise</span></a></li>
                <li><a href="admin_chart_risk.aspx"><span>PTW Risk Wise</span></a></li>
                <li><a href="admin_chart_reject_reason.aspx"><span>PTW Rejection Wise</span></a></li>
                <li><a href="admin_ptw_chart_type_night.aspx"><span>T@DH Type</span></a></li>
                <li><a href="admin_chart_status_night.aspx"><span>T@DH Status</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A"/"P" Charts</span></a>
            <ul>
                <li><a href="admin_pmt_PM_Chart.aspx"><span>g-pm "P" ChecksheetWise Chart</span></a></li>
                <li><a href="admin_pmt_PM_Chart_Survey.aspx"><span>g-pm "P" SiteWise Chart</span></a></li>
                <li><a href="admin_pmt_PM_Chart_YesNoNA.aspx"><span>g-pm "P" QuestionWise Chart</span></a></li>
                <li><a href="admin_Active_PM_Chart.aspx"><span>g-pm "A" ChecksheetWise Chart</span></a></li>
                <li><a href="admin_Active_PM_Chart_Survey.aspx"><span>g-pm "A" SiteWise Chart</span></a></li>
                <li><a href="admin_Active_PM_Chart_YesNoNA.aspx"><span>g-pm "A" QuestionWise Chart</span></a></li>
                <li><a href="admin_Active_PM_Chart_new.aspx"><span>g-pm "A" ChecksheetWise New</span></a></li>
                <li><a href="admin_Active_PM_Chart_Survey_new.aspx"><span>g-pm "A" SiteWise New</span></a></li>
                <li><a href="admin_Active_PM_Chart_YesNoNA_new.aspx"><span>g-pm "A" QuestionWise New</span></a></li>
                <li><a href="admin_chart_tx.aspx"><span>Txequip Pie Chart</span></a></li>
                <li><a href="admin_bar_chart_tx.aspx"><span>Txequip Bar Chart</span></a></li>
                <li><a href="admin_Active_PM_Chart_YesNoNA_tx.aspx"><span>Txequip QuestionWise Chart</span></a></li>
                <%--   <li><a href="admin_bar_chart_tx.aspx"><span>Txequip Bar Chart</span></a></li>
                  <li><a href="#"><span>Txequip Checksheetpoint</span></a></li>--%>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Issuer Mapping</span></a>
            <ul>
                <li><a href="admin_all_purpose.aspx"><span>Circle Wise Mapping</span></a></li>
                <li><a href="admin_issuer_purpose.aspx"><span>Manual Mapping</span></a></li>
                <li><a href="admin_issuer_purpose_night.aspx"><span>T@DH Manual Mapping</span></a></li>
            </ul>
        </li>
    </ul>
</div>
