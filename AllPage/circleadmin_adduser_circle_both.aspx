<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master"
    AutoEventWireup="true" CodeFile="circleadmin_adduser_circle_both.aspx.cs" Inherits="admin_adduser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        //function click(){
        $('input[id$=txtpass]').val('');
        $('input[id$=txtrepass]').val('');
        function click1() {
            // alert('hi');
            var rpass = $('input[id$=txtpass]').val();
            var hash = CryptoJS.SHA256(rpass);
            var pass = hash.toString(CryptoJS.enc.Base64);
            $('input[id$=HiddenField1]').val(pass);
            //     alert($('input[id$=HiddenField1]').val());
            // alert(rpass);  
            //alert(pass);       
            var rpass1 = $('input[id$=txtrepass]').val();
            var hash1 = CryptoJS.SHA256(rpass1);
            var pass1 = hash1.toString(CryptoJS.enc.Base64);
            // $('input[id$=txtrepass]').val(pass1);

        }


    </script>
    <script language="javascript" type="text/javascript">
        function ValidateFileUpload(Source, args) {
            var fuData = document.getElementById('<%= SpacialCharRemove.XSS_Remove(FileUpload1.ClientID) %>');
            var FileUploadPath = fuData.value;

            if (FileUploadPath == '') {
                // There is no file selected 
                args.IsValid = true;
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "jpg") {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
    </script>

    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel Width="1030px" ID="pan1" runat="server">
        <table width="100%">
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: center;" colspan="2">
                    <asp:Label ID="Label2" runat="server" CssClass="lblstly" Text="User Creation"></asp:Label>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: right;" colspan="2">
                    <hr />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;"></td>
                <td style="text-align: right; width: 45%;">
                    <asp:Label ID="lblusername" runat="server" Text="User Name :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtuser" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtuser"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars"
                        InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," TargetControlID="txtuser">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="width: 5%;"></td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblpass" runat="server" Text="Password :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtpass" runat="server" Width="150px" CssClass="genall" TextMode="Password" AutoCompleteType="disabled" autoaomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpass"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" InvalidChars="'#&$%^*()-+=!|\/?;:{}[]`~,"
                        FilterMode="InvalidChars" TargetControlID="txtpass">
                    </cc1:FilteredTextBoxExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtpass"
                        Display="None" ErrorMessage=" Password must be between 8 to 19 characters. Password must contain at least one numeric and one Special character and One Capital letter. "
                        SetFocusOnError="True" ValidationExpression="^(?=.*[a-zA-Z])(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RegularExpressionValidator2">
                    </cc1:ValidatorCalloutExtender>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblrepass" runat="server" CssClass="lblall" Text="Re-Password :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtrepass" runat="server" CssClass="genall" TextMode="Password"
                        Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtpass"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="InvalidChars"
                        InvalidChars="'#&amp;$%^*()-+=!|\/?;:{}[]`~," TargetControlID="txtrepass">
                    </cc1:FilteredTextBoxExtender>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtpass"
                        ControlToValidate="txtrepass" CssClass="lblall" ErrorMessage="Password Not Match"></asp:CompareValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtrepass"
                        Display="None" ErrorMessage=" Password must be between 8 to 19 characters. Password must contain at least one numeric and one Special character and One Capital letter. "
                        SetFocusOnError="True" ValidationExpression="^(?=.*[a-zA-Z])(?=.*\d)(?=.*[A-Z])(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$"></asp:RegularExpressionValidator>
                    <cc1:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="RegularExpressionValidator3">
                    </cc1:ValidatorCalloutExtender>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblproject" runat="server" CssClass="lblall" Text="Project :"></asp:Label>

                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlproject" runat="server" CssClass="genall" Width="154px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlproject_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlproject"
                        ErrorMessage="*" InitialValue="Select Project" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblcompany" runat="server" CssClass="lblall" Text="Parent Company :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlcompany" runat="server" CssClass="genall" Width="154px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlcompany_SelectedIndexChanged">
                        <%-- <asp:ListItem>Select Company</asp:ListItem>--%>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlcompany"
                        ErrorMessage="*" InitialValue="Select Company" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblsubcompany" runat="server" CssClass="lblall" Text="Sub Company :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtsubcompany" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtsubcompany"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="InvalidChars"
                        InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," TargetControlID="txtsubcompany">
                    </cc1:FilteredTextBoxExtender>--%>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblrole" runat="server" CssClass="lblall" Text="CIAT Role :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlrole" runat="server" CssClass="genall" Width="154px" AutoPostBack="True" OnSelectedIndexChanged="ddlrole_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblrole0" runat="server" CssClass="lblall" Text="PTW Role :"></asp:Label>
                    &nbsp;<asp:DropDownList ID="ddlrole0" runat="server" CssClass="genall"
                        Width="154px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlrole0_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblcircle" runat="server" Text="Circle :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlcircle" runat="server" CssClass="genall" Width="154px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlcircle_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlcircle"
                        ErrorMessage="*" InitialValue="---Select---" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr id="trcirclelb" runat="server">
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">&nbsp;
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:ListBox ID="lbcircle" runat="server" Width="120px" OnSelectedIndexChanged="lbcircle_SelectedIndexChanged"></asp:ListBox>
                    <asp:Button ID="btnremove" runat="server" CausesValidation="False" OnClick="btnremove_Click"
                        Text="Remove" />
                    <asp:Label ID="lberrorlb" runat="server"></asp:Label>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr id="trzone" runat="server">
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="Label4" runat="server" Text="Zone :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlzone" runat="server" CssClass="genall" Width="154px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlzone_SelectedIndexChanged">
                        <asp:ListItem>-----------Select-----------</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlzone"
                        ErrorMessage="*" InitialValue="---Select---" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr id="trzonelb" runat="server">
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">&nbsp;
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:ListBox ID="lbzone" runat="server" Width="120px"></asp:ListBox>
                    <asp:Button ID="btnzoneremove" runat="server" CausesValidation="False"
                        Text="Remove" OnClick="btnzoneremove_Click" />
                    <asp:Label ID="lbzoneerror" runat="server"></asp:Label>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblemailid" runat="server" Text="Email - ID :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtemailid" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtemailid"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemailid"
                        ErrorMessage="Invalid Email-ID." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblcontactno" runat="server" CssClass="lblall" Text="Contact No :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtcontactno" runat="server" CssClass="genall" Width="150px" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtcontactno"
                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                        ControlToValidate="txtcontactno" ErrorMessage="Enter Only 10 Digit." Display="Dynamic"
                        ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtcontactno"
                        ValidChars="1234567890">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: right; width: 45%;">
                    <asp:Label ID="lblcontactno0" runat="server" CssClass="lblall" Text="IMEI No :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtimei" runat="server" CssClass="genall" MaxLength="20" Width="150px"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtimei"
                        ValidChars="1234567890.">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: right; width: 45%;">
                    <asp:Label ID="Label3" runat="server" CssClass="lblall" Font-Bold="False" Text="Select File To Upload(Sign)"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="genall" Font-Size="Medium" />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btncreate" runat="server" OnClick="btncreate_Click" OnClientClick="click1()"
                        Text="Create" />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td colspan="2" style="text-align: center;">
                    <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
                    <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please select valid .jpg file"></asp:CustomValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
