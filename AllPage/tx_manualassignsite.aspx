﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="tx_manualassignsite.aspx.cs" Inherits="admin_Active_manualassignsite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function ValidateFileUpload(Source, args) {
            var fuData = document.getElementById('<%= SpacialCharRemove.XSS_Remove(FileUpload1.ClientID) %>');
            var FileUploadPath = fuData.value;

            if (FileUploadPath == '') {
                // There is no file selected 
                args.IsValid = false;
            }
            else {
                var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "xls") {
                    args.IsValid = true; // Valid file type
                }
                else {
                    args.IsValid = false; // Not valid file type
                }
            }
        }
    </script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%">
        <tr>
            <td style="text-align: right; width: 45%;">
                <asp:Label ID="Label3" runat="server" CssClass="lblall" Font-Bold="False" Text="Select File To Upload"></asp:Label>
            </td>
            <td style="width: 45%; text-align: left;">
                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="genall" Font-Size="Medium" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right; width: 45%;"></td>
            <td style="width: 45%; text-align: left;">
                <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateFileUpload"
                    ErrorMessage="Please select valid .xls file"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Button ID="btnsubmit" runat="server" OnClick="btnsubmit_Click" Text="Submit" />

            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="lblresult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>
