﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="Active_dashboard.aspx.cs" Inherits="admin_Active_dashboard" EnableEventValidation ="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%">
        <tr>
            <td style="width: 100%; text-align: center;">
                <asp:Label ID="Label1" runat="server" Font-Names="Calibri" Font-Size="20px" ForeColor="#FFA500"
                    Text="Active PM Dashboard"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: left;">
                <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" 
                    Font-Names="calibri" Font-Size="13px" ActiveTabIndex="0">
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Current Schedule" >
                        <ContentTemplate>
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                ToolTip="Export To Excel" Width="25px" OnClick="ImageButton2_Click" />
                                <asp:Panel
                                    ID="PanelCurrent" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                    <asp:GridView ID="grdjobshedulecurrent" runat="server" BackColor="White" BorderColor="#999999"
                                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                        ForeColor="Black" Width="100%" OnSorting="grdjobshedulecurrent_Sorting">
                                        <FooterStyle BackColor="White" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                        <HeaderStyle BackColor="#FFA500" Wrap="True" Font-Size="13px" ForeColor="White" HorizontalAlign="Center" />
                                    </asp:GridView>
                                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Planned schedule">
                        <ContentTemplate>
                             <asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                ToolTip="Export To Excel" Width="25px" OnClick="ImageButton1_Click" />
                                <asp:Panel
                                    ID="Panelplanned" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                    <asp:GridView ID="grdjobsheduleplanned" runat="server" BackColor="White" BorderColor="#999999"
                                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                        ForeColor="Black" Width="100%" OnSorting="grdjobsheduleplanned_Sorting">
                                        <FooterStyle BackColor="White" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                        <HeaderStyle BackColor="#FFA500" Wrap="True" Font-Size="13px" ForeColor="White" HorizontalAlign="Center" />
                                    </asp:GridView>
                                    </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Pending Schedule">
                        <ContentTemplate>
                            <asp:ImageButton ID="ImageButton3" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                OnClick="ImageButton3_Click" ToolTip="Export To Excel" Width="25px" /><asp:Panel
                                    ID="panel2" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                    <asp:GridView ID="grdPendingSchedule" runat="server" BackColor="White" BorderColor="#999999"
                                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                        ForeColor="Black" Width="100%" OnSorting="grdPendingSchedule_Sorting">
                                        <FooterStyle BackColor="White" />
                                        <HeaderStyle BackColor="#FFA500" Wrap="True" Font-Size="13px" ForeColor="White" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                    </asp:GridView>
                                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="Critical site">
                        <ContentTemplate>
                            <asp:ImageButton ID="ImageButton4" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                OnClick="ImageButton4_Click" ToolTip="Export To Excel" Width="25px" /><asp:Panel
                                    ID="panel3" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                    <asp:GridView ID="grdFortnightlyCritical" runat="server" BackColor="White" BorderColor="#999999"
                                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                        ForeColor="Black" Width="100%" OnSorting="grdFortnightlyCritical_Sorting">
                                        <FooterStyle BackColor="White" />
                                        <HeaderStyle BackColor="#FFA500" Wrap="True" Font-Size="13px" ForeColor="White" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                        <RowStyle HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                    </asp:GridView>
                                </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel Visible = "false" ID="TabPanel5" runat="server" >
                        <ContentTemplate>
                            
                                
                        </ContentTemplate>
                    </cc1:TabPanel>
                    
                </cc1:TabContainer>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: center;">
                <asp:ScriptManager runat="server" ID="sc1">
                </asp:ScriptManager>
            </td>
        </tr>
    </table>
</asp:Content>
