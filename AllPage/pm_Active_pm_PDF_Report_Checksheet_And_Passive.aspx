﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pm_Active_pm_PDF_Report_Checksheet_And_Passive.aspx.cs" Inherits="pm_Active_pm_PDF_Report_Checksheet_And_Passive" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/pm_menu.ascx" TagName="Menu" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>HSW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="admin_styleinner1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/cufon-yui.js"></script>

    <script type="text/javascript" src="../js/arial.js"></script>

    <script type="text/javascript" src="../js/cuf_run.js"></script>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <link href="../css/menu.css" rel="stylesheet" type="text/css" />

    <script src="../js/menu.js" type="text/javascript"></script>

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        <! -- #roll a {
            display: inline-block;
            text-decoration: none;
        }

        #roll ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

            #roll ul li {
                list-style-type: none;
                background: none;
            }

                #roll ul li a, #roll ul li a:visited {
                    /* styles for the default button state */
                    margin: 0 0 5px 0;
                    padding: 0 15px;
                    line-height: 32px; /* this value must be at least twice the border-radius value */
                    color: #FFF;
                    text-shadow: 0px 0px 3px #000;
                    background: #EAEAEA url(/images/misc/pattern1.gif);
                    font-family: 'Arial Black', Impact, sans-serif;
                    font-size: 16px;
                    text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
                    letter-spacing: -.06em; /* should be set to 0 for most cases */
                    -moz-border-radius: 16px;
                    -khtml-border-radius: 16px;
                    -webkit-border-radius: 16px;
                    border-radius: 16px;
                }

                    #roll ul li a:hover {
                        /* styles for the rollover button state */
                        color: #FFF;
                        text-shadow: 0px 0px 3px #000;
                        background: #0099FF url(/images/misc/pattern2.gif);
                    }

        -- > .style1 {
            width: 100%;
        }

        .style1 {
            width: 724px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="main">
            <div class="header">
                <div class="header_resize">
                    <div class="logo">
                        <table width="100%">
                            <tr>
                                <td width="6%">
                                    <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                                </td>
                                <td class="style1">
                                  <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                                </td>
                                <td width="25%" style="text-align: right;">
                                    <asp:Label ID="lblusername" runat="server"></asp:Label>
                                    |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change Password</asp:LinkButton>
                                    |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr">
                    </div>
                    <uc2:Menu ID="menubar" runat="server" />

                    <div class="clr">
                    </div>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="content">
                <div class="content_resize">
                    <div class="mainbar">
                        <div class="article1">
                            <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server"  CssClass="lblmarquee"></asp:Label>
                    
                    </marquee>
                        </div>
                    </div>
                    <div class="sidebar">
                        <div class="gadget">
                            <table class="style2">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" ForeColor="Black" Text="SiteId :"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>&nbsp;
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                            ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" Text="Search" ValidationGroup="a" OnClick="btnsearch_Click" CausesValidation="False" />
                                        <br>
                                        <asp:Label ID="Label2" runat="server" CssClass="lblall" ></asp:Label>
                                    </td>
                                </tr>

                           
                            </table>
                           <%-- <div id="checksheet_div" runat="server">
                                <asp:CheckBoxList ID="chk_checksheet" runat="server" OnCheckedChanged="chk_checksheet_CheckedChanged" RepeatLayout="Table">
                                    <asp:ListItem Value="0">All</asp:ListItem>
                                    <asp:ListItem Value="45">TxEquip</asp:ListItem>
                                    <asp:ListItem Value="46">BTS_New</asp:ListItem>
                                    <asp:ListItem Value="47">MW</asp:ListItem>
                                    <asp:ListItem Value="48">TMEA</asp:ListItem>
                                    <asp:ListItem Value="49">BSC</asp:ListItem>
                                    <asp:ListItem Value="50">InfraVisual</asp:ListItem>
                                    <asp:ListItem Value="20">SiteLocation</asp:ListItem>
                                    <asp:ListItem Value="21">HVAC</asp:ListItem>
                                    <asp:ListItem Value="22">X_mer</asp:ListItem>
                                    <asp:ListItem Value="23">EB</asp:ListItem>
                                    <asp:ListItem Value="24">PIU</asp:ListItem>
                                    <asp:ListItem Value="25">SMPS</asp:ListItem>
                                    <asp:ListItem Value="26">BB</asp:ListItem>
                                    <asp:ListItem Value="27">DG</asp:ListItem>
                                    <asp:ListItem Value="28">Earthing</asp:ListItem>
                                    <asp:ListItem Value="29">Shelter</asp:ListItem>
                                    <asp:ListItem Value="30">THCA_PHCA</asp:ListItem>
                                    <asp:ListItem Value="31">FPS</asp:ListItem>
                                    <asp:ListItem Value="32">FSS</asp:ListItem>
                                    <asp:ListItem Value="33">Alarm</asp:ListItem>
                                    <asp:ListItem Value="34">EMF</asp:ListItem>
                                    <asp:ListItem Value="35">General</asp:ListItem>
                                </asp:CheckBoxList>
                            </div>--%>
                            <div id="btn_pdf" runat="server">
                                <%--<asp:Button ID="View_Data" runat="server" Text="View Data" ValidationGroup="a" OnClick="btnView_Data_Click" CausesValidation="False" />--%>
                            </div>
                        </div>
                    </div>
                    <div class="mainbar">
                        <div class="article">
                            <asp:ScriptManager ID="SM1" runat="server">
                            </asp:ScriptManager>
                            <center>
                                <asp:Label ID="Label3" runat="server" CssClass="lblstly">View Site</asp:Label></center>
                            <hr />
                           <%-- <asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                OnClick="ImageButton1_Click" ToolTip="Export To Excel" Width="25px" />--%>
                           
                            <asp:Panel ID="pnlgrd1" Width="730px" ScrollBars="Both" runat="server" Height="450px">
                                <asp:GridView ID="gridview1" runat="server"
                                    BackColor="WhiteSmoke" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px"
                                    CellPadding="4" CellSpacing="2" ForeColor="Black" Width="100%" EmptyDataText="No Details Found..!">
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

                                </asp:GridView>
                                 <asp:Button ID="View_Data" style="margin-left:83%; margin-bottom:1%;" runat="server" Text="Download PDF" ValidationGroup="a"  OnClick="btnView_Data_Click" CausesValidation="False" Visible="false" />

                                <asp:Panel ID="panel1_tbl" runat="server" Width="710px" Height="320px" ForeColor="Black"
                                    BackColor="#E4E4E4" Visible="false">
                                <table id="tbl_datachk" runat="server" width="100%" style="color: Black;background-color: White;border-color: #999999;border-width: 3px;border-style: Solid;text-align:center" 
                                    cellspacing="2" cellpadding="4" rules="all" border="3"  >
                                    <thead>
                                    <tr style="color: White;background-color: #FFA500;font-size: 13px;">
                                        <th style="width:10%" >Select</th>
                                        <th style="width:15%">Checksheet Name</th>
                                        <th style="width:15%">Site ID</th>
                                        <th style="width:15%">Site Name</th>
                                    </tr>
                                        </thead>
                                    <tbody>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox0" Value="0" runat="server" Checked="false" AutoPostBack="true" OnCheckedChanged="CheckBox0_CheckedChanged" /></td>
                                        <td>All</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox1" Value="45" runat="server" name="TxEquip" /></td>
                                        <td>TxEquip</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox2" Value="46" runat="server" name="BTS_New" /></td>
                                        <td>BTS_New</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox3" Value="47" runat="server" name="MW" /></td>
                                        <td>MW</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                     <tr>
                                        <td><asp:CheckBox ID="CheckBox4" Value="48" runat="server" name="TMEA" /></td>
                                        <td>TMEA</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox5" Value="49" runat="server" name="BSC" /></td>
                                        <td>BSC</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox6" Value="50" runat="server" name="InfraVisual" /></td>
                                        <td>InfraVisual</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox7" Value="20" runat="server" name="SiteLocation" /></td>
                                        <td>SiteLocation</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                     <tr>
                                        <td><asp:CheckBox ID="CheckBox8" Value="21" runat="server" name="HVAC" /></td>
                                        <td>HVAC</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox9" Value="22" runat="server" name="X_mer" /></td>
                                        <td>X_mer</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox10" Value="23" runat="server" name="EB" /></td>
                                        <td>EB</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox11" Value="24" runat="server" name="PIU" /></td>
                                        <td>PIU</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                     <tr>
                                        <td><asp:CheckBox ID="CheckBox12" Value="25" runat="server" name="SMPS" /></td>
                                        <td>SMPS</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox13" Value="26" runat="server" name="BB" /></td>
                                        <td>BB</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox14" Value="27" runat="server" name="DG" /></td>
                                        <td>DG</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox15" Value="28" runat="server" name="Earthing" /></td>
                                        <td>Earthing</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox16" Value="29" runat="server" name="Shelter" /></td>
                                        <td>Shelter</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox17" Value="30" runat="server" name="THCA_PHCA" /></td>
                                        <td>THCA_PHCA</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox18" Value="31" runat="server" name="FPS" /></td>
                                        <td>FPS</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox19" Value="32" runat="server" name="FSS" /></td>
                                        <td>FSS</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox20" Value="33" runat="server" name="Alarm" /></td>
                                        <td>Alarm</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox21" Value="34" runat="server"  name="EMF" /></td>
                                        <td>EMF</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><asp:CheckBox ID="CheckBox22" Value="35" runat="server"  name="General" /></td>
                                        <td>General</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                        </tbody>
                                </table>
                                    </asp:Panel>
                                <center>
                                </center>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="clr">
                    </div>
                </div>
                <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                    TargetControlID="panel2" runat="server">
                </cc1:RoundedCornersExtender>
                <asp:Panel ID="panel2" runat="server" Width="950px" Height="320px" ForeColor="Black"
                    BackColor="#E4E4E4">
                    <table width="100%">
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: center" colspan="4">
                                <asp:Label ID="Label4" runat="server" CssClass="lblstly">Edit Site</asp:Label>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td colspan="4" style="text-align: center">
                                <hr />
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">SiteId:
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt1" runat="server" Enabled="False"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="InvalidChars"
                                    InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|=" TargetControlID="txt1">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%">SiteName:
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt2" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txt2"
                                    FilterMode="InvalidChars" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|=">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">Address:
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt3" runat="server"></asp:TextBox>
                            </td>
                            <td style="text-align: right" width="10%">Lat:
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt4" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txt4"
                                    ValidChars="-0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">long
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt5" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txt5"
                                    ValidChars="-0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%">Tower Type
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt6" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                                    TargetControlID="txt6" FilterMode="InvalidChars">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">User Id
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt7" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txt7"
                                    ValidChars="0123456789.">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%">Technician
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt8" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                                    TargetControlID="txt8" FilterMode="InvalidChars">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">Circle
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt9" runat="server" Enabled="False"></asp:TextBox>
                            </td>
                            <td style="text-align: right" width="10%">Zone
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt10" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txt10"
                                    FilterMode="InvalidChars" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|=">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">Subzone
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt11" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                                    TargetControlID="txt11" FilterMode="InvalidChars">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%">TechnicianContact
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt12" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txt12"
                                    ValidChars="0123456789+-">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">Check Sheet
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt13" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="InvalidChars"
                                    InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|=" TargetControlID="txt13">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%">Site Date
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txt14" runat="server"></asp:TextBox>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">Infra Provider Name
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txtipprovider" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="InvalidChars"
                                    InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|=" TargetControlID="txtipprovider">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%">IP ID
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txtipid" runat="server"></asp:TextBox>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">Site Type
                            </td>
                            <td style="text-align: left" width="35%">
                                <asp:TextBox ID="txtsitetype" runat="server"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="InvalidChars"
                                    InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|=" TargetControlID="txtsitetype">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: right" width="10%"></td>
                            <td style="text-align: left" width="35%">
                                <%--<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>--%>
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">&nbsp;
                            </td>
                            <td style="text-align: right" width="35%">
                                <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" Text="Update" />
                                <asp:Button ID="btncancle" runat="server" Text="Cancel" />
                            </td>
                            <td style="text-align: right" width="10%">&nbsp;
                            </td>
                            <td style="text-align: left" width="35%">&nbsp;
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="5%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">&nbsp;
                            </td>
                            <td style="text-align: left" width="35%">&nbsp;
                            </td>
                            <td style="text-align: right" width="10%">&nbsp;
                            </td>
                            <td style="text-align: left" width="35%">&nbsp;
                            </td>
                            <td width="5%">&nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
                <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
                    TargetControlID="butt1" PopupControlID="panel2">
                </cc1:ModalPopupExtender>
            </div>
            <div class="fbg">
                <div class="footer">
                    <div id="Copyright 2011">
                        <a href="http://apycom.com/"></a>
                    </div>
                    <%-- <p class="lf">
                    &copy; Copyright 2011 <a href="#"></a>.</p>
                <p class="rf">
                    Powered By. Teleysia Networks Pvt. Ltd.</p>--%>
                    <div class="clr">
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
