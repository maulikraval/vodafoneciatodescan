﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using mybusiness;

public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                string strPreviousPage = "";
                if (Request.UrlReferrer != null)
                {
                    strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
                }
                if (strPreviousPage == "")
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }

                int i = 4;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }


            try
            {

                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {


                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    da = new mydataaccess1();
                    dt = new DataTable();
                    string sp_user_d = da.select_user_cookie(Session["user"].ToString());
                    lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                 /*   da = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
					*/

                    if (Session["role"].ToString() == "4")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            da = new mydataaccess1();
                            ba = new myvodav2();
                            ba = new myvodav2();
                            DataTable circle = new DataTable();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                           // drpcircle.Items.Insert(1, "All");
                        }
                    }


                }
            }

            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
    }






    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }

    protected void drpcircle_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {





        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }




    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            DataTable zone = new DataTable();
            ba.Circle = drpcircle.SelectedValue;
            zone = da.ptw_select_zone(drpcircle.SelectedValue);

            ddlsheet.DataSource = zone;
            ddlsheet.DataTextField = "zone";
            ddlsheet.DataBind();
            ddlsheet.Items.Insert(0, "select");
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ddlsheet_SelectedIndexChanged1(object sender, EventArgs e)
    {
        if (ddlsheet.SelectedIndex != 0)
        {
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.ptw_select_issuer(ddlsheet.Text);
            ddlcategory0.DataTextField = "username";
            ddlcategory0.DataSource = dt;
            ddlcategory0.DataBind();

            ddlcategory0.Items.Insert(0, "---Select Issuer---");
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cat = 0;
        if (ddlcategory.SelectedIndex != 0)
        {
            if (ddlcategory.SelectedIndex == 1)
            {
                cat = 1;
            }
            if (ddlcategory.SelectedIndex == 2)
            {
                cat = 2;
            }
            if (ddlcategory.SelectedIndex == 3)
            {
                cat = 3;
            }
            if (ddlcategory.SelectedIndex == 4)
            {
                cat = 4;
            }
            if (ddlcategory.SelectedIndex == 5)
            {
                cat = 5;
            }
            dt = new DataTable();
            da = new mydataaccess1();
            dt = da.ptw_select_issuer_purpose(cat);
            GridView1.DataSource = dt;
            GridView1.DataBind();

           // ddlcategory0.SelectedIndex = 0;
        }
    }
    protected void ddlcategory0_SelectedIndexChanged(object sender, EventArgs e)
    {
        int cat = 0;
        if (ddlcategory.SelectedIndex == 1)
        {
            cat = 1;
        }
        if (ddlcategory.SelectedIndex == 2)
        {
            cat = 2;
        }
        if (ddlcategory.SelectedIndex == 3)
        {
            cat = 3;
        }
        if (ddlcategory.SelectedIndex == 4)
        {
            cat = 4;
        }
        if (ddlcategory.SelectedIndex == 5)
        {
            cat = 5;
        }
        dt = new DataTable();
        da = new mydataaccess1();
        dt = da.ptw_select_issuer_purpose(cat);
        GridView1.DataSource = dt;
        GridView1.DataBind();


        DataTable dt1 = new DataTable();
        da = new mydataaccess1();
        dt1 = da.ptw_select_issuer_purpose_data(cat, ddlsheet.Text, ddlcategory0.Text,drpcircle.Text);

        DataTable com_dt = new DataTable();
        da = new mydataaccess1();
        com_dt = da.select_company();
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            DropDownList drp = (DropDownList)GridView1.Rows[i].Cells[3].FindControl("ddlcompany");
            drp.DataSource = com_dt;
            drp.DataTextField = "company_name";
            drp.DataBind();
        }

        if (dt1.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                           

                for (int j = 0; j < dt1.Rows.Count; j++)
                {

                    if (dt.Rows[i][0].ToString() == dt1.Rows[j][0].ToString())
                    {
                        DropDownList drp = (DropDownList)GridView1.Rows[i].Cells[3].FindControl("ddlcompany");
                        CheckBox chk = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("CheckBox1");
                        TextBox txt = (TextBox)GridView1.Rows[i].Cells[2].FindControl("txtesc");
                        chk.Checked = true;
                        txt.Text = SpacialCharRemove.XSS_Remove(dt1.Rows[j][1].ToString());
                        drp.SelectedValue = SpacialCharRemove.XSS_Remove(dt1.Rows[j][2].ToString());


                    }
                }
            }
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string a = "";
        int check = 0;
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            CheckBox chk = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("CheckBox1");
            TextBox txt = (TextBox)GridView1.Rows[i].Cells[2].FindControl("txtesc");
            DropDownList drp = (DropDownList)GridView1.Rows[i].Cells[3].FindControl("ddlcompany");
            if (chk.Checked == false)
            {
                check = 2;
            }
            if (chk.Checked == true)
            {
                check = 1;
            }
            da = new mydataaccess1();
            da.inserissuerdata_manual_loop(drpcircle.Text, ddlsheet.Text, ddlcategory.Text, chk.ToolTip, ddlcategory0.Text, check, txt.Text, drp.SelectedItem.Text);
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
