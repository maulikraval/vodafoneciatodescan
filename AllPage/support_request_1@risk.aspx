﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="support_request_1@risk.aspx.cs" Inherits="AllPage_support_request_1Atrisk" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel Width="1030px" ID="pan1" runat="server">
        <table width="100%">
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: center;" colspan="2">
                    <asp:Label ID="Label2" runat="server" CssClass="lblstly" Text="1@Risk Request"></asp:Label>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: right;" colspan="2">
                    <hr />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lbltrainigtype" runat="server" CssClass="lblall" Text="Training Type :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddltrainigtype" runat="server" CssClass="genall" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddltrainigtype_SelectedIndexChanged">
                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                        <asp:ListItem Value="1">HSW training on Height CBTHR/BTHR/ATHR</asp:ListItem>
                        <asp:ListItem Value="2">HSW training on Electrical Safety</asp:ListItem>
                        <asp:ListItem Value="3">HSW training on OFC Safety</asp:ListItem>
                        <asp:ListItem Value="4">HSW training on RF & EMF safety</asp:ListItem>
                        <asp:ListItem Value="5">Defenssive Driving - 2W</asp:ListItem>
                        <asp:ListItem Value="6">Defenssive Driving - 4W</asp:ListItem>
                        <asp:ListItem Value="7">CBTHR</asp:ListItem>
                        <asp:ListItem Value="8">FARM ToCli</asp:ListItem>
                        <asp:ListItem Value="9">FARM Refresher</asp:ListItem>
                        <asp:ListItem Value="10">FARM TowEr</asp:ListItem>
                        <asp:ListItem Value="11">FARM OffRe</asp:ListItem>
                        <asp:ListItem Value="12">FARM ScaCon</asp:ListItem>
                    </asp:DropDownList> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddltrainigtype"
                        ErrorMessage="*" InitialValue="--Select--" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>      
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblcircle" runat="server" Text="Circle :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlcircle" runat="server" CssClass="genall" Width="150px" AutoPostBack="True"
                        CausesValidation="true">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlcircle"
                        ErrorMessage="*" InitialValue="--Select--" SetFocusOnError="True" Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblrepass" runat="server" CssClass="lblall" Text="Function :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtfunction" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtfunction"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblpass" runat="server" Text="Sub-function :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtsfunction" runat="server" Width="150px" CssClass="genall" 
                        AutoCompleteType="disabled" autoaomplete="off" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtsfunction"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;"></td>
                <td style="text-align: right; width: 45%">
                    <asp:Label ID="lbldriver" runat="server" Text="Name :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtdriver" runat="server" CssClass="genall" Width="150px" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtdriver"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="InvalidChars"
                        InvalidChars="'#&amp;$%^*()-+=!|\@/?;:{}[]`~," TargetControlID="txtdriver">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="width: 5%;"></td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: right" width="45%">
                    <asp:Label ID="lblbdate" runat="server" Font-Bold="False" Text="Birthdate : " CssClass="lblall"></asp:Label>
                </td>
                <td  style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtbdate" runat="server" CssClass="genall" Width="150px" CausesValidation="true"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtbdate">
                    </cc1:CalendarExtender>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtbdate"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator> 
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblcompany" runat="server" CssClass="lblall" Text="Compnay Name :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtcompany" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcompany"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblcontactno" runat="server" CssClass="lblall" Text="Contact No. :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtcontactno" runat="server" CssClass="genall" Width="150px" MaxLength="10" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtcontactno"
                        Display="Static" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcontactno"
                        ErrorMessage="Enter Only 10 Digit." Display="Dynamic" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtcontactno"
                        ValidChars="1234567890">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblspno" runat="server" CssClass="lblall" Text="Safety Passport No. :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtspno" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtspno"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblempdetails" runat="server" Text="Employment Details :" CssClass="lblall"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlempdetails" runat="server" CssClass="genall" Width="150px" AutoPostBack="True"
                        CausesValidation="true">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>Contractual</asp:ListItem>
                        <asp:ListItem>On-role</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlempdetails"
                        ErrorMessage="*" InitialValue="--Select--" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblqualification" runat="server" CssClass="lblall" Text="Qualification :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlqualification" runat="server" CssClass="genall" Width="150px" AutoPostBack="True"
                        Enabled="false">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtqualification" runat="server" CssClass="genall"
                        Width="150px" placeholder="Enter Qualification"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtqualification"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblexp" runat="server" CssClass="lblall" Text="Experience (In Year) :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtexp" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtexp"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidater1" ControlToValidate="txtexp" MinimumValue="1" MaximumValue="35" Type="Integer" 
                    ErrorMessage="*" runat="server" />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lbllicenceno" runat="server" CssClass="lblall" Text="Licence No. :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtlicenceno" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtlicenceno"
                        ErrorMessage="*" SetFocusOnError="true"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lbllicenceval" runat="server" CssClass="lblall" Text="Licence Validity (In Year) :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txtlicvalyear" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtlicvalyear"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblyesno" runat="server" CssClass="lblall" Text="Vodafone Induction Training Taken? :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlyesno" runat="server" CssClass="genall" Width="150px" AutoPostBack="True"
                        CausesValidation="true">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="ddlyesno"
                        ErrorMessage="*" InitialValue="--Select--" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator> 
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="text-align: right" width="45%">
                    <asp:Label ID="lbltrainingdate" runat="server" Font-Bold="False" Text="Training Date : " CssClass="lblall"></asp:Label>
                </td>
                <td  style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txttrainingdate" runat="server" CssClass="genall" Width="150px" CausesValidation="true"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txttrainingdate">
                    </cc1:CalendarExtender>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txttrainingdate"
                        ErrorMessage="*" SetFocusOnError="True" Display="Static"></asp:RequiredFieldValidator> 
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lbltrainedcompany" runat="server" CssClass="lblall" Text="Trained By Company Name :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddltrainedcompany" runat="server" CssClass="genall" Width="150px" AutoPostBack="True">
                    </asp:DropDownList>    
                    <asp:TextBox ID="txtothercompany" runat="server" CssClass="genall"
                        Width="150px" Visible="false" placeholder="Enter Trained Company"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtothercompany"
                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lbltcertino" runat="server" CssClass="lblall" Text="Training Certificate No. :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txttcertino" runat="server" CssClass="genall"
                        Width="150px" CausesValidation="true"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txttcertino"
                        ErrorMessage="*" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr> 

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lbltrainingvaldate" runat="server" CssClass="lblall" Text="Training Validity (In Year/Date) :"></asp:Label>
                </td>
                <td  style="width: 45%; text-align: left;">
                    <asp:TextBox ID="txttrainingvaldate" runat="server" CssClass="genall" Width="150px" CausesValidation="true"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txttrainingvaldate">
                    </cc1:CalendarExtender>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ControlToValidate="txttrainingvaldate"
                        ErrorMessage="*" SetFocusOnError="True" Display="Static"></asp:RequiredFieldValidator> 
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblpcompany" runat="server" CssClass="lblall" Text="Parent Company :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlpcompany" runat="server" CssClass="genall" Width="150px" AutoPostBack="True"
                        CausesValidation="true">
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="ddlpcompany"
                        ErrorMessage="*" InitialValue="--Select--" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator> 
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td style="width: 45%; text-align: right;">
                    <asp:Label ID="lblmedfit" runat="server" CssClass="lblall" Text="Medical Fitness :"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:DropDownList ID="ddlmedfit" runat="server" CssClass="genall" Width="150px" AutoPostBack="True"
                        CausesValidation="true">
                        <asp:ListItem>--Select--</asp:ListItem>
                        <asp:ListItem>Yes</asp:ListItem>
                        <asp:ListItem>No</asp:ListItem>
                    </asp:DropDownList>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ControlToValidate="ddlmedfit"
                        ErrorMessage="*" InitialValue="--Select--" SetFocusOnError="True"  Display="Static"></asp:RequiredFieldValidator> 
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">
                    &nbsp;
                </td>
                <td style="text-align: right; width: 45%;">
                    <asp:Label ID="Label4" runat="server" CssClass="lblall" Font-Bold="False" Text="Select Medical Fitness File To Upload(Sign)"></asp:Label>
                </td>
                <td style="width: 45%; text-align: left;">
                    <asp:FileUpload ID="FileUpload1" runat="server" CssClass="genall" Font-Size="Medium" />
                </td>
                <td style="width: 5%;">
                    &nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btncreate" runat="server" OnClick="btncreate_Click" OnClientClick="click1()"
                        ValidationGroup="a" Text="Create" />
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>

            <tr>
                <td style="width: 5%;">&nbsp;
                </td>
                <td colspan="2" style="text-align: center;">
                    <asp:Label ID="Label1" runat="server" Text="Label" Visible="False" ></asp:Label>
                     <%--<asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateFileUpload"
                        ErrorMessage="Please select valid .jpg file" ControlToValidate="FileUpload1"></asp:CustomValidator>--%>
                </td>
                <td style="width: 5%;">&nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
