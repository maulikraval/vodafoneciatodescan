﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;
using InfoSoftGlobal;


public partial class PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;

    protected void Page_Load(object sender, EventArgs e)
    {
        // lblmsg.Text = "";
        //FCLiteral.Text = CreateChart();
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        panel1.Visible = false;
        panel2.Visible = false;
        panel3.Visible = false;

        if (!IsPostBack)
        {
            if (Session["flag"].ToString() == "4")
            {
                lnkchangeproject.Visible = true;
            }
            else
            {
                lnkchangeproject.Visible = false;
            }

            try
            {
                int i = 3;

                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                da = new mydataaccess1();
                dt = new DataTable();
                string sp_user_d = da.select_user_cookie(Session["user"].ToString());
                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                /*da = new mydataaccess1();
                count = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                da = new mydataaccess1();
                count1 = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);

                lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
*/
            }
            catch (Exception ee)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();

                            DataTable circle = new DataTable();
                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            ba = new myvodav2();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
                            if (sp_user_d == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_company();
                            ddlissuercompany.DataTextField = "company_name";
                            ddlissuercompany.DataSource = dt;
                            ddlissuercompany.DataBind();
                            ddlissuercompany.Items.Insert(0, "Select");
                            ddlissuercompany.Items.Insert(1, "All");

                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.select_risk_company_master(drpcircle.Text);
                            ddlreceivercompany.DataTextField = "company_name";
                            ddlreceivercompany.DataSource = dt;
                            ddlreceivercompany.DataBind();
                            ddlreceivercompany.Items.Insert(0, "Select");
                            ddlreceivercompany.Items.Insert(1, "All");
                        }
                    }

                    if (Session["role"].ToString() == "4")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                        }
                    }

                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_risk_company_master(drpcircle.Text);
        ddlreceivercompany.DataTextField = "company_name";
        ddlreceivercompany.DataSource = dt;
        ddlreceivercompany.DataBind();
        ddlreceivercompany.Items.Insert(0, "Select");
        ddlreceivercompany.Items.Insert(1, "All");
    }



    private string CreateChart_barchart(string fil1, string fil2, string cat)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE;

        strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='' rotateNames='1' formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Height' color='F6BD0F'>";
        strDataProdB = "<dataset seriesName='Electrical' color='AFD8F8'>";
        strDataProdC = "<dataset seriesName='OFC' color='8EB229'>";
        strDataProdD = "<dataset seriesName='RF Energy' color='FD6035'>";
        strDataProdE = "<dataset seriesName='T@DH' color='FD60FF'>";
        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_counts(fil1, fil2, cat);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {


                //Append <category name='...' /> to strCategories

                
                    int sum_count = Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString()) + Convert.ToInt32(dt.Rows[i][7].ToString()) + Convert.ToInt32(dt.Rows[i][8].ToString());
                    strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count + ")' />";
                    strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                    strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                    strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][7].ToString()) + "' />";
                    strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][8].ToString()) + "' />";
                   // strDataProdE += "<set value='" + dt.Rows[i][10].ToString() + "' />";
               
               
                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";
            strDataProdD += "</dataset>";
            // strDataProdE += "</dataset>";
            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales1", countdrp, "400", false, false);

    }
    private string CreateChart_barchart_trend(string fil1, string fil2, string cat)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE;

        strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix=''  formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Height' color='F6BD0F'>";
        strDataProdB = "<dataset seriesName='Electrical' color='AFD8F8'>";
        strDataProdC = "<dataset seriesName='OFC' color='8EB229'>";
        strDataProdD = "<dataset seriesName='RF Energy' color='FD6035'>";
        strDataProdE = "<dataset seriesName='T@DH' color='FD60FF'>";


        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_counts_blanket_trend(fil1, fil2, cat);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][4].ToString()) + Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString()) + Convert.ToInt32(dt.Rows[i][7].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][8].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][4].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][7].ToString()) + "' />";


                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";
            strDataProdD += "</dataset>";
            //strDataProdE += "</dataset>";
            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales", countdrp, "400", false, false);

    }
    private string CreateChart_barchart_trend_indi(string fil1, string fil2, string cat)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE;

        strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix=''  formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Height' color='F6BD0F'>";
        strDataProdB = "<dataset seriesName='Electrical' color='AFD8F8'>";
        strDataProdC = "<dataset seriesName='OFC' color='8EB229'>";
        strDataProdD = "<dataset seriesName='RF Energy' color='FD6035'>";
        strDataProdE = "<dataset seriesName='T@DH' color='FD60FF'>";
        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_counts_person_work_trend(fil1, fil2, cat);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][4].ToString()) + Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString()) + Convert.ToInt32(dt.Rows[i][7].ToString());

                //Append <category name='...' /> to strCategories

                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][8].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][4].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][7].ToString()) + "' />";
              //  strDataProdE += "<set value='" + dt.Rows[i][9].ToString() + "' />";


                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";
            strDataProdD += "</dataset>";
            //strDataProdE += "</dataset>";
            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales789", countdrp, "400", false, false);

    }
    private string CreateChart_barchart_indi(string fil1, string fil2, string cat)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE;

        strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='' rotateNames='1' formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Height' color='F6BD0F'>";
        strDataProdB = "<dataset seriesName='Electrical' color='AFD8F8'>";
        strDataProdC = "<dataset seriesName='OFC' color='8EB229'>";
        strDataProdD = "<dataset seriesName='RF Energy' color='FD6035'>";
        strDataProdE = "<dataset seriesName='T@DH' color='FD60FF'>";

        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_counts_person_work(fil1, fil2, cat);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString()) + Convert.ToInt32(dt.Rows[i][7].ToString()) + Convert.ToInt32(dt.Rows[i][8].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][7].ToString()) + "' />";
                strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][8].ToString()) + "' />";
                //strDataProdE += "<set value='" + dt.Rows[i][10].ToString() + "' />";

                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";
            strDataProdD += "</dataset>";
            // strDataProdE += "</dataset>";
            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales123", countdrp, "400", false, false);

    }

    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    public string CreateChart_circle()
    {
        string strXML = "";
        try
        {
            // lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();

            dt = da.ptw_chart_counts(" where site_master.circle='" + drpcircle.Text + "'", "", ddlcategory.SelectedValue);

            if (dt.Rows[0][0].ToString() == "")
            {

                strXML = "<graph caption='Blanket PTW Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='%' decimalPrecision='0' >";

                // lblmsg.Text = "No Data Found.";
                return strXML;

            }

            strXML = "<graph caption='Blanket PTW Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";
            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Electrical" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";

            strXML += "<set name='" + "Height" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }
        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);

    }
    public string CreateChart(string fil1, string fil2, string cat)
    {
        string strXML = "";
        try
        {
            //lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();
            string username = lblusername.Text;
            dt = da.ptw_chart_counts(fil1, fil2, cat);


            int sum_count = Convert.ToInt32(dt.Rows[0][4].ToString()) + Convert.ToInt32(dt.Rows[0][5].ToString()) + Convert.ToInt32(dt.Rows[0][6].ToString()) + Convert.ToInt32(dt.Rows[0][7].ToString());

            strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " (" + sum_count + ")" + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Electrical" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][5].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";

            strXML += "<set name='" + "Height" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][4].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";

            strXML += "<set name='" + "OFC" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][6].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' />";

            strXML += "<set name='" + "RF Energy" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][7].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + "' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }
        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);
    }
    public string CreateChart_indi(string fil1, string fil2, string cat)
    {
        string strXML = "";
        try
        {
            // lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();
            string username = lblusername.Text;
            dt = da.ptw_chart_counts_person_work(fil1, fil2, cat);



            int sum_count = Convert.ToInt32(dt.Rows[0][4].ToString()) + Convert.ToInt32(dt.Rows[0][5].ToString()) + Convert.ToInt32(dt.Rows[0][6].ToString()) + Convert.ToInt32(dt.Rows[0][7].ToString());

            strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " (" + sum_count + ")" + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Electrical" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][5].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";

            strXML += "<set name='" + "Height" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][4].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";

            strXML += "<set name='" + "OFC" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][6].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' />";

            strXML += "<set name='" + "RF Energy" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][7].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + "' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }

        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);



    }


    //protected void btnsubmit_Click(object sender, EventArgs e)
    //{
    //   // lblmsg.Text = "";

    //    string filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";
    //    string message = "";

    //    if (drpcircle.SelectedIndex == 1)
    //    {
    //        message = "Summary of PTW (" + ddlcategory.SelectedItem.Text + ") issued by " + ddlissuercompany.SelectedValue + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + ddlissuer.SelectedValue + " and Receiver Company -" + ddlreceivercompany.SelectedValue + ")";

    //        if (ddlcategory.SelectedIndex != 1)
    //        {
    //            filter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
    //        }
    //        if (ddlissuercompany.SelectedIndex != 1)
    //        {
    //            filter += "  and  company_master.company_name='" + ddlissuercompany.SelectedValue + "'";
    //        }
    //        if (ddlissuer.SelectedIndex != 1)
    //        {
    //            filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
    //        }
    //        if (ddlreceivercompany.SelectedIndex != 1)
    //        {
    //            filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
    //        }
    //    }
    //    else
    //    {
    //        filter = " where site_master.circle='" + drpcircle.SelectedItem.Text.ToString() + "' and (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";
    //        message = "Summary of PTW (" + ddlcategory.SelectedItem.Text + ") issued by " + ddlissuercompany.SelectedValue + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + ddlissuer.SelectedValue + " and Receiver Company -" + ddlreceivercompany.SelectedValue + ")";
    //        if (ddlcategory.SelectedIndex != 1)
    //        {
    //            filter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
    //        }
    //        if (ddlissuercompany.SelectedIndex != 1)
    //        {
    //            filter += "  and  company_master.company_name='" + ddlissuercompany.SelectedValue + "'";
    //        }
    //        if (ddlissuer.SelectedIndex != 1)
    //        {
    //            filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
    //        }
    //        if (ddlreceivercompany.SelectedIndex != 1)
    //        {
    //            filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
    //        }
    //    }

    //    if (ddlcounttype.SelectedIndex == 1 && TextBox1.Text != "" && TextBox2.Text != "")
    //    {
    //        panel1.Visible = true;
    //        panel2.Visible = true;
    //        panel3.Visible = true;
    //        lbl1.Text = message;
    //        lbl2.Text = message;
    //        lbl3.Text = message;

    //        FCLiteral.Text = CreateChart(filter, "",ddlcategory.SelectedValue);
    //        Literal1.Text = CreateChart_barchart(filter, "All",ddlcategory.SelectedValue);
    //        Literal2.Text = CreateChart_barchart_trend(filter, "All", ddlcategory.SelectedValue);
    //    }
    //    else
    //    {
    //        panel1.Visible = true;
    //        panel2.Visible = true;
    //        panel3.Visible = true;
    //        lbl1.Text = message;
    //        lbl2.Text = message;
    //        lbl3.Text = message;

    //        filter = filter.Replace("ptw_basic_detail_master.status", "ptw_extension_master.status");
    //        FCLiteral.Text = CreateChart_indi(filter, "",ddlcategory.SelectedValue);
    //        Literal1.Text = CreateChart_barchart_indi(filter, "All", ddlcategory.SelectedValue);
    //        Literal2.Text = CreateChart_barchart_trend_indi(filter, "All", ddlcategory.SelectedValue);
    //    }
    //    LogWriter log = new LogWriter();
    //}

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        // lblmsg.Text = "";

        // string filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";

        string filter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";
        string datefilter = " where (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";
        string circlefilter = "";
        string message = "";

        if (drpcircle.SelectedIndex == 1)
        {
            message = "Summary of PTW (" + SpacialCharRemove.XSS_Remove(ddlcategory.SelectedItem.Text) + ") issued by " + SpacialCharRemove.XSS_Remove(ddlissuercompany.SelectedValue) + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + SpacialCharRemove.XSS_Remove(ddlissuer.SelectedValue) + " and Receiver Company -" + SpacialCharRemove.XSS_Remove(ddlreceivercompany.SelectedValue) + ")";
            circlefilter += "where";
            if (ddlcategory.SelectedIndex != 1)
            {
                filter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
                circlefilter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
            }
            if (ddlissuercompany.SelectedIndex != 1)
            {
                filter += " and  issuer_mail_master.company='" + ddlissuercompany.SelectedValue + "'";
                circlefilter += "  and  issuer_mail_master.company='" + ddlissuercompany.SelectedValue + "'";
            }
            if (ddlissuer.SelectedIndex != 1)
            {
                filter += " and  user_master.username='" + ddlissuer.SelectedValue + "'";
                circlefilter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
            }
            if (ddlreceivercompany.SelectedIndex != 1)
            {
                filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
                circlefilter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
            }
        }
        else
        {
            filter = " where site_master.circle='" + drpcircle.SelectedItem.Text.ToString() + "' and (ptw_basic_detail_master.issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102)) and ptw_basic_detail_master.status!=0 and ptw_basic_detail_master.status!=1 and ptw_basic_detail_master.status!=4 and ptw_basic_detail_master.status!=5 and (ptw_basic_detail_master.issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ";
            message = "Summary of PTW (" + SpacialCharRemove.XSS_Remove(ddlcategory.SelectedItem.Text) + ") issued by " + SpacialCharRemove.XSS_Remove(ddlissuercompany.SelectedValue) + " from " + Convert.ToDateTime(TextBox1.Text).ToString("dd-MM-yyyy") + " to " + Convert.ToDateTime(TextBox2.Text).ToString("dd-MM-yyyy") + "(Issuer-" + SpacialCharRemove.XSS_Remove(ddlissuer.SelectedValue) + " and Receiver Company -" + SpacialCharRemove.XSS_Remove(ddlreceivercompany.SelectedValue) + ")";
            circlefilter += "where site_master.circle in ('" + drpcircle.SelectedItem.Text.ToString() + "')";
            if (ddlcategory.SelectedIndex != 1)
            {
                filter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
                circlefilter += " and ptw_basic_detail_master.cat=" + ddlcategory.SelectedValue + "";
            }
            if (ddlissuercompany.SelectedIndex != 1)
            {
                filter += "  and  issuer_mail_master.company='" + ddlissuercompany.SelectedValue + "'";
                circlefilter += "  and  issuer_mail_master.company='" + ddlissuercompany.SelectedValue + "'";
            }
            if (ddlissuer.SelectedIndex != 1)
            {
                filter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
                circlefilter += "  and  user_master.username='" + ddlissuer.SelectedValue + "'";
            }
            if (ddlreceivercompany.SelectedIndex != 1)
            {
                filter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
                circlefilter += " and risk_company_master.company_name='" + ddlreceivercompany.SelectedValue + "'";
            }
        }

        if (ddlcounttype.SelectedIndex == 1 && TextBox1.Text != "" && TextBox2.Text != "")
        {
            panel1.Visible = true;
            panel2.Visible = true;
            panel3.Visible = true;
            lbl1.Text = message;
            lbl2.Text = message;
            lbl3.Text = message;

            FCLiteral.Text = CreateChart(filter, "", ddlcategory.SelectedValue);
            Literal1.Text = CreateChart_barchart(filter, "All", ddlcategory.SelectedValue);
            Literal2.Text = CreateChart_barchart_trend(filter, "All", ddlcategory.SelectedValue);
        }
        else
        {
            panel1.Visible = true;
            panel2.Visible = true;
            panel3.Visible = true;
            lbl1.Text = message;
            lbl2.Text = message;
            lbl3.Text = message;

            filter = filter.Replace("ptw_basic_detail_master.status", "ptw_extension_master.status");
            //FCLiteral.Text = CreateChart_indi(filter, "",ddlcategory.SelectedValue);
            //bool check = circlefilter.Contains("where and");
            if (circlefilter.Contains("where and"))
            {
                circlefilter = circlefilter.Replace("where and", "where");
                FCLiteral.Text = CreateChart_indi_new(datefilter, circlefilter, "", ddlcategory.SelectedValue);
                Literal1.Text = CreateChart_barchart_indi_new(datefilter, circlefilter, "All", ddlcategory.SelectedValue);
                Literal2.Text = CreateChart_barchart_trend_indi_new(datefilter, circlefilter, "All", ddlcategory.SelectedValue);
            }
            else
            {
                FCLiteral.Text = CreateChart_indi_new(datefilter, circlefilter, "", ddlcategory.SelectedValue);
                Literal1.Text = CreateChart_barchart_indi_new(datefilter, circlefilter, "All", ddlcategory.SelectedValue);
                Literal2.Text = CreateChart_barchart_trend_indi_new(datefilter, circlefilter, "All", ddlcategory.SelectedValue);
            }
            //FCLiteral.Text = CreateChart_indi_new(datefilter,circlefilter, "", ddlcategory.SelectedValue);
            //Literal1.Text = CreateChart_barchart_indi(filter, "All", ddlcategory.SelectedValue);
            //Literal2.Text = CreateChart_barchart_trend_indi(filter, "All", ddlcategory.SelectedValue);
        }

    }

    public string CreateChart_indi_new(string datefil, string cirfil, string fil2, string cat)
    {
        string strXML = "";
        try
        {
            // lblmsg.Text = "";
            da = new mydataaccess1();
            dt = new DataTable();
            string username = lblusername.Text;
            dt = da.ptw_chart_counts_person_work_new(datefil, cirfil, fil2, cat);



            int sum_count = Convert.ToInt32(dt.Rows[0][4].ToString()) + Convert.ToInt32(dt.Rows[0][5].ToString()) + Convert.ToInt32(dt.Rows[0][6].ToString()) + Convert.ToInt32(dt.Rows[0][7].ToString());

            strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status, Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " (" + sum_count + ")" + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

            //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
            strXML += "<set name='" + "Electrical" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][5].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";

            strXML += "<set name='" + "Height" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][4].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";

            strXML += "<set name='" + "OFC" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][6].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' />";

            strXML += "<set name='" + "RF Energy" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][7].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + "' />";
            // strXML += "<set name='" + "T@DH" + " + (" + dt.Rows[0][9].ToString() + ") + " + "' value='" + dt.Rows[0][4].ToString() + "' />";

            strXML += "</graph>";
        }
        catch (Exception ee)
        {
            strXML = "No Data Found..";
        }

        return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);



    }

    private string CreateChart_barchart_indi_new(string datefil, string cirfil, string fil2, string cat)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE;

        strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix='' rotateNames='1' formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Height' color='F6BD0F'>";
        strDataProdB = "<dataset seriesName='Electrical' color='AFD8F8'>";
        strDataProdC = "<dataset seriesName='OFC' color='8EB229'>";
        strDataProdD = "<dataset seriesName='RF Energy' color='FD6035'>";
        strDataProdE = "<dataset seriesName='T@DH' color='FD60FF'>";

        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_counts_person_work_new(datefil, cirfil, fil2, cat);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString()) + Convert.ToInt32(dt.Rows[i][7].ToString()) + Convert.ToInt32(dt.Rows[i][8].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][7].ToString()) + "' />";
                strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][8].ToString()) + "' />";
                //strDataProdE += "<set value='" + dt.Rows[i][10].ToString() + "' />";

                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";
            strDataProdD += "</dataset>";
            // strDataProdE += "</dataset>";
            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales123", countdrp, "400", false, false);

    }

    private string CreateChart_barchart_trend_indi_new(string datefil, string cirfil, string fil2, string cat)
    {

        string strXML = "", strCategories, strDataProdA, strDataProdB, strDataProdC, strDataProdD, strDataProdE;

        strXML = "<graph caption=' " + SpacialCharRemove.XSS_Remove(ddlcounttype.SelectedItem.Text.ToString()) + " Status for Circle : " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "' numberPrefix=''  formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";

        //Initiate <dataset> elements
        strDataProdA = "<dataset seriesName='Height' color='F6BD0F'>";
        strDataProdB = "<dataset seriesName='Electrical' color='AFD8F8'>";
        strDataProdC = "<dataset seriesName='OFC' color='8EB229'>";
        strDataProdD = "<dataset seriesName='RF Energy' color='FD6035'>";
        strDataProdE = "<dataset seriesName='T@DH' color='FD60FF'>";
        //Iterate through the data	

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.ptw_chart_counts_person_work_trend_new(datefil, cirfil, fil2, cat);
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][4].ToString()) + Convert.ToInt32(dt.Rows[i][5].ToString()) + Convert.ToInt32(dt.Rows[i][6].ToString()) + Convert.ToInt32(dt.Rows[i][7].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][8].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][4].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][5].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "' />";
                strDataProdD += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][7].ToString()) + "' />";
                //  strDataProdE += "<set value='" + dt.Rows[i][9].ToString() + "' />";

                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";
            strDataProdD += "</dataset>";
            //strDataProdE += "</dataset>";
            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + strDataProdD + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales789", countdrp, "400", false, false);

    }
    protected void ddlissuercompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        da = new mydataaccess1();
        dt = da.select_issuer_from_company_chart(ddlissuercompany.Text, ddlcategory.SelectedValue, drpcircle.SelectedValue);
        if (dt.Rows.Count > 0)
        {
            ddlissuer.DataSource = dt;
            ddlissuer.DataTextField = "username";
            ddlissuer.DataBind();
            ddlissuer.Items.Insert(0, "Select");
            ddlissuer.Items.Insert(1, "All");
        }
        else
        {
            ddlissuer.Items.Clear();

            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('There is no any issuer for selected company');</script>");
        }
    }
}
