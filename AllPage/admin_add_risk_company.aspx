﻿<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_add_risk_company.aspx.cs" Inherits="AllPage_admin_manual_add_imei" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var isShift = false;
        $(document).ready(function () {
            $("#<%=SpacialCharRemove.XSS_Remove(TextBox1.ClientID) %>").keypress(function (e) {
                //alert(e.keyCode);

                if (e.keyCode == 16) {
                    //alert("Shift");
                    isShift = true;
                } else {
                    isShift = false;
                }
                if (e.keyCode == 53 && isShift == true) {
                    return false;
                } else if (e.keyCode == 37) {
                    return false;
                } else {
                    return true;
                }
            })

        })
    </script>
    <table width="100%">
        <tr>
            <td style="width: 5%">&nbsp;</td>
            <td style="text-align: center;" colspan="2">
                <asp:Label ID="Label3" runat="server" CssClass="lblstly"
                    Text="Add Risk Company"></asp:Label>
            </td>
            <td style="width: 5%">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 5%"></td>
            <td style="text-align: right;" colspan="2">
                <hr __designer:mapid="25c" />
            </td>
            <td style="width: 5%"></td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                <asp:Label ID="lblcircle" runat="server" Text="Company :" CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox1"
                    ErrorMessage="*"></asp:RequiredFieldValidator>

            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">&nbsp;
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click"
                    Width="55px" />
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:Label ID="lblmsg" runat="server"></asp:Label>
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 20px;">&nbsp;
            </td>
            <td style="width: 35%; text-align: right; height: 20px;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left; height: 20px;">&nbsp;
            </td>
            <td style="width: 5%; height: 20px;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 20px;"></td>
            <td style="width: 35%; text-align: right; height: 20px;"></td>
            <td style="width: 55%; text-align: left; height: 20px;">&nbsp;
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td style="width: 5%; height: 20px;"></td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="text-align: center;" colspan="2">&nbsp;
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">&nbsp;
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
