﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using business;
using dataaccesslayer;
using System.Data;
using mybusiness;
public partial class circle_admin_ptw : System.Web.UI.MasterPage
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
		  da = new mydataaccess1();
                   string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (!IsPostBack)
        {

            try
            {
string strPreviousPage = "";
 if (Request.UrlReferrer != null)
   {
    strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];         
    }       
if(strPreviousPage =="")
    {
       Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
     }
                if (Session["flag"].ToString() == "4")
                {
                    lnkchangeproject.Visible = true;
                }
                else
                {
                    lnkchangeproject.Visible = false;
                }
                // marque Start
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                da = new mydataaccess1();
                dt = new DataTable();
                string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                da = new mydataaccess1();
                count = da.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

                da = new mydataaccess1();
                count1 = da.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);
              
                lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                //Marquee End


                int i = 3;
                if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString()==sp_user_)
                {

                }
                else
                {

                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();

                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        } 
      

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            string r = da.select_user_cookie(Session["user"].ToString());

            da = new mydataaccess1();
            da.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}
