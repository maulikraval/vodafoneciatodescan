﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FindDataAccessLayer;
using System.Data;
using dataaccesslayer;
using dataaccesslayer2;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using System.Configuration;

public partial class AllPage_LatLongApproval : System.Web.UI.Page
{
    FindDataAccess DA;
    DataTable dt;
    mydataaccess1 da;
    mydataaccess2 da2;
    DataSet ds;

    protected void Page_Load(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (!IsPostBack)
        {
            Fill_GV_LatLongApproval();
        }
    }

    protected void Fill_GV_LatLongApproval()
    {
        try
        {
            DA = new FindDataAccess();
            GV_LatLongApproval.DataSource = DA.Select_LatLong_Approval();
            GV_LatLongApproval.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void lnbtn_Approve_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = sender as LinkButton;
        //getting particular row linkbutton
        GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
        //getting userid of particular row
        int Id = Convert.ToInt32(GV_LatLongApproval.DataKeys[gvrow.RowIndex].Value.ToString());
        DropDownList DDL = (DropDownList)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("drp_Closeby");
        Label Project = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Project");
        Label NewLat = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Userlat");
        Label NewLong = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Userlong");
        Label OldLat = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Sitelat");
        Label OldLong = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Sitelong");
        Label Username = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Username");
        Label Siteid = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Siteid");
        Label Sitename = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Sitename");
        Label OTime = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Requestedtime");
        Label Distance = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Distance");
        Label Circle = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Circle");
        Label MCompany = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_MCompany");
        Label SCompany = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_SCompany");
        Label Mobile = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Mobile");
        Label Zone = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Zone");

        if (DDL.SelectedValue.ToString() != "0")
        {
            da2 = new mydataaccess2();
            da2.insert_circleadmin_email_in_tt(Id, "ciat.helpdesk@skyproductivity.com");
            string CloseBy = DDL.SelectedItem.Text;
            DA = new FindDataAccess();
            DataTable SiteDT = new DataTable();
            SiteDT = DA.SELECT_ID_SITEID(Siteid.Text);
            DA = new FindDataAccess();

            ////FindDataAccess DA = new FindDataAccess();
            //string str = "select top 1 flag, Action_Status from tt_master where siteid='" + Siteid.Text + "' and id='" + Id + "' order by id desc";
            //cmd = new SqlCommand(str, connection);
            //da = new SqlDataAdapter(cmd);
            da = new mydataaccess1();
            ds = new DataSet();
            ds = da.CheckCloseBeforeApprove_Reject(Id, Siteid.Text);

            int res = Convert.ToInt32(ds.Tables[0].Rows[0]["flag"].ToString());
            string res2 = ds.Tables[0].Rows[0]["Action_Status"].ToString();
            if ((res == 1) && (res2 == "Closed by User"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(Siteid.Text) + " Request already processed.');</script>");
            }
            else
            {
                DA.select_update_lat_long_su(float.Parse(NewLat.Text), float.Parse(NewLong.Text), Convert.ToInt32(SiteDT.Rows[0]["ID"]), 1, Username.Text);
                DA = new FindDataAccess();
                DataTable DC = new DataTable();
                DC = DA.CLOSE_TT_22042016(Id, CloseBy, "Approved by CIAT Helpdesk");
                if (DC.Rows.Count > 0)
                {
                    if (Convert.ToInt32(DC.Rows[0][0].ToString()) == 1)
                    {
                        //Send_Email_To_User("Approve", Project.Text, Username.Text, Siteid.Text, Sitename.Text, OTime.Text, DateTime.Now.ToString(), OldLat.Text, OldLong.Text, NewLat.Text, NewLong.Text, Distance.Text, Circle.Text, Mobile.Text, MCompany.Text, SCompany.Text, Zone.Text);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + SpacialCharRemove.XSS_Remove(Siteid.Text) + " Request already processed.')", true);
                    }
                }
            }
            Fill_GV_LatLongApproval();
        }
        else
        {

        }
    }

    protected void lnbtn_Reject_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = sender as LinkButton;
        //getting particular row linkbutton
        GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
        //getting userid of particular row
        int Id = Convert.ToInt32(GV_LatLongApproval.DataKeys[gvrow.RowIndex].Value.ToString());
        DropDownList DDL = (DropDownList)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("drp_Closeby");
        Label Project = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Project");
        Label NewLat = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Userlat");
        Label NewLong = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Userlong");
        Label OldLat = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Sitelat");
        Label OldLong = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Sitelong");
        Label Username = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Username");
        Label Siteid = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Siteid");
        Label Sitename = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Sitename");
        Label OTime = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Requestedtime");
        Label Distance = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Distance");
        Label Circle = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Circle");
        Label MCompany = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_MCompany");
        Label SCompany = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_SCompany");
        Label Mobile = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Mobile");
        Label Zone = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Zone");

        if (DDL.SelectedValue.ToString() != "0")
        {
            string CloseBy = DDL.SelectedItem.Text;
            /*DA = new FindDataAccess();
            DataTable DC = new DataTable();*/

            ////FindDataAccess DA = new FindDataAccess();
            //string str = "select top 1 flag, Action_Status from tt_master where siteid='" + Siteid.Text + "' and id='" + Id + "' order by id desc";
            //cmd = new SqlCommand(str, connection);
            //da = new SqlDataAdapter(cmd);
            da = new mydataaccess1();
            ds = new DataSet();
            ds = da.CheckCloseBeforeApprove_Reject(Id, Siteid.Text);

            int res = Convert.ToInt32(ds.Tables[0].Rows[0]["flag"].ToString());
            string res2 = ds.Tables[0].Rows[0]["Action_Status"].ToString();
            if ((res == 1) && (res2 == "Closed by User"))
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + SpacialCharRemove.XSS_Remove(Siteid.Text) + " Request already processed.');</script>");
            }
            else
            {
                /*DC = DA.CLOSE_TT_22042016(Id, CloseBy, "Rejected by CIAT Helpdesk");
                if (DC.Rows.Count > 0)
                {
                    if (Convert.ToInt32(DC.Rows[0][0].ToString()) == 1)
                    {*/
                //Send_Email_To_User("Reject", Project.Text, Username.Text, Siteid.Text, Sitename.Text, OTime.Text, DateTime.Now.ToString(), OldLat.Text, OldLong.Text, NewLat.Text, NewLong.Text, Distance.Text, Circle.Text, Mobile.Text, MCompany.Text, SCompany.Text, Zone.Text);
                //Send_Email_To_CircleAdmin("Reject", Project.Text, Username.Text, Siteid.Text, Sitename.Text, OTime.Text, DateTime.Now.ToString(), OldLat.Text, OldLong.Text, NewLat.Text, NewLong.Text, Distance.Text, Circle.Text, Mobile.Text, MCompany.Text, SCompany.Text, Zone.Text);
                Response.Redirect("http://ptw.skyproductivity.com/AllPage/RejectMailSelection.aspx?id=" + SpacialCharRemove.XSS_Remove(Id.ToString()) + "&pro=" + SpacialCharRemove.XSS_Remove(Project.Text) + "&siteid=" + SpacialCharRemove.XSS_Remove(Siteid.Text) + "&sname=" + SpacialCharRemove.XSS_Remove(Sitename.Text) + "&otime=" + SpacialCharRemove.XSS_Remove(OTime.Text) + "&olat=" + SpacialCharRemove.XSS_Remove(OldLat.Text)+ "&olong=" + SpacialCharRemove.XSS_Remove(OldLong.Text) + "&nlat=" + SpacialCharRemove.XSS_Remove(NewLat.Text) + "&nlong=" + SpacialCharRemove.XSS_Remove(NewLong.Text) + "&user=" + SpacialCharRemove.XSS_Remove(Username.Text) + "&dist=" + SpacialCharRemove.XSS_Remove(Distance.Text) + "&circle=" + SpacialCharRemove.XSS_Remove(Circle.Text) + "&mo=" + SpacialCharRemove.XSS_Remove(Mobile.Text) + "&mcom=" + SpacialCharRemove.XSS_Remove(MCompany.Text) + "&scom=" + SpacialCharRemove.XSS_Remove(SCompany.Text) + "&zone=" + SpacialCharRemove.XSS_Remove(Zone.Text) + "&closeby=" + SpacialCharRemove.XSS_Remove(CloseBy) + "");
                /*}
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + Siteid.Text + " Request already processed.')", true);
                }
            }*/
            }
            Fill_GV_LatLongApproval();
        }
        else
        {

        }
    }

    string to_email = "";
    protected void Send_Email_To_User(string Action_Status, string ProjectName, string Username, string SiteId, string SiteName, string OTime, string CTime, string OLat, string OLong, string NLat, string NLong, string Distance, string Circle, string Mobile, string MCompnay, string SCompany, string Zone)
    {
        try
        {
            to_email = da2.get_email_id_user(Username);
            if (to_email != "" && to_email != null)
            {

                string subject = "";
                if (Action_Status.ToLower() == "approve")
                {
                    subject = "Your Lat-Long Update Request Approved by Helpdesk For Site : " + SendMail.ishtml(SiteId);
                }
                else
                {
                    subject = "Your Lat-Long Update Request Rejected by Helpdesk For Site : " + SendMail.ishtml(SiteId);
                }
                string body = Email_Body(Action_Status, ProjectName, Username, SiteId, SiteName, OTime, CTime, OLat, OLong, NLat, NLong, Distance, Circle, Mobile, to_email, MCompnay, SCompany, Zone);
                SendMail sm = new SendMail();
                sm.mailsend(to_email, subject, body);

                // Vodafone SMTP Credentials
                //string Body = Email_Body(Action_Status, ProjectName, Username, SiteId, SiteName, OTime, CTime, OLat, OLong, NLat, NLong, Distance, Circle, Mobile, to_email, MCompnay, SCompany, Zone);
                //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", to_email);
                ////MailMessage Msg = new MailMessage();
                ////MailAddress fromMail = new MailAddress(fromEmail);
                ////Msg.From = fromMail;
                ////Msg.To.Add(new MailAddress(toEmail));
                ////if (ccEmail != "" && bccEmail != "")
                ////{
                ////    Msg.CC.Add(new MailAddress(ccEmail));
                ////    Msg.Bcc.Add(new MailAddress(bccEmail));
                ////}
                //if (Action_Status.ToLower() == "approve")
                //{
                //    message.Subject = "Your Lat-Long Update Request Approved by Helpdesk For Site : " + SiteId;
                //}
                //else
                //{
                //    message.Subject = "Your Lat-Long Update Request Rejected by Helpdesk For Site : " + SiteId;
                //}
                //message.Body = Body;
                ////message.CC.Add("latlong.helpdesk@gmail.com");
                //message.IsBodyHtml = true;

                //#region "VODAFONE SERVER CREDENTIALS"

                //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

                //emailClient.Credentials =
                //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                //emailClient.EnableSsl = false;

                //#endregion

                //#region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

                ///* SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
                //emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
                //emailClient.EnableSsl = true; */

                //#endregion

                //emailClient.Send(message);
            }

        }
        catch (Exception ex)
        {

        }
    }

    #region Send_Email_To_CircleAdmin-old_code_don't_remove
    ////string to_email2 = "";
    //protected void Send_Email_To_CircleAdmin(string Action_Status, string ProjectName, string Username, string SiteId, string SiteName, string OTime, string CTime, string OLat, string OLong, string NLat, string NLong, string Distance, string Circle, string Mobile, string MCompnay, string SCompany, string Zone)
    //{
    //    try
    //    {
    //        //Label Circle = (Label)GV_LatLongApproval.Rows[gvrow.RowIndex].FindControl("lbl_Circle");
    //        DataSet ds = new DataSet();
    //        ds = da2.get_email_id_user_rolewise(Circle);
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            string[] to_email2 = new string[ds.Tables[0].Rows.Count];
    //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
    //            {
    //                to_email2[i] = ds.Tables[0].Rows[i]["emailid"].ToString();
    //                if (to_email2[i] != "" && to_email2[i] != null)
    //                {
    //                    // Vodafone SMTP Credentials
    //                    string Body = Email_Body2(Action_Status, ProjectName, Username, SiteId, SiteName, OTime, CTime, OLat, OLong, NLat, NLong, Distance, Circle, Mobile, to_email, MCompnay, SCompany, Zone);
    //                    MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", to_email2[i]);

    //                    if (Action_Status.ToLower() == "approve")
    //                    {
    //                        message.Subject = "Your Lat-Long Update Request Approved by Helpdesk For Site : " + SiteId;
    //                    }
    //                    else
    //                    {
    //                        message.Subject = "Your Lat-Long Update Request Rejected by Helpdesk For Site : " + SiteId;
    //                    }
    //                    message.Body = Body;
    //                    //message.CC.Add("latlong.helpdesk@gmail.com");
    //                    message.IsBodyHtml = true;

    //                    #region "VODAFONE SERVER CREDENTIALS"

    //                    //SmtpClient emailClient = new SmtpClient("10.87.152.16", 25);
    //                    //emailClient.Credentials =
    //                    //           new NetworkCredential("Chetan.Patel12@vodafone.com", "Sep@2016@P");

    //                    //emailClient.EnableSsl = false;

    //                    #endregion

    //                    #region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

    //                    SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
    //                    emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
    //                    emailClient.EnableSsl = true;

    //                    #endregion

    //                    emailClient.Send(message);
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}
    #endregion Send_Email_To_CircleAdmin-old_code_don't_remove

    public string Email_Body(string Action_Status, string ProjectName, string Username, string SiteId, string SiteName, string OTime, string CTime, string OLat, string OLong, string NLat, string NLong, string Distance, string Circle, string Mobile, string Email, string MCompnay, string SCompany, String Zone)
    {
        string Body = "";

        Body = "Dear " + SendMail.ishtml(Username) + "<br /><br />";

        if (Action_Status.ToLower() == "approve")
        {
            Body += "Your Lat / Long Request has been approved. Please refresh Your site selection page and select zone name as per below.<br /><br />";

            Body += "<table border='1'>";
            Body += "<tr style='background-color:red;color:white'>";
            Body += "<th>ProjectName</th><th>Site Id</th> <th>Site Name</th> <th>Zone</th> <th>Requested time by user</th> <th>Approve Time By Support Team</th> <th>Old lat of site</th> <th>Old long of site</th> <th>New lat of site</th> <th>New long of site</th> <th>Distance updated in mtr</th>";
            Body += "</tr>";
            Body += "<tr>";
            Body += "<td>" + SendMail.ishtml(ProjectName) + "</td><td>" + SendMail.ishtml(SiteId) + "</td> <td>" + SendMail.ishtml(SiteName) + "</td> <td>" + SendMail.ishtml(Zone) + "</td> <td>" + SendMail.ishtml(OTime) + "</td> <td>" + SendMail.ishtml(CTime) + "</td> <td>" + SendMail.ishtml(OLat) + "</td> <td>" + SendMail.ishtml(OLong) + "</td> <td>" + SendMail.ishtml(NLat) + "</td> <td>" + SendMail.ishtml(NLong) + "</td> <td>" + SendMail.ishtml(Distance) + "</td>";
            Body += "</tr>";
            Body += "</table><br />";
        }
        else
        {
            Body += "Your Lat / Long Request has been Rejected due to Distance from site is more than 2km.<br /><br />";
            Body += "If you want to Update lat-long as per your location ,then please take approval of your circle admin on this mail and send mail post approval to CIAT Helpdesk on ciat.ptw@gmail.com.<br />";
            Body += "Do not send this mail without any approval of your circle admin, Lat-long will not update by CIAT Help desk until Approval Given.<br /><br />";

            Body += "<table border='1'>";
            Body += "<tr style='background-color:red;color:white'>";
            Body += "<th>ProjectName</th><th>Site Id</th> <th>Site Name</th><th>Zone</th> <th>Requested time by user</th> <th>Rejected Time By Support Team</th> <th>Current lat of site</th> <th>Current long of site</th> <th>User lat</th> <th>User long</th> <th>Requested distance to update as per user location in mtr</th>";
            Body += "</tr>";
            Body += "<tr>";
            Body += "<td>" + SendMail.ishtml(ProjectName) + "</td><td>" + SendMail.ishtml(SiteId) + "</td> <td>" + SendMail.ishtml(SiteName) + "</td> <td>" + SendMail.ishtml(Zone) + "</td> <td>" + SendMail.ishtml(OTime) + "</td> <td>" + SendMail.ishtml(CTime) + "</td> <td>" + SendMail.ishtml(OLat) + "</td> <td>" + SendMail.ishtml(OLong) + "</td> <td>" + SendMail.ishtml(NLat) + "</td> <td>" + SendMail.ishtml(NLong) + "</td> <td>" + SendMail.ishtml(Distance) + "</td>";
            Body += "</tr>";
            Body += "</table><br />";
        }

        Body += "<table border='1'>";
        Body += "<tr style='background-color:red;color:white'>";
        Body += "<th>Requested by username</th> <th>Circle</th> <th>Mobile number of user</th> <th>EmailId of user</th> <th>Main company</th> <th>Sub Company</th>";
        Body += "</tr>";
        Body += "<tr>";
        Body += "<td>" + SendMail.ishtml(Username) + "</td> <td>" + SendMail.ishtml(Circle) + "</td> <td>" + SendMail.ishtml(Mobile) + "</td> <td>" + SendMail.ishtml(Email) + "</td> <td>" + SendMail.ishtml(MCompnay) + "</td> <td>" + SendMail.ishtml(SCompany) + "</td>";
        Body += "</tr>";
        Body += "</table><br />";

        Body += "Please Note :- Do not Reply on this mail if you have any query then ,please contact as below.<br /><br />";
        Body += "Contact Us :-<br />";
        Body += "Helpdesk Number :- 079 40009543 , 8141587709 , 9978791606, 9727716004<br />";
        Body += "Email :- ciat.helpdesk@skyproductivity.com<br />";

        return Body;
    }

    #region Email_Body2-old_code_don't_remove
    //public string Email_Body2(string Action_Status, string ProjectName, string Username, string SiteId, string SiteName, string OTime, string CTime, string OLat, string OLong, string NLat, string NLong, string Distance, string Circle, string Mobile, string Email, string MCompnay, string SCompany, String Zone)
    //{
    //    string Body = "";

    //    Body = "Dear Circle-Admin <br /><br />";

    //    if (Action_Status.ToLower() == "reject")
    //    {
    //        Body += "User " + Username +"'s Lat / Long Request has been Rejected due to Distance from site is more than 2km.<br /><br />";
    //        Body += "If you want to Update lat-long as per user's location ,then please take approval of your circle admin on this mail and send mail post approval to CIAT Helpdesk on ciat.ptw@gmail.com.<br />";
    //        Body += "Do not send this mail without any approval of your circle admin, Lat-long will not update by CIAT Help desk until Approval Given.<br /><br />";

    //        Body += "<table border='1'>";
    //        Body += "<tr style='background-color:red;color:white'>";
    //        Body += "<th>ProjectName</th><th>Site Id</th> <th>Site Name</th><th>Zone</th> <th>Requested time by user</th> <th>Rejected Time By Support Team</th> <th>Current lat of site</th> <th>Current long of site</th> <th>User lat</th> <th>User long</th> <th>Requested distance to update as per user location in mtr</th>";
    //        Body += "</tr>";
    //        Body += "<tr>";
    //        Body += "<td>" + ProjectName + "</td><td>" + SiteId + "</td> <td>" + SiteName + "</td> <td>" + Zone + "</td> <td>" + OTime + "</td> <td>" + CTime + "</td> <td>" + OLat + "</td> <td>" + OLong + "</td> <td>" + NLat + "</td> <td>" + NLong + "</td> <td>" + Distance + "</td>";
    //        Body += "</tr>";
    //        Body += "</table><br />";

    //    }
    //    Body += "<table border='1'>";
    //    Body += "<tr style='background-color:red;color:white'>";
    //    Body += "<th>Requested by username</th> <th>Circle</th> <th>Mobile number of user</th> <th>EmailId of user</th> <th>Main company</th> <th>Sub Company</th>";
    //    Body += "</tr>";
    //    Body += "<tr>";
    //    Body += "<td>" + Username + "</td> <td>" + Circle + "</td> <td>" + Mobile + "</td> <td>" + Email + "</td> <td>" + MCompnay + "</td> <td>" + SCompany + "</td>";
    //    Body += "</tr>";
    //    Body += "</table><br />";

    //    Body += "<a href='http://ptw.skyproductivity.com/PopupApprove.aspx?siteid="+SiteId+"&username="+Username+"&newlat="+NLat+"&newlong="+NLong+"' style='background:#44c767;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Approve</a>&nbsp;&nbsp;&nbsp;";
    //    Body += "<a href='http://ptw.skyproductivity.com/PopupReject.aspx?siteid="+SiteId+"&username="+Username+"&newlat="+NLat+"&newlong="+NLong+"' style='background:#FF0000;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Reject</a><br /><br />";
    //    //Body += "<a href='http://ptw.skyproductivity.com/PopupApprove.aspx?pro=" + ProjectName + "&siteid=" + SiteId + "&sname=" + SiteName + "&zone=" + Zone + "&otime=" + OTime + "&ctime=" + CTime + "&olat=" + OLat + "&olong=" + OLong + "&nlat=" + NLat + "&nlong=" + NLong + "&dis=" + Distance + "&user=" + Username + "&ctime=" + Circle + "&olat=" + Mobile + "&olong=" + Email + "&nlat=" + MCompnay + "&nlong=" + SCompany + "' style='background:#44c767;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Approve</a>&nbsp;&nbsp;&nbsp;";
    //    //Body += "<a href='http://ptw.skyproductivity.com/PopupReject.aspx?pro=" + ProjectName + "&siteid=" + SiteId + "&sname=" + SiteName + "&zone=" + Zone + "&otime=" + OTime + "&ctime=" + CTime + "&olat=" + OLat + "&olong=" + OLong + "&nlat=" + NLat + "&nlong=" + NLong + "&dis=" + Distance + "&user=" + Username + "&ctime=" + Circle + "&olat=" + Mobile + "&olong=" + Email + "&nlat=" + MCompnay + "&nlong=" + SCompany + "' style='background:#FF0000;background:-webkit-linear-gradient(#3d85c6, #073763);padding:5px;background:linear-gradient(#3d85c6, #073763);border-radius: 5px;color:#fff;display:inline-block;font:normal 700 24px/1 'Calibri',sans-serif;text-align:center;text-shadow:1px 1px 0 #000;'>Reject</a><br /><br />";

    //    Body += "Please Note :- Do not Reply on this mail if you have any query then ,please contact as below.<br /><br />";
    //    Body += "Contact Us :-<br />";
    //    Body += "Helpdesk Number :- 079 40009543 , 8141587709 , 9978791606, 9727716004<br />";
    //    Body += "Email :- ciat.helpdesk@skyproductivity.com<br />";

    //    return Body;
    //}
    #endregion Email_Body2-old_code_don't_remove

    protected void btnSearchSite_Click(object sender, EventArgs e)
    {
        if (txtSearchSite.Text != "")
        {
            da2 = new mydataaccess2();
            //sp_user_d = da.select_user_cookie(Session["user"].ToString());
            //da = new mydataaccess1();
            dt = new DataTable();
            //dt = da.viewlatlongrequest_userwise("%" + TextBox1.Text + "%", sp_user_d);
            dt = da2.viewsearchsite_latlongrequest(txtSearchSite.Text, "");
            if (dt.Rows.Count == 0)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('No Data Found!!!');</script>");
                GV_SearchLast5LatLong.Visible = false;
                panel1.Visible = false;
                txtSearchUser.Text = "";
            }
            else
            {
                GV_SearchLast5LatLong.DataSource = dt;
                GV_SearchLast5LatLong.DataBind();
                GV_SearchLast5LatLong.Visible = true;
                panel1.Visible = true;
                gv_user.Visible = false;
            }
            txtSearchUser.Text = "";
            gv_user.Visible = false;
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Enter Siteid!!!');</script>");
            GV_SearchLast5LatLong.Visible = false;
            panel1.Visible = false;
            txtSearchUser.Text = "";
            gv_user.Visible = false;
        }
    }

    protected void btnSearchuser_Click(object sender, EventArgs e)
    {
        if (txtSearchUser.Text != "")
        {
            DA = new FindDataAccess();
            dt = new DataTable();
            dt = DA.Support_Find_User_su(txtSearchUser.Text, "");
            gv_user.DataSource = dt;
            gv_user.DataBind();
            gv_user.Visible = true;
            GV_SearchLast5LatLong.Visible = false;
            panel1.Visible = false;
            txtSearchSite.Text = "";
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Enter Username!!!');</script>");
            gv_user.Visible = false;
            GV_SearchLast5LatLong.Visible = false;
            panel1.Visible = false;
            txtSearchSite.Text = "";
        }
    }

    protected void gv_user_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Determine the RowIndex of the Row whose Button was clicked.
        int rowIndex = Convert.ToInt32(e.CommandArgument);

        //Reference the GridView Row.
        GridViewRow row = gv_user.Rows[rowIndex];

        //Fetch value of Name.
        string name = "";
        name = gv_user.Rows[int.Parse(e.CommandArgument.ToString())].Cells[2].Text;

        da2 = new mydataaccess2();
        dt = new DataTable();
        dt = da2.viewsearchsite_latlongrequest("", name);
        GV_SearchLast5LatLong.DataSource = dt;
        GV_SearchLast5LatLong.DataBind();
        GV_SearchLast5LatLong.Visible = true;
        panel1.Visible = true;
        gv_user.Visible = false;
    }
}