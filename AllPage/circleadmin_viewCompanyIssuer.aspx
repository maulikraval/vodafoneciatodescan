﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="circleadmin_viewCompanyIssuer.aspx.cs" Inherits="admin_showuser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <link href="circleadmin_StyleSheet.css" rel="stylesheet" type="text/css" />
    <table width="100%">
        <tr>
            <td style="width: 5%;">
            </td>
            <td colspan="2">
                <div style="width: 100%; padding-left: 375px;">
                    <asp:Label ID="Label3" runat="server" CssClass="lblstly" Text="Distance Details"></asp:Label>
                </div>
                <hr />
                <div style="width: 100%; padding-left: 330px;">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/page_excel.png"
                        OnClick="ImageButton1_Click" />
                </div>
                <div style="width: 100%; text-align: center; padding-left: 332px;">
                    <asp:Panel ID="panel1" runat="server" ScrollBars="Horizontal" Width="250px" Height="500px">
                        <asp:GridView ID="grdcompanyiisuer" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                            BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                            Width="250px">
                            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                            <FooterStyle BackColor="#CCCCCC" />
                            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                            <RowStyle Font-Names="Calibri" Font-Size="13px" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </td>
            <td style="width: 5%;">
            </td>
        </tr>
        <tr>
            <td style="width: 5%;">
            </td>
            <td colspan="2" style="text-align: center">
                <asp:Label ID="Label4" runat="server"  CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 5%;">
            </td>
        </tr>
        <tr>
            <td style="width: 5%;">
                &nbsp;
            </td>
            <td colspan="2">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td style="width: 5%;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%;">
                &nbsp;
            </td>
            <td style="width: 45%;">
                &nbsp;
            </td>
            <td style="width: 45%;">
                &nbsp;
            </td>
            <td style="width: 5%;">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
