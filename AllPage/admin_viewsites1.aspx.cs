﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using mybusiness;
using dataaccesslayer;

public partial class admin_viewsites1 : System.Web.UI.Page
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        try
        {
            if (!IsPostBack)
            {
                try
                {
                    int i = 4;
                    if (Convert.ToInt32(Session["role"].ToString()) == i)
                    {

                    }
                    else
                    {

                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch
                {

                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }

                int count = 0;
                int count1 = 0;
                int count2 = 0;
                // Marquee start
            /*    da = new mydataaccess1();
                DataTable dtv5 = new DataTable();
                dtv5 = da.reminder_20_days();
                da = new mydataaccess1();
                DataTable dtv2 = new DataTable();
                dtv2 = da.reminder_20_days_v2();
                da = new mydataaccess1();
                DataTable dtv2ms = new DataTable();
                dtv2ms = da.reminder_20_days_v2ms();
                count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                // critical points
                da = new mydataaccess1();
                DataTable cv2 = new DataTable();
                cv2 = da.reminder_20_days_critical_v2();
                da = new mydataaccess1();
                DataTable cv5 = new DataTable();
                cv5 = da.reminder_20_days_critical();
                da = new mydataaccess1();
                DataTable cv2ms = new DataTable();
                cv2ms = da.reminder_20_days_critical_v2ms();
                count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                da = new mydataaccess1();
                DataTable dtv51 = new DataTable();
                dtv51 = da.reminder_10_days();
                da = new mydataaccess1();
                DataTable dtv21 = new DataTable();
                dtv21 = da.reminder_10_days_v2();
                da = new mydataaccess1();
                DataTable dtv2ms1 = new DataTable();
                dtv2ms1 = da.reminder_10_days_v2ms();
                count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";*/
                // Marquee End
                string sp_user_d = "";

                da = new mydataaccess1();
                dt = new DataTable();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                if (lblusername.Text == "")
                {
                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                }
                ba = new myvodav2();
                da = new mydataaccess1();
                dt = new DataTable();

                dt = da.getcirclename(sp_user_d);
                ddlcircle.DataSource = dt;
                ddlcircle.DataTextField = "Circle";
                ddlcircle.DataBind();
                ddlcircle.Items.Insert(0, "select");


            }


        }

        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            DataTable zone = new DataTable();
            ba.Circle = ddlcircle.SelectedValue;
            zone = da.getzonesbycirclename(ba);

            ddlzone.DataSource = zone;
            ddlzone.DataTextField = "zone";
            ddlzone.DataBind();
            ddlzone.Items.Insert(0, "select");
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ba = new myvodav2();
            da = new mydataaccess1();
            dt = new DataTable();
            ba.Zone = ddlzone.SelectedItem.Text.ToString();
            ba.Circle = ddlcircle.SelectedItem.Text.ToString();
            dt = da.getviewzone(ba);
            grd1.DataSource = dt;
            grd1.DataBind();
            Label2.Text = "";

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void grd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = grd1.SelectedRow;
            txt1.Text = gr.Cells[1].Text;
            txt2.Text = gr.Cells[2].Text;
            txt3.Text = gr.Cells[3].Text;
            txt4.Text = gr.Cells[4].Text;
            txt5.Text = gr.Cells[5].Text;
            txt6.Text = gr.Cells[6].Text;
            txt7.Text = gr.Cells[7].Text;
            txt8.Text = gr.Cells[8].Text;
            txt9.Text = gr.Cells[9].Text;
            txt10.Text = gr.Cells[10].Text;
            txt11.Text = gr.Cells[11].Text;
            txt12.Text = gr.Cells[12].Text;
            txt13.Text = gr.Cells[13].Text;
            
            txt14.Text = gr.Cells[14].Text;

            txtipprovider.Text = gr.Cells[15].Text;
            //txtipprovider.Text = gr.Cells[15].Text;
            txtipid.Text = gr.Cells[16].Text;
            txtsitetype.Text = gr.Cells[17].Text;
            if (txt13.Text.ToLower() == "ciic v1" || txt13.Text.ToLower() == "cic v1")
            {
                mod1.Hide();
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('You can not update this site');</script>");

            }
            else
            {
                mod1.Show();
            }
            

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string sp_user_d = "";
            da = new mydataaccess1();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());

            ba = new myvodav2();
            da = new mydataaccess1();
            ba.Siteid = txt1.Text;
            ba.Lat = Convert.ToDouble(txt4.Text);
            ba.Longitude = Convert.ToDouble(txt5.Text);
            double dis = da.select_update_lat_long(ba);

            if (dis <= 2000)
            {
//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('1');</script>");
                ba = new myvodav2();
                da = new mydataaccess1();
                ba.Siteid = txt1.Text.Trim();
                ba.Sitename = txt2.Text;
                ba.Address = txt3.Text;
                ba.Lat = Convert.ToDouble(txt4.Text);
                ba.Longitude = Convert.ToDouble(txt5.Text);
                ba.Towertype = txt6.Text;
                ba.Userid = txt7.Text;
                ba.Technician = txt8.Text;
                ba.Circle = txt9.Text;
                ba.Zone = txt10.Text;
                ba.Subzone = txt11.Text;
                ba.Tech_contact = txt12.Text;
                ba.Check_sheet = txt13.Text;
                ba.Datetime = txt14.Text;
                ba.User = sp_user_d;
                ba.Provider = txtipprovider.Text;
                ba.Ip_id = txtipid.Text;
                ba.Site_type = txtsitetype.Text;
                int i = da.InsertVodaSite(ba);
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Updated Successfully...');</script>");
            }
            else
            {
//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('2');</script>");
                ba = new myvodav2();
                da = new mydataaccess1();
                ba.Siteid = txt1.Text.Trim();
                ba.Sitename = txt2.Text;
                ba.Address = txt3.Text;
                ba.Lat = Convert.ToDouble(txt4.Text);
                ba.Longitude = Convert.ToDouble(txt5.Text);
                ba.Towertype = txt6.Text;
                ba.Userid = txt7.Text;
                ba.Technician = txt8.Text;
                ba.Circle = txt9.Text;
                ba.Zone = txt10.Text;
                ba.Subzone = txt11.Text;
                ba.Tech_contact = txt12.Text;
                ba.Check_sheet = txt13.Text;
                ba.Datetime = txt14.Text;
                ba.User = sp_user_d;
                ba.Provider = txtipprovider.Text;
                ba.Ip_id = txtipid.Text;
                ba.Site_type = txtsitetype.Text;
                int i = da.InsertVodaSite_without_latlong(ba);

                da = new mydataaccess1();
                ba = new myvodav2();
                ba.Siteid = txt1.Text.Trim();
                ba.Lat = Convert.ToDouble(txt4.Text);
                ba.Longitude = Convert.ToDouble(txt5.Text);
                ba.Distance = dis;
                da.insert_into_latlong(ba);
Response.Redirect("admin_approve_lat_long.aspx",false);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your Lat/Long are sent for approval/Rejection to Teleysia Support');</script>");

            }
            if (ddlcircle.SelectedIndex != 0 && ddlzone.SelectedIndex != 0)
            {
                ba = new myvodav2();
                da = new mydataaccess1();
                dt = new DataTable();
                ba.Zone = ddlzone.SelectedItem.Text.ToString();
                ba.Circle = ddlcircle.SelectedValue;
                dt = da.getviewzone(ba);
                grd1.DataSource = dt;
                grd1.DataBind();
            }
            if (TextBox1.Text != "")
            {
                da = new mydataaccess1();
                ba = new myvodav2();
                dt = new DataTable();
                ba.Siteid = TextBox1.Text;
                //   ba.Circle = "";
                //  ba.Zone = "";
                dt = da.searchsiteid(ba);
                grd1.DataSource = dt;
                grd1.DataBind();
            }
          //  Label2.Text = "Update Successfully......";



        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {


            da = new mydataaccess1();
            da = new mydataaccess1();
            dt = new DataTable();
            string a= da.select_user_cookie(Session["user"].ToString());
            da.update_user_master_status(a);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
      protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text != "")
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            ba.Siteid = TextBox1.Text;
            //   ba.Circle = "";
            //  ba.Zone = "";
            dt = da.searchsiteid(ba);
            grd1.DataSource = dt;
            grd1.DataBind();
        }
        else
        {
            Label2.Text = "Please enter siteid!!!";
        }
    }
}
