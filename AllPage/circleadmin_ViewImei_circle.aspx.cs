﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;
public partial class admin_ViewImei : System.Web.UI.Page
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["flag"] == "1")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "2")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "4")
        {
 if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }

            //this.MasterPageFile = "~/circle_admin/NewCircle.master";
        }
        if (Session["flag"] == "3")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                if (Session["role"].ToString() != "3")
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    string sp_user_d = "";
                    da = new mydataaccess1();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    if (sp_user_d == "")
                    {
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                    }

                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();


                    dt = da.getcircle_user(sp_user_d);
                    string circle = dt.Rows[0][0].ToString();

                    ba = new myvodav2();
                    da = new mydataaccess1();
                    dt = new DataTable();

                    dt = da.selectimeno_circle(sp_user_d);
                    grd1.DataSource = dt;
                    grd1.DataBind();
                }
            }

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {

            string sp_user_d = "";
            da = new mydataaccess1();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());

            ba = new myvodav2();
            da = new mydataaccess1();
            dt = new DataTable();
           
            // ba.Imeiid = Convert.ToInt32(txtid.Text);
            ba.Im = txt1.Text;
            ba.Mob_model = txt2.Text;

            ba.User = sp_user_d;
            da.updateimeino(ba);

            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            dt = da.getcircle_user(sp_user_d);
            string circle = dt.Rows[0][0].ToString();

            ba = new myvodav2();
            da = new mydataaccess1();
            dt = new DataTable();

            dt = da.selectimeno_circle(sp_user_d);
            grd1.DataSource = dt;
            grd1.DataBind();

            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            dt = da.getcircle_user(sp_user_d);
            

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void btncancle_Click(object sender, EventArgs e)
    {

    }


    protected void grd1_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {

    }
    //protected void lb_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //    da = new mydataaccess1();

    //    LinkButton lb = (LinkButton)sender;
    //    //   GridViewRow gr = grd1.SelectedRow;
    //    string s = lb.ToolTip.ToString();
    //    da.deleteimeino(s);

    //    da = new mydataaccess1();
    //    ba = new myvodav2();
    //    dt = new DataTable();
    //    string sp_user_d = "";
    //    // split for decodation username.
    //    string[] sp_user = Session["user"].ToString().Split('?');
    //    sp_user_d = decode(sp_user[1].ToString());

    //    dt = da.getcircle_user(sp_user_d);
    //    string circle = dt.Rows[0][0].ToString();

    //    ba = new myvodav2();
    //    da = new mydataaccess1();
    //    dt = new DataTable();

    //    dt = da.selectimeno_circle(sp_user_d, circle);
    //    grd1.DataSource = dt;
    //    grd1.DataBind();

    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
    //    }

    //}
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }


    protected void grd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gr = grd1.SelectedRow;
            txt1.Text = gr.Cells[1].Text;
            txt2.Text = gr.Cells[2].Text;
            // txtid.Text = gr.Cells[2].Text;

            mod1.Show();

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void grd1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {

    }
}
