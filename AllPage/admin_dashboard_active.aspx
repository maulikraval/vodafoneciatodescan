<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_dashboard_active.aspx.cs" Inherits="AllPage_admin_20daysreminder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%">
        <tr>
            <td style="width: 100%; text-align: center;">
                <asp:Label ID="Label1" runat="server" Font-Names="Calibri" Font-Size="20px" ForeColor="#FFA500"
                    Text="Dashboard"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: left;">
                <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" 
                    Font-Names="calibri" Font-Size="13px" ActiveTabIndex="0">
                    <cc1:TabPanel ID="tpanel1" runat="server" HeaderText="All">
                        <ContentTemplate>
                            <asp:Panel ID="panel1" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grd20" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%"  >
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel6" runat="server" HeaderText="All Punchpoint">
                        <ContentTemplate>
                            <asp:Panel ID="panel7" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdallp" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%"  >
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>


                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="BTS_New">
                        <ContentTemplate>
                            <asp:Panel ID="panel2" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grd10" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel7" runat="server" HeaderText="BTS_New Punchpoint">
                        <ContentTemplate>
                            <asp:Panel ID="panel8" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdbtsp" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="MW">
                        <ContentTemplate>
                            <asp:Panel ID="panel3" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdmw" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel8" runat="server" HeaderText="MW Punchpoint">
                        <ContentTemplate>
                            <asp:Panel ID="panel9" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdmwp" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="TMEA">
                        <ContentTemplate>
                            <asp:Panel ID="panel4" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdtmea" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel9" runat="server" HeaderText="TMEA Punchpoint">
                        <ContentTemplate>
                            <asp:Panel ID="panel10" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdtmeap" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="BSC">
                        <ContentTemplate>
                            <asp:Panel ID="panel5" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdbsc" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel10" runat="server" HeaderText="BSC Punchpoint">
                        <ContentTemplate>
                            <asp:Panel ID="panel11" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdbscp" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>

                    <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="InfraVisual">
                        <ContentTemplate>
                            <asp:Panel ID="panel6" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdinfra" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel11" runat="server" HeaderText="InfraVisual Punchpoint">
                        <ContentTemplate>
                            <asp:Panel ID="panel12" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grdivp" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No sites found ..!"
                                    ForeColor="Black" Width="100%" >
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: center;">
                <asp:ScriptManager runat="server" ID="sc1">
                </asp:ScriptManager>
            </td>
        </tr>
    </table>
</asp:Content>
