﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true" CodeFile="FindSite.aspx.cs" Inherits="Support_FindSite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <asp:Button ID="btnstart" runat="server" Text="Open Your Ticket" OnClick="btnstart_Click" Height="40px" CausesValidation="false" />
     <asp:Label ID="lblerror" runat="server" ></asp:Label>
    <div id="divtt" runat="server">
        <table>
            <tr>
                <td>
                  
                </td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_Username" runat="server" Text="Username"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_Username" runat="server" Text="" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_Site" runat="server" Text="Site Id"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:TextBox ID="txt_Site" runat="server" Text=""></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_Site" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Project"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="drpproject" runat="server">
                        <asp:ListItem>CIAT</asp:ListItem>
                        <asp:ListItem>PTW</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Company"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="drpcompany" runat="server">
                        <asp:ListItem>Vodafone</asp:ListItem>
                        <asp:ListItem>NSN</asp:ListItem>
                        <asp:ListItem>huawei</asp:ListItem>
                        <asp:ListItem>INDUS</asp:ListItem>
                        <asp:ListItem>Haribhakti</asp:ListItem>
                        <asp:ListItem>Wipro</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Issue Handler"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="drpsupport" runat="server">
                        <asp:ListItem>---Select Your Name---</asp:ListItem>
                        <asp:ListItem>Suresh</asp:ListItem>
                        <asp:ListItem>Vishal</asp:ListItem>
                        <asp:ListItem>Poem</asp:ListItem>
                        <asp:ListItem>Dixit</asp:ListItem>
                        <asp:ListItem>Sumit</asp:ListItem>
                        <asp:ListItem>Chetan</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="drpsupport" ErrorMessage="*" InitialValue="---Select Your Name---"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Issue Category" Visible="false"></asp:Label>
                </td>
                <td>&nbsp
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" Visible="false" AutoPostBack="false" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem>---Select Category---</asp:ListItem>
                        <asp:ListItem>Change LAT/LONG Of Site</asp:ListItem>
                        <asp:ListItem>Site not found due to user is far from Site</asp:ListItem>
                        <asp:ListItem>Site not found due to user has selected Wrong Zone</asp:ListItem>
                        <asp:ListItem>Site Not Exist in system</asp:ListItem>
                        <asp:ListItem>Already Sent To Circle admin for request to recheck</asp:ListItem>
                        <asp:ListItem>Other</asp:ListItem>

                    </asp:DropDownList>
                    <br />
                    <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" Width="316px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;<asp:Button ID="btnclose" runat="server" Text="Close Your Ticket" OnClick="btnclose_Click" Height="40px" />

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_find_site" runat="server" Text="Find" OnClick="btn_find_site_Click" Width="155px" Height="40px" />
                    <br />
                    <br />

                    <br />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
   
    <div>
         <asp:Panel ID="panel1" runat="server" ScrollBars="Horizontal" Width="1030px" Visible="false">
        <asp:GridView ID="gv_site" runat="server" Visible="false"
            EnableModelValidation="True" DataKeyNames="id"
            BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="100%">
            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle Font-Names="Calibri" Font-Size="13px" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />

            <EmptyDataTemplate>
                <asp:Label ID="lbl_gv" runat="server" Text="No Data Found...!!" ForeColor="white"></asp:Label>
            </EmptyDataTemplate>
        </asp:GridView>
        </asp:Panel>
    </div>
        <br />
    <div>
        <asp:Button ID="btn_find_distance" runat="server" Text="Find Distance" Visible="false" OnClick="btn_find_distance_Click" />
    </div>
        <br />
    <div>
        <asp:Panel ID="panel2" runat="server" ScrollBars="Horizontal" Width="1030px" Visible="false">
        <asp:GridView ID="gv_distance" runat="server" Visible="false"
            EnableModelValidation="True" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" Width="100%">
            <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
            <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
            <FooterStyle BackColor="#CCCCCC" />
            <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
            <RowStyle Font-Names="Calibri" Font-Size="13px" />
            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <EmptyDataTemplate>
                <asp:Label ID="lbl_gv" runat="server" Text="No Data Found...!!" ForeColor="white"></asp:Label>
            </EmptyDataTemplate>
        </asp:GridView>
        </asp:Panel>
        <br />
        <br />
        <asp:Label ID="lbdis" Text="Please Enter Distance If Distance is showing more than 2000mtr :" runat="server"></asp:Label>
        <asp:TextBox ID="txtdiss" runat="server"></asp:TextBox>
       
        <br />
        <br />
    </div>
    <div>
        <asp:Button ID="btn_show_site" runat="server" Text="Show Site" Visible="false" OnClick="btn_show_site_Click" Height="40px" />
        &nbsp;<asp:Button ID="btnviewdis" runat="server" Text="View Updated Distance" OnClick="btnviewdis_Click" Height="40px" />
        &nbsp;
    </div>
    </div>
</asp:Content>

