﻿<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_Viewsite.aspx.cs" Inherits="AllPage_admin_Viewsite" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />
   
    <table width="100%">
        <tr>
            <td width="5%">
                &nbsp;
            </td>
            <td width="25%">
                &nbsp;
            </td>
            <td width="65%">
            </td>
            <td width="5%">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="5%">
                &nbsp;
            </td>
            <td width="25%" style="vertical-align: top;">
                <table class="style2">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlcircle" runat="server" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged"
                                AutoPostBack="true" Width="200px" CssClass="genall">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblzone" runat="server" Font-Bold="False" Text="Zone Name" CssClass="lblall"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DropDownList ID="ddlzone" runat="server" AutoPostBack="true" Width="200px" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged"
                                CssClass="genall">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td width="65%" style="vertical-align: top;">
                <div>
                    <asp:ScriptManager ID="SM1" runat="server">
                    </asp:ScriptManager>
                    <asp:Panel ID="pnlgrd1" Width="800px" ScrollBars="Horizontal" runat="server">
                        <asp:GridView ID="grd1" BackColor="WhiteSmoke" runat="server" OnSelectedIndexChanged="grd1_SelectedIndexChanged"
                            Width="100%">
                              <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </div>
            </td>
            <td width="5%">
                &nbsp;
            </td>
        </tr>
    </table>
    <%--  <cc1:RoundedCornersExtender ID="rd1" runat="server" TargetControlID="panel2" Radius="20"
        Color="Black">
    </cc1:RoundedCornersExtender>--%>
    <asp:Panel ID="panel2" runat="server" ScrollBars="Vertical" Width="950px" Height="300px"
        ForeColor="Black" BackColor="#E4E4E4">
        <table width="100%">
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td width="10%" style="text-align: right">
                    &nbsp;
                </td>
                <td width="35%" style="text-align: left">
                    &nbsp;
                </td>
                <td width="10%" style="text-align: right">
                    &nbsp;
                </td>
                <td width="35%" style="text-align: left">
                    &nbsp;
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    SiteId:
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt1" runat="server" Enabled="False"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt1" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right" width="10%">
                    SiteName:
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt2" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt2" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    Address:
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt3" runat="server"></asp:TextBox>
                </td>
                <td style="text-align: right" width="10%">
                    Lat:
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt4" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txt4"
                        ValidChars="0123456789.">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    long
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt5" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txt5"
                        ValidChars="0123456789.">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right" width="10%">
                    Towertype
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt6" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt6" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    Userid
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt7" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txt7"
                        ValidChars="0123456789.">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right" width="10%">
                    Technician
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt8" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt8" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    Circle
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt9" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txt9"
                        ValidChars="0123456789.">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right" width="10%">
                    zone
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt10" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt10" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    subzone
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt11" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt11" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right" width="10%">
                    TechnicianContact
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt12" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txt12"
                        ValidChars="0123456789+-">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    Check Sheet
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt13" runat="server"></asp:TextBox>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|="
                        TargetControlID="txt13" FilterMode="InvalidChars">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right" width="10%">
                    Site Date
                </td>
                <td style="text-align: left" width="35%">
                    <asp:TextBox ID="txt14" runat="server"></asp:TextBox>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="35%">
                    <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" Text="Update" />
                    <asp:Button ID="btncancle" runat="server" Text="Cancel" />
                </td>
                <td style="text-align: right" width="10%">
                    &nbsp;
                </td>
                <td style="text-align: left" width="35%">
                    &nbsp;
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    &nbsp;
                </td>
                <td style="text-align: left" width="35%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    &nbsp;
                </td>
                <td style="text-align: left" width="35%">
                    &nbsp;
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="butt1" PopupControlID="panel2">
    </cc1:ModalPopupExtender>
</asp:Content>
