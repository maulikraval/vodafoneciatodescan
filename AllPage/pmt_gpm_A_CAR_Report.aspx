﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pmt_gpm_A_CAR_Report.aspx.cs" Inherits="PM_Active_Circle_Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/pmt_menu.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>CIAT | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/circleadmin_styleinner1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/cufon-yui.js"></script>

    <script type="text/javascript" src="../js/arial.js"></script>

    <script type="text/javascript" src="../js/cuf_run.js"></script>

    <script src="../js/jquery.js" type="text/javascript"></script>

    <link href="../css/menu.css" rel="stylesheet" type="text/css" />

    <script src="../js/menu.js" type="text/javascript"></script>

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .ajax__calendar_body
        {
            z-index: 100004;
        }
        #roll a
        {
            display: inline-table;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 762px;
        }
        .style2
        {
            width: 269px;
        }
        .style3
        {
            width: 269px;
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="updatepanel" runat="server">
        <ContentTemplate>--%>
    <div class="main">
        <div class="header">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                        <tr>
                            <td width="6%" rowspan="2">
                                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style1" rowspan="2">
                                 <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change 
                                        Password</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%" style="text-align: right;">
                                <asp:LinkButton ID="lnkchangeproject" runat="server" OnClick="lnkchange_Click" CausesValidation="False"
                                    PostBackUrl="~/login/ciat_ptw.aspx" Visible="False">Change 
                                Project</asp:LinkButton>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clr">
                </div>
                <uc2:Menu ID="menubar" runat="server" />
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
                <div class="mainbar">
                    <div class="article1">
                        <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server" Text="" CssClass="lblmarquee"></asp:Label></marquee>
                    </div>
                </div>
                <div class="sidebar">
                    <div class="gadget">
                        <table class="style2">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    <asp:DropDownList ID="drpcircle" runat="server" AutoPostBack="True" Width="200px"
                                        CssClass="genall">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    <asp:Label ID="Label13" runat="server" Font-Bold="False" Text="Provider Type" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    <asp:DropDownList ID="ddlprovidername" runat="server" AutoPostBack="false" Width="200px"
                                        CssClass="genall" OnSelectedIndexChanged="ddlprovidername_SelectedIndexChanged">
                                        <asp:ListItem>Select Provider</asp:ListItem>
                                        <asp:ListItem Value="1">VIL</asp:ListItem>
                                        <asp:ListItem Value="2">Indus</asp:ListItem>
                                        <asp:ListItem Value="3">Other BOO</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlprovidername"
                                        ErrorMessage="*" InitialValue="Select Provider" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label7" runat="server" Font-Bold="False" Text="Select Sheet" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:DropDownList ID="ddlCheckSheet" runat="server" AutoPostBack="true" Width="200px"
                                        CssClass="genall" OnSelectedIndexChanged="ddlCheckSheet_SelectedIndexChanged">
                                        <asp:ListItem>Select</asp:ListItem>
                                        <asp:ListItem Value="46">BTS_New</asp:ListItem> 
                                        <asp:ListItem Value="47">MW</asp:ListItem>
                                        <asp:ListItem Value="48">TMEA</asp:ListItem>
                                        <asp:ListItem Value="49">BSC</asp:ListItem>
                                        <asp:ListItem Value="50">InfraVisual</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlCheckSheet"
                                        ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:MultiView ID="MultiView1" runat="server">
                                        <asp:View ID="View1" runat="server">
                                            <table class="style2">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Font-Bold="False" Text="Select Dates" CssClass="lblall"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="style2">
                                                            <tr>
                                                                <td style="text-align: right" width="30%">
                                                                    <asp:Label ID="Label4" runat="server" Font-Bold="False" Text="From : " CssClass="lblall"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBox1" runat="server" CssClass="genall" Width="100px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox1"
                                                                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right" width="30%">
                                                                    <asp:Label ID="Label5" runat="server" Font-Bold="False" Text="To : " CssClass="lblall"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="TextBox2" runat="server" CssClass="genall" Width="100px"></asp:TextBox>
                                                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TextBox2">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox2"
                                                                        ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">
                                                        <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" Text="Search"
                                                            CausesValidation="true" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center">
                                                        <asp:Label ID="Label3_" runat="server" Font-Names="Calibri" ></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="mainbar">
                    <div class="article">
                        <table width="100%">
                             <tr>
                                <td width="5%">
                                    <asp:Label ID="Label6" runat="server" Font-Names="Calibri" Font-Size="13px" ForeColor="#FFA500"
                                        Text="Search For : "></asp:Label>
                                    <asp:TextBox ID="txtsearxh" runat="server"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="tw" runat="server" WatermarkText="SiteID" WatermarkCssClass="waterback"
                                        TargetControlID="txtsearxh">
                                    </cc1:TextBoxWatermarkExtender>
                                    <asp:Button ID="btnsitesearch" runat="server" Text="Search" CausesValidation ="false"
                                        onclick="btnsitesearch_Click" />
                                </td>
                                <td width="65%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <asp:Panel ID="pp" runat="server">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="text-align: center; background-color: #D8D9DA" colspan="6">
                                                                <asp:Label ID="Label28" runat="server" ForeColor="Black" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center; background-color: #D8D9DA" colspan="2">
                                                                <asp:Label ID="Label11" runat="server" Text="Action Status Details" ForeColor="Black"
                                                                    Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td style="text-align: center; background-color: #D8D9DA" colspan="2">
                                                                <asp:Label ID="Label17" runat="server" Text="Criticality Details" ForeColor="Black"
                                                                    Font-Bold="True"></asp:Label>
                                                            </td>
                                                            <td style="text-align: center; background-color: #D8D9DA" colspan="2">
                                                                <asp:Label ID="Label25" runat="server" Text="Legend Details" ForeColor="Black" Font-Bold="True"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 20%; text-align: right">
                                                                <asp:Label ID="Label19" runat="server" Text="Open" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:Label ID="lbla" runat="server" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; text-align: right">
                                                                <asp:Label ID="Label20" runat="server" Text="Critical A points" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:Label ID="lblaa" runat="server" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="width: 16%; text-align: left">
                                                                <asp:TextBox ID="TextBox12" runat="server" ReadOnly="true" Width="10%" BackColor="Red"></asp:TextBox>
                                                                <asp:Label ID="Label21" runat="server" Text="Open" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 16%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:TextBox ID="TextBox9" runat="server" ReadOnly="true" Width="10%" BackColor="Green"></asp:TextBox>
                                                                <asp:Label ID="Label18" runat="server" Text="Close" ForeColor="Black"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 20%; text-align: right">
                                                                <asp:Label ID="Label12" runat="server" Text="Close" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:Label ID="lblb" runat="server" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; text-align: right">
                                                                <asp:Label ID="Label15" runat="server" Text="Critical B points" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:Label ID="lblbb" runat="server" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="width: 16%; text-align: left">
                                                                <asp:TextBox ID="TextBox10" runat="server" ReadOnly="true" Width="10%" BackColor="Yellow"></asp:TextBox>
                                                                <asp:Label ID="Label24" runat="server" Text="WIP" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 16%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:TextBox ID="TextBox13" runat="server" ReadOnly="true" Width="10%" BackColor="Khaki"></asp:TextBox>
                                                                <asp:Label ID="Label10" runat="server" ForeColor="Black" Text="First CAR"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 20%; text-align: right">
                                                                <asp:Label ID="Label2" runat="server" Text="WIP" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:Label ID="lblc" runat="server" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="width: 20%; text-align: right">
                                                                <asp:Label ID="Label22" runat="server" Text="Critical C points" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                                                                border-right-style: solid;">
                                                                <asp:Label ID="lblcc" runat="server" ForeColor="Black"></asp:Label>
                                                            </td>
                                                            <td style="width: 16%; text-align: left">
                                                            </td>
                                                            <td style="border-width: thin; border-color: #000000; width: 16%; text-align: left;
                                                                border-right-style: solid;">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" style="text-align: center">
                                    <asp:Label ID="rptlable" runat="server" Font-Bold="False" CssClass="lblstly"></asp:Label>
                                </td>
                                <td width="65%" style="text-align: center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td width="5%" style="text-align: center">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;" width="25%">
                                    <div>
                                        <table class="style2">
                                            <tr>
                                                <td>
                                                   <%-- <asp:Button ID="Button1" runat="server" Text="Bulk Download" OnClick="Button1_Click1" />
                                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                                    &nbsp;<asp:Button ID="Button2" runat="server" BackColor="#DE2021" Font-Bold="True"
                                                        ForeColor="White" OnClick="Button2_Click" Text="Upload Bulk Data" />--%>
                                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="720px" Height="500px"
                                                        ForeColor="White">
                                                        <asp:GridView ID="grdMaster" runat="server" BackColor="White" BorderColor="#999999"
                                                            BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                                            ForeColor="Black" Width="100%" AutoGenerateColumns="False" 
                                                            OnRowDataBound="grdMaster_RowDataBound">
                                                            <FooterStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="siteid" ItemStyle-BackColor="Khaki">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkSiteId" runat="server" ToolTip='<% #Eval("siteid") %>'
                                                                            Text='<% #Eval("siteid") %>' OnClick="lnkSiteId_Click" ForeColor="DarkBlue"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="SiteName" HeaderText="SiteName" />
                                                                <asp:BoundField DataField="Address" HeaderText="Address" />
                                                                <asp:BoundField DataField="SurveyDate" HeaderText="SurveyDate" />
                                                                <asp:BoundField DataField="Technician" HeaderText="Technician" />
                                                                <%--<asp:BoundField DataField="Open" HeaderText="Open" />
                                                                <asp:BoundField DataField="Close" HeaderText="Close" />
                                                                <asp:BoundField DataField="WIP" HeaderText="WIP" />
                                                                <asp:BoundField DataField="Criticality_A" HeaderText="Criticality_A" />
                                                                <asp:BoundField DataField="Criticality_B" HeaderText="Criticality_B" />
                                                                <asp:BoundField DataField="Criticality_C" HeaderText="Criticality_C" />
                                                                <asp:BoundField DataField="final_status" HeaderText="CAR Status" />--%>
                                                            </Columns>
                                                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                                            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                                            <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
              <%--  <p class="lf">
                    © Copyright 2011 <a href="http://teleysia.com">Teleysia Networks Pvt. Ltd.</a></p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
        <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
