﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using dataaccesslayer2;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.fonts;
using System.IO;
using mybusiness;
using System.Collections.Generic;

//try
public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    mydataaccess1 da1;
    mydataaccess2 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;

    //vodabal ba;
    int cat;
    string ptw;
    //int ptwid = 0;
    iTextSharp.text.Document doc;
    string ptwid;
    string ptwid1;

    DataTable dt_site;
    iTextSharp.text.Font verdana;
    Phrase p2;
    Phrase p1_mahesh;
    Chunk titleChunk;
    PdfTemplate template;
    BaseFont bf = null;
    iTextSharp.text.Image footer;
    string pdfFilePath;
    string file;
    string imag_file1;
    string imag_file2;
    string imag_file3;
    string imag_file4;
    string imag_file5;
    string imag_ptw;
    string vlogo;
    iTextSharp.text.Rectangle rec;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

     //    imag_file1 = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\image002.png";
             imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image002.png";
     //   imag_file2 = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\image001.png";
              imag_file2 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image001.png";
      //   vlogo = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\vlogo.png";
              vlogo = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\vlogo.png";
      //  imag_file3 = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\image003.png";
              imag_file3 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image003.png";
       // imag_file4 = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\image004.png";
               imag_file4 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image004.png";

       // imag_file5 = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\image005.png";
               imag_file5 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image005.png";      
        //imag_file1 = "F:\\Vodafone\\Vodafone_Ciat_production_24052013\\Copy of Vodafone_Ciat_production_24052013\\Vodafone_Ciat_production_24052013\\image002.png";
        //imag_file2 = "F:\\Vodafone\\Vodafone_Ciat_production_24052013\\Copy of Vodafone_Ciat_production_24052013\\Vodafone_Ciat_production_24052013\\image001.png";
        //vlogo = "F:\\Vodafone\\Vodafone_Ciat_production_24052013\\Copy of Vodafone_Ciat_production_24052013\\Vodafone_Ciat_production_24052013\\vlogo.png";
        //imag_file3 = "F:\\Vodafone\\Vodafone_Ciat_production_24052013\\Copy of Vodafone_Ciat_production_24052013\\Vodafone_Ciat_production_24052013\\image003.png";
        //imag_file4 = "F:\\Vodafone\\Vodafone_Ciat_production_24052013\\Copy of Vodafone_Ciat_production_24052013\\Vodafone_Ciat_production_24052013\\image004.png";


        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                ImageButton1.Visible = false;
	 da1 = new mydataaccess1();
                string sp_user_ = da1.select_user_cookie(Session["user"].ToString());
                if (Convert.ToInt32(Session["role"].ToString()) == 4 && Session["um"].ToString()==sp_user_)
                {
                }

                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }
            catch
            {
                 Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {



            if (!IsPostBack)
            {


                if (drpcircle.SelectedIndex != 0)
                {
                    //da = new mydataaccess1();
                    //  sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da1 = new mydataaccess1();
                    string sp_user_d = da1.select_user_cookie(Session["user"].ToString());
                    lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                    if (lblusername.Text == "")
                    {
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                    }
                    da1 = new mydataaccess1();
                    ba = new myvodav2();
                    ba = new myvodav2();
                    DataTable circle = new DataTable();

                    ba.User = lblusername.Text;
                    circle = da1.getcirclenamefromusername(ba);

                    drpcircle.DataSource = circle;
                    drpcircle.DataTextField = "circle";
                    drpcircle.DataBind();
                    drpcircle.Items.Insert(0, "Select");
                    drpcircle.Items.Insert(1, "All");


                }
            }
        }
        catch (Exception ex)
        {
          Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu;
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        Response.ClearHeaders();

        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }




    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }



    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView1.DataSource = null;
        GridView1.DataBind();
    }

    protected void btnsitesearch_Click(object sender, EventArgs e)
    {
        da1 = new mydataaccess1();
        dt = new DataTable();

        // dt = da1.select_ptw_Master_report("", "where site_master.siteid='" + txtsearxh.Text + "'");
        dt = da1.ptw_select_approved_ptw_issuer_pdf(" and site_master.siteid='" + txtsearxh.Text + "'");

        GridView1.DataSource = dt;
        GridView1.DataBind();

        GridView1.Visible = true;
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (drpcircle.SelectedIndex != 0)
        {

            da1 = new mydataaccess1();
            dt = new DataTable();
            if (drpcircle.SelectedIndex == 1)
            {
                dt = da1.ptw_select_approved_ptw_issuer_pdf_night(" and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102) and issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102))  ");
            }
            else
            {
                dt = da1.ptw_select_approved_ptw_issuer_pdf_night(" and location='" + drpcircle.Text + "' and (issuedate >= CONVERT(DATETIME, '" + TextBox1.Text + "', 102) and issuedate <= CONVERT(DATETIME, '" + TextBox2.Text + "  23:59:59', 102)) ");
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

        GridViewRow gr = GridView1.SelectedRow;
        ptwid = gr.Cells[4].Text.Replace("&#160;", " ");
        //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + ptwid + "');</script>");
        string[] ptw = ptwid.Split('/');
        int p_id = Convert.ToInt32(ptw[ptw.Length - 1].ToString());
        string category = ptw[ptw.Length - 3].ToString();
        //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + p_id+ "');</script>");
        da = new mydataaccess2();
        ptwid = da.ptw_select_ptw_from_id(p_id);
        PdfPTable question1 = new PdfPTable(4);
        verdana = FontFactory.GetFont("Verdana", 12, Font.NORMAL, BaseColor.ORANGE);
        Font verdana_small = FontFactory.GetFont("Verdana", 7, Font.NORMAL, BaseColor.BLACK);

        Font calibri_small = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        //  ptwid = "TES/VF1234/22062012/EL/ptw/2568";
        //   ptwid = "ROB/WBVF5487/20062012/HT/firojuddin.mandal/220";
        // ptwid = "TES/VF1234/23062012/HT/ptw/2569";
        ptwid1 = ptwid.Replace("/", "_");
        ptwid1 = ptwid1.Replace(" ", "");
        //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + ptwid + "');</script>");
        da = new mydataaccess2();
        dt = new DataTable();
        dt = da.ptw_select_person_for_pdf(ptwid);

        da = new mydataaccess2();
        dt_site = new DataTable();
        dt_site = da.ptw_Select_site_data_report_night(ptwid);
        // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + dt.Rows.Count + "');</script>");

        if (dt.Rows.Count != 0)
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + ptwid + "');</script>");
            file = SpacialCharRemove.SpacialChar_Remove(ptwid1) + ".pdf";
            doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 40, 40, 40, 35);
            pdfFilePath = Server.MapPath("~") + "\\" + "report_ptw\\height_pdf" + "\\" + SpacialCharRemove.SpacialChar_Remove(ptwid1) + ".pdf";


            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(pdfFilePath, FileMode.Create));

            doc.Open();

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                rec = doc.PageSize;
                PdfPTable head = new PdfPTable(5);
                head.WidthPercentage = 100f;

                head.SetWidths(new float[] { 1, 4, 5, 4, 1 });

                head.DefaultCell.Border = 0;

                if (dt.Rows[0][0].ToString() == "1")
                {

                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file1);
                    image.SetAbsolutePosition(0, 100);
                    image.ScaleAbsolute(20f, 20f);



                    //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                    //image_voda.SetAbsolutePosition(0, 0);
                    //image_voda.ScaleAbsolute(35f, 25f);



                    PdfPCell c = new PdfPCell(image);
                    c.FixedHeight = 30f;
                    c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1 = new PdfPCell();
                    c1.FixedHeight = 30f;
                    c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1.VerticalAlignment = Element.ALIGN_RIGHT;
                    c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_ = new PdfPCell();
                    c1_.FixedHeight = 30f;
                    c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                    c1_.VerticalAlignment = Element.ALIGN_CENTER;
                    c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_1 = new PdfPCell();
                    c1_1.FixedHeight = 30f;
                    c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_11 = new PdfPCell();
                    c1_11.FixedHeight = 30f;
                    c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                    c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    head.AddCell(c);
                    head.AddCell(c1);
                    head.AddCell(c1_);
                    head.AddCell(c1_1);
                    head.AddCell(c1_11);
                }
                if (dt.Rows[0][0].ToString() == "2")
                {
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file2);
                    image.SetAbsolutePosition(0, 100);
                    image.ScaleAbsolute(20f, 20f);



                    //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                    //image_voda.SetAbsolutePosition(0, 0);
                    //image_voda.ScaleAbsolute(35f, 25f);



                    PdfPCell c = new PdfPCell(image);
                    c.FixedHeight = 30f;
                    c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1 = new PdfPCell();
                    c1.FixedHeight = 30f;
                    c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1.VerticalAlignment = Element.ALIGN_RIGHT;
                    c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_ = new PdfPCell();
                    c1_.FixedHeight = 30f;
                    c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                    c1_.VerticalAlignment = Element.ALIGN_CENTER;
                    c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_1 = new PdfPCell();
                    c1_1.FixedHeight = 30f;
                    c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_11 = new PdfPCell();
                    c1_11.FixedHeight = 30f;
                    c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                    c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    head.AddCell(c);
                    head.AddCell(c1);
                    head.AddCell(c1_);
                    head.AddCell(c1_1);
                    head.AddCell(c1_11);
                }
                if (dt.Rows[0][0].ToString() == "3")
                {
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file3);
                    image.SetAbsolutePosition(0, 100);
                    image.ScaleAbsolute(20f, 20f);



                    //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                    //image_voda.SetAbsolutePosition(0, 0);
                    //image_voda.ScaleAbsolute(35f, 25f);



                    PdfPCell c = new PdfPCell(image);
                    c.FixedHeight = 30f;
                    c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1 = new PdfPCell();
                    c1.FixedHeight = 30f;
                    c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1.VerticalAlignment = Element.ALIGN_RIGHT;
                    c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_ = new PdfPCell();
                    c1_.FixedHeight = 30f;
                    c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                    c1_.VerticalAlignment = Element.ALIGN_CENTER;
                    c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_1 = new PdfPCell();
                    c1_1.FixedHeight = 30f;
                    c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_11 = new PdfPCell();
                    c1_11.FixedHeight = 30f;
                    c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                    c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    head.AddCell(c);
                    head.AddCell(c1);
                    head.AddCell(c1_);
                    head.AddCell(c1_1);
                    head.AddCell(c1_11);
                }
                if (dt.Rows[0][0].ToString() == "4")
                {
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file4);
                    image.SetAbsolutePosition(0, 100);
                    image.ScaleAbsolute(20f, 20f);



                    //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                    //image_voda.SetAbsolutePosition(0, 0);
                    //image_voda.ScaleAbsolute(35f, 25f);



                    PdfPCell c = new PdfPCell(image);
                    c.FixedHeight = 30f;
                    c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1 = new PdfPCell();
                    c1.FixedHeight = 30f;
                    c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1.VerticalAlignment = Element.ALIGN_RIGHT;
                    c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_ = new PdfPCell();
                    c1_.FixedHeight = 30f;
                    c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                    c1_.VerticalAlignment = Element.ALIGN_CENTER;
                    c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_1 = new PdfPCell();
                    c1_1.FixedHeight = 30f;
                    c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_11 = new PdfPCell();
                    c1_11.FixedHeight = 30f;
                    c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                    c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    head.AddCell(c);
                    head.AddCell(c1);
                    head.AddCell(c1_);
                    head.AddCell(c1_1);
                    head.AddCell(c1_11);
                }
                if (dt.Rows[0][0].ToString() == "5")
                {
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file5);
                    image.SetAbsolutePosition(0, 100);
                    image.ScaleAbsolute(20f, 20f);



                    //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                    //image_voda.SetAbsolutePosition(0, 0);
                    //image_voda.ScaleAbsolute(35f, 25f);



                    PdfPCell c = new PdfPCell(image);
                    c.FixedHeight = 30f;
                    c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c.HorizontalAlignment = Element.ALIGN_CENTER;
                    c.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1 = new PdfPCell();
                    c1.FixedHeight = 30f;
                    c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1.VerticalAlignment = Element.ALIGN_RIGHT;
                    c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_ = new PdfPCell();
                    c1_.FixedHeight = 30f;
                    c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                    c1_.VerticalAlignment = Element.ALIGN_CENTER;
                    c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_1 = new PdfPCell();
                    c1_1.FixedHeight = 30f;
                    c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                    c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    PdfPCell c1_11 = new PdfPCell();
                    c1_11.FixedHeight = 30f;
                    c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                    c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                    c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                    c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                    head.AddCell(c);
                    head.AddCell(c1);
                    head.AddCell(c1_);
                    head.AddCell(c1_1);
                    head.AddCell(c1_11);
                }
                PdfPTable t1 = new PdfPTable(1);
                t1.WidthPercentage = 100f;
                iTextSharp.text.Font f = new iTextSharp.text.Font(new Font(Font.FontFamily.HELVETICA, 11,
                Font.NORMAL, iTextSharp.text.BaseColor.ORANGE));
                f.SetStyle("underline");
                f.Size = 7;

                iTextSharp.text.Font t = new iTextSharp.text.Font(new Font(Font.FontFamily.HELVETICA, 11,
                Font.NORMAL, iTextSharp.text.BaseColor.BLACK));
                t.SetStyle("underline");
                t.Size = 7;
                // f.SetStyle("bold");
                Phrase p1 = new Phrase("Annexure - A", f);

                PdfPCell c2 = new PdfPCell(p1);
                c2.HorizontalAlignment = Element.ALIGN_CENTER;
                c2.BorderColor = iTextSharp.text.BaseColor.WHITE;
                t1.AddCell(c2);


                PdfPTable t2 = new PdfPTable(1);
                t2.DefaultCell.Border = 3;
                t2.WidthPercentage = 100f;
                t2.DefaultCell.BorderColor = iTextSharp.text.BaseColor.WHITE;

                //iTextSharp.text.Image image_voda1 = iTextSharp.text.Image.GetInstance(Server.MapPath("vlogo.png"));
                //image_voda1.SetAbsolutePosition(100, 5);
                //image_voda1.ScaleAbsolute(45f, 35f);
                //image_voda1.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                PdfPCell c2_1 = new PdfPCell();
                c2_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c2_1.BorderColor = iTextSharp.text.BaseColor.WHITE;

                t2.AddCell(c2_1);

                if (dt.Rows[0][0].ToString() == "1")
                {
                    p2 = new Phrase("\n VIL HSW: Working at height", verdana);
                }
                if (dt.Rows[0][0].ToString() == "2")
                {
                    p2 = new Phrase("\n VIL HSW: Electrical ", verdana);
                }
                if (dt.Rows[0][0].ToString() == "3")
                {
                    p2 = new Phrase("\n VIL HSW: OFC Job ", verdana);
                }
                if (dt.Rows[0][0].ToString() == "4")
                {
                    p2 = new Phrase("\n VIL HSW: RF Energy Isolation", verdana);
                }
                if (dt.Rows[0][0].ToString() == "5")
                {
                    p2 = new Phrase("\n VIL HSW: T@DH", verdana);
                }

                Phrase p2_11 = new Phrase("", verdana_small);

                Phrase p2_1 = new Phrase("", verdana_small);
                if (dt.Rows[0][0].ToString() == "1")
                {
                    p2_1 = new Phrase("\n This permit is to be issued for any work being carried out 2 mtrs above normal floor level.", verdana_small);
                }
                if (dt.Rows[0][0].ToString() == "2")
                {
                    p2_1 = new Phrase("\n This permit is to be issued for any work with possibility of unwarranted and uncontrolled release of electrical,mechanical and RF energy that can cause injury or harm to a person.", verdana_small);
                }
                if (dt.Rows[0][0].ToString() == "3")
                {
                    p2_1 = new Phrase("\n This permit is to be issued for any OFC work with possibility of unwarranted and uncontrolled release of electrical,mechanical and optical energy or fall from height that can cause injury or harm to a person.", verdana_small);
                }
                if (dt.Rows[0][0].ToString() == "4")
                {
                    p2_1 = new Phrase("\n This permit is to be issued for any work, with possibility of unwarranted and uncontrolled release of RF Energy that can cause injury or harm to a person.", verdana_small);
                }
                if (dt.Rows[0][0].ToString() == "5")
                {
                    p2_1 = new Phrase("\n This permit is to be issued for travel during dark hours by respective Function Head/Vertical Head (as applicable) to minimise the risk during journey for better travel management.", verdana_small);
                }

                Phrase site_name = new Phrase("\n \n Route Name ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase site_data = new Phrase("" + dt_site.Rows[0][4].ToString() + "        ", t);

                Phrase loc_name = new Phrase("  Location ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase loc_data = new Phrase("" + dt_site.Rows[0][3].ToString() + "                       ", t);

                Phrase vehicle_reg = new Phrase("   Vehicle registration No  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                //Phrase sr_data = new Phrase("" + ptwid + "                                                     ", f);
                Phrase vehicle_reg_data = new Phrase("" + dt_site.Rows[0][8].ToString() + "                       ", t);

                Phrase sr_name = new Phrase(" \n  Permit Sr. No.  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                //Phrase sr_data = new Phrase("" + ptwid + "                                                     ", f);
                Phrase sr_data = new Phrase("" + ptwid + "", t);

                //tring[] date = dt_site.Rows[0][2].ToString().Split(' ');
                string date_i = dt_site.Rows[0][2].ToString();
                string date_t = dt_site.Rows[0][7].ToString();

                Phrase issue_name = new Phrase("Application Date  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase issue_date = new Phrase("" + date_i + "        ", t);

                Phrase time_name = new Phrase("  Time  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase time_data = new Phrase("" + date_t + "                                         ", t);

                Phrase purpose_name = new Phrase("\nPurpose Of Visit  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase purpose_data = new Phrase(" " + dt_site.Rows[0][9].ToString() + "", t);

                // Phrase purpose_data = new Phrase(" " + dt_site.Rows[0][3].ToString() + "", t);

                Phrase from_name = new Phrase("\nValid from (Date & Time) ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase from_data = new Phrase("         " + dt_site.Rows[0][5].ToString() + "            ", t);

                Phrase to_name = new Phrase("  Valid up to (Date & Time)  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase to_data = new Phrase("           " + dt_site.Rows[0][6].ToString() + "           ", t);
                Phrase work = new Phrase("    Type of project / activity", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase work_data = new Phrase("         " + dt_site.Rows[0][12].ToString() + "            ", f);

                Phrase mwfm = new Phrase("  MWFM ID  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase mwfm_data = new Phrase("         " + dt_site.Rows[0][10].ToString() + "            ", f);

                Phrase number = new Phrase("  Work Order Number  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Phrase number_data = new Phrase("         " + dt_site.Rows[0][11].ToString() + "            ", f);

                // Phrase basic_purpose = new Phrase("\nPurpose of Visit : ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                // Phrase basic_purpose_data = new Phrase("         " + dt_site.Rows[0][5].ToString() + "            ", t);
                Phrase new_line = new Phrase("\n");


                PdfPTable new_t = new PdfPTable(1);
                new_t.WidthPercentage = 100f;

                Phrase head_ph = new Phrase("PTW form - Driver", new Font(Font.FontFamily.HELVETICA, 7,
                 Font.BOLD));

                PdfPCell new_cell = new PdfPCell(head_ph);
                new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell.FixedHeight = 12f;
                new_t.AddCell(new_cell);

                PdfPTable new_t7 = new PdfPTable(1);
                new_t7.WidthPercentage = 100f;
                Phrase head_ph7 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell7 = new PdfPCell(head_ph7);
                new_cell7.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell7.Border = Rectangle.NO_BORDER;
                new_cell7.FixedHeight = 7f;
                new_t7.AddCell(new_cell7);

                da = new mydataaccess2();
                DataTable dt_log = new DataTable();
                dt_log = da.ptw_select_que_log_data_report(ptwid, Convert.ToInt32(dt.Rows[0][0].ToString()), dt.Rows[i][1].ToString());

                PdfPTable log = new PdfPTable(4);
                log.WidthPercentage = 100f;
                log.SetWidths(new float[] { 1, 7, 2, 4 });
                PdfPCell l1 = new PdfPCell(new Phrase("No.", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell l2 = new PdfPCell(new Phrase("Consideration", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell l3 = new PdfPCell(new Phrase("Answer", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell l4 = new PdfPCell(new Phrase("Remarks", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));


                log.AddCell(l1);
                log.AddCell(l2);
                log.AddCell(l3);
                log.AddCell(l4);
                if (dt_log.Rows.Count > 0)
                {
                    for (int j = 0; j < dt_log.Rows.Count; j++)
                    {
                        l1 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                        l2 = new PdfPCell(new Phrase(dt_log.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                        l3 = new PdfPCell(new Phrase(dt_log.Rows[j][1].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                        l4 = new PdfPCell(new Phrase(dt_log.Rows[j][2].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                        l1.HorizontalAlignment = Element.ALIGN_CENTER;

                        l3.HorizontalAlignment = Element.ALIGN_CENTER;

                        log.AddCell(l1);
                        log.AddCell(l2);
                        log.AddCell(l3);
                        log.AddCell(l4);

                    }
                }
                else
                {
                    l1 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                    l2 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                    l3 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                    l4 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL)));
                    l1.HorizontalAlignment = Element.ALIGN_CENTER;

                    l3.HorizontalAlignment = Element.ALIGN_CENTER;

                    log.AddCell(l1);
                    log.AddCell(l2);
                    log.AddCell(l3);
                    log.AddCell(l4);
                }
                PdfPTable new_t1 = new PdfPTable(1);
                new_t1.WidthPercentage = 100f;
                Phrase head_ph1 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell1 = new PdfPCell(head_ph1);
                new_cell1.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell1.Border = Rectangle.NO_BORDER;
                new_cell1.FixedHeight = 7f;
                new_t1.AddCell(new_cell1);



                PdfPTable new_t2 = new PdfPTable(1);
                new_t2.WidthPercentage = 100f;
                Phrase head_ph2 = new Phrase("Endorsement", new Font(Font.FontFamily.HELVETICA, 7,
                 Font.BOLD));
                PdfPCell new_cell2 = new PdfPCell(head_ph2);
                new_cell2.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell2.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell2.FixedHeight = 12f;
                new_t2.AddCell(new_cell2);

                PdfPTable new_t3 = new PdfPTable(1);
                new_t3.WidthPercentage = 100f;
                Phrase head_ph3 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell3 = new PdfPCell(head_ph3);
                new_cell3.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell3.Border = Rectangle.NO_BORDER;
                new_cell3.FixedHeight = 7f;
                new_t3.AddCell(new_cell3);

                PdfPTable end1 = new PdfPTable(4);
                end1.WidthPercentage = 100f;
                end1.SetWidths(new float[] { 1, 4, 2, 6 });
                PdfPCell e11 = new PdfPCell(new Phrase("  ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell e21 = new PdfPCell(new Phrase("Name", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell e31 = new PdfPCell(new Phrase("PTW ID (blanket)", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell e51 = new PdfPCell(new Phrase("Company", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                // PdfPCell e61 = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                // PdfPCell e71 = new PdfPCell(new Phrase("Remarks", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                // PdfPCell e41 = new PdfPCell(new Phrase("Date & Time", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));

                end1.AddCell(e11);
                end1.AddCell(e21);
                end1.AddCell(e51);
                // end1.AddCell(e61);
                // end1.AddCell(e71);
                end1.AddCell(e31);
                // end1.AddCell(e41);
                da = new mydataaccess2();
                DataTable dt_riskat = new DataTable();
                dt_riskat = da.select_at_risk_data_for_report_night(ptwid, dt.Rows[i][1].ToString());

                if (dt_riskat.Rows.Count > 0)
                {
                    e11 = new PdfPCell(new Phrase("1 @ Risk ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e21 = new PdfPCell(new Phrase(dt_riskat.Rows[0][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e31 = new PdfPCell(new Phrase(ptwid, new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e51 = new PdfPCell(new Phrase(dt_riskat.Rows[0][6].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    // e61 = new PdfPCell(new Phrase(dt_riskat.Rows[0][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    // e71 = new PdfPCell(new Phrase(dt_riskat.Rows[0][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    // e41 = new PdfPCell(new Phrase(dt_riskat.Rows[0][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    end1.AddCell(e11);
                    end1.AddCell(e21);
                    end1.AddCell(e51);
                    // end1.AddCell(e61);
                    // end1.AddCell(e71);
                    end1.AddCell(e31);
                    // end1.AddCell(e41);
                }
                else
                {
                    e11 = new PdfPCell(new Phrase("1 @ Risk ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e21 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e31 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e51 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    // e61 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    // e71 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    // e41 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    end1.AddCell(e11);
                    end1.AddCell(e21);
                    end1.AddCell(e51);
                    // end1.AddCell(e61);
                    // end1.AddCell(e71);
                    end1.AddCell(e31);
                    // end1.AddCell(e41);
                }

                PdfPTable new_t20 = new PdfPTable(1);
                new_t20.WidthPercentage = 100f;
                Phrase head_ph20 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.NORMAL));
                PdfPCell new_cell20 = new PdfPCell(head_ph20);
                new_cell20.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell20.Border = Rectangle.NO_BORDER;
                new_cell20.FixedHeight = 7f;
                new_t20.AddCell(new_cell20);

                da = new mydataaccess2();
                DataTable dt_veri = new DataTable();
                dt_veri = da.ptw_select_varify_data_for_report_night(ptwid);

                PdfPTable new_t2_ = new PdfPTable(1);
                new_t2_.WidthPercentage = 100f;
                Phrase head_ph2_ = new Phrase("Approval /Rejection by  Issuer & Receiver", new Font(Font.FontFamily.HELVETICA, 7,
                Font.BOLD));
                PdfPCell new_cell2_ = new PdfPCell(head_ph2_);
                new_cell2_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell2_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell2_.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell2_.FixedHeight = 12f;
                new_t2_.AddCell(new_cell2_);

                PdfPTable new_t3_ = new PdfPTable(1);
                new_t3_.WidthPercentage = 100f;
                Phrase head_ph3_ = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell3_ = new PdfPCell(head_ph3);
                new_cell3_.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell3_.Border = Rectangle.NO_BORDER;
                new_cell3_.FixedHeight = 7f;
                new_t3_.AddCell(new_cell3_);

                PdfPTable end = new PdfPTable(5);
                end.WidthPercentage = 100f;
                end.SetWidths(new float[] { 2, 5, 6, 2, 3 });

                PdfPCell e0 = new PdfPCell(new Phrase("Route", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                e0.Colspan = 2;
                // PdfPCell e1 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell e2 = new PdfPCell(new Phrase("Name of the person", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell e3 = new PdfPCell(new Phrase("Date", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell e4 = new PdfPCell(new Phrase("Time", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));

                end.AddCell(e0);
                end.AddCell(e2);
                end.AddCell(e3);
                end.AddCell(e4);
                for (int h = 0; h < dt_veri.Rows.Count; h++)
                {
                    e0 = new PdfPCell(new Phrase(dt_veri.Rows[h][6].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                    e0.Rowspan = 2;

                    PdfPCell e1 = new PdfPCell(new Phrase("Permit Receiver ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e2 = new PdfPCell(new Phrase(dt_veri.Rows[h][5].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e3 = new PdfPCell(new Phrase(dt_veri.Rows[h][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e4 = new PdfPCell(new Phrase(dt_veri.Rows[h][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    end.AddCell(e0);
                    end.AddCell(e1);
                    end.AddCell(e2);
                    end.AddCell(e3);
                    end.AddCell(e4);


                    e1 = new PdfPCell(new Phrase("Permit Issuer ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    e2 = new PdfPCell(new Phrase(dt_veri.Rows[h][4].ToString() + " (Email: " + dt_veri.Rows[h][7].ToString() + " )", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    e3 = new PdfPCell(new Phrase(dt_veri.Rows[h][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    e4 = new PdfPCell(new Phrase(dt_veri.Rows[h][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));


                    end.AddCell(e1);
                    end.AddCell(e2);
                    end.AddCell(e3);
                    end.AddCell(e4);

                    /*  //  e0 = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                      e1 = new PdfPCell(new Phrase("Permit Receiver ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                      e2 = new PdfPCell(new Phrase(dt_veri.Rows[h][5].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                      e3 = new PdfPCell(new Phrase(dt_veri.Rows[h][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                      e4 = new PdfPCell(new Phrase(dt_veri.Rows[h][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                      //end.AddCell(e0);
                      end.AddCell(e1);
                      end.AddCell(e2);
                      end.AddCell(e3);
                      end.AddCell(e4);*/

                }


                PdfPTable new_t4 = new PdfPTable(1);
                new_t4.WidthPercentage = 100f;
                Phrase head_ph4 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.NORMAL));
                PdfPCell new_cell4 = new PdfPCell(head_ph4);
                new_cell4.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell4.Border = Rectangle.NO_BORDER;
                new_cell4.FixedHeight = 7f;
                new_t4.AddCell(new_cell4);

                PdfPTable new_t5 = new PdfPTable(1);
                new_t5.WidthPercentage = 100f;
                Phrase head_ph5 = new Phrase("PTW Closer", new Font(Font.FontFamily.HELVETICA, 7,
                 Font.BOLD));
                PdfPCell new_cell5 = new PdfPCell(head_ph5);
                new_cell5.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell5.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell5.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell5.FixedHeight = 12f;
                new_t5.AddCell(new_cell5);

                PdfPTable new_t6 = new PdfPTable(1);
                new_t6.WidthPercentage = 100f;
                Phrase head_ph6 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell6 = new PdfPCell(head_ph6);
                new_cell6.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell6.Border = Rectangle.NO_BORDER;
                new_cell6.FixedHeight = 7f;
                new_t6.AddCell(new_cell4);

                PdfPTable veri = new PdfPTable(5);
                veri.WidthPercentage = 100f;
                veri.SetWidths(new float[] { 2, 5, 6, 2, 3 });
                PdfPCell v0 = new PdfPCell(new Phrase("Route ", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                v0.Colspan = 2;
                PdfPCell v2 = new PdfPCell(new Phrase("Name of the person", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell v3 = new PdfPCell(new Phrase("Date", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                PdfPCell v4 = new PdfPCell(new Phrase("Time", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));

                veri.AddCell(v0);
                veri.AddCell(v2);
                veri.AddCell(v3);
                veri.AddCell(v4);


                da = new mydataaccess2();
                DataTable dt_confirm = new DataTable();
                dt_confirm = da.ptw_select_confirm_data_for_report_night(ptwid);

                for (int v = 0; v < dt_confirm.Rows.Count; v++)
                {
                    v0 = new PdfPCell(new Phrase(dt_confirm.Rows[v][6].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                    //v0.Rowspan = 2;


                    PdfPCell v1 = new PdfPCell(new Phrase("Permit Receiver ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    v2 = new PdfPCell(new Phrase(dt_confirm.Rows[v][5].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    v3 = new PdfPCell(new Phrase(dt_confirm.Rows[v][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    v4 = new PdfPCell(new Phrase(dt_confirm.Rows[v][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    veri.AddCell(v0);
                    veri.AddCell(v1);
                    veri.AddCell(v2);
                    veri.AddCell(v3);
                    veri.AddCell(v4);


                    /* v1 = new PdfPCell(new Phrase("Permit Issuer ", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
                    v2 = new PdfPCell(new Phrase(dt_confirm.Rows[v][4].ToString()+" (Email: "+dt_confirm.Rows[v][7].ToString()+" )", new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    v3 = new PdfPCell(new Phrase(dt_confirm.Rows[v][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

                    v4 = new PdfPCell(new Phrase(dt_confirm.Rows[v][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));


                    veri.AddCell(v1);
                    veri.AddCell(v2);

                    veri.AddCell(v3);

                    veri.AddCell(v4);*/



                }



                PdfPTable new_t8 = new PdfPTable(1);
                new_t8.WidthPercentage = 100f;
                Phrase head_ph8 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell8 = new PdfPCell(head_ph8);
                new_cell8.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell8.Border = Rectangle.NO_BORDER;
                new_cell8.FixedHeight = 7f;
                new_t8.AddCell(new_cell8);

                PdfPTable new_t9 = new PdfPTable(1);
                if (dt.Rows[0][0].ToString() == "4")
                {
                    new_t9.WidthPercentage = 100f;
                    Phrase head_ph9 = new Phrase("DEFINITION", new Font(Font.FontFamily.HELVETICA, 7,
                     Font.BOLD));
                    PdfPCell new_cell9 = new PdfPCell(head_ph9);
                    new_cell9.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell9.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    new_cell9.FixedHeight = 12f;
                    new_t9.AddCell(new_cell9);
                }
                else
                {
                    new_t9.WidthPercentage = 100f;
                    Phrase head_ph9 = new Phrase("Declaration", new Font(Font.FontFamily.HELVETICA, 7,
                     Font.BOLD));
                    PdfPCell new_cell9 = new PdfPCell(head_ph9);
                    new_cell9.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell9.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell9.VerticalAlignment = Element.ALIGN_MIDDLE;
                    new_cell9.FixedHeight = 12f;
                    new_t9.AddCell(new_cell9);
                }


                Phrase head_ph_def_head = new Phrase("Permit Issuer:", new Font(Font.FontFamily.HELVETICA, 7,
                Font.BOLD));
                Phrase head_ph_def = new Phrase("Person issuing this work permit to the employee/contractor in order to ensure the travel is done safely", new Font(Font.FontFamily.HELVETICA, 7,
                Font.NORMAL));

                Phrase head_ph_def2_head = new Phrase("\n Permit Receiver:", new Font(Font.FontFamily.HELVETICA, 7,
                Font.BOLD));
                Phrase head_ph_def2 = new Phrase("Person doing the risk assessment for travelling in 2 wheelers/4 wheelers. \n 1@isk : Driver of  2 wheelers/4 wheelers", new Font(Font.FontFamily.HELVETICA, 7,
                Font.NORMAL));
                Phrase head_ph_def2_head_ = new Phrase();
                if (dt.Rows[0][0].ToString() == "4")
                {
                    head_ph_def2_head_ = new Phrase("\n Sector shutdown / power down: Switching off RF transmission from the antenna.", new Font(Font.FontFamily.HELVETICA, 7,
                     Font.BOLD));
                }
                PdfPTable new_t10 = new PdfPTable(1);
                new_t10.WidthPercentage = 100f;
                Phrase head_ph10 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
                 Font.BOLD));
                PdfPCell new_cell10 = new PdfPCell(head_ph10);
                new_cell10.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell10.Border = Rectangle.NO_BORDER;
                new_cell10.FixedHeight = 7f;
                new_t10.AddCell(new_cell10);

                PdfPTable new_t11 = new PdfPTable(1);
                new_t11.WidthPercentage = 100f;
                Phrase head_ph11 = new Phrase("CAUTION", new Font(Font.FontFamily.HELVETICA, 7,
                 Font.BOLD));
                PdfPCell new_cell11 = new PdfPCell(head_ph11);
                new_cell11.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell11.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell11.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell11.FixedHeight = 12f;
                new_t11.AddCell(new_cell11);
                Phrase head_ph_def3 = new Phrase();
                if (dt.Rows[0][0].ToString() == "4")
                {
                    head_ph_def3 = new Phrase("1. This permit does not indicate that the job/site is absolutely safe, but indicates the condition in which the equipment/site is being handed over to the Receiver for carrying out the work & what safety precautions have to be observed by the Receiver \n 2. In the event of conditions becoming unsafe in the immediate vicinity, the permit becomes invalid & must be revalidated \n 3. If there is a change in responsible persons (Issuer/Receiver/Workers) or in the activity as mentioned in the permit, the permit ceases automatically to be valid & in such case new permit must be issued and old permit must be cancelled \n 4. Where the job is an extension or alteration of an existing installation, it must be verified that the extension or alteration complies with this part of the Regulations and does not impair the safety of the existing installation \n 5.No work to be allowed in case of low visibility", new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL));
                }
                else
                {
                    head_ph_def3 = new Phrase("1. In the event of conditions becoming unsafe in the immediate vicinity, the permit becomes invalid & must be revalidated. \n 2.	If there is a change in respective person (Issuer/Receiver/Driver) or in the activity as mentioned in the permit, the permit automatically ceases to be valid &  in such case new permit must be issued and old permit must be cancelled ", new Font(Font.FontFamily.HELVETICA, 7,
                   Font.NORMAL));
                }


                PdfPTable img = new PdfPTable(2);
                img.WidthPercentage = 100f;
                iTextSharp.text.Image image_last;
                if (dt.Rows[0][0].ToString() == "1")
                {
                    image_last = iTextSharp.text.Image.GetInstance(imag_file1);
                    image_last.SetAbsolutePosition(0, 100);
                    image_last.ScaleAbsolute(20f, 20f);
                }
                else
                {
                    image_last = iTextSharp.text.Image.GetInstance(imag_file2);
                    image_last.SetAbsolutePosition(0, 100);
                    image_last.ScaleAbsolute(20f, 20f);
                }






                PdfPCell last = new PdfPCell(image_last);
                last.Border = Rectangle.NO_BORDER;
                last.HorizontalAlignment = Rectangle.ALIGN_LEFT;
                last.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
                if (dt.Rows[0][0].ToString() == "1")
                {
                    last.FixedHeight = 160f;
                }
                if (dt.Rows[0][0].ToString() == "2")
                {
                    last.FixedHeight = 100f;
                }
                img.AddCell(last);

                PdfPCell last_voda = new PdfPCell(new Phrase("Vodafone Idea Ltd.", new Font(Font.FontFamily.HELVETICA, 8,
                Font.NORMAL, iTextSharp.text.BaseColor.ORANGE)));
                last_voda.Border = Rectangle.NO_BORDER;
                last_voda.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
                last_voda.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
                if (dt.Rows[0][0].ToString() == "1")
                {
                    last_voda.FixedHeight = 160f;
                }
                if (dt.Rows[0][0].ToString() == "2")
                {
                    last_voda.FixedHeight = 100f;
                }
                img.AddCell(last_voda);



                if (dt.Rows[0][0].ToString() == "5")
                {
                    doc.Add(head);
                    doc.Add(t1);
                    doc.Add(t2);
                    doc.Add(p2);
                    doc.Add(p2_11);
                    doc.Add(p2_1);
                    doc.Add(site_name);
                    doc.Add(site_data);
                    doc.Add(loc_name);
                    doc.Add(loc_data);
                    doc.Add(vehicle_reg);
                    doc.Add(vehicle_reg_data);
                    doc.Add(sr_name);
                    doc.Add(sr_data);
                    doc.Add(issue_name);
                    doc.Add(issue_date);
                    doc.Add(time_name);
                    doc.Add(time_data);
                    doc.Add(purpose_name);
                    doc.Add(purpose_data);
                    doc.Add(from_name);
                    doc.Add(from_data);
                    doc.Add(to_name);
                    doc.Add(to_data);
                    doc.Add(new_line);
                    doc.Add(work);
                    doc.Add(work_data);
                    doc.Add(mwfm);
                    doc.Add(mwfm_data);
                    doc.Add(number);
                    doc.Add(number_data);
                    doc.Add(new_t);
                    doc.Add(new_t7);
                    doc.Add(log);
                    doc.Add(new_t1);
                    doc.Add(new_t2);
                    doc.Add(new_t3);
                    doc.Add(end1);
                    doc.Add(new_t20);
                    doc.Add(new_t2_);
                    doc.Add(new_t3_);
                    doc.Add(end);
                    doc.Add(new_t4);
                    doc.Add(new_t5);
                    doc.Add(new_t6);
                    doc.Add(veri);
                    doc.Add(new_t8);
                    doc.Add(new_t9);
                    // doc.Add(new_t10);
                    doc.Add(head_ph_def_head);
                    doc.Add(head_ph_def);
                    doc.Add(head_ph_def2_head);
                    doc.Add(head_ph_def2);
                    doc.Add(new_t10);
                    doc.Add(new_t11);
                    doc.Add(head_ph_def3);
                    // doc.Add(img);

                    PdfContentByte cb = writer.DirectContent;
                    if (dt.Rows[0][0].ToString() == "1")
                    {
                        footer = iTextSharp.text.Image.GetInstance(imag_file1);
                    }
                    if (dt.Rows[0][0].ToString() == "2")
                    {
                        footer = iTextSharp.text.Image.GetInstance(imag_file2);
                    }
                    if (dt.Rows[0][0].ToString() == "3")
                    {
                        footer = iTextSharp.text.Image.GetInstance(imag_file3);
                    }
                    if (dt.Rows[0][0].ToString() == "4")
                    {
                        footer = iTextSharp.text.Image.GetInstance(imag_file4);
                    }
                    if (dt.Rows[0][0].ToString() == "5")
                    {
                        footer = iTextSharp.text.Image.GetInstance(imag_file5);
                    }
                    footer.SetAbsolutePosition(0, 0);
                    footer.ScaleAbsolute(30f, 30f);
                    PdfContentByte byte1 = writer.DirectContent;
                    PdfTemplate tp1 = cb.CreateTemplate(600, 150);


                    var red_footer = FontFactory.GetFont("HELVETICA", 7, BaseColor.ORANGE);


                    string str = "Vodafone Idea Ltd.";
                    template = cb.CreateTemplate(10, 10);
                    bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

                    cb.BeginText();
                    cb.SetFontAndSize(bf, 8);
                    cb.SetTextMatrix(rec.GetRight(150), rec.GetBottom(40));
                    // cb.SetColorFill(BaseColor.ORANGE);

                    cb.ShowText(str);
                    tp1.AddImage(footer);
                    cb.EndText();

                    cb.AddTemplate(template, doc.Right, doc.Bottom - 15);
                    cb.AddTemplate(tp1, doc.Left, doc.Bottom - 5);


                }



                doc.NewPage();
            }

            //doc.Add(site);





            doc.NewPage();

            //=================================================Mahesh=================================================


            Rectangle rec1 = doc.PageSize;

            PdfPTable head1 = new PdfPTable(5);
            head1.WidthPercentage = 100f;

            head1.SetWidths(new float[] { 1, 4, 5, 4, 1 });

            head1.DefaultCell.Border = 0;

            if (dt.Rows[0][0].ToString() == "1")
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file1);
                image.SetAbsolutePosition(0, 100);
                image.ScaleAbsolute(20f, 20f);



                //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                //image_voda.SetAbsolutePosition(0, 0);
                //image_voda.ScaleAbsolute(35f, 25f);



                PdfPCell c = new PdfPCell(image);
                c.FixedHeight = 30f;
                c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.VerticalAlignment = Element.ALIGN_MIDDLE;
                c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1 = new PdfPCell();
                c1.FixedHeight = 30f;
                c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1.VerticalAlignment = Element.ALIGN_RIGHT;
                c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_ = new PdfPCell();
                c1_.FixedHeight = 30f;
                c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                c1_.VerticalAlignment = Element.ALIGN_CENTER;
                c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_1 = new PdfPCell();
                c1_1.FixedHeight = 30f;
                c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_11 = new PdfPCell();
                c1_11.FixedHeight = 30f;
                c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                head1.AddCell(c);
                head1.AddCell(c1);
                head1.AddCell(c1_);
                head1.AddCell(c1_1);
                head1.AddCell(c1_11);
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file2);
                image.SetAbsolutePosition(0, 100);
                image.ScaleAbsolute(20f, 20f);



                //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                //image_voda.SetAbsolutePosition(0, 0);
                //image_voda.ScaleAbsolute(35f, 25f);



                PdfPCell c = new PdfPCell(image);
                c.FixedHeight = 30f;
                c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.VerticalAlignment = Element.ALIGN_MIDDLE;
                c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1 = new PdfPCell();
                c1.FixedHeight = 30f;
                c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1.VerticalAlignment = Element.ALIGN_RIGHT;
                c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_ = new PdfPCell();
                c1_.FixedHeight = 30f;
                c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                c1_.VerticalAlignment = Element.ALIGN_CENTER;
                c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_1 = new PdfPCell();
                c1_1.FixedHeight = 30f;
                c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_11 = new PdfPCell();
                c1_11.FixedHeight = 30f;
                c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                head1.AddCell(c);
                head1.AddCell(c1);
                head1.AddCell(c1_);
                head1.AddCell(c1_1);
                head1.AddCell(c1_11);
            }

            if (dt.Rows[0][0].ToString() == "3")
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file3);
                image.SetAbsolutePosition(0, 100);
                image.ScaleAbsolute(20f, 20f);



                //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                //image_voda.SetAbsolutePosition(0, 0);
                //image_voda.ScaleAbsolute(35f, 25f);



                PdfPCell c = new PdfPCell(image);
                c.FixedHeight = 30f;
                c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.VerticalAlignment = Element.ALIGN_MIDDLE;
                c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1 = new PdfPCell();
                c1.FixedHeight = 30f;
                c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1.VerticalAlignment = Element.ALIGN_RIGHT;
                c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_ = new PdfPCell();
                c1_.FixedHeight = 30f;
                c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                c1_.VerticalAlignment = Element.ALIGN_CENTER;
                c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_1 = new PdfPCell();
                c1_1.FixedHeight = 30f;
                c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_11 = new PdfPCell();
                c1_11.FixedHeight = 30f;
                c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                head1.AddCell(c);
                head1.AddCell(c1);
                head1.AddCell(c1_);
                head1.AddCell(c1_1);
                head1.AddCell(c1_11);
            }

            if (dt.Rows[0][0].ToString() == "4")
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file4);
                image.SetAbsolutePosition(0, 100);
                image.ScaleAbsolute(20f, 20f);



                //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                //image_voda.SetAbsolutePosition(0, 0);
                //image_voda.ScaleAbsolute(35f, 25f);



                PdfPCell c = new PdfPCell(image);
                c.FixedHeight = 30f;
                c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.VerticalAlignment = Element.ALIGN_MIDDLE;
                c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1 = new PdfPCell();
                c1.FixedHeight = 30f;
                c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1.VerticalAlignment = Element.ALIGN_RIGHT;
                c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_ = new PdfPCell();
                c1_.FixedHeight = 30f;
                c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                c1_.VerticalAlignment = Element.ALIGN_CENTER;
                c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_1 = new PdfPCell();
                c1_1.FixedHeight = 30f;
                c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_11 = new PdfPCell();
                c1_11.FixedHeight = 30f;
                c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                head1.AddCell(c);
                head1.AddCell(c1);
                head1.AddCell(c1_);
                head1.AddCell(c1_1);
                head1.AddCell(c1_11);
            }
            if (dt.Rows[0][0].ToString() == "5")
            {
                iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imag_file5);
                image.SetAbsolutePosition(0, 100);
                image.ScaleAbsolute(20f, 20f);



                //iTextSharp.text.Image image_voda = iTextSharp.text.Image.GetInstance(vlogo);
                //image_voda.SetAbsolutePosition(0, 0);
                //image_voda.ScaleAbsolute(35f, 25f);



                PdfPCell c = new PdfPCell(image);
                c.FixedHeight = 30f;
                c.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c.HorizontalAlignment = Element.ALIGN_CENTER;
                c.VerticalAlignment = Element.ALIGN_MIDDLE;
                c.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1 = new PdfPCell();
                c1.FixedHeight = 30f;
                c1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1.VerticalAlignment = Element.ALIGN_RIGHT;
                c1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_ = new PdfPCell();
                c1_.FixedHeight = 30f;
                c1_.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_.HorizontalAlignment = Element.ALIGN_CENTER;
                c1_.VerticalAlignment = Element.ALIGN_CENTER;
                c1_.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_1 = new PdfPCell();
                c1_1.FixedHeight = 30f;
                c1_1.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c1_1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_1.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                PdfPCell c1_11 = new PdfPCell();
                c1_11.FixedHeight = 30f;
                c1_11.BackgroundColor = iTextSharp.text.BaseColor.ORANGE;
                c1_11.HorizontalAlignment = Element.ALIGN_LEFT;
                c1_11.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1_11.BorderColor = iTextSharp.text.BaseColor.ORANGE;

                head1.AddCell(c);
                head1.AddCell(c1);
                head1.AddCell(c1_);
                head1.AddCell(c1_1);
                head1.AddCell(c1_11);
            }

            PdfPTable t1_b = new PdfPTable(1);
            t1_b.WidthPercentage = 100f;
            Phrase p1_b = new Phrase("Annexure - B", new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE));
            PdfPCell c2_b = new PdfPCell(p1_b);
            c2_b.HorizontalAlignment = Element.ALIGN_CENTER;
            c2_b.BorderColor = iTextSharp.text.BaseColor.WHITE;
            t1_b.AddCell(c2_b);


            PdfPTable t1_mahesh = new PdfPTable(1);
            t1_mahesh.WidthPercentage = 100f;
            if (dt.Rows[0][0].ToString() == "1")
            {
                p1_mahesh = new Phrase("Risk Assessment Template - Application For PTW " + "@ Height \n", new Font(Font.FontFamily.TIMES_ROMAN, 11));
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                p1_mahesh = new Phrase("Risk Assessment Template - Application For PTW " + "for Electrical \n", new Font(Font.FontFamily.TIMES_ROMAN, 11));
            }
            if (dt.Rows[0][0].ToString() == "3")
            {
                p1_mahesh = new Phrase("Risk Assessment Template - Application For PTW " + "for OFC Job \n", new Font(Font.FontFamily.TIMES_ROMAN, 11));
            }
            if (dt.Rows[0][0].ToString() == "4")
            {
                p1_mahesh = new Phrase("Risk Assessment Template - Application For PTW " + " - RF Energy Isolation \n", new Font(Font.FontFamily.TIMES_ROMAN, 11));
            }
            if (dt.Rows[0][0].ToString() == "5")
            {
                p1_mahesh = new Phrase("Risk Assessment Template - Application For T@DH \n", new Font(Font.FontFamily.TIMES_ROMAN, 11));
            }
            PdfPCell c2_mahesh = new PdfPCell(p1_mahesh);
            c2_mahesh.Padding = (10.0f);
            c2_mahesh.HorizontalAlignment = Element.ALIGN_CENTER;
            c2_mahesh.BorderColor = iTextSharp.text.BaseColor.WHITE;
            t1_mahesh.AddCell(c2_mahesh);

            PdfPTable t2_mahesh = new PdfPTable(1);
            t2_mahesh.WidthPercentage = 100f;
            t2_mahesh.DefaultCell.Border = 3;
            t2_mahesh.DefaultCell.BorderColor = iTextSharp.text.BaseColor.BLACK;


            Font blackListTextFont = FontFactory.GetFont("HELVETICA", 8);
            Font blackListTextFont1 = FontFactory.GetFont("HELVETICA", 10);
            Font redListTextFont = FontFactory.GetFont("HELVETICA", 6);



            if (dt.Rows[0][0].ToString() == "1")
            {
                titleChunk = new Chunk("Risk Assessment Template - Work @ Height\n", blackListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                titleChunk = new Chunk("Risk Assessment Template - Work for Electrical\n", blackListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "3")
            {
                titleChunk = new Chunk("Risk Assessment Template - Work for OFC\n", blackListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "4")
            {
                titleChunk = new Chunk("Risk Assessment Template – RF Work\n", blackListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "5")
            {
                titleChunk = new Chunk("Risk Assessment Template – Travel during dark hours \n", blackListTextFont);
            }
            Chunk descriptionChunk1 = new Chunk("", redListTextFont);
            Chunk descriptionChunk = new Chunk();
            if (dt.Rows[0][0].ToString() == "1")
            {
                descriptionChunk = new Chunk("\nThis assessment form is to be used to assess risk related to Work @ Height and propose action to control the risks\n", redListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                descriptionChunk = new Chunk("\nThis assessment form is to be used to assess risk related to Work for Electrical and propose action to control the risks\n", redListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "3")
            {
                descriptionChunk = new Chunk("\nThis assessment form is to be used to assess risk related to Work for OFC and propose action to control the risks\n", redListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "4")
            {
                descriptionChunk = new Chunk("\nThis assessment form is to be used to assess risk related to RF Works\n", redListTextFont);
            }
            if (dt.Rows[0][0].ToString() == "5")
            {
                descriptionChunk = new Chunk("\nThis assessment form is to be used to assess all risks related to travel during dark hours\n", redListTextFont);
            }


            Phrase phrase = new Phrase(titleChunk);
            phrase.Add(descriptionChunk);
            phrase.Add(descriptionChunk1);


            t2_mahesh.AddCell(new PdfPCell(phrase));



            //-----------------------------
            PdfPTable blanktb = new PdfPTable(1);
            blanktb.WidthPercentage = 100f;
            Phrase blank_ph5 = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
            PdfPCell blank_cell4 = new PdfPCell(blank_ph5);
            blank_cell4.VerticalAlignment = Element.ALIGN_MIDDLE;
            blank_cell4.Border = Rectangle.NO_BORDER;
            blank_cell4.FixedHeight = 7f;
            blanktb.AddCell(blank_cell4);

            //-----------------------------

            da = new mydataaccess2();
            DataTable dt_log3 = new DataTable();
            dt_log3 = da.ptw_Select_site_data_report_night_risk(ptwid);

            da = new mydataaccess2();
            DataTable dt_assessby = new DataTable();
            dt_assessby = da.select_at_risk_data_for_report_approved(ptwid);
            string issuedto = "";
            if (dt_assessby.Rows.Count > 0)
            {
                for (int a = 0; a < dt_assessby.Rows.Count; a++)
                {
                    issuedto = issuedto + dt_assessby.Rows[a][0].ToString() + ",";
                }
                issuedto = issuedto.Remove(issuedto.Length - 1, 1);
            }
            else
            {
                issuedto = "N/A";
            }
            doc.Add(head1);
            doc.Add(t1_b);
            doc.Add(t1_mahesh);
            doc.Add(t2_mahesh);


            for (int m = 0; m < dt_log3.Rows.Count; m++)
            {

                PdfPTable table = new PdfPTable(4);
                table.WidthPercentage = 100f;
                table.SetWidths(new float[] { 15, 15, 15, 15 });
                PdfPCell line1 = new PdfPCell();
                PdfPCell line2 = new PdfPCell();
                PdfPCell line3 = new PdfPCell();
                PdfPCell line4 = new PdfPCell();
                PdfPCell line5 = new PdfPCell();

                PdfPCell line6 = new PdfPCell();
                PdfPCell line7 = new PdfPCell();




                Paragraph d1 = new Paragraph(dt_log3.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));
                Paragraph d2 = new Paragraph("Purpose of work : " + new Chunk(dt_log3.Rows[m][1].ToString()), new Font(Font.FontFamily.HELVETICA, 7));
                Paragraph d3 = new Paragraph("Start date & time : " + new Chunk(dt_log3.Rows[m][2].ToString()), new Font(Font.FontFamily.HELVETICA, 7));
                Paragraph d4 = new Paragraph("End date & time : " + new Chunk(dt_log3.Rows[m][3].ToString()), new Font(Font.FontFamily.HELVETICA, 7));
                Paragraph d5 = new Paragraph("PTW No : " + new Chunk(dt_log3.Rows[m][4].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)), new Font(Font.FontFamily.HELVETICA, 7));
                Paragraph d6 = new Paragraph("Assessed by : " + new Chunk(dt_log3.Rows[m][5].ToString()), new Font(Font.FontFamily.HELVETICA, 7));
                Paragraph d7 = new Paragraph("Issued by :" + new Chunk(dt_log3.Rows[m][6].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)), new Font(Font.FontFamily.HELVETICA, 7));

                //risk1 = new PdfPCell(new Phrase((k + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD)));
                line1 = new PdfPCell(d1);
                line1.Padding = (4.0f);
                line2 = new PdfPCell(d2);
                line2.Padding = (4.0f);
                line3 = new PdfPCell(d3);
                line3.Padding = (4.0f);
                line4 = new PdfPCell(d4);
                line4.Padding = (4.0f);
                line5 = new PdfPCell(d5);
                line5.Padding = (4.0f);
                line5.Colspan = 2;
                line6 = new PdfPCell(d6);
                line6.Padding = (4.0f);
                line7 = new PdfPCell(d7);
                line7.Padding = (4.0f);


                table.AddCell(line1);
                table.AddCell(line2);
                table.AddCell(line3);
                table.AddCell(line4);
                table.AddCell(line5);
                table.AddCell(line6);
                table.AddCell(line7);
                doc.Add(table);
            }


            //----------------------------

            PdfPTable new_t4_ = new PdfPTable(1);
            new_t4_.WidthPercentage = 100f;
            Phrase head_ph4_ = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
             Font.NORMAL));
            PdfPCell new_cell4_ = new PdfPCell(head_ph4_);
            new_cell4_.VerticalAlignment = Element.ALIGN_MIDDLE;
            new_cell4_.Border = Rectangle.NO_BORDER;
            new_cell4_.FixedHeight = 7f;
            new_t4_.AddCell(new_cell4_);

            PdfPTable new_t5_ = new PdfPTable(1);
            new_t5_.WidthPercentage = 100f;
            Phrase head_ph5_ = new Phrase("Issuer Details", new Font(Font.FontFamily.HELVETICA, 7,
             Font.BOLD));
            PdfPCell new_cell5_ = new PdfPCell(head_ph5_);
            new_cell5_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            new_cell5_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
            new_cell5_.VerticalAlignment = Element.ALIGN_MIDDLE;
            new_cell5_.FixedHeight = 12f;
            new_t5_.AddCell(new_cell5_);

            PdfPTable new_t6_ = new PdfPTable(1);
            new_t6_.WidthPercentage = 100f;
            Phrase head_ph6_ = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6,
             Font.BOLD));
            PdfPCell new_cell6_ = new PdfPCell(head_ph6_);
            new_cell6_.VerticalAlignment = Element.ALIGN_MIDDLE;
            new_cell6_.Border = Rectangle.NO_BORDER;
            new_cell6_.FixedHeight = 7f;
            new_t6_.AddCell(new_cell6_);

            PdfPTable veri_ = new PdfPTable(4);
            veri_.WidthPercentage = 100f;
            veri_.SetWidths(new float[] { 2, 2, 2, 6 });
            PdfPCell v0_ = new PdfPCell(new Phrase("Verical/Functional Head Name", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            PdfPCell v2_ = new PdfPCell(new Phrase("Company", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            PdfPCell v3_ = new PdfPCell(new Phrase("Authority", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            PdfPCell v4_ = new PdfPCell(new Phrase("If No-Remark", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));

            veri_.AddCell(v0_);
            veri_.AddCell(v2_);
            veri_.AddCell(v3_);
            veri_.AddCell(v4_);


            da = new mydataaccess2();
            DataTable dt_issuer = new DataTable();
            dt_issuer = da.ptw_select_issuer_details_report_night(ptwid);


            v0_ = new PdfPCell(new Phrase(dt_issuer.Rows[0][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            v2_ = new PdfPCell(new Phrase(dt_issuer.Rows[0][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
            v3_ = new PdfPCell(new Phrase(dt_issuer.Rows[0][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));
            v4_ = new PdfPCell(new Phrase(dt_issuer.Rows[0][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL)));

            veri_.AddCell(v0_);
            veri_.AddCell(v2_);
            veri_.AddCell(v3_);
            veri_.AddCell(v4_);

            doc.Add(new_t4_);
            doc.Add(new_t5_);
            doc.Add(new_t6_);
            doc.Add(veri_);


            da = new mydataaccess2();
            DataTable dt_log2 = new DataTable();
            dt_log2 = da.ptw_select_risk_data_for_report(ptwid, 1, "");


            PdfPTable question = new PdfPTable(4);
            question.WidthPercentage = 100f;
            question.SetWidths(new float[] { 2, 15, 3, 15 });
            PdfPCell risk1 = new PdfPCell(new Phrase("No.", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            risk1.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell risk2 = new PdfPCell(new Phrase("Risks involved", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            risk2.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell risk3 = new PdfPCell(new Phrase("Risk Level", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            risk3.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell risk4 = new PdfPCell(new Phrase("Remarks", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
            risk4.HorizontalAlignment = Element.ALIGN_CENTER;
            question.AddCell(risk1);
            question.AddCell(risk2);
            question.AddCell(risk3);
            question.AddCell(risk4);

            for (int k = 0; k < dt_log2.Rows.Count; k++)
            {
                risk1 = new PdfPCell(new Phrase((k + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6)));
                risk2 = new PdfPCell(new Phrase(dt_log2.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6)));
                risk3 = new PdfPCell(new Phrase(dt_log2.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6)));
                risk3.HorizontalAlignment = Element.ALIGN_CENTER;
                risk4 = new PdfPCell(new Phrase(dt_log2.Rows[k][2].ToString(), new Font(Font.FontFamily.HELVETICA, 6)));
                risk1.HorizontalAlignment = Element.ALIGN_CENTER;
                question.AddCell(risk1);
                question.AddCell(risk2);
                question.AddCell(risk3);
                question.AddCell(risk4);

            }


            doc.Add(blanktb);
            //doc.Add(table1);
            doc.Add(question);
            //-------------------------

            PdfPTable matiga_t3 = new PdfPTable(1);
            matiga_t3.WidthPercentage = 100f;
            Phrase head_ph3_mahesh = new Phrase("", new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD));
            PdfPCell matiga_cell3 = new PdfPCell(head_ph3_mahesh);
            matiga_cell3.VerticalAlignment = Element.ALIGN_MIDDLE;
            matiga_cell3.Border = Rectangle.NO_BORDER;
            matiga_cell3.FixedHeight = 7f;
            matiga_t3.AddCell(matiga_cell3);




            Phrase head_ph_met3 = new Phrase();
            if (dt.Rows[0][0].ToString() == "1")
            {
                head_ph_met3 = new Phrase("\n Note:-Ensure that High risks are reduced to as low as reasonably possible risk levels by appropriate actions before undertaking work on height", new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD | Font.UNDERLINE | Font.ITALIC));
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                head_ph_met3 = new Phrase("\n Note:-Ensure that High risks are reduced to as low as reasonably possible risk levels by appropriate actions before undertaking work on electrical", new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD | Font.UNDERLINE | Font.ITALIC));
            }
            if (dt.Rows[0][0].ToString() == "3")
            {
                head_ph_met3 = new Phrase("\n Note:-Ensure that High risks are reduced to as low as reasonably possible risk levels by appropriate actions before undertaking work for OFC", new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD | Font.UNDERLINE | Font.ITALIC));
            }
            if (dt.Rows[0][0].ToString() == "4")
            {
                head_ph_met3 = new Phrase("\n Note:-Ensure that High risks are reduced to as low as reasonably possible risk levels by appropriate actions before undertaking work for RF Work", new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD | Font.UNDERLINE | Font.ITALIC));
            }


            doc.Add(head_ph_met3);
            //--------------------photo
            #region photo_add
            doc.NewPage();
            da = new mydataaccess2();
            DataSet dt_photo = new DataSet();
            dt_photo = da.select_photos_pdf_ptw(p_id.ToString(), category);
            DataTable dr = new DataTable();
            DataTable dp = new DataTable();
            dr = dt_photo.Tables[0];
            dp = dt_photo.Tables[1];
            List<DataTable> result = dp.AsEnumerable().GroupBy(row => row.Field<string>("p_name")).Select(g => g.CopyToDataTable()).ToList();
            #region for_risk
            /*for (int i = 0; i < dr.Rows.Count; i++)
            {*/
            if (dr.Rows.Count > 0)//------------------------------------
            {
                PdfPTable new_t1 = new PdfPTable(1);
                Phrase head_ph_;
                new_t1.WidthPercentage = 100f;
                head_ph_ = new Phrase("Risk Assessment Template :", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD));
                PdfPCell new_cell_ = new PdfPCell(head_ph_);
                new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                new_cell_.FixedHeight = 20f;
                new_t1.AddCell(new_cell_);
                Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                ));

                doc.Add(new_t1);
                //doc.Add(break2);
                /*}*/

                PdfPTable photo = new PdfPTable(4);
                PdfPCell photo_cell = new PdfPCell();
                photo.WidthPercentage = 100;
                int flag = 0;
                for (int p = 0; p < dr.Rows.Count; p++)
                {
                    if (flag == 4)
                    {
                        flag = 0;
                    }
                    #region try_catch
                    try
                    {
                        PdfPTable pp = new PdfPTable(1);
                        Phrase name = new Phrase(dr.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 7));
                        PdfPCell ppc = new PdfPCell(name);
                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                        pp.AddCell(ppc);
                        //  imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                     //   imag_ptw = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\pics\\" + dr.Rows[p][0].ToString();
                        imag_ptw = "D:\\Inetpub\\wwwroot\\production\\Copy of Vodafone_production_for_deploy\\hsw_final_06_11_2014\\hsw_final_06_11_2014\\pics\\" + dr.Rows[p][0].ToString();
                        iTextSharp.text.Image image_photo;
                        image_photo = iTextSharp.text.Image.GetInstance(imag_ptw);
                        image_photo.SetAbsolutePosition(50, 50);
                        image_photo.ScaleAbsolute(70f, 70f);
                        pp.AddCell(image_photo);
                        photo_cell = new PdfPCell(pp);
                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        // photo_cell.FixedHeight = 220;
                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                        photo.AddCell(photo_cell);

                        if (dr.Rows.Count % 4 == 0)
                        {
                        }
                        else
                        {
                            if (p == dr.Rows.Count - 1)
                            {
                                photo_cell = new PdfPCell();
                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                photo.AddCell(photo_cell);
                                flag++;

                                for (int b = 0; b < 4 - flag; b++)
                                {
                                    photo_cell = new PdfPCell();
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                }
                            }
                            else
                            {
                                flag++;
                            }
                        }
                    }
                    catch
                    {
                        photo_cell = new PdfPCell();
                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                        photo.AddCell(photo_cell);
                    }
                    #endregion try_catch
                }

                doc.Add(photo);
                Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                doc.Add(break23);
                //---------------------------
            }
            Phrase break24 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 25));
            doc.Add(break24);
            /*}*/
            #endregion for_risk
            #region for_permit
            for (int j = 0; j < result.Count; j++)
            {
                /*for (int i = 0; i < result[j].Rows.Count; i++)
                {*/
                if (result[j].Rows.Count > 0)//------------------------------------
                {

                    PdfPTable new_t1 = new PdfPTable(1);
                    Phrase head_ph_;
                    new_t1.WidthPercentage = 100f;
                    head_ph_ = new Phrase("Permit Receiver : " + result[j].Rows[result[j].Rows.Count - 1]["p_name"].ToString(), new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD));
                    PdfPCell new_cell_ = new PdfPCell(head_ph_);
                    new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                    new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                    new_cell_.FixedHeight = 20f;
                    new_t1.AddCell(new_cell_);
                    Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                    ));

                    doc.Add(new_t1);
                    //doc.Add(break2);
                    /*}*/

                    PdfPTable photo = new PdfPTable(4);
                    PdfPCell photo_cell = new PdfPCell();
                    photo.WidthPercentage = 100;
                    int flag = 0;
                    for (int p = 0; p < result[j].Rows.Count; p++)
                    {
                        if (flag == 4)
                        {
                            flag = 0;
                        }
                        #region try_catch
                        try
                        {
                            PdfPTable pp = new PdfPTable(1);
                            Phrase name = new Phrase(result[j].Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 7));
                            PdfPCell ppc = new PdfPCell(name);
                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                            pp.AddCell(ppc);
                            // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                          //  imag_ptw = "D:\\Vodafone\\PTW_TESTING_CODE\\PTW_TESTING_CODE\\pics\\" + result[j].Rows[p][0].ToString();
                            imag_ptw = "D:\\Inetpub\\wwwroot\\production\\Copy of Vodafone_production_for_deploy\\hsw_final_06_11_2014\\hsw_final_06_11_2014\\pics\\" + result[j].Rows[p][0].ToString();
                            iTextSharp.text.Image image_photo;
                            image_photo = iTextSharp.text.Image.GetInstance(imag_ptw);
                            image_photo.SetAbsolutePosition(50, 50);
                            image_photo.ScaleAbsolute(70f, 70f);
                            pp.AddCell(image_photo);
                            photo_cell = new PdfPCell(pp);
                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            // photo_cell.FixedHeight = 220;
                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                            photo.AddCell(photo_cell);

                            if (result[j].Rows.Count % 4 == 0)
                            {
                            }
                            else
                            {
                                if (p == result[j].Rows.Count - 1)
                                {
                                    photo_cell = new PdfPCell();
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                    flag++;

                                    for (int b = 0; b < 4 - flag; b++)
                                    {
                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                    }
                                }
                                else
                                {
                                    flag++;
                                }
                            }
                        }
                        catch
                        {
                            photo_cell = new PdfPCell();
                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                            photo.AddCell(photo_cell);
                        }
                        #endregion try_catch
                    }

                    doc.Add(photo);
                    Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                    doc.Add(break23);
                    //---------------------------
                }
                Phrase break25 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 25));
                doc.Add(break25);
                /*}*/
            }
            #endregion for_permit
            #endregion photo_add
            //--------------------photo


 //---Checklist

            da = new mydataaccess2();
             dt_log2 = new DataTable();
            dt_log2 = da.ptw_select_checklist_pdf(ptwid);

            if (dt_log2.Rows.Count > 0)
            {
                doc.NewPage();
                doc.Add(head1);
                PdfPTable t1 = new PdfPTable(1);
                t1.WidthPercentage = 100f;
                iTextSharp.text.Font f = new iTextSharp.text.Font();
                f.SetStyle("underline");
                f.Size = 7;
                // f.SetStyle("bold");
                Phrase p1 = new Phrase("Annexure - C", f);

                PdfPCell c2 = new PdfPCell(p1);
                c2.HorizontalAlignment = Element.ALIGN_CENTER;
                c2.BorderColor = iTextSharp.text.BaseColor.WHITE;
                t1.AddCell(c2);


                PdfPTable t2 = new PdfPTable(1);
                t2.DefaultCell.Border = 3;
                t2.WidthPercentage = 100f;
                t2.DefaultCell.BorderColor = iTextSharp.text.BaseColor.WHITE;

                //iTextSharp.text.Image image_voda1 = iTextSharp.text.Image.GetInstance(Server.MapPath("vlogo.png"));
                //image_voda1.SetAbsolutePosition(100, 5);
                //image_voda1.ScaleAbsolute(45f, 35f);
                //image_voda1.Alignment = iTextSharp.text.Image.ALIGN_RIGHT;
                PdfPCell c2_1 = new PdfPCell();
                c2_1.HorizontalAlignment = Element.ALIGN_RIGHT;
                c2_1.BorderColor = iTextSharp.text.BaseColor.WHITE;

                t2.AddCell(c2_1);
                doc.Add(t1);
              //  doc.Add(t2);
                question1.WidthPercentage = 100f;
                question1.SetWidths(new float[] { 2, 15, 3, 15 });
                PdfPCell risk1_check = new PdfPCell(new Phrase("No.", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                risk1_check.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell risk2_check = new PdfPCell(new Phrase("Checklist", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                risk2_check.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell risk3_check = new PdfPCell(new Phrase("Answer (YES/NO/NA)", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                risk3_check.HorizontalAlignment = Element.ALIGN_CENTER;
                PdfPCell risk4_check = new PdfPCell(new Phrase("Remarks", new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD)));
                risk4_check.HorizontalAlignment = Element.ALIGN_CENTER;
                question1.AddCell(risk1_check);
                question1.AddCell(risk2_check);
                question1.AddCell(risk3_check);
                question1.AddCell(risk4_check);

                for (int k = 0; k < dt_log2.Rows.Count; k++)
                {
                    risk1_check = new PdfPCell(new Phrase((k + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 7)));
                    risk2_check = new PdfPCell(new Phrase(dt_log2.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7)));
                    risk3_check = new PdfPCell(new Phrase(dt_log2.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 7)));
                    risk3_check.HorizontalAlignment = Element.ALIGN_CENTER;
                    risk4_check = new PdfPCell(new Phrase(dt_log2.Rows[k][2].ToString(), new Font(Font.FontFamily.HELVETICA, 7)));
                    risk1_check.HorizontalAlignment = Element.ALIGN_CENTER;
                    question1.AddCell(risk1_check);
                    question1.AddCell(risk2_check);
                    question1.AddCell(risk3_check);
                    question1.AddCell(risk4_check);

                }

               
                titleChunk = new Chunk("Project Site Checklist", blackListTextFont1);

                doc.Add(titleChunk);
                //doc.Add(table1);
                doc.Add(question1);

            }
            //----------------------------


            PdfPTable img_mahesh = new PdfPTable(2);
            img_mahesh.WidthPercentage = 100f;
            iTextSharp.text.Image image_last_mahesh;
            if (dt.Rows[0][0].ToString() == "1")
            {
                image_last_mahesh = iTextSharp.text.Image.GetInstance(imag_file1);
                image_last_mahesh.SetAbsolutePosition(0, 100);
                image_last_mahesh.ScaleAbsolute(20f, 20f);
            }
            else
            {
                image_last_mahesh = iTextSharp.text.Image.GetInstance(imag_file2);
                image_last_mahesh.SetAbsolutePosition(0, 100);
                image_last_mahesh.ScaleAbsolute(20f, 20f);
            }
            PdfPCell last_mahesh = new PdfPCell(image_last_mahesh);
            last_mahesh.Border = Rectangle.NO_BORDER;
            last_mahesh.HorizontalAlignment = Rectangle.ALIGN_LEFT;
            last_mahesh.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
            if (dt.Rows[0][0].ToString() == "1")
            {
                last_mahesh.FixedHeight = 400f;
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                last_mahesh.FixedHeight = 400f;
            }
            img_mahesh.AddCell(last_mahesh);

            PdfPCell last_voda_mahesh = new PdfPCell(new Phrase("Vodafone Idea Ltd.", new Font(Font.FontFamily.HELVETICA, 8,
            Font.NORMAL, iTextSharp.text.BaseColor.ORANGE)));
            last_voda_mahesh.Border = Rectangle.NO_BORDER;
            last_voda_mahesh.HorizontalAlignment = Rectangle.ALIGN_RIGHT;
            last_voda_mahesh.VerticalAlignment = Rectangle.ALIGN_BOTTOM;
            if (dt.Rows[0][0].ToString() == "1")
            {
                last_voda_mahesh.FixedHeight = 400f;
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                last_voda_mahesh.FixedHeight = 400f;
            }
            img_mahesh.AddCell(last_voda_mahesh);
            // doc.Add(img_mahesh);

            PdfContentByte cb11 = writer.DirectContent;
            if (dt.Rows[0][0].ToString() == "1")
            {
                footer = iTextSharp.text.Image.GetInstance(imag_file1);
            }
            if (dt.Rows[0][0].ToString() == "2")
            {
                footer = iTextSharp.text.Image.GetInstance(imag_file2);
            }
            footer.SetAbsolutePosition(0, 0);
            footer.ScaleAbsolute(30f, 30f);
            PdfContentByte byte111 = writer.DirectContent;
            PdfTemplate tp111 = cb11.CreateTemplate(600, 150);


            var red_footer11 = FontFactory.GetFont("HELVETICA", 7, BaseColor.ORANGE);


            string str11 = "Vodafone Idea Ltd.";
            template = cb11.CreateTemplate(10, 10);
            bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

            cb11.BeginText();
            cb11.SetFontAndSize(bf, 8);
            cb11.SetTextMatrix(rec.GetRight(150), rec.GetBottom(40));
            // cb.SetColorFill(BaseColor.ORANGE);

            cb11.ShowText(str11);
            tp111.AddImage(footer);
            cb11.EndText();

            cb11.AddTemplate(template, doc.Right, doc.Bottom - 15);
            cb11.AddTemplate(tp111, doc.Left, doc.Bottom - 5);
            doc.Close();
        }

        //  Response.Redirect(System.IO.Path.Combine(Request.PhysicalApplicationPath, file));
        // Response.Redirect("http://ptw.skyproductivity.com/report_ptw/height_pdf/" + file);
        //   Server.Transfer("http://ptw.skyproductivity.com/report_ptw/height_pdf/" + file, true);
        //Response.Redirect(pdfFilePath);
        //  webclient 
        //  Response.WriteFile(pdfFilePath);
        //  Response.Write("<SCRIPT language='javascript' type='text/javascript'>var pdf=window.open('http://ptw.skyproductivity.com/report_ptw/height_pdf/" + file + "','PDF');</SCRIPT>");
        //  Response.ContentType = "Application/pdf";
        //Response.TransmitFile("F:/ptw_application_final/PTW_Application_1/report_ptw/height_pdf/" + ptwid1 + ".pdf");
        //  Response.TransmitFile("http://ptw.skyproductivity.com/report_ptw/height_pdf/" + file);

        Response.ContentType = "Application/pdf";

        Response.AppendHeader("Content-Disposition", "attachment; filename=" + file);

        Response.TransmitFile(Server.MapPath("~/report_ptw/height_pdf/" + SpacialCharRemove.SpacialChar_Remove(ptwid1) + ".pdf"));
        Response.End();
    }
}

//file:///F:\ptw_application_final\PTW_Application_1\report_ptw\height_pdf\ORI_OR_BRGM001_29062012_HT_bibhuti.muduli_2664.pdf
