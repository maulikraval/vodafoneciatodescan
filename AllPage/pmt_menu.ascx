﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="pmt_menu.ascx.cs" Inherits="admin_menu" %>
<div id="menu">
    <ul class="menu">
       <li><a href="#" class="parent"><span>DashBoard</span></a>
            <ul>
                <li><a href="pmt_dashboard.aspx"><span>g-pm "P"</span></a></li>
                <li><a href="pmt_dashboard_active.aspx"><span>ACTIVE</span></a> </li>               
                <li><a href="pmt_dashboard_tx.aspx"><span>TXEQUIP</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "P" Assign</span></a>
            <ul>
                <li><a href="pmt_assignsite.aspx"><span>g-pm "P" Assign</span></a></li>
                <li><a href="pmt_re_assignsite.aspx"><span>g-pm "P" Re-Assign</span></a></li>
                <li><a href="pmt_manualassignsite.aspx"><span>g-pm "P" Unsheduled Assign</span></a></li>
                <li><a href="fps_pmt_assignsite.aspx"><span>FPS Assign</span></a></li>
                <li><a href="Fps_re_assignsite.aspx"><span>FPS Re-Assign</span></a></li>
                <li><a href="Fps_manualassignsite.aspx"><span>FPS Unsheduled Assign</span></a></li>
            </ul>
        </li>
      
        <li><a href="#" class="parent"><span>g-pm "A" Assign</span></a>
            <ul>
              
                <li><a href="Active_new_assignsite.aspx"><span>g-pm "A" Assign new</span></a></li>
                <li><a href="Active_new_re_assignsite.aspx"><span>g-pm "A" Re-Assign new</span></a></li>
                <li><a href="Active_new_manualassignsite.aspx"><span>g-pm "A" Unsheduled new</span></a></li>
            </ul>
        </li>
          <li><a href="#" class="parent"><span>Tx-Equip Assign</span></a>
            <ul>
                 <li><a href="Tx_assignsite.aspx"><span>Tx-Equip Assign</span></a></li>
                <li><a href="tx_re_assignsite.aspx"><span>Tx-Equip Re-Assign</span></a> </li>
                <li><a href="tx_manualassignsite.aspx"><span>Tx-Equip Unsheduled Assign</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "P" Reports</span></a>
            <ul>
                <li><a href="pmt_Circle_Report.aspx"><span>g-pm "P" Excel Report</span></a></li>
                <li><a href="pmt_Circle_Report_archive.aspx"><span>g-pm "P" Archive Report</span></a></li>
                <li><a href="pmt_pm_PDF_Report.aspx"><span>g-pm "P" PDF Report</span></a></li>
                <li><a href="pmt_TechnicianAssignReport_.aspx"><span>g-pm "P" Technician Report</span></a></li>

                <li><a href="Active_pm_PDF_Report_Checksheet_And_Passive.aspx"><span>g-pm "A"/"P" PunchPoint PDF Report</span></a></li>
                
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A" Reports</span></a>
            <ul>
                <li><a href="Active_Circle_Report.aspx"><span>g-pm "A" Excel Report</span></a></li>
                <li><a href="Active_Circle_Report_archive.aspx"><span>g-pm "A" Archive Report</span></a></li>
                <li><a href="Active_pm_PDF_Report.aspx"><span>g-pm "A" PDF Report</span></a></li>
                <li><a href="Active_TechnicianAssignReport_.aspx"><span>g-pm "A" Technician Report</span></a></li>

                <li><a href="pmt_Active_Circle_Report_new.aspx"><span>g-pm "A" Excel New</span></a></li>
                <li><a href="pmt_Active_Circle_Report_archive_new.aspx"><span>g-pm "A" Archive New</span></a></li>
                <li><a href="pmt_Active_pm_PDF_Report_new.aspx"><span>g-pm "A" PDF New</span></a></li>
                <li><a href="pmt_Active_TechnicianAssignReport_new.aspx"><span>g-pm "A" Technician New</span></a></li>
              
              <li><a href="pmt_gpm_A_CAR_Report.aspx"><span>g-pm "A" CAR Report</span></a></li>

              <li><a href="Punchpoint_Excel_Report_Active.aspx"><span>g-pm "A" PunchPoint Excel Report</span></a></li>
                <li><a href="Active_TechnicianAssignReport_123.aspx"><span>g-pm "A" Change Technician</span></a></li>
                
                
            </ul>
        </li>
          <li><a href="#" class="parent"><span>Tx-Equip</span></a>
            <ul>
                
                <li><a href="pmt_Circle_Report_tx.aspx"><span>Tx-Equip Excel Report</span></a></li>
                <li><a href="pmt_Circle_Report_archive_tx.aspx"><span>Tx-Equip Archive Report</span></a></li>
                <li><a href="pmt_tx_pdf_Report.aspx"><span>Tx-Equip PDF Report</span></a></li>
                <li><a href="pmt_tx_TechnicianAssignReport_new.aspx"><span>Tx-Equip Technician Report</span></a></li>
                
                      <li><a href="pmt_chart_tx.aspx"><span>Tx-Equip Pie Chart</span></a></li>
                <li><a href="pmt_bar_chart_tx.aspx"><span>Tx-Equip Bar Chart</span></a></li>
                <li><a href="pmt_Active_PM_Chart_YesNoNA_tx.aspx"><span>Tx-Equip QuestionWise Chart</span></a></li>

                <li><a href="pmt_tx_equip_CAR_Report.aspx"><span>Tx-Equip CAR Report</span></a></li>

                <li><a href="Punch_Point_Excel_Report_TXT.aspx"><span>Tx-Equip PunchPoint Excel Report</span></a></li>
                <%--<li><a href="Punch_Point_Excel_Report_TXT.aspx"><span>Tx-Equip PunchPoint PDF Report</span></a></li>--%>
                <li><a href="txequip_TechnicianAssignReport_123.aspx"><span>Tx-Equip Change Technician</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "P" Charts</span></a>
            <ul>
                <li><a href="pmt_PM_Chart.aspx"><span>g-pm "P" ChecksheetWise Chart</span></a></li>
                <li><a href="pmt_PM_Chart_Survey.aspx"><span>g-pm "P" SiteWise Chart</span></a></li>
                <li><a href="pmt_PM_Chart_YesNoNA.aspx"><span>g-pm "P" QuestionWise Chart</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A" Charts</span></a>
            <ul>
                <li><a href="Active_PM_Chart.aspx"><span>g-pm "A" ChecksheetWise Chart </span></a></li>
                <li><a href="Active_PM_Chart_Survey.aspx"><span>g-pm "A" SiteWise Chart</span></a></li>
                <li><a href="Active_PM_Chart_YesNoNA.aspx"><span>g-pm "A" QuestionWise Chart</span></a></li>

                <li><a href="pmt_Active_PM_Chart_new.aspx"><span>g-pm "A" ChecksheetWise New</span></a></li>
                <li><a href="pmt_Active_PM_Chart_Survey_new.aspx"><span>g-pm "A" SiteWise New</span></a></li>
                <li><a href="pmt_Active_PM_Chart_YesNoNA_new.aspx"><span>g-pm "A" QuestionWise New</span></a></li>
          
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Site Details</span></a>
            <ul>
                <li><a href="pmt_pm_lockassignsite.aspx"><span>g-pm "P" Lock/UnLock</span></a></li>
                <li><a href="Active_pm_lockassignsite.aspx"><span>g-pm "A" Lock/UnLock</span></a></li>
                <li><a href="PMT_Site_Download.aspx"><span>Site Dump</span></a></li>
                <li><a href="PMT_Site_New_Download.aspx"><span>Sub Site Dump</span></a></li>
            </ul>
        </li>
        <li><a href="pmt_Showuser.aspx" class="parent"><span>ViewUser</span></a></li>
        
    </ul>
</div>
