﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using mybusiness;
using System.Data;

public partial class PM_reminder : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;

    DataRow row;
    protected void Page_Load(object sender, EventArgs e)
    {
	  da = new mydataaccess1();
                   string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (!IsPostBack)
        {
            try
            {
                if (Session["role"].ToString() != "2" && Session["um"].ToString()==sp_user_)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {

                    da = new mydataaccess1();
                    dt = new DataTable();
                    string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da.reminder_20_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da.reminder_20_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da.reminder_20_days_circle_admin_v2ms(sp_user_d);

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);

                    dt = new DataTable();
                    dt.Columns.Add("LastSurveyedOn");
                    dt.Columns.Add("DateAfterSixMonths");
                    dt.Columns.Add("Circle");
                    dt.Columns.Add("SiteId");
                    dt.Columns.Add("SiteName");
                    dt.Columns.Add("Address");
                    dt.Columns.Add("Technician");
                    dt.Columns.Add("Contact");
                    row = null;

                    for (int i = 0; i < dtv5.Rows.Count; i++)
                    {

                        row = dt.NewRow();

                        row["LastSurveyedOn"] = dtv5.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = dtv5.Rows[i][1].ToString();
                        row["Circle"] = dtv5.Rows[i][2].ToString();
                        row["SiteId"] = dtv5.Rows[i][3].ToString();
                        row["SiteName"] = dtv5.Rows[i][4].ToString();
                        row["Address"] = dtv5.Rows[i][5].ToString();
                        row["Technician"] = dtv5.Rows[i][6].ToString();
                        row["Contact"] = dtv5.Rows[i][7].ToString();

                        dt.Rows.Add(row);

                    }



                    for (int i = 0; i < dtv2.Rows.Count; i++)
                    {
                        row = dt.NewRow();

                        row["LastSurveyedOn"] = dtv2.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = dtv2.Rows[i][1].ToString();
                        row["Circle"] = dtv2.Rows[i][2].ToString();
                        row["SiteId"] = dtv2.Rows[i][3].ToString();
                        row["SiteName"] = dtv2.Rows[i][4].ToString();
                        row["Address"] = dtv2.Rows[i][5].ToString();
                        row["Technician"] = dtv2.Rows[i][6].ToString();
                        row["Contact"] = dtv2.Rows[i][7].ToString();

                        dt.Rows.Add(row);
                    }
                    for (int i = 0; i < dtv2ms.Rows.Count; i++)
                    {
                        row = dt.NewRow();

                        row["LastSurveyedOn"] = dtv2ms.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = dtv2ms.Rows[i][1].ToString();
                        row["Circle"] = dtv2ms.Rows[i][2].ToString();
                        row["SiteId"] = dtv2ms.Rows[i][3].ToString();
                        row["SiteName"] = dtv2ms.Rows[i][4].ToString();
                        row["Address"] = dtv2ms.Rows[i][5].ToString();
                        row["Technician"] = dtv2ms.Rows[i][6].ToString();
                        row["Contact"] = dtv2ms.Rows[i][7].ToString();

                        dt.Rows.Add(row);
                    }


                    grd20.DataSource = dt;
                    grd20.DataBind();

                    dt = new DataTable();
                    dt.Columns.Add("LastSurveyedOn");
                    dt.Columns.Add("DateAfterSixMonths");
                    dt.Columns.Add("Circle");
                    dt.Columns.Add("SiteId");
                    dt.Columns.Add("SiteName");
                    dt.Columns.Add("Address");
                    dt.Columns.Add("Technician");
                    dt.Columns.Add("Contact");
                    da = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da.reminder_10_days_circle_admin(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da.reminder_10_days_circle_admin_v2(sp_user_d);
                    da = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da.reminder_10_days_circle_admin_v2ms(sp_user_d);

                    row = null;

                    for (int i = 0; i < dtv51.Rows.Count; i++)
                    {

                        row = dt.NewRow();

                        row["LastSurveyedOn"] = dtv51.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = dtv51.Rows[i][1].ToString();
                        row["Circle"] = dtv51.Rows[i][2].ToString();
                        row["SiteId"] = dtv51.Rows[i][3].ToString();
                        row["SiteName"] = dtv51.Rows[i][4].ToString();
                        row["Address"] = dtv51.Rows[i][5].ToString();
                        row["Technician"] = dtv51.Rows[i][6].ToString();
                        row["Contact"] = dtv51.Rows[i][7].ToString();

                        dt.Rows.Add(row);

                    }



                    for (int i = 0; i < dtv21.Rows.Count; i++)
                    {
                        row = dt.NewRow();

                        row["LastSurveyedOn"] = dtv21.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = dtv21.Rows[i][1].ToString();
                        row["Circle"] = dtv21.Rows[i][2].ToString();
                        row["SiteId"] = dtv21.Rows[i][3].ToString();
                        row["SiteName"] = dtv21.Rows[i][4].ToString();
                        row["Address"] = dtv21.Rows[i][5].ToString();
                        row["Technician"] = dtv21.Rows[i][6].ToString();
                        row["Contact"] = dtv21.Rows[i][7].ToString();

                        dt.Rows.Add(row);
                    }
                    for (int i = 0; i < dtv2ms1.Rows.Count; i++)
                    {
                        row = dt.NewRow();

                        row["LastSurveyedOn"] = dtv2ms1.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = dtv2ms1.Rows[i][1].ToString();
                        row["Circle"] = dtv2ms1.Rows[i][2].ToString();
                        row["SiteId"] = dtv2ms1.Rows[i][3].ToString();
                        row["SiteName"] = dtv2ms1.Rows[i][4].ToString();
                        row["Address"] = dtv2ms1.Rows[i][5].ToString();
                        row["Technician"] = dtv2ms1.Rows[i][6].ToString();
                        row["Contact"] = dtv2ms1.Rows[i][7].ToString();

                        dt.Rows.Add(row);
                    }

                    grd10.DataSource = dt;
                    grd10.DataBind();

                    /////////////////////// critical....

                    dt = new DataTable();
                    dt.Columns.Add("LastSurveyedOn");
                    dt.Columns.Add("DateAfterSixMonths");
                    dt.Columns.Add("Circle");
                    dt.Columns.Add("SiteId");
                    dt.Columns.Add("SiteName");
                    dt.Columns.Add("Address");
                    dt.Columns.Add("Technician");
                    dt.Columns.Add("Contact");
                    row = null;

                    for (int i = 0; i < cv2.Rows.Count; i++)
                    {

                        row = dt.NewRow();

                        row["LastSurveyedOn"] = cv2.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = cv2.Rows[i][1].ToString();
                        row["Circle"] = cv2.Rows[i][2].ToString();
                        row["SiteId"] = cv2.Rows[i][3].ToString();
                        row["SiteName"] = cv2.Rows[i][4].ToString();
                        row["Address"] = cv2.Rows[i][5].ToString();
                        row["Technician"] = cv2.Rows[i][6].ToString();
                        row["Contact"] = cv2.Rows[i][7].ToString();

                        dt.Rows.Add(row);

                    }



                    for (int i = 0; i < cv5.Rows.Count; i++)
                    {
                        row = dt.NewRow();

                        row["LastSurveyedOn"] = cv5.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = cv5.Rows[i][1].ToString();
                        row["Circle"] = cv5.Rows[i][2].ToString();
                        row["SiteId"] = cv5.Rows[i][3].ToString();
                        row["SiteName"] = cv5.Rows[i][4].ToString();
                        row["Address"] = cv5.Rows[i][5].ToString();
                        row["Technician"] = cv5.Rows[i][6].ToString();
                        row["Contact"] = cv5.Rows[i][7].ToString();

                        dt.Rows.Add(row);
                    }
                    for (int i = 0; i < cv2ms.Rows.Count; i++)
                    {
                        row = dt.NewRow();

                        row["LastSurveyedOn"] = cv2ms.Rows[i][0].ToString();
                        row["DateAfterSixMonths"] = cv2ms.Rows[i][1].ToString();
                        row["Circle"] = cv2ms.Rows[i][2].ToString();
                        row["SiteId"] = cv2ms.Rows[i][3].ToString();
                        row["SiteName"] = cv2ms.Rows[i][4].ToString();
                        row["Address"] = cv2ms.Rows[i][5].ToString();
                        row["Technician"] = cv2ms.Rows[i][6].ToString();
                        row["Contact"] = cv2ms.Rows[i][7].ToString();

                        dt.Rows.Add(row);
                    }


                    grdcritical.DataSource = dt;
                    grdcritical.DataBind();
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }
    }

    private string ConvertSortDirectionToSql(SortDirection sortDirection)
    {
        string newSortDirection = String.Empty;

        switch (sortDirection)
        {
            case SortDirection.Ascending:
                newSortDirection = "ASC";
                break;

            case SortDirection.Descending:
                newSortDirection = "DESC";
                break;
        }

        return newSortDirection;
    }
    protected void grd20_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grd20.DataSource as DataTable;


        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grd20.DataSource = dataView;
            grd20.DataBind();
        }
    }
    protected void grd10_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grd10.DataSource as DataTable;


        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grd10.DataSource = dataView;
            grd10.DataBind();
        }
    }
    protected void grdcritical_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dataTable = grdcritical.DataSource as DataTable;

        if (dataTable != null)
        {
            DataView dataView = new DataView(dataTable);
            dataView.Sort = e.SortExpression + " " + ConvertSortDirectionToSql(e.SortDirection);

            grdcritical.DataSource = dataView;
            grdcritical.DataBind();
        }
    }
}