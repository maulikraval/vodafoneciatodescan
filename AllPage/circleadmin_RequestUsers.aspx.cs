﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using System.Net.Mail;
using System.Net;

public partial class circle_admin_RequestUsers : System.Web.UI.Page
{
    mydataaccess1 da;
    vodabal bal;
    DataTable dt = new DataTable();
    string sp_user_d = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                {
                    int i = 3;
                    if (Convert.ToInt32(Session["role"].ToString()) == i)
                    {

                    }
                    else
                    {
                        Session.Abandon();
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }

                if (Session.Count != 0)
                {
                    // split for decodation username.
                    da = new mydataaccess1();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                    if (lblusername.Text == "")
                    {
                        // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                    }
                    da = new mydataaccess1();
                    dt = new DataTable();

                    dt = da.Viewsitegridbyrequest(sp_user_d);
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    Response.Redirect("~/login/Default.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }


    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            Response.Redirect("~/login/Default.aspx",false);
        }
        catch (Exception ee)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        int status = 0;
        string siteid;
        int check_type = 0;
        try
        {
            ImageButton im1 = (ImageButton)sender;
            string[] data = im1.ToolTip.ToString().Split('.');
            siteid = data[0].ToString();
            check_type = Convert.ToInt32(data[1].ToString());


            status = 3;

            da = new mydataaccess1();
            da.updaterequestedstatus(siteid, status,lblusername.Text,check_type);
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.Viewsitegridbyrequest(lblusername.Text);
            GridView1.DataSource = dt;
            GridView1.DataBind();
            mail(siteid, lblusername.Text, "Modify");


        }

        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        finally
        { }

    }
    void mail(string siteid, string user, string app)
    {
        try
        {
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.select_sitedetails_for_mail(siteid);
            //string mail = dt.Rows[0][0].ToString();

            string password = dt.Rows[0][1].ToString();

            string subject = "Site Reinspection Details";
            string body = "Dear User,<br /><br />Please find below details for re-inspection.<br /><br /> Circle : " + SendMail.ishtml(dt.Rows[0][0].ToString()) + " <br />Zone :" + SendMail.ishtml(dt.Rows[0][1].ToString()) + "<br />Siteid :" + SendMail.ishtml(dt.Rows[0][2].ToString()) + "<br />SiteName : " + SendMail.ishtml(dt.Rows[0][3].ToString()) + "<br />Technician Name : " + SendMail.ishtml(dt.Rows[0][4].ToString()) + "<br />Approved Status : " + SendMail.ishtml(app) + " <br />Approved By : " + SendMail.ishtml(user) + " <br />Approved Date :" + System.DateTime.Now + "<br /><br />Regards, <br />CIAT Team ";
            string tomail = "ciat.mail2@gmail.com";
            SendMail sm = new SendMail();
            sm.mailsend(tomail, subject, body);
            //   //mail for password

            //   //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", mail, "Your Password", "Dear Sir/Madam,<br /><br />There are some details regarding your Account Details.<br /><br />UserName: " + txtUser.Text + " <br /><br /> Password: " + password + " <br /><br />Regards, <br />CIAT Team ");
            //   MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", "ciat.mail2@gmail.com", "Site Reinspection Details", "Dear User,<br /><br />Please find below details for re-inspection.<br /><br /> Circle : " + dt.Rows[0][0].ToString() + " <br />Zone :" + dt.Rows[0][1].ToString() + "<br />Siteid :" + dt.Rows[0][2].ToString() + "<br />SiteName : " + dt.Rows[0][3].ToString() + "<br />Technician Name : " + dt.Rows[0][4].ToString() + "<br />Approved Status : " + app + " <br />Approved By : " + user + " <br />Approved Date :" + System.DateTime.Now + "<br /><br />Regards, <br />CIAT Team ");
            //   message.IsBodyHtml = true;

            //   #region "VODAFONE SERVER CREDENTIALS"

            // SmtpClient emailClient = new SmtpClient("10.94.64.107", 25);

            //   emailClient.Credentials =
            //   new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

            //   emailClient.EnableSsl = false;

            //   #endregion

            //   #region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

            ///*   SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
            //   emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
            //   emailClient.EnableSsl = true; */

            //   #endregion

            //   emailClient.Send(message);
        }
        catch
        {

        }
    }

    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {

        int status = 0;
        string siteid;
        int check_type = 0;
        try
        {
            ImageButton im1 = (ImageButton)sender;
            string[] data = im1.ToolTip.ToString().Split('.');
            siteid = data[0].ToString();
            check_type = Convert.ToInt32(data[1].ToString());

            status = 4;

            da = new mydataaccess1();
          da.updaterequestedstatus(siteid, status,lblusername.Text,check_type);

            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.Viewsitegridbyrequest(lblusername.Text);
            GridView1.DataSource = dt;
            GridView1.DataBind();
                mail(siteid, lblusername.Text, "Reset");

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
        finally
        { }
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}
