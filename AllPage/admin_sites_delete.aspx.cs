﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;

public partial class admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try
            {
                da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
                if (Session["role"].ToString() != "4" && Session["um"].ToString() != sp_user_)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();
                    dt = da.delete_site_view_superadmin();
                    grduser.DataSource = dt;
                    grduser.DataBind();

                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }
    }

    protected void lnkdelete_deleteclick(object sender, EventArgs e)
    {
        try
        {
            LinkButton tb = (LinkButton)sender;
            string s = tb.ToolTip.ToString();

            da = new mydataaccess1();
            dt = new DataTable();
            string user = da.select_user_cookie(Session["user"].ToString());

            da = new mydataaccess1();
            string dat = System.DateTime.Now.ToString("dd/MM/yyyy");
            da.delete_site_superadmin(s, dat, user); ;


            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            dt = da.delete_site_view_superadmin();
            grduser.DataSource = dt;
            grduser.DataBind();


            Label4.Text = "'" + SpacialCharRemove.XSS_Remove(s.ToString()) + "' is deleted successfully...";
        }
        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }

}
