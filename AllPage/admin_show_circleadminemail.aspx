﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_show_circleadminemail.aspx.cs" Inherits="AllPage_admin_showuser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />

    <table width="100%">
        <tr>
            <td style="width: 100%; text-align: center">
                <asp:Label ID="Label3" runat="server" CssClass="lblstly" Text="Circle-Admin Email Details"></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <hr />
            <td style="text-align: center; width: 100%">
                <asp:Panel ID="panel1" runat="server" ScrollBars="Horizontal" Width="1025px" Height="450px">
                    <asp:GridView ID="grduser" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                       OnSelectedIndexChanged="grduser_SelectedIndexChanged" Width="100%">
                        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle Font-Names="Calibri" Font-Size="13px" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:CommandField HeaderText="Edit" ShowSelectButton="True" SelectText="Edit" />
                        </Columns>
                        <%--<asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LkB1" runat="server" CommandName="Edit">Edit</asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LB2" runat="server" CommandName="Update">Update</asp:LinkButton>
                                <asp:LinkButton ID="LB3" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                            </EditItemTemplate>
                        </asp:TemplateField>--%>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server"  CssClass="lblall"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                    TargetControlID="pan1" runat="server">
                </cc1:RoundedCornersExtender>
                <asp:Panel ID="pan1" BackColor="#E4E4E4" runat="server">
                    <table width="100%">
                        <tr>
                            <td style="width: 5%"></td>
                            <td style="text-align: center;" colspan="2">
                                <asp:Label ID="Label2" runat="server" CssClass="lblstly" Text="Edit User"></asp:Label>
                            </td>
                            <td style="width: 5%;"></td>
                        </tr>
                        <tr>
                            <td style="width: 5%">&nbsp;
                            </td>
                            <td colspan="2" style="text-align: center;">
                                <hr />
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblcircle" runat="server" Text="Circle :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtcircle" Enabled="false" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblname1" runat="server" Text="Name 1 :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtname1" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblemail1" runat="server" CssClass="lblall" Text="Email 1 :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtemail1" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtemail1"
                                    ErrorMessage="Enter Correct Email-Id" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblname2" runat="server" Text="Name 2 :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtname2" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblemail2" runat="server" CssClass="lblall" Text="Email 2 :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtemail2" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtemail2"
                                    ErrorMessage="Enter Correct Email-Id" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblname3" runat="server" Text="Name 3 :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtname3" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblemail3" runat="server" CssClass="lblall" Text="Email 3 :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtemail3" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtemail3"
                                    ErrorMessage="Enter Correct Email-Id" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblname4" runat="server" Text="Name 4 :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtname4" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblemail4" runat="server" CssClass="lblall" Text="Email 4 :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtemail4" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtemail4"
                                    ErrorMessage="Enter Correct Email-Id" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblname5" runat="server" Text="Name 5 :" CssClass="lblall"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtname5" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: right;">
                                <asp:Label ID="lblemail5" runat="server" CssClass="lblall" Text="Email 5 :"></asp:Label>
                            </td>
                            <td style="width: 40%; text-align: left;">
                                <asp:TextBox ID="txtemail5" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtemail5"
                                    ErrorMessage="Enter Correct Email-Id" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 5%;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 5%; height: 26px;"></td>
                            <td style="text-align: center; height: 26px;" colspan="2">
                                <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" Text="Update" />
                                <asp:Button ID="btncancel" runat="server" CausesValidation="false" Text="Cancel" />
                            </td>
                            <td style="width: 5%; height: 26px;"></td>
                        </tr>
                        <tr>
                            <td style="width: 5%;">&nbsp;
                            </td>
                            <td style="text-align: right; width: 40%;">&nbsp;
                            </td>
                            <td style="width: 40%; text-align: left;">&nbsp;
                                <asp:Label ID="Label1" runat="server" ></asp:Label>
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:LinkButton ID="lnk" runat="server" Width="1px"></asp:LinkButton>
                <cc1:ModalPopupExtender ID="mp1" runat="server" BackgroundCssClass="modalBackground"
                    TargetControlID="lnk" PopupControlID="pan1">
                </cc1:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>
