﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;

public partial class admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    string project = "";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["flag"] == "1")
        {
            this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
            project = "ciat";
        }
        if (Session["flag"] == "2")
        {
            this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
            project = "ptw";
        }
        if (Session["flag"] == "4")
        {
            //  this.MasterPageFile = "~/circle_admin/NewCircle.master";
            project = "both";
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
            }

        }
        if (Session["flag"] == "3")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
                project = "ciat";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
                project = "ptw";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try
            {
			 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
                if (Session["role"].ToString() != "3" && Session["um"].ToString()==sp_user_)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    if (Session["flag"] == "1")
                    {

                        project = "ciat";
                    }
                    if (Session["flag"] == "2")
                    {

                        project = "ptw";
                    }
                    if (Session["flag"] == "4")
                    {

                        project = "both";
                    }
                    if (Session["flag"] == "3")
                    {
                        if (Session["project"].ToString() == "ciat")
                        {

                            project = "ciat";
                        }
                        else
                        {

                            project = "ptw";
                        }
                    }
                    string sp_user_d = "";
                    da = new mydataaccess1();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());
                    da = new mydataaccess1();
                    dt = new DataTable();
                    dt = da.select_distance_data_circle_admin(sp_user_d);
                    grduser.DataSource = dt;
                    grduser.DataBind();
                }

            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }

        }

    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }

    protected void LinkButton1_Click1(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        da = new mydataaccess1();
        da.unblock_user(lb.ToolTip);
        this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('User Unblocked');", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        da = new mydataaccess1();
        da.update_approval_siteid(Convert.ToInt32(lb.ToolTip), 2);

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_distance_data();
        grduser.DataSource = dt;
        grduser.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lb = (LinkButton)sender;
        da = new mydataaccess1();
        da.update_approval_siteid(Convert.ToInt32(lb.ToolTip), -2);

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.select_distance_data();
        grduser.DataSource = dt;
        grduser.DataBind();
    }
    protected void grduser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
          //  e.Row.Cells[2].Visible = false;
        }
        catch
        { }
    }
}
