﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using System.IO;
using NPOI.POIFS.FileSystem;
using System.Data.OleDb;
//using Excel = Microsoft.Office.Interop.Excel;

public partial class admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    HSSFWorkbook hssfworkbook;
    string project = "";

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["flag"] == "1")
        {
            this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
            project = "ciat";
        }
        if (Session["flag"] == "2")
        {
            this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
            project = "ptw";
        }
        if (Session["flag"] == "4")
        {
            //  this.MasterPageFile = "~/circle_admin/NewCircle.master";
            project = "both";
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
            }

        }
        if (Session["flag"] == "3")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
                project = "ciat";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
                project = "ptw";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            try
            {

                if (Session["flag"] == "1")
                {

                    project = "ciat";
                }
                if (Session["flag"] == "2")
                {

                    project = "ptw";
                }
                if (Session["flag"] == "4")
                {

                    project = "both";
                }
                if (Session["flag"] == "3")
                {
                    if (Session["project"].ToString() == "ciat")
                    {

                        project = "ciat";
                    }
                    else
                    {

                        project = "ptw";
                    }
                }
                string sp_user_d = "";
                da = new mydataaccess1();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());
                da = new mydataaccess1();
                dt = new DataTable();
                dt = da.select_company_receiver();
                Session["grdCompanyReceiver"] = dt;
                grdcompanyreceiver.DataSource = dt;
                grdcompanyreceiver.DataBind();

            }
            catch(Exception ex)
            {
throw ex;
              //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }

        }

    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    void export(System.Data.DataTable dt1)
    {

        string filename = "CompanyRecieverData.xls";

        int flag = 0;
        InitializeWorkbook();

        if (dt1 == null)
        {
            //Label1.Text = "All Sites Uploaded Successfully...,Wait For reports";
        }
        else
        {
            exporttoexcel(dt1, "Company Reciever");
            flag++;

        }

        if (flag != 0)
        {
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));

            Response.Clear();
            Response.BinaryWrite(WriteToStream().GetBuffer());
            Response.Flush();
        }
        else
        {
            //  TabContainer1.Visible = true;
        }

    }

    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }

    void exporttoexcel(System.Data.DataTable dt, string name)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet(name);
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }

    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        System.Data.DataTable dt1 = (System.Data.DataTable)Session["grdCompanyReceiver"];
        export(dt1);
    }
}
