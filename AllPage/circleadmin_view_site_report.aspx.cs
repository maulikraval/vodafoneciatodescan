﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using System.Data;

public partial class admin_view_site_report : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;



    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            int i = 3;
            if (Convert.ToInt32(Session["role"].ToString()) == i)
            {

            }
            else
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }


            if (!IsPostBack)
            {
                da = new mydataaccess1();
                dt = new DataTable();
                dt = da.select_doc_master();
                grd20.DataSource = dt;
                grd20.DataBind();
            }
        }

        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }

    }
    protected void grdcalctenant0_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.Equals("goto"))
        {
            string file =SpacialCharRemove.SpacialChar_Remove(e.CommandArgument.ToString());

	    Response.ClearHeaders();
	    Response.AddHeader("Cache-Control", "no-store, no-cache");

            Response.ContentType = "Application/csv";

            Response.AppendHeader("Content-Disposition", "attachment; filename=" + file);

            Response.TransmitFile(Server.MapPath("~/site_reports/" + file));

            Response.End();
            // Response.Redirect(Server.MapPath("~/site_reports/" + file));

        }


    }
}
