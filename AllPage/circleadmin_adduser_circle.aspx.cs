﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;

public partial class admin_adduser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["flag"] == "1")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "2")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "4")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }

            Response.Redirect("circleadmin_adduser_circle_both.aspx");

            // this.MasterPageFile = "~/circle_admin/NewCircle.master";
        }
        if (Session["flag"] == "3")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            try
            {
			 da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
                if (Session["role"].ToString() != "3" && Session["um"].ToString()==sp_user_)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    string sp_user_d = "";
                    da = new mydataaccess1();
                    sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();

                    dt = da.getcircle_user(sp_user_d);
                    ddlcircle.DataTextField = "circle";
                    ddlcircle.DataSource = dt;
                    ddlcircle.DataBind();
                    ddlcircle.Items.Insert(0, "---Select---");
                    ddlrole.Enabled = false;
                    ddlrole0.Enabled = false;

                    if (Session["flag"] == "1")
                    {
                        lblrole0.Visible = false;
                        ddlrole0.Visible = false;
                        ddlproject.Items.Insert(0, "Select Project");
                        ddlproject.Items.Add("CIAT");
                        ddlrole.Items.Insert(0, "Select");
                        ddlrole.Items.Add("Technician");
                        ddlrole.Items.Add("External Inspector");
                        ddlrole.Items.Add("Zonal Manager");
                    }
                    if (Session["flag"] == "2")
                    {
                        lblrole.Visible = false;
                        ddlrole.Visible = false;

                        ddlproject.Items.Insert(0, "Select Project");
                        ddlproject.Items.Add("PTW");
                        ddlrole0.Items.Insert(0, "Select");
                        ddlrole0.Items.Add("Issuer");
                        ddlrole0.Items.Add("Receiver");
                    }
                    if (Session["flag"] == "3")
                    {
                        if (Session["project"].ToString() == "ciat")
                        {
                            lblrole0.Visible = false;
                            ddlrole0.Visible = false;
                            ddlproject.Items.Insert(0, "Select Project");
                            ddlproject.Items.Add("CIAT");

                            ddlrole.Items.Insert(0, "Select");
                            ddlrole.Items.Add("Technician");
                            ddlrole.Items.Add("External Inspector");
                            ddlrole.Items.Add("Zonal Manager");
                        }
                        else
                        {
                            lblrole.Visible = false;
                            ddlrole.Visible = false;

                            ddlproject.Items.Insert(0, "Select Project");
                            ddlproject.Items.Add("PTW");
                            ddlrole0.Items.Insert(0, "Select");

                            ddlrole0.Items.Add("Issuer");
                            ddlrole0.Items.Add("Receiver");
                            ddlrole0.Items.Add("Service Partner");
                        }
                    }
                    if (Session["flag"] == "4")
                    {
                        //ha
                        Response.Redirect("circleadmin_adduser_circle_both.aspx", false);
                        //

                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }

        }
    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    //for-show users in Gridview----------------------------

    //protected void grduser_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    GridViewRow gr = grduser.SelectedRow;
    //    txtuserupdate.Text = gr.Cells[1].Text;
    //    txtpassupdate.Text = gr.Cells[2].Text;
    //    ddlroleupdate.SelectedItem.Text = gr.Cells[3].Text;
    //    txtcircleupdate.Text = gr.Cells[4].Text;
    //    mp1.Show();
    //}
    //protected void btnupdate_Click(object sender, EventArgs e)
    //{
    //    da = new mydataaccess1();
    //    ba = new myvodav2();
    //    dt = new DataTable();
    //    ba.User = txtuserupdate.Text;
    //    ba.Password = txtpassupdate.Text;
    //    ba.Role = ddlroleupdate.SelectedValue;
    //    ba.Circle = txtcircleupdate.Text;
    //    da.updateuser(ba);

    //    da = new mydataaccess1();
    //    ba = new myvodav2();
    //    dt = new DataTable();
    //    dt = da.showuser(ba);
    //    grduser.DataSource = dt;
    //    grduser.DataBind();
    //}



    //protected void lnkdelete_deleteclick(object sender, EventArgs e)
    //{
    //    LinkButton tb = (LinkButton)sender;
    //    string s = tb.ToolTip.ToString();

    //    da = new mydataaccess1();
    //    ba = new myvodav2();
    //    ba.User = s.ToString();
    //    da.deleteuser(ba);

    //    //da = new mydataaccess1();
    //    //ba = new myvodav2();
    //    //dt = new DataTable();
    //    //dt = da.showuser(ba);
    //    //grduser.DataSource = dt;
    //    //grduser.DataBind();
    //}
    //for-show users in Gridview--------------------------------
    protected void btncreate_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlcircle.SelectedIndex != 0)
            {
                Label1.Visible = false;
                da = new mydataaccess1();
                ba = new myvodav2();
                dt = new DataTable();
                ba.User = txtuser.Text.Replace(" ", "");


                //// Create a new instance of the hash crypto service provider.
                //HashAlgorithm hashAlg = new SHA256CryptoServiceProvider();
                //// Convert the data to hash to an array of Bytes.
                //byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(txtpass.Text);
                //// Compute the Hash. This returns an array of Bytes.
                //byte[] bytHash = hashAlg.ComputeHash(bytValue);
                //// Optionally, represent the hash value as a base64-encoded string, 
                //// For example, if you need to display the value or transmit it over a network.
                //string base64 = Convert.ToBase64String(bytHash);

                ba.Password = HiddenField1.Value;
                ba.Dump = txtpass.Text;
                if (ddlproject.Text == "CIAT" && ddlrole.SelectedIndex != 0)
                {
                    ba.Stepid = 1;
                    ba.Role = ddlrole.SelectedValue;
                    ba.Siteidnew = ""; //ptw role
                }
                if (ddlproject.Text == "PTW" && ddlrole0.SelectedIndex != 0)
                {
                    ba.Stepid = 2;
                    ba.Role = "";//ciat role
                    ba.Siteidnew = ddlrole0.SelectedValue;
                }
                if (ddlproject.Text == "BOTH" && ddlrole.SelectedIndex != 0 && ddlrole0.SelectedIndex != 0)
                {
                    ba.Stepid = 3;
                    ba.Role = ddlrole.SelectedValue; //ciat role
                    ba.Siteidnew = ddlrole0.SelectedValue; //ptw role

                }

                ba.Circle = ddlcircle.SelectedValue;
                ba.Emailid = txtemailid.Text;
                ba.Contactno = txtcontactno.Text;
                ba.Imei = txtimei.Text;
                int s = da.insertnewuser(ba);

                string abc = FileUpload1.FileName;
                abc = SpacialCharRemove.SpacialChar_Remove(abc);
                if (abc == "")
                {
                    //Label1.Text = "Select file first.";
                    Label1.Visible = true;
                }
                else
                {
                    FileUpload1.SaveAs(Server.MapPath("~/signatures/") + abc);
                    string path = Server.MapPath("~/signatures/") + abc;
                    da = new mydataaccess1();
                    da.ptw_insert_into_signature(txtuser.Text, ddlcircle.Text, path);
                }



                if (s == 4)
                {
                    Label1.Visible = true;
                    Label1.Text = "User Already Exist";
                }
                else if (s == 3)
                {
                    Label1.Visible = true;
                    Label1.Text = "User Added Succesfully";
                    Label1.Text = "";

                    if (lbcircle.Items.Count > 0)
                    {
                        for (int i = 0; i < lbcircle.Items.Count; i++)
                        {
                            da = new mydataaccess1();
                            da.insertcircle_user(txtuser.Text, lbcircle.Items[i].ToString());
                        }
                    }
                    else
                    {
                        da = new mydataaccess1();
                        da.insertcircle_user(txtuser.Text, ddlcircle.SelectedValue);

                    }
                    if (lbzone.Items.Count > 0)
                    {
                        for (int i = 0; i < lbzone.Items.Count; i++)
                        {
                            da = new mydataaccess1();
                            da.insertzone_user(txtuser.Text, lbzone.Items[i].ToString());
                        }
                    }
                    else
                    {
                        if (ddlzone.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            da.insertzone_user(txtuser.Text, ddlzone.SelectedValue);
                        }

                    }
                    txtuser.Text = "";
                    txtcontactno.Text = "";
                    txtemailid.Text = "";
                    ddlcircle.SelectedIndex = 0;

                    ddlrole.SelectedIndex = 0;
                    lbcircle.Items.Clear();
                    txtpass.Text = "";
                    ddlzone.SelectedIndex = 0;
                    lbzone.Items.Clear();
                    txtimei.Text = "";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('User Added successfully');window.location ='circleadmin_adduser_circle.aspx';", true);
                }
                else if (s == 1)
                {
                    Label1.Visible = true;
                    Label1.Text = "Email-ID Already Exist";
                }

            }

        }

        catch
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

        }
    }

    protected void btnremove_Click(object sender, EventArgs e)
    {
        if (lbcircle.SelectedIndex != -1)
        {
            lberrorlb.Text = "";
            lbcircle.Items.Remove(lbcircle.SelectedValue);
        }
        else
        {
            lberrorlb.Text = "Select Any Circle from ListBox then Remove.";
        }
    }
    protected void ddlcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlcircle.SelectedIndex != 0)
        {
            if (ddlrole.SelectedValue == "Zonal Manager")
            {
                lbcircle.Enabled = false;
                lbzone.Enabled = true;
                da = new mydataaccess1();
                ba = new myvodav2();
                dt = new DataTable();
                ba.Circle = ddlcircle.SelectedValue;
                dt = da.getzonesbycirclename(ba);
                ddlzone.DataTextField = "zone";
                ddlzone.DataSource = dt;
                ddlzone.DataBind();
                ddlzone.Items.Insert(0, "---Select---");
            }
            else
            {
                int flag = 0;
                for (int i = 0; i < lbcircle.Items.Count; i++)
                {
                    if (lbcircle.Items[i].ToString() == ddlcircle.SelectedValue)
                    {
                        flag = 1;
                    }
                }
                if (flag == 0)
                {
                    lbcircle.Items.Add(ddlcircle.SelectedValue);
                }
            }
        }
    }
    protected void ddlproject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlproject.SelectedIndex != 0)
        {
            if (ddlproject.Text == "CIAT")
            {
                ddlrole.Enabled = true;
                ddlrole0.Enabled = false;
            }
            if (ddlproject.Text == "PTW")
            {
                ddlrole.Enabled = false;
                ddlrole0.Enabled = true;
                trcirclelb.Visible = true;
            }
            if (ddlproject.Text == "BOTH")
            {
                ddlrole.Enabled = true;
                ddlrole0.Enabled = true;
                trcirclelb.Visible = true;
            }
        }
        else
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ex", "alert('Please select Project');", true);
        }
    }
    protected void lbcircle_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlrole_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlrole.SelectedValue == "Zonal Manager")
        {
            trcirclelb.Visible = false;
            trzone.Visible = true;
            trzonelb.Visible = true;
        }
        else
        {
            trcirclelb.Visible = true;
            trzonelb.Visible = false;
            trzone.Visible = false;
        }
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlzone.SelectedIndex != 0)
        {
            int flag = 0;
            for (int i = 0; i < lbzone.Items.Count; i++)
            {
                if (lbzone.Items[i].ToString() == ddlzone.SelectedValue)
                {
                    flag = 1;
                }
            }
            if (flag == 0)
            {
                lbzone.Items.Add(ddlzone.SelectedValue);
            }
        }
    }
    protected void btnzoneremove_Click(object sender, EventArgs e)
    {
        if (lbzone.SelectedIndex != -1)
        {
            lbzoneerror.Text = "";
            lbzone.Items.Remove(lbzone.SelectedValue);
        }
        else
        {
            lbzoneerror.Text = "Select Any Zone from ListBox then Remove.";
        }
    }
    protected void ddlrole0_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlrole.SelectedValue == "Zonal Manager")
        {
            trcirclelb.Visible = false;
            trzone.Visible = true;
            trzonelb.Visible = true;
        }
        else
        {
            trcirclelb.Visible = true;
            trzonelb.Visible = false;
            trzone.Visible = false;
        }
    }
}
