﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;

public partial class circle_admin_Home : System.Web.UI.Page
{
    //protected void Page_PreInit(object sender, EventArgs e)
    //{
    //    if (Session["flag"] == "1")
    //    {
    //        this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
    //       // this.MasterPageFile = "~/circle_admin/ciat.master";
    //    }
    //    if (Session["flag"] == "2")
    //    {
    //        this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
    //        //this.MasterPageFile = "~/circle_admin/ptw.master";
    //    }
    //    if (Session["flag"] == "4")
    //    {
    //       // this.MasterPageFile = "~/circle_admin/NewCircle.master";
    //    }
    //    if (Session["flag"] == "3")
    //    {
    //        if (Session["project"].ToString() == "ciat")
    //        {
    //            this.MasterPageFile = "~/AllPage/circleadmin_ciat.master";
    //           // this.MasterPageFile = "~/circle_admin/ciat.master";
    //        }
    //        else
    //        {
    //            this.MasterPageFile = "~/AllPage/circleadmin_ptw.master";
    //      //this.MasterPageFile = "~/circle_admin/ptw.master";
    //        }
    //    }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["project"].ToString() == "ciat")
        //   {
        //       Response.Redirect("circleadmin_20daysreminder.aspx");
        //   }
        //   else
        //   {
        //      Response.Redirect("circleadmin_20daysreminder_ptw.aspx");
        //   }
		mydataaccess1  da = new mydataaccess1();
                   string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        try
        {
            if (Session["role"].ToString() != "3" && Session["um"].ToString()==sp_user_)
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
              //  Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch
        {
       Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

}
