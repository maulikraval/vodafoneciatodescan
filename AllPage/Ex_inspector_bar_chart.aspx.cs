﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using InfoSoftGlobal;
using mybusiness;

public partial class PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        //  FCLiteral.Text = CreateChart();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        if (!IsPostBack)
        {
            //     drpchecksheet.Visible = false;

            try
            {
                int i = 11;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {

                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            int count = 0;
            int count1 = 0;
            int count2 = 0;
            // Marquee start
            da = new mydataaccess1();
            DataTable dtv5 = new DataTable();
            dtv5 = da.reminder_20_days();
            da = new mydataaccess1();
            DataTable dtv2 = new DataTable();
            dtv2 = da.reminder_20_days_v2();
            da = new mydataaccess1();
            DataTable dtv2ms = new DataTable();
            dtv2ms = da.reminder_20_days_v2ms();
            da = new mydataaccess1();
            DataTable dtall = new DataTable();
            dtall = da.reminder_20_days_all();
            da = new mydataaccess1();
            DataTable cvall = new DataTable();
            cvall = da.reminder_20_days_critical_all();
            da = new mydataaccess1();
            DataTable dtall1 = new DataTable();
            dtall1 = da.reminder_10_days_all();
            count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

            // critical points
            da = new mydataaccess1();
            DataTable cv2 = new DataTable();
            cv2 = da.reminder_20_days_critical_v2();
            da = new mydataaccess1();
            DataTable cv5 = new DataTable();
            cv5 = da.reminder_20_days_critical();
            da = new mydataaccess1();
            DataTable cv2ms = new DataTable();
            cv2ms = da.reminder_20_days_critical_v2ms();
            count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

            da = new mydataaccess1();
            DataTable dtv51 = new DataTable();
            dtv51 = da.reminder_10_days();
            da = new mydataaccess1();
            DataTable dtv21 = new DataTable();
            dtv21 = da.reminder_10_days_v2();
            da = new mydataaccess1();
            DataTable dtv2ms1 = new DataTable();
            dtv2ms1 = da.reminder_10_days_v2ms();
            count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
            lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
            // Marquee End
            // lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>10</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>24</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>3</span>)";
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    if (Session["role"].ToString() == "11")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            ba = new myvodav2();

                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                          //  drpcircle.Items.Insert(1, "All");
                        }
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            ba = new myvodav2();
                            ba = new myvodav2();
                            DataTable circle = new DataTable();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                        }
                    }


                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        drpchecksheet.SelectedIndex = 0;
        panel2.Visible = false;
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }

    protected void drpchecksheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        panel2.Visible = true;
        if (drpchecksheet.SelectedIndex != 0)
        {

            FCLiteral.Text = CreateChart();


        }
        else
        {

        }
    }

    public string CreateChart()
    {
        string strXML = "";
        lblmsg.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();
        string check = "";

        check = drpchecksheet.SelectedItem.Text;

        dt = da.bar_chart(drpcircle.SelectedValue, check);
        {

            if (drpcircle.SelectedIndex != 0 )
            {
                if (dt.Rows[0][0].ToString() == "no")
                {

                    strXML = "<graph caption=' Inspection Status Circle: " + SpacialCharRemove.XSS_Remove( drpcircle.SelectedValue) + " , CheckSheet: " + SpacialCharRemove.XSS_Remove(check) + "' numberPrefix='%' decimalPrecision='0' >";

                    lblmsg.Text = "No Data Found.";
                    return strXML;

                }
                else
                {
                    string[,] arrData = new string[dt.Rows.Count, 5];

                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        arrData[j, 0] = dt.Rows[j][0].ToString();
                        arrData[j, 1] = dt.Rows[j][1].ToString();
                        arrData[j, 2] = dt.Rows[j][2].ToString();
                        arrData[j, 3] = dt.Rows[j][6].ToString();
                        arrData[j, 4] = dt.Rows[j][7].ToString();

                    }


                    string strCategories, strDataCurr, strDataPrev;
                    int i;

                    //Initialize <graph> element
                    strXML = "<graph caption=' Inspection Status Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + SpacialCharRemove.XSS_Remove(check) + "' numberPrefix='%' decimalPrecision='0' >";


                    //Initialize <categories> element - necessary to generate a multi-series chart
                    strCategories = "<categories>";

                    //Initiate <dataset> elements
                    strDataCurr = "<dataset seriesName='Inspection Done' color='AFD8F8'>";
                    strDataPrev = "<dataset seriesName='Inspection Not Done' color='F6BD0F'>";

                    //Iterate through the data	
                    for (i = 0; i < dt.Rows.Count; i++)
                    {
                        strDataCurr = "<dataset seriesName='Inspection Done (" + SpacialCharRemove.XSS_Remove(arrData[i, 3]) + ")' color='AFD8F8'>";
                        strDataPrev = "<dataset seriesName='Inspection Not Done (" + SpacialCharRemove.XSS_Remove(arrData[i, 4]) + ")' color='F6BD0F'>";

                        //Append <category name='...' /> to strCategories
                        strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(arrData[i, 0]) + "' />";
                        //Add <set value='...' /> to both the datasets
                        strDataCurr += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 1]) + "' />";
                        strDataPrev += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 2]) + "' />";
                    }

                    //Close <categories> element
                    strCategories += "</categories>";

                    //Close <dataset> elements
                    strDataCurr += "</dataset>";
                    strDataPrev += "</dataset>";

                    //Assemble the entire XML now
                    strXML += strCategories + strDataCurr + strDataPrev + "</graph>";

                    //Create the chart - MS Column 3D Chart with data contained in strXML
                    return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales", "600", "300", false, false);
                }
            }

            // return FusionCharts.RenderChart("../FusionCharts/FCF_MSColumn3D.swf", "", strXML, "productSales", "600", "300", false, false);
            string countdrp = "";
            if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
            {
                countdrp = "600";
            }
            else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
            {
                countdrp = "1000";
            }
            else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
            {
                countdrp = "2000";
            }
            else
            {
                countdrp = "600";
            }
            return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales", countdrp, "300", false, false);

        }
    }


}
