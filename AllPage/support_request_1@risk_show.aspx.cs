﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using FindDataAccessLayer;
using business;
using System.Data;
using System.Security.Cryptography;
using mybusiness;
using System.IO;
using System.Globalization;

public partial class AllPage_support_request_1_risk_show : System.Web.UI.Page
{
    FindDataAccess DA;
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["role"].ToString() != "13")
            {
                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();

                if (Request.Cookies["ASP.NET_SessionId"] != null)
                {
                    Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                    Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                }

                if (Request.Cookies["AuthToken"] != null)
                {
                    Response.Cookies["AuthToken"].Value = string.Empty;
                    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                }
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                Response.End();
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
            else
            {
                da = new mydataaccess1();
                DataTable circle = new DataTable();
                circle = da.getallcirclename();
                drpcircle.DataSource = circle;
                drpcircle.DataTextField = "circle";
                drpcircle.DataBind();
                drpcircle.Items.Insert(0, "Select");
                drpcircle.Items.Insert(1, "Testcircle");
                drpcircle.Items.Insert(2, "All");
                da = new mydataaccess1();
                dt = new DataTable();
                string sp_user_d = da.select_user_cookie(Session["user"].ToString());
                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                //Fill_GV_1ATRISK();
            }
        }
    }

    protected void Fill_GV_1ATRISK()
    {
        /*try
        {
            DA = new FindDataAccess();
            GridView1.DataSource = DA.Select_Person_Risk_Master_Request();
            GridView1.DataBind();
        }
        catch (Exception ex)
        {

        }*/
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        string FileName = "All_1@Risk_" + DateTime.Now + ".xls";
        Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GridView1.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        da = new mydataaccess1();
        string sp_user_d = da.select_user_cookie(Session["user"].ToString());

        if (drpcircle.SelectedIndex != 0 && drpcircle.SelectedItem.Text != "Select")
        {
            ImageButton1.Visible = true;
            if (TextBox1.Text != "" && TextBox2.Text != "")
            {
                string first = Convert.ToDateTime(TextBox1.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                string second = Convert.ToDateTime(TextBox2.Text).ToString("yyyy-MM-dd 23:59:59.000");

                DateTime first2 = Convert.ToDateTime(TextBox1.Text);
                DateTime second2 = Convert.ToDateTime(TextBox2.Text);

                /*var monthDiff = Math.Abs((second2.Year * 12 + (second2.Month - 1)) - (first2.Year * 12 + (first2.Month - 1)));
                if (monthDiff <= 3)
                {*/
                #region try_catch_finally
                try
                {
                    if (first2.Day > second2.Day || first2.Month > second2.Month || first2.Year > second2.Year)
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select small date in From than To!!!');</script>");
                    }
                    DA = new FindDataAccess();
                    dt = DA.Select_Person_Risk_Master_Request(drpcircle.SelectedItem.Text, sp_user_d, first, second);
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    GridView1.Visible = true;
                    rptlable.Text = "Report from '" + SpacialCharRemove.XSS_Remove(TextBox1.Text) + "' to '" + SpacialCharRemove.XSS_Remove(TextBox2.Text) + "'";
                    ImageButton1.Visible = true;
                    if (dt.Rows.Count <= 0)
                    {
                        ImageButton1.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    GridView1.DataSource = null;
                }
                #endregion try_catch_finally
                /*}
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please enter Dates having only 3 months difference!!!');</script>");
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView1.Visible = false;
                }*/
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Dates!!!');</script>");
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView1.Visible = false;
                ImageButton1.Visible = false;
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select circle!!!');</script>");
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.Visible = false;
            /*rptlable.Text = "";*/
        }
    }

    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }

    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
}
