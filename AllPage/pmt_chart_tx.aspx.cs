﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using InfoSoftGlobal;
using mybusiness;

public partial class PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        //FCLiteral.Text = CreateChart();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
        if (!IsPostBack)
        {
            drpchecksheet.Visible = false;

            try
            {
                int i = 12;

                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                // Marquee start
               /* da = new mydataaccess1();
                DataTable dtv5 = new DataTable();
                dtv5 = da.reminder_20_days();
                da = new mydataaccess1();
                DataTable dtv2 = new DataTable();
                dtv2 = da.reminder_20_days_v2();
                da = new mydataaccess1();
                DataTable dtv2ms = new DataTable();
                dtv2ms = da.reminder_20_days_v2ms();
                da = new mydataaccess1();
                DataTable dtall = new DataTable();
                dtall = da.reminder_20_days_all();
                da = new mydataaccess1();
                DataTable cvall = new DataTable();
                cvall = da.reminder_20_days_critical_all();
                da = new mydataaccess1();
                DataTable dtall1 = new DataTable();
                dtall1 = da.reminder_10_days_all();
                count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                // critical points
                da = new mydataaccess1();
                DataTable cv2 = new DataTable();
                cv2 = da.reminder_20_days_critical_v2();
                da = new mydataaccess1();
                DataTable cv5 = new DataTable();
                cv5 = da.reminder_20_days_critical();
                da = new mydataaccess1();
                DataTable cv2ms = new DataTable();
                cv2ms = da.reminder_20_days_critical_v2ms();
                count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                da = new mydataaccess1();
                DataTable dtv51 = new DataTable();
                dtv51 = da.reminder_10_days();
                da = new mydataaccess1();
                DataTable dtv21 = new DataTable();
                dtv21 = da.reminder_10_days_v2();
                da = new mydataaccess1();
                DataTable dtv2ms1 = new DataTable();
                dtv2ms1 = da.reminder_10_days_v2ms();
                count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
				*/
                // Marquee End
                // lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>10</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>24</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>3</span>)";
            }
            catch (Exception ee)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            ba = new myvodav2();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                           // drpcircle.Items.Insert(1, "All");
                        }
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                        }
                    }

                    if (Session["role"].ToString() == "12")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                           // drpcircle.Items.Insert(1, "All");

                        }
                    }

                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        panel2.Visible = true;

        drpchecksheet.Visible = true;
        drpchecksheet.SelectedIndex = 0;

       /* if (drpcircle.SelectedIndex != 0 && drpcircle.SelectedIndex != 1)
        {
            panel2.Visible = true;
            drpchecksheet.SelectedIndex = 0;
            FCLiteral.Text = CreateChart_circle();
            drpchecksheet.Visible = true;

        }
        if (drpcircle.SelectedIndex == 1)
        {
            drpchecksheet.Visible = false;

            panel2.Visible = true;
            FCLiteral.Text = CreateChart();
        }*/
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }

    public string CreateChart_circle_check()
    {
        da = new mydataaccess1();
        dt = new DataTable();
        string check = "TxEquip";

        dt = da.pie_chart_compl_circle_tx(drpcircle.SelectedValue);
        if (dt.Rows[0][0].ToString() == "no")
        {
            string strXML = "";
            lblmsg.Text = "No Data Found as there are no sites for " + SpacialCharRemove.XSS_Remove(check) + " .";
            strXML = "<graph caption='No Data Found'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            return strXML;
        }
        else
        {
            string strXML = "";


            strXML = "<graph caption=' Inspection Status Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + SpacialCharRemove.XSS_Remove(check) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
            strXML += "<set name='" + "Inspection Not Done" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + "' />";

            strXML += "<set name='" + "Inspection Done" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + "' />";


            strXML += "</graph>";



            return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);
        }

    }

    public string CreateChart_circle()
    {
        lblmsg.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();

        dt = da.pie_chart_compl_circle_tx(drpcircle.SelectedValue);
        string strXML = "";
        if (dt.Rows[0][0].ToString() == "no")
        {

            strXML = "<graph caption=' Inspection Status Circle: " + drpcircle.SelectedValue + " , CheckSheet: " + drpchecksheet.SelectedItem.Text + "' numberPrefix='%' decimalPrecision='0' >";

            lblmsg.Text = "No Data Found.";
            return strXML;

        }



        strXML = "<graph caption=' Inspection Status Circle: " + drpcircle.SelectedValue + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
        strXML += "<set name='" + "Inspection Not Done" + " + (" + dt.Rows[0][3].ToString() + ") +" + "' value='" + dt.Rows[0][1].ToString() + "' />";

        strXML += "<set name='" + "Inspection Done" + " + (" + dt.Rows[0][2].ToString() + ") + " + "' value='" + dt.Rows[0][0].ToString() + "' />";


        strXML += "</graph>";



        return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);


    }
    public string CreateChart()
    {

        lblmsg.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();
        string username = lblusername.Text;
        dt = da.pie_chart_compl(username);
        string strXML = "";


        strXML = "<graph caption=' Inspection Status Circle: " + drpcircle.SelectedValue + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='0' showNames='1'>";
        //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

        //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
        strXML += "<set name='" + "Inspection Not Done" + " + (" + dt.Rows[0][3].ToString() + ") +" + "' value='" + dt.Rows[0][1].ToString() + "' />";

        strXML += "<set name='" + "Inspection Done" + " + (" + dt.Rows[0][2].ToString() + ") + " + "' value='" + dt.Rows[0][0].ToString() + "' />";


        strXML += "</graph>";



        return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);


    }
    protected void drpchecksheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        if (drpchecksheet.SelectedIndex != 0)
        {
            panel2.Visible = true;
            FCLiteral.Text = CreateChart_circle_check();


        }
        else
        {
            panel2.Visible = false;
        }

    }

}
