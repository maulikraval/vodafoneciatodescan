﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="circleadmin_Active_PM_Chart_YesNoNA_new.aspx.cs"
    Inherits="admin_admin_Active_PM_Chart_YesNoNA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/AllPage/circleadmin_menu_ciat.ascx" TagName="Menu" TagPrefix="uc2" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HSW | Teleysia Networks Pvt. Ltd.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
    <link href="../AllPage/admin_styleinner1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/cufon-yui.js"></script>
    <script type="text/javascript" src="../js/arial.js"></script>
    <script type="text/javascript" src="../js/cuf_run.js"></script>
    <script src="../js/jquery.js" type="text/javascript"></script>
    <link href="../css/menu.css" rel="stylesheet" type="text/css" />
    <script src="../js/menu.js" type="text/javascript"></script>
    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #roll a
        {
            display: inline-table;
            text-decoration: none;
        }
        #roll ul
        {
            margin: 0;
            padding: 0;
            list-style: none;
        }
        #roll ul li
        {
            list-style-type: none;
            background: none;
        }
        #roll ul li a, #roll ul li a:visited
        {
            /* styles for the default button state */
            margin: 0 0 5px 0;
            padding: 0 15px;
            line-height: 32px; /* this value must be at least twice the border-radius value */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #EAEAEA url(/images/misc/pattern1.gif);
            font-family: 'Arial Black' , Impact, sans-serif;
            font-size: 16px;
            text-transform: lowercase; /* remove this line unless you want to use lowercase, uppercase or small-caps */
            letter-spacing: -.06em; /* should be set to 0 for most cases */
            -moz-border-radius: 16px;
            -khtml-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
        }
        #roll ul li a:hover
        {
            /* styles for the rollover button state */
            color: #FFF;
            text-shadow: 0px 0px 3px #000;
            background: #0099FF url(/images/misc/pattern2.gif);
        }
        .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 762px;
        }
        .style2
        {
            width: 269px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <script src="../FusionCharts/FusionCharts.js" type="text/javascript"></script>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="updatepanel" runat="server">
        <ContentTemplate>--%>
    <div class="main">
        <div class="header">
            <div class="header_resize">
                <div class="logo">
                    <table width="100%">
                        <tr>
                            <td width="6%">
                                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../images/1.jpg" Height="48px" Width="57px" />--%>
                            </td>
                            <td class="style1">
                              <label style="font-size:20px;color:#ffa500;font-weight:bold"/">
                                Cellsite Inspection Automation Tool
                                <br />
                                <span style="font-size: 11px; font-style: italic; /*margin-left: -20%;*/">
                                    An initiative of VIL Technology HSW
                                </span>
                            </label>
                            </td>
                            <td width="25%" style="text-align: right;">
                                <asp:Label ID="lblusername" runat="server"></asp:Label>
                                |
                                <asp:LinkButton ID="lnkchange" runat="server" OnClick="lnkchange_Click" CausesValidation="False">Change Password</asp:LinkButton>
                                |
                                <asp:LinkButton ID="lnklogout" runat="server" OnClick="lnklogout_Click" CausesValidation="False">Log Out</asp:LinkButton>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clr">
                </div>
                <uc2:Menu ID="menubar" runat="server" />
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="content">
            <div class="content_resize">
                <div class="mainbar">
                    <div class="article1">
                        <marquee behaviour="slide" direction="left" scrollamount="3">
                    <asp:Label ID="lblmarquee" runat="server"  CssClass="lblmarquee"></asp:Label></marquee>
                    </div>
                </div>
                <div class="sidebar">
                    <div class="gadget">
                        <table class="style2">
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:DropDownList ID="drpcircle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged"
                                        Width="200px" CssClass="genall">
                                    </asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                        ErrorMessage="*" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="False" Text="Zone Name" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:DropDownList ID="DrpZone" runat="server" AutoPostBack="true" Width="200px" CssClass="genall"
                                        OnSelectedIndexChanged="DrpZone_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="False" Text="Check Sheet" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                     <asp:DropDownList ID="drpchecksheet" runat="server" AutoPostBack="true" Width="200px"
                                        CssClass="genall" OnSelectedIndexChanged="drpchecksheet_SelectedIndexChanged1">
                                        <asp:ListItem>--Select--</asp:ListItem>
                                        
                                         <asp:ListItem Value="46">BTS</asp:ListItem> 
                                        <asp:ListItem Value="47">MW</asp:ListItem>
                                        <asp:ListItem Value="48">TMEA</asp:ListItem>
                                        <asp:ListItem Value="49">BSC</asp:ListItem>
                                        <asp:ListItem Value="50">InfraVisual</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="False" Text="Question" CssClass="lblall"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                   <asp:DropDownList ID="DrpQuestion" runat="server" AutoPostBack="true" 
                                        Width="200px" CssClass="genall" onselectedindexchanged="DrpQuestion_SelectedIndexChanged"
                                       >
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="mainbar">
                    <div class="article">
                        <table width="100%">
                            <tr>
                                <td width="5%" style="text-align: center">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;" width="25%">
                                    <div>
                                        <table class="style2">
                                            <tr>
                                                <td>
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="panel2" runat="server" Width="700px"
                                                        Height="400px">
                                                        <h2>
                                                            &nbsp;</h2>
                                                        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                                                        <asp:Literal ID="FCLiteral" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td width="5%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;" width="25%">
                                    <div>
                                        <table class="style2">
                                            <tr>
                                                <td>
                                                    <asp:Panel BorderColor="Black" BorderWidth="2px" ID="pnlbarchart" runat="server"
                                                        Width="700px" Height="400px">
                                                        <h2>
                                                            &nbsp;</h2>
                                                        <asp:Label ID="Labelblmsgbarchart" runat="server" Text=""></asp:Label>
                                                        <asp:Literal ID="FCLiteralbarchart" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td width="5%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="clr">
                </div>
            </div>
        </div>
        <div class="fbg">
            <div class="footer">
                <div id="Copyright 2011">
                    <a href="http://apycom.com/"></a>
                </div>
                <%--   <p class="lf">
                    &copy; Copyright 2011 <a href="#"></a>.</p>
                <p class="rf">
                    Powered By. Teleysia Networks Pvt. Ltd.</p>--%>
                <div class="clr">
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
