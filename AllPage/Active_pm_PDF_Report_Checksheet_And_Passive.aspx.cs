﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using mybusiness;
using dataaccesslayer;

public partial class Active_pm_PDF_Report_Checksheet_And_Passive : System.Web.UI.Page
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        //checksheet_div.Visible = false;
       // btn_pdf.Visible = false;
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
        try
        {
		   da = new mydataaccess1();
      string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            if (!IsPostBack)
            {
                try
                {
                    int i = 12;
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString()==sp_user_d)
                    {

                    }
                    else
                    {

                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch
                {

                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }

                int count = 0;
                int count1 = 0;
                int count2 = 0;
                // Marquee start
                /*    da = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da.reminder_20_days();
                    da = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da.reminder_20_days_v2();
                    da = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da.reminder_20_days_v2ms();
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da.reminder_20_days_critical_v2();
                    da = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da.reminder_20_days_critical();
                    da = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da.reminder_20_days_critical_v2ms();
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da.reminder_10_days();
                    da = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da.reminder_10_days_v2();
                    da = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da.reminder_10_days_v2ms();
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";*/
                // Marquee End
             //   string sp_user_d = "";

				 dt = new DataTable();
              

                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                if (lblusername.Text == "")
                {
                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                }
                ba = new myvodav2();
                da = new mydataaccess1();
                dt = new DataTable();

                dt = da.getcirclename(sp_user_d);
                //ddlcircle.DataSource = dt;
                //ddlcircle.DataTextField = "Circle";
                //ddlcircle.DataBind();
                //ddlcircle.Items.Insert(0, "select");


            }


        }

        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            DataTable zone = new DataTable();
            //  ba.Circle = ddlcircle.SelectedValue;
            zone = da.getzonesbycirclename(ba);

            //ddlzone.DataSource = zone;
            //ddlzone.DataTextField = "zone";
            //ddlzone.DataBind();
            //ddlzone.Items.Insert(0, "select");
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ba = new myvodav2();
            da = new mydataaccess1();
            dt = new DataTable();
            // ba.Zone = ddlzone.SelectedItem.Text.ToString();
            // ba.Circle = ddlcircle.SelectedItem.Text.ToString();
            dt = da.getviewzone(ba);
            gridview1.DataSource = dt;
            gridview1.DataBind();
            Label2.Text = "";

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    //protected void grd1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        GridViewRow gr = gridview1.SelectedRow;
    //        txt1.Text = gr.Cells[1].Text;
    //        txt2.Text = gr.Cells[2].Text;
    //        txt3.Text = gr.Cells[3].Text;
    //        txt4.Text = gr.Cells[4].Text;
    //        txt5.Text = gr.Cells[5].Text;
    //        txt6.Text = gr.Cells[6].Text;
    //        txt7.Text = gr.Cells[7].Text;
    //        txt8.Text = gr.Cells[8].Text;
    //        txt9.Text = gr.Cells[9].Text;
    //        txt10.Text = gr.Cells[10].Text;
    //        txt11.Text = gr.Cells[11].Text;
    //        txt12.Text = gr.Cells[12].Text;
    //        txt13.Text = gr.Cells[13].Text;

    //        txt14.Text = gr.Cells[14].Text;

    //        txtipprovider.Text = gr.Cells[15].Text;
    //        //txtipprovider.Text = gr.Cells[15].Text;
    //        txtipid.Text = gr.Cells[16].Text;
    //        txtsitetype.Text = gr.Cells[17].Text;
    //        if (txt13.Text.ToLower() == "ciic v1" || txt13.Text.ToLower() == "cic v1")
    //        {
    //            mod1.Hide();
    //            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('You can not update this site');</script>");

    //        }
    //        else
    //        {
    //            mod1.Show();
    //        }


    //    }
    //    catch (Exception ex)
    //    {
    //        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
    //    }
    //}
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
            string sp_user_d = "";
            da = new mydataaccess1();
            sp_user_d = da.select_user_cookie(Session["user"].ToString());

            ba = new myvodav2();
            da = new mydataaccess1();
            ba.Siteid = txt1.Text;
            ba.Lat = Convert.ToDouble(txt4.Text);
            ba.Longitude = Convert.ToDouble(txt5.Text);
            double dis = da.select_update_lat_long(ba);

            if (dis <= 2000)
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('1');</script>");
                ba = new myvodav2();
                da = new mydataaccess1();
                ba.Siteid = txt1.Text.Trim();
                ba.Sitename = txt2.Text;
                ba.Address = txt3.Text;
                ba.Lat = Convert.ToDouble(txt4.Text);
                ba.Longitude = Convert.ToDouble(txt5.Text);
                ba.Towertype = txt6.Text;
                ba.Userid = txt7.Text;
                ba.Technician = txt8.Text;
                ba.Circle = txt9.Text;
                ba.Zone = txt10.Text;
                ba.Subzone = txt11.Text;
                ba.Tech_contact = txt12.Text;
                ba.Check_sheet = txt13.Text;
                ba.Datetime = txt14.Text;
                ba.User = sp_user_d;
                ba.Provider = txtipprovider.Text;
                ba.Ip_id = txtipid.Text;
                ba.Site_type = txtsitetype.Text;
                int i = da.InsertVodaSite(ba);
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Updated Successfully...');</script>");
            }
            else
            {
                //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('2');</script>");
                ba = new myvodav2();
                da = new mydataaccess1();
                ba.Siteid = txt1.Text.Trim();
                ba.Sitename = txt2.Text;
                ba.Address = txt3.Text;
                ba.Lat = Convert.ToDouble(txt4.Text);
                ba.Longitude = Convert.ToDouble(txt5.Text);
                ba.Towertype = txt6.Text;
                ba.Userid = txt7.Text;
                ba.Technician = txt8.Text;
                ba.Circle = txt9.Text;
                ba.Zone = txt10.Text;
                ba.Subzone = txt11.Text;
                ba.Tech_contact = txt12.Text;
                ba.Check_sheet = txt13.Text;
                ba.Datetime = txt14.Text;
                ba.User = sp_user_d;
                ba.Provider = txtipprovider.Text;
                ba.Ip_id = txtipid.Text;
                ba.Site_type = txtsitetype.Text;
                int i = da.InsertVodaSite_without_latlong(ba);

                da = new mydataaccess1();
                ba = new myvodav2();
                ba.Siteid = txt1.Text.Trim();
                ba.Lat = Convert.ToDouble(txt4.Text);
                ba.Longitude = Convert.ToDouble(txt5.Text);
                ba.Distance = dis;
                da.insert_into_latlong(ba);
                Response.Redirect("admin_approve_lat_long.aspx", false);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your Lat/Long are sent for approval/Rejection to Teleysia Support');</script>");

            }
            //if (ddlcircle.SelectedIndex != 0 && ddlzone.SelectedIndex != 0)
            //{
            ba = new myvodav2();
            da = new mydataaccess1();
            dt = new DataTable();
            //  ba.Zone = ddlzone.SelectedItem.Text.ToString();
            //  ba.Circle = ddlcircle.SelectedValue;
            dt = da.getviewzone(ba);
            gridview1.DataSource = dt;
            gridview1.DataBind();
            // }
            if (TextBox1.Text != "")
            {
                da = new mydataaccess1();
                ba = new myvodav2();
                dt = new DataTable();
                ba.Siteid = TextBox1.Text;
                //   ba.Circle = "";
                //  ba.Zone = "";
                dt = da.searchsiteid(ba);
                gridview1.DataSource = dt;
                gridview1.DataBind();
            }
            //  Label2.Text = "Update Successfully......";



        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {


            da = new mydataaccess1();
            da = new mydataaccess1();
            dt = new DataTable();
            string a = da.select_user_cookie(Session["user"].ToString());
            da.update_user_master_status(a);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
   
    protected void CheckBox0_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            int cnt = tbl_datachk.Rows.Count;
            CheckBox d = tbl_datachk.Rows[1].FindControl("CheckBox0") as CheckBox;
            string checkval = d.Checked.ToString();
            for (int i = 0; i < cnt; i++)
            {
                if (tbl_datachk.Rows[i].Cells[2].InnerText == "Site ID") {
                }
                else {
                    
                    if (checkval == "True")
                    {
                        string chname = "CheckBox" + i;
                        CheckBox d1 = tbl_datachk.Rows[i].Cells[0].FindControl(chname) as CheckBox;
                        d1.Checked = true;
                        //string val = d1.Attributes["Value"].ToString();
                    }
                    else {
                        string chname = "CheckBox" + i;
                        CheckBox d1 = tbl_datachk.Rows[i].Cells[0].FindControl(chname) as CheckBox;
                        d1.Checked = false;
                    }
                }
            }

            //if (CheckBox0.Checked == true)
            //{
            
                //foreach (HtmlTableRow row in tbl_datachk.Rows)
                //{
                //    //CheckBox chk = row.FindControl("CheckBox0") as CheckBox;

                //    if (row.Cells[2].InnerText == "Site ID") { }
                //    else
                //    {
                //        CheckBox d =  row.Cells[0].FindControl("CheckBox1") as CheckBox;
                //        d.Checked = true;
                //    }
                //}
            //}
            //else {

            //}
        }
        catch { }

    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    //    String qu = SqlDataSource1.SelectCommand;
    //    //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

    //    //    SqlDataSource1.SelectCommand = qu; 
    //    //    GridView1.DataBind();

    //    //GridView1.Columns[0].Visible = false;
    //    string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
    //    foreach (GridViewRow gr in gridview1.Rows)
    //    {
    //        gr.Cells[2].Attributes.Add("class", "date");
    //    }
    //    Response.Clear();

    //    Response.ClearHeaders();

    //    Response.AppendHeader("Cache-Control", "no-cache");

    //    Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

    //    Response.Charset = "";

    //    // If you want the option to open the Excel file without saving than

    //    // comment out the line below

    //    // Response.Cache.SetCacheability(HttpCacheability.NoCache);

    //    Response.ContentType = "application/vnd.ms-excel";
    //    //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    //    System.IO.StringWriter stringWrite = new System.IO.StringWriter();

    //    System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

    //    Response.Write(datestyle);
    //    gridview1.RenderControl(htmlWrite);

    //    Response.Write(stringWrite.ToString());

    //    Response.End();
    //    gridview1.Columns[0].Visible = true;
    //}

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text != "")
        {
            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            ba.Siteid = TextBox1.Text;
            //   ba.Circle = "";
            //  ba.Zone = "";
            dt = da.searchsiteid(ba);
            if (dt.Rows.Count > 0)
            {
                string site_id = dt.Rows[0][0].ToString();
                string site_name = dt.Rows[0][1].ToString();
                panel1_tbl.Visible = true;
                //checksheet_div.Visible = true;
                View_Data.Visible = true;
                
                foreach (HtmlTableRow row in tbl_datachk.Rows)
                {
                    //CheckBox chk = (CheckBox)row.FindControl("checkbox");
                    if (row.Cells[2].InnerText == "Site ID") { }
                    else { 
                    row.Cells[2].InnerText = site_id;
                    row.Cells[3].InnerText = site_name;
                    }
                }

            }
            else
            {
                Label2.Text = "Site Not Availble In System !!!";
                //checksheet_div.Visible = false;
                View_Data.Visible = false;
                panel1_tbl.Visible = false;
            }
        }
        else
        {
            Label2.Text = "Please enter siteid!!!";
        }
    }
    protected void btnView_Data_Click(object sender, EventArgs e)
    {
        try
        {
            string checksheet_value = "";
            int cnt = tbl_datachk.Rows.Count - 1;
            //CheckBox d = tbl_datachk.Rows[1].FindControl("CheckBox0") as CheckBox;
            //string checkval = d.Checked.ToString();
            for (int i = 1; i < cnt; i++)
            {
                if (tbl_datachk.Rows[i].Cells[2].InnerText == "Site ID")
                {
                }
                else
                {
                    string chname = "CheckBox" + i;
                    CheckBox d = tbl_datachk.Rows[i].FindControl(chname) as CheckBox;
                    string checkval_ = d.Checked.ToString();

                    int checkval = Convert.ToInt32(d.Attributes["Value"]);
                    if (checkval > 0 && checkval_ == "True")
                    {
                        string str_val = d.Attributes["name"].ToString();
                        checksheet_value += str_val + ", ";
                    }
                    else
                    {
                    }
                }
            }
            if (checksheet_value != "")
            {


                ba = new myvodav2();
                ba.Siteid = TextBox1.Text;
                da = new mydataaccess1();
                dt = da.searchsiteid(ba);

                PdfCreation pdfCreation = new PdfCreation();
                string pdfFileName = pdfCreation.CreatePdfOfChecksheets(SpacialCharRemove.SpacialChar_Remove(ba.Siteid), SpacialCharRemove.SpacialChar_Remove(ba.Siteid), checksheet_value);

                if (dt.Rows.Count > 0)
                {
                    gridview1.DataSource = dt;
                    gridview1.DataBind();
                }
                else
                {
                    gridview1.DataSource = null;
                    gridview1.DataBind();
                }

                string strUserAgent = Request.UserAgent.ToString().ToLower();
                if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iphone") ||
                strUserAgent.Contains("blackberry") || strUserAgent.Contains("mobile") ||
                strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera mini") ||
                strUserAgent.Contains("palm"))
                {
                    Response.ContentType = "Application/pdf";

                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdfFileName);

                    Response.TransmitFile(System.IO.Path.Combine(ConfigurationManager.AppSettings["Pdf_Report_Path"].ToString(), pdfFileName));

                    Response.End();
                    //           Response.Redirect("https://ciat.vodafone.in/report_ptw/height_pdf/" + file);
                }
                else
                {
                    if (Session["imei"] == null)
                    {
                        Response.ContentType = "Application/pdf";

                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdfFileName);
                        //Response.TransmitFile("C:/Users/HareshPatel/Desktop/ChangesPortal/AllPageInOneFolder/Copy of Vodafone_production_for_deploy/PM_PDF/" + file1);
                        //Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_08_10_2014/PM_PDF/" + file1);
                        Response.TransmitFile(System.IO.Path.Combine(ConfigurationManager.AppSettings["Pdf_Report_Path"].ToString(), pdfFileName));
                        //Response.TransmitFile("D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/PM_PDF/" + file1);
                        // Response.TransmitFile("E:/Haresh/Project/Vodafone_PM/Copy of Vodafone_production_for_deploy/Copy of Vodafone_production_for_deploy/PM_PDF/" + file);
                        Response.End();
                    }
                    else
                    {
                        Response.ContentType = "Application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdfFileName);
                        //Response.TransmitFile("C:/Users/HareshPatel/Desktop/ChangesPortal/AllPageInOneFolder/Copy of Vodafone_production_for_deploy/PM_PDF/" + file1);
                        //Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_08_10_2014/PM_PDF/" + file1);
                        Response.TransmitFile(System.IO.Path.Combine(ConfigurationManager.AppSettings["Pdf_Report_Path"].ToString(), pdfFileName));
                        //Response.TransmitFile("D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/PM_PDF/" + file1);
                        Response.End();
                        //Response.Redirect("https://ciat.vodafone.in/report_ptw/height_pdf/" + file);
                    }
                }
            }
            else {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select at least one checksheet. ');</script>");

            }

        }
        catch (Exception ex)
        {

        }
    }
}
