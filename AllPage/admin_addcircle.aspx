﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_addcircle.aspx.cs" Inherits="AllPage_admin_addcircle" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var isShift = false;
        $(document).ready(function () {
            $("#<%=SpacialCharRemove.XSS_Remove(txtcircle.ClientID) %>").keypress(function (e) {
                //alert(e.keyCode);

                if (e.keyCode == 16) {
                    //alert("Shift");
                    isShift = true;
                } else {
                    isShift = false;
                }
                if (e.keyCode == 53 && isShift == true) {
                    return false;
                } else if (e.keyCode == 37) {
                    return false;
                } else {
                    return true;
                }
            })

        })
    </script>
    <table width="100%">
        <tr>
            <td style="width: 5%"></td>
            <td style="text-align: center;" colspan="2">
                <asp:Label ID="Label2" runat="server" CssClass="lblstly"
                    Text="Add Circle"></asp:Label>
            </td>
            <td style="width: 5%"></td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;</td>
            <td style="text-align: center;" colspan="2">
                <hr />
            </td>
            <td style="width: 5%">&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">
                <asp:Label ID="lblcircle" runat="server" Text="Circle :" CssClass="lblall"></asp:Label>
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:TextBox ID="txtcircle" runat="server" Width="150px" CssClass="genall"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcircle"
                    ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>

                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                    TargetControlID="txtcircle" FilterMode="InvalidChars"
                    InvalidChars="0123456789.!@#$%^&*()_+={[}]:;',<>?/\|-">
                </cc1:FilteredTextBoxExtender>
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">&nbsp;
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 30px;">&nbsp;
            </td>
            <td style="width: 35%; text-align: right; height: 30px;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left; height: 30px;">
                <asp:Button ID="btncircle" runat="server" OnClick="btncircle_Click"
                    Text="Add Circle" ValidationGroup="a" />
            </td>
            <td style="width: 5%; height: 30px;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">
                <asp:Label ID="lblmsg" runat="server" Text="Label"></asp:Label>
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 20px;">&nbsp;
            </td>
            <td style="width: 35%; text-align: right; height: 20px;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left; height: 20px;">&nbsp;
            </td>
            <td style="width: 5%; height: 20px;">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%; height: 20px;"></td>
            <td style="text-align: center; height: 20px;"></td>
            <td style="text-align: center; height: 20px;">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <asp:Panel ID="pan1" ScrollBars="Auto" Height="200px" Width="410px"
                    runat="server">

                    <asp:GridView ID="grduser" runat="server" BackColor="#CCCCCC" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                        OnSelectedIndexChanged="grduser_SelectedIndexChanged" Width="100%"
                        OnRowDataBound="grduser_RowDataBound">
                        <Columns>
                            <asp:CommandField HeaderText="Edit" SelectText="Edit" ShowSelectButton="True" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                </asp:Panel>
            </td>
            <td style="width: 5%; height: 20px;"></td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="text-align: center;" colspan="2">&nbsp;
                 <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                     TargetControlID="panel2" runat="server">
                 </cc1:RoundedCornersExtender>
                <asp:Panel ID="panel2" runat="server" Width="700px" Height="170px"
                    ForeColor="Black" BackColor="#E4E4E4">
                    <table width="100%">
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td style="text-align: center" colspan="3">
                                <asp:Label ID="lblid" runat="server" Text="Label" Visible="False"></asp:Label>
                                <asp:Label ID="Label4" runat="server" CssClass="lblstly">Edit Circle</asp:Label>
                            </td>
                            <td width="5%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 5%">&nbsp;</td>
                            <td style="text-align: right;" colspan="2">
                                <hr />
                            </td>
                            <td style="width: 5%">&nbsp;</td>
                        </tr>

                        <tr>
                            <td style="width: 5%">&nbsp;
                            </td>
                            <td style="width: 35%; text-align: right;">
                                <asp:Label ID="Label1" runat="server" CssClass="lblall" Text="Circle :"></asp:Label>
                            </td>
                            <td style="width: 55%; text-align: left;">
                                <asp:TextBox ID="txteditcircle" runat="server" CssClass="genall" Width="150px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                    ControlToValidate="txteditcircle" ErrorMessage="*" ValidationGroup="b"></asp:RequiredFieldValidator>
                                <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                    InvalidChars="./*-+@#$%^&amp;*~!&lt;&gt;:&quot;?|=" TargetControlID="txtcircle">
                </cc1:FilteredTextBoxExtender>--%>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                    FilterMode="InvalidChars"
                                    InvalidChars="0123456789.!@#$%^&amp;*()_+={[}]:;',&lt;&gt;?/\|"
                                    TargetControlID="txteditcircle">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 5%">&nbsp;
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 5%">&nbsp;</td>
                            <td style="width: 35%; text-align: right;">&nbsp;</td>
                            <td style="width: 55%; text-align: left;">
                                <asp:Button ID="btncircle0" runat="server" OnClick="btncircle0_Click"
                                    Text="Update" ValidationGroup="b" />
                                <asp:Button ID="btncancel" runat="server" CausesValidation="false"
                                    Text="Cancel" OnClick="btncancel_Click" />
                                <asp:Label ID="lblerror" runat="server" ForeColor="#FF3300"></asp:Label>
                            </td>
                            <td style="width: 5%">&nbsp;</td>
                        </tr>

                    </table>
                </asp:Panel>
                <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
                <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
                    TargetControlID="butt1" PopupControlID="panel2">
                </cc1:ModalPopupExtender>
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 5%">&nbsp;
            </td>
            <td style="width: 35%; text-align: right;">&nbsp;
            </td>
            <td style="width: 55%; text-align: left;">&nbsp;
            </td>
            <td style="width: 5%">&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
