﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using mybusiness;
using System.Data;
using System.Web.Security;
public partial class AllPage_admin_addcircle : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;


    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    public static bool TryParseGuid(string s, out Guid guid)
    {
        try
        {
            guid = new Guid(s);
            return true;
        }
        catch
        {
            guid = Guid.Empty;
            return false;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //First, check for the existence of the Anti-XSS cookie
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        //If the CSRF cookie is found, parse the token from the cookie.
        //Then, set the global page variable and view state user
        //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
        //method.
        // if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        if (requestCookie != null && TryParseGuid(requestCookie.Value.ToString(), out requestCookieGuidValue))
        {
            //Set the global token variable so the cookie value can be
            //validated against the value in the view state form field in
            //the Page.PreLoad method.
            _antiXsrfTokenValue = requestCookie.Value;

            //Set the view state user key, which will be validated by the
            //framework during each request
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        //If the CSRF cookie is not found, then this is a new session.
        else
        {
            //Generate a new Anti-XSRF token
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

            //Set the view state user key, which will be validated by the
            //framework during each request
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            //Create the non-persistent CSRF cookie
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                //Set the HttpOnly property to prevent the cookie from
                //being accessed by client side script
                HttpOnly = true,

                //Add the Anti-XSRF token to the cookie value
                Value = _antiXsrfTokenValue
            };

            //If we are using SSL, the cookie should be set to secure to
            //prevent it from being sent over HTTP connections
            if (FormsAuthentication.RequireSSL &&
            Request.IsSecureConnection)
                responseCookie.Secure = true;

            //Add the CSRF cookie to the response
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += Page_PreLoad;
    }

    protected void Page_PreLoad(object sender, EventArgs e)
    {
        //During the initial page load, add the Anti-XSRF token and user
        //name to the ViewState
        if (!IsPostBack)
        {
            //Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

            //If a user name is assigned, set the user name
            ViewState[AntiXsrfUserNameKey] =
            Context.User.Identity.Name ?? String.Empty;
        }
        //During all subsequent post backs to the page, the token value from
        //the cookie should be validated against the token in the view state
        //form field. Additionally user name should be compared to the
        //authenticated users name
        else
        {
            //Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
            || (string)ViewState[AntiXsrfUserNameKey] !=
            (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                da = new mydataaccess1();
                string sp_user_ = da.select_user_cookie(Session["user"].ToString());
                if (Session["role"].ToString() != "4" && Session["um"].ToString() != sp_user_)
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                else
                {
                    lblmsg.Visible = false;
                    da = new mydataaccess1();
                    ba = new myvodav2();
                    dt = new DataTable();
                    dt = da.selectcircle(ba);
                    grduser.DataSource = dt;
                    grduser.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void btncircle_Click(object sender, EventArgs e)
    {
        try
        {
            lblmsg.Visible = false;
            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            ba.Circle = txtcircle.Text;

            int s = da.insertcircle(ba);
            if (s == -1)
            {
                lblmsg.Visible = true;
                lblmsg.Text = "Circle Already Exist";
            }
            else
            {
                lblmsg.Visible = true;
                lblmsg.Text = "Circle Added Succesfully";
            }
            txtcircle.Text = "";
            da = new mydataaccess1();
            ba = new myvodav2();
            dt = new DataTable();
            dt = da.selectcircle(ba);
            grduser.DataSource = dt;
            grduser.DataBind();
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }

    protected void grduser_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow gr = grduser.SelectedRow;
        lblid.Text = SpacialCharRemove.XSS_Remove(gr.Cells[2].Text);
        txteditcircle.Text = gr.Cells[1].Text;
        mod1.Show();
    }
    protected void grduser_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        da = new mydataaccess1();
        ba = new myvodav2();
        dt = new DataTable();
        dt = da.selectcircle(ba);
        if (dt.Rows.Count > 0)
        {

            e.Row.Cells[2].Visible = false;

        }


        //if ((e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate) &&

        //(e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header))
        //{

        //    e.Row.Cells[2].Visible = false;

        //}

    }
    protected void btncircle0_Click(object sender, EventArgs e)
    {

        lblmsg.Visible = false;
        da = new mydataaccess1();
        ba = new myvodav2();
        dt = new DataTable();
        ba.Circle = txteditcircle.Text;
        ba.Id = Convert.ToInt32(lblid.Text);
        da.update_circle(ba);

        da = new mydataaccess1();
        ba = new myvodav2();
        dt = new DataTable();
        dt = da.selectcircle(ba);
        grduser.DataSource = dt;
        grduser.DataBind();
        mod1.Hide();



    }
    protected void btncancel_Click(object sender, EventArgs e)
    {
        txteditcircle.Text = "";
        mod1.Hide();
    }
}
