﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using mybusiness;
using dataaccesslayer;

public partial class AllPage_admin_Viewsite : System.Web.UI.Page
{
    myvodav2 ba;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("admin_viewsites1.aspx",false);
        try
        {
            if (!IsPostBack)
            {

                ba = new myvodav2();
                da = new mydataaccess1();
                dt = new DataTable();

                dt = da.getcirclename(Session["user"].ToString());
                ddlcircle.DataSource = dt;
                ddlcircle.DataTextField = "Circle";
                ddlcircle.DataBind();
                ddlcircle.Items.Insert(0, "-------select-------");





            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
        da = new mydataaccess1();
        ba = new myvodav2();
        DataTable zone = new DataTable();
        ba.Circle = ddlcircle.SelectedItem.Text.ToString();
        zone = da.getzonesbycirclename(ba);

        ddlzone.DataSource = zone;
        ddlzone.DataTextField = "zone";
        ddlzone.DataBind();
        ddlzone.Items.Insert(0, "----select-----");
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
        ba = new myvodav2();
        da = new mydataaccess1();
        dt = new DataTable();
        ba.Zone = ddlzone.SelectedItem.Text.ToString();
        ba.Circle = ddlcircle.SelectedItem.Text.ToString();
        dt = da.getviewzone(ba);
        grd1.DataSource = dt;
        grd1.DataBind();

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void grd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
        GridViewRow gr = grd1.SelectedRow;
        txt1.Text = gr.Cells[1].Text;
        txt2.Text = gr.Cells[2].Text;
        txt3.Text = gr.Cells[3].Text;
        txt4.Text = gr.Cells[4].Text;
        txt5.Text = gr.Cells[5].Text;
        txt6.Text = gr.Cells[6].Text;
        txt7.Text = gr.Cells[7].Text;
        txt8.Text = gr.Cells[8].Text;
        txt9.Text = gr.Cells[9].Text;
        txt10.Text = gr.Cells[10].Text;
        txt11.Text = gr.Cells[11].Text;
        txt12.Text = gr.Cells[12].Text;
        txt13.Text = gr.Cells[13].Text;
        txt14.Text = gr.Cells[14].Text;

        mod1.Show();

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        try
        {
        ba = new myvodav2();
        da = new mydataaccess1();
        ba.Siteid = txt1.Text;
        ba.Sitename = txt2.Text;
        ba.Address = txt3.Text;
        ba.Lat = Convert.ToDouble(txt4.Text);
        ba.Longitude = Convert.ToDouble(txt5.Text);
        ba.Towertype = txt6.Text;
        ba.Userid = txt7.Text;
        ba.Technician = txt8.Text;
        ba.Circle = txt9.Text;
        ba.Zone = txt10.Text;
        ba.Subzone = txt11.Text;
        ba.Tech_contact = txt12.Text;
        ba.Check_sheet = txt13.Text;
        ba.Datetime = txt14.Text;
        string sp_user_d = "";
        // split for decodation username.
        da = new mydataaccess1();
        sp_user_d = da.select_user_cookie(Session["user"].ToString());

        ba.User = sp_user_d;
        int i = da.InsertVodaSite(ba);
        ba = new myvodav2();
        da = new mydataaccess1();
        dt = new DataTable();
        ba.Zone = ddlzone.SelectedItem.Text.ToString();
        dt = da.getviewzone(ba);
        grd1.DataSource = dt;
        grd1.DataBind();

        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}
