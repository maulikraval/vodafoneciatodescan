﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="circleadmin_menu_ciat.ascx.cs"
    Inherits="admin_menu" %>
<div id="menu">
    <ul class="menu">
        <li><a href="#"><span>Dashboard</span></a>
            <ul>
                <li><a href="circleadmin_20daysreminder.aspx"><span>CIAT</span></a> </li>
                       <li><a href="circleadmin_dashboard_active.aspx"><span>ACTIVE</span></a> </li>               
                <li><a href="circleadmin_dashboard_tx.aspx"><span>TXEQUIP</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>User Details</span></a>
            <ul>
                <%-- <li><a href="circleadmin_adduser_circle_both.aspx"><span>CreateUser</span></a> </li>--%>
               <li><a href="circleadmin_adduser_circle.aspx"><span>CreateUser</span></a> </li>
                <li><a href="circleadmin_showuser_circle.aspx"><span>ViewUser</span></a> </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>SiteDetails</span></a>
            <ul>
                <li><a href="circleadmin_UploadSitesNew_circle.aspx"><span>Upload Site</span></a></li>
                
                <li><a href="circleadmin_viewsites1.aspx"><span>View Site</span></a> </li>
                <%--<li><a href="circleadmin_viewsites1.aspx"><span>View Site</span></a> </li>--%>
                <li><a href="circleadmin_view_site_report.aspx"><span>Site Master</span></a> </li>
                 <li><a href="Circleadmin_Site_Download.aspx"><span>Site Dump</span></a> </li>
                <li><a href="Circleadmin_Site_New_Download.aspx"><span>Sub Site Dump</span></a> </li>
                <li><a href="circleadmin_approve_lat_long.aspx"><span>Approve Lat/Long</span></a>
                </li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>MobileDetails</span></a>
            <ul>
                <li><a href="circleadmin_manual_add_imei_circle.aspx"><span>Add Mobile Manually</span></a></li>
                <li><a href="circleadmin_UploadImei_circle.aspx"><span>Upload Mobile</span></a></li>
                <li><a href="circleadmin_ViewImei_circle.aspx"><span>View Register Mobile</span></a>
                </li>
            </ul>
        </li>
        <li><a href="circleadmin_finduser.aspx" class="parent"><span>FindSites</span></a></li>
        <li><a href="circleadmin_RequestUsers.aspx" class="parent"><span>Reset/Modify Site</span></a></li>
        
        <li><a class="parent"><span>Report</span></a>
            <ul>
                <li><a href="circleadmin_report_circle.aspx"><span>CIAT Excel Report</span></a></li>
                <li><a href="circleadmin_CAR_Report.aspx"><span>CIAT CAR Report</span></a></li>
                <li><a href="circleadmin_car_pdf_Report.aspx"><span>CIAT PDF Report</span></a></li>
                <li><a href="Circleadmin_Circle_Report_archive.aspx"><span>CIAT Archieve Report</span></a></li>
            </ul>
        </li>
        <li><a class="parent"><span>Chart</span></a>
            <ul>
                <li><a href="circleadmin_chart.aspx"><span>CIAT Inspection(PieChart)</span></a></li>
                <li><a href="circleadmin_bar_chart.aspx"><span>CIAT Inspection(BarChart)</span></a></li>
                <%--<li><a href="bar_chart_gold.aspx"><span>GSBB Classification</span></a></li>--%>
                <li><a href="circleadmin_bar_chart_category.aspx"><span>CIAT Checksheet point</span></a></li>
                <li><a href="circleadmin_compli_chart.aspx"><span>CIAT Site Compliance</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A"/"P" Charts</span></a>
            <ul>
                <li><a href="circleadmin_pmt_PM_Chart.aspx"><span>g-pm "P" ChecksheetWise Chart</span></a></li>
                <li><a href="circleadmin_pmt_PM_Chart_Survey.aspx"><span>g-pm "P" SiteWise Chart</span></a></li>
                <li><a href="circleadmin_pmt_PM_Chart_YesNoNA.aspx"><span>g-pm "P" QuestionWise Chart</span></a></li>
                <li><a href="circleadmin_Active_PM_Chart.aspx"><span>g-pm "A" ChecksheetWise Chart</span></a></li>
                <li><a href="circleadmin_Active_PM_Chart_Survey.aspx"><span>g-pm "A" SiteWise Chart</span></a></li>
                <li><a href="circleadmin_Active_PM_Chart_YesNoNA.aspx"><span>g-pm "A" QuestionWise Chart</span></a></li>

                <li><a href="circleadmin_Active_PM_Chart_new.aspx"><span>g-pm "A" ChecksheetWise New</span></a></li>
                <li><a href="circleadmin_Active_PM_Chart_Survey_new.aspx"><span>g-pm "A" SiteWise New</span></a></li>
                <li><a href="circleadmin_Active_PM_Chart_YesNoNA_new.aspx"><span>g-pm "A" QuestionWise New</span></a></li>
                <li><a href="circleadmin_chart_tx.aspx"><span>Txequip Pie Chart</span></a></li>
                <li><a href="circleadmin_bar_chart_tx.aspx"><span>Txequip Bar Chart</span></a></li>
                <li><a href="circleadmin_Active_PM_Chart_YesNoNA_tx.aspx"><span>Txequip QuestionWise Chart</span></a></li>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>g-pm "A"/"P" Reports</span></a>
            <ul>
                <li><a href="circleadmin_pmt_Circle_Report.aspx"><span>g-pm "P" Excel Report</span></a></li>
                <li><a href="circleadmin_pmt_Circle_Report_archive.aspx"><span>g-pm "P" Archive Report</span></a></li>
                <li><a href="circleadmin_pmt_pm_PDF_Report.aspx"><span>g-pm "P" PDF Report</span></a></li>
                <li><a href="circleadmin_pmt_TechnicianAssignReport_.aspx"><span>g-pm "P" Technician Report</span></a></li>
               
                <li><a href="circleadmin_Active_Circle_Report.aspx"><span>g-pm "A" Excel Report</span></a></li>
                <li><a href="circleadmin_Active_Circle_Report_archive.aspx"><span>g-pm "A" Archive Report</span></a></li>
                <li><a href="circleadmin_Active_pm_PDF_Report.aspx"><span>g-pm "A" PDF Report</span></a></li>
                <li><a href="circleadmin_Active_TechnicianAssignReport_.aspx"><span>g-pm "A" Technician Report</span></a></li>

                <li><a href="circleadmin_Active_Circle_Report_new.aspx"><span>g-pm "A" Excel New</span></a></li>
                <li><a href="circleadmin_Active_Circle_Report_archive_new.aspx"><span>g-pm "A" Archive New</span></a></li>
                <li><a href="circleadmin_Active_pm_PDF_Report_new.aspx"><span>g-pm "A" PDF New</span></a></li>
                <li><a href="circleadmin_Active_TechnicianAssignReport_new.aspx"><span>g-pm "A" Technician New</span></a></li>

                <li><a href="circleadmin_Punchpoint_Excel_Report_Active.aspx"><span>g-pm "A" PunchPoint Excel Report</span></a></li>
                <li><a href="circleadmin_Punch_Point_Excel_Report_TXT.aspx"><span>Tx-Equip PunchPoint Excel Report</span></a></li>
                <li><a href="circleadmin_Active_pm_PDF_Report_Checksheet_And_Passive.aspx"><span>g-pm "A"/"P" PunchPoint PDF Report</span></a></li>
                

                <li><a href="circleadmin_Circle_Report_tx.aspx"><span>Tx-Equip Excel Report</span></a></li>
                <li><a href="circleadmin_Circle_Report_archive_tx.aspx"><span>Tx-Equip Archive Report</span></a></li>
                <li><a href="circleadmin_tx_pdf_Report.aspx"><span>Tx-Equip PDF Report</span></a></li>
                <li><a href="circleadmin_tx_TechnicianAssignReport_new.aspx"><span>Tx-Equip Technician Report</span></a></li>
            </ul>
        </li>
    </ul>
</div>
