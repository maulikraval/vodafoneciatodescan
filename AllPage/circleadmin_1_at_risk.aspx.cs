﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using dataaccesslayer2;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.fonts;
using System.IO;
using mybusiness;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.Data.OleDb;

public partial class circle_admin_Circle_Report : System.Web.UI.Page
{
    HSSFWorkbook hssfworkbook;
    mydataaccess1 da1;
    mydataaccess2 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;

    //vodabal ba;
    int cat;
    string ptw;
    //int ptwid = 0;
    iTextSharp.text.Document doc;
    string ptwid;
    string ptwid1;

    DataTable dt_site;
    iTextSharp.text.Font verdana;
    Phrase p2;
    Phrase p1_mahesh;
    Chunk titleChunk;
    PdfTemplate template;
    BaseFont bf = null;
    iTextSharp.text.Image footer;
    string pdfFilePath;
    string file;
    string imag_file1;
    string imag_file2;
    string imag_file3;
    string vlogo;
    iTextSharp.text.Rectangle rec;

    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //imag_file1 = "F:\\ptw_application_final\\PTW_Application_1\\image002.png";
        //imag_file2 = "F:\\ptw_application_final\\PTW_Application_1\\image001.png";
        //vlogo = "F:\\ptw_application_final\\PTW_Application_1\\vlogo.png";
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        ImageButton2.Visible = false;
        imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image002.png";
        imag_file2 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image001.png";
        vlogo = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\vlogo.png";
        imag_file3 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\Vodafone_Production_CIAT_09042013\\image003.png";

        /*
       imag_file1 = "C:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_CIAT_07082012\\Vodafone_CIAT_07082012\\image002.png";
       imag_file2 = "C:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_CIAT_07082012\\Vodafone_CIAT_07082012\\image001.png";
       vlogo = "C:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_CIAT_07082012\\Vodafone_CIAT_07082012\\vlogo.png";
        */
        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                ImageButton1.Visible = false;

                // if (Response.IsClientConnected)
                {

                    int i = 3;
                da1 = new mydataaccess1();
                string sp_user_ = da1.select_user_cookie(Session["user"].ToString());
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString()==sp_user_)
                    {
                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                // else
                {
                    //     Response.End();
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {

            if (Session["flag"].ToString() == "4")
            {
                lnkchangeproject.Visible = true;
            }
            else
            {
                lnkchangeproject.Visible = false;
            }


            // marque Start
           int count = 0;
            int count1 = 0;
            int count2 = 0;
            da1 = new mydataaccess1();
            dt = new DataTable();
            string sp_user_d = da1.select_user_cookie(Session["user"].ToString());

          /*   da1 = new mydataaccess1();
            count = da1.ptw_dashboard_active_ptw_circle_admin(sp_user_d);

            da1 = new mydataaccess1();
            count1 = da1.ptw_dashboard_expire_ptw_circle_admin(sp_user_d);

            lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";

*/


            //Marquee End

            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    /*
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = sp_user_d;

                            circle = da.getcirclename();

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                     */
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            //da = new mydataaccess1();
                            //  sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            da1 = new mydataaccess1();
                            ba = new myvodav2();
                            ba = new myvodav2();
                            DataTable circle = new DataTable();

                            ba.User = lblusername.Text;
                            circle = da1.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            //drpcircle.Items.Insert(1, "All");
                        }
                    }

                    if (Session["role"].ToString() == "2")
                    {
                        //da = new mydataaccess1();
                        //DataTable dt = new DataTable();
                        //dt = da.viewpivotrpt();
                        //GridView1.DataSource = dt;
                        //GridView1.DataBind();



                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu;
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        Response.ClearHeaders();

        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpcircle.SelectedIndex == 0)
        {
            GridView1.Visible = false;
            GridView1.DataSource = null;
            GridView1.DataBind();
            ImageButton2.Visible = false;
        }
    }
    //protected void btnsitesearch_Click(object sender, EventArgs e)
    //{
    //da1 = new mydataaccess1();
    //dt = new DataTable();

    //// dt = da1.select_ptw_Master_report("", "where site_master.siteid='" + txtsearxh.Text + "'");
    ////dt = da1.ptw_select_approved_ptw_issuer_pdf(" and site_master.siteid='" + txtsearxh.Text + "'");
    //dt = da1.select_1_at_risk(txtsearxh.Text);
    //GridView1.DataSource = dt;
    //GridView1.DataBind();
    //GridView1.Visible = true;
    //}
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (drpcircle.SelectedIndex != 0)
        {
            da1 = new mydataaccess1();
            dt = new DataTable();
            dt = da1.select_1_at_risk(drpcircle.Text.Trim());
            if (dt.Rows.Count > 0)
            {
                GridView1.Visible = true;
                Session["grd1atRsik"] = dt;
                GridView1.DataSource = dt;
                GridView1.DataBind();
                ImageButton2.Visible = true;
            }
            else
            {
                GridView1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
                ImageButton2.Visible = false;
            }
        }
    }
    void export(System.Data.DataTable dt1)
    {

        string filename = "1AtRiskData.xls";

        int flag = 0;
        InitializeWorkbook();

        if (dt1 == null)
        {
            //Label1.Text = "All Sites Uploaded Successfully...,Wait For reports";
        }
        else
        {
            exporttoexcel(dt1, "1 At Risk");
            flag++;

        }

        if (flag != 0)
        {
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", filename));

            Response.Clear();
            Response.BinaryWrite(WriteToStream().GetBuffer());
            Response.Flush();
        }
        else
        {
            //  TabContainer1.Visible = true;
        }

    }

    void InitializeWorkbook()
    {
        hssfworkbook = new HSSFWorkbook();

        ////create a entry of DocumentSummaryInformation
        DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
        dsi.Company = "NPOI Team";
        hssfworkbook.DocumentSummaryInformation = dsi;

        ////create a entry of SummaryInformation
        SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
        si.Subject = "NPOI SDK Example";
        hssfworkbook.SummaryInformation = si;
    }

    void exporttoexcel(System.Data.DataTable dt, string name)
    {
        HSSFSheet sheet1 = hssfworkbook.CreateSheet(name);
        HSSFRow row;
        row = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            row.CreateCell(j).SetCellValue(dt.Columns[j].ColumnName);
        }
        int x = 1;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                row.CreateCell(j).SetCellValue(dt.Rows[i][j].ToString());
            }
        }
    }

    MemoryStream WriteToStream()
    {
        //Write the stream data of workbook to the root directory
        MemoryStream file = new MemoryStream();
        hssfworkbook.Write(file);
        return file;
    }
    protected void ImageButton11_Click(object sender, ImageClickEventArgs e)
    {
        System.Data.DataTable dt1 = (System.Data.DataTable)Session["grd1atRsik"];
        export(dt1);
    }
}
