﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using dataaccesslayer2;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf.fonts;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using System.IO;
using mybusiness;

public partial class PMT_pm_PDF_Report : System.Web.UI.Page
{
    mydataaccess1 da1;
    mydataaccess2 da;
    DataTable dt;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    HSSFWorkbook hssfworkbook;
    //vodabal ba;
    int cat;
    string ptw;
    //int ptwid = 0;
    iTextSharp.text.Document doc;
    string ptwid;
    string ptwid1;

    DataTable dt_site;
    iTextSharp.text.Font verdana;
    Phrase p2;
    Phrase p1_mahesh;
    Chunk titleChunk;
    PdfTemplate template;
    BaseFont bf = null;
    iTextSharp.text.Image footer;
    string pdfFilePath;
    string file1;
    string file;
    string imag_file1;
    string imag_file2;
    string imag_file3;
    string vlogo;
    string sp_user_d;
    iTextSharp.text.Rectangle rec;
    string siteid = "";
    int flag = 0;
    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //  lblError.Visible = false;
        try
        {

            string strPreviousPage = "";
            if (Request.UrlReferrer != null)
            {
                strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
            }
            if (strPreviousPage == "")
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }

            da1 = new mydataaccess1();
            sp_user_d = da1.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);


            if (!IsPostBack)
            {
                try
                {
                    //ddlCheckSheet.Visible = false;
                    //  ddlprovidername.Visible = false;
                    Label13.Visible = false;
                    ddlzone.Visible = false;
                    //Label7.Visible = false;
                    ImageButton1.Visible = false;
                    //  ImageButton2.Visible = false;
                    int i = 3;
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString()==sp_user_d)
                    {

                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "3")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da1= new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da1 = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da1.reminder_20_days_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da1.reminder_20_days_circle_admin_v2(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da1.reminder_20_days_circle_admin_v2ms(sp_user_d);
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da1 = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da1.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da1.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da1.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da1 = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da1.reminder_10_days_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da1.reminder_10_days_circle_admin_v2(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da1.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";

                    //Marquee End

                    //if (Session.Count != 0)
                    {
                        if (Session["role"].ToString() == "3")
                        {
                            if (drpcircle.SelectedIndex != 0)
                            {
                                da1 = new mydataaccess1();
                                sp_user_d = da1.select_user_cookie(Session["user"].ToString());

                                lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                                if (lblusername.Text == "")
                                {
                                    // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                                }

                                da1 = new mydataaccess1();
                                ba = new myvodav2();
                                ba = new myvodav2();
                                DataTable circle = new DataTable();

                                ba.User = sp_user_d;
                                circle = da1.getcirclenamefromusername(ba);

                                drpcircle.DataSource = circle;
                                drpcircle.DataTextField = "circle";
                                drpcircle.DataBind();
                                drpcircle.Items.Insert(0, "Select");
                               // drpcircle.Items.Insert(1, "All");

                                #region Fill Zone
                                ddlzone.Items.Clear();
                                ddlzone.DataSource = null;
                                ddlzone.DataBind();
                                ddlzone.Items.Insert(0, "Select");
                                #endregion

                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        //    String qu = SqlDataSource1.SelectCommand;
        //    qu = qu + "AND (SiteMaster.CM ='" + Session["user"].ToString() + "')";

        //    SqlDataSource1.SelectCommand = qu; 
        //    GridView1.DataBind();

        //GridView1.Columns[0].Visible = false;
        string datestyle = @"<style>.date{ mso-number-format:'Short Date';}</style>";
        foreach (GridViewRow gr in GridView1.Rows)
        {
            gr.Cells[2].Attributes.Add("class", "date");
        }
        Response.Clear();

        Response.ClearHeaders();

        Response.AppendHeader("Cache-Control", "no-cache");

        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.ms-excel";
        //	Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Response.Write(datestyle);
        GridView1.RenderControl(htmlWrite);

        Response.Write(stringWrite.ToString());

        Response.End();
        GridView1.Columns[0].Visible = true;
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        Response.Clear();

        Response.ClearHeaders();

        Response.AddHeader("content-disposition", "attachment; filename=Criticality_Report.xls");

        Response.Charset = "";

        // If you want the option to open the Excel file without saving than

        // comment out the line below

        // Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.ContentType = "application/vnd.xls";

        System.IO.StringWriter stringWrite = new System.IO.StringWriter();

        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);




        Response.Write(stringWrite.ToString());

        Response.End();

    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            myvodav2 vb = new myvodav2();
            da1 = new mydataaccess1();
            DataTable zone = new DataTable();

            if (drpcircle.SelectedIndex > 0)
            {
                
                {
                    vb.Circle = drpcircle.SelectedItem.Text.ToString();
                    zone = da1.getzonesbycirclename(vb);
                    if (zone.Rows.Count > 0)
                    {
                        ddlzone.Items.Clear();
                        ddlzone.DataSource = zone;
                        ddlzone.DataTextField = "zone";
                        ddlzone.DataBind();
                        ddlzone.Items.Insert(0, "Select");
                        ddlzone.Items.Insert(1, "All");
                        MultiView1.Visible = false;
                        Label13.Visible = true;
                        ddlzone.Visible = true;
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                    }
                    else
                    {
                        ddlzone.Items.Clear();
                        ddlzone.DataSource = null;
                        ddlzone.DataBind();
                        ddlzone.Items.Insert(0, "Select");
                        Label13.Visible = true;
                        ddlzone.Visible = true;
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                        MultiView1.Visible = false;
                    }
                }
            }
            else
            {
                ddlzone.Items.Clear();
                ddlzone.DataSource = null;
                ddlzone.DataBind();
                ddlzone.Items.Insert(0, "Select");
                MultiView1.Visible = false;
                Label13.Visible = false;
                ddlzone.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        string zone = "";
        
        {
            if (ddlzone.SelectedIndex > 0)
            {
                zone = ddlzone.SelectedItem.Text;
            }
        }

        da1 = new mydataaccess1();
        dt = new DataTable();
        dt = da1.pm_select_sites_pdf(drpcircle.SelectedValue, zone);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int srnocounter = 1;
        int stepcounter = 1;
        GridViewRow gr = GridView1.SelectedRow;
        siteid = gr.Cells[1].Text;
        //siteid = "PM_Test_6_11";
        //string sheet = gr.Cells[6].Text;


        verdana = FontFactory.GetFont("Calibri", 16, Font.NORMAL, BaseColor.BLACK);
        Font verdana_small = FontFactory.GetFont("Calibri", 11, Font.NORMAL, BaseColor.BLACK);
        Font calibri_heading = FontFactory.GetFont("Calibri", 11, Font.BOLD, BaseColor.BLACK);


        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
        //Font Site_Heading_1 = FontFactory.GetFont("Calibri", 7, Font.BOLD, BaseColor.BLACK);


        Random rnd = new Random();
        int Random = rnd.Next(1, 100);
        string cudate = Convert.ToDateTime(System.DateTime.Now).ToString("dd/MM/yyyy");
        file1 = SpacialCharRemove.SpacialChar_Remove(siteid) + "_(" + cudate.Substring(0, 10).Replace('/', '_') + ")" + Random + ".pdf";
       /* #region Check File In Directory
        //string file = @"C:\Users\HareshPatel\Desktop\ChangesPortal\AllPageInOneFolder\Copy of Vodafone_production_for_deploy\PM_PDF\" + file1;
        string file = @"F:\Vodafone\ciat_hsw\hsw_final_08_10_2014\PM_PDF\" + file1;
        if (Directory.Exists(Path.GetDirectoryName(file)))
        {
            File.Delete(file);
        }
        #endregion*/
        //file1 = siteid + "1_" + dt.Rows[0][7].ToString().Substring(0, 10).Replace('/', '_') + ".pdf";
        doc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 40f, 40f, 30f, 40f);
        // pdfFilePath = "D:\\Project\\Vodafone_PM\\New folder\\Copy of Vodafone_production_for_deploy\\Copy of Vodafone_production_for_deploy\\PM_PDF\\" + file;
        //pdfFilePath = "C:\\Users\\HareshPatel\\Desktop\\ChangesPortal\\AllPageInOneFolder\\Copy of Vodafone_production_for_deploy\\PM_PDF\\" + file1;
        //pdfFilePath = "F:\\Vodafone\\ciat_hsw\\hsw_final_08_10_2014\\PM_PDF\\" + file1;

        //pdfFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\PM_PDF\\" + file1; // 08122019        
        pdfFilePath = System.Configuration.ConfigurationManager.AppSettings["Pdf_Report_Path"].ToString() + file1;
        
        PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(pdfFilePath, FileMode.Create));
        //writer.GetVerticalPosition(true);
        doc.Open();
        #region try catch
        try
        {
            #region try
            rec = doc.PageSize;
            cic_header(doc);
            #region CheckSheet Array
            string[,] a = new string[,]
	             {
	                 {"SiteLocation", "Site Location"},
	                 {"HVAC", "Air conditioner & FCU"},
	                 {"X_mer", "Transformer"},
	                 {"EB", "Electricity Board"},
	                 {"PIU", "PIU"},
                     {"SMPS", "SMPS & other electrical"},
                     {"BB", "Battery Bank"},
                     {"DG", "Diesel Generator"},
                     {"Earthing", "Earthing"},
                     {"Shelter", "Shelter"},
                     {"THCA_PHCA", "THCA/PHCA"},
                     {"FPS", "Fall Protection System"},
                     {"FSS", "Fire Supression System"},
                     {"Alarm", "External Alarms"},
                     {"EMF", "EMF Signage"},
                     {"General", "General Maintenance"}
	             };
            #endregion
            #region Checksheet Loop
            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                int te = a.GetUpperBound(0);
                string s1type = a[i, 0]; //type
                string s2description = a[i, 1]; //Discription
                #region category
                //Column
                PdfPTable data = new PdfPTable(5);
                data.WidthPercentage = 100f;
                data.SetWidths(new float[] { 1, 3, 1, 1, 1 });
                if (i == 0)
                {
                    PdfPCell cell = new PdfPCell(new Phrase("Sr. No.", new Font(calibri_heading)));
                    cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    data.AddCell(cell);

                    PdfPCell cell1_ = new PdfPCell(new Phrase("Description", new Font(calibri_heading)));
                    cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell1_.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    data.AddCell(cell1_);

                    PdfPCell cell1_Spe = new PdfPCell(new Phrase("Specification", new Font(calibri_heading)));
                    cell1_Spe.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell1_Spe.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    data.AddCell(cell1_Spe);

                    PdfPCell cell4_ = new PdfPCell(new Phrase("Answer", new Font(calibri_heading)));
                    cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell4_.HorizontalAlignment = Element.ALIGN_CENTER;
                    //cell4_.Width = 20;
                    data.AddCell(cell4_);


                    PdfPCell cell5_ = new PdfPCell(new Phrase("Comments / Explanations", new Font(calibri_heading)));
                    cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell5_.HorizontalAlignment = Element.ALIGN_CENTER;
                    //cell5_.Width = 20;
                    data.AddCell(cell5_);
                }
                //
                //Count 
                #region Fine Site Count
                da1 = new mydataaccess1();
                DataTable pm_findsitecount = new DataTable();
                pm_findsitecount = da1.pm_findcount(siteid, s1type.ToString());
                string AllCount = "";
                if (pm_findsitecount.Rows.Count > 0)
                {
                    AllCount = " Date Of PM : " + pm_findsitecount.Rows[0][2].ToString() + " " + pm_findsitecount.Rows[0][1].ToString();
                    AllCount += " Done By : " + pm_findsitecount.Rows[0][3].ToString();
                    AllCount += " Contact Number : " + pm_findsitecount.Rows[0][4].ToString();
                    AllCount += " Next Due Date : " + pm_findsitecount.Rows[0][5].ToString();
                    AllCount += " Assigned To : " + pm_findsitecount.Rows[0][6].ToString();
                }
                #endregion
                #region Check Checksheet Com,NonCom,PartialCom
                string CheckStatus = "";
                da1 = new mydataaccess1();
                DataTable DTCheckStatus = new DataTable();
                DTCheckStatus = da1.pm_checkStatus(siteid, s1type.ToString());
                if (DTCheckStatus.Rows.Count > 0)
                {
                    CheckStatus = DTCheckStatus.Rows[0][2].ToString();
                }
                #endregion
                if (s1type.ToString() == "SiteLocation")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    data.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(s2description.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    data.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase("", new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    data.AddCell(cell1_Specount);
                }
                else
                {

                    if (CheckStatus == "Completed")
                    {
                        PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                        cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell.Width = 20;
                        data.AddCell(cellcount);

                        PdfPCell cell1_count = new PdfPCell(new Phrase(s2description.ToString(), new Font(Site_Heading)));
                        cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_count);

                        PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                        cell1_Specount.Colspan = 3;
                        cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_Specount);
                    }
                    if (CheckStatus == "NonComplete" || CheckStatus == "")
                    {
                        PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                        cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell.Width = 20;
                        data.AddCell(cellcount);

                        PdfPCell cell1_count = new PdfPCell(new Phrase(s2description.ToString(), new Font(Site_Heading)));
                        cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_count);

                        PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                        cell1_Specount.Colspan = 3;
                        cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_Specount);
                    }
                    if (CheckStatus == "PartialCompleted")
                    {
                        PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                        cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell.Width = 20;
                        data.AddCell(cellcount);

                        PdfPCell cell1_count = new PdfPCell(new Phrase(s2description.ToString(), new Font(Site_Heading)));
                        cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_count);

                        PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                        cell1_Specount.Colspan = 3;
                        cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                        // cell1_.Width = 40;
                        data.AddCell(cell1_Specount);
                    }
                }
                #region Step
                //    for (int j = 0; j < pm_getallchecksheet.Rows.Count; j++)
                {
                    flag = 0;
                    da1 = new mydataaccess1();
                    DataTable dt1 = new DataTable();
                    dt1 = da1.pm_select_site_que_log_pdf(s1type.ToString(), siteid);
                    //ha
                    //dt1 = da.pm_select_site_que_log_pdf(sheet, siteid, i_counter, j + 1);
                    //

                    int sec = 0;
                    // stepcounter = stepcounter + 1;
                    if (dt1.Rows.Count > 0)
                    {
                        int counter_t = 0;
                        string Kye_Count = "";

                        for (int k = 0; k < dt1.Rows.Count; k++)
                        {
                            if (s1type == "X_mer" || s1type == "PIU" || s1type == "SMPS" || s1type == "BB" || s1type == "DG"
                                || s1type == "HVAC" || s1type == "Earthing" || s1type == "Shelter" || s1type == "FPS")
                            {

                                if (Kye_Count != dt1.Rows[k][3].ToString())
                                {
                                    counter_t = 0;
                                }

                                Kye_Count = dt1.Rows[k][3].ToString();
                                // flagCheck = "True";
                                if (Kye_Count == "1" || Kye_Count == "2" || Kye_Count == "3" || Kye_Count == "4"
                                || Kye_Count == "5" || Kye_Count == "7" || Kye_Count == "8" || Kye_Count == "9"
                                || Kye_Count == "10" || Kye_Count == "11" || Kye_Count == "12" || Kye_Count == "13"
                                || Kye_Count == "14" || Kye_Count == "15" || Kye_Count == "16")
                                {
                                    if (counter_t == 0)
                                    {
                                        PdfPCell cell00_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                        cell00_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                        cell00_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                        // cell.Width = 20;
                                        data.AddCell(cell00_key);
                                        PdfPCell cell100_key = new PdfPCell(new Phrase(s1type.ToString() + "(" + Kye_Count + ")", new Font(calibri_Data)));
                                        cell100_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                        cell100_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell100_key);
                                        PdfPCell cell100_spe_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                        cell100_spe_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell100_spe_key);
                                        PdfPCell cell400_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                        cell400_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                        //cell4_.Width = 20;
                                        data.AddCell(cell400_key);
                                        PdfPCell cell500_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                        cell500_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                        //cell5_.Width = 20;
                                        //    cell500_.Rowspan = dt1.Rows.Count;
                                        cell500_key.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        cell500_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                        data.AddCell(cell500_key);
                                        counter_t = counter_t + 1;
                                    }
                                }
                            }

                            PdfPCell cell00 = new PdfPCell(new Phrase((srnocounter + "." + stepcounter).ToString(), new Font(calibri_Data)));
                            cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                            cell00.HorizontalAlignment = Element.ALIGN_CENTER;
                            // cell.Width = 20;
                            data.AddCell(cell00);
                            PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(calibri_Data)));
                            cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                            // cell1_.Width = 40;
                            data.AddCell(cell100_);
                            PdfPCell cell100_spe = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(calibri_Data)));
                            cell100_spe.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                            // cell1_.Width = 40;
                            data.AddCell(cell100_spe);

                            if (dt1.Rows[k][1].ToString().ToUpper() == "YES")
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }
                            else if (dt1.Rows[k][1].ToString().ToUpper() == "NO")
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }
                            else if (dt1.Rows[k][1].ToString().ToUpper() == "NA")
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }
                            else
                            {
                                PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                                cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                //cell4_.Width = 20;
                                data.AddCell(cell400_);
                            }

                            PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][5].ToString(), new Font(calibri_Data)));
                            cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                            //cell5_.Width = 20;
                            //    cell500_.Rowspan = dt1.Rows.Count;
                            cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                            cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                            data.AddCell(cell500_);
                            // flag++;
                            stepcounter = stepcounter + 1;
                        }

                        /*else
                        {
                            if (j == 0)
                            {
                                da = new mydataaccess2();
                                DataTable dt2 = new DataTable();
                                dt2 = da.pm_GetDefaultQuestion(pm_getallchecksheet.Rows[get]["type"].ToString());
                                if (dt2.Rows.Count > 0)
                                {
                                    for (int k = 0; k < dt2.Rows.Count; k++)
                                    {
                                        PdfPCell cell00_check = new PdfPCell(new Phrase((srnocounter + "." + stepcounter).ToString(), new Font(calibri_Data)));
                                        cell00_check.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                        cell00_check.HorizontalAlignment = Element.ALIGN_CENTER;
                                        // cell.Width = 20;
                                        data.AddCell(cell00_check);
                                        PdfPCell cell100_check = new PdfPCell(new Phrase(dt2.Rows[k][0].ToString(), new Font(calibri_Data)));
                                        cell100_check.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell100_check);
                                        PdfPCell cell100_specheck = new PdfPCell(new Phrase(dt2.Rows[k][1].ToString(), new Font(calibri_Data)));
                                        cell100_specheck.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                        // cell1_.Width = 40;
                                        data.AddCell(cell100_specheck);

                                        PdfPCell cell400_check = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                        cell400_check.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                        //cell4_.Width = 20;
                                        data.AddCell(cell400_check);


                                        PdfPCell cell500_check = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                        cell500_check.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                                        //cell5_.Width = 20;
                                        //    cell500_.Rowspan = dt1.Rows.Count;
                                        cell500_check.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        cell500_check.HorizontalAlignment = Element.ALIGN_CENTER;
                                        data.AddCell(cell500_check);
                                        // flag++;
                                        stepcounter = stepcounter + 1;
                                    }
                                }
                            }
                        }*/
                    }
                #endregion

                    doc.Add(data);
                #endregion
                    srnocounter = srnocounter + 1;
                    stepcounter = 1;
                }
            }
            #endregion

            /////////////////////////////////

            doc.NewPage();
            for (int j = 0; j <= a.GetUpperBound(0); j++)
            {
                string s1type1 = a[j, 0]; //type
                string s2description1 = a[j, 1]; //Discription
                int vsitemasterinsp = 0;
                da1 = new mydataaccess1();
                dt = new DataTable();
                dt = da1.pm_GetinspectionCounter(siteid, s1type1.ToString());
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "")
                    {
                        vsitemasterinsp = Convert.ToInt32(dt.Rows[0][0].ToString()) - 1;
                    }
                }
                if (vsitemasterinsp >= 0)
                {
                    #region temp
                    da1 = new mydataaccess1();

                    dt = da1.pm_select_categories_by_sheet(s1type1.ToString());
                    for (int m = 0; m < dt.Rows.Count; m++)
                    {
                        da1 = new mydataaccess1();
                        DataTable dt_photo = new DataTable();

                        dt_photo = da1.pm_select_photos_pdf(siteid, vsitemasterinsp, (m+1), s1type1.ToString());
                        if (dt_photo.Rows.Count > 0)
                        {
                            if (m == 0)
                            {
                                PdfPTable new_t1 = new PdfPTable(1);

                                new_t1.WidthPercentage = 100f;

                                Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                                 Font.BOLD));

                                PdfPCell new_cell_ = new PdfPCell(head_ph_);
                                new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                                new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                                new_cell_.FixedHeight = 20f;
                                new_t1.AddCell(new_cell_);
                                Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                                ));

                                doc.Add(new_t1);
                                doc.Add(break2);
                            }
                            if (dt_photo.Rows.Count > 0)
                            {
                                PdfPTable new_t = new PdfPTable(1);
                                new_t.WidthPercentage = 100f;

                                Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                                 Font.BOLD));

                                PdfPCell new_cell = new PdfPCell(head_ph);
                                new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                                new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                new_cell.FixedHeight = 12f;
                                new_t.AddCell(new_cell);
                                doc.Add(new_t);

                                PdfPTable photo = new PdfPTable(4);
                                PdfPCell photo_cell = new PdfPCell();
                                photo.WidthPercentage = 100;
                                int flag = 0;
                                for (int p = 0; p < dt_photo.Rows.Count; p++)
                                {
                                    try
                                    {
                                        if (dt_photo.Rows.Count % 4 == 0)
                                        {
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            //imag_file1 = "E:\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                           // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);
                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            //  photo_cell.FixedHeight = 220;
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                        }
                                        else
                                        {
                                            if (p == dt_photo.Rows.Count - 1)
                                            {
                                                PdfPTable pp = new PdfPTable(1);
                                                Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                                PdfPCell ppc = new PdfPCell(name);
                                                ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                                pp.AddCell(ppc);
                                               // imag_file1 = "E:\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                iTextSharp.text.Image image_photo;
                                                image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                                image_photo.SetAbsolutePosition(50, 50);
                                                image_photo.ScaleAbsolute(70f, 70f);
                                                pp.AddCell(image_photo);
                                                photo_cell = new PdfPCell(pp);
                                                photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                                // photo_cell.FixedHeight = 220;
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                                photo.AddCell(photo_cell);

                                                photo_cell = new PdfPCell();
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                photo.AddCell(photo_cell);


                                                for (int b = 0; b < 4 - flag; b++)
                                                {
                                                    photo_cell = new PdfPCell();
                                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                    photo.AddCell(photo_cell);
                                                }


                                            }
                                            else
                                            {
                                                // imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                PdfPTable pp = new PdfPTable(1);
                                                Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                                PdfPCell ppc = new PdfPCell(name);
                                                ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                                pp.AddCell(ppc);
                                                //imag_file1 = "E:\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                //imag_file1 = "F:\\Vodafone\\vfcode_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                                iTextSharp.text.Image image_photo;
                                                image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                                image_photo.SetAbsolutePosition(50, 50);
                                                image_photo.ScaleAbsolute(70f, 70f);
                                                pp.AddCell(image_photo);
                                                photo_cell = new PdfPCell(pp);
                                                photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                                photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                                //  photo_cell.FixedHeight = 220;
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                photo.AddCell(photo_cell);
                                                flag++;
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                    }
                                }
                                doc.Add(photo);
                                Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                                doc.Add(break23);
                            }
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }
        //doc.Close();
        catch (Exception ex)
        { }
        finally
        {
            doc.Close();
        }
        #endregion

        string strUserAgent = Request.UserAgent.ToString().ToLower();
        if (Request.Browser.IsMobileDevice == true || strUserAgent.Contains("iphone") ||
strUserAgent.Contains("blackberry") || strUserAgent.Contains("mobile") ||
strUserAgent.Contains("windows ce") || strUserAgent.Contains("opera mini") ||
strUserAgent.Contains("palm"))
        {
            Response.ContentType = "Application/pdf";

            Response.AppendHeader("Content-Disposition", "attachment; filename=" + file1);

            Response.TransmitFile(Server.MapPath("~/pdf_files/" + SpacialCharRemove.SpacialChar_Remove(ptwid1) + ".pdf"));

            Response.End();
            //           Response.Redirect("https://ciat.vodafone.in/report_ptw/height_pdf/" + file);
        }
        else
        {
            if (Session["imei"] == null)
            {
                Response.ContentType = "Application/pdf";

                Response.AppendHeader("Content-Disposition", "attachment; filename=" + file1);
               // Response.TransmitFile("C:/Users/HareshPatel/Desktop/ChangesPortal/AllPageInOneFolder/Copy of Vodafone_production_for_deploy/PM_PDF/" + file1);
                //Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_08_10_2014/PM_PDF/" + file1);
                Response.TransmitFile("D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/PM_PDF/" + file1);
                //Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_18_08_2014/PM_PDF/" + file1);
                // Response.TransmitFile("E:/Haresh/Project/Vodafone_PM/Copy of Vodafone_production_for_deploy/Copy of Vodafone_production_for_deploy/PM_PDF/" + file);
                Response.End();
            }
            else
            {
                Response.ContentType = "Application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + file1);
                //Response.TransmitFile("C:/Users/HareshPatel/Desktop/ChangesPortal/AllPageInOneFolder/Copy of Vodafone_production_for_deploy/PM_PDF/" + file1);
                //Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_08_10_2014/PM_PDF/" + file1);
                Response.TransmitFile("D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/PM_PDF/" + file1);
                // Response.TransmitFile("F:/Vodafone/ciat_hsw/hsw_final_18_08_2014/PM_PDF/" + file1);
                Response.End();
                //Response.Redirect("https://ciat.vodafone.in/report_ptw/height_pdf/" + file);
            }
        }

    }
    private void cic_header(Document doc)
    {
        Font calibri_Display = FontFactory.GetFont("Calibri", 10, Font.NORMAL, BaseColor.BLACK);
        Font calibri_Heading = FontFactory.GetFont("Calibri", 15, Font.BOLD, BaseColor.BLACK);

        PdfPTable head = new PdfPTable(1);
        head.WidthPercentage = 100f;

        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });

        //head.DefaultCell.Border = 0;
        string headersheet = "";
        string address = "";

        {
            headersheet = "Preventive Maintenance Checklist V1";
            address = "Address: ";
        }

        Phrase p1header = new Phrase(headersheet, new Font(calibri_Heading));
        PdfPCell c = new PdfPCell(p1header);
        c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
        c.HorizontalAlignment = Element.ALIGN_CENTER;
        c.VerticalAlignment = Element.ALIGN_MIDDLE;
        c.BorderColor = iTextSharp.text.BaseColor.BLACK;
        c.FixedHeight = 20f;
        head.AddCell(c);


        // head.SetWidths(new float[] { 1, 4, 5, 4, 1 });


        PdfPTable table1 = new PdfPTable(2);
        table1.WidthPercentage = 100f;
        //table1.SetWidths(new float[] { 4, 3, 4, 3 });

        da1 = new mydataaccess1();
        dt = new DataTable();
        dt = da1.pm_select_site_data_all_pdf(siteid);


        Phrase p1_b2 = new Phrase("Circle : " + dt.Rows[0][1].ToString(), new Font(calibri_Display));
        PdfPCell cell2 = new PdfPCell(p1_b2);
        cell2.HorizontalAlignment = Element.ALIGN_LEFT;
        cell2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase p1_b3 = new Phrase("Site ID : " + siteid, new Font(calibri_Display));
        PdfPCell cell3 = new PdfPCell(p1_b3);
        cell3.HorizontalAlignment = Element.ALIGN_LEFT;
        cell3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase p1_b4 = new Phrase(address + dt.Rows[0][0].ToString(), new Font(calibri_Display));
        PdfPCell cell4 = new PdfPCell(p1_b4);
        cell4.HorizontalAlignment = Element.ALIGN_LEFT;
        cell4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
        // cell1.BackgroundColor.Brighter(GDI.Color.);

        Phrase p1_b5 = new Phrase("Site Incharge Name & No. : " + dt.Rows[0][2].ToString(), new Font(calibri_Display));
        PdfPCell cell5 = new PdfPCell(p1_b5);
        cell5.HorizontalAlignment = Element.ALIGN_LEFT;
        cell5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        //ha
        /* Phrase p1_b3 = new Phrase("Location of Control room & Contact No. : " + dt.Rows[0][3].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
         PdfPCell cell4 = new PdfPCell(p1_b3);
         cell4.HorizontalAlignment = Element.ALIGN_LEFT;
         cell4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

         Phrase p1_b14 = new Phrase("Date of Inspection:- " + dt.Rows[0][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
         PdfPCell cell5 = new PdfPCell(p1_b14);
         cell5.HorizontalAlignment = Element.ALIGN_LEFT;
         cell5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

         Phrase p1_b15 = new Phrase("Lattitude & Longitude :" + dt.Rows[0][5].ToString(), new Font(Font.FontFamily.HELVETICA, 6));
         PdfPCell cell6 = new PdfPCell(p1_b15);
         cell6.HorizontalAlignment = Element.ALIGN_LEFT;
         cell6.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
         */
        //

        table1.AddCell(cell2);
        table1.AddCell(cell3);
        table1.AddCell(cell4);
        table1.AddCell(cell5);

        //ha
        /* table1.AddCell(cell4);
         table1.AddCell(cell5);
         table1.AddCell(cell6);
         */
        //

        //PdfPCell note = new PdfPCell(new Phrase("Notes : Every positive answer gets full marks for the question and every negative answer would get Zero marks. Mark 'NA' if the question is Not Applicable for any particular site. Comment / explanation should be given for where a corrective action is requried. Weightage for different critical safety requirement is 2, 1 and 0.5. In case any activity description is not applicable for the Cellsite, then the points would be automatically deducted from the total score to calculate the achieved percentage. Every observation made on the site should be classified as A, B or C (A being most critical) depending on the criticality / hazard Potential (for more details refer procedure).The bold text questions imply basis of rejection at the time site acceptance from Infra Provider.", new Font(Font.FontFamily.TIMES_ROMAN, 7)));
        //note.Colspan = 2;
        //note.FixedHeight = 60f;
        //note.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#04B4AE"));
        //note.HorizontalAlignment = Element.ALIGN_CENTER;
        //note.VerticalAlignment = Element.ALIGN_MIDDLE;
        //table1.AddCell(note);

        PdfPCell blank = new PdfPCell(new Phrase(""));
        blank.Colspan = 2;
        blank.FixedHeight = 15f;
        table1.AddCell(blank);

        doc.Add(head);
        doc.Add(table1);
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlzone.SelectedIndex > 0)
            {
                MultiView1.ActiveViewIndex = 0;
                MultiView1.Visible = true;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
            else
            {
                MultiView1.Visible = false;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnsitesearch_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            dt = new DataTable();
            dt = da1.pm_select_sites_pdf_Search(txtsearxh.Text);
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }
        catch
        {
        }
    }
}