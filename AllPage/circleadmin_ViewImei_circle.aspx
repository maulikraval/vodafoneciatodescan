﻿<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="circleadmin_ViewImei_circle.aspx.cs" Inherits="admin_ViewImei" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <table width="100%">
        
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td width="90%" align="right" style="width: 60%; text-align: center">
                    <asp:Label ID="Label2" runat="server" Font-Bold="False" Font-Underline="False" Text="View IMEI"
                        CssClass="lblstly"></asp:Label>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td width="90%" align="right" style="width: 60%; text-align: center">
                    <hr />
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td width="90%" align="right" style="width: 60%; text-align: center">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td width="90%" align="right" style="width: 60%; text-align: center">
                    <asp:Panel ID="panl" runat="server" Width="950px" ScrollBars="Vertical">
                        <asp:GridView ID="grd1" runat="server"  Width="100%" ForeColor="Black" 
                            BorderStyle="Solid" BackColor="WhiteSmoke" BorderColor="#999999"
                            onselectedindexchanged="grd1_SelectedIndexChanged" 
                            onrowdatabound="grd1_RowDataBound1">
                          <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:CommandField HeaderText="Edit" ShowSelectButton="True" SelectText="Edit" />
                            </Columns>
                        </asp:GridView>
                       <%-- <asp:GridView ID="grd1" runat="server" OnSelectedIndexChanged="grd1_SelectedIndexChanged"
                            OnRowDeleted="grd1_RowDeleted" Width="100%" OnRowDataBound="grd1_RowDataBound" ForeColor="Black" BorderStyle="Solid" BackColor="#CCCCCC" BorderColor="#999999">
                            <FooterStyle BackColor="#CCCCCC" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                            <Columns>
                                <asp:CommandField HeaderText="Edit" ShowSelectButton="True" SelectText="Edit" />
                            </Columns>
                        </asp:GridView>--%>
                    </asp:Panel>
                </td>
                <td width="5%">
                </td>
            </tr>
    </table>
    <cc1:RoundedCornersExtender ID="RoundedCornersExtender1" Radius="20" BorderColor="Black"
                    TargetControlID="panel2" runat="server">
                </cc1:RoundedCornersExtender>
    <asp:Panel ID="panel2" runat="server"  Width="850px" Height="150px"
        ForeColor="Black" BackColor="#E4E4E4">
        <table width="100%" style="text-align: center">
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td width="15%" style="text-align: center" colspan="4">
                    <asp:Label ID="Label3" runat="server" CssClass="lblstly" Font-Bold="False" 
                        Font-Underline="False" Text="View IMEI"></asp:Label>
                </td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;</td>
                <td colspan="4" style="text-align: right; width: 45%;" width="15%">
                    <hr />
                </td>
                <td width="5%">
                    &nbsp;</td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="15%">
                    Imei No:
                </td>
                <td style="text-align: left" width="30%">
                    <asp:TextBox ID="txt1" runat="server" Width="170px" Enabled="False"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="txt1" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                        TargetControlID="txt1" ValidChars="0123456789">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td style="text-align: right; width: 15%;">
                    Mobile Model:
                </td>
                <td style="text-align: left" width="30%">
                    <asp:TextBox ID="txt2" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="txt2" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" 
                        FilterMode="InvalidChars" InvalidChars="./*+@#$%^&amp;*~!&lt;&gt;:&quot;?|='()" 
                        TargetControlID="txt2">
                    </cc1:FilteredTextBoxExtender>
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    &nbsp;
                </td>
                <td style="text-align: left" width="35%">
                    &nbsp;</td>
                <td style="text-align: left; width: 11%;">
                    &nbsp;
                </td>
                <td style="text-align: left" width="35%">
                    &nbsp;
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td width="5%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="10%">
                    &nbsp;
                </td>
                <td style="text-align: right" width="35%">
                    <asp:Button ID="btnupdate" runat="server" OnClick="btnupdate_Click" 
                        Text="Update" />
                </td>
                <td style="text-align: left; width: 11%;">
                    <asp:Button ID="btncancle0" runat="server" OnClick="btncancle_Click" 
                        Text="Cancel" />
                </td>
                <td style="text-align: left" width="35%">
                    &nbsp;
                </td>
                <td width="5%">
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:LinkButton ID="butt1" runat="server" Width="1px"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="mod1" runat="server" BackgroundCssClass="modalBackground"
        TargetControlID="butt1" PopupControlID="panel2">
    </cc1:ModalPopupExtender>
</asp:Content>
