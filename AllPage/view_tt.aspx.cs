﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using FindDataAccessLayer;
using mybusiness;
using System.Data;
using System.Web.Security;
using dataaccesslayer;
using dataaccesslayer2;
using System.Net.Mail;
using System.Net;
public partial class AllPage_admin_addcircle : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
	 FindDataAccess DA;
 

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
			// da = new mydataaccess1();
            //    string sp_user_ = da.select_user_cookie(Session["user"].ToString());
               
                  //  lblmsg.Visible = false;
                   Fill_grduser();
               
            }
        }
        catch (Exception ex)
        {
	//	throw ex;
          Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
	 protected void Fill_grduser(){
					 da = new mydataaccess1();
                  
                    dt = new DataTable();
                    dt = da.view_call_tt();
                    grduser.DataSource = dt;
                    grduser.DataBind();
	 }
	 protected void lnbtn_Approve_Click(object sender, EventArgs e)
    {
        Button lnkbtn = sender as Button;
        //getting particular row linkbutton
        GridViewRow gvrow = lnkbtn.NamingContainer as GridViewRow;
        //getting userid of particular row
       int Id = Convert.ToInt32(grduser.DataKeys[gvrow.RowIndex].Value.ToString());
        DropDownList DDL = (DropDownList)grduser.Rows[gvrow.RowIndex].FindControl("drp_Closeby");
        Label Project = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_project");
		TextBox remark = (TextBox)grduser.Rows[gvrow.RowIndex].FindControl("txt_remark");
       
        Label Username = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_Username");
        Label Siteid = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_siteid");
        Label OTime = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_Requestedtime");
        Label Circle = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_circle");
        Label MCompany = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_company");
       Label Mobile = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_mobile");
	    Label SCompany = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_subcompany");
        Label issue = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_issue");
		 Label desc = (Label)grduser.Rows[gvrow.RowIndex].FindControl("lbl_desc");
        

        if (DDL.SelectedValue.ToString() != "0")
        {
            string CloseBy = DDL.SelectedItem.Text;
           
            DA = new FindDataAccess();
            DataTable DC = new DataTable();
            DC = DA.CLOSE_TT(Id, CloseBy, "Closed By Support",remark.Text);
           
                   Send_Email_To_User("Approve", Project.Text, Username.Text, Siteid.Text, OTime.Text, DateTime.Now.ToString(), Circle.Text, Mobile.Text, MCompany.Text, SCompany.Text,remark.Text,desc.Text,issue.Text);
				 ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Submitted.')", true);
               
            
           Fill_grduser();
        }
        else
        {

        }
    }


	 protected void Send_Email_To_User(string Action_Status, string ProjectName, string Username, string SiteId,  string OTime, string CTime, string Circle, string Mobile, string MCompnay, string SCompany,String Remark,String Desc,String issue)
    {
        try
        {
            mydataaccess2 da2 = new mydataaccess2();
            string to_email = "";
            to_email = da2.get_email_id_user(Username);

            if (to_email != "" && to_email != null)
            {
                string subject = "Your Request Closed by Helpdesk ";
                string body = Email_Body(Action_Status, ProjectName, Username, SiteId, OTime, CTime, Circle, Mobile, to_email, MCompnay, SCompany, Remark, Desc, issue);
                SendMail sm = new SendMail();
                sm.mailsend(to_email, subject, body);


           //     // Vodafone SMTP Credentials
           //     string Body = Email_Body(Action_Status, ProjectName, Username, SiteId,  OTime, CTime, Circle, Mobile, to_email, MCompnay, SCompany,Remark,Desc,issue);

           //     MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", to_email);
              
           //         message.Subject = "Your Request Closed by Helpdesk ";
              
           //     message.Body = Body;
           ////     message.CC.Add("latlong.helpdesk@gmail.com");
           //     message.IsBodyHtml = true;

           //     #region "VODAFONE SERVER CREDENTIALS"

           //	    SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);
           //         emailClient.Credentials = new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");
           //         emailClient.EnableSsl = false;

           //     #endregion

           //     #region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

           //  /*   SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
           //     emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
           //     emailClient.EnableSsl = true;*/

           //     #endregion

           //     emailClient.Send(message);
            }
        }
        catch (Exception ex)
        {

        }
    }
    public string Email_Body(string Action_Status, string ProjectName, string Username, string SiteId, string OTime, string CTime, string Circle, string Mobile, string Email, string MCompnay, string SCompany,String Remark,String Desc,String issue)
    {
        string Body = "";

        Body = "Dear " + SendMail.ishtml(Username) + "<br /><br />";

            Body += "Your Request has been Closed for category "+ SendMail.ishtml(issue) +" <br /><br />";

            Body += "<table border='1'>";
            Body += "<tr style='background-color:red;color:white'>";
            Body += "<th>ProjectName</th><th>Site Id</th>  <th>Requested time by user</th> <th>Close Time By Support Team</th> ";
            Body += "</tr>";
            Body += "<tr>";
            Body += "<td>" + SendMail.ishtml(ProjectName) + "</td><td>" + SendMail.ishtml(SiteId) + "</td> <td>" + SendMail.ishtml(OTime) + "</td> <td>" + SendMail.ishtml(CTime) + "</td>";
            Body += "</tr>";
            Body += "</table><br />";
       

        Body += "<table border='1'>";
        Body += "<tr style='background-color:red;color:white'>";
        Body += "<th>Requested by username</th> <th>Circle</th> <th>Mobile number of user</th> <th>EmailId of user</th> <th>Main company</th> <th>Sub Company</th>";
        Body += "</tr>";
        Body += "<tr>";
        Body += "<td>" + SendMail.ishtml(Username) + "</td> <td>" + SendMail.ishtml(Circle) + "</td> <td>" + SendMail.ishtml(Mobile) + "</td> <td>" + SendMail.ishtml(Email) + "</td> <td>" + SendMail.ishtml(MCompnay) + "</td> <td>" + SendMail.ishtml(SCompany) + "</td>";
        Body += "</tr>";
		Body += "</table><br />";
		Body += "<table border='1'>";
		Body += "<tr>";
        Body += "<td>Technician Remark : </td> <td>" + SendMail.ishtml(Desc) + "</td> ";
        Body += "</tr>";
		Body += "<tr>";
        Body += "<td>CIAT HelpDesk Remark :</td> <td>" + SendMail.ishtml(Remark) + "</td> ";
        Body += "</tr>";
        Body += "</table><br />";

        Body += "Please Note :- Do not Reply on this mail if you have any query then ,please contact as below.<br /><br />";
        Body += "Contact Us :-<br />";
        Body += "Helpdesk Number :- 9909924585 , 8141587709 , 9978791606, 9727716004<br />";
        Body += "Email :- ciat.helpdesk@skyproductivity.com<br />";

        return Body;
    }
 
}
