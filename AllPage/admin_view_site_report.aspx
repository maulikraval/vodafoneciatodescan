﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_view_site_report.aspx.cs" Inherits="admin_view_site_report" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%">
        <tr>
            <td style="width: 25%">
            </td>
            <td style="width: 25%; text-align: right">
            </td>
            <td style="width: 25%; text-align: left">
            </td>
            <td style="width: 25%">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Panel ID="panel1" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                    <asp:GridView ID="grd20" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                        BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No reports found "
                        ForeColor="Black" Width="100%" OnRowCommand="grdcalctenant0_RowCommand">
                        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle Font-Names="Calibri" Font-Size="13px" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="View">
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%# Eval("Report") %>'
                                        CommandName="goto" Text="Click Here To Open Report" ToolTip='<%# Bind("Report") %>'
                                        CausesValidation="False"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="width: 25%">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: right">
                &nbsp;
            </td>
            <td style="width: 25%; text-align: left">
                &nbsp;
            </td>
            <td style="width: 25%">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
