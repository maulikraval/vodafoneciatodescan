﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using business;
using dataaccesslayer;
using mybusiness;
using System.Collections.Generic;
public partial class admin_Imei : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["flag"] == "1")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "2")
        {
            this.MasterPageFile = "~/AllPage/AllCommon.master";
        }
        if (Session["flag"] == "4")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }

            //this.MasterPageFile = "~/circle_admin/NewCircle.master";
        }
        if (Session["flag"] == "3")
        {
            if (Session["project"].ToString() == "ciat")
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
            else
            {
                this.MasterPageFile = "~/AllPage/AllCommon.master";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["role"].ToString() != "3")
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.End();
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }


    }
    private bool CompareArray(byte[] a1, byte[] a2)
    {

        if (a1.Length != a2.Length)

            return false;



        for (int i = 0; i < a1.Length; i++)
        {

            if (a1[i] != a2[i])

                return false;

        }



        return true;

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
            imageHeader.Add("XLS", new byte[] { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 });

            // bool acceptFile = false;
            // bool acceptFile1 = false;
            byte[] header;
            string fileExt;

            fileExt = FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf('.') + 1).ToUpper();
            string acceptedFileTypes = ".XLS";



            byte[] tmp = imageHeader[fileExt];

            header = new byte[tmp.Length];



            // GET HEADER INFORMATION OF UPLOADED FILE

            FileUpload1.FileContent.Read(header, 0, header.Length);



            if (CompareArray(tmp, header))
            {

                try
                {
                    da = new mydataaccess1();
                    ba = new myvodav2();

                    string abc = FileUpload1.FileName;
                    abc = SpacialCharRemove.SpacialChar_Remove(abc);
                    if (abc == "")
                    {
                        lblresult.Text = "Select file first.";
                        lblresult.Visible = true;
                    }
                    else
                    {
                        FileUpload1.SaveAs(Server.MapPath("~/SampleData/sitedata/") + abc);
                        string path = Server.MapPath("~/SampleData/sitedata/") + abc;
                  //      string excelConnectionString = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                        string excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";
                        string constring = excelConnectionString;
                        int result;
                        string sp_user_d = "";

                        da = new mydataaccess1();
                        dt = new DataTable();
                        sp_user_d = da.select_user_cookie(Session["user"].ToString());


                        result = Convert.ToInt32(da.insertdataimeino(constring, sp_user_d));
                        if (result == 1)
                        {
                            lblresult.Visible = true;
                            lblresult.Text = "Problem in data.Upload Proper Data";

                        }
                        else
                        {
                            lblresult.Visible = true;
                            lblresult.Text = "Imei Number Successfully Uploaded.";
                        }
                    }

                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx", false);
                }
            }
        }
        catch
        {
            lblresult.Visible = true;
            lblresult.Text = "Upload Wrong file,Upload .xls File.";

        }
    }
    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
}



