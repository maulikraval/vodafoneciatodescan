<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="admin_20daysreminder_ptw_night.aspx.cs" Inherits="AllPage_admin_20daysreminder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 100%">
        <tr>
            <td style="width: 100%; text-align: center;">
              <asp:Label ID="Label1" runat="server" Font-Names="Calibri" Font-Size="20px" ForeColor="#FFA500"
                    Text="Dashboard"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; text-align: left;">
            <cc1:TabContainer ID="TabContainer1" runat="server" Width="100%" 
                    Font-Names="calibri" Font-Size="13px" ActiveTabIndex="0">
                    <cc1:TabPanel ID="tpanel1" runat="server" HeaderText="Active PTW">
                        <ContentTemplate>
                            <asp:Panel ID="panel1" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grd20" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No PTW found..!"
                                    ForeColor="Black" Width="100%" AllowSorting="True" OnSorting="grd20_Sorting">
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Extended Approved PTW ">
                        <ContentTemplate>
                            <asp:Panel ID="panel2" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="grd10" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No PTW found..!"
                                    ForeColor="Black" Width="100%" AllowSorting="True" OnSorting="grd10_Sorting">
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    
                    <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Pending PTW">
                        <ContentTemplate>
                            <asp:Panel ID="panel4" runat="server" ScrollBars="Auto" Width="1020px" Height="450px">
                                <asp:GridView ID="GridView1" runat="server" BackColor="WhiteSmoke" BorderColor="#FFA500"
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="4" CellSpacing="2" EmptyDataText="No PTW found..!"
                                    ForeColor="Black" Width="100%" AllowSorting="True" OnSorting="grd20_Sorting">
                                    <RowStyle Font-Names="Calibri" Font-Size="13px" />
                                    <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                                    <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                                    <FooterStyle BackColor="#CCCCCC" />
                                    <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                            </asp:Panel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    
                    
                </cc1:TabContainer>
            </td>
        </tr>
        
        <tr>
            <td style="width: 100%; text-align: center;">
                    <asp:ScriptManager runat="server" ID="sc1">
                </asp:ScriptManager>
            </td>
        </tr>
        
        
        
    </table>
</asp:Content>
