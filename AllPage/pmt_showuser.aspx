﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="pmt_showuser.aspx.cs" Inherits="AllPage_admin_showuser" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="admin_StyleSheet.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var isShift = false;
        $(document).ready(function () {
            $("#<%=SpacialCharRemove.XSS_Remove(TextBox1.ClientID) %>").keypress(function (e) {
                //alert(e.keyCode);

                if (e.keyCode == 16) {
                    //alert("Shift");
                    isShift = true;
                } else {
                    isShift = false;
                }
                if (e.keyCode == 53 && isShift == true) {
                    return false;
                } else if (e.keyCode == 37) {
                    return false;
                } else {
                    return true;
                }
            })

        })
    </script>
    <table width="100%">
        <tr>
            <td style="width: 100%; text-align: center">
                <asp:Label ID="Label3" runat="server" CssClass="lblstly" Text="User Details"></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <asp:Label ID="Label6" runat="server" Text="Enter Username :"></asp:Label>
                <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True"
                    OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                    ToolTip="Export To Excel"
                    Width="25px" OnClick="ImageButton1_Click1" />
            </td>
        </tr>
        <tr>
            <td style="text-align: center; width: 100%">
                <asp:Panel ID="panel1" runat="server" ScrollBars="Both" Width="950px" Height="450px">
                    <asp:GridView ID="grduser" runat="server" BackColor="WhiteSmoke" BorderColor="#999999"
                        BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black"
                        Width="100%" EmptyDataText="Data Not Found.">
                        <EmptyDataRowStyle BackColor="#FFA500" ForeColor="White" />
                        <EditRowStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="#FFA500" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                        <RowStyle Font-Names="Calibri" Font-Size="13px" />
                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
