﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Ex_inspector_menu.ascx.cs" Inherits="admin_menu" %>

<div id="menu">
    <ul class="menu">
        <li><a href="Ex_inspector_reminder.aspx" class="parent"><span>Dashboard</span></a>
            <ul>
            </ul>
        </li>
        <li><a href="Ex_inspector_rptgrid.aspx" class="parent"><span>Report View</span></a>
            <ul>
            </ul>
        </li>
        <li><a href="#" class="parent"><span>Chart</span></a>
            <ul>
                <li><a href="Ex_inspector_chart.aspx"><span>Inspection(PieChart)</span></a> </li>
                <li><a href="Ex_inspector_bar_chart.aspx"><span>Inspection(BarChart)</span></a> </li>
                <li><a href="Ex_inspector_bar_chart_category.aspx"><span> CheckPoint</span></a> </li>
            </ul>
        </li>
        <li><a href="Ex_inspector_car_pdf_Report.aspx" class="parent"><span>Inspection PDF Report</span></a>
            <ul>
            </ul>
        </li>
    </ul>
</div>
