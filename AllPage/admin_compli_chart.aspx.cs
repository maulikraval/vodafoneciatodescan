﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using InfoSoftGlobal;
using mybusiness;

public partial class PM_rptgrid : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    myvodav2 ba;
    string sp_user_d = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        lblmsg.Text = "";
        //FCLiteral.Text = CreateChart();
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
        if (!IsPostBack)
        {
            drpchecksheet.Visible = false;
            drpprovider.Visible = false;
            ddlsite.Visible = false;

            try
            {
                int i = 4;

                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
                int count = 0;
                int count1 = 0;
                int count2 = 0;
                // Marquee start
              /*  da = new mydataaccess1();
                DataTable dtv5 = new DataTable();
                dtv5 = da.reminder_20_days();
                da = new mydataaccess1();
                DataTable dtv2 = new DataTable();
                dtv2 = da.reminder_20_days_v2();
                da = new mydataaccess1();
                DataTable dtv2ms = new DataTable();
                dtv2ms = da.reminder_20_days_v2ms();
                da = new mydataaccess1();
                DataTable dtall = new DataTable();
                dtall = da.reminder_20_days_all();
                da = new mydataaccess1();
                DataTable cvall = new DataTable();
                cvall = da.reminder_20_days_critical_all();
                da = new mydataaccess1();
                DataTable dtall1 = new DataTable();
                dtall1 = da.reminder_10_days_all();
                count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count + dtall.Rows.Count;

                // critical points
                da = new mydataaccess1();
                DataTable cv2 = new DataTable();
                cv2 = da.reminder_20_days_critical_v2();
                da = new mydataaccess1();
                DataTable cv5 = new DataTable();
                cv5 = da.reminder_20_days_critical();
                da = new mydataaccess1();
                DataTable cv2ms = new DataTable();
                cv2ms = da.reminder_20_days_critical_v2ms();
                count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count + cvall.Rows.Count;

                da = new mydataaccess1();
                DataTable dtv51 = new DataTable();
                dtv51 = da.reminder_10_days();
                da = new mydataaccess1();
                DataTable dtv21 = new DataTable();
                dtv21 = da.reminder_10_days_v2();
                da = new mydataaccess1();
                DataTable dtv2ms1 = new DataTable();
                dtv2ms1 = da.reminder_10_days_v2ms();
                count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count + dtall1.Rows.Count;
                lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";
				*/
                // Marquee End
                // lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>10</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>24</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>3</span>)";
            }
            catch (Exception ee)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {
            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }
                            ba = new myvodav2();

                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                        }
                    }

                    if (Session["role"].ToString() == "4")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();



                            ba.User = sp_user_d;
                            circle = da.getcirclename(sp_user_d);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");

                        }
                    }

                }

            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";

        if (drpcircle.SelectedIndex != 0)
        {
            panel2.Visible = true;
            drpchecksheet.SelectedIndex = 0;
            ddlsite.SelectedIndex = 0;
            drpprovider.SelectedIndex = 0;
            // FCLiteral.Text = CreateChart_circle();
            drpchecksheet.Visible = true;
            drpprovider.Visible = true;
            ddlsite.Visible = true;
        }
        //if (drpcircle.SelectedIndex == 1)
        //{
        //    drpchecksheet.Visible = false;
        //    drpprovider.Visible = false;
        //    panel2.Visible = true;
        //    // FCLiteral.Text = CreateChart();
        //}
    }
    public string encode(string lbl)
    {

        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;

    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }

            if (Request.Cookies["AuthToken"] != null)
            {
                Response.Cookies["AuthToken"].Value = string.Empty;
                Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            }

            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }

    public string CreateChart_circle_check(string filter, string strXML)
    {
        try
        {

//Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + filter+ "');</script>");

            da = new mydataaccess1();
            dt = new DataTable();
            string check = "";
            if (drpchecksheet.SelectedIndex == 1)
            {
                check = "v2";
            }
            if (drpchecksheet.SelectedIndex == 2)
            {
                check = "v5";
            }
            if (drpchecksheet.SelectedIndex == 3)
            {
                check = "v2ms";
            }
            //if (TextBox1.Text != "" && TextBox2.Text != "")
            //{
            //    dt = da.compliance_sites_v5(drpcircle.SelectedValue, check, 1, TextBox1.Text, TextBox2.Text);
            //}
            //else
            //{
            dt = da.compliance_sites_v5(filter, check);
            //  }
            if (dt.Rows[0][0].ToString() == "")
            {
                // string strXML = "";
                lblmsg.Text = "No Data Found as there are no sites for " + SpacialCharRemove.XSS_Remove(drpchecksheet.SelectedItem.Text) + " .";
                strXML = "<graph caption='No Data Found'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                return strXML;
            }
            else
            {
                //  string strXML = "";
                string str = "", strDataProdA, strDataProdB, strDataProdC;
                strDataProdA = "<dataset seriesName='Site Compliance' color='33FF33'>";
                strDataProdB = "<dataset seriesName='Site Non Compliance' color='FF3333'>";
                strDataProdC = "<dataset seriesName='Site Not Inspected' color='FFC200'>";

                //   strXML = "<graph caption='Site Inspection Status For Circle: " + drpcircle.SelectedValue + " , CheckSheet: " + check + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                strXML += "<set name='" + "Site Compliance" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + ") +" + "' value='" + Math.Round(Convert.ToDouble(dt.Rows[0][3].ToString()), 2) + "' color='33FF33' />";

                strXML += "<set name='" + "Site Non Compliance" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + ") + " + "' value='" + Math.Round(Convert.ToDouble(dt.Rows[0][4].ToString()), 2) + "' color='FF3333' />";
                strXML += "<set name='" + "Site Not Inspected" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + ") + " + "' value='" + Math.Round(Convert.ToDouble(dt.Rows[0][5].ToString()), 2) + "' color='FFC200' />";


                strXML += "</graph>";

                strDataProdA += "</dataset>";
                strDataProdB += "</dataset>";
                strDataProdC += "</dataset>";

                return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);
                //return FusionCharts.RenderChart("../FusionCharts/Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);
            }
        }
        catch
        {
            panel1.Visible = false;
            return "";

        }

    }

    public string CreateChart_circle()
    {
        lblmsg.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();

        dt = da.pie_chart_compl_circle(drpcircle.SelectedValue);
        string strXML = "";
        if (dt.Rows[0][0].ToString() == "no")
        {

            strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + SpacialCharRemove.XSS_Remove(drpchecksheet.SelectedValue) + "' numberPrefix='%' decimalPrecision='1' >";

            lblmsg.Text = "No Data Found.";
            return strXML;

        }



        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
        strXML += "<set name='" + "Compliance Sites" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' />";

        strXML += "<set name='" + "Non Compliance Sites" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + "' />";


        strXML += "</graph>";

        return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);


    }
    public string CreateChart()
    {

        lblmsg.Text = "";
        da = new mydataaccess1();
        dt = new DataTable();
        string username = lblusername.Text;
        dt = da.pie_chart_compl(username);
        string strXML = "";


        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
        //strXML += "<set name='" + "InComplete" + " + " + "' value='" + dt.Rows[0][1].ToString() + "' />";

        //strXML += "<set name='" + "Complete" + " + " + "' value='" + dt.Rows[0][0].ToString() + "' />";
        strXML += "<set name='" + "Compliance Sites" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][0].ToString()) + ") +" + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][2].ToString()) + "' />";

        strXML += "<set name='" + "Non Compliance Sites" + " + (" + SpacialCharRemove.XSS_Remove(dt.Rows[0][1].ToString()) + ") + " + "' value='" + SpacialCharRemove.XSS_Remove(dt.Rows[0][3].ToString()) + "' />";


        strXML += "</graph>";



        return FusionCharts.RenderChart("../FusionCharts/FCF_Pie3D.swf", "", strXML, "Sales", "600", "350", false, false);


    }
    protected void drpchecksheet_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        drpprovider.SelectedIndex = 0;
        ddlsite.SelectedIndex = 0;
        string check = "";
        if (drpchecksheet.SelectedIndex == 1)
        {
            check = "v2";
        }
        if (drpchecksheet.SelectedIndex == 2)
        {
            check = "v5";
        }
        string strXML = "";
        string strXML1 = "";
        if (drpchecksheet.SelectedIndex != 0)
        {
            panel2.Visible = true;
            panel1.Visible = true;
            panel3.Visible = false;

            string filter = "";
            string filter1 = "";

            if (drpcircle.SelectedIndex == 1)
            {

                da = new mydataaccess1();
                 sp_user_d = da.select_user_cookie(Session["user"].ToString());

                da = new mydataaccess1();
                ba = new myvodav2();
                DataTable circle = new DataTable();
                ba.User = sp_user_d;
                circle = da.getcirclenamefromusername(ba);
                filter = "where (";
                for (int k = 0; k < circle.Rows.Count; k++)
                {
                    if (k != circle.Rows.Count - 1)
                    {
                        filter += " site_master.circle='" + circle.Rows[k][0].ToString() + "' or ";
                    }
                    else
                    {
                        filter += " site_master.circle='" + circle.Rows[k][0].ToString() + "')";
                    }

                }

                //filter1 = "";
                strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + "'  showPercentageInLabel='1' pieSliceDepth='25' numberPrefix='%' decimalPrecision='1' showNames='1'>";
                strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
            }
            else
            {

                strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + "'  showPercentageInLabel='1' pieSliceDepth='25' numberPrefix='%'  decimalPrecision='1' showNames='1'>";
                strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + "'  showPercentageInLabel='1' pieSliceDepth='25'   decimalPrecision='1' showNames='1'>";
                filter = "where site_master.circle='" + drpcircle.SelectedValue + "'";
            }
            FCLiteral.Text = CreateChart_circle_check(filter, strXML);
            Literal1.Text = CreateChart_barchart(filter, strXML);
            Literal2.Text = CreateChart_barchart_count(filter, strXML1);

        }
        else
        {
            panel2.Visible = false;
            panel1.Visible = false;
            panel3.Visible = false;
        }

    }


    protected void drpprovider_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        ddlsite.SelectedIndex = 0;
        string check = "";
        if (drpchecksheet.SelectedIndex == 1)
        {
            check = "v2";
        }
        if (drpchecksheet.SelectedIndex == 2)
        {
            check = "v5";
        }
        string strXML = "";
        string strXML1 = "";
        if (drpchecksheet.SelectedIndex != 0 && drpprovider.SelectedIndex != 0)
        {
            panel2.Visible = true;
            panel1.Visible = true;
            panel3.Visible = false;
            string filter = "";
            if (drpcircle.SelectedIndex == 1)
            {

                da = new mydataaccess1();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                da = new mydataaccess1();
                ba = new myvodav2();
                DataTable circle = new DataTable();
                ba.User = sp_user_d;
                circle = da.getcirclenamefromusername(ba);
                filter = "where (";
                for (int k = 0; k < circle.Rows.Count; k++)
                {
                    if (k != circle.Rows.Count - 1)
                    {
                        filter += " site_master.circle='" + circle.Rows[k][0].ToString() + "' or ";
                    }
                    else
                    {
                        filter += " site_master.circle='" + circle.Rows[k][0].ToString() + "')";
                    }

                }
                if (drpprovider.SelectedIndex == 1)
                {
                    filter += " and (site_master.provider in ('Vodafone','VIL') )";
                    strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue)+ " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                }
                if (drpprovider.SelectedIndex == 2)
                {
                    filter += " and (site_master.provider='Indus') ";
                    strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

                }
                if (drpprovider.SelectedIndex == 3)
                {
                    filter += " and (site_master.provider not in ('Vodafone','VIL') and  site_master.provider!='Indus')";
                    strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

                }
            }
            else
            {

                if (drpprovider.SelectedIndex == 1)
                {
                    filter = "where (site_master.provider in ('Vodafone','VIL')  and site_master.circle='" + drpcircle.SelectedValue + "' )";
                    strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "'   showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                }
                if (drpprovider.SelectedIndex == 2)
                {
                    filter = "where (site_master.provider='Indus'  and site_master.circle='" + drpcircle.SelectedValue + "') ";
                    strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

                }
                if (drpprovider.SelectedIndex == 3)
                {
                    filter = "where (site_master.provider not in ('Vodafone','VIL') and  site_master.provider!='Indus' and site_master.circle='" + drpcircle.SelectedValue + "' )";
                    strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + "' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                }
            }

            FCLiteral.Text = CreateChart_circle_check(filter, strXML);
            Literal1.Text = CreateChart_barchart(filter, strXML);
            Literal2.Text = CreateChart_barchart_count(filter, strXML1);


        }
        else
        {
            panel2.Visible = false;
            panel1.Visible = false;
            panel3.Visible = false;
        }
    }




    private string CreateChart_barchart(string filter, string str)
    {
        try
        {
            string strXML = str, strCategories;
            // string strXML1 = str, strCategories, strDataProdA, strDataProdB, strDataProdC;//, strDataProdD, strDataProdE, strDataProdF, strDataProdH, strDataProdG, strDataProdI;
            string check = "";
            if (drpchecksheet.SelectedIndex == 1)
            {
                check = "v2";
            }
            if (drpchecksheet.SelectedIndex == 2)
            {
                check = "v5";
            }
            //  strXML = "<graph caption='Site Inspection Status For Circle: " + drpcircle.SelectedValue + " , CheckSheet: " + check + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

            //Initialize <categories> element - necessary to generate a stacked chart
            strCategories = "<categories>";



            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.compliance_sites_v5_bar(filter, check);
            if (dt.Rows.Count > 0)
            {
                string[,] arrData = new string[dt.Rows.Count, 7];

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    arrData[j, 0] = SpacialCharRemove.XSS_Remove(dt.Rows[j][0].ToString());
                    arrData[j, 1] = SpacialCharRemove.XSS_Remove(dt.Rows[j][1].ToString());
                    arrData[j, 2] = SpacialCharRemove.XSS_Remove(dt.Rows[j][2].ToString());
                    arrData[j, 3] = SpacialCharRemove.XSS_Remove(dt.Rows[j][3].ToString());
                    arrData[j, 4] = SpacialCharRemove.XSS_Remove(dt.Rows[j][4].ToString());
                    arrData[j, 5] = SpacialCharRemove.XSS_Remove(dt.Rows[j][5].ToString());
                    arrData[j, 6] = SpacialCharRemove.XSS_Remove(dt.Rows[j][6].ToString());

                }


                string strDataCurr, strDataPrev, strDatanot;
                strDataCurr = "<dataset seriesName='Site Compliance' color='33FF33'>";
                strDataPrev = "<dataset seriesName='Site Non Compliance' color='FF3333'>";
                strDatanot = "<dataset seriesName='Site Not Inspected' color='FFC200'>";


                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    int sum_count = Convert.ToInt32(dt.Rows[i][0].ToString()) + Convert.ToInt32(dt.Rows[i][1].ToString()) + Convert.ToInt32(dt.Rows[i][2].ToString());

                    //Append <category name='...' /> to strCategories
                    if (drpcircle.SelectedIndex != 1)
                    {
                        strDataCurr = "<dataset seriesName='Site Compliance (" + SpacialCharRemove.XSS_Remove(arrData[i, 0]) + ")' color='33FF33'>";
                        strDataPrev = "<dataset seriesName='Site Non Compliance (" + SpacialCharRemove.XSS_Remove(arrData[i, 1]) + ")' color='FF3333'>";
                        strDatanot = "<dataset seriesName='Site Not Inspected (" + SpacialCharRemove.XSS_Remove(arrData[i, 2]) + ")' color='FFC200'>";



                    }

                    //Append <category name='...' /> to strCategories
                    strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "(" + sum_count + ")' />";
                    //Add <set value='...' /> to both the datasets
                    strDataCurr += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 3]) + "' />";
                    strDataPrev += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 4]) + "' />";
                    strDatanot += "<set value='" + SpacialCharRemove.XSS_Remove(arrData[i, 5]) + "' />";




                }


                strCategories += "</categories>";
                strDataCurr += "</dataset>";
                strDataPrev += "</dataset>";
                strDatanot += "</dataset>";



                //Assemble the entire XML now
                strXML += strCategories + strDataCurr + strDataPrev + strDatanot + "</graph>";


            }
            else
            {
                strXML = "No Data Found..";

            }
            string countdrp = "";
            if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
            {
                countdrp = "600";
            }
            else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
            {
                countdrp = "1000";
            }
            else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
            {
                countdrp = "2000";
            }
            else
            {
                countdrp = "600";
            }
            return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales1", countdrp, "300", false, false);


        }
        catch
        {
            panel2.Visible = false;
            return "";
        }

    }


    protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblmsg.Text = "";
        string check = "";
        if (drpchecksheet.SelectedIndex == 1)
        {
            check = "v2";
        }
        if (drpchecksheet.SelectedIndex == 2)
        {
            check = "v5";
        }
        string strXML = "";
        string strXML1 = "";
        if (drpchecksheet.SelectedIndex != 0 && drpprovider.SelectedIndex != 0)
        {
            panel2.Visible = true;
            panel1.Visible = true;
            panel3.Visible = false;
            string filter = "";
            if (drpcircle.SelectedIndex == 1)
            {
                da = new mydataaccess1();
                sp_user_d = da.select_user_cookie(Session["user"].ToString());

                da = new mydataaccess1();
                ba = new myvodav2();
                DataTable circle = new DataTable();
                ba.User = sp_user_d;
                circle = da.getcirclenamefromusername(ba);
                filter = "where (";
                for (int k = 0; k < circle.Rows.Count; k++)
                {
                    if (k != circle.Rows.Count - 1)
                    {
                        filter += " site_master.circle='" + circle.Rows[k][0].ToString() + "' or ";
                    }
                    else
                    {
                        filter += " site_master.circle='" + circle.Rows[k][0].ToString() + "')";
                    }

                }
                if (ddlsite.SelectedIndex == 1)
                {

                    if (drpprovider.SelectedIndex == 1)
                    {
                        filter += " and (site_master.provider in ('Vodafone','VIL') and site_master.site_date>'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

                    }
                    if (drpprovider.SelectedIndex == 2)
                    {
                        filter += " and (site_master.provider='Indus' and site_master.site_date>'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text)+ " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text)+ "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:"+ SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                    if (drpprovider.SelectedIndex == 3)
                    {
                        filter += " and (site_master.provider not in ('Vodafone','VIL') and  site_master.provider!='Indus' and site_master.site_date>'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue )+ " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text )+ " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text )+ "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

                    }
                }
                else
                {
                    if (drpprovider.SelectedIndex == 1)
                    {
                        filter += " and (site_master.provider in ('Vodafone','VIL') and site_master.site_date<'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue)+ " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text )+ " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text)+ "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: "+ SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";

                    }
                    if (drpprovider.SelectedIndex == 2)
                    {
                        filter += " and (site_master.provider='Indus' and site_master.site_date<'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue )+ " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text )+ " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text)+ "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                    if (drpprovider.SelectedIndex == 3)
                    {
                        filter += " and (site_master.provider not in ('Vodafone','VIL') and  site_master.provider!='Indus' and site_master.site_date<'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue)+ " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text )+ " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: "+ SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:"+ SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }

                }
            }
            else
            {
                if (ddlsite.SelectedIndex == 1)
                {

                    if (drpprovider.SelectedIndex == 1)
                    {
                        filter = "where (site_master.provider in ('Vodafone','VIL')  and site_master.circle='" + drpcircle.SelectedValue + "' and site_master.site_date>'2017/03/31') ";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: "+ SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                    if (drpprovider.SelectedIndex == 2)
                    {
                        filter = "where (site_master.provider='Indus'  and site_master.circle='" + drpcircle.SelectedValue + "' and site_master.site_date>'2017/03/31' )";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: "+ SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'   showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                    if (drpprovider.SelectedIndex == 3)
                    {

                        filter = "where (site_master.provider not in ('Vodafone','VIL') and  site_master.provider!='Indus' and site_master.circle='" + drpcircle.SelectedValue + "' and site_master.site_date>'2017/03/31' )";
         //   Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + filter+ "');</script>");
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: "+ SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                }
                else
                {
                    if (drpprovider.SelectedIndex == 1)
                    {
                        filter = "where (site_master.provider in ('Vodafone','VIL')  and site_master.circle='" + drpcircle.SelectedValue + "' and site_master.site_date<'2017/03/31')";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: "+ SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                    if (drpprovider.SelectedIndex == 2)
                    {
                        filter = "where (site_master.provider='Indus'  and site_master.circle='" + drpcircle.SelectedValue + "' and site_master.site_date<'2017/03/31')";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: "+ SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                    if (drpprovider.SelectedIndex == 3)
                    {
                        filter = "where (site_master.provider not in ('Vodafone','VIL') and  site_master.provider!='Indus' and site_master.circle='" + drpcircle.SelectedValue + "' and site_master.site_date<'2017/03/31')";
                        strXML = "<graph caption='Site Inspection Status For Circle: " + SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "' numberPrefix='%' showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                        strXML1 = "<graph caption='Site Inspection Status For Circle: "+ SpacialCharRemove.XSS_Remove(drpcircle.SelectedValue) + " , CheckSheet: " + check + " , Provider: " + SpacialCharRemove.XSS_Remove(drpprovider.SelectedItem.Text) + " ,Sites:" + SpacialCharRemove.XSS_Remove(ddlsite.SelectedItem.Text) + "'  showPercentageInLabel='1' pieSliceDepth='25'  decimalPrecision='1' showNames='1'>";
                    }
                }
            }

            FCLiteral.Text = CreateChart_circle_check(filter, strXML);
            Literal1.Text = CreateChart_barchart(filter, strXML);
            Literal2.Text = CreateChart_barchart_count(filter, strXML1);


        }
        else
        {
            panel1.Visible = false;
            panel2.Visible = false;
            panel3.Visible = false;
        }
    }

    private string CreateChart_barchart_count(string filter, string str)
    {

        string strXML = str, strCategories, strDataProdA, strDataProdB, strDataProdC;//, strDataProdD, strDataProdE, strDataProdF, strDataProdH, strDataProdG, strDataProdI;
        string check = "";
        if (drpchecksheet.SelectedIndex == 1)
        {
            check = "v2";
        }
        if (drpchecksheet.SelectedIndex == 2)
        {
            check = "v5";
        }
        // strXML = "<graph caption=' PTW Status for Circle : " + drpcircle.SelectedValue + "' numberPrefix='' formatNumberScale='0' decimalPrecision='0'>";

        //Initialize <categories> element - necessary to generate a stacked chart
        strCategories = "<categories>";
        strDataProdA = "<dataset seriesName='Site Compliance' color='33FF33'>";
        strDataProdB = "<dataset seriesName='Site Non Compliance' color='FF3333'>";
        strDataProdC = "<dataset seriesName='Site Not Inspected' color='FFC200'>";

        da = new mydataaccess1();
        dt = new DataTable();
        dt = da.compliance_sites_v5_bar(filter, check);

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int sum_count = Convert.ToInt32(dt.Rows[i][0].ToString()) + Convert.ToInt32(dt.Rows[i][1].ToString()) + Convert.ToInt32(dt.Rows[i][2].ToString());

                //Append <category name='...' /> to strCategories
                strCategories += "<category name='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][6].ToString()) + "(" + sum_count + ")' />";
                strDataProdA += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString()) + "' />";
                strDataProdB += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][1].ToString()) + "' />";
                strDataProdC += "<set value='" + SpacialCharRemove.XSS_Remove(dt.Rows[i][2].ToString()) + "' />";

                //strDataProdA += "<set value='" + arrData[i, 1] + "' />";
                //strDataProdB += "<set value='" + arrData[i, 2] + "' />";
            }


            strCategories += "</categories>";

            //Close <dataset> elements
            strDataProdA += "</dataset>";
            strDataProdB += "</dataset>";
            strDataProdC += "</dataset>";

            //Close <categories> element


            //Assemble the entire XML now
            strXML += strCategories + strDataProdA + strDataProdB + strDataProdC + "</graph>";
        }
        else
        {
            strXML = "No Data Found..";
        }
        string countdrp = "";
        if (drpcircle.Items.Count <= 7 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "600";
        }
        else if (drpcircle.Items.Count > 8 && drpcircle.Items.Count <= 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "1000";
        }
        else if (drpcircle.Items.Count > 15 && drpcircle.SelectedIndex == 1)
        {
            countdrp = "2000";
        }
        else
        {
            countdrp = "600";
        }
        //  return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales1", countdrp, "300", false, false);
        return FusionCharts.RenderChart("../FusionCharts/FCF_StackedColumn3D.swf", "", strXML, "productSales156", countdrp, "300", false, false);

    }
}
