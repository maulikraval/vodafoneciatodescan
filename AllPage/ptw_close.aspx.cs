﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using System.Data;

public partial class ptw_close : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drp1AtRiskStatus.ClearSelection();
            drpPTWStatus.ClearSelection();
        }
        //ImageButton1.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearchBlkId.Text != "")
        {
            da = new mydataaccess1();
             //dt = new DataTable();
			DataSet dt = new DataSet();
            dt = da.select_ptw_Master_report("", "where ptw_basic_detail_master.ptwid='" + txtSearchBlkId.Text + "'");
            GridView1.DataSource = dt.Tables[0];
            GridView1.DataBind();
            GridView1.Visible = true;
            Panel1.Visible = true;
            lbl1risk.Visible = true;
            lblptw.Visible = true;
            drp1AtRiskStatus.Visible = true;
            drpPTWStatus.Visible = true;
            btnUpdate.Visible = true;
            if (GridView1.Rows.Count > 0)
            {
                for (int i = 0; i < GridView1.Rows.Count; i++)
                {
                    CheckBox chkdelete = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkSelect");
                    string ptwid = GridView1.Rows[i].Cells[13].Text;
                    int id2 = ptwid.LastIndexOf("/") + 1;
                    int id = Convert.ToInt32(ptwid.Substring(id2, (ptwid.Length - id2)));
                    chkdelete.ToolTip = id.ToString();
                }
            }
        }
        else
        {
            GridView1.Visible = false;
            Panel1.Visible = false;
        }
    }

    //protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    //{
    //    Response.ClearContent();
    //    string FileName = "PTWCustomizeReport_" + DateTime.Now + ".xls";
    //    Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
    //    Response.ContentType = "application/excel";
    //    System.IO.StringWriter sw = new System.IO.StringWriter();
    //    HtmlTextWriter htw = new HtmlTextWriter(sw);
    //    GridView1.RenderControl(htw);
    //    Response.Write(sw.ToString());
    //    Response.End();
    //}

    protected void drp1AtRiskStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drp1AtRiskStatus.SelectedIndex == 1)
        {
            drpPTWStatus.Items.FindByText("Closed By Issuer").Enabled = true;
            drpPTWStatus.Items.FindByText("Closed By Receiver").Enabled = true;
            drpPTWStatus.Items.FindByText("Rejected").Enabled = false;
        }
        else if (drp1AtRiskStatus.SelectedIndex == 2)
        {

            drpPTWStatus.Items.FindByText("Rejected").Enabled = true;
            drpPTWStatus.Items.FindByText("Closed By Issuer").Enabled = false;
            drpPTWStatus.Items.FindByText("Closed By Receiver").Enabled = false;
        }
        else
        {
            drpPTWStatus.Items.FindByText("Rejected").Enabled = false;
            drpPTWStatus.Items.FindByText("Closed By Issuer").Enabled = false;
            drpPTWStatus.Items.FindByText("Closed By Receiver").Enabled = false;
        }
    }

    protected void drpPTWStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (drp1AtRiskStatus.SelectedIndex != 0)
        {
            if (drpPTWStatus.SelectedIndex != 0)
            {
                if (GridView1.Rows.Count > 0)
                {
                    for (int i = 0; i < GridView1.Rows.Count; i++)
                    {
                        CheckBox chkdelete = (CheckBox)GridView1.Rows[i].Cells[0].FindControl("chkSelect");
                        string ptwid = GridView1.Rows[i].Cells[10].Text;
                        //string person = GridView1.Rows[i].Cells[14].Text;
                        string personid = GridView1.Rows[i].Cells[13].Text;

                        if (chkdelete.Checked)
                        {
                            da = new mydataaccess1();
                            int id2 = ptwid.LastIndexOf("/") + 1;
                            int id = Convert.ToInt32(ptwid.Substring(id2, (ptwid.Length - id2)));
                            int personid2 = personid.LastIndexOf("/") + 1;
                            int pid = Convert.ToInt32(personid.Substring(personid2, (personid.Length - personid2)));
                            int ptwstatus = Convert.ToInt32(drpPTWStatus.SelectedItem.Value);
                            da.ptw_report_update(id, pid, drp1AtRiskStatus.SelectedItem.Text, ptwstatus);
                            /*Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Selected Record Updated Successfully!!!');</script>");*/
                        }
                        //else
                        //{
                        //    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select Checkbox First!!!');</script>");
                        //} 
                    }
                    da = new mydataaccess1();
                    DataSet dt = new DataSet();
                    dt = da.select_ptw_Master_report("", "where ptw_basic_detail_master.ptwid='" + txtSearchBlkId.Text + "'");
                    GridView1.DataSource = dt.Tables[0];;
                    GridView1.DataBind();
                    GridView1.Visible = true;
                    Panel1.Visible = true;
                    drp1AtRiskStatus.ClearSelection();
                    drpPTWStatus.ClearSelection();
                }
                else
                {
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView1.Visible = false;
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('You Have No Any Record To Update Status!!!');</script>");
                    drp1AtRiskStatus.ClearSelection();
                    drpPTWStatus.ClearSelection();
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select PTW Status!!!');</script>");
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Select 1@Risk Status!!!');</script>");
        }  
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string strScript = "SelectDeSelectHeader(" + ((CheckBox)e.Row.Cells[0].FindControl("chkSelect")).ClientID + ");";
                ((CheckBox)e.Row.Cells[0].FindControl("chkSelect")).Attributes.Add("onclick", strScript);
            }
        }
        catch (Exception Ex)
        {
            //report error
        } 
    }
}