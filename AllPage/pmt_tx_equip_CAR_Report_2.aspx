﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AllPage/AllCommon.master"
    AutoEventWireup="true" CodeFile="pmt_tx_equip_CAR_Report_2.aspx.cs" Inherits="circle_admin_car_report_2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type='text/javascript'>
        xAddEventListener(window, 'load',
            function () { new xTableHeaderFixed('gvTheGrid', 'table-container', 0); }, false);
    </script>
<script type="text/javascript" language="javascript">
    function HideModal() {
        $find('modalPopupBehavior').hide();
    }
</script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">
    </script>

   
     <script type="text/javascript">
$(function () {
    //Enable Disable TextBoxes in a Row when the Row CheckBox is checked.
    $("[id*=CheckBox1]").bind("click", function () {
 
        //Find and reference the GridView.
        var grid = $(this).closest("table");
 
        //If the CheckBox is Checked then enable the TextBoxes in thr Row.
        if (!$(this).is(":checked")) {
            var td = $("td", $(this).closest("tr"));
            //td.css({ "background-color": "#FFF" });
            $("input[type=text]", td).attr("disabled", "disabled");
            $("[id*=ddlActionStatus]", td).attr("disabled", "disabled");
            $("input[type=file]", td).attr("disabled", "disabled");
        } else {
            var td = $("td", $(this).closest("tr"));
            //td.css({ "background-color": "#D8EBF2" });
            $("input[type=text]", td).removeAttr("disabled");
            $("[id*=ddlActionStatus]", td).removeAttr("disabled");
            $("input[type=file]", td).removeAttr("disabled");
        }
    });
});
</script>

<%--<script type="text/javascript">
        window.onload = function() {
            var check = document.getElementById("<%=CheckBox1.ClientID %>");
            check.onchange = function() {
                if (this.checked == true)
                    document.getElementById("<%=txtFollowupComments.ClientID %>").disabled = false;
                else
                    document.getElementById("<%=txtFollowupComments.ClientID %>").disabled = true;
            };
        };
</script>--%>  

    <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <style>
        .ajax__calendar_body
        {
            position: relative;
        }
    </style>
    <style type="text/css">
        .calendarOverridedivdiv
        {
            padding: 0;
            margin: 0;
        }
        .calendarOverridetabletr
        {
            padding: 0;
            margin: 0;
        }
        .calendarOverridetabletrtd
        {
            padding: 0;
            margin: 0;
            border-style: none;
            border-width: 0;
        }
    </style>
    <style>
        .textboxstyle
        {
            z-index: 0;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%">
        <tr>
            <td style="width: 25%; text-align: Left; background-color: #D8D9DA;" colspan="4">
                <asp:Label ID="Label9" runat="server" Text="Site Basic Detail : " ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label1" runat="server" Text="Circle" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label2" runat="server" Text="Zone" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox2" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label3" runat="server" Text="Site ID" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox3" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label4" runat="server" Text="Last Inspection Date" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox4" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label5" runat="server" Text="Infra Provider ID" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox5" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label6" runat="server" Text="Infra Provider Name" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox6" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
          <%--  <td style="width: 25%; text-align: right">
                <asp:Label ID="Label7" runat="server" Text="Achieved Score(%)" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox7" runat="server" ReadOnly="true"></asp:TextBox>
            </td>--%>
            <td style="width: 25%; text-align: right">
                <asp:Label ID="Label8" runat="server" Text="Site Inspector" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 25%; text-align: left">
                <asp:TextBox ID="TextBox8" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 100%" colspan="4">
                <table width="100%">
                   <%-- <tr>
                        <td style="text-align: center; background-color: #D8D9DA" colspan="2">
                            <asp:Label ID="Label11" runat="server" Text="Last Inspection Status" ForeColor="Black"
                                Font-Bold="True"></asp:Label>
                        </td>
                        <td style="text-align: center; background-color: #D8D9DA" colspan="2">
                            <asp:Label ID="Label17" runat="server" Text="Status after Risk Mitigation " ForeColor="Black"
                                Font-Bold="True"></asp:Label>
                        </td>
                        <td style="text-align: center; background-color: #D8D9DA" colspan="2">
                            <asp:Label ID="Label25" runat="server" Text="Legend Details" ForeColor="Black" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>--%>
                   <%-- <tr>
                        <td style="width: 20%; text-align: right">
                            <asp:Label ID="Label19" runat="server" Text="Critical A points" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                            border-right-style: solid;">
                            <asp:Label ID="lbla" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 20%; text-align: right">
                            <asp:Label ID="Label20" runat="server" Text="Critical A points" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                            border-right-style: solid;">
                            <asp:Label ID="lblaa" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 16%; text-align: left">
                            <asp:TextBox ID="TextBox12" runat="server" ReadOnly="true" Width="10%" BackColor="Red"></asp:TextBox>
                            <asp:Label ID="Label21" runat="server" Text="Open" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 16%; text-align: left;
                            border-right-style: solid;">
                            <asp:TextBox ID="TextBox9" runat="server" ReadOnly="true" Width="10%" BackColor="Green"></asp:TextBox>
                            <asp:Label ID="Label18" runat="server" Text="Close" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>--%>
                   <%-- <tr>
                        <td style="width: 20%; text-align: right">
                            <asp:Label ID="Label12" runat="server" Text="Critical B points" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                            border-right-style: solid;">
                            <asp:Label ID="lblb" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 20%; text-align: right">
                            <asp:Label ID="Label15" runat="server" Text="Critical B points" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                            border-right-style: solid;">
                            <asp:Label ID="lblbb" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 16%; text-align: left">
                            <asp:TextBox ID="TextBox10" runat="server" ReadOnly="true" Width="10%" BackColor="Yellow"></asp:TextBox>
                            <asp:Label ID="Label24" runat="server" Text="WIP" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 16%; text-align: left;
                            border-right-style: solid;">
                            <asp:TextBox ID="TextBox11" runat="server" ReadOnly="true" Width="10%" BackColor="SkyBlue"></asp:TextBox>
                            <asp:Label ID="Label27" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>--%>
                   <%-- <tr>
                        <td style="width: 20%; text-align: right">
                            <asp:Label ID="Label13" runat="server" Text="Critical C points" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                            border-right-style: solid;">
                            <asp:Label ID="lblc" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 20%; text-align: right">
                            <asp:Label ID="Label22" runat="server" Text="Critical C points" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 12%; text-align: left;
                            border-right-style: solid;">
                            <asp:Label ID="lblcc" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 16%; text-align: left">
                            <asp:TextBox ID="TextBox13" runat="server" ReadOnly="true" Width="10%" BackColor="Khaki"></asp:TextBox>
                            <asp:Label ID="Label10" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-color: #000000; width: 16%; text-align: left;
                            border-right-style: solid;">
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td style="text-align: Left; background-color: #D8D9DA;">
                <asp:Label ID="Label14" runat="server" Text="Corrective Action Report :" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblError" runat="Server" Visible="false"  Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" Text="Submit" CausesValidation="true" OnClick="btnSave_Click1" BackColor="#DE2021" Font-Bold="True" ForeColor="White"  />
                &nbsp; 
                               <%-- <td width="5%">--%>
                                    <asp:Label ID="Label61" runat="server" Font-Names="Calibri" Font-Size="13px" ForeColor="#FFA500"
                                        Text="Search For : "></asp:Label>
                                    <asp:TextBox ID="txtsearxh" runat="server"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="tw" runat="server" WatermarkText="NSS ID" WatermarkCssClass="waterback"
                                        TargetControlID="txtsearxh">
                                    </cc1:TextBoxWatermarkExtender>
                                    <asp:Button ID="btnsitesearch" runat="server" Text="Search" CausesValidation ="false"
                                        onclick="btnsitesearch_Click" />
                             <%--   </td>
                                <td width="65%">
                                    &nbsp;
                                </td>--%>
                            
           <%--     <asp:Button ID="btndownload" runat="server" Text="Download Data" OnClick="btndownload_Click" />
                &nbsp; &nbsp; &nbsp;
                <asp:FileUpload ID="FileUpload1" runat="server" />
                &nbsp;<asp:Button ID="Button2" runat="server" BackColor="#DE2021" Font-Bold="True"
                    ForeColor="White" Text="Upload Bulk Data" OnClick="Button2_Click" />--%>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="up1" runat="server">
       <ContentTemplate>
    <div style="width: 1020px; overflow: auto; height: 300px">
        <asp:GridView ID="grd" runat="server" GridLines="Both" CellPadding="3" AutoGenerateColumns="false"
            BackColor="White" BorderColor="Black" EmptyDataText="No Details Found..!" OnRowDataBound="grd_RowDataBound2"
            OnRowCommand="grd_RowCommand">
            <Columns>
                <%--  <asp:BoundField DataField="Circle" HeaderText="Circle" />
                            <asp:BoundField DataField="Zone" HeaderText="Zone" />
                            <asp:BoundField DataField="SiteId" HeaderText="Site ID" />
                            <asp:BoundField DataField="Inspection_Date" HeaderText="Inspection Date" />
                            <asp:BoundField DataField="IP_ID" HeaderText="IP ID" />
                            <asp:BoundField DataField="Inspector" HeaderText="Inspector" />--%>
                <%-- <asp:BoundField DataField="Description" HeaderText="Description" />
                                                                <asp:BoundField DataField="Rate" HeaderText="Rate" />--%>
            <asp:TemplateField ItemStyle-BackColor="Khaki">
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server"  ToolTip='<%# Eval("id")+ "," + Eval("siteid") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
               <%--     <asp:BoundField DataField="Last_Status1" HeaderText="Action Status On Previous Inspection" />--%>
                <asp:BoundField DataField="siteid" HeaderText="siteid" ItemStyle-BackColor="SkyBlue" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Black" />

                  <asp:BoundField DataField="nss_id" HeaderText="NodeId" ItemStyle-BackColor="SkyBlue" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="Black" />
                 <asp:BoundField DataField="Punchpointreference" HeaderText="Punch point reference" />
                 <asp:BoundField DataField="question" HeaderText="Question" ItemStyle-ForeColor="DarkBlue" /> 
                 <asp:BoundField DataField="Status" HeaderText="Status" Visible="false" />
                <asp:BoundField DataField="step" HeaderText="Step" />
                <asp:BoundField DataField="Inspection_counter" HeaderText="Inspection Counter" Visible="false" />

                
              <%--  <asp:BoundField DataField="qno" HeaderText="Question No" />
                <asp:TemplateField HeaderText="Question Details">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkque" runat="server" Text='<%# Eval("Question_Name") %>' ToolTip='<%# Eval("file_") %>'
                            CommandName="SetURL" CommandArgument='<%# DataBinder.Eval (Container.DataItem, "file_") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Criticality" HeaderText="Last Inspection Criticality" />
                <asp:BoundField DataField="remarks" HeaderText="Remarks of Inspector" />--%>
            
                   <asp:TemplateField HeaderText="Action Remark">
                    <ItemTemplate>
                        <asp:TextBox ID="txtperson" runat="server" CssClass="genall" Width="100px" Enabled="False"
                            Text='<%# Eval("Risk_Mitigity") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
             
             
                <asp:TemplateField HeaderText="Action Status">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlActionStatus" runat="server" Width="100px" CssClass="genall"
                            Enabled="False">
                            <asp:ListItem>---Select---</asp:ListItem>
                            <asp:ListItem>Open</asp:ListItem>
                            <asp:ListItem>Close</asp:ListItem>
                            <asp:ListItem>WIP</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderText="Upload">
                    <ItemTemplate>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="genall" Enabled="False" accept=".png" />
                        <asp:RegularExpressionValidator ID="RegExValFileUploadFileType" runat="server"
                        ControlToValidate="FileUpload1"
                        ErrorMessage="Only .png Files are allowed" Font-Bold="True"
                        Font-Size="Medium"
                        ValidationExpression="(.*?)\.(png|PNG)$"></asp:RegularExpressionValidator>
                      <%--  <asp:Button ID="bt_upload" runat="server" EnableViewState="False" CssClass="genall"
                            Text="Upload" CommandName="Upload"/>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BorderColor="Black" BorderStyle="Inset" Wrap="false" />
            <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="14px" ForeColor="White" Height="20px" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
        </asp:GridView>
    </div>
     </ContentTemplate></asp:UpdatePanel>
      
     <div style="text-align: center">
        <asp:Panel ID="pn" runat="server" Width="310px" Height="300px" ScrollBars="Auto">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
       <ContentTemplate>
            <asp:Image ID="Image1" runat="server" ImageAlign="Middle" Width="250px" Height="250px" />
            <asp:ImageButton ID="btnClose" runat="server" Text="Close" ImageUrl="~/icons/Windows-Close-Program-icon.png"  OnClientClick="HideModal()"/>
           </ContentTemplate></asp:UpdatePanel>
        </asp:Panel>
    </div>
    
     
     <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>
    <asp:LinkButton ID="lnkk" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="md1" runat="server" TargetControlID="lnkk" PopupControlID="pn"
        BackgroundCssClass="modalBackground"   BehaviorID="modalPopupBehavior">
    </cc1:ModalPopupExtender>
</asp:Content>
