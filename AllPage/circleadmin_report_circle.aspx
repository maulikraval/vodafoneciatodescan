<%@ Page Language="C#" MasterPageFile="~/AllPage/AllCommon.master" AutoEventWireup="true"
    CodeFile="circleadmin_report_circle.aspx.cs" Inherits="circle_admin_report_circle" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<style type="text/css">
 .style1
        {
            width: 100%;
        }
        .style1
        {
            width: 762px;
        }
        .style2
        {
            width: 269px;
        }
        .style3
        {
            width: 269px;
            height: 26px;
        }
        </style>
        <link href="../admin/styleinner1.css" rel="stylesheet" type="text/css" />
        <link href="../css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <div>
        <div class="sidebar">
            <div class="gadget">
                <table class="style2">
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label1" runat="server" Font-Bold="False" Text="Circle Name" CssClass="lblall"></asp:Label>
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                    </tr>
                    <tr>
                        <td class="style3">
                            <asp:DropDownList ID="drpcircle" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpcircle_SelectedIndexChanged1"
                                Width="200px" CssClass="genall">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="drpcircle"
                                ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label7" runat="server" Font-Bold="False" Text="Select Sheet" CssClass="lblall"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:DropDownList ID="ddlsheet" runat="server" AutoPostBack="true" Width="200px"
                                CssClass="genall" OnSelectedIndexChanged="ddlsheet_SelectedIndexChanged">
                                <asp:ListItem>Select</asp:ListItem>
                                <asp:ListItem>ISIC V2</asp:ListItem>
                                <asp:ListItem>BSIC V5</asp:ListItem>
                                <asp:ListItem>MSIC V3</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlsheet"
                                ErrorMessage="*" InitialValue="Select" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label2" runat="server" Font-Bold="False" Text="Category Details" CssClass="lblall"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:DropDownList ID="ddlcategory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged"
                                Width="200px" CssClass="genall" CausesValidation="True">
                                <asp:ListItem>Select Category</asp:ListItem>
                                <asp:ListItem>Date Wise</asp:ListItem>
                                <asp:ListItem>Technician Wise</asp:ListItem>
                                <asp:ListItem>Percentage Wise</asp:ListItem>
                                <asp:ListItem>Status Wise</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style2">
                            <asp:MultiView ID="MultiView1" runat="server">
                                <asp:View ID="View1" runat="server">
                                    <table class="style2">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" runat="server" Font-Bold="False" Text="Select Dates" CssClass="lblall"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table class="style2">
                                                    <tr>
                                                        <td style="text-align: right" width="30%">
                                                            <asp:Label ID="Label4" runat="server" Font-Bold="False" Text="From : " CssClass="lblall"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="genall" Width="100px"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: right" width="30%">
                                                            <asp:Label ID="Label5" runat="server" Font-Bold="False" Text="To : " CssClass="lblall"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="genall" Width="100px"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="TextBox2">
                                                            </cc1:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" Text="Search" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center">
                                                <asp:Label ID="Label10" runat="server" Font-Names="Calibri" ></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <table class="style2">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" Font-Bold="False" Text="Select Technician Name"
                                                    CssClass="lblall"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddltechnicianname" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drptechnician_SelectedIndexChanged"
                                                    Width="200px" CssClass="genall">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label11" runat="server" Font-Bold="False" Text="Select Status" CssClass="lblall"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlstatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstatus_SelectedIndexChanged"
                                                    Width="200px" CssClass="genall">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                    <asp:ListItem>Completed</asp:ListItem>
                                                    <asp:ListItem>Not Completed</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View3" runat="server">
                                    <table class="style2">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label9" runat="server" Font-Bold="False" Text="Select Percentage"
                                                    CssClass="lblall"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlpercentage" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlpercentage_SelectedIndexChanged"
                                                    Width="200px" CssClass="genall">
                                                    <asp:ListItem>Select Percentage</asp:ListItem>
                                                    <asp:ListItem>Less Than 50 %</asp:ListItem>
                                                    <asp:ListItem>Less Than 70 % And More Than 50 %</asp:ListItem>
                                                    <asp:ListItem>Less Than 90 % And More Than 70 %</asp:ListItem>
                                                    <asp:ListItem>More Than 90 %</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View4" runat="server">
                                    <table class="style2">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label8" runat="server" Font-Bold="False" Text="Select Status" CssClass="lblall"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlstatusall" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlstatusall_SelectedIndexChanged"
                                                    Width="200px" CssClass="genall">
                                                    <asp:ListItem>Select</asp:ListItem>
                                                    <asp:ListItem>Completed</asp:ListItem>
                                                    <asp:ListItem>Not Completed</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div>
            <table width="100%" class="style2">
                
                <tr>
                    <td width="5%" style="text-align: center">
                        <asp:Label ID="rptlable" runat="server" Font-Bold="False" CssClass="lblstly"></asp:Label>
                    </td>
                    <td width="65%" style="text-align: center">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="5%" style="text-align: center">
                       <hr/>
                    </td>
                </tr>
                <tr>
               
                    <td style="vertical-align: top;" width="35%">
                        <div>
                            <table class="style2">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="ImageButton1" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                            OnClick="ImageButton1_Click" ToolTip="Export To Excel" Width="25px" />
                                        <asp:Panel ID="Panel1" runat="server" ScrollBars="Both" Width="720px" Height="500px">
                                            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"
                                                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                                ForeColor="Black" Width="100%" OnRowDataBound="GridView1_RowDataBound1">
                                                <FooterStyle BackColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                                <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <center>
                                            <asp:Label ID="rptlable0" runat="server" Font-Bold="False" CssClass="lblstly">Criticality 
                                                            Report</asp:Label></center>
                                        <hr />
                                        <asp:ImageButton ID="ImageButton2" runat="server" Height="22px" ImageUrl="~/images/page_excel.png"
                                            OnClick="ImageButton2_Click" ToolTip="Export To Excel" Width="25px" />
                                        <asp:Panel ID="Panel2" runat="server" ScrollBars="Both" Width="720px" Height="500px">
                                            <asp:GridView ID="GridView2" runat="server" BackColor="White" BorderColor="#999999"
                                                BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" EmptyDataText="No Details Found..!"
                                                ForeColor="Black" Width="100%" OnRowDataBound="GridView2_RowDataBound1">
                                                <FooterStyle BackColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="Black" />
                                                 <HeaderStyle BackColor="#FFA500" Wrap="true" Font-Size="13px" ForeColor="White" />
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td width="5%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <div class="clr">
        </div>
    </div>--%>
</asp:Content>
