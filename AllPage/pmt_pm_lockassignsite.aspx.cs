﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using businessaccesslayer;
using InfoSoftGlobal;
using mybusiness;
using dataaccesslayer2;

public partial class PMT_pm_lockassignsite : System.Web.UI.Page
{
    mydataaccess1 da1;
    mydataaccess2 da;
    DataTable dt;

    string sp_user_d;
    private void Page_PreRender(object sender, System.EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.Now);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        string strPreviousPage = "";
        if (Request.UrlReferrer != null)
        {
            strPreviousPage = Request.UrlReferrer.Segments[Request.UrlReferrer.Segments.Length - 1];
        }
        if (strPreviousPage == "")
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

        //  lblError.Visible = false;
        da1 = new mydataaccess1();
        string sp_user_ = da1.select_user_cookie(Session["user"].ToString());
        try
        {
            da1 = new mydataaccess1();
            sp_user_d = da1.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);


            if (!IsPostBack)
            {
                try
                {
                    //ddlCheckSheet.Visible = false;
                    //  ddlprovidername.Visible = false;
                    Label13.Visible = false;
                    ddlzone.Visible = false;
                    //Label7.Visible = false;
                    //ImageButton1.Visible = false;
                    //  ImageButton2.Visible = false;
                    int i = 12;
                    if (Convert.ToInt32(Session["role"].ToString()) == i && Session["um"].ToString() == sp_user_)
                    {

                    }
                    else
                    {
                        Session.Clear();
                        Session.Abandon();
                        Session.RemoveAll();

                        if (Request.Cookies["ASP.NET_SessionId"] != null)
                        {
                            Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                            Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                        }

                        if (Request.Cookies["AuthToken"] != null)
                        {
                            Response.Cookies["AuthToken"].Value = string.Empty;
                            Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                        }
                        Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                        Response.End();
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }

            try
            {
                if (Session["user"].ToString() == "user")
                { }

                if (!IsPostBack)
                {

                    if (Session["flag"].ToString() == "4")
                    {
                        lnkchangeproject.Visible = true;
                    }
                    else
                    {
                        lnkchangeproject.Visible = false;
                    }

                    // marque Start
                    int count = 0;
                    int count1 = 0;
                    int count2 = 0;
                    //da1= new mydataaccess1();
                    //dt = new DataTable();
                    //string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                    da1 = new mydataaccess1();
                    DataTable dtv5 = new DataTable();
                    dtv5 = da1.reminder_20_days_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2 = new DataTable();
                    dtv2 = da1.reminder_20_days_circle_admin_v2(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2ms = new DataTable();
                    dtv2ms = da1.reminder_20_days_circle_admin_v2ms(sp_user_d);
                    count = dtv5.Rows.Count + dtv2.Rows.Count + dtv2ms.Rows.Count;

                    // critical points
                    da1 = new mydataaccess1();
                    DataTable cv2 = new DataTable();
                    cv2 = da1.reminder_20_days_critical_v2_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable cv5 = new DataTable();
                    cv5 = da1.reminder_20_days_critical_v5_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable cv2ms = new DataTable();
                    cv2ms = da1.reminder_20_days_critical_v2ms_circle_admin(sp_user_d);
                    count1 = cv2.Rows.Count + cv5.Rows.Count + cv2ms.Rows.Count;

                    da1 = new mydataaccess1();
                    DataTable dtv51 = new DataTable();
                    dtv51 = da1.reminder_10_days_circle_admin(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv21 = new DataTable();
                    dtv21 = da1.reminder_10_days_circle_admin_v2(sp_user_d);
                    da1 = new mydataaccess1();
                    DataTable dtv2ms1 = new DataTable();
                    dtv2ms1 = da1.reminder_10_days_circle_admin_v2ms(sp_user_d);
                    count2 = dtv51.Rows.Count + dtv21.Rows.Count + dtv2ms1.Rows.Count;
                    // lblmarquee.Text = "Sites to be inspected within 10 Days (<span class=lblmarqueespan>" + count + "</span>), Sites to be inspected within Extended 10 Days (<span class=lblmarqueespan>" + count2 + "</span>), Sites are critical because of delayed in the submission (<span class=lblmarqueespan>" + count1 + "</span>)";

                    //Marquee End

                    //if (Session.Count != 0)
                    {
                        if (Session["role"].ToString() == "12")
                        {
                            if (sp_user_d == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            myvodav2 vb = new myvodav2();
                            da1 = new mydataaccess1();
                            DataTable zone = new DataTable();

                            zone = da1.pm_getzonesbycirclename(sp_user_d);
                            if (zone.Rows.Count > 0)
                            {
                                ddlzone.Items.Clear();
                                ddlzone.DataSource = zone;
                                ddlzone.DataTextField = "zone";
                                ddlzone.DataBind();
                                ddlzone.Items.Insert(0, "Select");
                                // ddlzone.Items.Insert(1, "All");
                                Label13.Visible = true;
                                ddlzone.Visible = true;
                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                            else
                            {
                                ddlzone.Items.Clear();
                                ddlzone.DataSource = null;
                                ddlzone.DataBind();
                                ddlzone.Items.Insert(0, "Select");
                                Label13.Visible = true;
                                ddlzone.Visible = true;
                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }

                        if (Session["role"].ToString() == "2")
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da1 = new mydataaccess1();
            string r = da1.select_user_cookie(Session["user"].ToString());


            da1 = new mydataaccess1();

            da1.update_user_master_status(r);

            Session.Abandon();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            //Session["user"] = "Logout";

            Response.Redirect("~/login.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void ddlzone_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlzone.SelectedIndex > 0)
            {
                da1 = new mydataaccess1();
                dt = new DataTable();
                dt = da1.pm_sitedata(ddlzone.SelectedItem.Text);
                if (dt.Rows.Count > 0)
                {
                    message.Visible = false;
                    message.Text = "";
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
                else
                {
                    message.Visible = false;
                    message.Text = "";
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }
            }
            else
            {
                message.Visible = false;
                message.Text = "";
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
    }
    //protected void LinkButton1_Click(object sender, EventArgs e)
    //{

    //    LinkButton lnkView = (sender as LinkButton);
    //    GridViewRow row = (lnkView.NamingContainer as GridViewRow);
    //    int id = Convert.ToInt32(lnkView.CommandArgument);
    //    string LockUnLock = lnkView.Text;
    //    int LockUnLock_ = 0;
    //    if (LockUnLock == "Lock")
    //    {
    //        LockUnLock_ = 0;
    //    }
    //    else if (LockUnLock == "UnLock")
    //    {
    //        LockUnLock_ = 1;
    //    }
    //    try
    //    {
    //        da1 = new mydataaccess1();
    //        da1.pm_UpdateLockUnLock(id, LockUnLock_);
    //        #region Reload Grid
    //        da1 = new mydataaccess1();
    //        dt = new DataTable();
    //        dt = da1.pm_sitedata(ddlzone.SelectedItem.Text);
    //        if (dt.Rows.Count > 0)
    //        {
    //            GridView1.DataSource = dt;
    //            GridView1.DataBind();
    //        }
    //        else
    //        {
    //            GridView1.DataSource = null;
    //            GridView1.DataBind();
    //        }
    //        #endregion
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        try
        {
            message.Visible = false;
            message.Text = "";
            string data = "";
            foreach (GridViewRow row in GridView1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkRow = (row.Cells[0].FindControl("LinkButton1") as CheckBox);
                    if (chkRow.Checked)
                    {
                        string Siteid_N = row.Cells[1].Text.ToString();
                        da1 = new mydataaccess1();
                        da1.pm_UpdateLockAndUnLock(Siteid_N, "Lock");
                        #region Re Bind Data
                        if (ddlzone.SelectedIndex > 0)
                        {
                            da1 = new mydataaccess1();
                            dt = new DataTable();
                            dt = da1.pm_sitedata(ddlzone.SelectedItem.Text);
                            if (dt.Rows.Count > 0)
                            {
                                GridView1.DataSource = dt;
                                GridView1.DataBind();
                            }
                            else
                            {
                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }
                        else
                        {
                            GridView1.DataSource = null;
                            GridView1.DataBind();
                        }
                        #endregion
                    }
                    else
                    {
                        string Siteid_Na = row.Cells[1].Text.ToString();
                        da1 = new mydataaccess1();
                        da1.pm_UpdateLockAndUnLock(Siteid_Na, "UnLock");
                        #region Re Bind Data
                        if (ddlzone.SelectedIndex > 0)
                        {
                            da1 = new mydataaccess1();
                            dt = new DataTable();
                            dt = da1.pm_sitedata(ddlzone.SelectedItem.Text);
                            if (dt.Rows.Count > 0)
                            {
                                GridView1.DataSource = dt;
                                GridView1.DataBind();
                            }
                            else
                            {
                                GridView1.DataSource = null;
                                GridView1.DataBind();
                            }
                        }
                        else
                        {
                            GridView1.DataSource = null;
                            GridView1.DataBind();
                        }
                        #endregion
                    }
                }
            }
            message.Visible = true;
            message.Text = "Successfuly LockUnLock..";
        }
        catch (Exception ex)
        {
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string siteid = e.Row.Cells[1].Text;
            CheckBox ddl = (CheckBox)e.Row.FindControl("LinkButton1");
            da1 = new mydataaccess1();
            dt = new DataTable();
            dt = da1.pm_siteckecklockUnlock(siteid);
            if (dt.Rows.Count > 0)
            {
                int islockunlock = Convert.ToInt32(dt.Rows[0]["lockunlock"]);
                if (islockunlock == 1)
                {
                    ddl.Checked = true;
                }
                else
                {
                    ddl.Checked = false;
                }
            }

        }
    }
}