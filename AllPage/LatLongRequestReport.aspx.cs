﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using business;
using mybusiness;
using NPOI.HPSF;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel.Contrib;
using NPOI.HSSF.UserModel;
using System.IO;

public partial class AllPage_LatLongRequestReport : System.Web.UI.Page
{
    mydataaccess1 da;
    DataTable dt;
    DataTable circle;

    myvodav2 ba;
    myvodav23 ba1;
    int statusflag;
    HSSFWorkbook hssfworkbook;

    protected void Page_Load(object sender, EventArgs e)
    {
        ImageButton1.Visible = false;
        //GridView1.Visible = false;
        if (!IsPostBack)
        {
            da = new mydataaccess1();
            DataTable circle = new DataTable();
            circle = da.getallcirclename();
            drpcircle.DataSource = circle;
            drpcircle.DataTextField = "circle";
            drpcircle.DataBind();
            drpcircle.Items.Insert(0, "Select");
            drpcircle.Items.Insert(1, "All");
        }
        if (!IsPostBack)
        {
            statusflag = 0;
            try
            {
                ImageButton1.Visible = false;

                int i = 4;
                if (Convert.ToInt32(Session["role"].ToString()) == i)
                {

                }
                else
                {

                    if (Session["role"].ToString() == "2")
                    {
                        Response.Redirect("~/PM/Home.aspx");
                    }
                    if (Session["role"].ToString() == "3")
                    {
                        Response.Redirect("~/circle_admin/Home.aspx");
                    }
                    if (Session["role"].ToString() == "4")
                    {
                        Response.Redirect("~/AllPage/admin_Default3.aspx");
                    }
                    if (Session["role"].ToString() == "5")
                    {
                        Response.Redirect("~/PM/SiteInfonew.aspx");
                    }
                }
            }
            catch
            {

                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");

            }
        }

        try
        {

            // marque Start
            int count = 0;
            int count1 = 0;
            int count2 = 0;
            //  da = new mydataaccess1();
            //   count = da.ptw_dashboard_active_ptw();

            // da = new mydataaccess1();
            // count1 = da.ptw_dashboard_expire_ptw();
            // lblmarquee.Text = "Active PTW (<span class=lblmarqueespan>" + count + "</span>), Expired PTW (<span class=lblmarqueespan>" + count1 + "</span>)";
            da = new mydataaccess1();
            dt = new DataTable();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);

            //Marquee End

            if (Session["user"].ToString() == "user")
            { }

            if (!IsPostBack)
            {
                //            if (Session.Count != 0)
                {
                    /*
                    if (Session["role"].ToString() == "2")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {

                            da = new mydataaccess1();
                            string sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            da = new mydataaccess1();
                            DataTable circle = new DataTable();
                            lblusername.Text = sp_user_d;

                            circle = da.getcirclename();

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                        }
                    }
                     */
                    if (Session["role"].ToString() == "4")
                    {
                        if (drpcircle.SelectedIndex != 0)
                        {
                            da = new mydataaccess1();
                            sp_user_d = da.select_user_cookie(Session["user"].ToString());

                            lblusername.Text = SpacialCharRemove.XSS_Remove(sp_user_d);
                            if (lblusername.Text == "")
                            {
                                // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Your session is expired,please login!!!');</script>");
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Your session is expired!!!');window.location ='../login/Default.aspx';", true);

                            }

                            da = new mydataaccess1();
                            ba = new myvodav2();

                            DataTable circle = new DataTable();

                            ba.User = lblusername.Text;
                            circle = da.getcirclenamefromusername(ba);

                            drpcircle.DataSource = circle;
                            drpcircle.DataTextField = "circle";
                            drpcircle.DataBind();
                            drpcircle.Items.Insert(0, "Select");
                            drpcircle.Items.Insert(1, "All");
                            drpcircle.Items.Insert(2, "North Hub");
                            drpcircle.Items.Insert(3, "East Hub");
                            drpcircle.Items.Insert(4, "South Hub");
                            drpcircle.Items.Insert(5, "West Hub");
                        }
                    }

                    if (Session["role"].ToString() == "2")
                    {
                        //da = new mydataaccess1();
                        //DataTable dt = new DataTable();
                        //dt = da.viewpivotrpt();
                        //GridView1.DataSource = dt;
                        //GridView1.DataBind();



                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
            //Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.ClearContent();
        string FileName = "LatLongReport_" + DateTime.Now + ".xls";
        Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
        Response.ContentType = "application/excel";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        GridView1.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    protected void drpcircle_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (drpcircle.SelectedIndex != 0)
        {
            ImageButton1.Visible = true;
            if (TextBox1.Text != "" && TextBox2.Text != "")
            {
                string first = Convert.ToDateTime(TextBox1.Text).ToString("yyyy-MM-dd HH:mm:ss.fff");
                string second = Convert.ToDateTime(TextBox2.Text).ToString("yyyy-MM-dd 23:59:59.000");

                DateTime first2 = Convert.ToDateTime(TextBox1.Text);
                DateTime second2 = Convert.ToDateTime(TextBox2.Text);

                var monthDiff = Math.Abs((second2.Year * 12 + (second2.Month - 1)) - (first2.Year * 12 + (first2.Month - 1)));

                //  Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + monthDiff + "');</script>");

                if (monthDiff <= 3)
                {
                    if (drpcircle.SelectedItem.Text != "Select")
                    {
                        try
                        {
                            //string first = Convert.ToDateTime((TextBox1.Text)).ToString("yyyy-MM-dd");
                            //string second = Convert.ToDateTime((TextBox2.Text)).ToString("yyyy-MM-dd");

                            //IFormatProvider culture = new CultureInfo("en-US", true);
                            //DateTime from = DateTime.ParseExact(TextBox1.Text, "yyyy-MM-dd", culture);
                            //DateTime to = DateTime.ParseExact(TextBox2.Text, "yyyy-MM-dd", culture);
                            if (drpcircle.SelectedItem.Text == "All")
                            {
                                if (first2.Day > second2.Day || first2.Month > second2.Month || first2.Year > second2.Year)
                                {
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select small date in From than To!!!');</script>");
                                }
                                da = new mydataaccess1();
                                dt = da.LatLongRequestReport("", first, second);
                                GridView1.DataSource = dt;
                                GridView1.DataBind();
                                GridView1.Visible = true;
                                rptlable.Text = "Report from '" + SpacialCharRemove.XSS_Remove(TextBox1.Text) + "' to '" + SpacialCharRemove.XSS_Remove(TextBox2.Text) + "'";
                                ImageButton1.Visible = true;
                                if (dt.Rows.Count <= 0)
                                {
                                    ImageButton1.Visible = false;
                                }
                            }
                            else
                            {
                                if (first2.Day > second2.Day || first2.Month > second2.Month || first2.Year > second2.Year)
                                {
                                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select small date in From than To!!!');</script>");
                                }
                                da = new mydataaccess1();
                                dt = da.LatLongRequestReport(drpcircle.SelectedItem.Text, first, second);
                                GridView1.DataSource = dt;
                                GridView1.DataBind();
                                GridView1.Visible = true;
                                rptlable.Text = "Report from '" + SpacialCharRemove.XSS_Remove(TextBox1.Text) + "' to '" + SpacialCharRemove.XSS_Remove(TextBox2.Text) + "'";
                                ImageButton1.Visible = true;
                                if (dt.Rows.Count <= 0)
                                {
                                    ImageButton1.Visible = false;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        finally
                        {
                            GridView1.DataSource = null;
                        }
                    }
                    else
                    {
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select circle!!!');</script>");
                        GridView1.DataSource = null;
                        GridView1.DataBind();
                        GridView1.Visible = false;
                    }
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please enter Dates having only 3 months difference!!!');</script>");
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                    GridView1.Visible = false;
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select Dates!!!');</script>");
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView1.Visible = false;
                ImageButton1.Visible = false;
            }
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please select circle!!!');</script>");
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.Visible = false;
            rptlable.Text = "";
        }
    }
    protected void lnkchange_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/changepassword/change_password.aspx");
    }
    protected void lnklogout_Click(object sender, EventArgs e)
    {
        try
        {
            da = new mydataaccess1();
            string r = da.select_user_cookie(Session["user"].ToString());


            da = new mydataaccess1();

            da.update_user_master_status(r);

            Session.Clear();
            Session.RemoveAll();
            //Session["user"] = "Logout";

            Response.Redirect("~/login/Default.aspx", false);
        }
        catch
        {

            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }

    }
}
