﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dataaccesslayer;
using business;
using System.Data;
using mybusiness;
using System.Web.Security;

public partial class AllPage_admin_showuser : System.Web.UI.Page
{
    mydataaccess1 da;
    myvodav2 ba;
    DataTable dt;

    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    public static bool TryParseGuid(string s, out Guid guid)
    {
        try
        {
            guid = new Guid(s);
            return true;
        }
        catch
        {
            guid = Guid.Empty;
            return false;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //First, check for the existence of the Anti-XSS cookie
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;

        //If the CSRF cookie is found, parse the token from the cookie.
        //Then, set the global page variable and view state user
        //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
        //method.
        // if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        if (requestCookie != null && TryParseGuid(requestCookie.Value.ToString(), out requestCookieGuidValue))
        {
            //Set the global token variable so the cookie value can be
            //validated against the value in the view state form field in
            //the Page.PreLoad method.
            _antiXsrfTokenValue = requestCookie.Value;

            //Set the view state user key, which will be validated by the
            //framework during each request
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        //If the CSRF cookie is not found, then this is a new session.
        else
        {
            //Generate a new Anti-XSRF token
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

            //Set the view state user key, which will be validated by the
            //framework during each request
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            //Create the non-persistent CSRF cookie
            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                //Set the HttpOnly property to prevent the cookie from
                //being accessed by client side script
                HttpOnly = true,

                //Add the Anti-XSRF token to the cookie value
                Value = _antiXsrfTokenValue
            };

            //If we are using SSL, the cookie should be set to secure to
            //prevent it from being sent over HTTP connections
            if (FormsAuthentication.RequireSSL &&
            Request.IsSecureConnection)
                responseCookie.Secure = true;

            //Add the CSRF cookie to the response
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += Page_PreLoad;
    }

    protected void Page_PreLoad(object sender, EventArgs e)
    {
        //During the initial page load, add the Anti-XSRF token and user
        //name to the ViewState
        if (!IsPostBack)
        {
            //Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;

            //If a user name is assigned, set the user name
            ViewState[AntiXsrfUserNameKey] =
            Context.User.Identity.Name ?? String.Empty;
        }
        //During all subsequent post backs to the page, the token value from
        //the cookie should be validated against the token in the view state
        //form field. Additionally user name should be compared to the
        //authenticated users name
        else
        {
            //Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
            || (string)ViewState[AntiXsrfUserNameKey] !=
            (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }
    }
   

    protected void Page_Load(object sender, EventArgs e)
    {
	 da = new mydataaccess1();
                   string sp_user_ = da.select_user_cookie(Session["user"].ToString());
        if (!IsPostBack)
        {
            try
            {
                int i = 12;

                if (Convert.ToInt32(Session["role"].ToString()) == i && Session["flag"] == "1" && Session["um"].ToString()==sp_user_)
                {
                    try
                    {
                        da = new mydataaccess1();
                        string sp_user_d = da.select_user_cookie(Session["user"].ToString());
                        if (Session["User_Auth_Pmt"].ToString() == sp_user_d)
                        {
                            da = new mydataaccess1();
                            dt = new DataTable();
                            dt = da.pm_showuser_from_user(sp_user_d, "", "withcircle");
                            if (dt.Rows.Count > 0)
                            {
                                grduser.DataSource = dt;
                                grduser.DataBind();
                            }
                            else
                            {
                                grduser.DataSource = null;
                                grduser.DataBind();
                            }
                        }
                        else
                        {
                            Session.Clear();
                            Session.Abandon();
                            Session.RemoveAll();
                            Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                        }
                    }
                    catch
                    {
                        Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                    }
                }
                else
                {
                    Session.Clear();
                    Session.Abandon();
                    Session.RemoveAll();

                    if (Request.Cookies["ASP.NET_SessionId"] != null)
                    {
                        Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                        Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                    }

                    if (Request.Cookies["AuthToken"] != null)
                    {
                        Response.Cookies["AuthToken"].Value = string.Empty;
                        Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
                    }
                    Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                    Response.End();
                    Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
                }
            }
            catch
            {
                Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
            }
        }
    }
    //public string encode(string lbl)
    //{
    //    byte[] enc = new byte[lbl.Length];
    //    enc = System.Text.Encoding.UTF8.GetBytes(lbl);
    //    string encoded_data = Convert.ToBase64String(enc);
    //    return encoded_data;
    //}
    //public string decode(string encoded)
    //{
    //    System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

    //    System.Text.Decoder utf8Decode = encoder.GetDecoder();

    //    byte[] todecode_byte = Convert.FromBase64String(encoded);

    //    int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

    //    char[] decoded_char = new char[charCount];

    //    utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

    //    string result = new String(decoded_char);

    //    return result;

    //}
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        if (TextBox1.Text != "")
        {
            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.pm_showuser_from_user(sp_user_d, TextBox1.Text, "withuser");
            if (dt.Rows.Count > 0)
            {
                grduser.DataSource = dt;
                grduser.DataBind();
            }
            else
            {
                grduser.DataSource = null;
                grduser.DataBind();
            }
        }
        else
        {

            da = new mydataaccess1();
            string sp_user_d = da.select_user_cookie(Session["user"].ToString());
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.pm_showuser_from_user(sp_user_d, "", "withcircle");
            if (dt.Rows.Count > 0)
            {
                grduser.DataSource = dt;
                grduser.DataBind();
            }
            else
            {
                grduser.DataSource = null;
                grduser.DataBind();
            }
            // Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Enter Username!!!');</script>");
        }
    }
    protected void ImageButton1_Click1(object sender, ImageClickEventArgs e)
    {
        Response.Clear();
        Response.AddHeader("content-disposition", "attachment; filename=Report.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        grduser.RenderControl(htmlWrite);
        Response.Write(stringWrite.ToString());
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
    private void Page_PreRender(object sender, System.EventArgs e)
    {
    }
}
