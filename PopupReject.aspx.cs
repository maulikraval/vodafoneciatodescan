﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer;
using FindDataAccessLayer;
using business;
using System.Data;
using System.Data.SqlClient;

public partial class PopupReject : System.Web.UI.Page
{
    //SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    //SqlCommand cmd2;
    //SqlDataAdapter da2;
    DataSet ds;
    mydataaccess1 da;
    int id;
    string Siteid;
    string Username;
    string NewLat;
    string NewLong;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
                Siteid = Request.QueryString["siteid"].ToString();
                Username = Request.QueryString["username"].ToString();
                NewLat = Request.QueryString["newlat"].ToString();
                NewLong = Request.QueryString["newlong"].ToString();

                FindDataAccess DA = new FindDataAccess();
                //string str = "select top 1 id, flag, Action_Status, closedby from tt_master where siteid='" + Siteid + "' order by id desc";
                //cmd = new SqlCommand(str, connection);
                //da = new SqlDataAdapter(cmd);
                //ds = new DataSet();
                //da.Fill(ds);
                da = new mydataaccess1();
                ds = new DataSet();
                ds = da.CheckCloseBeforeApprove_Reject(Convert.ToInt32(id), Siteid);

                int Id = Convert.ToInt32(ds.Tables[0].Rows[0]["id"].ToString());
                string CloseBy = ds.Tables[0].Rows[0]["closedby"].ToString();
                int res = Convert.ToInt32(ds.Tables[0].Rows[0]["flag"].ToString());
                string res2 = ds.Tables[0].Rows[0]["Action_Status"].ToString();

                if ((res == 1) && (res2 == "Rejected by CIAT Helpdesk"))
                {
                    //Response.Write("<script type='text/javascript'>alert('Rejected successfully!!!');</script>");
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Rejected successfully!!!');</script>");
                    //cmd2 = new SqlCommand("update tt_master set Action_Status='Rejected by circleadmin' where id='" + Id + "'", connection);
                    //connection.Open();
                    //cmd2.ExecuteNonQuery();
                    //connection.Close();
                    mydataaccess1 PopupDA = new mydataaccess1();
                    //DataTable PopupDT = new DataTable();
                    PopupDA.Popup_Reject_Approved(Id);
                }
                else if ((res == 1) && (res2 == "Approved by circleadmin"))
                {
                    Response.Redirect("https://ciat.vodafone.in/PopupApprove.aspx?id=" + SpacialCharRemove.XSS_Remove(id.ToString()) + "&siteid=" + SpacialCharRemove.XSS_Remove(Siteid) + "&username=" + SpacialCharRemove.XSS_Remove(Username) + "&newlat=" + SpacialCharRemove.XSS_Remove(NewLat) + "&newlong=" + SpacialCharRemove.XSS_Remove(NewLong) + "");
                    //Response.Write("<script type='text/javascript'>alert('Action has already taken on this mail !!!');</script>");
                }
                else if ((res == 1) && (res2 == "Rejected by circleadmin"))
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action has already taken on this mail!!!');</script>");
                    //Response.Write("<script type='text/javascript'>alert('Action has already taken on this mail!!!');</script>");
                }
                else if ((res == 1) && (res2 == "Closed by User"))
                {
                    Response.Write("<script type='text/javascript'>alert('Action has already taken on this mail!!!');</script>");
                }
                else
                {
                    Response.Write("<script type='text/javascript'>alert('Action has already taken on this mail !!!');</script>");
                }
            }
            catch
            {

            }
        }
    }
}