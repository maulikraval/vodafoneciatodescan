﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Net;
using dataaccesslayer;


/// <summary>
/// Summary description for mail
/// </summary>
public class mail
{
    public mail()
    {


    }
    public void mail_fun(DataTable dt, DataTable maildt, string user, DataTable s_dt1, DataTable s_dt2, string score)
    {
        for (int j = 0; j < maildt.Rows.Count; j++)
        {
            if (dt.Rows.Count > 0)
            {
                string MailBo = " <table style=\"border-width: medium; border-color: #FF0000; width: 100%;\">" +
                   "<tr>" +
                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Circle</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Zone</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site ID</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site Name</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site Address</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Inspector name</b>" +
                       "</td>" +

                         "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Inspector Mobile number</b>" +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Date & Time</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>IP name</b>" +
                       "</td>" +


                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Check Sheet</b>" +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site on Air date</b>" +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Achieved Score</b>" +
                       "</td>" +

                   "</tr>" +
                   "<tr>" +
                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         SendMail.ishtml(s_dt1.Rows[0][0].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][1].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][2].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][3].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][4].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         SendMail.ishtml(user) +
                       "</td>" +

                         "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt2.Rows[0][0].ToString()) +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                        System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute + ":" + System.DateTime.Now.Second +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][5].ToString()) +
                       "</td>" +


 "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][7].ToString()) +
                       "</td>" + "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][6].ToString()) +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         SendMail.ishtml(score) + " %" +
                       "</td>" +

                   "</tr>" +

                  "</table>";

                string MailBody = " <table style=\"border-width: medium; border-color: #FF0000; width: 100%;\">" +
                    "<tr>" +
                        "<td style=\"border: thin solid #FF0000; width:70%\" >" +
                          "<b>Question Name</b>" +
                        "</td>" +

                         "<td style=\"border: thin solid #FF0000; width:30%\" >" +
                          "<b>Remarks</b>" +
                        "</td>" +

                    "</tr>";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MailBody += "<tr>" +
                                               "<td style=\"border: thin solid #FF0000; width:70%\" >" +
                                                 SendMail.ishtml(dt.Rows[i][0].ToString()) +
                                               "</td>" +

                                               "<td style=\"border: thin solid #FF0000; width:30%\" >" +
                                                 SendMail.ishtml(dt.Rows[i][1].ToString()) +
                                               "</td>" +
                                               "</tr>";
                }
                MailBody += "</table>";

                string subject = "Critical A identified @ " + SendMail.ishtml(s_dt1.Rows[0][3].ToString()) + "_" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + "_" + SendMail.ishtml(user);
                string body = "Hi,<br />Critical A identified  @ site !<br />Please take necessary action.<br/><br/>" + MailBo + "<br/><br/> Below questions have Criticality A :" + MailBody + "<br /> <br /> Regards, <br />CIAT Team ";
                string tomail = maildt.Rows[j][0].ToString();
                SendMail sm = new SendMail();
                sm.mailsend(tomail, subject, body);







                //    MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", maildt.Rows[j][0].ToString(), "Critical A identified @ " + s_dt1.Rows[0][3].ToString() + "_" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + "_" + user, "Hi,<br />Critical A identified  @ site !<br />Please take necessary action.<br/><br/>" + MailBo + "<br/><br/> Below questions have Criticality A :" + MailBody + "<br /> <br /> Regards, <br />CIAT Team ");


                //    message.IsBodyHtml = true;
                //    SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

                //    emailClient.Credentials =
                //    new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

                //    emailClient.EnableSsl =
                //    false;

                //    emailClient.Send(message);

                //  /*    MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", maildt.Rows[j][0].ToString(), "Critical A identified @ " + s_dt1.Rows[0][3].ToString() + "_" + System.DateTime.Now.Day + System.DateTime.Now.Month + System.DateTime.Now.Year + "_" + user, "Hi,<br />Critical A identified  @ site !<br />Please take necessary action.<br/><br/>" + MailBo + "<br/><br/> Below questions have Criticality A :" + MailBody + "<br /> <br /> Regards, <br />CIAT Team ");


                //    message.IsBodyHtml = true;

                //   SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
                //emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
                //emailClient.EnableSsl = true;
                //    emailClient.Send(message);*/
            }
        }
    }

    public void mail_zm(DataTable maildt, string user, DataTable s_dt1, DataTable s_dt2)
    {
        for (int j = 0; j < maildt.Rows.Count; j++)
        {

            {
                string MailBo = " <table style=\"border-width: medium; border-color: #FF0000; width: 100%;\">" +
                   "<tr>" +
                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Circle</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Zone</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site ID</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site Name</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site Address</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Inspector name</b>" +
                       "</td>" +

                         "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Inspector Mobile number</b>" +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Date & Time</b>" +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>IP name</b>" +
                       "</td>" +


                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Check Sheet</b>" +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         "<b>Site on Air date</b>" +
                       "</td>" +



                   "</tr>" +
                   "<tr>" +
                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         SendMail.ishtml(s_dt1.Rows[0][0].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][1].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][2].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][3].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][4].ToString()) +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                         SendMail.ishtml(user) +
                       "</td>" +

                         "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt2.Rows[0][0].ToString()) +
                       "</td>" +

                       "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                        System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute + ":" + System.DateTime.Now.Second +
                       "</td>" +

                        "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][5].ToString()) +
                       "</td>" +


 "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][7].ToString()) +
                       "</td>" + "<td style=\"border: thin solid #FF0000; width:8%\" >" +
                          SendMail.ishtml(s_dt1.Rows[0][6].ToString()) +
                       "</td>" +



                   "</tr>" +

                  "</table>";

                string subject = "Important : PTW is not taken for this activity-" + SendMail.ishtml(s_dt1.Rows[0][2].ToString()) + "_" + SendMail.ishtml(user);
                string body = "Hi,<br />PTW is not taken for this below mentioned G-PM activity , please take necessary action.!<br /><br/>" + MailBo + "<br/><br/>  <br /> Regards, <br />CIAT Team ";
                string tomail = maildt.Rows[j][2].ToString();
                SendMail sm = new SendMail();
                sm.mailsend(tomail, subject, body);

                //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", maildt.Rows[j][2].ToString(), "Important : PTW is not taken for this activity-" + s_dt1.Rows[0][2].ToString() + "_" + user, "Hi,<br />PTW is not taken for this below mentioned G-PM activity , please take necessary action.!<br /><br/>" + MailBo + "<br/><br/>  <br /> Regards, <br />CIAT Team ");


                //message.IsBodyHtml = true;
                //#region "VODAFONE SERVER CREDENTIALS"

                //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);
                //emailClient.Credentials = new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");
                //emailClient.EnableSsl = false;

                //#endregion

                //#region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

                ///*   SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
                //    emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
                //    emailClient.EnableSsl = true;*/

                //#endregion

                //emailClient.Send(message);


            }
        }
    }
}
