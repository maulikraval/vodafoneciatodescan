﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using dataaccesslayer;
using mybusiness;
using System.Data;

[WebService(Namespace = "http://ciat.vodafone.in/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class MyService : System.Web.Services.WebService {
    string finaldoc;
    string gsm;
    string other;
    string umts;
    mydataaccess1 da;
    myvodav2 vb;
    // myvodav23 vb_;
    DataTable dt;
    public MyService () {
    }

    [WebMethod]
    public virtual double Surface(double radius) {
        return radius * radius * Math.PI;
    }
    [WebMethod]
    public virtual void get_questions(string data, string callback)
    {
       // return step;
    }
    [WebMethod]
    public virtual void insert_log_v5(string data, string callback)
    {
      //  return "1";
    }
    [WebMethod]
    public virtual void request_to_recheck(string data, string callback)
    { 
    
    }
    [WebMethod]
    public virtual string insert_log_v2(string que, string ans, string criticality, string remark, string step, string siteid, string username_, string u_c, string check_type)
    {
        return "1";
    }
    [WebMethod]
    public virtual void select_step(string data, string callback)
    {
       // return 0;
    }
    [WebMethod]
    public virtual void updateuniquecode_android(string data, string callback)
    {
       
    }
    [WebMethod]
    public virtual void select_user_pass(string data, string callback)
    {

    }
    [WebMethod]
    public virtual void getcirclename(string data, string callback)
    {
      //  return "1";
    }
    [WebMethod]
    public virtual void getzonesbycirclename(string data, string callback)
    {
        //return "1";
    }
    [WebMethod]
    public virtual void insertintorating_master(string data, string callback)
    {
        //return "1";
    }
    [WebMethod]
    public virtual void get_score(string data, string callback)
    {
       // return "1";
    }
    [WebMethod]
    public virtual void viewsite(string data, string callback)
    {
       // return "1";
    }
    [WebMethod]
    public virtual void viewsite_inspected(string data, string callback)
    {
       // return "1";
    }
    [WebMethod]
    public virtual void selectchecksheet(string data, string callback)
    {
       // return "1";
    }
    [WebMethod]
    public virtual void selectmaxstep(string data, string callback)
    {
        //return 0;
    }
    [WebMethod]
    public virtual void selectmaxstep_audit(string data, string callback)
    {
        //return 0;
    }
    [WebMethod]
    public virtual void select_photo_validation(string data, string callback)
    {
       // return 0;
    }
    [WebMethod]
    public virtual void insertlatlongfrommobile(string data, string callback)
    {
       // return 0;
    }
    [WebMethod]
    public virtual void getcategory(string data, string callback)
    {
        // return 0;
    }

    [WebMethod]
    public virtual void ptw_viewsitesbydis(string data, string callback)
    {
        // return 0;
    }

    [WebMethod]
    public virtual void updateuniquecode_android_newos(string data, string callback)
    {

    }
}

