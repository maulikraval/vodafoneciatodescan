﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using sqlconnection;
using mybusiness;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for FindDataAccess
/// </summary>
namespace FindDataAccessLayer
{
    public class FindDataAccess
    {
        connection con = new connection();
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataAdapter Adap;
        private SqlDataReader reader;
        public FindDataAccess()
        {
            //Connection con = new Connection();

            this.connection = con.open();
            this.command = new SqlCommand("", this.connection);
            this.command.CommandType = CommandType.StoredProcedure;
            this.command.CommandTimeout = 6000;
        }

        // Haresh
        public DataTable Nss_Nodeid_Search(string siteid, string nssid)
        {
            try
            {
                command.CommandText = "Nss_Nodeid_Search";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@nssid", nssid);
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();
            }
        }
        public DataTable searchsiteid_su(string Siteid)
        {
            try
            {
                command.CommandText = "Supp_Select_Site_Detail";
                command.Parameters.AddWithValue("@siteid", Siteid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable Site_User_Distance_su(string site, string user)
        {
            try
            {
                command.CommandText = "viewsitesbydis_for_map_su";
                command.Parameters.AddWithValue("@site", site);
                command.Parameters.AddWithValue("@user", user);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public void select_update_lat_long_su(float Lat, float Longitude, int Siteid, int flag, string username)
        {
            try
            {
                command.CommandText = "select_update_lat_long_su";
                command.Parameters.AddWithValue("@siteid", Siteid);
                command.Parameters.AddWithValue("@lat", Lat);
                command.Parameters.AddWithValue("@long", Longitude);
                command.Parameters.AddWithValue("@flag", flag);
                command.Parameters.AddWithValue("@username", username);

                reader = command.ExecuteReader();
                double res = 0;
                while (reader.Read())
                {
                    res = Convert.ToDouble(reader[0].ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }

        }
        public int update_nss_on_site(string siteid, string nssid, string user)
        {
            try
            {
                command.CommandText = "update_nss_on_site";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@nssid", nssid);
                command.Parameters.AddWithValue("@updated_by", user);


                reader = command.ExecuteReader();
                int res = 0;
                while (reader.Read())
                {
                    res = Convert.ToInt32(reader[0].ToString());
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }

        }

        public DataTable Support_Find_User_su(string username, string contactno)
        {
            try
            {
                command.CommandText = "Support_Find_User_su";
                command.Parameters.AddWithValue("@username", username);
                command.Parameters.AddWithValue("@contactno", contactno);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public void manual_update_lat_long(string username, string siteid)
        {
            try
            {
                command.CommandText = "manual_update_lat_long";
                command.Parameters.AddWithValue("@username", username);
                command.Parameters.AddWithValue("@siteid", siteid);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }
        }

        public void insert_tt(string project, string siteid, string techname, string Company, string issue_category, string description, DateTime open_time, DateTime close_time, int minute, string status, string closedby, string old_lat, string old_long, string new_lat, string new_long)
        {
            try
            {
                command.CommandText = "insert_tt";
                command.Parameters.AddWithValue("@project", project);
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@techname", techname);
                command.Parameters.AddWithValue("@Company", Company);
                command.Parameters.AddWithValue("@issue_category", issue_category);
                command.Parameters.AddWithValue("@description", description);
                command.Parameters.AddWithValue("@open_time", open_time);
                command.Parameters.AddWithValue("@close_time", close_time);
                command.Parameters.AddWithValue("@minute", minute);
                command.Parameters.AddWithValue("@status", status);
                command.Parameters.AddWithValue("@closedby", closedby);
                command.Parameters.AddWithValue("@old_lat", old_lat);
                command.Parameters.AddWithValue("@old_long", old_long);
                command.Parameters.AddWithValue("@new_lat", new_lat);
                command.Parameters.AddWithValue("@new_long", new_long);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }
        //End Haresh

        public int delete_nodeid(string siteid, string nssid, string user)
        {
            try
            {
                command.CommandText = "delete_nodeid";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@nssid", nssid);
                command.Parameters.AddWithValue("@deleted_by", user);

                reader = command.ExecuteReader();
                int res = 0;
                while (reader.Read())
                {
                    res = Convert.ToInt32(reader[0].ToString());
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }

        }

        #region "Manan"
        public DataTable Select_LatLong_Approval()
        {
            try
            {
                command.CommandText = "Select_LatLong_Approval";
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
        }

        public void INSERT_TT_21042016(string Project, string Circle, string SiteId, string Techname, string Company, string Issue_Category, string Description, string Old_Lat, string Old_Long, string New_Lat, string New_Long, string Main_Compnay, string Sub_Compnay, string Tech_Mobile, float Distance, string Zone, string DistanceBy, string Sitename, string date)
        {
            try
            {
                command.CommandText = "INSERT_TT_21042016";
                command.Parameters.AddWithValue("@PROJECT", Project);
                command.Parameters.AddWithValue("@CIRCLE", Circle);
                command.Parameters.AddWithValue("@SITE_ID", SiteId);
                command.Parameters.AddWithValue("@TECH_NAME", Techname);
                command.Parameters.AddWithValue("@COMPANY", Company);
                command.Parameters.AddWithValue("@ISSUE_CATEGORY", Issue_Category);
                command.Parameters.AddWithValue("@DESCRIPTION", Description);
                command.Parameters.AddWithValue("@OLD_LAT", Old_Lat);
                command.Parameters.AddWithValue("@OLD_LONG", Old_Long);
                command.Parameters.AddWithValue("@NEW_LAT", New_Lat);
                command.Parameters.AddWithValue("@NEW_LONG", New_Long);
                command.Parameters.AddWithValue("@MAIN_COMPANY", Main_Compnay);
                command.Parameters.AddWithValue("@SUB_COMPANY", Sub_Compnay);
                command.Parameters.AddWithValue("@TECHNICIAN_MOBILE", Tech_Mobile);
                command.Parameters.AddWithValue("@DISTANCE", Distance);
                command.Parameters.AddWithValue("@ZONE", Zone);
                command.Parameters.AddWithValue("@DISTANCE_BY", DistanceBy);
                command.Parameters.AddWithValue("@SITE_NAME", Sitename);
                command.Parameters.AddWithValue("@date", Convert.ToDateTime(date));
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        public DataTable CLOSE_TT_22042016(int Id, string CloseBy, string Action_Status)
        {
            try
            {
                command.CommandText = "CLOSE_TT_22042016";
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@CLOSEBY", CloseBy);
                command.Parameters.AddWithValue("@ACTION_STATUS", Action_Status);
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        public DataTable SELECT_ID_SITEID(string Site_Id)
        {
            try
            {
                command.CommandText = "SELECT_ID_SITEID";
                command.Parameters.AddWithValue("@SITE_ID", Site_Id);
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        public DataSet PhotoDeleteMail(string Username, string SiteId, string ChecksheetType, int CategoryId, string ptwid)
        {
            try
            {
                command.CommandText = "PhotoDeleteMail_New";
                command.Parameters.AddWithValue("@USERNAME", Username);
                command.Parameters.AddWithValue("@SITEID", SiteId);
                command.Parameters.AddWithValue("@CHECKSHEETTYPE", ChecksheetType);
                command.Parameters.AddWithValue("@CATEGORYID", CategoryId);
                command.Parameters.AddWithValue("@PTWID", ptwid);
                DataSet DT = new DataSet();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(DT);
                return DT;
            }
            catch (Exception ex)
            {
                DataSet DT = new DataSet();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        #endregion

        #region JAYESH KOSHTI
        public DataTable Select_LatLong_Approval_byuser(string username)
        {
            try
            {
                command.CommandText = "Select_LatLong_Approval_byuser";
                command.Parameters.AddWithValue("@username", username);
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        public DataTable Select_Person_Risk_Master_Request(string circle, string requested_username, string from, string to)
        {
            try
            {
                command.CommandText = "Select_Person_Risk_Master_Request";
                command.Parameters.AddWithValue("@circle", circle);
                command.Parameters.AddWithValue("@requested_username", requested_username);
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }
        #endregion JAYESH KOSHTI
        public int INSERT_TT(string Project, string Circle, string SiteId, string Techname, string Company, string Issue_Category, string Description, string Old_Lat, string Old_Long, string New_Lat, string New_Long, string Main_Compnay, string Sub_Compnay, string Tech_Mobile, float Distance, string Zone, string DistanceBy, string Sitename, string date)
        {
            try
            {
                command.CommandText = "INSERT_TT_21042016";
                command.Parameters.AddWithValue("@PROJECT", Project);
                command.Parameters.AddWithValue("@CIRCLE", Circle);
                command.Parameters.AddWithValue("@SITE_ID", SiteId);
                command.Parameters.AddWithValue("@TECH_NAME", Techname);
                command.Parameters.AddWithValue("@COMPANY", Company);
                command.Parameters.AddWithValue("@ISSUE_CATEGORY", Issue_Category);
                command.Parameters.AddWithValue("@DESCRIPTION", Description);
                command.Parameters.AddWithValue("@OLD_LAT", Old_Lat);
                command.Parameters.AddWithValue("@OLD_LONG", Old_Long);
                command.Parameters.AddWithValue("@NEW_LAT", New_Lat);
                command.Parameters.AddWithValue("@NEW_LONG", New_Long);
                command.Parameters.AddWithValue("@MAIN_COMPANY", Main_Compnay);
                command.Parameters.AddWithValue("@SUB_COMPANY", Sub_Compnay);
                command.Parameters.AddWithValue("@TECHNICIAN_MOBILE", Tech_Mobile);
                command.Parameters.AddWithValue("@DISTANCE", Distance);
                command.Parameters.AddWithValue("@ZONE", Zone);
                command.Parameters.AddWithValue("@DISTANCE_BY", DistanceBy);
                command.Parameters.AddWithValue("@SITE_NAME", Sitename);
                command.Parameters.AddWithValue("@date", Convert.ToDateTime(date));
                reader = command.ExecuteReader();
                int res = 0;
                while (reader.Read())
                {
                    res = Convert.ToInt32(reader[0].ToString());
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        public void update_tt(int id)
        {
            try
            {
                command.CommandText = "update_tt";
                command.Parameters.AddWithValue("@id", id);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }
        public DataTable CLOSE_TT(int Id, string CloseBy, string Action_Status, string remark)
        {
            try
            {
                command.CommandText = "CLOSE_TT";
                command.Parameters.AddWithValue("@ID", Id);
                command.Parameters.AddWithValue("@CLOSEBY", CloseBy);
                command.Parameters.AddWithValue("@ACTION_STATUS", Action_Status);
                command.Parameters.AddWithValue("@remark", remark);
                DataTable DT = new DataTable();
                Adap = new SqlDataAdapter(command);
                //reader = command.ExecuteReader();
                Adap.Fill(DT);
                //DT.Load(reader);
                return DT;
            }
            catch (Exception ex)
            {
                DataTable DT = new DataTable();
                DT = null;
                return DT;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }

        public void update_user_status(string username, int flag)
        {
            try
            {
                command.CommandText = "update_user_status";
                command.Parameters.AddWithValue("@username", username);
                command.Parameters.AddWithValue("@flag", flag);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }
        public int check_user(string username)
        {
            try
            {
                command.CommandText = "check_user";
                command.Parameters.AddWithValue("@username", username);
                reader = command.ExecuteReader();
                int res = 0;
                while (reader.Read())
                {
                    res = Convert.ToInt32(reader[0].ToString());
                }
                return res;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();

            }
        }
    }
}