﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for businessaccess
/// </summary>
///
namespace business
{
    public class vodabal
    {
        private string user;
        private string siteid;
        private double i;
        private double i1;
        private double i2;
        private double i3;
        private double i4;
        private double i5;
        private double i6;
        private int step;
        private string labelvalue;
        private string radvalue;
        private string error;
        private string sitename;
        private string address;
        private double lat;
        private double longitude;
        private string towertype;
        private string technician;
        private string circle;
        private string zone;
        private string subzone;
        private string datetime;
        private string siteidnew;
        private string question;
        private string tech_contact;
        private int stepid;
        private int percentage;
        private DateTime date1;
        private DateTime date2;
        private int status;
        private string val;
        private string criticality;
        private double distance;
        private string imei;
        private string cookie;
        private double accuracy;
        private double accuracynp;
        private double latnp;
        
        public double Latnp
        {
            get { return latnp; }
            set { latnp = value; }
        }
        private double longnp;
        public double Longnp
        {
            get { return longnp; }
            set { longnp = value; }
        }

        public double Accuracynp
        {
            get { return accuracynp; }
            set { accuracynp = value; }
        }
        public double Accuracy
        {
            get { return accuracy; }
            set { accuracy = value; }
        }
        public string Imei
        {
            get { return imei; }
            set { imei = value; }
        }
        public double Distance
        {
            get { return distance; }
            set { distance = value; }
        }
        public string criticality1
        {
            get { return criticality; }
            set { criticality = value; }
        }

        public string Val
        {
            get { return val; }
            set { val = value; }
        }

        public int Status
        {
            get { return status; }
            set { status = value; }
        }


        public DateTime Date2
        {
            get { return date2; }
            set { date2 = value; }
        }

        public DateTime Date1
        {
            get { return date1; }
            set { date1 = value; }
        }


        public int Percentage
        {
            get { return percentage; }
            set { percentage = value; }
        }




        public int Stepid
        {
            get { return stepid; }
            set { stepid = value; }
        }
        public string Tech_contact
        {
            get { return tech_contact; }
            set { tech_contact = value; }
        }

        public string Question
        {
            get { return question; }
            set { question = value; }
        }
        private double rate;
        private string subcat;

        public string Subcat
        {
            get { return subcat; }
            set { subcat = value; }
        }

        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }



        public string Siteidnew
        {
            get { return siteidnew; }
            set { siteidnew = value; }
        }
        public string Datetime
        {
            get { return datetime; }
            set { datetime = value; }
        }

        public string Subzone
        {
            get { return subzone; }
            set { subzone = value; }
        }

        public string Zone
        {
            get { return zone; }
            set { zone = value; }
        }

        public string Circle
        {
            get { return circle; }
            set { circle = value; }
        }

        public string Technician
        {
            get { return technician; }
            set { technician = value; }
        }

        public string Towertype
        {
            get { return towertype; }
            set { towertype = value; }
        }

        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }

        public double Lat
        {
            get { return lat; }
            set { lat = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string Sitename
        {
            get { return sitename; }
            set { sitename = value; }
        }

        public string Error
        {
            get { return error; }
            set { error = value; }
        }

        public string Radvalue
        {
            get { return radvalue; }
            set { radvalue = value; }
        }

        public string Labelvalue
        {
            get { return labelvalue; }
            set { labelvalue = value; }
        }

        public int Step
        {
            get { return step; }
            set { step = value; }
        }

        public double I6
        {
            get { return i6; }
            set { i6 = value; }
        }
        private double i7;

        public double I7
        {
            get { return i7; }
            set { i7 = value; }
        }
        private double i8;

        public double I8
        {
            get { return i8; }
            set { i8 = value; }
        }
        private double i9;

        public double I9
        {
            get { return i9; }
            set { i9 = value; }
        }
        private double i10;

        public double I10
        {
            get { return i10; }
            set { i10 = value; }
        }
        private double i11;

        public double I11
        {
            get { return i11; }
            set { i11 = value; }
        }
        private double i12;

        public double I12
        {
            get { return i12; }
            set { i12 = value; }
        }
        private double i13;

        public double I13
        {
            get { return i13; }
            set { i13 = value; }
        }
        private double i14;

        public double I14
        {
            get { return i14; }
            set { i14 = value; }
        }
        private double i15;

        public double I15
        {
            get { return i15; }
            set { i15 = value; }
        }
        private double i16;

        public double I16
        {
            get { return i16; }
            set { i16 = value; }
        }
        private double i17;

        public double I17
        {
            get { return i17; }
            set { i17 = value; }
        }
        private double i18;

        public double I18
        {
            get { return i18; }
            set { i18 = value; }
        }
        private double i19;

        public double I19
        {
            get { return i19; }
            set { i19 = value; }
        }
        private double i20;

        public double I20
        {
            get { return i20; }
            set { i20 = value; }
        }
        private double i21;

        public double I21
        {
            get { return i21; }
            set { i21 = value; }
        }
        private double i22;

        public double I22
        {
            get { return i22; }
            set { i22 = value; }
        }
        private double i23;

        public double I23
        {
            get { return i23; }
            set { i23 = value; }
        }
        private double i24;

        public double I24
        {
            get { return i24; }
            set { i24 = value; }
        }
        private double i25;
        private double i26;
        private double i27;
        private double i28;
        private double i29;
        private double i30;
        private double i31;
        private double i32;
        private double i33;
        private double i34;
        public double I25
        {
            get { return i25; }
            set { i25 = value; }
        }
        public double I26
        {
            get { return i26; }
            set { i26 = value; }
        }
        public double I27
        {
            get { return i27; }
            set { i27 = value; }
        }
        public double I28
        {
            get { return i28; }
            set { i28 = value; }
        }

        public double I29
        {
            get { return i29; }
            set { i29 = value; }
        }
        public double I30
        {
            get { return i30; }
            set { i30 = value; }
        }
        public double I31
        {
            get { return i31; }
            set { i31 = value; }
        }
        public double I32
        {
            get { return i32; }
            set { i32 = value; }
        }
        public double I33
        {
            get { return i33; }
            set { i33 = value; }

        }
        public double I34
        {
            get { return i34; }
            set { i34 = value; }

        }

        private string remarks;

        public string Remarks1
        {
            get { return remarks; }
            set { remarks = value; }
        }

        private string q;

        public string Q
        {
            get { return q; }
            set { q = value; }
        }
        private string q1;
        private string q2;
        private string q3;
        private string q4;
        private string q5;
        private string q6;
        private string q7;
        private string q8;

        public string Q8
        {
            get { return q8; }
            set { q8 = value; }
        }
        private string q9;

        public string Q9
        {
            get { return q9; }
            set { q9 = value; }
        }
        private string q10;

        public string Q10
        {
            get { return q10; }
            set { q10 = value; }
        }
        private string q11;

        public string Q11
        {
            get { return q11; }
            set { q11 = value; }
        }
        private string q12;

        public string Q12
        {
            get { return q12; }
            set { q12 = value; }
        }
        private string q13;

        public string Q13
        {
            get { return q13; }
            set { q13 = value; }
        }
        private string q14;

        public string Q14
        {
            get { return q14; }
            set { q14 = value; }
        }
        private string q15;

        public string Q15
        {
            get { return q15; }
            set { q15 = value; }
        }
        private string q16;

        public string Q16
        {
            get { return q16; }
            set { q16 = value; }
        }
        private string q17;

        public string Q17
        {
            get { return q17; }
            set { q17 = value; }
        }
        private string q18;

        public string Q18
        {
            get { return q18; }
            set { q18 = value; }
        }
        private string q19;

        public string Q19
        {
            get { return q19; }
            set { q19 = value; }
        }
        private string q20;

        public string Q20
        {
            get { return q20; }
            set { q20 = value; }
        }
        private string q21;

        public string Q21
        {
            get { return q21; }
            set { q21 = value; }
        }
        private string q22;

        public string Q22
        {
            get { return q22; }
            set { q22 = value; }
        }
        private string q23;

        public string Q23
        {
            get { return q23; }
            set { q23 = value; }
        }
        private string q24;

        public string Q24
        {
            get { return q24; }
            set { q24 = value; }
        }

        public string Q5
        {
            get { return q5; }
            set { q5 = value; }
        }

        public string Q4
        {
            get { return q4; }
            set { q4 = value; }
        }

        public string Q3
        {
            get { return q3; }
            set { q3 = value; }
        }

        public string Q2
        {
            get { return q2; }
            set { q2 = value; }
        }

        public string Q1
        {
            get { return q1; }
            set { q1 = value; }
        }

        public string Q6
        {
            get { return q6; }
            set { q6 = value; }
        }
        public string Q7
        {
            get { return q7; }
            set { q7 = value; }
        }
        private string q25;
        private string q26;
        private string q27;
        private string q28;
        private string q29;
        private string q30;
        private string q31;
        private string q32;
        private string q33;
        private string q34;
        public string Q25
        {
            get { return q25; }
            set { q25 = value; }
        }
        public string Q26
        {
            get { return q26; }
            set { q26 = value; }
        }
        public string Q27
        {
            get { return q27; }
            set { q27 = value; }
        }
        public string Q28
        {
            get { return q28; }
            set { q28 = value; }
        }
        public string Q29
        {
            get { return q29; }
            set { q29 = value; }
        }
        public string Q30
        {
            get { return q30; }
            set { q30 = value; }
        }
        public string Q31
        {
            get { return q31; }
            set { q31 = value; }
        }
        public string Q32
        {
            get { return q32; }
            set { q32 = value; }
        }
        public string Q33
        {
            get { return q33; }
            set { q33 = value; }
        }
        public string Q34
        {
            get { return q34; }
            set { q34 = value; }
        }


        public string Remarks
        {
            get { return remarks; }
            set { remarks = value; }
        }

        public double I5
        {
            get { return i5; }
            set { i5 = value; }
        }

        public double I4
        {
            get { return i4; }
            set { i4 = value; }
        }

        public double I3
        {
            get { return i3; }
            set { i3 = value; }
        }

        public double I2
        {
            get { return i2; }
            set { i2 = value; }
        }

        public double I1
        {
            get { return i1; }
            set { i1 = value; }
        }

        public double I
        {
            get { return i; }
            set { i = value; }
        }

        public string Siteid
        {
            get { return siteid; }
            set { siteid = value; }
        }

        public string User
        {
            get { return user; }
            set { user = value; }
        }

        public string Cookie
        {
            get { return cookie; }
            set { cookie = value; }
        }

    }
}
