﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for connection
/// </summary>
/// 
namespace sqlconnection
{
    public class connection
    {
        private SqlConnection Connection;
        public connection()
        {
            this.Connection = null;
        }

        public SqlConnection open()
        {
            try
            {
                this.Connection = new SqlConnection();

                this.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ToString();
                if (this.Connection.State != ConnectionState.Open)
                {
                    this.Connection.Open();
                }
                return this.Connection;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlConnection close()
        {
            try
            {
                if (this.Connection.State != ConnectionState.Closed)
                {
                    this.Connection.Close();
                    this.Connection.Dispose();
                }
                return this.Connection;

            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {
                this.Connection.Close();
                this.Connection.Dispose();
            }
        }

    }
}