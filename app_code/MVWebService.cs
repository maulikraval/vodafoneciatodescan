﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using dataaccesslayer;
using mybusiness;
using System.Data;
using System.Collections;
using Newtonsoft.Json;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Net;
using System.IO;
using MVdataaccesslayer;
using FindDataAccessLayer;

/// <summary>
/// Summary description for MVWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class MVWebService : System.Web.Services.WebService
{
    MVDataAccess da = new MVDataAccess();
    DataTable dt = new DataTable();
    public MVWebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }
    [WebMethod]
    public string upload_photos(string data)
    {
        string FileName_new = "";
        try
        {
            var serializer = new JavaScriptSerializer();
            var upload_photo = serializer.Deserialize<List<upload_photo1>>(data);

            string siteid = upload_photo[0].siteid;
            string stepid = upload_photo[0].stepid;
            string inspection_counter = upload_photo[0].inspection_counter;
            string check_type = upload_photo[0].check_type;
            string ques_id = upload_photo[0].ques_id;

            Image i = Base64ToImage(upload_photo[0].buffer);
            bool fileSaved = false;
            string nssid = "";
            if (check_type == "45" && Convert.ToInt32(stepid) > 3)
            {
                nssid = upload_photo[0].nss_id;
                FileName_new =SpacialCharRemove.SpacialChar_Remove(nssid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + ".jpg";
                FileName_new = FileName_new.Replace("\r\n", "");
            }
            else
            {
                FileName_new = SpacialCharRemove.SpacialChar_Remove(siteid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + ".jpg";
                FileName_new = FileName_new.Replace("\r\n", "");
            }

            //  string serverPath = "D:/Vodafone/PTW_TESTING_CODE/PTW_TESTING_CODE/pics/";
            //string serverPath = "D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/vodafone_pics/UploadPhoto/";
            string serverPath = "E:/Vodafone/pics/";
            if (!Directory.Exists(serverPath))
            {
                Directory.CreateDirectory(serverPath);
            }

            i.Save(serverPath + "/" + FileName_new);


            da = new MVDataAccess();
            dt = new DataTable();
            if (check_type == "45" && Convert.ToInt32(stepid) < 4)
            {

                dt = da.insertphotodetails_tx(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, siteid);
            }

            else if (check_type == "45" && Convert.ToInt32(stepid) > 3)
            {

                dt = da.insertphotodetails_tx(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, nssid);
            }
            else
            {
                dt = da.insertphotodetails(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new);
            }

            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 1;
            rn.message = "success";
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;

        }
        catch (Exception e)
        {
            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 2;
            rn.message = "Error -:" + FileName_new + "_" + e;
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }

    }

    [WebMethod]
    public string upload_photos_offline(string data)
    {
        string FileName_new = "";
        try
        {
            if (data.Contains("@"))
            {
                data = data.Replace("@", "").ToString();
            }
            if (data.Contains("*"))
            {
                data = data.Replace("*", "").ToString();
            }
            if (data.Contains("("))
            {
                data = data.Replace("(", "").ToString();
            }

            if (data.Contains(")"))
            {
                data = data.Replace(")", "").ToString();
            }
            var serializer = new JavaScriptSerializer();
            var upload_photo = serializer.Deserialize<List<upload_photo1>>(data);

            string siteid = upload_photo[0].siteid;
            string stepid = upload_photo[0].stepid;
            string inspection_counter = upload_photo[0].inspection_counter;
            string check_type = upload_photo[0].check_type;
            string ques_id = upload_photo[0].ques_id;

            Image i = Base64ToImage(upload_photo[0].buffer);
            bool fileSaved = false;
            string nssid = "";
            if (check_type == "45" && Convert.ToInt32(stepid) > 3)
            {
                nssid = upload_photo[0].nss_id;
                FileName_new = SpacialCharRemove.SpacialChar_Remove(nssid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + ".jpg";
                FileName_new = FileName_new.Replace("\r\n", "");
            }
            else
            {
                FileName_new = SpacialCharRemove.SpacialChar_Remove(siteid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + ".jpg";
                FileName_new = FileName_new.Replace("\r\n", "");
            }

            // string serverPath = "D:/Vodafone/PTW_TESTING_CODE/PTW_TESTING_CODE/pics/";
            //string serverPath = "D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/vodafone_pics/UploadPhoto/";
            string serverPath = "E:/Vodafone/pics/";
            if (!Directory.Exists(serverPath))
            {
                Directory.CreateDirectory(serverPath);
            }

            i.Save(serverPath + "/" + FileName_new);


            da = new MVDataAccess();
            dt = new DataTable();
            if (check_type == "45" && Convert.ToInt32(stepid) < 4)
            {

                dt = da.insertphotodetails_tx_offline(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, siteid);
            }

            else if (check_type == "45" && Convert.ToInt32(stepid) > 3)
            {

                dt = da.insertphotodetails_tx_offline(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, nssid);
            }
            else
            {
                dt = da.insertphotodetails_offline(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new);
            }

            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 1;
            rn.message = "success";
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;

        }
        catch (Exception e)
        {
            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 2;
            rn.message = "Error -:" + FileName_new + "_" + e;
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }

    }

    [WebMethod]
    public string upload_photos_punchpoint(string data)
    {
        string FileName_new = "";
        try
        {
            if (data.Contains("@"))
            {
                data = data.Replace("@", "").ToString();
            }
            if (data.Contains("*"))
            {
                data = data.Replace("*", "").ToString();
            }
            if (data.Contains("("))
            {
                data = data.Replace("(", "").ToString();
            }

            if (data.Contains(")"))
            {
                data = data.Replace(")", "").ToString();
            }

            var serializer = new JavaScriptSerializer();
            var upload_photo = serializer.Deserialize<List<upload_photo1>>(data);

            string siteid = upload_photo[0].siteid;
            string stepid = upload_photo[0].stepid;
            string inspection_counter = upload_photo[0].inspection_counter;
            string check_type = upload_photo[0].check_type;
            string ques_id = upload_photo[0].ques_id;

            Image i = Base64ToImage(upload_photo[0].buffer);
            bool fileSaved = false;
            string nssid = "";
            if (check_type == "45" && Convert.ToInt32(stepid) > 3)
            {
                nssid = upload_photo[0].nss_id;
                FileName_new = SpacialCharRemove.SpacialChar_Remove(nssid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + ".jpg";
                FileName_new = FileName_new.Replace("\r\n", "");
            }
            else
            {
                FileName_new = SpacialCharRemove.SpacialChar_Remove(siteid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(inspection_counter) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + ".jpg";
                FileName_new = FileName_new.Replace("\r\n", "");
            }

            // string serverPath = "D:/Vodafone/PTW_TESTING_CODE/PTW_TESTING_CODE/pics/";
            //string serverPath = "D:/Inetpub/wwwroot/today-29-8-2011/Vodafone_Production_CIAT_09042013/vodafone_pics/UploadPhoto/";
            string serverPath = "E:/Vodafone/pics/";
            if (!Directory.Exists(serverPath))
            {
                Directory.CreateDirectory(serverPath);
            }

            i.Save(serverPath + "/" + FileName_new);


            da = new MVDataAccess();
            dt = new DataTable();
            if (check_type == "45" && Convert.ToInt32(stepid) > 3)
            {

                dt = da.insertphotodetails_tx_punchpoint(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, nssid);
            }
            else if (check_type == "45" && Convert.ToInt32(stepid) < 4)
            {

                dt = da.insertphotodetails_tx_punchpoint(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new, siteid);
            }
            else
            {
                dt = da.insertphotodetails_punchpoint(siteid, stepid, inspection_counter, check_type, ques_id, FileName_new);
            }

            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 1;
            rn.message = "success";
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;

        }
        catch (Exception e)
        {
            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 2;
            rn.message = "Error -:" + FileName_new + "_" + e;
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }

    }
    [WebMethod]
    public string upload_photos_ptw(string data)
    {
        string FileName_new = "";
        try
        {
            if (data.Contains("@"))
            {
                data = data.Replace("@", "").ToString();
            }
            if (data.Contains("*"))
            {
                data = data.Replace("*", "").ToString();
            }
            if (data.Contains("("))
            {
                data = data.Replace("(", "").ToString();
            }

            if (data.Contains(")"))
            {
                data = data.Replace(")", "").ToString();
            }



            /* if ( data.Contains("@") || data.Contains("=") || data.Contains("*") )
                        {
                            data = data.Replace("/", "").Replace("@", "").Replace("=", "").Replace("*", "").Replace("[", "").Replace("]", "").Replace("(", "").Replace(")", "");
                        }*/
            var serializer = new JavaScriptSerializer();
            var upload_photo = serializer.Deserialize<List<upload_photo1>>(data);

            string ptwid = upload_photo[0].ptwid;
            string stepid = upload_photo[0].stepid;
            string category = upload_photo[0].category;
            string ques_id = upload_photo[0].ques_id;
            string check_type = upload_photo[0].check_type;
            string p_name = upload_photo[0].p_name;
            string ptw = upload_photo[0].ptw;

            Image i = Base64ToImage(upload_photo[0].buffer);
            bool fileSaved = false;


            FileName_new = SpacialCharRemove.SpacialChar_Remove(ptwid) + "_" + SpacialCharRemove.SpacialChar_Remove(stepid) + "_" + SpacialCharRemove.SpacialChar_Remove(ques_id) + "_" + SpacialCharRemove.SpacialChar_Remove(category) + "_" + SpacialCharRemove.SpacialChar_Remove(check_type) + "_" + SpacialCharRemove.SpacialChar_Remove(p_name) + ".jpg";
            FileName_new = FileName_new.Replace("\r\n", "");

            //  string serverPath = "D:/Vodafone/PTW_TESTING_CODE/PTW_TESTING_CODE/pics/";
            //string serverPath = "D:/Inetpub/wwwroot/production/Copy of Vodafone_production_for_deploy/hsw_final_06_11_2014/hsw_final_06_11_2014/pics/";
            string serverPath = "E:/Vodafone/pics/";
            if (!Directory.Exists(serverPath))
            {
                Directory.CreateDirectory(serverPath);
            }

            i.Save(serverPath + "/" + FileName_new);

            da = new MVDataAccess();
            dt = new DataTable();
            dt = da.insertphotodetails_ptw(ptwid, stepid, category, ques_id, check_type, p_name, ptw, FileName_new);

            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 1;
            rn.message = "success";
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;

        }
        catch (Exception e)
        {
            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 2;
            rn.message = "Error -:" + FileName_new + "_" + e;
            aList.Add(rn);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }

    }

    [WebMethod]
    public void select_photos_issuer_ptw(string data, string callback)
    {
        string FileName_new = "";
        string serializedData = "";
        try
        {
            var serializer = new JavaScriptSerializer();
            var upload_photo = serializer.Deserialize<List<upload_photo1>>(data);

            string ptwid = upload_photo[0].ptwid;
            string stepid = upload_photo[0].stepid;
            string category = upload_photo[0].category;
            string ques_id = upload_photo[0].ques_id;
            string check_type = upload_photo[0].check_type;
            string ptw = upload_photo[0].ptw;
            string p_name = upload_photo[0].p_name;

            da = new MVDataAccess();
            dt = new DataTable();
            dt = da.select_photos_issuer_ptw(ptwid, category, ques_id, check_type, p_name);

            if (dt.Rows.Count > 0)
            {
                string filename = dt.Rows[0]["filename"].ToString();
                string serverPath = "D:/Inetpub/wwwroot/production/Copy of Vodafone_production_for_deploy/hsw_final_06_11_2014/hsw_final_06_11_2014/pics/";
                //string serverPath="D:/Vodafone/PTW_TESTING_CODE/PTW_TESTING_CODE/pics/";
                string path = serverPath + filename;
                string base64String = null;
                //Image image2;
                using (System.Drawing.Image image = System.Drawing.Image.FromFile(path))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();
                        base64String = Convert.ToBase64String(imageBytes);

                        return_ rn = new return_();
                        ArrayList aList = new ArrayList();
                        rn.status = 1;
                        rn.url = base64String;
                        if (category == "5")
                        {
                            rn.ptw_role = dt.Rows[0]["ques_id"].ToString();
                        }
                        else
                        {
                            rn.ptw_role = ques_id;
                        }
                        rn.message = "success";
                        aList.Add(rn);
                        serializedData = JsonConvert.SerializeObject(aList);

                        /*byte[] imageBytes2 = Convert.FromBase64String(base64String);
                        MemoryStream ms = new MemoryStream(imageBytes2, 0,
                    imageBytes2.Length);

                        // Convert byte[] to Image
                        ms.Write(imageBytes2, 0, imageBytes2.Length);
                        image2 = Image.FromStream(ms, true);
                        return image2;*/
                    }
                }
            }
            else
            {
                return_ rn = new return_();
                ArrayList aList = new ArrayList();
                rn.status = 3;
                rn.ptw_role = ques_id;
                rn.message = "Photo is not captured for this question";
                aList.Add(rn);
                serializedData = JsonConvert.SerializeObject(aList);
            }

            if (callback == "no")
            {
                callback_no(serializedData);
            }
            else
            {
                callback_function(serializedData, callback);
            }
        }
        catch (Exception e)
        {
            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 2;
            rn.message = "Error -:" + FileName_new + "_" + e;
            aList.Add(rn);
            serializedData = JsonConvert.SerializeObject(aList);

            if (callback == "no")
            {
                callback_no(serializedData);
            }
            else
            {
                callback_function(serializedData, callback);
            }
        }
    }

    public void callback_no(string data)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(data);
        Context.Response.AddHeader("Access-Control-Allow-Origin", "*");
        Context.Response.AddHeader("Connection", "Keep-Alive");
        Context.Response.AddHeader("Keep-Alive", "timeout=5, max=100");
        Context.Response.ContentType = "text/html";
        Context.Response.Write(sb.ToString());
    }

    public void callback_function(string data, string callback)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(callback + "(");
        // sb.Append("{\"d\":\"ok\"}"); // indentation is just for ease of reading while testing
        sb.Append(data + ");");
        Context.Response.Clear();
        Context.Response.ContentType = "application/json";
        Context.Response.Write(sb.ToString());
    }

    [WebMethod]
    public string GetApplicationURL(string data)
    {
        return_ rn_off = new return_();
        try
        {
            ArrayList aList = new ArrayList();
            //if (version == "V1.4")
            //{
            rn_off.url = "http://182.18.181.202/downloads/ciat_ptw_pmt_new.apk";
            rn_off.status = 1;
            rn_off.message = "Success";
            aList.Add(rn_off);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }
        catch (Exception ex)
        {
            ArrayList aList = new ArrayList();
            rn_off.status = 3;
            rn_off.message = "Error";
            aList.Add(rn_off);
            string serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }
    }
    public Image Base64ToImage(string base64String)
    {
        byte[] imageBytes = Convert.FromBase64String(base64String);
        MemoryStream ms = new MemoryStream(imageBytes, 0,
    imageBytes.Length);

        // Convert byte[] to Image
        ms.Write(imageBytes, 0, imageBytes.Length);
        Image image = Image.FromStream(ms, true);
        return image;
    }
    [WebMethod]
    public string updateuniquecode_android_native(string data, string callback)
    {

        try
        {
            IList<updateuniquecode> log_ = new JavaScriptSerializer()
           .Deserialize<IList<updateuniquecode>>(data);

            string password = log_[0].password_;
            string correct_password = password.Replace(" ", "+");
            //if (log_[0].version == "a_3.1" || log_[0].version == "a_3.2" || log_[0].version == "b_3.1" || log_[0].version == "b_3.2")
            //if (log_[0].version == "3.6" || log_[0].version == "b_3.2" || log_[0].version == "3.8" || log_[0].version == "3.9" || log_[0].version == "3.10")
            //if (log_[0].version == "3.10" || log_[0].version == "3.6.1" || log_[0].version == "3.6" || log_[0].version == " 3.6" || log_[0].version == " 3.6.1")
            if (log_[0].version == "a_4.19" || log_[0].version == "a_4.20")
            //if (log_[0].username_!="")
            {
                da = new MVDataAccess();
                int flag = da.updateuniquecode_android_native(log_[0].username_, correct_password, log_[0].uniq_code, log_[0].imeino);

                da = new MVDataAccess();
                string imei = da.select_imei_of_user(log_[0].username_);
                string a = "";
                string serializedData = "";
                ArrayList aList = new ArrayList();
                if (imei == log_[0].imeino)
                {
                    return_ rn = new return_();
                    rn.status = 1;
                    rn.message = "Success";
                    aList.Add(rn);
                    serializedData = JsonConvert.SerializeObject(aList);
                    return serializedData;
                }
                else
                {
                    return_ rn = new return_();
                    rn.status = 2;
                    rn.message = "Dear User Please un-install ciat application from your phone and download new version from google play store , search on play store 'ciat'";
                    aList.Add(rn);
                    serializedData = JsonConvert.SerializeObject(aList);
                    return serializedData;
                }

            }
            else
            {
                return_ rn = new return_();
                rn.status = 4;
                rn.message = "Upgrade Your Application!!!";
                string serializedData = "";
                ArrayList aList = new ArrayList();
                aList.Add(rn);
                serializedData = JsonConvert.SerializeObject(aList);
                return serializedData;
            }
        }
        catch (Exception ex)
        {
            string serializedData = "";
            ArrayList aList = new ArrayList();
            return_ rn = new return_();
            rn.status = 3;
            rn.message = ex.Message.ToString() + "_updateuniquecode_android_native";
            aList.Add(rn);
            serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }
    }

    [WebMethod]
    public string insertlatlongfrommobile_native(string data, string callback)
    {
        IList<insertlatlongfrommobile> log_ = new JavaScriptSerializer()
       .Deserialize<IList<insertlatlongfrommobile>>(data);
        da = new MVDataAccess();

        da.insertlatlongfrommobile_native(Convert.ToDouble(log_[0].latitude), Convert.ToDouble(log_[0].longitude), log_[0].status, log_[0].accuracy, Convert.ToInt32(log_[0].gps_status), log_[0].imei);
        ArrayList aList = new ArrayList();
        return_ rn = new return_();
        rn.status = 1;
        rn.message = "Success";
        aList.Add(rn);
        string serializedData = JsonConvert.SerializeObject(aList);
        return serializedData;
    }

    [WebMethod]
    public string select_user_pass(string data, string callback)
    {
        try
        {
            IList<login_> log_ = new JavaScriptSerializer()
         .Deserialize<IList<login_>>(data);
            da = new MVDataAccess();
            dt = new DataTable();
            dt = da.select_user_pass(log_[0].uniq_code);

            string serializedData = "";
            ArrayList aList = new ArrayList();
            return_ rn = new return_();
            rn.status = 1;
            rn.message = "Success";
            rn.row_count = dt.Rows[0][3].ToString();
            rn.role = dt.Rows[0][4].ToString();
            rn.ptw_role = dt.Rows[0][5].ToString();

            aList.Add(rn);
            serializedData = JsonConvert.SerializeObject(aList);

            return serializedData;
        }
        catch (Exception ex)
        {
            string serializedData = "";
            ArrayList aList = new ArrayList();
            return_ rn = new return_();
            rn.status = 2;
            rn.message = ex.Message.ToString();
            rn.row_count = "";

            aList.Add(rn);
            serializedData = JsonConvert.SerializeObject(aList);
            return serializedData;
        }

    }
    #region
    [WebMethod]
    public string PhotoDeleteMail(string data)
    {
        try
        {
            IList<login_> log_ = new JavaScriptSerializer()
         .Deserialize<IList<login_>>(data);
            //da = new mydataaccess1();
            //dt = new DataTable();
            //dt = da.select_check_aunthentication(log_[0].username_, log_[0].u_c);
            //if (dt.Rows.Count == 1)
            if (1 == 1)
            {
                FindDataAccess fda = new FindDataAccess();
                DataSet PhotoDeleteMail = new DataSet();
                string Category = log_[0].category;
                int CategoryId = 0;
                if (Category != "")
                {
                    CategoryId = Convert.ToInt16(Category);
                }
                PhotoDeleteMail = fda.PhotoDeleteMail(log_[0].username, log_[0].siteid, log_[0].check_type, CategoryId, log_[0].ptwid);
                ArrayList aList = new ArrayList();
                if (PhotoDeleteMail.Tables[0].Rows.Count > 0)
                {
                    string Username = PhotoDeleteMail.Tables[0].Rows[0]["USERNAME"].ToString();
                    string SurveyType = "";
                    string checksheetDescription = "";
                    string Subject = "";
                    string ptwsiteid = "";
                    if (log_[0].ptwid != "")
                    {
                        ptwsiteid = PhotoDeleteMail.Tables[2].Rows[0]["ptwid"].ToString();
                        Subject = "Alert for Photo deleted from mobile application-" + SendMail.ishtml(ptwsiteid);
                        SurveyType = "PTW";
                    }
                    else
                    {
                        checksheetDescription = PhotoDeleteMail.Tables[0].Rows[0]["CHECKSHEET_DESCRIPTION"].ToString();
                        Subject = "Alert for Photo deleted from mobile application-" + SendMail.ishtml(log_[0].siteid) + "(" + SendMail.ishtml(checksheetDescription) + ")";
                        Category = PhotoDeleteMail.Tables[0].Rows[0]["CATEGORY"].ToString();
                        string[] arrayCiatSurveyType = { "UBR V1", "WIC V1", "VOIC V1", "VSIC V1", "VMSIC V1", "PSIC V2", "GHIC V1", "FWIC V1", "VSLIC V1", "v2ms", "v2", "v5", "EMPSC V1", "EMSC V1", "EMVSC V1", "AOIC V1", "MSCSC V1", "CIIC V1", "CIC V1", "2W4W" };
                        string[] arrayGpmPassiveSurveyType = { "SiteLocation", "HVAC", "X_mer", "EB", "PIU", "SMPS", "BB", "DG", "Earthing", "Shelter", "THCA_PHCA", "FPS", "FSS", "Alarm", "EMF", "General" };
                        string[] arrayTxEquSurveyType = { "TxEquip" };
                        string[] arrayGpmActiveSurveyType = { "BTS_New", "MW", "TMEA", "BSC", "InfraVisual" };
                        SurveyType = PhotoDeleteMail.Tables[0].Rows[0]["CHECKSHEET_TYPE"].ToString();
                        checksheetDescription = PhotoDeleteMail.Tables[0].Rows[0]["CHECKSHEET_DESCRIPTION"].ToString();
                        int Flag = 0;
                        if (Flag == 0)
                        {
                            foreach (var x in arrayCiatSurveyType)
                            {
                                if (x.Equals(SurveyType))
                                {
                                    SurveyType = "CIAT";
                                    Flag = 1;
                                    break;
                                }
                            }
                        }
                        if (Flag == 0)
                        {
                            foreach (var x in arrayGpmPassiveSurveyType)
                            {
                                if (x.Equals(SurveyType))
                                {
                                    SurveyType = "G-PM Passive";
                                    Flag = 1;
                                    break;
                                }
                            }
                        }
                        if (Flag == 0)
                        {
                            foreach (var x in arrayTxEquSurveyType)
                            {
                                if (x.Equals(SurveyType))
                                {
                                    SurveyType = "TxEquip";
                                    Flag = 1;
                                    break;
                                }
                            }

                        }
                        if (Flag == 0)
                        {
                            foreach (var x in arrayGpmActiveSurveyType)
                            {
                                if (x.Equals(SurveyType))
                                {
                                    SurveyType = "G-PM Active";
                                    Flag = 1;
                                    break;
                                }
                            }
                        }
                    }



                    string MobileNo = PhotoDeleteMail.Tables[0].Rows[0]["CONTACTNO"].ToString();
                    string HtmlBody = "Hi Circle Admin,<br /><br />";
                    HtmlBody += "We have identified that , Photos which were captured during survey from CIAT application has been deleted from " + SendMail.ishtml(Username) + " mobile hence it will not reflect in report.<br />";

                    if (log_[0].ptwid != "")
                    {
                        HtmlBody += "PTWid :- " + SendMail.ishtml(ptwsiteid) + "<br /><br />";
                    }
                    else
                    {
                        if (Category == "")
                        {
                            HtmlBody += "Survey Type :- " + SendMail.ishtml(SurveyType) + "<br />";
                        }
                        else
                        {
                            HtmlBody += "Category type :- " + SendMail.ishtml(checksheetDescription) + "<br />";
                        }
                        HtmlBody += "Siteid :- " + SendMail.ishtml(log_[0].siteid) + "<br /><br />";
                    }

                    HtmlBody += "Username :- " + SendMail.ishtml(Username) + "<br /><br />";
                    HtmlBody += "Mobile Number :- " + SendMail.ishtml(MobileNo) + "<br /><br />";

                    HtmlBody += "<b>Thanks & Regards</b><br />";
                    HtmlBody += "<b>CIAT support team</b>";


                    string EmailTo = "";
                    for (int i = 0; i < PhotoDeleteMail.Tables[1].Rows.Count; i++)
                    {
                        if (i == (PhotoDeleteMail.Tables[1].Rows.Count - 1))
                        {
                            EmailTo += PhotoDeleteMail.Tables[1].Rows[i]["EMAILID"].ToString();
                        }
                        else
                        {
                            EmailTo += PhotoDeleteMail.Tables[1].Rows[i]["EMAILID"].ToString() + ",";
                        }
                    }
                    SendMail sm = new SendMail();
                    sm.mailsend(EmailTo, Subject, HtmlBody);
                    // SendMail(HtmlBody, EmailTo, Subject);
                    ////  SendMail(HtmlBody, "nitin.thakor@skyproductivity.com,sudhir.panchal@skyproductivity.com,darshini@skyproductivity.com", Subject);

                    return_ rn = new return_();
                    rn.status = 1;
                    rn.message = "Email Sent";
                    aList.Add(rn);

                }
                else
                {
                    return_ rn = new return_();
                    rn.status = 4;
                    rn.message = "EmailId not found.";
                    aList.Add(rn);
                }

                return JsonConvert.SerializeObject(aList);
            }
            else
            {
                return_ rn = new return_();
                ArrayList aList = new ArrayList();
                rn.status = 2;
                rn.message = "Authentication Fail";
                aList.Add(rn);
                return JsonConvert.SerializeObject(aList);
            }
        }
        catch (Exception Ex)
        {
            return_ rn = new return_();
            ArrayList aList = new ArrayList();
            rn.status = 3;
            rn.message = Ex.Message.ToString();
            aList.Add(rn);
            return JsonConvert.SerializeObject(aList);
        }
    }

    //private void SendMail(string HtmlBody, string EmailTo, string Subject)
    //{
    //    try
    //    {
    //        SendMail sm = new SendMail();
    //        sm.mailsend(EmailTo, Subject, HtmlBody);

    //        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", EmailTo, Subject, HtmlBody);

    //        //message.IsBodyHtml = true;
    //        //message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
    //        //message.CC.Add("CITappsupport@vodafoneidea.com");

    //        //#region "VODAFONE SERVER CREDENTIALS"

    //        //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);
    //        //emailClient.Credentials = new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");
    //        //emailClient.EnableSsl = false;

    //        //#endregion

    //        //#region "SUPPORT@SKYPRODUCTIVITY CREDENTIALS"

    //        ///*  SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);
    //        //  emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
    //        //  emailClient.EnableSsl = true; */

    //        //#endregion

    //        //emailClient.Send(message);
    //    }
    //    catch (Exception Ex)
    //    {

    //    }
    //}
    #endregion
}
public class login_
{
    public string purpose;
    public string siteid;
    public string username_;
    public string username;
    public int flow;
    public string password_;
    public string uniq_code;
    public string imeino;
    public string u_c;
    public string circle;
    public string zone;
    public string imei;
    public string inspection_counter;
    public string latitude;
    public string longitude;
    public string status;
    public string accuracy;
    public string gps_status;
    public string que_id;
    public string step;
    public string counter;
    public string check_type;
    public string value;
    public string key_count;
    public string height;
    public string tower;
    public string userid;
    public string version;
    public string ant_no;
    public string check_sheet;
    public string category;
    public string ptwid;
}
public class insertlatlongfrommobile
{
    public string imei;
    public string latitude;
    public string longitude;
    public string status;
    public string accuracy;
    public string gps_status;

}
public class updateuniquecode
{
    public string password_;
    public string username_;
    public string version;
    public string uniq_code;
    public string imeino;
}
public class upload_photo1
{
    public string siteid;
    public string stepid;
    public string inspection_counter;
    public string check_type;
    public string ques_id;
    public string buffer;
    public string ptwid;
    public string category;
    public string p_name;
    public string ptw;
    public string nss_id;
}
public class return_
{
    public string row_count;
    public int status;
    public string message;
    public string url;
    public string role;
    public string ptw_role;
}