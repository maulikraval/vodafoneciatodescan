﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace businessaccesslayer
{
    public class business_2
    {
        private string userid;
        private string tower;
        private double rate;
        private string siteid;
        private double percentage;
        private DateTime date;

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public double Percentage
        {
            get { return percentage; }
            set { percentage = value; }
        }
        public string Siteid
        {
            get { return siteid; }
            set { siteid = value; }
        }

        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }
  

        public string Tower
        {
            get { return tower; }
            set { tower = value; }
        }
        public string Userid
        {
            get { return userid; }
            set { userid = value; }
        }
    }


}
