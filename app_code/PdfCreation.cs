﻿using dataaccesslayer;
using dataaccesslayer2;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PdfCreation
/// </summary>
public class PdfCreation
{
    public PdfCreation()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string CreatePdfOfChecksheets(string fileName, string siteId, string checksheets)
    {
        try
        {
            string filePath = CreatePdfReportPath();
            fileName = SpacialCharRemove.SpacialChar_Remove(fileName) + "_" + CreateUniqueFileNameWithExtention("pdf");
            Document documentPdf = new Document(PageSize.A4, 30F, 30F, 10F, 10F);
            PdfWriter writer = PdfWriter.GetInstance(documentPdf, new FileStream(Path.Combine(filePath, fileName), FileMode.Create));
            documentPdf.Open();

            //PDF_ActivePassivePreventiveMaintenanceChecklist(ref documentPdf, siteId, checksheets);
            ChecksheetPdfCreation(ref documentPdf, SpacialCharRemove.SpacialChar_Remove(siteId), checksheets);

            documentPdf.Close();
            return fileName;
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public string CreateUniqueFileNameWithExtention(string extention)
    {
        try
        {
            Random randomNumber = new Random();
            return DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString() + "_" + randomNumber.Next(1000).ToString() + "." + extention;
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public string CreatePdfReportPath()
    {
        try
        {
            string pdfFilePath = ConfigurationManager.AppSettings["Pdf_Report_Path"].ToString();
            if (!Directory.Exists(pdfFilePath))
            {
                Directory.CreateDirectory(pdfFilePath);
            }
            return pdfFilePath;
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public Font SetFont(string fontName, float fontSize, string fontStyle, string fontColor)
    {
        if (fontStyle.ToLower() == "normal" && fontColor.ToLower() == "black")
        {
            return FontFactory.GetFont(fontName, fontSize, Font.NORMAL, BaseColor.BLACK);
        }
        else if (fontStyle.ToLower() == "bold" && fontColor.ToLower() == "black")
        {
            return FontFactory.GetFont(fontName, fontSize, Font.BOLD, BaseColor.BLACK);
        }
        else
        {
            return FontFactory.GetFont(fontName, fontSize, Font.NORMAL, BaseColor.BLACK);
        }
    }

    #region Active Preventive Maintenance Checklist V1
    public void PDF_ActivePreventiveMaintenanceChecklistV1(ref Document documentPdf, string siteId, string checksheets)
    {
        try
        {
            int flag = 0;

            PdfPTable pTableHeader = Header_ActivePreventiveMaintenanceChecklistV1(siteId);
            documentPdf.Add(pTableHeader);

            #region CheckSheet Array
            string[,] a = new string[,]
                 {
                     {"TxEquip", "Tx & IP Equipment"},
                     {"BTS_New", "PrM Checklist BTS"},
                     {"MW", "PrM Chklist_MW & Mux_Mod"},
                     {"TMEA", "PrM Check list TMEA"},
                     {"BSC", "BSC Checks Points"},
                     {"InfraVisual", "Infra Visual"}
                 };
            #endregion

            #region Checksheet Loop
            PdfPTable pTableBody = new PdfPTable(5);
            pTableBody.WidthPercentage = 100f;
            pTableBody.SetWidths(new float[] { 1, 3, 1, 1, 1 });

            int srnocounter = 1;
            int stepcounter = 1;

            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                string checksheetType = a[i, 0]; //type
                string checksheetDiscription = a[i, 1]; //Discription

                if (checksheets.Contains(checksheetType))
                {
                    stepcounter = 1;
                    if (i == 0)
                    {
                        Font fontCalibriHeader = SetFont("Calibri", 15, "bold", "black");

                        PdfPCell pCellBody1 = new PdfPCell(new Phrase("Sr. No.", fontCalibriHeader));
                        pCellBody1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody1.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody1);

                        PdfPCell pCellBody2 = new PdfPCell(new Phrase("Description", fontCalibriHeader));
                        pCellBody2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody2);

                        PdfPCell pCellBody3 = new PdfPCell(new Phrase("Specification", fontCalibriHeader));
                        pCellBody3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody3.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody3);

                        PdfPCell pCellBody4 = new PdfPCell(new Phrase("Answer", fontCalibriHeader));
                        pCellBody4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody4.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody4);


                        PdfPCell pCellBody5 = new PdfPCell(new Phrase("Comments / Explanations", fontCalibriHeader));
                        pCellBody5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody5.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody5);
                    }



                    ChecksheetHeader_ActivePreventiveMaintenanceChecklistV1(siteId, checksheetType, checksheetDiscription, ref srnocounter, ref flag, ref pTableBody);

                    Step_ActivePreventiveMaintenanceChecklistV1(siteId, checksheetType, checksheetDiscription, ref srnocounter, ref stepcounter, ref flag, ref pTableBody);

                    //documentPdf.Add(pTableBody);

                    srnocounter = srnocounter + 1;
                    //stepcounter = stepcounter + 1;
                }
            }

            documentPdf.Add(pTableBody);

            #endregion

            documentPdf.NewPage();

            int vsitemasterinsp = 0;

            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                string checksheetType = a[i, 0]; //type
                string checksheetDiscription = a[i, 1]; //Discription

                if (checksheets.Contains(checksheetType))
                {
                    Photos_ActivePreventiveMaintenanceChecklistV1(siteId, checksheetType, checksheetDiscription, ref vsitemasterinsp, ref pTableBody, ref documentPdf);
                }
            }

            Car_ActivePreventiveMaintenanceChecklistV1(ref documentPdf, siteId, "Active", vsitemasterinsp);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void PDF_ActivePassivePreventiveMaintenanceChecklist(ref Document documentPdf, string siteId, string checksheets)
    {
        try
        {
            int flag = 0;

            PdfPTable pTableHeader = Header_ActivePreventiveMaintenanceChecklistV1(siteId);
            documentPdf.Add(pTableHeader);

            #region CheckSheet Array
            string[,] a = new string[,]
                 {
                     {"TxEquip", "Tx & IP Equipment"},
                     {"BTS_New", "PrM Checklist BTS"},
                     {"MW", "PrM Chklist_MW & Mux_Mod"},
                     {"TMEA", "PrM Check list TMEA"},
                     {"BSC", "BSC Checks Points"},
                     {"InfraVisual", "Infra Visual"},
                     {"SiteLocation", "Site Location"},
                     {"HVAC", "Air conditioner & FCU"},
                     {"X_mer", "Transformer"},
                     {"EB", "Electricity Board"},
                     {"PIU", "PIU"},
                     {"SMPS", "SMPS & other electrical"},
                     {"BB", "Battery Bank"},
                     {"DG", "Diesel Generator"},
                     {"Earthing", "Earthing"},
                     {"Shelter", "Shelter"},
                     {"THCA_PHCA", "THCA/PHCA"},
                     {"FPS", "Fall Protection System"},
                     {"FSS", "Fire Supression System"},
                     {"Alarm", "External Alarms"},
                     {"EMF", "EMF Signage"},
                     {"General", "General Maintenance"}
                 };
            #endregion

            #region Checksheet Loop
            PdfPTable pTableBody = new PdfPTable(5);
            pTableBody.WidthPercentage = 100f;
            pTableBody.SetWidths(new float[] { 1, 3, 1, 1, 1 });

            int srnocounter = 1;
            int stepcounter = 1;

            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                string checksheetType = a[i, 0]; //type
                string checksheetDiscription = a[i, 1]; //Discription

                if (checksheets.Contains(checksheetType))
                {
                    stepcounter = 1;
                    if (i == 0)
                    {
                        Font fontCalibriHeader = SetFont("Calibri", 15, "bold", "black");

                        PdfPCell pCellBody1 = new PdfPCell(new Phrase("Sr. No.", fontCalibriHeader));
                        pCellBody1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody1.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody1);

                        PdfPCell pCellBody2 = new PdfPCell(new Phrase("Description", fontCalibriHeader));
                        pCellBody2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody2.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody2);

                        PdfPCell pCellBody3 = new PdfPCell(new Phrase("Specification", fontCalibriHeader));
                        pCellBody3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody3.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody3);

                        PdfPCell pCellBody4 = new PdfPCell(new Phrase("Answer", fontCalibriHeader));
                        pCellBody4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody4.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody4);


                        PdfPCell pCellBody5 = new PdfPCell(new Phrase("Comments / Explanations", fontCalibriHeader));
                        pCellBody5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pCellBody5.HorizontalAlignment = Element.ALIGN_CENTER;
                        pTableBody.AddCell(pCellBody5);
                    }

                    ChecksheetHeader_ActivePassivePreventiveMaintenanceChecklist(siteId, checksheetType, checksheetDiscription, ref srnocounter, ref flag, ref pTableBody);

                    Step_ActivePassivePreventiveMaintenanceChecklist(siteId, checksheetType, checksheetDiscription, ref srnocounter, ref stepcounter, ref flag, ref pTableBody);

                    //documentPdf.Add(pTableBody);

                    srnocounter = srnocounter + 1;
                    //stepcounter = stepcounter + 1;
                }
            }

            documentPdf.Add(pTableBody);

            #endregion

            documentPdf.NewPage();

            int vsitemasterinsp = 0;

            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                string checksheetType = a[i, 0]; //type
                string checksheetDiscription = a[i, 1]; //Discription

                if (checksheets.Contains(checksheetType))
                {
                    Photos_ActivePreventiveMaintenanceChecklistV1(siteId, checksheetType, checksheetDiscription, ref vsitemasterinsp, ref pTableBody, ref documentPdf);
                }
            }

            Car_ActivePreventiveMaintenanceChecklistV1(ref documentPdf, siteId, "Active", vsitemasterinsp);
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public PdfPTable Header_ActivePreventiveMaintenanceChecklistV1(string siteId)
    {
        PdfPTable pTableHeader = new PdfPTable(1);
        pTableHeader.WidthPercentage = 100f;

        Font fontCalibriDisplay = SetFont("Calibri", 10, "normal", "black");

        PdfPCell pCellHeader1 = new PdfPCell(new Phrase("Active Preventive Maintenance Checklist V1", SetFont("Calibri", 15, "bold", "black")));
        pCellHeader1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
        pCellHeader1.HorizontalAlignment = Element.ALIGN_CENTER;
        pCellHeader1.VerticalAlignment = Element.ALIGN_MIDDLE;
        pCellHeader1.BorderColor = BaseColor.BLACK;
        pCellHeader1.FixedHeight = 20f;
        pTableHeader.AddCell(pCellHeader1);

        PdfPTable pTableInner1 = new PdfPTable(2);
        pTableInner1.WidthPercentage = 100f;

        mydataaccess1 mDa = new mydataaccess1();
        DataTable dataTable = new DataTable();
        dataTable = mDa.pm_select_site_data_all_pdf(siteId);

        Phrase phrase1 = new Phrase("Circle : " + dataTable.Rows[0][1].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner1 = new PdfPCell(phrase1);
        pCellInner1.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase2 = new Phrase("Site ID : " + siteId, new Font(fontCalibriDisplay));
        PdfPCell pCellInner2 = new PdfPCell(phrase2);
        pCellInner2.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase3 = new Phrase("Address :" + dataTable.Rows[0][0].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner3 = new PdfPCell(phrase3);
        pCellInner3.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase4 = new Phrase("Site Incharge Name & No. : " + dataTable.Rows[0][2].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner4 = new PdfPCell(phrase4);
        pCellInner4.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase5 = new Phrase("ISPTW : " + dataTable.Rows[0][3].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner5 = new PdfPCell(phrase5);
        pCellInner5.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase6 = new Phrase("PTWID : " + dataTable.Rows[0][4].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner6 = new PdfPCell(phrase6);
        pCellInner6.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner6.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        pTableInner1.AddCell(pCellInner1);
        pTableInner1.AddCell(pCellInner2);
        pTableInner1.AddCell(pCellInner3);
        pTableInner1.AddCell(pCellInner4);
        //pTableInner1.AddCell(pCellInner5);
        //pTableInner1.AddCell(pCellInner6);

        PdfPCell pCellBlank = new PdfPCell(new Phrase(""));
        pCellBlank.Colspan = 2;
        pCellBlank.FixedHeight = 15f;
        pTableInner1.AddCell(pCellBlank);

        pTableHeader.AddCell(pTableInner1);
        return pTableHeader;
    }

    public void ChecksheetHeader_ActivePreventiveMaintenanceChecklistV1(string siteId, string checksheetType, string checksheetDiscription, ref int srnocounter, ref int flag, ref PdfPTable pTableBody)
    {
        try
        {
            Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
            Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

            #region Fine Site Count
            mydataaccess1 da1 = new mydataaccess1();
            DataTable pm_findsitecount = new DataTable();
            pm_findsitecount = da1.pm_findcount(siteId, checksheetType.ToString());
            string AllCount = "";
            if (pm_findsitecount.Rows.Count > 0)
            {
                AllCount = " Date Of Active PM : " + pm_findsitecount.Rows[0][2].ToString() + " " + pm_findsitecount.Rows[0][1].ToString();
                AllCount += " Done By : " + pm_findsitecount.Rows[0][3].ToString();
                AllCount += " Contact Number : " + pm_findsitecount.Rows[0][4].ToString();
                AllCount += " Next Due Date : " + pm_findsitecount.Rows[0][5].ToString();
                AllCount += " Assigned To : " + pm_findsitecount.Rows[0][6].ToString();
                AllCount += " ISPTW : " + pm_findsitecount.Rows[0][7].ToString();
                AllCount += " PTWID : " + pm_findsitecount.Rows[0][8].ToString();
            }
            #endregion
            #region Check Checksheet Com,NonCom,PartialCom
            string CheckStatus = "";
            da1 = new mydataaccess1();
            DataTable DTCheckStatus = new DataTable();
            DTCheckStatus = da1.pm_checkStatus(siteId, checksheetType);
            if (DTCheckStatus.Rows.Count > 0)
            {
                CheckStatus = DTCheckStatus.Rows[0][2].ToString();
            }
            #endregion

            if (CheckStatus == "Completed")
            {
                PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell.Width = 20;
                pTableBody.AddCell(cellcount);

                PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_count);

                PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                cell1_Specount.Colspan = 3;
                cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_Specount);
            }
            if (CheckStatus == "NonComplete" || CheckStatus == "")
            {
                PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell.Width = 20;
                pTableBody.AddCell(cellcount);

                PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_count);

                PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                cell1_Specount.Colspan = 3;
                cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_Specount);
            }
            if (CheckStatus == "PartialCompleted")
            {
                PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell.Width = 20;
                pTableBody.AddCell(cellcount);

                PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_count);

                PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                cell1_Specount.Colspan = 3;
                cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_Specount);
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void ChecksheetHeader_ActivePassivePreventiveMaintenanceChecklist(string siteId, string checksheetType, string checksheetDiscription, ref int srnocounter, ref int flag, ref PdfPTable pTableBody)
    {
        try
        {
            Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
            Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

            #region Fine Site Count
            mydataaccess1 da1 = new mydataaccess1();
            DataTable pm_findsitecount = new DataTable();
            pm_findsitecount = da1.pm_findcount(siteId, checksheetType.ToString());
            string AllCount = "";
            if (pm_findsitecount.Rows.Count > 0)
            {
                AllCount = " Date Of Active PM : " + pm_findsitecount.Rows[0][2].ToString() + " " + pm_findsitecount.Rows[0][1].ToString();
                AllCount += " Done By : " + pm_findsitecount.Rows[0][3].ToString();
                AllCount += " Contact Number : " + pm_findsitecount.Rows[0][4].ToString();
                AllCount += " Next Due Date : " + pm_findsitecount.Rows[0][5].ToString();
                AllCount += " Assigned To : " + pm_findsitecount.Rows[0][6].ToString();
                AllCount += " ISPTW : " + pm_findsitecount.Rows[0][7].ToString();
                AllCount += " PTWID : " + pm_findsitecount.Rows[0][8].ToString();
            }
            #endregion
            #region Check Checksheet Com,NonCom,PartialCom
            string CheckStatus = "";
            da1 = new mydataaccess1();
            DataTable DTCheckStatus = new DataTable();
            DTCheckStatus = da1.pm_checkStatus(siteId, checksheetType);
            if (DTCheckStatus.Rows.Count > 0)
            {
                CheckStatus = DTCheckStatus.Rows[0][2].ToString();
            }
            #endregion

            if (checksheetType.ToString() == "SiteLocation")
            {
                PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell.Width = 20;
                pTableBody.AddCell(cellcount);

                PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_count);

                PdfPCell cell1_Specount = new PdfPCell(new Phrase("", new Font(Site_Heading)));
                cell1_Specount.Colspan = 3;
                cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_Specount);
            }
            else
            {
                if (CheckStatus == "Completed")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_Specount);
                }
                if (CheckStatus == "NonComplete" || CheckStatus == "")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_Specount);
                }
                if (CheckStatus == "PartialCompleted")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_Specount);
                }
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void Step_ActivePreventiveMaintenanceChecklistV1(string siteId, string checksheetType, string checksheetDiscription, ref int srnocounter, ref int stepcounter, ref int flag, ref PdfPTable pTableBody)
    {
        Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        #region Step
        //    for (int j = 0; j < pm_getallchecksheet.Rows.Count; j++)
        {
            flag = 0;
            mydataaccess1 da1 = new mydataaccess1();
            DataTable dt1 = new DataTable();
            dt1 = da1.pm_select_site_que_log_pdf(checksheetType.ToString(), siteId);

            int sec = 0;
            // stepcounter = stepcounter + 1;
            if (dt1.Rows.Count > 0)
            {
                int counter_t = 0;
                string Kye_Count = "";

                for (int k = 0; k < dt1.Rows.Count; k++)
                {
                    if (checksheetType == "MW")
                    {

                        if (Kye_Count != dt1.Rows[k][3].ToString())
                        {
                            counter_t = 0;
                        }

                        Kye_Count = dt1.Rows[k][3].ToString();
                        // flagCheck = "True";
                        if (Kye_Count == "1" || Kye_Count == "2" || Kye_Count == "3" || Kye_Count == "4"
                            || Kye_Count == "5" || Kye_Count == "6" || Kye_Count == "7" || Kye_Count == "8" || Kye_Count == "9"
                            || Kye_Count == "10" || Kye_Count == "11" || Kye_Count == "12" || Kye_Count == "13"
                            || Kye_Count == "14" || Kye_Count == "15" || Kye_Count == "16" || Kye_Count == "17"
                            || Kye_Count == "18" || Kye_Count == "19" || Kye_Count == "20" || Kye_Count == "21"
                            || Kye_Count == "22" || Kye_Count == "23" || Kye_Count == "24" || Kye_Count == "25"
                            || Kye_Count == "26" || Kye_Count == "27" || Kye_Count == "28" || Kye_Count == "29"
                            || Kye_Count == "30" || Kye_Count == "31" || Kye_Count == "32" || Kye_Count == "33"
                            || Kye_Count == "34" || Kye_Count == "35" || Kye_Count == "36" || Kye_Count == "37"
                            || Kye_Count == "38" || Kye_Count == "39" || Kye_Count == "40")
                        {
                            if (counter_t == 0)
                            {
                                PdfPCell cell00_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell00_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                cell00_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                // cell.Width = 20;
                                pTableBody.AddCell(cell00_key);
                                PdfPCell cell100_key = new PdfPCell(new Phrase(checksheetType.ToString() + "(" + Kye_Count + ")", new Font(calibri_Data)));
                                cell100_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell100_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_key);
                                PdfPCell cell100_spe_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell100_spe_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_spe_key);
                                PdfPCell cell400_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell400_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell4_.Width = 20;
                                pTableBody.AddCell(cell400_key);
                                PdfPCell cell500_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell500_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_key.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                pTableBody.AddCell(cell500_key);
                                counter_t = counter_t + 1;
                            }
                        }
                    }

                    PdfPCell cell00 = new PdfPCell(new Phrase((srnocounter + "." + stepcounter).ToString(), new Font(calibri_Data)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    cell00.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(calibri_Data)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell100_);

                    PdfPCell cell100_spe = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(calibri_Data)));
                    cell100_spe.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    pTableBody.AddCell(cell100_spe);

                    if (dt1.Rows[k][1].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        pTableBody.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][1].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        pTableBody.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][1].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pTableBody.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        pTableBody.AddCell(cell400_);
                    }

                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][5].ToString(), new Font(calibri_Data)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    pTableBody.AddCell(cell500_);

                    stepcounter = stepcounter + 1;
                }
            }
        }
        #endregion
    }

    public void Step_ActivePassivePreventiveMaintenanceChecklist(string siteId, string checksheetType, string checksheetDiscription, ref int srnocounter, ref int stepcounter, ref int flag, ref PdfPTable pTableBody)
    {
        Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        #region Step
        //    for (int j = 0; j < pm_getallchecksheet.Rows.Count; j++)
        {
            flag = 0;
            mydataaccess1 da1 = new mydataaccess1();
            DataTable dt1 = new DataTable();
            dt1 = da1.pm_select_site_que_log_pdf(checksheetType.ToString(), siteId);

            int sec = 0;
            // stepcounter = stepcounter + 1;
            if (dt1.Rows.Count > 0)
            {
                int counter_t = 0;
                string Kye_Count = "";

                for (int k = 0; k < dt1.Rows.Count; k++)
                {
                    if (checksheetType == "MW")
                    {

                        if (Kye_Count != dt1.Rows[k][3].ToString())
                        {
                            counter_t = 0;
                        }

                        Kye_Count = dt1.Rows[k][3].ToString();
                        // flagCheck = "True";
                        if (Kye_Count == "1" || Kye_Count == "2" || Kye_Count == "3" || Kye_Count == "4"
                            || Kye_Count == "5" || Kye_Count == "6" || Kye_Count == "7" || Kye_Count == "8" || Kye_Count == "9"
                            || Kye_Count == "10" || Kye_Count == "11" || Kye_Count == "12" || Kye_Count == "13"
                            || Kye_Count == "14" || Kye_Count == "15" || Kye_Count == "16" || Kye_Count == "17"
                            || Kye_Count == "18" || Kye_Count == "19" || Kye_Count == "20" || Kye_Count == "21"
                            || Kye_Count == "22" || Kye_Count == "23" || Kye_Count == "24" || Kye_Count == "25"
                            || Kye_Count == "26" || Kye_Count == "27" || Kye_Count == "28" || Kye_Count == "29"
                            || Kye_Count == "30" || Kye_Count == "31" || Kye_Count == "32" || Kye_Count == "33"
                            || Kye_Count == "34" || Kye_Count == "35" || Kye_Count == "36" || Kye_Count == "37"
                            || Kye_Count == "38" || Kye_Count == "39" || Kye_Count == "40")
                        {
                            if (counter_t == 0)
                            {
                                PdfPCell cell00_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell00_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                cell00_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                // cell.Width = 20;
                                pTableBody.AddCell(cell00_key);
                                PdfPCell cell100_key = new PdfPCell(new Phrase(checksheetType.ToString() + "(" + Kye_Count + ")", new Font(calibri_Data)));
                                cell100_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell100_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_key);
                                PdfPCell cell100_spe_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell100_spe_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_spe_key);
                                PdfPCell cell400_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell400_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell4_.Width = 20;
                                pTableBody.AddCell(cell400_key);
                                PdfPCell cell500_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell500_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_key.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                pTableBody.AddCell(cell500_key);
                                counter_t = counter_t + 1;
                            }
                        }
                    }
                    else if (checksheetType == "X_mer" || checksheetType == "PIU" || checksheetType == "SMPS" || checksheetType == "BB" || checksheetType == "DG"
                                || checksheetType == "HVAC" || checksheetType == "Earthing" || checksheetType == "Shelter" || checksheetType == "FPS")
                    {
                        if (Kye_Count != dt1.Rows[k][3].ToString())
                        {
                            counter_t = 0;
                        }

                        Kye_Count = dt1.Rows[k][3].ToString();
                        // flagCheck = "True";
                        if (Kye_Count == "1" || Kye_Count == "2" || Kye_Count == "3" || Kye_Count == "4"
                        || Kye_Count == "5" || Kye_Count == "7" || Kye_Count == "8" || Kye_Count == "9"
                        || Kye_Count == "10" || Kye_Count == "11" || Kye_Count == "12" || Kye_Count == "13"
                        || Kye_Count == "14" || Kye_Count == "15" || Kye_Count == "16")
                        {
                            if (counter_t == 0)
                            {
                                PdfPCell cell00_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell00_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                cell00_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                // cell.Width = 20;
                                pTableBody.AddCell(cell00_key);
                                PdfPCell cell100_key = new PdfPCell(new Phrase(checksheetType.ToString() + "(" + Kye_Count + ")", new Font(calibri_Data)));
                                cell100_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell100_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_key);
                                PdfPCell cell100_spe_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell100_spe_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_spe_key);
                                PdfPCell cell400_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell400_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell4_.Width = 20;
                                pTableBody.AddCell(cell400_key);
                                PdfPCell cell500_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell500_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_key.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                pTableBody.AddCell(cell500_key);
                                counter_t = counter_t + 1;
                            }
                        }
                    }
                    PdfPCell cell00 = new PdfPCell(new Phrase((srnocounter + "." + stepcounter).ToString(), new Font(calibri_Data)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    cell00.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(calibri_Data)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell100_);

                    PdfPCell cell100_spe = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(calibri_Data)));
                    cell100_spe.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    pTableBody.AddCell(cell100_spe);

                    if (dt1.Rows[k][1].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        pTableBody.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][1].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        pTableBody.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][1].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pTableBody.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        pTableBody.AddCell(cell400_);
                    }

                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][5].ToString(), new Font(calibri_Data)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    pTableBody.AddCell(cell500_);

                    stepcounter = stepcounter + 1;
                }
            }
        }
        #endregion
    }

    public void Photos_ActivePreventiveMaintenanceChecklistV1(string siteId, string checksheetType, string checksheetDiscription, ref int vsitemasterinsp, ref PdfPTable pTableBody, ref Document documentPdf)
    {
        try
        {
            string imageFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\";
            mydataaccess1 da1 = new mydataaccess1();
            DataTable dt = new DataTable();
            dt = da1.pm_GetinspectionCounter(siteId, checksheetType);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0][0].ToString() != "")
                {
                    vsitemasterinsp = Convert.ToInt32(dt.Rows[0][0].ToString()) - 1;
                }
            }
            if (vsitemasterinsp >= 0)
            {
                #region temp
                da1 = new mydataaccess1();

                dt = da1.pm_select_categories_by_sheet(checksheetType.ToString());
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    da1 = new mydataaccess1();
                    DataTable dt_photo = new DataTable();

                    dt_photo = da1.pm_select_photos_pdf(siteId, vsitemasterinsp, (m + 1), checksheetType);
                    if (dt_photo.Rows.Count > 0)
                    {
                        if (m == 0)
                        {
                            PdfPTable new_t1 = new PdfPTable(1);

                            new_t1.WidthPercentage = 100f;

                            Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                             Font.BOLD));

                            PdfPCell new_cell_ = new PdfPCell(head_ph_);
                            new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                            new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                            //new_cell_.FixedHeight = 20f;
                            new_t1.AddCell(new_cell_);
                            Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                            ));

                            documentPdf.Add(new_t1);
                            documentPdf.Add(break2);
                        }
                        if (dt_photo.Rows.Count > 0)
                        {
                            PdfPTable new_t = new PdfPTable(1);
                            new_t.WidthPercentage = 100f;

                            Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                             Font.BOLD));

                            PdfPCell new_cell = new PdfPCell(head_ph);
                            new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            //new_cell.FixedHeight = 20f;

                            new_t.AddCell(new_cell);
                            documentPdf.Add(new_t);

                            PdfPTable photo = new PdfPTable(4);
                            PdfPCell photo_cell = new PdfPCell();
                            photo.WidthPercentage = 100;
                            int flag = 0;
                            for (int p = 0; p < dt_photo.Rows.Count; p++)
                            {
                                try
                                {
                                    if (dt_photo.Rows.Count % 4 == 0)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        // imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                    }
                                    else
                                    {
                                        if (p == dt_photo.Rows.Count - 1)
                                        {
                                            if (flag == 4)
                                            {
                                                flag = 0;
                                            }
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                            // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);

                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;

                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                            photo.AddCell(photo_cell);

                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            //photo_cell.FixedHeight = 50F;
                                            photo.AddCell(photo_cell);
                                            flag++;

                                            for (int b = 0; b < 4 - flag; b++)
                                            {
                                                photo_cell = new PdfPCell();
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                //photo_cell.FixedHeight = 100;
                                                photo.AddCell(photo_cell);
                                            }


                                        }
                                        else
                                        {
                                            // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                            //imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            // imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);
                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            // photo_cell.FixedHeight = 100;
                                            //  photo_cell.FixedHeight = 220;
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                            flag++;
                                        }
                                    }
                                }
                                catch
                                {
                                    photo_cell = new PdfPCell();
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    // photo_cell.FixedHeight = 100;
                                    photo.AddCell(photo_cell);
                                }
                            }
                            documentPdf.Add(photo);
                            Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                            documentPdf.Add(break23);
                        }
                    }
                }
                #endregion
            }

        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void Car_ActivePreventiveMaintenanceChecklistV1(ref Document documentPdf, string siteId, string sheet, int i_counter)
    {
        string imageFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\";

        PdfPTable data = new PdfPTable(4);
        data.WidthPercentage = 100f;
        data.SetWidths(new float[] { 1, 4, 1, 1 });

        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        mydataaccess1 da2 = new mydataaccess1();
        DataTable dt_node = new DataTable();
        dt_node = da2.select_categories_by_sheet("Active");
        string checksheet = "";
        string checkstatus = "";
        // for (int d = 0; d < dt_node.Rows.Count; d++)
        {

            PdfPTable head = new PdfPTable(1);
            head.WidthPercentage = 100f;
            //checksheet = dt_node.Rows[d][0].ToString();

            documentPdf.NewPage();

            PdfPTable head1 = new PdfPTable(1);
            head1.WidthPercentage = 100f;
            Phrase p1header1 = new Phrase("Corrective Action Report", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD));
            PdfPCell c1 = new PdfPCell(p1header1);
            c1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFA500"));
            c1.HorizontalAlignment = Element.ALIGN_CENTER;
            c1.VerticalAlignment = Element.ALIGN_MIDDLE;
            c1.FixedHeight = 20f;
            head1.AddCell(c1);
            documentPdf.Add(head1);

            Phrase p1header = new Phrase(checksheet, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
            PdfPCell c = new PdfPCell(p1header);
            // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFA500"));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.BorderColor = iTextSharp.text.BaseColor.BLACK;
            c.FixedHeight = 20f;
            head.AddCell(c);

            PdfPCell blank = new PdfPCell(new Phrase(""));
            blank.Colspan = 2;
            blank.FixedHeight = 15f;


            documentPdf.Add(head);
            documentPdf.Add(blank);

            mydataaccess2 da = new mydataaccess2();
            DataTable dt = new DataTable();
            dt = da.select_categories_by_sheet("Active");

            data = new PdfPTable(6);
            data.WidthPercentage = 100f;
            data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });
            PdfPCell cell = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell.Width = 20;
            data.AddCell(cell);
            PdfPCell cell1_ = new PdfPCell(new Phrase("Question", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell1_.Width = 40;
            data.AddCell(cell1_);


            PdfPCell cell4_ = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell4_);

            PdfPCell cell5_ = new PdfPCell(new Phrase("Action Remarks", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell5_);

            cell4_ = new PdfPCell(new Phrase("Violation Severity", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell4_);

            cell5_ = new PdfPCell(new Phrase("Punch Point(External/Internal)", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell5_);


            /*   da = new mydataaccess2();
               dt = new DataTable();
               dt = da.select_categories_by_sheet(sheet);*/

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                checksheet = dt_node.Rows[j][0].ToString();
                int flag = 0;
                PdfPCell cell0 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell0.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell.Width = 20;
                data.AddCell(cell0);
                PdfPCell cell10_ = new PdfPCell(new Phrase(dt.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell10_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell1_.Width = 40;
                data.AddCell(cell10_);


                PdfPCell cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                PdfPCell cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);

                cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);


                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt_c = new DataTable();
                dt_c = da1.pm_GetinspectionCounter(siteId, dt.Rows[j][0].ToString());
                if (dt_c.Rows.Count > 0)
                {
                    if (dt_c.Rows[0][0].ToString() != "")
                    {
                        i_counter = Convert.ToInt32(dt_c.Rows[0][0].ToString()) - 1;
                    }
                }

                da = new mydataaccess2();
                DataTable dt1 = new DataTable();
                dt1 = da.select_site_que_log_pdf_tx_car(checksheet, siteId, i_counter, j + 1, "");


                int sec = 0;
                for (int k = 0; k < dt1.Rows.Count; k++)
                {

                    PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell.Width = 20;
                    data.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell1_.Width = 40;
                    data.AddCell(cell100_);

                    if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }


                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    //cell5_.Width = 20;
                    //    cell500_.Rowspan = dt1.Rows.Count;
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    data.AddCell(cell500_);

                    PdfPCell cell4001 = new PdfPCell(new Phrase(dt1.Rows[k][6].ToString(), new Font(calibri_Data)));
                    cell4001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell4001);


                    PdfPCell cell5001 = new PdfPCell(new Phrase(dt1.Rows[k][7].ToString(), new Font(calibri_Data)));
                    cell5001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell5001);

                }

            }
            documentPdf.Add(data);

            da = new mydataaccess2();
            dt = new DataTable();
            dt = da.select_categories_by_sheet("Active");
            for (int m = 0; m < dt.Rows.Count; m++)
            {
                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt_c = new DataTable();
                dt_c = da1.pm_GetinspectionCounter(siteId, dt.Rows[m][0].ToString());
                if (dt_c.Rows.Count > 0)
                {
                    if (dt_c.Rows[0][0].ToString() != "")
                    {
                        i_counter = Convert.ToInt32(dt_c.Rows[0][0].ToString()) - 1;
                    }
                }

                da = new mydataaccess2();
                DataTable dt_photo = new DataTable();

                dt_photo = da.select_photos_pdf_tx_car(siteId, i_counter, m + 1, dt.Rows[m][0].ToString(), "");


                if (dt_photo.Rows.Count > 0)//------------------------------------
                {
                    if (m == 0)
                    {
                        PdfPTable new_t1 = new PdfPTable(1);

                        new_t1.WidthPercentage = 100f;

                        Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                         Font.BOLD));

                        PdfPCell new_cell_ = new PdfPCell(head_ph_);
                        new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                        new_cell_.FixedHeight = 20f;
                        new_t1.AddCell(new_cell_);
                        Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                        ));

                        documentPdf.Add(new_t1);
                        documentPdf.Add(break2);
                    }
                    if (dt_photo.Rows.Count > 0)
                    {
                        PdfPTable new_t = new PdfPTable(1);
                        new_t.WidthPercentage = 100f;

                        Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                         Font.BOLD));

                        PdfPCell new_cell = new PdfPCell(head_ph);
                        new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        new_cell.FixedHeight = 12f;
                        new_t.AddCell(new_cell);
                        documentPdf.Add(new_t);

                        PdfPTable photo = new PdfPTable(4);
                        PdfPCell photo_cell = new PdfPCell();
                        photo.WidthPercentage = 100;
                        int flag = 0;
                        for (int p = 0; p < dt_photo.Rows.Count; p++)
                        {
                            if (flag == 4)
                            {
                                flag = 0;
                            }
                            try
                            {
                                if (dt_photo.Rows.Count % 4 == 0)
                                {
                                    PdfPTable pp = new PdfPTable(1);
                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                    PdfPCell ppc = new PdfPCell(name);
                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pp.AddCell(ppc);
                                    //  imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                    string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                    iTextSharp.text.Image image_photo;
                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                    image_photo.SetAbsolutePosition(50, 50);
                                    image_photo.ScaleAbsolute(70f, 70f);
                                    pp.AddCell(image_photo);
                                    photo_cell = new PdfPCell(pp);
                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //  photo_cell.FixedHeight = 220;
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                }
                                else
                                {
                                    if (p == dt_photo.Rows.Count - 1)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //  imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        // photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                        photo.AddCell(photo_cell);

                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;

                                        for (int b = 0; b < 4 - flag; b++)
                                        {
                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                        }


                                    }
                                    else
                                    {
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;
                                    }
                                }
                            }
                            catch
                            {
                                photo_cell = new PdfPCell();
                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                photo.AddCell(photo_cell);
                            }
                        }
                        documentPdf.Add(photo);
                        Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                        documentPdf.Add(break23);
                    }

                }//---------------------------


            }//----
        }
        //-----------------Node complete
    }
    #endregion

    #region "Checksheet Pdf Creation"

    #region CheckSheet Array
    private static string[,] arrayChecksheet = new string[,]
         {
                     {"TxEquip", "Tx & IP Equipment"},
                     {"BTS_New", "PrM Checklist BTS"},
                     {"MW", "PrM Chklist_MW & Mux_Mod"},
                     {"TMEA", "PrM Check list TMEA"},
                     {"BSC", "BSC Checks Points"},
                     {"InfraVisual", "Infra Visual"},
                     {"SiteLocation", "Site Location"},
                     {"HVAC", "Air conditioner & FCU"},
                     {"X_mer", "Transformer"},
                     {"EB", "Electricity Board"},
                     {"PIU", "PIU"},
                     {"SMPS", "SMPS & other electrical"},
                     {"BB", "Battery Bank"},
                     {"DG", "Diesel Generator"},
                     {"Earthing", "Earthing"},
                     {"Shelter", "Shelter"},
                     {"THCA_PHCA", "THCA/PHCA"},
                     {"FPS", "Fall Protection System"},
                     {"FSS", "Fire Supression System"},
                     {"Alarm", "External Alarms"},
                     {"EMF", "EMF Signage"},
                     {"General", "General Maintenance"}
         };
    #endregion

    private void ChecksheetPdfCreation(ref Document documentPdf, string siteId, string checksheets)
    {
        try
        {
            ChecksheetsHeader(ref documentPdf, siteId);

            PdfPTable pTableChecksheets = new PdfPTable(5);
            pTableChecksheets.WidthPercentage = 100f;
            pTableChecksheets.SetWidths(new float[] { 1, 3, 1, 1, 1 });

            #region pTableChecksheets Header labels

            Font fontCalibriHeader = SetFont("Calibri", 15, "bold", "black");

            PdfPCell pCellBody1 = new PdfPCell(new Phrase("Sr. No.", fontCalibriHeader));
            pCellBody1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            pCellBody1.HorizontalAlignment = Element.ALIGN_CENTER;
            pTableChecksheets.AddCell(pCellBody1);

            PdfPCell pCellBody2 = new PdfPCell(new Phrase("Description", fontCalibriHeader));
            pCellBody2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            pCellBody2.HorizontalAlignment = Element.ALIGN_CENTER;
            pTableChecksheets.AddCell(pCellBody2);

            PdfPCell pCellBody3 = new PdfPCell(new Phrase("Specification", fontCalibriHeader));
            pCellBody3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            pCellBody3.HorizontalAlignment = Element.ALIGN_CENTER;
            pTableChecksheets.AddCell(pCellBody3);

            PdfPCell pCellBody4 = new PdfPCell(new Phrase("Answer", fontCalibriHeader));
            pCellBody4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            pCellBody4.HorizontalAlignment = Element.ALIGN_CENTER;
            pTableChecksheets.AddCell(pCellBody4);


            PdfPCell pCellBody5 = new PdfPCell(new Phrase("Comments / Explanations", fontCalibriHeader));
            pCellBody5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            pCellBody5.HorizontalAlignment = Element.ALIGN_CENTER;
            pTableChecksheets.AddCell(pCellBody5);

            #endregion

            for (int i = 0; i <= arrayChecksheet.GetUpperBound(0); i++)
            {
                string checksheetType = arrayChecksheet[i, 0]; //type
                string checksheetDiscription = arrayChecksheet[i, 1]; //Discription

                int srnocounter = 1;
                int stepcounter = 1;

                if (checksheets.Contains(checksheetType))
                {
                    ChecksheetHeader(siteId, checksheetType, checksheetDiscription, ref srnocounter, ref pTableChecksheets);
                    StepLog(siteId, checksheetType, checksheetDiscription, ref srnocounter, ref stepcounter, ref pTableChecksheets);
                    srnocounter = srnocounter + 1;
                }
            }
            documentPdf.Add(pTableChecksheets);

            documentPdf.NewPage();

            int vsitemasterinsp = 0;

            for (int i = 0; i <= arrayChecksheet.GetUpperBound(0); i++)
            {
                string checksheetType = arrayChecksheet[i, 0]; //type
                string checksheetDiscription = arrayChecksheet[i, 1]; //Discription

                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt = new DataTable();
                dt = da1.pm_GetinspectionCounter(siteId, checksheetType);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "")
                    {
                        vsitemasterinsp = Convert.ToInt32(dt.Rows[0][0].ToString()) - 1;
                    }
                }

                if (checksheets.Contains(checksheetType))
                {
                    ChecksheetsPhotos(siteId, checksheetType, checksheetDiscription, ref vsitemasterinsp, ref documentPdf);
                }
            }

            documentPdf.NewPage();

            for (int i = 0; i <= arrayChecksheet.GetUpperBound(0); i++)
            {
                string checksheetType = arrayChecksheet[i, 0]; //type
                string checksheetDiscription = arrayChecksheet[i, 1]; //Discription

                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt = new DataTable();
                dt = da1.pm_GetinspectionCounter(siteId, checksheetType);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0][0].ToString() != "")
                    {
                        vsitemasterinsp = Convert.ToInt32(dt.Rows[0][0].ToString()) - 1;
                    }
                }

                if (checksheets.Contains(checksheetType))
                {
                    if (checksheetType != "TxEquip")
                    {
                        ChecksheetsCar(ref documentPdf, siteId, checksheetType, vsitemasterinsp);
                    }
                    else
                    {
                        ChecksheetsTxEquipCar(ref documentPdf, siteId, checksheetType, vsitemasterinsp);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void ChecksheetsHeader(ref Document documentPdf, string siteId)
    {
        PdfPTable pTableHeader = new PdfPTable(1);
        pTableHeader.WidthPercentage = 100f;

        Font fontCalibriDisplay = SetFont("Calibri", 10, "normal", "black");

        PdfPCell pCellHeader1 = new PdfPCell(new Phrase("Active Preventive Maintenance Checklist V1", SetFont("Calibri", 15, "bold", "black")));
        pCellHeader1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
        pCellHeader1.HorizontalAlignment = Element.ALIGN_CENTER;
        pCellHeader1.VerticalAlignment = Element.ALIGN_MIDDLE;
        pCellHeader1.BorderColor = BaseColor.BLACK;
        pCellHeader1.FixedHeight = 20f;
        pTableHeader.AddCell(pCellHeader1);

        PdfPTable pTableInner1 = new PdfPTable(2);
        pTableInner1.WidthPercentage = 100f;

        mydataaccess1 mDa = new mydataaccess1();
        DataTable dataTable = new DataTable();
        dataTable = mDa.pm_select_site_data_all_pdf(siteId);

        Phrase phrase1 = new Phrase("Circle : " + dataTable.Rows[0][1].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner1 = new PdfPCell(phrase1);
        pCellInner1.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase2 = new Phrase("Site ID : " + siteId, new Font(fontCalibriDisplay));
        PdfPCell pCellInner2 = new PdfPCell(phrase2);
        pCellInner2.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner2.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase3 = new Phrase("Address :" + dataTable.Rows[0][0].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner3 = new PdfPCell(phrase3);
        pCellInner3.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner3.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase4 = new Phrase("Site Incharge Name & No. : " + dataTable.Rows[0][2].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner4 = new PdfPCell(phrase4);
        pCellInner4.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner4.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase5 = new Phrase("ISPTW : " + dataTable.Rows[0][3].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner5 = new PdfPCell(phrase5);
        pCellInner5.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner5.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        Phrase phrase6 = new Phrase("PTWID : " + dataTable.Rows[0][4].ToString(), new Font(fontCalibriDisplay));
        PdfPCell pCellInner6 = new PdfPCell(phrase6);
        pCellInner6.HorizontalAlignment = Element.ALIGN_LEFT;
        pCellInner6.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));

        pTableInner1.AddCell(pCellInner1);
        pTableInner1.AddCell(pCellInner2);
        pTableInner1.AddCell(pCellInner3);
        pTableInner1.AddCell(pCellInner4);
        //pTableInner1.AddCell(pCellInner5);
        //pTableInner1.AddCell(pCellInner6);

        PdfPCell pCellBlank = new PdfPCell(new Phrase(""));
        pCellBlank.Colspan = 2;
        pCellBlank.FixedHeight = 15f;
        pTableInner1.AddCell(pCellBlank);

        pTableHeader.AddCell(pTableInner1);

        documentPdf.Add(pTableHeader);
    }

    public void ChecksheetHeader(string siteId, string checksheetType, string checksheetDiscription, ref int srnocounter, ref PdfPTable pTableBody)
    {
        try
        {
            Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
            Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

            #region Fine Site Count
            mydataaccess1 da1 = new mydataaccess1();
            DataTable pm_findsitecount = new DataTable();
            pm_findsitecount = da1.pm_findcount(siteId, checksheetType.ToString());
            string AllCount = "";
            if (pm_findsitecount.Rows.Count > 0)
            {
                AllCount = " Date Of Active PM : " + pm_findsitecount.Rows[0][2].ToString() + " " + pm_findsitecount.Rows[0][1].ToString();
                AllCount += " Done By : " + pm_findsitecount.Rows[0][3].ToString();
                AllCount += " Contact Number : " + pm_findsitecount.Rows[0][4].ToString();
                AllCount += " Next Due Date : " + pm_findsitecount.Rows[0][5].ToString();
                AllCount += " Assigned To : " + pm_findsitecount.Rows[0][6].ToString();
                AllCount += " ISPTW : " + pm_findsitecount.Rows[0][7].ToString();
                AllCount += " PTWID : " + pm_findsitecount.Rows[0][8].ToString();
            }
            #endregion
            #region Check Checksheet Com,NonCom,PartialCom
            string CheckStatus = "";
            da1 = new mydataaccess1();
            DataTable DTCheckStatus = new DataTable();
            DTCheckStatus = da1.pm_checkStatus(siteId, checksheetType);
            if (DTCheckStatus.Rows.Count > 0)
            {
                CheckStatus = DTCheckStatus.Rows[0][2].ToString();
            }
            #endregion

            if (checksheetType.ToString() == "SiteLocation")
            {
                PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell.Width = 20;
                pTableBody.AddCell(cellcount);

                PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_count);

                PdfPCell cell1_Specount = new PdfPCell(new Phrase("", new Font(Site_Heading)));
                cell1_Specount.Colspan = 3;
                cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                // cell1_.Width = 40;
                pTableBody.AddCell(cell1_Specount);
            }
            else
            {
                if (CheckStatus == "Completed")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_Specount);
                }
                if (CheckStatus == "NonComplete" || CheckStatus == "")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_Specount);
                }
                if (CheckStatus == "PartialCompleted")
                {
                    PdfPCell cellcount = new PdfPCell(new Phrase(srnocounter.ToString(), new Font(Site_Heading)));
                    cellcount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cellcount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cellcount);

                    PdfPCell cell1_count = new PdfPCell(new Phrase(checksheetDiscription.ToString(), new Font(Site_Heading)));
                    cell1_count.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell1_count.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_count);

                    PdfPCell cell1_Specount = new PdfPCell(new Phrase(AllCount, new Font(Site_Heading)));
                    cell1_Specount.Colspan = 3;
                    cell1_Specount.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                    cell1_Specount.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell1_Specount);
                }
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void StepLog(string siteId, string checksheetType, string checksheetDiscription, ref int srnocounter, ref int stepcounter, ref PdfPTable pTableBody)
    {
        Font Site_Heading = FontFactory.GetFont("Calibri", 8, Font.BOLD, BaseColor.BLACK);
        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        #region Step
        //    for (int j = 0; j < pm_getallchecksheet.Rows.Count; j++)
        {
            mydataaccess1 da1 = new mydataaccess1();
            DataTable dt1 = new DataTable();
            dt1 = da1.pm_select_site_que_log_pdf(checksheetType.ToString(), siteId);

            int sec = 0;
            // stepcounter = stepcounter + 1;
            if (dt1.Rows.Count > 0)
            {
                int counter_t = 0;
                string Kye_Count = "";

                for (int k = 0; k < dt1.Rows.Count; k++)
                {
                    if (checksheetType == "MW")
                    {

                        if (Kye_Count != dt1.Rows[k][3].ToString())
                        {
                            counter_t = 0;
                        }

                        Kye_Count = dt1.Rows[k][3].ToString();
                        // flagCheck = "True";
                        if (Kye_Count == "1" || Kye_Count == "2" || Kye_Count == "3" || Kye_Count == "4"
                            || Kye_Count == "5" || Kye_Count == "6" || Kye_Count == "7" || Kye_Count == "8" || Kye_Count == "9"
                            || Kye_Count == "10" || Kye_Count == "11" || Kye_Count == "12" || Kye_Count == "13"
                            || Kye_Count == "14" || Kye_Count == "15" || Kye_Count == "16" || Kye_Count == "17"
                            || Kye_Count == "18" || Kye_Count == "19" || Kye_Count == "20" || Kye_Count == "21"
                            || Kye_Count == "22" || Kye_Count == "23" || Kye_Count == "24" || Kye_Count == "25"
                            || Kye_Count == "26" || Kye_Count == "27" || Kye_Count == "28" || Kye_Count == "29"
                            || Kye_Count == "30" || Kye_Count == "31" || Kye_Count == "32" || Kye_Count == "33"
                            || Kye_Count == "34" || Kye_Count == "35" || Kye_Count == "36" || Kye_Count == "37"
                            || Kye_Count == "38" || Kye_Count == "39" || Kye_Count == "40")
                        {
                            if (counter_t == 0)
                            {
                                PdfPCell cell00_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell00_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                cell00_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                // cell.Width = 20;
                                pTableBody.AddCell(cell00_key);
                                PdfPCell cell100_key = new PdfPCell(new Phrase(checksheetType.ToString() + "(" + Kye_Count + ")", new Font(calibri_Data)));
                                cell100_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell100_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_key);
                                PdfPCell cell100_spe_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell100_spe_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_spe_key);
                                PdfPCell cell400_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell400_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell4_.Width = 20;
                                pTableBody.AddCell(cell400_key);
                                PdfPCell cell500_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell500_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_key.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                pTableBody.AddCell(cell500_key);
                                counter_t = counter_t + 1;
                            }
                        }
                    }
                    else if (checksheetType == "X_mer" || checksheetType == "PIU" || checksheetType == "SMPS" || checksheetType == "BB" || checksheetType == "DG"
                                || checksheetType == "HVAC" || checksheetType == "Earthing" || checksheetType == "Shelter" || checksheetType == "FPS")
                    {
                        if (Kye_Count != dt1.Rows[k][3].ToString())
                        {
                            counter_t = 0;
                        }

                        Kye_Count = dt1.Rows[k][3].ToString();
                        // flagCheck = "True";
                        if (Kye_Count == "1" || Kye_Count == "2" || Kye_Count == "3" || Kye_Count == "4"
                        || Kye_Count == "5" || Kye_Count == "7" || Kye_Count == "8" || Kye_Count == "9"
                        || Kye_Count == "10" || Kye_Count == "11" || Kye_Count == "12" || Kye_Count == "13"
                        || Kye_Count == "14" || Kye_Count == "15" || Kye_Count == "16")
                        {
                            if (counter_t == 0)
                            {
                                PdfPCell cell00_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell00_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                cell00_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                // cell.Width = 20;
                                pTableBody.AddCell(cell00_key);
                                PdfPCell cell100_key = new PdfPCell(new Phrase(checksheetType.ToString() + "(" + Kye_Count + ")", new Font(calibri_Data)));
                                cell100_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                cell100_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_key);
                                PdfPCell cell100_spe_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell100_spe_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                // cell1_.Width = 40;
                                pTableBody.AddCell(cell100_spe_key);
                                PdfPCell cell400_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell400_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell4_.Width = 20;
                                pTableBody.AddCell(cell400_key);
                                PdfPCell cell500_key = new PdfPCell(new Phrase("", new Font(calibri_Data)));
                                cell500_key.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#5F9EA0"));
                                //cell5_.Width = 20;
                                //    cell500_.Rowspan = dt1.Rows.Count;
                                cell500_key.VerticalAlignment = Element.ALIGN_MIDDLE;
                                cell500_key.HorizontalAlignment = Element.ALIGN_CENTER;
                                pTableBody.AddCell(cell500_key);
                                counter_t = counter_t + 1;
                            }
                        }
                    }
                    PdfPCell cell00 = new PdfPCell(new Phrase((srnocounter + "." + stepcounter).ToString(), new Font(calibri_Data)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    cell00.HorizontalAlignment = Element.ALIGN_CENTER;
                    // cell.Width = 20;
                    pTableBody.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(calibri_Data)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    // cell1_.Width = 40;
                    pTableBody.AddCell(cell100_);

                    PdfPCell cell100_spe = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(calibri_Data)));
                    cell100_spe.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    pTableBody.AddCell(cell100_spe);

                    if (dt1.Rows[k][1].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        pTableBody.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][1].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        pTableBody.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][1].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        pTableBody.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        pTableBody.AddCell(cell400_);
                    }

                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][5].ToString(), new Font(calibri_Data)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    pTableBody.AddCell(cell500_);

                    stepcounter = stepcounter + 1;
                }
            }
        }
        #endregion
    }

    public void ChecksheetsPhotos(string siteId, string checksheetType, string checksheetDiscription, ref int vsitemasterinsp, ref Document documentPdf)
    {
        try
        {
            string imageFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\";
            
            if (vsitemasterinsp >= 0)
            {
                #region temp
                mydataaccess1 da1 = new mydataaccess1();

                DataTable dt = da1.pm_select_categories_by_sheet(checksheetType.ToString());
                for (int m = 0; m < dt.Rows.Count; m++)
                {
                    da1 = new mydataaccess1();
                    DataTable dt_photo = new DataTable();

                    dt_photo = da1.pm_select_photos_pdf(siteId, vsitemasterinsp, (m + 1), checksheetType);
                    if (dt_photo.Rows.Count > 0)
                    {
                        if (m == 0)
                        {
                            PdfPTable new_t1 = new PdfPTable(1);

                            new_t1.WidthPercentage = 100f;

                            Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                             Font.BOLD));

                            PdfPCell new_cell_ = new PdfPCell(head_ph_);
                            new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                            new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                            //new_cell_.FixedHeight = 20f;
                            new_t1.AddCell(new_cell_);
                            Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                            ));

                            documentPdf.Add(new_t1);
                            documentPdf.Add(break2);
                        }
                        if (dt_photo.Rows.Count > 0)
                        {
                            PdfPTable new_t = new PdfPTable(1);
                            new_t.WidthPercentage = 100f;

                            Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                             Font.BOLD));

                            PdfPCell new_cell = new PdfPCell(head_ph);
                            new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                            new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                            //new_cell.FixedHeight = 20f;

                            new_t.AddCell(new_cell);
                            documentPdf.Add(new_t);

                            PdfPTable photo = new PdfPTable(4);
                            PdfPCell photo_cell = new PdfPCell();
                            photo.WidthPercentage = 100;
                            int flag = 0;
                            for (int p = 0; p < dt_photo.Rows.Count; p++)
                            {
                                try
                                {
                                    if (dt_photo.Rows.Count % 4 == 0)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        // imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                    }
                                    else
                                    {
                                        if (p == dt_photo.Rows.Count - 1)
                                        {
                                            if (flag == 4)
                                            {
                                                flag = 0;
                                            }
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                            // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);

                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;

                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                            photo.AddCell(photo_cell);

                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            //photo_cell.FixedHeight = 50F;
                                            photo.AddCell(photo_cell);
                                            flag++;

                                            for (int b = 0; b < 4 - flag; b++)
                                            {
                                                photo_cell = new PdfPCell();
                                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                                //photo_cell.FixedHeight = 100;
                                                photo.AddCell(photo_cell);
                                            }


                                        }
                                        else
                                        {
                                            // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            PdfPTable pp = new PdfPTable(1);
                                            Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                            PdfPCell ppc = new PdfPCell(name);
                                            ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                            pp.AddCell(ppc);
                                            string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                            //imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            // imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                            iTextSharp.text.Image image_photo;
                                            image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                            image_photo.SetAbsolutePosition(50, 50);
                                            image_photo.ScaleAbsolute(70f, 70f);
                                            pp.AddCell(image_photo);
                                            photo_cell = new PdfPCell(pp);
                                            photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                            photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                            // photo_cell.FixedHeight = 100;
                                            //  photo_cell.FixedHeight = 220;
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                            flag++;
                                        }
                                    }
                                }
                                catch
                                {
                                    photo_cell = new PdfPCell();
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    // photo_cell.FixedHeight = 100;
                                    photo.AddCell(photo_cell);
                                }
                            }
                            documentPdf.Add(photo);
                            Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                            documentPdf.Add(break23);
                        }
                    }
                }
                #endregion
            }

        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void ChecksheetsCar(ref Document documentPdf, string siteId, string sheet, int i_counter)
    {
        string imageFilePath = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\";

        PdfPTable data = new PdfPTable(4);
        data.WidthPercentage = 100f;
        data.SetWidths(new float[] { 1, 4, 1, 1 });

        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        //mydataaccess1 da2 = new mydataaccess1();
        //DataTable dt_node = new DataTable();
        //dt_node = da2.select_categories_by_sheet(sheet);
        string checksheet = "";
        string checkstatus = "";
        // for (int d = 0; d < dt_node.Rows.Count; d++)
        {

            PdfPTable head = new PdfPTable(1);
            head.WidthPercentage = 100f;
            //checksheet = dt_node.Rows[d][0].ToString();

            documentPdf.NewPage();

            PdfPTable head1 = new PdfPTable(1);
            head1.WidthPercentage = 100f;
            Phrase p1header1 = new Phrase("Corrective Action Report", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD));
            PdfPCell c1 = new PdfPCell(p1header1);
            c1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFA500"));
            c1.HorizontalAlignment = Element.ALIGN_CENTER;
            c1.VerticalAlignment = Element.ALIGN_MIDDLE;
            c1.FixedHeight = 20f;
            head1.AddCell(c1);
            documentPdf.Add(head1);

            Phrase p1header = new Phrase(checksheet, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
            PdfPCell c = new PdfPCell(p1header);
            // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFA500"));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.BorderColor = iTextSharp.text.BaseColor.BLACK;
            c.FixedHeight = 20f;
            head.AddCell(c);

            PdfPCell blank = new PdfPCell(new Phrase(""));
            blank.Colspan = 2;
            blank.FixedHeight = 15f;


            documentPdf.Add(head);
            documentPdf.Add(blank);

            //mydataaccess2 da = new mydataaccess2();
            //DataTable dt = new DataTable();
            //dt = da.select_categories_by_sheet(sheet);

            data = new PdfPTable(6);
            data.WidthPercentage = 100f;
            data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });
            PdfPCell cell = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell.Width = 20;
            data.AddCell(cell);
            PdfPCell cell1_ = new PdfPCell(new Phrase("Question", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell1_.Width = 40;
            data.AddCell(cell1_);


            PdfPCell cell4_ = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell4_);

            PdfPCell cell5_ = new PdfPCell(new Phrase("Action Remarks", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell5_);

            cell4_ = new PdfPCell(new Phrase("Violation Severity", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell4_);

            cell5_ = new PdfPCell(new Phrase("Punch Point(External/Internal)", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell5_);


            /*   da = new mydataaccess2();
               dt = new DataTable();
               dt = da.select_categories_by_sheet(sheet);*/

            //for (int j = 0; j < dt.Rows.Count; j++)
            {
                checksheet = sheet; //dt_node.Rows[j][0].ToString();
                int flag = 0;
                //PdfPCell cell0 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                PdfPCell cell0 = new PdfPCell(new Phrase((1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell0.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell.Width = 20;
                data.AddCell(cell0);
                //PdfPCell cell10_ = new PdfPCell(new Phrase(dt.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                PdfPCell cell10_ = new PdfPCell(new Phrase(sheet, new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell10_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell1_.Width = 40;
                data.AddCell(cell10_);


                PdfPCell cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                PdfPCell cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);

                cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);


                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt_c = new DataTable();
                //dt_c = da1.pm_GetinspectionCounter(siteId, dt.Rows[j][0].ToString());
                dt_c = da1.pm_GetinspectionCounter(siteId, sheet);
                if (dt_c.Rows.Count > 0)
                {
                    if (dt_c.Rows[0][0].ToString() != "")
                    {
                        i_counter = Convert.ToInt32(dt_c.Rows[0][0].ToString()) - 1;
                    }
                }

                mydataaccess2 da2 = new mydataaccess2();
                DataTable dt1 = new DataTable();
                //dt1 = da2.select_site_que_log_pdf_tx_car(checksheet, siteId, i_counter, j + 1, "");
                dt1 = da2.select_site_que_log_pdf_tx_car(checksheet, siteId, i_counter, 1, "");


                int sec = 0;
                for (int k = 0; k < dt1.Rows.Count; k++)
                {

                    PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell.Width = 20;
                    data.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell1_.Width = 40;
                    data.AddCell(cell100_);

                    if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }


                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    //cell5_.Width = 20;
                    //    cell500_.Rowspan = dt1.Rows.Count;
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    data.AddCell(cell500_);

                    PdfPCell cell4001 = new PdfPCell(new Phrase(dt1.Rows[k][6].ToString(), new Font(calibri_Data)));
                    cell4001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell4001);


                    PdfPCell cell5001 = new PdfPCell(new Phrase(dt1.Rows[k][7].ToString(), new Font(calibri_Data)));
                    cell5001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell5001);

                }

            }
            documentPdf.Add(data);

            mydataaccess2 da = new mydataaccess2();
            DataTable dt = new DataTable();
            dt = da.select_categories_by_sheet("Active");
            //for (int m = 0; m < dt.Rows.Count; m++)
            {
                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt_c = new DataTable();
                dt_c = da1.pm_GetinspectionCounter(siteId, sheet); //da1.pm_GetinspectionCounter(siteId, dt.Rows[m][0].ToString());
                if (dt_c.Rows.Count > 0)
                {
                    if (dt_c.Rows[0][0].ToString() != "")
                    {
                        i_counter = Convert.ToInt32(dt_c.Rows[0][0].ToString()) - 1;
                    }
                }

                da = new mydataaccess2();
                DataTable dt_photo = new DataTable();

                //dt_photo = da.select_photos_pdf_tx_car(siteId, i_counter, m + 1, dt.Rows[m][0].ToString(), "");
                dt_photo = da.select_photos_pdf_tx_car(siteId, i_counter, 1, sheet, "");


                if (dt_photo.Rows.Count > 0)//------------------------------------
                {
                    //if (m == 0)
                    {
                        PdfPTable new_t1 = new PdfPTable(1);

                        new_t1.WidthPercentage = 100f;

                        Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                         Font.BOLD));

                        PdfPCell new_cell_ = new PdfPCell(head_ph_);
                        new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                        new_cell_.FixedHeight = 20f;
                        new_t1.AddCell(new_cell_);
                        Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                        ));

                        documentPdf.Add(new_t1);
                        documentPdf.Add(break2);
                    }
                    if (dt_photo.Rows.Count > 0)
                    {
                        PdfPTable new_t = new PdfPTable(1);
                        new_t.WidthPercentage = 100f;

                        //Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD));
                        Phrase head_ph = new Phrase(sheet, new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD));


                        PdfPCell new_cell = new PdfPCell(head_ph);
                        new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        new_cell.FixedHeight = 12f;
                        new_t.AddCell(new_cell);
                        documentPdf.Add(new_t);

                        PdfPTable photo = new PdfPTable(4);
                        PdfPCell photo_cell = new PdfPCell();
                        photo.WidthPercentage = 100;
                        int flag = 0;
                        for (int p = 0; p < dt_photo.Rows.Count; p++)
                        {
                            if (flag == 4)
                            {
                                flag = 0;
                            }
                            try
                            {
                                if (dt_photo.Rows.Count % 4 == 0)
                                {
                                    PdfPTable pp = new PdfPTable(1);
                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                    PdfPCell ppc = new PdfPCell(name);
                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pp.AddCell(ppc);
                                    //  imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                    string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                    iTextSharp.text.Image image_photo;
                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                    image_photo.SetAbsolutePosition(50, 50);
                                    image_photo.ScaleAbsolute(70f, 70f);
                                    pp.AddCell(image_photo);
                                    photo_cell = new PdfPCell(pp);
                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //  photo_cell.FixedHeight = 220;
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                }
                                else
                                {
                                    if (p == dt_photo.Rows.Count - 1)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //  imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        // photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                        photo.AddCell(photo_cell);

                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;

                                        for (int b = 0; b < 4 - flag; b++)
                                        {
                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                        }


                                    }
                                    else
                                    {
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        string imag_file1 = Path.Combine(imageFilePath, dt_photo.Rows[p][0].ToString());
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;
                                    }
                                }
                            }
                            catch
                            {
                                photo_cell = new PdfPCell();
                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                photo.AddCell(photo_cell);
                            }
                        }
                        documentPdf.Add(photo);
                        Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                        documentPdf.Add(break23);
                    }

                }//---------------------------


            }//----
        }
        //-----------------Node complete
    }

    private void ChecksheetsTxEquipCar(ref Document documentPdf, string siteId, string sheet, int i_counter)
    {
        PdfPTable data = new PdfPTable(6);
        data.WidthPercentage = 100f;
        data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });

        Font calibri_Data = FontFactory.GetFont("Calibri", 8, Font.NORMAL, BaseColor.BLACK);

        ////-----Nodeid wise
        mydataaccess1 da2 = new mydataaccess1();
        DataTable dt_node = new DataTable();
        dt_node = da2.select_nodes_from_nss_car_nodes(siteId);
        string nodeid = "";
        string checkstatus = "";
        for (int d = 0; d < dt_node.Rows.Count; d++)
        {

            PdfPTable head = new PdfPTable(1);
            head.WidthPercentage = 100f;
            nodeid = dt_node.Rows[d][0].ToString();

            documentPdf.NewPage();
            if (d == 0)
            {
                PdfPTable head1 = new PdfPTable(1);
                head1.WidthPercentage = 100f;
                Phrase p1header1 = new Phrase("Corrective Action Report", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD));
                PdfPCell c1 = new PdfPCell(p1header1);
                c1.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFA500"));
                c1.HorizontalAlignment = Element.ALIGN_CENTER;
                c1.VerticalAlignment = Element.ALIGN_MIDDLE;
                c1.FixedHeight = 20f;
                head1.AddCell(c1);
                documentPdf.Add(head1);
            }
            Phrase p1header = new Phrase(nodeid, new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD));
            PdfPCell c = new PdfPCell(p1header);
            // c.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFA500"));
            c.HorizontalAlignment = Element.ALIGN_CENTER;
            c.VerticalAlignment = Element.ALIGN_MIDDLE;
            c.BorderColor = iTextSharp.text.BaseColor.BLACK;
            c.FixedHeight = 20f;
            head.AddCell(c);

            PdfPCell blank = new PdfPCell(new Phrase(""));
            blank.Colspan = 2;
            blank.FixedHeight = 15f;


            documentPdf.Add(head);
            documentPdf.Add(blank);

            mydataaccess2 da = new mydataaccess2();
            DataTable dt = new DataTable();
            dt = da.select_categories_by_sheet(sheet);

            data = new PdfPTable(6);
            data.WidthPercentage = 100f;
            data.SetWidths(new float[] { 1, 4, 1, 1, 1, 1 });
            PdfPCell cell = new PdfPCell(new Phrase("Sr. No.", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell.Width = 20;
            data.AddCell(cell);
            PdfPCell cell1_ = new PdfPCell(new Phrase("Question", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell1_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            // cell1_.Width = 40;
            data.AddCell(cell1_);


            PdfPCell cell4_ = new PdfPCell(new Phrase("Status", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell4_);

            PdfPCell cell5_ = new PdfPCell(new Phrase("Action Remarks", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell5_);

            cell4_ = new PdfPCell(new Phrase("Violation Severity", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell4_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell4_.Width = 20;
            data.AddCell(cell4_);

            cell5_ = new PdfPCell(new Phrase("Punch Point(External/Internal)", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
            cell5_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
            //cell5_.Width = 20;
            data.AddCell(cell5_);



            /*   da = new mydataaccess2();
               dt = new DataTable();
               dt = da.select_categories_by_sheet(sheet);*/

            for (int j = 2; j < dt.Rows.Count; j++)
            {
                //flag = 0;
                PdfPCell cell0 = new PdfPCell(new Phrase((j + 1).ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell0.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell.Width = 20;
                data.AddCell(cell0);
                PdfPCell cell10_ = new PdfPCell(new Phrase(dt.Rows[j][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell10_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                // cell1_.Width = 40;
                data.AddCell(cell10_);


                PdfPCell cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                PdfPCell cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);

                cell40_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell40_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell4_.Width = 20;
                data.AddCell(cell40_);
                cell50_ = new PdfPCell(new Phrase("", new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                cell50_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#817679"));
                //cell5_.Width = 20;
                data.AddCell(cell50_);

                mydataaccess1 da1 = new mydataaccess1();
                DataTable dt_c = new DataTable();
                dt_c = da1.pm_GetinspectionCounter_tx(siteId, dt.Rows[j][0].ToString(), nodeid);
                if (dt_c.Rows.Count > 0)
                {
                    if (dt_c.Rows[0][0].ToString() != "")
                    {
                        i_counter = Convert.ToInt32(dt_c.Rows[0][0].ToString()) - 1;
                    }
                }

                DataTable dt1 = new DataTable();
                if (j == 2)
                {
                    da = new mydataaccess2();

                    dt1 = da.select_site_que_log_pdf_tx_car(sheet, siteId, i_counter, j + 1);

                }
                else
                {
                    da = new mydataaccess2();

                    dt1 = da.select_site_que_log_pdf_tx_car(sheet, siteId, i_counter, j + 1, nodeid);
                }
                int sec = 0;
                for (int k = 0; k < dt1.Rows.Count; k++)
                {

                    PdfPCell cell00 = new PdfPCell(new Phrase(dt1.Rows[k][0].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell00.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell.Width = 20;
                    data.AddCell(cell00);
                    PdfPCell cell100_ = new PdfPCell(new Phrase(dt1.Rows[k][1].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell100_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    // cell1_.Width = 40;
                    data.AddCell(cell100_);

                    if (dt1.Rows[k][3].ToString().ToUpper() == "YES")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#008000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NO")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FF0000"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else if (dt1.Rows[k][3].ToString().ToUpper() == "NA")
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFF00"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }
                    else
                    {
                        PdfPCell cell400_ = new PdfPCell(new Phrase(dt1.Rows[k][3].ToString(), new Font(calibri_Data)));
                        cell400_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                        //cell4_.Width = 20;
                        data.AddCell(cell400_);
                    }


                    PdfPCell cell500_ = new PdfPCell(new Phrase(dt1.Rows[k][4].ToString(), new Font(Font.FontFamily.HELVETICA, 6f, Font.BOLD)));
                    cell500_.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#F5F5DC"));
                    //cell5_.Width = 20;
                    //    cell500_.Rowspan = dt1.Rows.Count;
                    cell500_.VerticalAlignment = Element.ALIGN_MIDDLE;
                    cell500_.HorizontalAlignment = Element.ALIGN_CENTER;
                    data.AddCell(cell500_);
                    // flag++;

                    PdfPCell cell4001 = new PdfPCell(new Phrase(dt1.Rows[k][6].ToString(), new Font(calibri_Data)));
                    cell4001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell4001);


                    PdfPCell cell5001 = new PdfPCell(new Phrase(dt1.Rows[k][7].ToString(), new Font(calibri_Data)));
                    cell5001.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#FFFFFF"));
                    //cell4_.Width = 20;
                    data.AddCell(cell5001);
                }

            }
            documentPdf.Add(data);


            for (int m = 2; m < dt.Rows.Count; m++)
            {
                da = new mydataaccess2();
                DataTable dt_photo = new DataTable();

                dt_photo = da.select_photos_pdf_tx_car(siteId, i_counter, m + 1, sheet, nodeid);


                if (dt_photo.Rows.Count > 0)//------------------------------------
                {
                    if (m == 0)
                    {
                        PdfPTable new_t1 = new PdfPTable(1);

                        new_t1.WidthPercentage = 100f;

                        Phrase head_ph_ = new Phrase("Photo Details :", new Font(Font.FontFamily.HELVETICA, 10,
                         Font.BOLD));

                        PdfPCell new_cell_ = new PdfPCell(head_ph_);
                        new_cell_.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell_.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell_.HorizontalAlignment = Element.ALIGN_LEFT;
                        new_cell_.FixedHeight = 20f;
                        new_t1.AddCell(new_cell_);
                        Phrase break2 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 2
                        ));

                        documentPdf.Add(new_t1);
                        documentPdf.Add(break2);
                    }
                    if (dt_photo.Rows.Count > 0)
                    {
                        PdfPTable new_t = new PdfPTable(1);
                        new_t.WidthPercentage = 100f;

                        Phrase head_ph = new Phrase(dt.Rows[m][0].ToString(), new Font(Font.FontFamily.HELVETICA, 7,
                         Font.BOLD));

                        PdfPCell new_cell = new PdfPCell(head_ph);
                        new_cell.BackgroundColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY;
                        new_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        new_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                        new_cell.FixedHeight = 12f;
                        new_t.AddCell(new_cell);
                        documentPdf.Add(new_t);

                        PdfPTable photo = new PdfPTable(4);
                        PdfPCell photo_cell = new PdfPCell();
                        photo.WidthPercentage = 100;
                        int flag = 0;
                        for (int p = 0; p < dt_photo.Rows.Count; p++)
                        {
                            if (flag == 4)
                            {
                                flag = 0;
                            }
                            try
                            {
                                if (dt_photo.Rows.Count % 4 == 0)
                                {
                                    PdfPTable pp = new PdfPTable(1);
                                    Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                    PdfPCell ppc = new PdfPCell(name);
                                    ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                    pp.AddCell(ppc);
                                    //  imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                    string imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                    iTextSharp.text.Image image_photo;
                                    image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                    image_photo.SetAbsolutePosition(50, 50);
                                    image_photo.ScaleAbsolute(70f, 70f);
                                    pp.AddCell(image_photo);
                                    photo_cell = new PdfPCell(pp);
                                    photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                    photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                    //  photo_cell.FixedHeight = 220;
                                    photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                    photo.AddCell(photo_cell);
                                }
                                else
                                {
                                    if (p == dt_photo.Rows.Count - 1)
                                    {
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //  imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        string imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        // photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));

                                        photo.AddCell(photo_cell);

                                        photo_cell = new PdfPCell();
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;

                                        for (int b = 0; b < 4 - flag; b++)
                                        {
                                            photo_cell = new PdfPCell();
                                            photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                            photo.AddCell(photo_cell);
                                        }


                                    }
                                    else
                                    {
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        PdfPTable pp = new PdfPTable(1);
                                        Phrase name = new Phrase(dt_photo.Rows[p][1].ToString(), new Font(Font.FontFamily.HELVETICA, 5));
                                        PdfPCell ppc = new PdfPCell(name);
                                        ppc.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        ppc.HorizontalAlignment = Element.ALIGN_CENTER;
                                        pp.AddCell(ppc);
                                        // imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "F:\\Vodafone\\code_without_report\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        //imag_file1 = "E:\\Vodafone_New\\PTW-Testing\\pics\\" + dt_photo.Rows[p][0].ToString();
                                        string imag_file1 = "D:\\Inetpub\\wwwroot\\today-29-8-2011\\Vodafone_Production_CIAT_09042013\\vodafone_pics\\UploadPhoto\\" + dt_photo.Rows[p][0].ToString();
                                        iTextSharp.text.Image image_photo;
                                        image_photo = iTextSharp.text.Image.GetInstance(imag_file1);
                                        image_photo.SetAbsolutePosition(50, 50);
                                        image_photo.ScaleAbsolute(70f, 70f);
                                        pp.AddCell(image_photo);
                                        photo_cell = new PdfPCell(pp);
                                        photo_cell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                        photo_cell.HorizontalAlignment = Element.ALIGN_CENTER;
                                        //  photo_cell.FixedHeight = 220;
                                        photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                        photo.AddCell(photo_cell);
                                        flag++;
                                    }
                                }
                            }
                            catch
                            {
                                photo_cell = new PdfPCell();
                                photo_cell.BackgroundColor = new BaseColor(System.Drawing.ColorTranslator.FromHtml("#E8E8E8"));
                                photo.AddCell(photo_cell);
                            }
                        }
                        documentPdf.Add(photo);
                        Phrase break23 = new Phrase("\n", new Font(Font.FontFamily.HELVETICA, 1));
                        documentPdf.Add(break23);
                    }

                }//---------------------------


            }//----
        }
        //-----------------Node complete
    }
    #endregion
}