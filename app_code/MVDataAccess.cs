﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using sqlconnection;
using mybusiness;

/// <summary>
/// Summary description for MVDataAccess
/// </summary>
namespace MVdataaccesslayer
{
    public class MVDataAccess
    {
        connection con = new connection();
        public MVDataAccess()
        {
            this.connection = con.open();
            this.command = new SqlCommand("", this.connection);
            this.command.CommandType = CommandType.StoredProcedure;
            this.command.CommandTimeout = 6000;
        }
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataAdapter Adap;
        private SqlDataReader reader;
        public DataTable select_user_pass(string uniquecode)
        {
            try
            {
                command.CommandText = "select_user_pass";
                command.Parameters.AddWithValue("@uniquecode", uniquecode);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable insertphotodetails(String siteid, String stepid, String inspection_counter, String check_type, String ques_id, String filename)
        {
            try
            {


                command.CommandText = "insertphotodetails";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@inspection_counter", inspection_counter);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@filename", filename);
                 
                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            } 
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();

            }
        }

        public DataTable insertphotodetails_tx(String siteid, String stepid, String inspection_counter, String check_type, String ques_id, String filename,string nssid)
        {
            try
            {


                command.CommandText = "insertphotodetails_tx";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@inspection_counter", inspection_counter);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@filename", filename);
                command.Parameters.AddWithValue("@nssid", nssid);

                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();

            }
        }


        public DataTable insertphotodetails_offline(String siteid, String stepid, String inspection_counter, String check_type, String ques_id, String filename)
        {
            try
            {


                command.CommandText = "insertphotodetails_offline";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@inspection_counter", inspection_counter);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@filename", filename);

                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();

            }
        }

        public DataTable insertphotodetails_tx_offline(String siteid, String stepid, String inspection_counter, String check_type, String ques_id, String filename, string nssid)
        {
            try
            {


                command.CommandText = "insertphotodetails_tx_offline";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@inspection_counter", inspection_counter);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@filename", filename);
                command.Parameters.AddWithValue("@nssid", nssid);

                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();

            }
        }

        public DataTable insertphotodetails_punchpoint(String siteid, String stepid, String inspection_counter, String check_type, String ques_id, String filename)
        {
            try
            {


                command.CommandText = "insertphotodetails_punchpoint";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@inspection_counter", inspection_counter);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@filename", filename);

                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();

            }
        }

        public DataTable insertphotodetails_tx_punchpoint(String siteid, String stepid, String inspection_counter, String check_type, String ques_id, String filename, string nssid)
        {
            try
            {


                command.CommandText = "insertphotodetails_tx_punchpoint";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@inspection_counter", inspection_counter);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@filename", filename);
                command.Parameters.AddWithValue("@nssid", nssid);

                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();

            }
        }
        public DataTable insertphotodetails_ptw(string ptwid, string stepid, string category, string ques_id, string check_type, string p_name, string ptw, string filename)
        {
            try
            {
                command.CommandText = "insertphotodetails_ptw";
                command.Parameters.AddWithValue("@ptwid", ptwid);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@category", category);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@p_name", p_name);
                command.Parameters.AddWithValue("@ptw", ptw); 
                command.Parameters.AddWithValue("@filename", filename);
                 
                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();
            }
        }

        public DataTable select_photos_issuer_ptw(string ptwid, string cat, string ques_id, string check_type, string p_name)
        {
            try
            {
                command.CommandText = "select_photos_issuer_ptw";
                command.Parameters.AddWithValue("@ptwid", ptwid);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@ques_id", ques_id);
                command.Parameters.AddWithValue("@check_type", check_type);
                command.Parameters.AddWithValue("@p_name", p_name);

                DataTable dt = new DataTable();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
                connection.Close();
                con.close();
                // reader.Close();
                // reader.Dispose();
            }
        }

        public int updateuniquecode_android_native(string username, string password, string uniqcode, string imei)
        {
            try
            {
                command.CommandText = "updateuniquecode_android_native";
                command.Parameters.AddWithValue("@username", username);
                command.Parameters.AddWithValue("@password", password);
                command.Parameters.AddWithValue("@uniquecode", uniqcode);
                command.Parameters.AddWithValue("@imei", imei);

                reader = command.ExecuteReader();
                int r = 0;
                while (reader.Read())
                {
                    r = Convert.ToInt32(reader[0].ToString());
                }
                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }
        }
        public string select_imei_of_user(string username)
        {
            try
            {
                command.CommandText = "select_imei_of_user";
                command.Parameters.AddWithValue("@user", username);

                reader = command.ExecuteReader();
                string imei = "";
                while (reader.Read())
                {
                    imei = reader[0].ToString();
                }
                return imei;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }
        }
        public void insertlatlongfrommobile_native(double lat1, double long1, string np, string accu, int gps_status, string imei)
        {
            try
            {
                command.CommandText = "insertlatlongfrommobile_native";
                command.Parameters.AddWithValue("@lat", lat1);
                command.Parameters.AddWithValue("@lon", long1);
                command.Parameters.AddWithValue("@np", np);
                command.Parameters.AddWithValue("@accuracy", accu);
                command.Parameters.AddWithValue("@gps", gps_status);
                command.Parameters.AddWithValue("@status", imei);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
                con.close();
            }
        }
    }
}
