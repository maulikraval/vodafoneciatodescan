﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using business;
using sqlconnection;
using System.Data.OleDb;
using businessaccesslayer;
using mybusiness;
using System.Text;
using System.Collections.Generic;

/// <summary>
/// Summary description for dataaccess
/// </summary>
namespace dataaccesslayer2
{
    public class mydataaccess2
    {
	  connection con = new connection();
        public mydataaccess2()
        {

           this.connection = con.open();
            this.command = new SqlCommand("", this.connection);
            this.command.CommandType = CommandType.StoredProcedure;
            this.command.CommandTimeout = 6000;

        }
        private SqlConnection connection;
        private SqlCommand command;
        private SqlDataAdapter Adap;
        private SqlDataReader reader;
        myvodav2 vb;
        mydataaccess2 da;

        public void insertintorating_master(business_2 ba)
        {
            try
            {
                command.CommandText = "insertintorating_master";
                command.Parameters.AddWithValue("@userid", ba.Userid);
                command.Parameters.AddWithValue("@tower", ba.Tower);
                command.Parameters.AddWithValue("@rate", ba.Rate);
                command.Parameters.AddWithValue("@siteid", ba.Siteid);
                command.Parameters.AddWithValue("@percentage", ba.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", ba.Date);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void insertintorating_master_v2(business_2 ba)
        {
            try
            {
                command.CommandText = "insertintorating_master_v2";
                command.Parameters.AddWithValue("@userid", ba.Userid);
                command.Parameters.AddWithValue("@tower", ba.Tower);
                command.Parameters.AddWithValue("@rate", ba.Rate);
                command.Parameters.AddWithValue("@siteid", ba.Siteid);
                command.Parameters.AddWithValue("@percentage", ba.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", ba.Date);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void updatesitestatus(business_2 ba)
        {
            try
            {
                command.CommandText = "updatesitestatus";

                command.Parameters.AddWithValue("@siteid", ba.Siteid);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void insertintorating_master_v2ms(business_2 ba, double height)
        {
            try
            {
                command.CommandText = "insertintorating_master_v2ms";
                command.Parameters.AddWithValue("@userid", ba.Userid);
                command.Parameters.AddWithValue("@tower", ba.Tower);
                command.Parameters.AddWithValue("@rate", ba.Rate);
                command.Parameters.AddWithValue("@siteid", ba.Siteid);
                command.Parameters.AddWithValue("@percentage", ba.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", ba.Date);
                command.Parameters.AddWithValue("@height", height);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void updatesitestatus_v2ms(business_2 ba)
        {
            try
            {
                command.CommandText = "updatesitestatus_v2ms";

                command.Parameters.AddWithValue("@siteid", ba.Siteid);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void insertintorating_master(business_2 ba, double height)
        {
            try
            {
                command.CommandText = "insertintorating_master";
                command.Parameters.AddWithValue("@userid", ba.Userid);
                command.Parameters.AddWithValue("@tower", ba.Tower);
                command.Parameters.AddWithValue("@rate", ba.Rate);
                command.Parameters.AddWithValue("@siteid", ba.Siteid);
                command.Parameters.AddWithValue("@percentage", ba.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", ba.Date);
                command.Parameters.AddWithValue("@height", height);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void insertintorating_master_v2(business_2 ba, double height)
        {
            try
            {
                command.CommandText = "insertintorating_master_v2";
                command.Parameters.AddWithValue("@userid", ba.Userid);
                command.Parameters.AddWithValue("@tower", ba.Tower);
                command.Parameters.AddWithValue("@rate", ba.Rate);
                command.Parameters.AddWithValue("@siteid", ba.Siteid);
                command.Parameters.AddWithValue("@percentage", ba.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", ba.Date);
                command.Parameters.AddWithValue("@height", height);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public DataTable ptw_select_person_work_closed(string ptw, int eid)
        {
            try
            {
                command.CommandText = "ptw_select_person_work_closed";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ext", eid);


                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_risk_back_data(int eid)
        {
            try
            {
                command.CommandText = "ptw_select_risk_back_data";
                command.Parameters.AddWithValue("@eid", eid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_selectquestion(vodabal ba)
        {
            try
            {
                command.CommandText = "selectquestion_ptw";

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();

                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable getpurpose_ptw()
        {
            try
            {
                command.CommandText = "getpurpose_ptw";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable getcategorytype_ptw(int cat)
        {
            try
            {
                command.CommandText = "getcategorytype_ptw";
                command.Parameters.AddWithValue("@category", cat);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable selecteriskheight_ptw()
        {
            try
            {
                command.CommandText = "selecteriskheight_ptw";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable selecteriskofc_ptw()
        {
            try
            {
                command.CommandText = "selecteriskofc_ptw";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable selecteriskrf_ptw()
        {
            try
            {
                command.CommandText = "selecteriskrf_ptw";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable selecteriskelectrical_ptw()
        {
            try
            {
                command.CommandText = "selecteriskelectrical_ptw";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable selecteissuer_ptw(int cat, int type)
        {
            try
            {
                command.CommandText = "selecteissuer_ptw";
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@type", type);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable selecteissuer_ptw_webservice(int cat, int type)
        {
            try
            {
                command.CommandText = "selecteissuer_ptw_webservice";
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@type", type);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }



        public DataTable ptw_select_person_log_issuer_webservice(int person)
        {
            try
            {
                command.CommandText = "ptw_select_person_log_issuer_webservice";
                command.Parameters.AddWithValue("@person", person);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }



        public DataTable ptw_insert_into_basic_detail(string siteid, string location, string ptw, DateTime issuedate, string purpose, string from, string to, string issuer, int cat, string remark, string vehicle_reg)
        {
            try
            {
                command.CommandText = "ptw_insert_into_basic_detail";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@location", location);
                command.Parameters.AddWithValue("@ptwid", ptw);
                command.Parameters.AddWithValue("@issued", issuedate);
                command.Parameters.AddWithValue("@purpose", purpose);
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                command.Parameters.AddWithValue("@issuer", issuer);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@remark", remark);
                command.Parameters.AddWithValue("@vehicle_reg", vehicle_reg);

                reader = command.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                return dt;

                // command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public int import_v2_new(string constring)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {


                da = new mydataaccess2();
                da.delete_error_sites();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 18)
                    {
                        count = count + 1;
                        vb = new myvodav2();

                        try
                        {

                            vb.Circle = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Circle contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Siteid = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_13 = Convert.ToDouble(rd1[2].ToString()); //1.4
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Achieved_14 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Sitename = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_13 = rd1[3].ToString(); //1.4c
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 14 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Address = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_31_v2 = Convert.ToDouble(rd1[4]); //2.2
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 22 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Lat = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Cri_31_v2 = (rd1[5].ToString()); //2.2
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 22 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Longitude = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_33_v2 = Convert.ToDouble(rd1[6].ToString()); //3.5
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 35 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Towertype = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_33_v2 = rd1[7].ToString();    //3.5
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 35 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Technician = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_37_v2 = Convert.ToDouble(rd1[8].ToString());   //3.8
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 38 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_37_v2 = rd1[9].ToString(); //3.8
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 38 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_310 = Convert.ToDouble(rd1[10].ToString());   //3.10
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 3.10 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_310 = rd1[11].ToString(); //3.10
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 3.10 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_102 = Convert.ToDouble(rd1[12].ToString());   //3.11
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 3.11 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_102 = rd1[13].ToString(); //3.11
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 3.11 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_39 = Convert.ToDouble(rd1[14].ToString());   //12.7
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 12.7 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_39 = rd1[15].ToString(); //12.7
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 12.7 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Tower = rd1[16].ToString(); //tower
                        }
                        catch (Exception ex)
                        {

                            string Princi = "tower contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Rate = Convert.ToDouble(rd1[17].ToString()); //tower
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Height contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        da = new mydataaccess2();
                        da.v2_changes_new_added_que(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.18 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[1].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return 2;

            }
            catch (Exception ex)
            {
                //throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public void v2_changes_new_added_que(myvodav2 vb)
        {
            try
            {
                command.CommandText = "v2_changes_new_added_que";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@siteid", vb.Siteid);
                command.Parameters.AddWithValue("@r14", vb.Achieved_13);
                command.Parameters.AddWithValue("@c14", vb.Cri_13);
                command.Parameters.AddWithValue("@r22", vb.Achieved_31_v2);
                command.Parameters.AddWithValue("@c22", vb.Cri_31_v2);
                command.Parameters.AddWithValue("@r35", vb.Achieved_33_v2);
                command.Parameters.AddWithValue("@c35", vb.Cri_33_v2);
                command.Parameters.AddWithValue("@r38", vb.Achieved_37_v2);
                command.Parameters.AddWithValue("@c38", vb.Cri_37_v2);
                command.Parameters.AddWithValue("@r310", vb.Achieved_310);
                command.Parameters.AddWithValue("@c310", vb.Cri_310);
                command.Parameters.AddWithValue("@r311", vb.Achieved_102);
                command.Parameters.AddWithValue("@c311", vb.Cri_102);
                command.Parameters.AddWithValue("@r127", vb.Achieved_39);
                command.Parameters.AddWithValue("@c127", vb.Cri_39);
                command.Parameters.AddWithValue("@tower", vb.Tower);
                command.Parameters.AddWithValue("@height", vb.Rate);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public DataTable select_categories_by_sheet(string sheet)
        {
            try
            {
                command.CommandText = "select_categories_by_sheet";
                command.Parameters.AddWithValue("@sheet", sheet);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int import_v5_new(string constring)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {


                da = new mydataaccess2();
                da.delete_error_sites();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 20)
                    {
                        count = count + 1;
                        vb = new myvodav2();

                        try
                        {

                            vb.Circle = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Circle contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Siteid = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_13 = Convert.ToDouble(rd1[2].ToString()); //1.4
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Achieved_14 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Sitename = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_13 = rd1[3].ToString(); //1.4c
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 14 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Address = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_31_v2 = Convert.ToDouble(rd1[4]); //2.2
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 22 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Lat = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Cri_31_v2 = (rd1[5].ToString()); //2.2
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 22 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Longitude = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_33_v2 = Convert.ToDouble(rd1[6].ToString()); //3.5
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 35 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Towertype = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_33_v2 = rd1[7].ToString();    //3.5
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 35 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Technician = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_37_v2 = Convert.ToDouble(rd1[8].ToString());   //3.8
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 38 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_37_v2 = rd1[9].ToString(); //3.8
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 38 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_310 = Convert.ToDouble(rd1[10].ToString());   //3.10
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 3.10 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_310 = rd1[11].ToString(); //3.10
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 3.10 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_102 = Convert.ToDouble(rd1[12].ToString());   //3.11
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 3.11 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_102 = rd1[13].ToString(); //3.11
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 3.11 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_39 = Convert.ToDouble(rd1[14].ToString());   //12.7
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 12.7 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_39 = rd1[15].ToString(); //12.7
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 12.7 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_31 = Convert.ToDouble(rd1[16].ToString());   //12.7
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 12.7 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_31 = rd1[17].ToString(); //12.7
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 12.7 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Tower = rd1[18].ToString(); //tower
                        }
                        catch (Exception ex)
                        {

                            string Princi = "tower contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Rate = Convert.ToDouble(rd1[19].ToString()); //tower
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Height contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        da = new mydataaccess2();
                        da.v5_changes_new_added_que(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.20 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[1].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return 2;

            }
            catch (Exception ex)
            {
                //throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public void v5_changes_new_added_que(myvodav2 vb)
        {
            try
            {
                command.CommandText = "v5_changes_new_added_que";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@siteid", vb.Siteid);
                command.Parameters.AddWithValue("@r16", vb.Achieved_13);
                command.Parameters.AddWithValue("@c16", vb.Cri_13);
                command.Parameters.AddWithValue("@r23", vb.Achieved_31_v2);
                command.Parameters.AddWithValue("@c23", vb.Cri_31_v2);
                command.Parameters.AddWithValue("@r316", vb.Achieved_33_v2);
                command.Parameters.AddWithValue("@c316", vb.Cri_33_v2);
                command.Parameters.AddWithValue("@r317", vb.Achieved_37_v2);
                command.Parameters.AddWithValue("@c317", vb.Cri_37_v2);
                command.Parameters.AddWithValue("@r318", vb.Achieved_310);
                command.Parameters.AddWithValue("@c318", vb.Cri_310);
                command.Parameters.AddWithValue("@r319", vb.Achieved_102);
                command.Parameters.AddWithValue("@c319", vb.Cri_102);
                command.Parameters.AddWithValue("@r45", vb.Achieved_39);
                command.Parameters.AddWithValue("@c45", vb.Cri_39);
                command.Parameters.AddWithValue("@r515", vb.Achieved_31);
                command.Parameters.AddWithValue("@c515", vb.Cri_31);
                command.Parameters.AddWithValue("@tower", vb.Tower);
                command.Parameters.AddWithValue("@height", vb.Rate);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public void ptw_insert_into_risk_log_master(int ptw, string risk, string ans, string remark, int cat)
        {
            try
            {
                command.CommandText = "ptw_insert_into_risk_log_master";
                command.Parameters.AddWithValue("@ptw_id", ptw);
                command.Parameters.AddWithValue("@risk", risk);
                command.Parameters.AddWithValue("@ans", ans);
                command.Parameters.AddWithValue("@remarks", remark);
                command.Parameters.AddWithValue("@cat", cat);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public int password_change_ptw(myvodav2 vb)
        {
            try
            {
                command.CommandText = "password_change_ptw";
                command.Parameters.AddWithValue("@user", vb.User);
                command.Parameters.AddWithValue("@pass", vb.Password);
                command.Parameters.AddWithValue("@old", vb.Val);
                command.Parameters.AddWithValue("@passcode", vb.Dump);
                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public void ptw_insert_into_question_log_master(string question, string answer, string remark, double stepid, int cat, int ptw, DateTime ins_date, DateTime rep_date, int flag)
        {
            try
            {
                command.CommandText = "ptw_insert_into_question_log_master";
                command.Parameters.AddWithValue("@question", question);
                command.Parameters.AddWithValue("@answer", answer);
                command.Parameters.AddWithValue("@remark", remark);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ins_date", ins_date);
                command.Parameters.AddWithValue("@rep_date", rep_date);
                command.Parameters.AddWithValue("@flag_", flag);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public string ptw_select_ptw_from_id(int id)
        {
            try
            {
                command.CommandText = "ptw_select_ptw_from_id";

                command.Parameters.AddWithValue("@id", id);
                //  command.ExecuteNonQuery();
                reader = command.ExecuteReader();
                string a = "";
                while (reader.Read())
                {
                    a = (reader[0].ToString());
                }
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
                return "1";
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public string ptw_select_max_ptwid()
        {
            try
            {
                command.CommandText = "ptw_select_max_ptwid";
                reader = command.ExecuteReader();
                string pt = "";
                while (reader.Read())
                {
                    pt = reader[0].ToString();
                }
                return pt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_subcategory(int cat)
        {
            try
            {
                command.CommandText = "ptw_select_subcategory";
                command.Parameters.AddWithValue("@cat", cat);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_risk_company_master(string circle)
        {
            try
            {
                command.CommandText = "select_risk_company_master";
                command.Parameters.AddWithValue("@circle", circle);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_issuer_view_basic_detail(string user)
        {
            try
            {
                command.CommandText = "ptw_issuer_view_basic_detail";
                command.Parameters.AddWithValue("@username", user);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_pending_basic_detail(string ptw)
        {
            try
            {
                command.CommandText = "ptw_pending_basic_detail";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_pending_basic_detail_issuer(string ptw)
        {
            try
            {
                command.CommandText = "ptw_pending_basic_detail_issuer";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_issuer_select_question_log(string ptw)
        {
            try
            {
                command.CommandText = "ptw_issuer_select_question_log";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_receiver_que_log(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_receiver_que_log";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_issuer_risk_assesment(int eid, int cat)
        {
            try
            {
                command.CommandText = "ptw_issuer_risk_assesment";
                command.Parameters.AddWithValue("@eid", eid);
                command.Parameters.AddWithValue("@cat", cat);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void ptw_update_remark_basic_detail(string ptw, string remark)
        {
            try
            {
                command.CommandText = "ptw_update_remark_basic_detail";
                command.Parameters.AddWithValue("@ptw", ptw);

                command.Parameters.AddWithValue("@remark", remark);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable ptw_select_loc_from_site(string siteid)
        {
            try
            {
                command.CommandText = "ptw_select_loc_from_site";
                command.Parameters.AddWithValue("@siteid", siteid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_sites_pdf(string ciecle, string provider, int sheet, DateTime from, DateTime to)
        {
            try
            {
                command.CommandText = "select_sites_pdf";
                command.Parameters.AddWithValue("@circle", ciecle);
                command.Parameters.AddWithValue("@provider", provider);
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_site_que_log_pdf(string sheet, string siteid, int i_c, int step)
        {
            try
            {
                command.CommandText = "select_site_que_log_pdf";
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_counter", i_c);
                command.Parameters.AddWithValue("@step", step);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_remark_row(string sheet, int step, string siteid)
        {
            try
            {
                command.CommandText = "select_remark_row";
                command.Parameters.AddWithValue("@step", step);
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@siteid", siteid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable car_select_rate_percentage(string sheet, string siteid)
        {
            try
            {
                command.CommandText = "car_select_rate_percentage";
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@siteid", siteid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_car_data_pdf(string siteid, int i_c, string sheet)
        {
            try
            {
                command.CommandText = "select_car_data_pdf";

                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_counter", i_c);
                command.Parameters.AddWithValue("@sheet", sheet);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_photos_pdf(string siteid, int i_c, int step, string sheet)
        {
            try
            {
                command.CommandText = "select_photos_pdf";

                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_c", i_c);
                command.Parameters.AddWithValue("@step", step);
                command.Parameters.AddWithValue("@sheet", sheet);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataSet select_photos_pdf_ptw(string ptwid, string category)
        {
            try
            {
                command.CommandText = "select_photos_pdf_ptw";

                command.Parameters.AddWithValue("@ptwid", ptwid);
                command.Parameters.AddWithValue("@category", category);
                //DataTable dt = new DataTable();
                //reader = command.ExecuteReader();
                //dt.Load(reader);
                DataSet dt = new DataSet();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                //reader.Close();
                //reader.Dispose();
            }
        }

        public DataTable select_sites_data_pdf(string siteid, string sheet)
        {
            try
            {
                command.CommandText = "select_sites_data_pdf";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@sheet", sheet);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_site_data_all_pdf(string siteid, int i, string sheet)
        {
            try
            {
                command.CommandText = "select_site_data_all_pdf";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_c", i);
                command.Parameters.AddWithValue("@sheet", sheet);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_data_for_mail(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_data_for_mail";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void ptw_update_status_basic_detail(string ptw, int status, string user, int eid, string from)
        {
            try
            {
                command.CommandText = "ptw_update_status_basic_detail";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@status", status);
                command.Parameters.AddWithValue("@user", user);
                command.Parameters.AddWithValue("@eid", eid);
                command.Parameters.AddWithValue("@from", from);
                command.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public void ptw_update_status_person_work(string remark, string status, int person_id, string r_remark)
        {
            try
            {
                command.CommandText = "ptw_update_status_person_work";
                command.Parameters.AddWithValue("@remark", remark);
                command.Parameters.AddWithValue("@status", status);
                command.Parameters.AddWithValue("@person_id", person_id);
                command.Parameters.AddWithValue("@r_remark", r_remark);



                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public void ptw_update_remark_risk(string ptw, string remark)
        {
            try
            {
                command.CommandText = "ptw_update_remark_risk";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@remark", remark);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public void ptw_update_remark_question_log(string ptw, string remark)
        {
            try
            {
                command.CommandText = "ptw_update_remark_question_log";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@remark", remark);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable ptw_issuer_data_for_mail(string ptw)
        {
            try
            {
                command.CommandText = "ptw_issuer_data_for_mail";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_awaiting_ptw_rowbound(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_awaiting_ptw_rowbound";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_person_work_view(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_person_work_view";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int ptw_select_cat(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_cat";
                command.Parameters.AddWithValue("@ptw", ptw);

                int cat = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cat = Convert.ToInt32(reader[0].ToString());
                }
                return cat;
            }
            catch (Exception ex)
            {
                // throw ex;
                return 0;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int ptw_select_count_status(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_count_status";
                command.Parameters.AddWithValue("@ptw", ptw);

                int cat = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cat = Convert.ToInt32(reader[0].ToString());
                }
                return cat;
            }
            catch (Exception ex)
            {
                // throw ex;
                return 0;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public string ptw_check_photographs(int ptw, string name, int cat)
        {
            try
            {
                command.CommandText = "ptw_check_photographs";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@person", name);
                command.Parameters.AddWithValue("@cat", cat);

                string error = "";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    error = reader[0].ToString();
                }
                return error;

            }
            catch (Exception ex)
            {
                return "error";
            }
            finally
            {
                connection.Close(); con.close();


            }
        }
        public int ptw_insert_person_work(int ptw, string name, int ext)
        {
            try
            {
                command.CommandText = "ptw_insert_person_work";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@person", name);
                command.Parameters.AddWithValue("@ext", ext);

                int cat = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cat = Convert.ToInt32(reader[0].ToString());
                }
                return cat;

            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public int ptw_select_ext_id(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_ext_id";
                command.Parameters.AddWithValue("@ptw", ptw);

                int cat = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    cat = Convert.ToInt32(reader[0].ToString());
                }
                return cat;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable ptw_select_person_work(string ptw, int eid)
        {
            try
            {
                command.CommandText = "ptw_select_person_work";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ext", eid);


                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_person(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_person";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public void ptw_delete_person_log_webservice(int pid, int flag)
        {
            try
            {
                command.CommandText = "ptw_delete_person_log_webservice";

                command.Parameters.AddWithValue("@pid", pid);
                command.Parameters.AddWithValue("@flag", flag);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                // throw ex;
                //  return null;
            }
            finally
            {
                connection.Close(); con.close();

                // reader.Close();
                // reader.Dispose();
            }
        }


        public DataTable ptw_select_person_work_normalize(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_person_work_normalize";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_person_log(string person, int ptw, int ext)
        {
            try
            {
                command.CommandText = "ptw_select_person_log";
                command.Parameters.AddWithValue("@person", person);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ext", ext);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_person_log_issuer(int person)
        {
            try
            {
                command.CommandText = "ptw_select_person_log_issuer";
                command.Parameters.AddWithValue("@person", person);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void ptw_delete_person_log(string person, int ptw, int flag, int ext)
        {
            try
            {
                command.CommandText = "ptw_delete_person_log";
                command.Parameters.AddWithValue("@person", person);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@flag", flag);
                command.Parameters.AddWithValue("@ext", ext);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                // throw ex;
                //  return null;
            }
            finally
            {
                connection.Close(); con.close();

                // reader.Close();
                // reader.Dispose();
            }
        }

        public void ptw_update_status_basic_issuer(string ptw)
        {
            try
            {
                command.CommandText = "ptw_update_status_basic_issuer";
                command.Parameters.AddWithValue("@ptw", ptw);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                // throw ex;
                //  return null;
            }
            finally
            {
                connection.Close(); con.close();

                // reader.Close();
                // reader.Dispose();
            }
        }

        public void ptw_insert_nirmalize(int ptw, string person, int ext)
        {
            try
            {
                command.CommandText = "ptw_insert_nirmalize";
                command.Parameters.AddWithValue("@ext", ext);
                command.Parameters.AddWithValue("@person", person);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                // throw ex;
                //  return null;
            }
            finally
            {
                connection.Close(); con.close();

                // reader.Close();
                // reader.Dispose();
            }
        }

        public void ptw_update_basic_ext_status(int ptw, int ext, int status)
        {
            try
            {
                command.CommandText = "ptw_update_basic_ext_status";
                command.Parameters.AddWithValue("@ext", ext);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@status", status);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                // throw ex;
                //  return null;
            }
            finally
            {
                connection.Close(); con.close();

                // reader.Close();
                // reader.Dispose();
            }
        }

        public DataTable ptw_select_awaiting_ptw(string siteid)
        {
            try
            {
                command.CommandText = "ptw_select_awaiting_ptw";
                command.Parameters.AddWithValue("@username", siteid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_ext_receiver(int ext)
        {
            try
            {
                command.CommandText = "ptw_select_ext_receiver";
                command.Parameters.AddWithValue("@ext", ext);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_risk_mitigation_new(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_risk_mitigation_new";
                command.Parameters.AddWithValue("@eid", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int ptw_insert_extension_master(int ptw, string ext, string from, string to, int counter)
        {
            try
            {
                command.CommandText = "ptw_insert_extension_master";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ext", ext);
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                command.Parameters.AddWithValue("@cou", counter);
                reader = command.ExecuteReader();
                int pt = 0;
                while (reader.Read())
                {
                    pt = Convert.ToInt32(reader[0].ToString());
                }
                return pt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public void ptw_insert_into_mitigation(string risk, string processed, string action1, string ppe, string due_date, string com_date, string ptw)
        {
            try
            {
                command.CommandText = "ptw_insert_into_mitigation";
                command.Parameters.AddWithValue("@riskid", risk);
                command.Parameters.AddWithValue("@processed", processed);
                command.Parameters.AddWithValue("@action1", action1);
                command.Parameters.AddWithValue("@ppe", ppe);
                command.Parameters.AddWithValue("@due_date", due_date);
                command.Parameters.AddWithValue("@complete_date", com_date);
                command.Parameters.AddWithValue("@ptwid", ptw);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public string ptw_select_max_ext(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_max_ext";
                command.Parameters.AddWithValue("@ptw", ptw);
                reader = command.ExecuteReader();
                string pt = "";
                while (reader.Read())
                {
                    pt = reader[0].ToString();
                }
                return pt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_user_pass(string uniquecode)
        {
            try
            {
                command.CommandText = "ptw_select_user_pass";
                command.Parameters.AddWithValue("@uniquecode", uniquecode);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_issuer_data_for_mail_close(string ptw)
        {
            try
            {
                command.CommandText = "ptw_issuer_data_for_mail_close";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable getcirclename(string username)
        {
            try
            {
                command.CommandText = "getcirclename";
                command.Parameters.AddWithValue("@username", username);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable getzonesbycirclename(vodabal vb)
        {
            try
            {
                command.CommandText = "getzonesbycircle";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void update_user_master_status(string user)
        {
            try
            {
                command.CommandText = "update_user_master_status";
                command.Parameters.AddWithValue("@user", user);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //return null;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public DataTable ptw_viewsitesbydis_latest(vodabal vb)
        {
            try
            {

                command.CommandText = "ptw_viewsitesbydis_latest";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@zone", vb.Zone);
                command.Parameters.AddWithValue("@imei", vb.Imei);



                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_sitedata(string siteid, string username)
        {
            try
            {
                command.CommandText = "ptw_select_sitedata";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@username", username);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_approved_ptw_issuer(string user)
        {
            try
            {
                command.CommandText = "ptw_select_approved_ptw_issuer";
                command.Parameters.AddWithValue("@username", user);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_issuer(int eid)
        {
            try
            {
                command.CommandText = "select_issuer";
                command.Parameters.AddWithValue("@eid", eid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_report_superadmin()
        {
            try
            {
                command.CommandText = "ptw_report_superadmin";

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void updatesitestatus(vodabal vb1)
        {
            try
            {
                command.CommandText = "insertintorequestmaster";
                command.Parameters.AddWithValue("@siteid", vb1.Siteid);
                command.Parameters.AddWithValue("@technician", vb1.User);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public int ptw_insert_into_address(string add1, string user)
        {
            try
            {
                command.CommandText = "ptw_insert_into_address";
                command.Parameters.AddWithValue("@add1", add1);
                command.Parameters.AddWithValue("@user", user);
                //  command.ExecuteNonQuery();
                reader = command.ExecuteReader();
                int a = 0;
                while (reader.Read())
                {
                    a = Convert.ToInt32(reader[0].ToString());
                }
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public void ptw_update_address_site(string user, string siteid, int ptw, string imei)
        {
            try
            {
                command.CommandText = "ptw_update_address_site";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@user", user);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@imei", imei);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public DataTable getlatlong(vodabal vb)
        {
            try
            {
                command.CommandText = "getlatestlatlong";
                command.Parameters.AddWithValue("@imei", vb.Imei);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable Viewsitegridbydis(vodabal vb)
        {
            try
            {

                command.CommandText = "viewsitesbydis";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@zone", vb.Zone);
                command.Parameters.AddWithValue("@radlatmob", vb.Lat);
                command.Parameters.AddWithValue("@radlongmob", vb.Longitude);
                command.Parameters.AddWithValue("@accuracy", vb.Accuracy);
                command.Parameters.AddWithValue("@accuracy_network", vb.Accuracynp);
                command.Parameters.AddWithValue("@radlatmob_network", vb.Latnp);
                command.Parameters.AddWithValue("@radlongmob_network", vb.Longnp);


                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_view_site_no_address(string siteid)
        {
            try
            {

                command.CommandText = "ptw_view_site_no_address";
                command.Parameters.AddWithValue("@siteid", siteid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void ptw_insert_into_risk_log_master_webservice(int ptw, string risk, string ans, string remark, int cat)
        {
            try
            {
                command.CommandText = "ptw_insert_into_risk_log_master_webservice";
                command.Parameters.AddWithValue("@ptw_id", ptw);
                command.Parameters.AddWithValue("@risk_id", Convert.ToInt64(risk));
                command.Parameters.AddWithValue("@ans", ans);
                command.Parameters.AddWithValue("@remarks", remark);
                command.Parameters.AddWithValue("@cat", cat);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public void ptw_insert_into_question_log_master_webservice(string question, string answer, string remark, double stepid, int cat, int ptw, DateTime ins_date, DateTime rep_date, int flag)
        {
            try
            {
                command.CommandText = "ptw_insert_into_question_log_master_webservice";
                command.Parameters.AddWithValue("@qid", Convert.ToInt32(question));
                command.Parameters.AddWithValue("@answer", answer);
                command.Parameters.AddWithValue("@remark", remark);
                command.Parameters.AddWithValue("@stepid", stepid);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ins_date", ins_date);
                command.Parameters.AddWithValue("@rep_date", rep_date);
                command.Parameters.AddWithValue("@flag_", flag);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable ViewInspectedsitegridbydis(vodabal vb)
        {
            try
            {

                command.CommandText = "viewInspectedsitesbydis";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@zone", vb.Zone);
                command.Parameters.AddWithValue("@radlatmob", vb.Lat);
                command.Parameters.AddWithValue("@radlongmob", vb.Longitude);
                command.Parameters.AddWithValue("@accuracy", vb.Accuracy);
                command.Parameters.AddWithValue("@accuracy_network", vb.Accuracynp);
                command.Parameters.AddWithValue("@radlatmob_network", vb.Latnp);
                command.Parameters.AddWithValue("@radlongmob_network", vb.Longnp);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void ptw_close(int ptw)
        {
            try
            {
                command.CommandText = "ptw_close";
                command.Parameters.AddWithValue("@ptw", ptw);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public int ptw_select_ptwid(string ptw)
        {
            try
            {

                command.CommandText = "ptw_select_ptwid";
                command.Parameters.AddWithValue("@ptw", ptw);
                reader = command.ExecuteReader();
                int pt = 0;
                while (reader.Read())
                {
                    pt = Convert.ToInt32(reader[0].ToString());
                }
                return pt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public string ptw_select_risk_miti(string ptw)
        {
            try
            {

                command.CommandText = "ptw_select_risk_mitigation";
                command.Parameters.AddWithValue("@ptw", ptw);
                reader = command.ExecuteReader();
                string risk = "";
                while (reader.Read())
                {
                    risk = reader[0].ToString();
                }
                return risk;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_purpose_email(int ptw)
        {
            try
            {

                command.CommandText = "ptw_select_purpose_email";
                command.Parameters.AddWithValue("@eid", ptw);
                reader = command.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_report_circleadmin(string circle)
        {
            try
            {

                command.CommandText = "ptw_report_circleadmin";
                command.Parameters.AddWithValue("@circle", circle);
                reader = command.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public string ptw_select_issuer_email(string receiver)
        {
            try
            {

                command.CommandText = "ptw_select_issuer_email";
                command.Parameters.AddWithValue("@receiver", receiver);
                reader = command.ExecuteReader();
                string risk = "";
                while (reader.Read())
                {
                    risk = reader[0].ToString();
                }
                return risk;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public void update_renew_status(int ptw)
        {
            try
            {
                command.CommandText = "update_renew_status";
                command.Parameters.AddWithValue("@ptw_id", ptw);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public void delete_error_sites()
        {

            try
            {
                command.CommandText = "delete_error_sites";
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable select_error_sites()
        {

            try
            {
                command.CommandText = "select_error_sites";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public string select_user_cookie(string cook)
        {
            try
            {
                command.CommandText = "select_user_cookie";
                command.Parameters.AddWithValue("@cookie", cook);
                string s = "";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    s = reader[0].ToString();
                }
                return s;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int importsite(string constring, string user)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {


                da = new mydataaccess2();
                da.delete_error_sites();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 16)
                    {
                        count = count + 1;
                        vb = new myvodav2();


                        try
                        {

                            vb.Siteid = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Sitename = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Site Name contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Sitename = "1";
                            //return result;

                        }
                        try
                        {
				string str = rd1[2].ToString();
                            str = str.Replace(@"\r\n\", "").Replace("\r","").Replace("\n", "");
                            vb.Address = str;
                            //vb.Address = rd1[2].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Address contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[0].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Address = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Lat = Convert.ToDouble(rd1[3]);
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Latitude contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[0].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Lat = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Longitude = Convert.ToDouble(rd1[4]);
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Longitude contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Longitude = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Towertype = rd1[5].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Towertype contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Towertype = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Technician = rd1[6].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Site Inspector contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Technician = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Circle = rd1[7].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Circle contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Zone = rd1[8].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Zone contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Subzone = rd1[9].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Subzone contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Subzone = "1";
                            //return result;

                        }
                        try
                        {
                            if (rd1[10].ToString() == "")
                            {
                                string Princi = "Date of RFAI is blank";
                                vb.Siteidnew = vb.Siteid;
                                vb.Error = Princi + "." + "The Row number is:-->" + count;
                                vb.Siteid = rd1[0].ToString();

                                string result = 1 + "'" + Princi + "'" + count;
                                da = new mydataaccess2();
                                da.error_log(vb);
                                da = new mydataaccess2();
                                da.error_sites(vb);
                                result1 = 1;
                                return result1;
                            }
                            else
                            {
                                DateTime datetime = DateTime.Parse(rd1[10].ToString());
                                vb.Datetime = datetime.ToString("yyyy-MM-dd");
                                //vb.Datetime = rd1[10].ToString();
                            }
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Datetime contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Datetime = "1";
                            result1 = 1;
                            return result1;
                        }

                        try
                        {

                            vb.Tech_contact = rd1[11].ToString();
                            vb.Check_sheet = rd1[12].ToString();
                            vb.Provider = rd1[13].ToString();
                            vb.Ip_id = rd1[14].ToString();
                            vb.Site_type = rd1[15].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Technician Contact No contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Tech_contact = "1";
                            //return result;

                        }

                        vb.User = user;
                        da = new mydataaccess2();
                        int res = da.InsertVodaSite(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.16 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[0].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return result1;

            }
            catch (Exception ex)
            {
                myvodav2 vb = new myvodav2();
                string Princi = ex.Message.ToString();
                vb.Siteidnew = vb.Siteid;
                vb.Error = Princi;
                vb.Siteid = "error";

                // string result = 1 + "'" + Princi + "'" + 0;
                da = new mydataaccess2();
                da.error_log(vb);
                da = new mydataaccess2();
                da.error_sites(vb);
                vb.Tech_contact = "1";
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public int InsertVodaSite(myvodav2 vb)
        {
            try
            {
                command.CommandText = "InsertVodaSite";


                command.Parameters.AddWithValue("@Siteid12", vb.Siteid);
                command.Parameters.AddWithValue("@Sitename12", vb.Sitename);
                command.Parameters.AddWithValue("@Address12", vb.Address);
                command.Parameters.AddWithValue("@Lat12", vb.Lat);
                command.Parameters.AddWithValue("@Longitude12", vb.Longitude);
                command.Parameters.AddWithValue("@Towertype12", vb.Towertype);
                command.Parameters.AddWithValue("@Technician12", vb.Technician);
                command.Parameters.AddWithValue("@Circle12", vb.Circle);
                command.Parameters.AddWithValue("@Zone12", vb.Zone);
                command.Parameters.AddWithValue("@Subzone12", vb.Subzone);
                command.Parameters.AddWithValue("@Datetime12", vb.Datetime);
                command.Parameters.AddWithValue("@techcontact12", vb.Tech_contact);
                command.Parameters.AddWithValue("@check_sheet", vb.Check_sheet);
                command.Parameters.AddWithValue("@user", vb.User);
                command.Parameters.AddWithValue("@provider", vb.Provider);
                command.Parameters.AddWithValue("@ip_id", vb.Ip_id);
                command.Parameters.AddWithValue("@site_type", vb.Site_type);
                int result = command.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        private void error_sites(myvodav2 vb)
        {
            try
            {
                command.CommandText = "insert_error";


                command.Parameters.AddWithValue("@Siteid12", vb.Siteid);
                command.Parameters.AddWithValue("@Sitename12", vb.Error);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        private void error_log(myvodav2 vb)
        {
            try
            {
                command.CommandText = "insert_log";


                command.Parameters.AddWithValue("@Siteid12", vb.Siteid);
                command.Parameters.AddWithValue("@Sitename12", vb.Error);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public DataTable ptw_select_person_for_pdf(string ptw)
        {

            try
            {
                command.CommandText = "ptw_select_person_for_pdf";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_person_receiver_for_pdf(string ptw, string person)
        {

            try
            {
                command.CommandText = "ptw_select_person_receiver_for_pdf";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@person", person);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable view_fto(string circle)
        {
            try
            {
                command.CommandText = "view_fto";
                command.Parameters.AddWithValue("@circle", circle);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                // con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_approved_ptw_issuer_pdf(string circle)
        {
            try
            {

                command.CommandText = "ptw_select_approved_ptw_issuer_pdf";
                command.Parameters.AddWithValue("@filter", circle);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                // con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_Select_site_data_report(string ptw)
        {

            try
            {
                command.CommandText = "ptw_Select_site_data_report";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_que_log_data_report(string ptw, int cat, string person)
        {

            try
            {
                command.CommandText = "ptw_select_que_log_data_report";
                command.Parameters.AddWithValue("@Ptw", ptw);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@person", person);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_at_risk_data_for_report(string ptw, string person)
        {

            try
            {
                command.CommandText = "select_at_risk_data_for_report";
                command.Parameters.AddWithValue("@Ptw", ptw);
                command.Parameters.AddWithValue("@person", person);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_issuer_data_for_mail_receiver(string ptw)
        {
            try
            {
                command.CommandText = "ptw_issuer_data_for_mail_receiver";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_issuer_data_for_mail_issuer(string ptw)
        {
            try
            {
                command.CommandText = "ptw_issuer_data_for_mail_issuer";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_at_risk_data_for_report_approved(string ptw)
        {

            try
            {
                command.CommandText = "select_at_risk_data_for_report_approved";
                command.Parameters.AddWithValue("@Ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_risk_data_for_report(string ptw, int cat, string person)
        {

            try
            {
                command.CommandText = "ptw_select_risk_data_for_report";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@person", person);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_miti_data_for_report(string ptw, int cat, string person)
        {

            try
            {
                command.CommandText = "ptw_select_miti_data_for_report";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@cat", cat);
                command.Parameters.AddWithValue("@person", person);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_varify_data_for_report(string ptw)
        {

            try
            {
                command.CommandText = "ptw_select_varify_data_for_report";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_confirm_data_for_report(string ptwid)
        {
            try
            {
                command.CommandText = "ptw_select_confirm_data_for_report";
                command.Parameters.AddWithValue("@ptw", ptwid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataSet ptw_select_issuer_receiver_report(string ptwid)
        {
            try
            {
                command.CommandText = "ptw_select_issuer_receiver_report";
                command.Parameters.AddWithValue("@ptw", ptwid);

                DataSet dt = new DataSet();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataSet ptw_images_sign_report(string issuer, string receiver)
        {
            try
            {
                command.CommandText = "ptw_images_sign_report";
                command.Parameters.AddWithValue("@issuer", issuer);
                command.Parameters.AddWithValue("@receiver", receiver);

                DataSet dt = new DataSet();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable ptw_select_date_diff(string ptwid, string from)
        {
            try
            {
                command.CommandText = "ptw_select_date_diff";
                command.Parameters.AddWithValue("@ptw", ptwid);
                command.Parameters.AddWithValue("@from", from);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public string ptw_Select_back_address(int counter, string user)
        {
            try
            {
                command.CommandText = "ptw_Select_back_address";
                command.Parameters.AddWithValue("@counter", counter);
                command.Parameters.AddWithValue("@user", user);
                //  command.ExecuteNonQuery();
                reader = command.ExecuteReader();
                string a = "";
                while (reader.Read())
                {
                    a = (reader[0].ToString());
                }
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
                return "1";
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public int import_v5(string constring)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {


                da = new mydataaccess2();
                da.delete_error_sites();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 14)
                    {
                        count = count + 1;
                        vb = new myvodav2();

                        try
                        {

                            vb.Circle = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Circle contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Siteid = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_31 = Convert.ToDouble(rd1[2].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Achieved_31 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Sitename = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_31 = rd1[3].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 31 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Address = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_32 = Convert.ToDouble(rd1[4]);
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 32 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Lat = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Cri_32 = (rd1[5].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 32 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Longitude = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_37 = Convert.ToDouble(rd1[6].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 37 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Towertype = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_37 = rd1[7].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 37 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Technician = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_39 = Convert.ToDouble(rd1[8].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 39 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_39 = rd1[9].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 39 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_310 = Convert.ToDouble(rd1[10].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 3.10 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Subzone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_310 = rd1[11].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 3.10 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Datetime = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_102 = Convert.ToDouble(rd1[12].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 10.2 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Subzone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_102 = rd1[13].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 10.2 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Datetime = "1";
                            //return result;

                        }



                        da = new mydataaccess2();
                        da.update_v5(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.14 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[1].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return 2;

            }
            catch (Exception ex)
            {
                //throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public int import_v2(string constring)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {


                da = new mydataaccess2();
                da.delete_error_sites();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 10)
                    {
                        count = count + 1;
                        vb = new myvodav2();

                        try
                        {

                            vb.Circle = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Circle contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Siteid = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Achieved_13 = Convert.ToDouble(rd1[2].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Achieved_13 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Sitename = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_13 = rd1[3].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 13 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Address = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_31_v2 = Convert.ToDouble(rd1[4]);
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 31 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[1].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Lat = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Cri_31_v2 = (rd1[5].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 31 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Longitude = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_33_v2 = Convert.ToDouble(rd1[6].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "achieved 33 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Towertype = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_33_v2 = rd1[7].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 33 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Technician = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Achieved_37_v2 = Convert.ToDouble(rd1[8].ToString());
                        }
                        catch (Exception ex)
                        {

                            string Princi = "archieved 37 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Cri_37_v2 = rd1[9].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "criticality 37 contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[1].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }


                        da = new mydataaccess2();
                        da.update_v2(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.10 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[1].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return 2;

            }
            catch (Exception ex)
            {
                //throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public void update_v5(myvodav2 vb)
        {
            try
            {
                command.CommandText = "update_v5";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@siteid", vb.Siteid);
                command.Parameters.AddWithValue("@31", vb.Achieved_31);
                command.Parameters.AddWithValue("@31c", vb.Cri_31);
                command.Parameters.AddWithValue("@32", vb.Achieved_32);
                command.Parameters.AddWithValue("@32c", vb.Cri_32);
                command.Parameters.AddWithValue("@37", vb.Achieved_37);
                command.Parameters.AddWithValue("@37c", vb.Cri_37);
                command.Parameters.AddWithValue("@39", vb.Achieved_39);
                command.Parameters.AddWithValue("@39c", vb.Cri_39);
                command.Parameters.AddWithValue("@310", vb.Achieved_310);
                command.Parameters.AddWithValue("@310c", vb.Cri_310);
                command.Parameters.AddWithValue("@102", vb.Achieved_102);
                command.Parameters.AddWithValue("@102c", vb.Cri_102);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public void update_v2(myvodav2 vb)
        {
            try
            {
                command.CommandText = "update_v2";
                command.Parameters.AddWithValue("@circle", vb.Circle);
                command.Parameters.AddWithValue("@siteid", vb.Siteid);
                command.Parameters.AddWithValue("@13", vb.Achieved_13);
                command.Parameters.AddWithValue("@13c", vb.Cri_13);
                command.Parameters.AddWithValue("@31", vb.Achieved_31_v2);
                command.Parameters.AddWithValue("@31c", vb.Cri_31_v2);
                command.Parameters.AddWithValue("@33", vb.Achieved_33_v2);
                command.Parameters.AddWithValue("@33c", vb.Cri_33_v2);
                command.Parameters.AddWithValue("@37", vb.Achieved_37_v2);
                command.Parameters.AddWithValue("@37c", vb.Cri_37_v2);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public void insertintorating_master(myvodav2 ba, double height)
        {
            try
            {
                command.CommandText = "insertintorating_master";
                command.Parameters.AddWithValue("@userid", ba.Userid);
                command.Parameters.AddWithValue("@tower", ba.Tower);
                command.Parameters.AddWithValue("@rate", ba.Rate);
                command.Parameters.AddWithValue("@siteid", ba.Siteid);
                command.Parameters.AddWithValue("@percentage", ba.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", System.DateTime.Now);
                command.Parameters.AddWithValue("@height", height);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public void insertintorating_master_v2(myvodav2 vb, double height)
        {
            try
            {
                command.CommandText = "insertintorating_master_v2";
                command.Parameters.AddWithValue("@userid", vb.Userid);
                command.Parameters.AddWithValue("@tower", vb.Tower);
                command.Parameters.AddWithValue("@rate", vb.Rate);
                command.Parameters.AddWithValue("@siteid", vb.Siteid);
                command.Parameters.AddWithValue("@percentage", vb.Percentage);
                command.Parameters.AddWithValue("@SurveyDate", System.DateTime.Now);
                command.Parameters.AddWithValue("@SurveyDate1", System.DateTime.Now);
                command.Parameters.AddWithValue("@height", height);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                //con.close();
            }
        }

        public void ptw_update_to_date_issuer(string ptw, string to)
        {
            try
            {
                command.CommandText = "ptw_update_to_date_issuer";


                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@to", to);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }

        public DataTable select_circle_zone(string username)
        {
            try
            {
                command.CommandText = "select_circle_zone";
                command.Parameters.AddWithValue("@username", username);


                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_category_purpose(string category)
        {

            try
            {
                command.CommandText = "select_category_purpose";
                command.Parameters.AddWithValue("@category", category);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }

        }

        public DataTable view_mapping(string zone, string category, string purpose)
        {

            try
            {
                command.CommandText = "view_mapping";
                command.Parameters.AddWithValue("@zonename", zone);
                command.Parameters.AddWithValue("@category", category);
                command.Parameters.AddWithValue("@subcategory", purpose);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }


        }

        internal DataTable get_reject_reason()
        {
            try
            {
                command.CommandText = "get_reject_reason";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public string get_email_id_user(string user)
        {
            try
            {

                command.CommandText = "get_email_id_user";
                command.Parameters.AddWithValue("@user", user);
                reader = command.ExecuteReader();
                string email = "";
                while (reader.Read())
                {
                    email = reader[0].ToString();
                }
                return email;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                reader.Close();
                connection.Close(); con.close();
            }
        }



        public DataTable ptw_select_risk_back_data_webservice(int eid)
        {
            try
            {
                command.CommandText = "ptw_select_risk_back_data_webservice";
                command.Parameters.AddWithValue("@eid", eid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_site_check_master()
        {

            try
            {
                command.CommandText = "select_site_check_master";
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

                reader.Close();
                reader.Dispose();
            }
        }


        public int updatechecksheet(string constring, string user)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {

                da = new mydataaccess2();
                da.delete_site_check_master();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 2)
                    {
                        count = count + 1;
                        vb = new myvodav2();


                        try
                        {

                            vb.Siteid = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Check_sheet = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Check_sheet Not Applicable.";
                            vb.Siteid = "Check_sheet Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Check_sheet = "1";
                            //return result;

                        }

                        da = new mydataaccess2();
                        int res = da.update_checksheet(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.2 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[0].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return result1;

            }
            catch (Exception ex)
            {
                //throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public int update_checksheet(myvodav2 vb)
        {
            try
            {
                command.CommandText = "update_checksheet";
                command.Parameters.AddWithValue("@siteid", vb.Siteid);
                command.Parameters.AddWithValue("@check_sheet", vb.Check_sheet);

                int result = command.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }
        public int checksite(string constring, string user)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {

                da = new mydataaccess2();
                da.delete_site_check_master();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 1)
                    {
                        count = count + 1;
                        vb = new myvodav2();


                        try
                        {

                            vb.Siteid = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }


                        da = new mydataaccess2();
                        int res = da.check_site(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.1 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[0].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return result1;

            }
            catch (Exception ex)
            {
                //throw ex;
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }



        public int check_site(myvodav2 vb)
        {
            try
            {
                command.CommandText = "check_site";
                command.Parameters.AddWithValue("@siteid", vb.Siteid);

                int result = command.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public void delete_site_check_master()
        {

            try
            {
                command.CommandText = "delete_site_check_master";
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public DataTable ptw_select_routes(string ptwid)
        {
            try
            {
                command.CommandText = "ptw_select_routes";
                command.Parameters.AddWithValue("@ptw", ptwid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable select_route_data_for_edit(string rid)
        {
            try
            {
                command.CommandText = "select_route_data_for_edit";
                command.Parameters.AddWithValue("@rid", Convert.ToInt32(rid));

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public int ptw_insert_extension_master_night(int ptw, string ext, string from, string to, string start_location, string end_location, string route, string purpose, string purposeremark, string purpose1, string purposeremark1)
        {
            try
            {
                command.CommandText = "ptw_insert_extension_master_night";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ext", ext);
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@to", to);
                command.Parameters.AddWithValue("@start_location", start_location);
                command.Parameters.AddWithValue("@end_location", end_location);
                command.Parameters.AddWithValue("@route", route);
                command.Parameters.AddWithValue("@purpose", purpose);
                command.Parameters.AddWithValue("@remark", purposeremark);
                command.Parameters.AddWithValue("@purpose1", purpose1);
                command.Parameters.AddWithValue("@remark1", purposeremark1);
                reader = command.ExecuteReader();
                int res = 0;
                while (reader.Read())
                {
                    res = Convert.ToInt32(reader[0].ToString());
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();

            }
        }

        public int ptw_edit_route(int ptw, string ext, string start_time, string end_time, string start_location, string end_location, string route, string purpose, string purposeremark, int rid)
        {
            try
            {
                command.CommandText = "ptw_edit_route";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@ext", ext);
                command.Parameters.AddWithValue("@from", start_time);
                command.Parameters.AddWithValue("@to", end_time);
                command.Parameters.AddWithValue("@start_location", start_location);
                command.Parameters.AddWithValue("@end_location", end_location);
                command.Parameters.AddWithValue("@route", route);
                command.Parameters.AddWithValue("@purpose", purpose);
                command.Parameters.AddWithValue("@remark", purposeremark);
                command.Parameters.AddWithValue("@rid", rid);
                //command.ExecuteNonQuery();
                reader = command.ExecuteReader();
                int res = 0;
                while (reader.Read())
                {
                    res = Convert.ToInt32(reader[0].ToString());
                }
                return res;
            }
            catch (Exception ex)
            {
                // return 0;
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public string ptw_delete_route(int rid)
        {
            try
            {
                command.CommandText = "ptw_delete_route";

                command.Parameters.AddWithValue("@rid", rid);
                string i = "";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = reader[0].ToString();
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_select_sub_category_master_night()
        {
            try
            {
                command.CommandText = "ptw_select_sub_category_master_night";

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable select_at_risk_data_for_report_night(string ptw, string person)
        {

            try
            {
                command.CommandText = "select_at_risk_data_for_report_night";
                command.Parameters.AddWithValue("@Ptw", ptw);
                command.Parameters.AddWithValue("@person", person);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int ptw_approveall_night(int ptwid, int ext, string issuer, int status, int flow, string rejection_remark)
        {
            try
            {
                command.CommandText = "ptw_approveall_night";
                command.Parameters.AddWithValue("@ptw", ptwid);
                command.Parameters.AddWithValue("@ext", ext);
                command.Parameters.AddWithValue("@issuer", issuer);
                command.Parameters.AddWithValue("@status", status);
                command.Parameters.AddWithValue("@flow", flow);
                command.Parameters.AddWithValue("@rejection_remark", rejection_remark);

                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_select_routes_for_partial(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_routes_for_partial";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_select_routes_for_extension(int ptw)
        {
            try
            {
                command.CommandText = "ptw_select_routes_for_extension";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_select_awaiting_ptw_night(string siteid)
        {
            try
            {
                command.CommandText = "ptw_select_awaiting_ptw_night";
                command.Parameters.AddWithValue("@username", siteid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public void ptw_close_night(string ptw)
        {
            try
            {
                command.CommandText = "ptw_close_night";
                command.Parameters.AddWithValue("@ptw", ptw);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public int ptw_close_route(string ptw)
        {
            try
            {
                command.CommandText = "ptw_close_route";
                command.Parameters.AddWithValue("@ptw", ptw);


                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_Select_site_data_report_night(string ptw)
        {

            try
            {
                command.CommandText = "ptw_Select_site_data_report_night";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_Select_site_data_report_night_risk(string ptw)
        {

            try
            {
                command.CommandText = "ptw_Select_site_data_report_night_risk";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }


        public DataTable ptw_select_varify_data_for_report_night(string ptw)
        {

            try
            {
                command.CommandText = "ptw_select_varify_data_for_report_night";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_select_confirm_data_for_report_night(string ptwid)
        {
            try
            {
                command.CommandText = "ptw_select_confirm_data_for_report_night";
                command.Parameters.AddWithValue("@ptw", ptwid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }


        public DataTable ptw_select_issuer_details_report_night(string ptwid)
        {
            try
            {
                command.CommandText = "ptw_select_issuer_details_report_night";
                command.Parameters.AddWithValue("@ptw", ptwid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int ptw_extension_night(string extension, string purpose, string endtime, string starttime, string issuer)
        {

            try
            {
                command.CommandText = "ptw_extension_night";
                command.Parameters.AddWithValue("@extension", extension);
                command.Parameters.AddWithValue("@purpose", purpose);
                command.Parameters.AddWithValue("@endtime", (endtime));
                command.Parameters.AddWithValue("@starttime", starttime);
                command.Parameters.AddWithValue("@issuer", (issuer));
                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public void update_unique_code_extension(int ptw, string unique_code)
        {
            try
            {
                command.CommandText = "update_unique_code_extension";
                command.Parameters.AddWithValue("@ptwid", ptw);
                command.Parameters.AddWithValue("@unique_code", unique_code);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }
        public int select_unique_code_extension(int ptw, string unique_code)
        {

            try
            {
                command.CommandText = "select_unique_code_extension";
                command.Parameters.AddWithValue("@ptwid", ptw);
                command.Parameters.AddWithValue("@unique_code", unique_code);


                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable ptw_select_contact_mail(string ptw)
        {
            try
            {
                command.CommandText = "ptw_select_contact_mail";
                command.Parameters.AddWithValue("@ptw", ptw);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public void ptw_update_status_basic_detail1(string ptw, int status, string user, int eid, string from, string remark, string reject_remark)
        {
            try
            {
                command.CommandText = "ptw_update_status_basic_detail1";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@status", status);
                command.Parameters.AddWithValue("@user", user);
                command.Parameters.AddWithValue("@eid", eid);
                command.Parameters.AddWithValue("@from", from);
                command.Parameters.AddWithValue("@remark", remark);
                command.Parameters.AddWithValue("@reject_remark", reject_remark);
                command.ExecuteNonQuery();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }
        public void ptw_update_is_push(string ptw, int flag, string Responce)
        {
            try
            {
                command.CommandText = "ptw_update_is_push";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@flag", flag);
                command.Parameters.AddWithValue("@Responce", Responce);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }
        public void update_mail_flag(int ptw, int flag)
        {
            try
            {
                command.CommandText = "update_mail_flag";
                command.Parameters.AddWithValue("@ptw", ptw);
                command.Parameters.AddWithValue("@flag", flag);


                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }
        public string ptw_issuer_mail_extension(string extension)
        {

            try
            {
                command.CommandText = "ptw_issuer_mail_extension";
                command.Parameters.AddWithValue("@extension", extension);

                string i = "";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = reader[0].ToString();
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        public void ptw_update_route_id(int rid, string ext)
        {
            try
            {
                command.CommandText = "ptw_update_route_id";
                command.Parameters.AddWithValue("@rid", rid);
                command.Parameters.AddWithValue("@ext", ext);


                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
            }
        }

        public int ptw_select_is_mail_used(int ptwid, string uniq)
        {
            try
            {
                command.CommandText = "ptw_select_is_mail_used";
                command.Parameters.AddWithValue("@ptw", ptwid);
                command.Parameters.AddWithValue("@uniq", uniq);



                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public int ptw_select_is_mail_used_for_close(string extension, string uniq)
        {
            try
            {
                command.CommandText = "ptw_select_is_mail_used_for_close";
                command.Parameters.AddWithValue("@extension", extension);
                command.Parameters.AddWithValue("@uniq", uniq);



                int i = 0;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = Convert.ToInt32(reader[0].ToString());
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_pending_basic_detail_issuer_night(string ptw)
        {
            try
            {
                command.CommandText = "ptw_pending_basic_detail_issuer_night";
                command.Parameters.AddWithValue("@ptw", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public string select_ptwid_from_ext(string extension)
        {

            try
            {
                command.CommandText = "select_ptwid_from_ext";
                command.Parameters.AddWithValue("@ext", extension);

                string i = "";
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    i = reader[0].ToString();
                }
                return i;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable GET_PTW_TO_PUSH_WFM(string ptw)
        {
            try
            {
                command.CommandText = "GET_PTW_TO_PUSH_WFM";
                command.Parameters.AddWithValue("@PTW", ptw);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        #region JAYESH KOSHTI
        public DataSet get_email_id_user_rolewise(string circle)
        {
            try
            {
                command.CommandText = "get_email_id_user_rolewise";  //This is not working
                command.Parameters.AddWithValue("@cirlce", circle);
                DataSet dt = new DataSet();
                Adap = new SqlDataAdapter(command);
                Adap.Fill(dt);
                return dt;
                //OR
                //DataTable dt = new DataTable();  //"Procedure or function get_email_id_user_rolewise has too many arguments specified."
                //reader = command.ExecuteReader();
                //dt.Load(reader);
                //return dt;
                //string str2 = "select emailid from user_master where circle='" + circle + "' and role=3";
                //SqlCommand cmd = new SqlCommand(str2, connection);
                //SqlDataAdapter da = new SqlDataAdapter(cmd);
                //DataSet ds = new DataSet();
                //da.Fill(ds);
                ////if (ds.Tables[0].Rows.Count > 0)
                ////{
                ////    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                ////    {
                ////        string[] to_email2 = new string[ds.Tables[0].Rows.Count];
                ////        to_email2[i] = ds.Tables[0].Rows[i]["emailid"].ToString();
                ////    }
                ////}
                //return ds;
                #region foreachloop
                //string[] strArr = new string[ds.Tables[0].Rows.Count];
                //int i = 0;
                //foreach (DataRow r in ds.Tables[0].Rows)
                //{
                //    strArr[i] = r["emailid"].ToString();
                //    i++;
                //}
                //return ds;
                #endregion foreachloop
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close(); con.close();
                //reader.Close();
                //reader.Dispose();
            }
        }

        public DataTable viewsearchsite_latlongrequest(string siteid, string techname)
        {
            try
            {
                command.CommandText = "viewsearchsite_latlongrequest";
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@techname", techname);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }

        public void insert_circleadmin_email_in_tt(int id, string email)
        {
            try
            {
                command.CommandText = "insert_circleadmin_email_in_tt";
                command.Parameters.AddWithValue("@id", id);
                command.Parameters.AddWithValue("@email", email);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }
        #endregion JAYESH KOSHTI

        #region tx
        public int importsite_tx(string constring, string user)
        {
            int result1;
            result1 = 0;
            OleDbConnection connection = new OleDbConnection(constring);
            try
            {


                da = new mydataaccess2();
                da.delete_error_sites();

                da = new mydataaccess2();
                vb = new myvodav2();
                connection.Open();

                String qu = "select * from [Sheet1$] ";
                OleDbCommand cmd = new OleDbCommand(qu, connection);
                System.Data.OleDb.OleDbDataReader rd1 = cmd.ExecuteReader();


                int count = 0;
                int rdcount = rd1.FieldCount;

                while (rd1.Read())
                {
                    if (rdcount == 20)
                    {
                        count = count + 1;
                        vb = new myvodav2();


                        try
                        {

                            vb.Siteid = rd1[0].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "SiteId Not Applicable.";
                            vb.Siteid = "SiteId Not Applicable.";
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();

                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Siteid = "1";
                            //return result;

                        }

                        try
                        {

                            vb.Sitename = rd1[1].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Site Name contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();
                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Sitename = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Address = rd1[2].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Address contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[0].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Address = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Lat = Convert.ToDouble(rd1[3]);
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Latitude contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            string result = 1 + "'" + Princi + "'" + count;
                            vb.Siteid = rd1[0].ToString();

                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Lat = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Longitude = Convert.ToDouble(rd1[4]);
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Longitude contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Longitude = 1;
                            //return result;

                        }
                        try
                        {

                            vb.Towertype = rd1[5].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Towertype contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Towertype = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Technician = rd1[6].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Site Inspector contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Technician = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Circle = rd1[7].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Circle contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Circle = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Zone = rd1[8].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Zone contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Zone = "1";
                            //return result;

                        }
                        try
                        {

                            vb.Subzone = rd1[9].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Subzone contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();

                            da.error_sites(vb);
                            vb.Subzone = "1";
                            //return result;

                        }
                        try
                        {
                            if (rd1[10].ToString() == "")
                            {
                                string Princi = "Date of RFAI is blank";
                                vb.Siteidnew = vb.Siteid;
                                vb.Error = Princi + "." + "The Row number is:-->" + count;
                                vb.Siteid = rd1[0].ToString();

                                string result = 1 + "'" + Princi + "'" + count;
                                da = new mydataaccess2();
                                da.error_log(vb);
                                da = new mydataaccess2();
                                da.error_sites(vb);
                                result1 = 1;
                                return result1;
                            }
                            else
                            {
                                DateTime datetime = DateTime.Parse(rd1[10].ToString());
                                vb.Datetime = datetime.ToString("yyyy-MM-dd");
                                //vb.Datetime = rd1[10].ToString();
                            }
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Datetime contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Datetime = "1";
                            result1 = 1;
                            return result1;
                        }

                        try
                        {

                            vb.Tech_contact = rd1[11].ToString();
                            vb.Check_sheet = rd1[12].ToString();
                            vb.Provider = rd1[13].ToString();
                            vb.Ip_id = rd1[14].ToString();
                            vb.Site_type = rd1[15].ToString();
                            vb.Equip_type = rd1[17].ToString();
                            vb.Network_type = rd1[18].ToString();
                            vb.Category = rd1[19].ToString();
                        }
                        catch (Exception ex)
                        {

                            string Princi = "Technician Contact No contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Tech_contact = "1";
                            //return result;

                        }
                        try
                        {
                            if (rd1[16].ToString() == "")
                            {
                                string Princi = "NSSID is blank";
                                vb.Siteidnew = vb.Siteid;
                                vb.Error = Princi + "." + "The Row number is:-->" + count;
                                vb.Siteid = rd1[0].ToString();

                                string result = 1 + "'" + Princi + "'" + count;
                                da = new mydataaccess2();
                                da.error_log(vb);
                                da = new mydataaccess2();
                                da.error_sites(vb);
                                result1 = 1;
                                return result1;
                            }
                            else
                            {
                                vb.Nssid = rd1[16].ToString();
                            }
                        }
                        catch (Exception ex)
                        {

                            string Princi = "NSSID contains error";
                            vb.Siteidnew = vb.Siteid;
                            vb.Error = Princi + "." + "The Row number is:-->" + count;
                            vb.Siteid = rd1[0].ToString();

                            string result = 1 + "'" + Princi + "'" + count;
                            da = new mydataaccess2();
                            da.error_log(vb);
                            da = new mydataaccess2();
                            da.error_sites(vb);
                            vb.Datetime = "1";
                            result1 = 1;
                            return result1;
                        }
                        vb.User = user;
                        da = new mydataaccess2();
                        int res = da.InsertVodaSite_tx(vb);
                        // return result1;
                    }


                    else
                    {
                        string Princi = "Number Of Rows are less.20 columns must be there.You are uploading wrong sheet.";
                        vb.Siteidnew = vb.Siteid;
                        vb.Error = Princi;
                        vb.Siteid = rd1[0].ToString();

                        string result = 1 + "'" + Princi + "'" + count;
                        da = new mydataaccess2();
                        da.error_log(vb);
                        da = new mydataaccess2();
                        da.error_sites(vb);
                        result1 = 1;
                        return result1;
                    }

                }
                return result1;

            }
            catch (Exception ex)
            {
                myvodav2 vb = new myvodav2();
                string Princi = ex.Message.ToString();
                vb.Siteidnew = vb.Siteid;
                vb.Error = Princi;
                vb.Siteid = "error";

                // string result = 1 + "'" + Princi + "'" + 0;
                da = new mydataaccess2();
                da.error_log(vb);
                da = new mydataaccess2();
                da.error_sites(vb);
                vb.Tech_contact = "1";
                return 1;
            }
            finally
            {
                connection.Close(); con.close();
                connection.Dispose();

            }
        }

        public int InsertVodaSite_tx(myvodav2 vb)
        {
            try
            {
                command.CommandText = "InsertVodaSite_tx";

 
                command.Parameters.AddWithValue("@Siteid12", vb.Siteid);
                 command.Parameters.AddWithValue("@Sitename12", vb.Sitename);
                command.Parameters.AddWithValue("@Address12", vb.Address);
                command.Parameters.AddWithValue("@Lat12", vb.Lat);
                command.Parameters.AddWithValue("@Longitude12", vb.Longitude);
                command.Parameters.AddWithValue("@Towertype12", vb.Towertype);
                command.Parameters.AddWithValue("@Technician12", vb.Technician);
                command.Parameters.AddWithValue("@Circle12", vb.Circle);
                command.Parameters.AddWithValue("@Zone12", vb.Zone);
                command.Parameters.AddWithValue("@Subzone12", vb.Subzone);
                command.Parameters.AddWithValue("@Datetime12", vb.Datetime);
                command.Parameters.AddWithValue("@techcontact12", vb.Tech_contact);
                command.Parameters.AddWithValue("@check_sheet", vb.Check_sheet);
                command.Parameters.AddWithValue("@user", vb.User);
                command.Parameters.AddWithValue("@provider", vb.Provider);
                command.Parameters.AddWithValue("@ip_id", vb.Ip_id);
                command.Parameters.AddWithValue("@site_type", vb.Site_type);
                command.Parameters.AddWithValue("@nssid", vb.Nssid);
                command.Parameters.AddWithValue("@equip_type", vb.Equip_type);
                command.Parameters.AddWithValue("@network_type", vb.Network_type);
                command.Parameters.AddWithValue("@category", vb.Category);
                int result = command.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();

            }
        }
#endregion
        #region tx

        public DataTable select_site_que_log_pdf_tx_car(string sheet, string siteid, int i_c, int step, string nssid)
        {
            try
            {
                command.CommandText = "select_site_que_log_pdf_tx_car";
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_counter", i_c);
                command.Parameters.AddWithValue("@step", step);
                command.Parameters.AddWithValue("@nssid", nssid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable select_site_que_log_pdf_tx_car(string sheet, string siteid, int i_c, int step)
        {
            try
            {
                command.CommandText = "select_site_que_log_pdf_tx_car_common";
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_counter", i_c);
                command.Parameters.AddWithValue("@step", step);
                
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable select_site_que_log_pdf_tx(string sheet, string siteid, int i_c, int step, string nssid)
        {
            try
            {
                command.CommandText = "select_site_que_log_pdf_tx";
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_counter", i_c);
                command.Parameters.AddWithValue("@step", step);
                command.Parameters.AddWithValue("@nssid", nssid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable select_photos_pdf_tx(string siteid, int i_c, int step, string sheet,string nssid)
        {
            try
            {
                command.CommandText = "select_photos_pdf_tx";

                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_c", i_c);
                command.Parameters.AddWithValue("@step", step);
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@nssid", nssid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        public DataTable select_photos_pdf_tx_car(string siteid, int i_c, int step, string sheet, string nssid)
        {
            try
            {
                command.CommandText = "select_photos_pdf_tx_car";

                command.Parameters.AddWithValue("@siteid", siteid);
                command.Parameters.AddWithValue("@i_c", i_c);
                command.Parameters.AddWithValue("@step", step);
                command.Parameters.AddWithValue("@sheet", sheet);
                command.Parameters.AddWithValue("@nssid", nssid);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();
                reader.Close();
                reader.Dispose();
            }
        }
        #endregion

  #region checklist
        public DataTable ptw_select_checklist(int eid, int cat)
        {
            try
            {
                command.CommandText = "ptw_select_checklist";
                command.Parameters.AddWithValue("@eid", eid);
                command.Parameters.AddWithValue("@cat", cat);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public DataTable ptw_select_checklist_issuer(string ptw, int cat)
        {
            try
            {
                command.CommandText = "ptw_select_checklist_issuer";
                command.Parameters.AddWithValue("@eid", ptw);
                command.Parameters.AddWithValue("@cat", cat);
                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }

        public void ptw_insert_into_checklist_log_master_webservice(int ptw, string checklist_id, string ans, string remark, int cat)
        {
            try
            {
                command.CommandText = "ptw_insert_into_checklist_log_master_webservice";
                command.Parameters.AddWithValue("@ptw_id", ptw);
                command.Parameters.AddWithValue("@checklist_id", Convert.ToInt64(checklist_id));
                command.Parameters.AddWithValue("@ans", ans);
                command.Parameters.AddWithValue("@remarks", remark);
                command.Parameters.AddWithValue("@cat", cat);

                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close(); con.close();


            }
        }

        public DataTable ptw_select_checklist_pdf(string ptwid)
        {
            try
            {
                command.CommandText = "ptw_select_checklist_pdf";
                command.Parameters.AddWithValue("@ptwid", ptwid);

                DataTable dt = new DataTable();
                reader = command.ExecuteReader();
                dt.Load(reader);
                return dt;

            }
            catch (Exception ex)
            {
                // throw ex;
                return null;
            }
            finally
            {
                connection.Close(); con.close();

                reader.Close();
                reader.Dispose();
            }
        }
        #endregion

    }
}
