﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for SpacialCharRemove
/// </summary>
public class SpacialCharRemove
{
    public SpacialCharRemove()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string SpacialChar_Remove(string str)
    {
        try
        {
            //string pettern = @"^[a-zA-Z0-9\_]+$";
            int lastIndex = str.LastIndexOf('.');
            string pathname = str.Substring(0, lastIndex);
            string pathext = str.Substring(lastIndex + 1);
            Regex reg = new Regex(@"^\w+$");
            if (!reg.IsMatch(pathname))
            {
                string s1 = Regex.Replace(pathname, "[^A-Za-z0-9 _]", "");
                return s1 + "." + pathext;
            }
            else
            {
                return str;
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    public static string XSS_Remove(string str)
    {
        try
        {
            //string pettern = @"([A - Za - z0 - 9\-\_] +)";
            //int lastIndex = str.LastIndexOf('.');
            //string pathname = str.Substring(0, lastIndex);
            //string pathext = str.Substring(lastIndex + 1);
            Regex reg = new Regex(@"^[a-zA-Z0-9 ./\\ _-]+$");
            if (!reg.IsMatch(str))
            {
                string s1 = Regex.Replace(str, "[^A-Za-z0-9 .\\/ _-]", "");
                return s1;
            }
            else
            {
                return str;
            }
        }
        catch (Exception ex)
        {

            throw;
        }
    }
}