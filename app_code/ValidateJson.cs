﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

/// <summary>
/// Summary description for ValidateJson
/// </summary>
public class ValidateJson
{
    //public static string Pattern = @"(=)|(!=)|(')|(<)|(>)|(;)|(\b(OR|waitfor delay|TRUNCATE|ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)";
    //Remove single cote (')
    public static string Pattern = @"(=)|(!=)|(<)|(>)|(')|(;)|(\b(OR|waitfor delay|TRUNCATE|ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\b)";
    public static string PatternAllow = @"(<@)|(<#)|(@>)|(#>)|(/)";

    public ValidateJson()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public bool DetectSqlInjection(string JsonValue)
    {
        try
        {
            Regex RE_Allow = new Regex(PatternAllow, RegexOptions.IgnoreCase);
            Match match_Allow = RE_Allow.Match(JsonValue);
            if (!match_Allow.Success)
            {
                Regex RE = new Regex(Pattern, RegexOptions.IgnoreCase);
                Match match = RE.Match(JsonValue);

                if (match.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        catch (Exception Ex)
        {
            return false;
        }
    }

    public bool DetectSqlInjection_20181012(string JsonValue)
    {
        try
        {
            Regex RE_Allow = new Regex(PatternAllow, RegexOptions.IgnoreCase);
            Match match_Allow = RE_Allow.Match(JsonValue);

            Regex RE = new Regex(Pattern, RegexOptions.IgnoreCase);
            Match match = RE.Match(JsonValue);

            if (match.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception Ex)
        {
            return false;
        }
    }

    public bool IsJsonValid(string Json)
    {
        try
        {
            if (IsJArray(Json) || IsJObject(Json))
            {
                JToken JT = JToken.Parse(Json);

                CheckTokenType(JT);
            }
            else
            {
                ValidateJobjectKeyValue(Json);
            }
            return true;
        }
        catch (Exception Ex)
        {
            return false;
        }

    }

    public void ParseJarray(string JsonArray)
    {
        try
        {
            JArray JA = JArray.Parse(JsonArray);
            foreach (var JO in JA.Children<JObject>())
            {
                List<string> keys = JO.Properties().Select(p => p.Name).ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    CheckTokenType(JO[keys[i].ToString()]);
                }
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void ParseJobject(string JsonObject)
    {
        try
        {
            JObject JO = JObject.Parse(JsonObject);
            List<string> keys = JO.Properties().Select(p => p.Name).ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                CheckTokenType(JO[keys[i].ToString()]);
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void CheckTokenType(JToken JT)
    {
        try
        {
            switch (JT.Type)
            {
                case JTokenType.Array:
                    ParseJarray(JT.ToString());
                    break;
                case JTokenType.Object:
                    ParseJobject(JT.ToString());
                    break;
                case JTokenType.String:
                    if (IsJArray(JT.ToString()))
                    {
                        ParseJarray(JT.ToString());
                    }
                    else if (IsJObject(JT.ToString()))
                    {
                        ParseJobject(JT.ToString());
                    }
                    else
                    {
                        ValidateJobjectKeyValue(JT.ToString());
                    }
                    break;
            }

        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public bool IsJArray(string Value)
    {
        try
        {
            JArray JA = JArray.Parse(Value);
            return true;
        }
        catch (Exception Ex)
        {
            return false;
        }
    }

    public bool IsJObject(string Value)
    {
        try
        {
            JObject JO = JObject.Parse(Value);
            return true;
        }
        catch (Exception Ex)
        {
            return false;
        }
    }

    public void ValidateJobjectKeyValue(string Value)
    {
        try
        {
            //ValidateJson VJ = new ValidateJson();
            //if (VJ.DetectSqlInjection(Value))
            if (DetectSqlInjection(Value))
            {
                throw new ApplicationException();
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public bool ValidateDataWithAuthJson(string data, string username, int userid)
    {
        try
        {
            if (IsJArray(data) || IsJObject(data))
            {
                JToken JT = JToken.Parse(data);

                DataTokenType(JT, username, userid);
            }
            return true;
        }
        catch (Exception Ex)
        {
            return false;
        }
    }

    public void ParseDataJarray(string JsonArray, string username, int userid)
    {
        try
        {
            JArray JA = JArray.Parse(JsonArray);
            foreach (var JO in JA.Children<JObject>())
            {
                List<string> keys = JO.Properties().Select(p => p.Name).ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    if (DataTokenIsStringOrInt(JO[keys[i].ToString()]))
                    {
                        if (CheckAuthValuesInDataKey(keys[i].ToString(), JO[keys[i].ToString()].ToString(), username, userid))
                        {
                            //success
                        }
                        else
                        {
                            throw new Exception("Invalid request");
                        }
                    }
                    else
                    {
                        DataTokenType(JO[keys[i].ToString()], username, userid);
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void ParseDataJobject(string JsonObject, string username, int userid)
    {
        try
        {
            JObject JO = JObject.Parse(JsonObject);
            List<string> keys = JO.Properties().Select(p => p.Name).ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                if (DataTokenIsStringOrInt(JO[keys[i].ToString()]))
                {
                    if (CheckAuthValuesInDataKey(keys[i].ToString(), JO[keys[i].ToString()].ToString(), username, userid))
                    {
                        //success
                    }
                    else
                    {
                        throw new Exception("Invalid request");
                    }
                }
                else
                {
                    DataTokenType(JO[keys[i].ToString()], username, userid);
                }
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public void DataTokenType(JToken JT, string username, int userid)
    {
        try
        {
            switch (JT.Type)
            {
                case JTokenType.Array:
                    ParseDataJarray(JT.ToString(), username, userid);
                    break;
                case JTokenType.Object:
                    ParseDataJobject(JT.ToString(), username, userid);
                    break;
                case JTokenType.String:
                    if (IsJArray(JT.ToString()))
                    {
                        ParseDataJarray(JT.ToString(), username, userid);
                    }
                    else if (IsJObject(JT.ToString()))
                    {
                        ParseDataJobject(JT.ToString(), username, userid);
                    }
                    else
                    {
                        //CheckAuthValuesInDataKey(JT, JT.ToString());
                    }
                    break;
            }

        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public bool DataTokenIsStringOrInt(JToken JT)
    {
        try
        {
            if (JT.Type == JTokenType.String || JT.Type == JTokenType.Integer)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception Ex)
        {
            throw Ex;
        }
    }

    public bool CheckAuthValuesInDataKey(string KeyName, string KeyValue, string username, int userid)
    {
        try
        {
            if (KeyName.ToUpper() == "USER_ID" || KeyName.ToUpper() == "USERID")
            {
                if (Convert.ToInt32(KeyValue) != userid)
                {
                    return false;
                }
            }
            if (KeyName.ToUpper() == "USERNAME")
            {
                if (KeyValue != username)
                {
                    return false;
                }
            }
            return true;
        }
        catch (Exception Ex)
        {
            return false;
        }
    }
}