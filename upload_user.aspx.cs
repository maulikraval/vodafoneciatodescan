﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using dataaccesslayer;
using dataaccesslayer2;
using business;
using businessaccesslayer;
using mybusiness;

public partial class sitecheck : System.Web.UI.Page
{
    vodabal vb;
    mydataaccess1 da;
    DataTable dt;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public string GetRandomString(int seed)
    {
        //use the following string to control your set of alphabetic characters to choose from
        //for example, you could include uppercase too
        const string alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMOPQRSTUVWXYZ123456789";

        // Random is not truly random, 
        // so we try to encourage better randomness by always changing the seed value
        Random rnd = new Random((seed + DateTime.Now.Millisecond));

        // basic 5 digit random number
        string result = rnd.Next(10000, 99999999).ToString();

        // single random character in ascii range a-z
        string alphaChar = alphabet.Substring(rnd.Next(0, alphabet.Length - 1), 1);

        // random position to put the alpha character
        int replacementIndex = rnd.Next(0, (result.Length - 1));
        result = result.Remove(replacementIndex, 1).Insert(replacementIndex, alphaChar);

        return result;
    }

    public string encode(string lbl)
    {
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }

    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //        try
        {
           
            // int flag = 0;
            da = new mydataaccess1();
            vb = new vodabal();
            //  int u = da.unique_no1(vb);


            string a = FileUpload1.FileName;
            char[] separator = new char[] { '.' };
            string[] split = a.Split(separator);
            string f = split[0] + GetRandomString(0);
            f = SpacialCharRemove.SpacialChar_Remove(f);
            f = f + ".xls";
            FileUpload1.SaveAs(Server.MapPath("~/sitesheets/") + f);

            string path = Server.MapPath("~/sitesheets/") + f;

            string excelConnectionString = @"Provider = Microsoft.Jet.OLEDB.4.0;Data Source= " + path + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\";";

            string constring = excelConnectionString;




            //int res;
            int res = da.upload_user(constring, "Sky");
            if (res == 1)
            {
                lblresult.Visible = true;
                lblresult.Text = "Incorrect Data .Upload Correct Data ";

            }
            else
            {
                lblresult.Visible = true;
                lblresult.Text = "Users Successfully Uploaded.";
            }
            da = new mydataaccess1();
            dt = new DataTable();
            dt = da.select_error_sites();
            GridView1.DataSource = dt;
            GridView1.DataBind();

            if (dt.Rows.Count > 0)
            {

                Response.Clear();

                Response.AddHeader("content-disposition", "attachment; filename=Report.xls");

                Response.Charset = "";

                // If you want the option to open the Excel file without saving than

                // comment out the line below

                // Response.Cache.SetCacheability(HttpCacheability.NoCache);

                Response.ContentType = "application/vnd.xls";

                System.IO.StringWriter stringWrite = new System.IO.StringWriter();

                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);


                GridView1.RenderControl(htmlWrite);

                Response.Write(stringWrite.ToString());

                Response.End();
            }


        }
        //      catch (Exception ex)
        {
            //throw ex;
            //Response.Redirect("~/sessionerror/sessionerrorpage.aspx");
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */

    }
}