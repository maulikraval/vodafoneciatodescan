﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using dataaccesslayer2;
using business;
using System.Net.Mail;
using System.Net;

public partial class reset : System.Web.UI.Page
{
    int ptwid = 0;

    int ext = 0;
    string issuer = "";
    int flag = 0;
    string extension = "";
    string person = "";
    string ptw = "";
    string issueremailid = "";
    string pur_mail = "";
    string uniq = "";
    int count = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                ptwid = Convert.ToInt32(Request.QueryString["ptwid"].ToString());
                uniq = Request.QueryString["uniq"].ToString();
                txt_remark.Visible = false;
                mydataaccess2 da_count = new mydataaccess2();
                count = da_count.select_unique_code_extension(ptwid, uniq);
                if (count > 0)
                {
                    ext = Convert.ToInt32(Request.QueryString["ext"].ToString());
                    flag = Convert.ToInt32(Request.QueryString["flag"].ToString());
                    issuer = Request.QueryString["issuer"].ToString();
                    person = Request.QueryString["person"].ToString();
                    ptw = Request.QueryString["ptw"].ToString();
                    issueremailid = Request.QueryString["issueremailid"].ToString();
                    pur_mail = Request.QueryString["pur_mail"].ToString();
                    int status = 0;
                    int res = 0;
                    if (flag == 1)
                    {
                        status = 2;
                        Button1.Visible = false;
                        partial.Visible = false;

                        mydataaccess2 da = new mydataaccess2();
                        res = da.ptw_select_is_mail_used(ptwid, uniq);

                        if (res == 0 || res == 5)
                        {
                            da = new mydataaccess2();
                            res = da.ptw_approveall_night(ptwid, ext, issuer, status, flag, "");
                            MyService_JSONP MJ = new MyService_JSONP();
                            string Pust_WFM = MJ.Push_WFM_ID(ptw);
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not approve this time!!!');</script>");
                        }


                    }
                    if (flag == 5)
                    {
                        status = 2;
                        Button1.Visible = false;
                        partial.Visible = false;

                        mydataaccess2 da = new mydataaccess2();
                        res = da.ptw_select_is_mail_used(ptwid, uniq);

                        if (res == 0 || res == 5)
                        {
                            da = new mydataaccess2();
                            res = da.ptw_approveall_night(ptwid, ext, issuer, status, flag, "");
                            MyService_JSONP MJ = new MyService_JSONP();
                            string Pust_WFM = MJ.Push_WFM_ID(ptw);
                        }
                        else
                        {
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not approve this time!!!');</script>");
                        }


                    }
                    if (flag == 2 || flag == 6)
                    {
                        mydataaccess2 da = new mydataaccess2();
                        res = da.ptw_select_is_mail_used(ptwid, uniq);

                        if (res == 0 || res == 5)
                        {

                            CreateDynamicTable();
                            Button1.Visible = true;
                            DataTable dt = (DataTable)Session["data"];
                            string username = "";
                            flag = 2;
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                TextBox txt1 = (TextBox)this.PlaceHolder1.FindControl("txt" + j);
                                RadioButtonList ch1 = (RadioButtonList)this.PlaceHolder1.FindControl("rd" + j);
                                int route_id = Convert.ToInt32(ch1.ToolTip);
                                ch1.SelectedIndex = 1;
                                ch1.Enabled = false;
                                txt1.Enabled = true;
                                txt1.Text = "";
                                txt1.Attributes.Add("placeholder", "Rejection Remark");
                            }
                        }
                        else
                        {
                            PlaceHolder1.Controls.Clear();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not approve/reject this time!!!');</script>");
                            partial.Visible = false;
                            Button1.Visible = false;
                        }

                        #region old_code
                        /*status = 4;
                        partial.Visible = false;
                        txt_remark.Visible = true;
                        Button1.Visible = true;*/
                        /* mydataaccess2 da = new mydataaccess2();
                         res = da.ptw_select_is_mail_used(ptwid, uniq);

                         if (res == 0 || res == 5)
                         {
                             da = new mydataaccess2();
                             res = da.ptw_approveall_night(ptwid, ext, issuer, status, flag,"");
                             MyService_JSONP MJ = new MyService_JSONP();
                             string Pust_WFM = MJ.Push_WFM_ID(ptw);
                         }
                         else
                         {
                             Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not reject this time!!!');</script>");
                         }

 */
                        #endregion old_code

                    }
                    if (flag == 3 || flag == 7)
                    {
                        mydataaccess2 da = new mydataaccess2();
                        res = da.ptw_select_is_mail_used(ptwid, uniq);

                        if (res == 0 || res == 5)
                        {

                            CreateDynamicTable();
                            Button1.Visible = true;
                        }
                        else
                        {
                            PlaceHolder1.Controls.Clear();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not approve/reject this time!!!');</script>");
                            partial.Visible = false;
                            Button1.Visible = false;
                        }

                    }
                    if (flag == 4)
                    {
                        partial.Visible = false;
                        Button1.Visible = false;
                        extension = Request.QueryString["extension"].ToString();
                        mydataaccess2 da1 = new mydataaccess2();
                        res = da1.ptw_select_is_mail_used_for_close(extension, uniq);

                        if (res == -1 || res == 3)
                        {
                            da1 = new mydataaccess2();
                            int close = da1.ptw_close_route(extension);
                            da1 = new mydataaccess2();
                            issueremailid = da1.ptw_issuer_mail_extension(extension);

                            if (close == 2)
                            {
                                mail_final_close_receiver();
                                mail_issuer_close_issuer();
                            }
                            else
                            {
                                mail();
                                mail_issuer();
                            }
                            da1 = new mydataaccess2();
                            da1.update_mail_flag(ptwid, 3);
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Closed Successfully!!!');</script>");
                        }
                        else
                        {
                            PlaceHolder1.Controls.Clear();
                            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not close this time!!!');</script>");
                        }
                    }

                    if (res == 2 && res != -1)
                    {
                        mail();
                        da_count = new mydataaccess2();
                        da_count.update_mail_flag(ptwid, 1);
                        mail_issuer();
                        da_count = new mydataaccess2();
                        da_count.update_mail_flag(ptwid, 2);
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('" + res + "');</script>");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Approved Successfully!!!');</script>");

                    }
                    if (res == 4 && res != -1)
                    {
                        mail();
                        da_count = new mydataaccess2();
                        da_count.update_mail_flag(ptwid, 1);
                        mail_issuer();
                        da_count = new mydataaccess2();
                        da_count.update_mail_flag(ptwid, 2);
                        Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Rejected Successfully!!!');</script>");

                    }
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Can not update,plz click again on link from mail!!!');</script>");
                }
            }
            catch (Exception ex)
            {
                partial.Visible = false;
                Button1.Visible = false;
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Something_Goes_Wrong');</script>");
            }
        }
    }
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        CreateDynamicTable();

    }
    private void CreateDynamicTable()
    {
        try
        {
            ptwid = Convert.ToInt32(Request.QueryString["ptwid"].ToString());
            ext = Convert.ToInt32(Request.QueryString["ext"].ToString());
            flag = Convert.ToInt32(Request.QueryString["flag"].ToString());
            issuer = Request.QueryString["issuer"].ToString();
            person = Request.QueryString["person"].ToString();
            ptw = Request.QueryString["ptw"].ToString();
            issueremailid = Request.QueryString["issueremailid"].ToString();
            pur_mail = Request.QueryString["pur_mail"].ToString();

            PlaceHolder1.Controls.Clear();
            Table tbl = new Table();
            Unit width = new Unit(100, UnitType.Percentage);
            tbl.Width = width;
            PlaceHolder1.Controls.Add(tbl);

            mydataaccess2 da1 = new mydataaccess2();
            DataTable dt = new DataTable();
            dt = da1.ptw_select_routes_for_partial(ptwid);
            Session["data"] = dt;

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    try
                    {
                        TableRow tr = new TableRow();
                        TableCell tc = new TableCell();

                        tc.Style["Width"] = "100%";


                        // tc.Style["Width"] = "100%";
                        tc.Controls.Add(new LiteralControl("<br>"));
                        Label lb = new Label();
                        lb.CssClass = "marginspace";
                        lb.Text = SpacialCharRemove.XSS_Remove(dt.Rows[i][0].ToString());
                        lb.ID = "lb" + i;
                        //lb.Width = Unit.Pixel(640);
                        tc.BorderWidth = 1;
                        tc.Controls.Add(lb);
                        tc.Controls.Add(new LiteralControl("<br>"));
                        tr.Cells.Add(tc);

                        // tc.Style["Width"] = "100%";
                        Label lb1 = new Label();
                        lb1.CssClass = "lb";
                        lb1.Text = "Start Time : " + SpacialCharRemove.XSS_Remove(dt.Rows[i]["date_from"].ToString());
                        lb1.ID = "lblstarttime" + i;

                        //lb.Width = Unit.Pixel(640);
                        tc.BorderWidth = 1;
                        tc.Controls.Add(lb1);
                        tc.Controls.Add(new LiteralControl("<br>"));
                        tr.Cells.Add(tc);

                        // tc.Style["Width"] = "100%";
                        Label lb2 = new Label();
                        lb2.CssClass = "lb";
                        lb2.Text = "End Time : " + SpacialCharRemove.XSS_Remove(dt.Rows[i]["date_to"].ToString());
                        lb2.ID = "lbendtime" + i;

                        //lb.Width = Unit.Pixel(640);
                        tc.BorderWidth = 1;
                        tc.Controls.Add(lb2);
                        tc.Controls.Add(new LiteralControl("<br>"));
                        tr.Cells.Add(tc);

                        // tc.Style["Width"] = "100%";
                        Label lb3 = new Label();
                        lb3.CssClass = "lb";
                        lb3.Text = "Start Location : " + SpacialCharRemove.XSS_Remove(dt.Rows[i]["start_location"].ToString());
                        lb3.ID = "lblStartlocation" + i;

                        //lb.Width = Unit.Pixel(640);
                        tc.BorderWidth = 1;
                        tc.Controls.Add(lb3);
                        tc.Controls.Add(new LiteralControl("<br>"));
                        tr.Cells.Add(tc);

                        // tc.Style["Width"] = "100%";
                        Label lb4 = new Label();
                        lb4.CssClass = "lb";
                        lb4.Text = "End Location : " + SpacialCharRemove.XSS_Remove(dt.Rows[i]["end_location"].ToString());
                        lb4.ID = "lblendlocation" + i;

                        //lb.Width = Unit.Pixel(640);
                        tc.BorderWidth = 1;
                        tc.Controls.Add(lb4);
                        tc.Controls.Add(new LiteralControl("<br>"));
                        tr.Cells.Add(tc);

                        Label lb_ext = new Label();
                        lb_ext.CssClass = "lb";
                        lb_ext.Text = "PTW Id : " + SpacialCharRemove.XSS_Remove(dt.Rows[i][2].ToString());
                        lb_ext.ID = "lb_ext" + i;
                        tc.BorderWidth = 1;
                        tc.Controls.Add(lb_ext);
                        tr.Cells.Add(tc);

                        tc.Controls.Add(new LiteralControl("<br>"));

                        RadioButtonList rd1 = new RadioButtonList();
                        rd1.Items.Add("Approve");
                        rd1.Items.Add("Reject");
                        rd1.ID = "rd" + i;
                        rd1.ToolTip = SpacialCharRemove.XSS_Remove(dt.Rows[i][1].ToString());
                        rd1.RepeatDirection = RepeatDirection.Horizontal;
                        //tc.BorderWidth = 1;
                        rd1.Style["width"] = "200px";
                        rd1.Height = Unit.Pixel(0);
                        rd1.CssClass = "marginspace";
                        rd1.TextChanged += new EventHandler(rd1_SelectedIndexChanged);
                        rd1.AutoPostBack = true;
                        tc.Controls.Add(rd1);

                        tr.Cells.Add(tc);

                        TextBox txt1 = new TextBox();
                        txt1.ID = "txt" + i;
                        txt1.Font.Name = "calibri";
                        txt1.Text = "";
                        txt1.Enabled = false;
                        txt1.Attributes.Add("placeholder", "Rejection Remark");
                        //txt1.ID = "details";
                        tc.Controls.Add(txt1);
                        tr.Cells.Add(tc);

                        //tc.Controls.Add(new LiteralControl("<br>"));
                        tc.HorizontalAlign = HorizontalAlign.Justify;


                        //TableCell tc1 = new TableCell();
                        ///* changes */
                        TableCell tclab = new TableCell();
                        // tclab.Style["Width"] = "10px";
                        tclab.HorizontalAlign = HorizontalAlign.Right;
                        ///* Till here */
                        //tc1.Style["Width"] = "50px";

                        /* TableCell tcreq = new TableCell();
                           RequiredFieldValidator objRequiredFieldConfirm = new RequiredFieldValidator();
                           objRequiredFieldConfirm.ControlToValidate = rd1.ClientID;
                           // tcreq.Style["Width"] = "10px";
                           objRequiredFieldConfirm.ErrorMessage = "*";

                           tcreq.Controls.Add(objRequiredFieldConfirm);*/
                        /* --- changes of label for validation*/
                        /*   Label lab1 = new Label();
                           lab1.Text = "*";
                           lab1.Visible = false;
                           lab1.ForeColor = System.Drawing.Color.Red;
                           lab1.ID = "lab1" + i;

                           tc.Controls.Add(rd1);
                           tr.Cells.Add(tc);
                           tclab.Controls.Add(lab1);


                           */
                        tr.Cells.Add(tc);
                        //tr.Cells.Add(tc1);
                        tbl.Rows.Add(tr);

                        // tr.Cells.Add(tcreq);
                        /* here  */
                        //    tr.Cells.Add(tclab);
                        /* here  */
                        //tclab.Controls.Add(new LiteralControl("<br />"));

                    }
                    catch (Exception ee)
                    { }
                }

            }
            else
            {

            }
        }
        catch (Exception ee)
        { }

    }
    public string encode(string lbl)
    {
        //byte[] img = (byte[])(Convert.FromBase64String(dr["value"].ToString()));
        //byte enc= (byte[])(Convert.FromBase64String(lblsiteid.Text));
        byte[] enc = new byte[lbl.Length];
        enc = System.Text.Encoding.UTF8.GetBytes(lbl);
        string encoded_data = Convert.ToBase64String(enc);
        return encoded_data;
    }
    public string decode(string encoded)
    {
        System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();

        System.Text.Decoder utf8Decode = encoder.GetDecoder();

        byte[] todecode_byte = Convert.FromBase64String(encoded);

        int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);

        char[] decoded_char = new char[charCount];

        utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

        string result = new String(decoded_char);

        return result;

    }
    protected void rd1_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)Session["data"];
        string username = "";
        int flag = 0;
        for (int j = 0; j < dt.Rows.Count; j++)
        {
            TextBox txt1 = (TextBox)this.PlaceHolder1.FindControl("txt" + j);
            RadioButtonList ch1 = (RadioButtonList)this.PlaceHolder1.FindControl("rd" + j);
            int route_id = Convert.ToInt32(ch1.ToolTip);
            int status = 0;
            if (ch1.Text == "Reject")
            {
                txt1.Enabled = true;
            }
            else
            {
                txt1.Text = "";
                txt1.Enabled = false;
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        DataTable dt = (DataTable)Session["data"];
        string username = "";
        int flag = 0;
        for (int j = 0; j < dt.Rows.Count; j++)
        {

            TextBox txt1 = (TextBox)this.PlaceHolder1.FindControl("txt" + j);
            RadioButtonList ch1 = (RadioButtonList)this.PlaceHolder1.FindControl("rd" + j);
            int route_id = Convert.ToInt32(ch1.ToolTip);
            int status = 0;
            if (ch1.Text == "Approve" || ch1.Text == "Reject")
            {
            }
            else
            {
                flag++;
            }
            if (ch1.Text == "Reject" && (txt1.Text == "" || txt1.Text == null))
            {
                flag = -1;
            }
        }
        if (flag == 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TextBox txt1 = (TextBox)this.PlaceHolder1.FindControl("txt" + i);
                username = dt.Rows[i]["username"].ToString();
                RadioButtonList ch1 = (RadioButtonList)this.PlaceHolder1.FindControl("rd" + i);
                int route_id = Convert.ToInt32(ch1.ToolTip);
                int status = 0;
                string rej_remark = "";
                if (ch1.Text == "Approve")
                {
                    status = 2;
                }
                else
                {
                    if (txt1.Enabled == true)
                    {
                        rej_remark = txt1.Text;
                    }
                    status = 4;
                }
                //for (int i = 0; i < dt1.Rows.Count; i++)
                //{
                //    issuer_name = dt1.Rows[i][1].ToString();
                //    string purpose_mail = "";
                //    string[] purpose_mail_split;
                //    if (purpose == "Other")
                //    {

                //        purpose_mail = purpose + "(" + purposeremark + ")";
                //    }
                //    else
                //    {
                //        purpose_mail = purpose;
                //    }
                mydataaccess2 da = new mydataaccess2();
                int res = da.ptw_approveall_night(ptwid, route_id, issuer, status, flag, rej_remark);

                MyService_JSONP MJ = new MyService_JSONP();
                string Pust_WFM = MJ.Push_WFM_ID(ptw);
            }
            /*if (flag == 2)
            {
                int status = 4;
                mydataaccess2 da = new mydataaccess2();
                int res = da.ptw_select_is_mail_used(ptwid, uniq);

                if (res == 0 || res == 5)
                {
                    da = new mydataaccess2();
                    res = da.ptw_approveall_night(ptwid, ext, issuer, status, flag, "");
                    MyService_JSONP MJ = new MyService_JSONP();
                    string Pust_WFM = MJ.Push_WFM_ID(ptw);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Action was already taken on this mail,Can Not reject this time!!!');</script>");
                }
            }*/
            mydataaccess2 da1 = new mydataaccess2();
            DataTable dtFinal = new DataTable();
            dtFinal = da1.ptw_select_routes(ptw);
            string email = dtFinal.Rows[0]["emailid"].ToString();
            string MailBody = "";
            if (dtFinal.Rows.Count > 0)
            {
                MailBody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                  "<tr>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\" >" +
                        "<b>Route</b>" +
                      "</td>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\">" +
                       "<b>Route ID</b>" +
                      "</td>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\">" +
                         "<b>Start Time</b>" +
                      "</td>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\">" +
                       "<b>End Time</b>" +
                      "</td>" +
                       "<td style=\"background-color:orange;color: #FFFFFF\">" +
                        "<b>Start Location</b>" +
                      "</td>" +
                       "</td>" +
                       "<td style=\"background-color:orange;color: #FFFFFF\">" +
                        "<b>End Location</b>" +
                      "</td>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\">" +
                        "<b>Purpose</b>" +
                      "</td>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\">" +
                        "<b>Status</b>" +
                      "</td>" +
                      "<td style=\"background-color:orange;color: #FFFFFF\">" +
                        "<b>Rejection Remark</b>" +
                      "</td>" +

                  "</tr>";
                //  "</table>";
                for (int loopCount = 0; loopCount < dtFinal.Rows.Count; loopCount++)
                {
                    string rejection_remark = dtFinal.Rows[loopCount]["rejection_remark"].ToString();
                    // MailBody += "<tr><td>" + dtFinal.Rows[loopCount][0] + "(" + dtFinal.Rows[loopCount][1] + ") </td></tr>";
                    MailBody += "<tr>" +
                        "<td style=\"border: thin solid #FF0000\" >" +
                         SendMail.ishtml(dtFinal.Rows[loopCount][0].ToString()) +
                        "</td>" +
                        "<td style=\"border: thin solid #FF0000\">" +
                          SendMail.ishtml(dtFinal.Rows[loopCount][6].ToString()) +
                        "</td>" +
                        "<td style=\"border: thin solid #FF0000\">" +
                          SendMail.ishtml(dtFinal.Rows[loopCount][1].ToString()) +
                        "</td>" +
                         "<td style=\"border: thin solid #FF0000\">" +
                           SendMail.ishtml(dtFinal.Rows[loopCount][2].ToString()) +
                        "</td>" +
                          "<td style=\"border: thin solid #FF0000\">" +
                           SendMail.ishtml(dtFinal.Rows[loopCount][3].ToString()) +
                        "</td>" +
                         "<td style=\"border: thin solid #FF0000\">" +
                         SendMail.ishtml(dtFinal.Rows[loopCount][4].ToString()) +
                        "</td>" +
                        "<td style=\"border: thin solid #FF0000\">" +
                         SendMail.ishtml(dtFinal.Rows[loopCount][7].ToString()) +
                        "</td>" +
                        "<td style=\"border: thin solid #FF0000\">" +
                         SendMail.ishtml(dtFinal.Rows[loopCount][8].ToString()) +
                        "</td>" +
                        "<td style=\"border: thin solid #FF0000\">" +
                         SendMail.ishtml(rejection_remark) +
                        "</td>" +
                    "</tr>";

                }
                MailBody += "</table>";
            }
            mydataaccess2 da_1 = new mydataaccess2();
            DataTable dt_riskat = new DataTable();
            dt_riskat = da_1.select_at_risk_data_for_report_night(ptw, person);

            string riskbody = "";
            if (dt_riskat.Rows.Count > 0)
            {
                riskbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                      "<tr>" +
                          "<td style=\"width:35%;background-color:orange;color: #FFFFFF\" >" +
                            "<b>Person</b>" +
                          "</td>" +
                          "<td style=\"width:35%;background-color:orange;color: #FFFFFF\">" +
                           "<b>Company</b>" +
                          "</td>" +

                            "</tr>";
                riskbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:35%\" >" +
                       SendMail.ishtml(dt_riskat.Rows[0][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:35%\">" +
                        SendMail.ishtml(dt_riskat.Rows[0][6].ToString()) +
                      "</td>" +

                      "</tr>";
                riskbody += "</table>";
            }

            da_1 = new mydataaccess2();
            DataTable dt_log = new DataTable();
            dt_log = da_1.ptw_select_que_log_data_report(ptw, 5, person);

            string quelogbody = "";
            if (dt_log.Rows.Count > 0)
            {
                quelogbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                   "<tr>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                         "<b>No.</b>" +
                       "</td>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                        "<b>Consideration</b>" +
                       "</td>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                          "<b>Answer(Yes/No/NA)</b>" +
                       "</td>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                          "<b>Remarks</b>" +
                       "</td>" +
                         "</tr>";
                for (int q = 0; q < dt_log.Rows.Count; q++)
                {
                    quelogbody += "<tr>" +
                          "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                           (q + 1) +
                          "</td>" +
                          "<td style=\"border: thin solid #FF0000;width:25%\">" +
                           SendMail.ishtml(dt_log.Rows[q][0].ToString()) +
                          "</td>" +
                          "<td style=\"border: thin solid #FF0000;width:25%\">" +
                            SendMail.ishtml(dt_log.Rows[q][1].ToString()) +
                          "</td>" +
                           "<td style=\"border: thin solid #FF0000;width:25%\">" +
                           SendMail.ishtml(dt_log.Rows[q][2].ToString()) +
                          "</td>" +
                          "</tr>";
                }
                quelogbody += "</table>";
            }

            mydataaccess2 da_2 = new mydataaccess2();
            DataTable dt_log2 = new DataTable();
            dt_log2 = da_2.ptw_select_risk_data_for_report(ptw, 1, "");
            string logbody = "";
            if (dt_log2.Rows.Count > 0)
            {
                logbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                   "<tr>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                         "<b>No.</b>" +
                       "</td>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                        "<b>Risks involved</b>" +
                       "</td>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                          "<b>Risk Level</b>" +
                       "</td>" +
                       "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                          "<b>Remarks</b>" +
                       "</td>" +
                         "</tr>";
                for (int k = 0; k < dt_log2.Rows.Count; k++)
                {
                    logbody += "<tr>" +
                          "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                           (k + 1) +
                          "</td>" +
                          "<td style=\"border: thin solid #FF0000;width:25%\">" +
                            SendMail.ishtml(dt_log2.Rows[k][0].ToString()) +
                          "</td>" +
                          "<td style=\"border: thin solid #FF0000;width:25%\">" +
                           SendMail.ishtml(dt_log2.Rows[k][1].ToString()) +
                          "</td>" +
                           "<td style=\"border: thin solid #FF0000;width:25%\">" +
                           SendMail.ishtml(dt_log2.Rows[k][2].ToString()) +
                          "</td>" +
                          "</tr>";
                }
                logbody += "</table>";
            }
            //string approvebody = "";
            //approvebody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
            //       "<tr>" +
            //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #99FF66;text-align:center;font-size:larger\" >" +
            //                         "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=1&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Approve All</b></a>" +
            //                       "</td>" +
            //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #FF0000;color: #FFFFFF;text-align:center;font-size:larger\">" +
            //                        "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=2&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Reject All</b></a>" +
            //                       "</td>" +

            //"</table>";

            //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", user_dt.Rows[j][1].ToString(), "Reset/Modify Request", "Dear Sir,<br /><br />Siteid is:" + log_[0].siteid + "<br />Sitename :" + dt1.Rows[0][1].ToString() + " <br />SiteAddress : " + dt1.Rows[0][2].ToString() + "Technician Name:" + log_[0].username_ + "<br /><br />To Modify : <br /><br/><a href=\"https://ciat.vodafone.in/modify.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Modify</a> <br /> <br />To Reset : <br /><br /><a href=\"https://ciat.vodafone.in/reset.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Reset</a> <br /> <br /> Regards, <br />CIAT Team ");
            // Vodafone SMTP Credentials

            //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", issueremailid, "PTW Status-(" + ptw + ")", "Dear " + issuer + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "<br/>Purpose of visit: -" + dtFinal.Rows[0]["basic_pur"].ToString() + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
            //message.IsBodyHtml = true;
            ///*            SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

            //            emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
            //            //MailAddress copy = new MailAddress("sachidanand@teleysia.com");
            //            //message.CC.Add(copy);
            //            emailClient.EnableSsl = true;*/

            //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

            //emailClient.Credentials =
            //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

            //emailClient.EnableSsl =
            //false;

            //emailClient.Send(message);
            string subject = "PTW Status-(" + ptw + ")";
            string body = "Dear " + SendMail.ishtml(issuer) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "<br/>Purpose of visit: -" + SendMail.ishtml(dtFinal.Rows[0]["basic_pur"].ToString()) + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";
            SendMail sm = new SendMail();
            sm.mailsend(issueremailid, subject, body);

            //message = new MailMessage("CITappsupport@vodafoneidea.com", email, "PTW Status-(" + ptw + ")", "Dear " + username + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "Purpose of visit" + pur_mail + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
            //message.IsBodyHtml = true;
            //emailClient = new SmtpClient("smtp.gmail.com", 587);

            string subject_mail = "PTW Status-(" + ptw + ")";
            string body_mail = "Dear " + SendMail.ishtml(username) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "Purpose of visit" + SendMail.ishtml(pur_mail) + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";

            sm.mailsend(email, subject_mail, body_mail);

            //emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
            ////MailAddress copy = new MailAddress("sachidanand@teleysia.com");
            ////message.CC.Add(copy);
            //emailClient.EnableSsl = true;

            //emailClient = new SmtpClient("10.94.147.12", 25);

            //emailClient.Credentials =
            //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

            //emailClient.EnableSsl =
            //false;

            //emailClient.Send(message);

            da_1 = new mydataaccess2();
            da_1.update_mail_flag(ptwid, 1);

            da_1 = new mydataaccess2();
            da_1.update_mail_flag(ptwid, 2);
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Proceed Successfully!!!');</script>");
            PlaceHolder1.Controls.Clear();
            partial.Visible = false;
            Button1.Visible = false;
        }
        else if (flag == -1)
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please fill the rejection remark!!!');</script>");
            flag = 0;
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "<script language=JavaScript>alert('Please Fill all values!!!');</script>");
        }
    }
    private void mail()
    {
        mydataaccess2 da1 = new mydataaccess2();
        DataTable dtFinal = new DataTable();
        dtFinal = da1.ptw_select_routes(ptw);
        string MailBody = "";
        string username = dtFinal.Rows[0]["username"].ToString();
        string email = dtFinal.Rows[0]["emailid"].ToString();
        if (dtFinal.Rows.Count > 0)
        {
            MailBody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
              "<tr>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\" >" +
                    "<b>Route</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>Route ID</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                     "<b>Start Time</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>End Time</b>" +
                  "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Start Location</b>" +
                  "</td>" +
                   "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>End Location</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Purpose</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Status</b>" +
                  "</td>" +

              "</tr>";
            //  "</table>";
            for (int loopCount = 0; loopCount < dtFinal.Rows.Count; loopCount++)
            {

                // MailBody += "<tr><td>" + dtFinal.Rows[loopCount][0] + "(" + dtFinal.Rows[loopCount][1] + ") </td></tr>";
                MailBody += "<tr>" +
                    "<td style=\"border: thin solid #FF0000\" >" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][0].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][6].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][1].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][2].ToString()) +
                    "</td>" +
                      "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][3].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][4].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                    SendMail.ishtml(dtFinal.Rows[loopCount][7].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                    SendMail.ishtml(dtFinal.Rows[loopCount][8].ToString()) +
                    "</td>" +
                "</tr>";

            }
            MailBody += "</table>";
        }
        mydataaccess2 da_1 = new mydataaccess2();
        DataTable dt_riskat = new DataTable();
        dt_riskat = da_1.select_at_risk_data_for_report_night(ptw, person);

        string riskbody = "";
        if (dt_riskat.Rows.Count > 0)
        {
            riskbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                  "<tr>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\" >" +
                        "<b>Person</b>" +
                      "</td>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\">" +
                       "<b>Company</b>" +
                      "</td>" +

                        "</tr>";
            riskbody += "<tr>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\" >" +
                   SendMail.ishtml(dt_riskat.Rows[0][0].ToString()) +
                  "</td>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\">" +
                    SendMail.ishtml(dt_riskat.Rows[0][6].ToString()) +
                  "</td>" +

                  "</tr>";
            riskbody += "</table>";
        }

        da_1 = new mydataaccess2();
        DataTable dt_log = new DataTable();
        dt_log = da_1.ptw_select_que_log_data_report(ptw, 5, person);

        string quelogbody = "";
        if (dt_log.Rows.Count > 0)
        {
            quelogbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Consideration</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Answer(Yes/No/NA)</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int q = 0; q < dt_log.Rows.Count; q++)
            {
                quelogbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (q + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            quelogbody += "</table>";
        }

        mydataaccess2 da_2 = new mydataaccess2();
        DataTable dt_log2 = new DataTable();
        dt_log2 = da_2.ptw_select_risk_data_for_report(ptw, 1, "");
        string logbody = "";
        if (dt_log2.Rows.Count > 0)
        {
            logbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Risks involved</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Risk Level</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int k = 0; k < dt_log2.Rows.Count; k++)
            {
                logbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (k + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log2.Rows[k][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log2.Rows[k][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log2.Rows[k][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            logbody += "</table>";
        }
        //string approvebody = "";
        //approvebody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
        //       "<tr>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #99FF66;text-align:center;font-size:larger\" >" +
        //                         "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=1&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Approve All</b></a>" +
        //                       "</td>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #FF0000;color: #FFFFFF;text-align:center;font-size:larger\">" +
        //                        "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=2&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Reject All</b></a>" +
        //                       "</td>" +

        //"</table>";

        ////MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", user_dt.Rows[j][1].ToString(), "Reset/Modify Request", "Dear Sir,<br /><br />Siteid is:" + log_[0].siteid + "<br />Sitename :" + dt1.Rows[0][1].ToString() + " <br />SiteAddress : " + dt1.Rows[0][2].ToString() + "Technician Name:" + log_[0].username_ + "<br /><br />To Modify : <br /><br/><a href=\"https://ciat.vodafone.in/modify.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Modify</a> <br /> <br />To Reset : <br /><br /><a href=\"https://ciat.vodafone.in/reset.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Reset</a> <br /> <br /> Regards, <br />CIAT Team ");
        //// Vodafone SMTP Credentials
        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", email, "PTW Status-(" + ptw + ")", "Dear " + username + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "<br/>Purpose of visit: -" + dtFinal.Rows[0]["basic_pur"].ToString() + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
        //message.IsBodyHtml = true;
        ///*  SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

        //  emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
        //  //MailAddress copy = new MailAddress("sachidanand@teleysia.com");
        //  //message.CC.Add(copy);
        //  emailClient.EnableSsl = true;*/
        //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

        //emailClient.Credentials =
        //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

        //emailClient.EnableSsl =
        //false;
        //emailClient.Send(message);
        string subject = "PTW Status-(" + ptw + ")";
        string body = "Dear " + SendMail.ishtml(username) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "<br/>Purpose of visit: -" + SendMail.ishtml(dtFinal.Rows[0]["basic_pur"].ToString()) + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";
        SendMail sm = new SendMail();
        sm.mailsend(email, subject, body);
    }

    private void mail_issuer()
    {
        mydataaccess2 da1 = new mydataaccess2();
        DataTable dtFinal = new DataTable();
        dtFinal = da1.ptw_select_routes(ptw);
        string MailBody = "";
        string username = dtFinal.Rows[0]["username"].ToString();
        if (dtFinal.Rows.Count > 0)
        {
            MailBody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
              "<tr>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\" >" +
                    "<b>Route</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>Route ID</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                     "<b>Start Time</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>End Time</b>" +
                  "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Start Location</b>" +
                  "</td>" +
                   "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>End Location</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Purpose</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Status</b>" +
                  "</td>" +

              "</tr>";
            //  "</table>";
            for (int loopCount = 0; loopCount < dtFinal.Rows.Count; loopCount++)
            {

                // MailBody += "<tr><td>" + dtFinal.Rows[loopCount][0] + "(" + dtFinal.Rows[loopCount][1] + ") </td></tr>";
                MailBody += "<tr>" +
                    "<td style=\"border: thin solid #FF0000\" >" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][0].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][6].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][1].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][2].ToString()) +
                    "</td>" +
                      "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][3].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][4].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][7].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][8].ToString()) +
                    "</td>" +
                "</tr>";

            }
            MailBody += "</table>";
        }
        mydataaccess2 da_1 = new mydataaccess2();
        DataTable dt_riskat = new DataTable();
        dt_riskat = da_1.select_at_risk_data_for_report_night(ptw, person);

        string riskbody = "";
        if (dt_riskat.Rows.Count > 0)
        {
            riskbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                  "<tr>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\" >" +
                        "<b>Person</b>" +
                      "</td>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\">" +
                       "<b>Company</b>" +
                      "</td>" +

                        "</tr>";
            riskbody += "<tr>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\" >" +
                    SendMail.ishtml(dt_riskat.Rows[0][0].ToString()) +
                  "</td>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\">" +
                    SendMail.ishtml(dt_riskat.Rows[0][6].ToString()) +
                  "</td>" +

                  "</tr>";
            riskbody += "</table>";
        }

        da_1 = new mydataaccess2();
        DataTable dt_log = new DataTable();
        dt_log = da_1.ptw_select_que_log_data_report(ptw, 5, person);

        string quelogbody = "";
        if (dt_log.Rows.Count > 0)
        {
            quelogbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Consideration</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Answer(Yes/No/NA)</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int q = 0; q < dt_log.Rows.Count; q++)
            {
                quelogbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (q + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            quelogbody += "</table>";
        }

        mydataaccess2 da_2 = new mydataaccess2();
        DataTable dt_log2 = new DataTable();
        dt_log2 = da_2.ptw_select_risk_data_for_report(ptw, 1, "");
        string logbody = "";
        if (dt_log2.Rows.Count > 0)
        {
            logbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Risks involved</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Risk Level</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int k = 0; k < dt_log2.Rows.Count; k++)
            {
                logbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (k + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            logbody += "</table>";
        }
        //string approvebody = "";
        //approvebody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
        //       "<tr>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #99FF66;text-align:center;font-size:larger\" >" +
        //                         "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=1&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Approve All</b></a>" +
        //                       "</td>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #FF0000;color: #FFFFFF;text-align:center;font-size:larger\">" +
        //                        "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=2&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Reject All</b></a>" +
        //                       "</td>" +

        //"</table>";

        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", user_dt.Rows[j][1].ToString(), "Reset/Modify Request", "Dear Sir,<br /><br />Siteid is:" + log_[0].siteid + "<br />Sitename :" + dt1.Rows[0][1].ToString() + " <br />SiteAddress : " + dt1.Rows[0][2].ToString() + "Technician Name:" + log_[0].username_ + "<br /><br />To Modify : <br /><br/><a href=\"https://ciat.vodafone.in/modify.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Modify</a> <br /> <br />To Reset : <br /><br /><a href=\"https://ciat.vodafone.in/reset.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Reset</a> <br /> <br /> Regards, <br />CIAT Team ");
        // Vodafone SMTP Credentials
        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", issueremailid, "PTW Status-(" + ptw + ")", "Dear " + issuer + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "<br/>Purpose of visit: -" + dtFinal.Rows[0]["basic_pur"].ToString() + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
        string htmlstr_subj = "PTW Status-(" + ptw + ")";
        string htmlstr_body = "Dear " + SendMail.ishtml(issuer) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "<br/>Purpose of visit: -" + SendMail.ishtml(dtFinal.Rows[0]["basic_pur"].ToString()) + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";
        SendMail sm = new SendMail();
        sm.mailsend(issueremailid, htmlstr_subj, htmlstr_body);
        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", issueremailid, Server.HtmlEncode(htmlstr_subj), Server.HtmlEncode(htmlstr_body));


        //message.IsBodyHtml = true;

        //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

        //emailClient.Credentials =
        //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

        //emailClient.EnableSsl =
        //false;
        ///*   SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

        //   emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
        //   //MailAddress copy = new MailAddress("sachidanand@teleysia.com");
        //   //message.CC.Add(copy);
        //   emailClient.EnableSsl = true;*/

        //emailClient.Send(message);

    }


    private void mail_final_close_receiver()
    {
        mydataaccess2 da1 = new mydataaccess2();
        DataTable dtFinal = new DataTable();
        dtFinal = da1.ptw_select_routes(ptw);
        string MailBody = "";
        string username = dtFinal.Rows[0]["username"].ToString();
        string email = dtFinal.Rows[0]["emailid"].ToString();
        if (dtFinal.Rows.Count > 0)
        {
            MailBody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
              "<tr>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\" >" +
                    "<b>Route</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>Route ID</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                     "<b>Start Time</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>End Time</b>" +
                  "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Start Location</b>" +
                  "</td>" +
                   "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>End Location</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Purpose</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Status</b>" +
                  "</td>" +

              "</tr>";
            //  "</table>";
            for (int loopCount = 0; loopCount < dtFinal.Rows.Count; loopCount++)
            {

                // MailBody += "<tr><td>" + dtFinal.Rows[loopCount][0] + "(" + dtFinal.Rows[loopCount][1] + ") </td></tr>";
                MailBody += "<tr>" +
                    "<td style=\"border: thin solid #FF0000\" >" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][0].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][6].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][1].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][2].ToString()) +
                    "</td>" +
                      "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][3].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][4].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][7].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                    SendMail.ishtml(dtFinal.Rows[loopCount][8].ToString()) +
                    "</td>" +
                "</tr>";

            }
            MailBody += "</table>";
        }
        mydataaccess2 da_1 = new mydataaccess2();
        DataTable dt_riskat = new DataTable();
        dt_riskat = da_1.select_at_risk_data_for_report_night(ptw, person);

        string riskbody = "";
        if (dt_riskat.Rows.Count > 0)
        {
            riskbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                  "<tr>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\" >" +
                        "<b>Person</b>" +
                      "</td>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\">" +
                       "<b>Company</b>" +
                      "</td>" +

                        "</tr>";
            riskbody += "<tr>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\" >" +
                   SendMail.ishtml(dt_riskat.Rows[0][0].ToString()) +
                  "</td>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\">" +
                    SendMail.ishtml(dt_riskat.Rows[0][6].ToString()) +
                  "</td>" +

                  "</tr>";
            riskbody += "</table>";
        }

        da_1 = new mydataaccess2();
        DataTable dt_log = new DataTable();
        dt_log = da_1.ptw_select_que_log_data_report(ptw, 5, person);

        string quelogbody = "";
        if (dt_log.Rows.Count > 0)
        {
            quelogbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Consideration</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Answer(Yes/No/NA)</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int q = 0; q < dt_log.Rows.Count; q++)
            {
                quelogbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (q + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            quelogbody += "</table>";
        }

        mydataaccess2 da_2 = new mydataaccess2();
        DataTable dt_log2 = new DataTable();
        dt_log2 = da_2.ptw_select_risk_data_for_report(ptw, 1, "");
        string logbody = "";
        if (dt_log2.Rows.Count > 0)
        {
            logbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Risks involved</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Risk Level</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int k = 0; k < dt_log2.Rows.Count; k++)
            {
                logbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (k + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log2.Rows[k][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            logbody += "</table>";
        }
        //string approvebody = "";
        //approvebody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
        //       "<tr>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #99FF66;text-align:center;font-size:larger\" >" +
        //                         "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=1&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Approve All</b></a>" +
        //                       "</td>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #FF0000;color: #FFFFFF;text-align:center;font-size:larger\">" +
        //                        "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=2&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Reject All</b></a>" +
        //                       "</td>" +

        //"</table>";

        ////MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", user_dt.Rows[j][1].ToString(), "Reset/Modify Request", "Dear Sir,<br /><br />Siteid is:" + log_[0].siteid + "<br />Sitename :" + dt1.Rows[0][1].ToString() + " <br />SiteAddress : " + dt1.Rows[0][2].ToString() + "Technician Name:" + log_[0].username_ + "<br /><br />To Modify : <br /><br/><a href=\"https://ciat.vodafone.in/modify.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Modify</a> <br /> <br />To Reset : <br /><br /><a href=\"https://ciat.vodafone.in/reset.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Reset</a> <br /> <br /> Regards, <br />CIAT Team ");
        //// Vodafone SMTP Credentials
        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", email, "PTW Final Close-(" + ptw + ")", "Dear " + username + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "<br/>Purpose of visit: -" + dtFinal.Rows[0]["basic_pur"].ToString() + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
        //message.IsBodyHtml = true;
        ///*   SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

        //   emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
        //   //MailAddress copy = new MailAddress("sachidanand@teleysia.com");
        //   //message.CC.Add(copy);
        //   emailClient.EnableSsl = true;*/
        //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

        //emailClient.Credentials =
        //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

        //emailClient.EnableSsl =
        //false;

        //emailClient.Send(message);
        string htmlstr_subj = "PTW Final Close-(" + ptw + ")";
        string htmlstr_body = "Dear " + SendMail.ishtml(username) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "<br/>Purpose of visit: -" + SendMail.ishtml(dtFinal.Rows[0]["basic_pur"].ToString()) + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";
        SendMail sm = new SendMail();
        sm.mailsend(email, htmlstr_subj, htmlstr_body);

    }

    private void mail_issuer_close_issuer()
    {
        mydataaccess2 da1 = new mydataaccess2();
        DataTable dtFinal = new DataTable();
        dtFinal = da1.ptw_select_routes(ptw);
        string MailBody = "";
        string username = dtFinal.Rows[0]["username"].ToString();
        if (dtFinal.Rows.Count > 0)
        {
            MailBody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
              "<tr>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\" >" +
                    "<b>Route</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>Route ID</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                     "<b>Start Time</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                   "<b>End Time</b>" +
                  "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Start Location</b>" +
                  "</td>" +
                   "</td>" +
                   "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>End Location</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Purpose</b>" +
                  "</td>" +
                  "<td style=\"background-color:orange;color: #FFFFFF\">" +
                    "<b>Status</b>" +
                  "</td>" +

              "</tr>";
            //  "</table>";
            for (int loopCount = 0; loopCount < dtFinal.Rows.Count; loopCount++)
            {

                // MailBody += "<tr><td>" + dtFinal.Rows[loopCount][0] + "(" + dtFinal.Rows[loopCount][1] + ") </td></tr>";
                MailBody += "<tr>" +
                    "<td style=\"border: thin solid #FF0000\" >" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][0].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][6].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][1].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                       SendMail.ishtml(dtFinal.Rows[loopCount][2].ToString()) +
                    "</td>" +
                      "<td style=\"border: thin solid #FF0000\">" +
                      SendMail.ishtml(dtFinal.Rows[loopCount][3].ToString()) +
                    "</td>" +
                     "<td style=\"border: thin solid #FF0000\">" +
                     SendMail.ishtml(dtFinal.Rows[loopCount][4].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                    SendMail.ishtml(dtFinal.Rows[loopCount][7].ToString()) +
                    "</td>" +
                    "<td style=\"border: thin solid #FF0000\">" +
                    SendMail.ishtml(dtFinal.Rows[loopCount][8].ToString()) +
                    "</td>" +
                "</tr>";

            }
            MailBody += "</table>";
        }
        mydataaccess2 da_1 = new mydataaccess2();
        DataTable dt_riskat = new DataTable();
        dt_riskat = da_1.select_at_risk_data_for_report_night(ptw, person);

        string riskbody = "";
        if (dt_riskat.Rows.Count > 0)
        {
            riskbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
                  "<tr>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\" >" +
                        "<b>Person</b>" +
                      "</td>" +
                      "<td style=\"width:35%;background-color:orange;color: #FFFFFF\">" +
                       "<b>Company</b>" +
                      "</td>" +

                        "</tr>";
            riskbody += "<tr>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\" >" +
                    SendMail.ishtml(dt_riskat.Rows[0][0].ToString()) +
                  "</td>" +
                  "<td style=\"border: thin solid #FF0000;width:35%\">" +
                    SendMail.ishtml(dt_riskat.Rows[0][6].ToString()) +
                  "</td>" +

                  "</tr>";
            riskbody += "</table>";
        }

        da_1 = new mydataaccess2();
        DataTable dt_log = new DataTable();
        dt_log = da_1.ptw_select_que_log_data_report(ptw, 5, person);

        string quelogbody = "";
        if (dt_log.Rows.Count > 0)
        {
            quelogbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Consideration</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Answer(Yes/No/NA)</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int q = 0; q < dt_log.Rows.Count; q++)
            {
                quelogbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (q + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log.Rows[q][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log.Rows[q][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            quelogbody += "</table>";
        }

        mydataaccess2 da_2 = new mydataaccess2();
        DataTable dt_log2 = new DataTable();
        dt_log2 = da_2.ptw_select_risk_data_for_report(ptw, 1, "");
        string logbody = "";
        if (dt_log2.Rows.Count > 0)
        {
            logbody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
               "<tr>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\" >" +
                     "<b>No.</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                    "<b>Risks involved</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Risk Level</b>" +
                   "</td>" +
                   "<td style=\"width:25%;background-color:orange;color: #FFFFFF\">" +
                      "<b>Remarks</b>" +
                   "</td>" +
                     "</tr>";
            for (int k = 0; k < dt_log2.Rows.Count; k++)
            {
                logbody += "<tr>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\" >" +
                       (k + 1) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                        SendMail.ishtml(dt_log2.Rows[k][0].ToString()) +
                      "</td>" +
                      "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log2.Rows[k][1].ToString()) +
                      "</td>" +
                       "<td style=\"border: thin solid #FF0000;width:25%\">" +
                       SendMail.ishtml(dt_log2.Rows[k][2].ToString()) +
                      "</td>" +
                      "</tr>";
            }
            logbody += "</table>";
        }
        //string approvebody = "";
        //approvebody = "<table style=\"border-width: medium; border-color: red; width: 100%;\">" +
        //       "<tr>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #99FF66;text-align:center;font-size:larger\" >" +
        //                         "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=1&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Approve All</b></a>" +
        //                       "</td>" +
        //                       "<td style=\"border: thin solid #FF0000;width:30%;background-color: #FF0000;color: #FFFFFF;text-align:center;font-size:larger\">" +
        //                        "<a href=\"http://ptw.skyproductivity.com/approve.aspx?ptwid=" + ptwid + "&flag=2&ext=" + ext + "&issuer=" + issuer + "&person=" + person + "&ptw=" + ptw + "&issueremailid=" + issueremailid + "&pur_mail=" + pur_mail + "\"><b>Reject All</b></a>" +
        //                       "</td>" +

        //"</table>";


        string htmlstr_subj = "PTW Final Close-(" + ptw + ")";
        string htmlstr_body = "Dear " + SendMail.ishtml(issuer) + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + SendMail.ishtml(ptw) + " <br />PTW applied by – " + SendMail.ishtml(username) + "<br/>Purpose of visit: -" + SendMail.ishtml(dtFinal.Rows[0]["basic_pur"].ToString()) + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ";
        SendMail sm = new SendMail();
        sm.mailsend(issueremailid, htmlstr_subj, htmlstr_body);

        ////MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", user_dt.Rows[j][1].ToString(), "Reset/Modify Request", "Dear Sir,<br /><br />Siteid is:" + log_[0].siteid + "<br />Sitename :" + dt1.Rows[0][1].ToString() + " <br />SiteAddress : " + dt1.Rows[0][2].ToString() + "Technician Name:" + log_[0].username_ + "<br /><br />To Modify : <br /><br/><a href=\"https://ciat.vodafone.in/modify.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Modify</a> <br /> <br />To Reset : <br /><br /><a href=\"https://ciat.vodafone.in/reset.aspx?user=" + name + "&siteid=" + log_[0].siteid + "&i=" + id + "&cou=" + cou + "&check_type=" + log_[0].check_type + "\">Reset</a> <br /> <br /> Regards, <br />CIAT Team ");
        //// Vodafone SMTP Credentials
        //MailMessage message = new MailMessage("CITappsupport@vodafoneidea.com", issueremailid, "PTW Final Close-(" + ptw + ")", "Dear " + issuer + ",<br /><br />PTW Request Details Are As Below :<br /><br /> Sr. No : " + ptw + " <br />PTW applied by – " + username + "<br/>Purpose of visit: -" + dtFinal.Rows[0]["basic_pur"].ToString() + "<br/>Route details as applied<br /><br />" + MailBody + "<br/>1@risk Details  result <br/>" + riskbody + "<br/>" + quelogbody + "<br/>Site /situation risk assessment result" + logbody + "<br/><br/>Click Here : https://ciat.vodafone.in <br /> <br /> Regards, <br />PTW Team ");
        //message.IsBodyHtml = true;

        //SmtpClient emailClient = new SmtpClient("10.94.147.12", 25);

        //emailClient.Credentials =
        //new NetworkCredential("CITappsupport@vodafoneidea.com", "Velvel@2011");

        //emailClient.EnableSsl =
        //false;
        ///* SmtpClient emailClient = new SmtpClient("smtp.gmail.com", 587);

        // emailClient.Credentials = new NetworkCredential("support@skyproductivity.com", "sky12345");
        // //MailAddress copy = new MailAddress("sachidanand@teleysia.com");
        // //message.CC.Add(copy);
        // emailClient.EnableSsl = true;*/

        //emailClient.Send(message);

    }
}
