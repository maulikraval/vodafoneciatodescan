﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="approve.aspx.cs" Inherits="reset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Approval Page</title>
    <link href="css/styles_low.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        input[type=radio]
        {
            display: none;
        }
        input[type=radio] + label:before
        {
            content: "";
            display: inline-block;
            width: 30px;
            height: 30px;
            vertical-align: middle;
            margin-right: 1px;
            background-color: #E5E4E2;
            box-shadow: inset 2px 2px 2px 2px rgba(0, 0, 0, .2);
            border-radius: 15px;
        }
        input[type=radio]:checked + label:before
        {
            content: "\2022";
            color: green;
            background-color: #E5E4E2;
            font-size: 3.8em;
            text-align: center;
            line-height: 31px;
            text-shadow: 0px 0px 0px #eee;
        }
        .radiobutton
        {
            background: #FFFFFF;
            color: #000000;
            border-radius: 8px;
            padding: 0.75em;
            margin: 0.25em;
            cursor: pointer;
        }
        .radiobutton_2
        {
            background: #FFFFFF;
            color: #000000;
            border-radius: 8px;
            padding: 0.95em;
            margin: 0.30em;
            cursor: pointer;
        }
        .marginspace
        {
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>

</head>
<body>
    <div id='body_bg'>
        <div class="pg" id="one">
            <div class='head'>
                <table style="width: 100%">
                    <tr>
                        <td style="text-align: center; font-names: Calibri; font-size: 13px">
                            <label style="font-size: large">
                                PTW Automation Tool</label>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            <div class="pg-main">
                <br />
                <form id="form1" runat="server">

                <div style="text-align: center" id="partial" runat="server">
                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    <br />
                    <asp:TextBox ID="txt_remark" runat="server"></asp:TextBox>
                    <asp:Button ID="Button1" runat="server" Text="GO" OnClick="Button1_Click" Style="text-align: center;
                        width: 100px; height: 30px" />
                </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>
